var structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC =
[
    [ "PoseGraphPipelineParametersC", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#aea68603d565a163129b9a1c1d892a659", null ],
    [ "flagConnected", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a40c97f4103c952cf6598831bc30a32d2", null ],
    [ "geometryEstimationPipelineParameters", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a54aee231f762b156be411a0e67829ed4", null ],
    [ "initialEstimateNoiseVariance", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a80fb2b76884b4503a467c0d3e5362d3e", null ],
    [ "inlierRejectionProbability", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a9dec0c79d47eb55b6e280f70bc3bed34", null ],
    [ "loGeneratorRatio1", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#ab9752c4d4726b736c54a29a01f50b87c", null ],
    [ "loGeneratorRatio2", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a2bebf0d810f61117b3eee78bbdb9955d", null ],
    [ "minEdgeStrength", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a6e6b71b0282368e1c57dccff45b54e46", null ],
    [ "nThreads", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineParametersC.html#a4be34e865877d953ce6d6607a14dc138", null ]
];