var structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC =
[
    [ "AppMulticameraSynchronisationParametersC", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a1ed5a7bef9871aac2678ed175e9e43a6", null ],
    [ "cameraFiles", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#af021d096c75aac053760f53f9bcf1997", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a2459d4d8bc78121be5d16a92b78ff0b8", null ],
    [ "initialEstimates", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a5e899c93ef33242fba3f0cb73f93492f", null ],
    [ "inputFilename", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a948eacba959e63c943c12fee7e7fa152", null ],
    [ "logFile", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a82ca31987907bda83ed40deede8f8c4f", null ],
    [ "minActivity", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a517e1a8aac8c88b35b6b06c773336d59", null ],
    [ "nThreads", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#aae9749bb17d8fa77ff73514ea21671aa", null ],
    [ "outputPath", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a80d370c7dbe28f8865c7543fb8d022ab", null ],
    [ "resultFile", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a387bc76eaf050ca3500aac679121cdcf", null ],
    [ "synchronisationParameters", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a3d7d2c5cb3b9508da37611c9aa7b1a90", null ],
    [ "trackFiles", "structSeeSawN_1_1AppMulticameraSynchronisationN_1_1AppMulticameraSynchronisationParametersC.html#a9e548c90683558ad23d7c5e1e6bbb320", null ]
];