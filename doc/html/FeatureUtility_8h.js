var FeatureUtility_8h =
[
    [ "ApplyFeatureCoordinateTransformation", "FeatureUtility_8h.html#a4ded92eb7f2daf16e30241fcc353fcc8", null ],
    [ "ApplyFeatureCoordinateTransformation", "FeatureUtility_8h.html#a0ed3d8dba94a6eea9961e879441e16dc", null ],
    [ "ApplyFeatureCoordinateTransformation", "FeatureUtility_8h.html#af5bfeadc3188a3a2d3f854644ba4fc42", null ],
    [ "ComputeBoundingBox2D", "FeatureUtility_8h.html#ga58fc9cd16b6263ea0d1eff6dc70ad799", null ],
    [ "DecimateFeatures2D", "FeatureUtility_8h.html#ga6db6c445bf9b07138dbb9cf279c6dce3", null ],
    [ "FindContained", "FeatureUtility_8h.html#a5fe8cf1215670b45d13f3129cebb5f7b", null ],
    [ "MakeCoordinateVector", "FeatureUtility_8h.html#a55b8a7d70fe7cb0af19aff28cf0f0995", null ],
    [ "MakeCoordinateVector", "FeatureUtility_8h.html#ga5a616c8cf1341a0fb9ed9c101e349f14", null ]
];