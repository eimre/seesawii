var Metrics =
[
    [ "Jacobian of the transfer error", "Jacobian_Transfer.html", null ],
    [ "Jacobian of the symmetric transfer error", "Jacobian_SymmetricTransfer.html", null ],
    [ "Jacobian of the Sampson error for the estimation of epipolar geometry", "Jacobian_Sampson_Epipolar.html", null ]
];