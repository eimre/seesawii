var structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC =
[
    [ "flagSort", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#a549e2cdda7b986d6702e98322f1ee1f1", null ],
    [ "maxAmbiguity", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#a7a0e553f6a28e69c33dab13c54584742", null ],
    [ "maxDensity", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#a7befa4fda3bf58e069b2f1c25f29031c", null ],
    [ "maxElements", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#ac170c1284602afb47fe60870d485b55a", null ],
    [ "minElements", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#a45c76fd655bd414d6f9890ac5507d905", null ],
    [ "quantisationStrategy", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#abe7b60d2ee26103ef1122526e7f8d818", null ]
];