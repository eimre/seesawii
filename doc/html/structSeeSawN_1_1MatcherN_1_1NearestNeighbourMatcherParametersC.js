var structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC =
[
    [ "NearestNeighbourMatcherParametersC", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC.html#aaf15c1ea4ab81dc8c047d94c96bc1674", null ],
    [ "flagConsistencyTest", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC.html#a3a5b570920b3f4f56c21d3294e8090cb", null ],
    [ "neighbourhoodSize12", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC.html#a99b1362ade948f65e060c203d7aa7826", null ],
    [ "neighbourhoodSize21", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC.html#a685825b2e9fbcf104b0894af0f2900e2", null ],
    [ "ratioTestThreshold", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC.html#aedf8beaf911b1eb62832832e4c7280c7", null ],
    [ "similarityTestThreshold", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherParametersC.html#a6521066882bab57a68a9e94d4584f771", null ]
];