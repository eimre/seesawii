var structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC =
[
    [ "TwoStageRANSACParametersC", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC.html#a9d8be5752566af3854d6abe3c359f9d5", null ],
    [ "eligibilityRatio", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC.html#ad67259bd1ea36d0006001e424ab9b1f0", null ],
    [ "minConfidence", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC.html#a10cd16e8ba0b223dacc3d573cf59198e", null ],
    [ "nThreads", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC.html#af6b81d0356642bb1db8c0cd3d8bde8db", null ],
    [ "parameters1", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC.html#a107559a1156fa65e8a03d2f0baddd8ed", null ],
    [ "parameters2", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACParametersC.html#ac3eef671c8490098aa5bb91123d75b40", null ]
];