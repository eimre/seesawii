var classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC =
[
    [ "Matrix3T", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#aff2a72044eaba44ab4ff568876352271", null ],
    [ "MatrixT", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a4879e2bbb7927d5bdc3fb8a416da0eaa", null ],
    [ "observation_type", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#ae5c25ca71a42d51c28b75e3f3e118521", null ],
    [ "ObservationQT", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a60caabf82aeb91f57fd70bccf0b39bc4", null ],
    [ "ObservationT", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a03cac11005d162447fe649cc616cbe24", null ],
    [ "quaternion_observation_type", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a9fc0fc3d122f10e023d6cb093926a393", null ],
    [ "real_type", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a9b3d9394404333bb77506b6cf207948a", null ],
    [ "RealT", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a015abc1b228b14f4c1a17ad93d3dda7d", null ],
    [ "ComputeAngularDeviation", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a1dde879297e402bcb7c6dc180c654af2", null ],
    [ "ComputeAngularDeviation", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#ad524c851a92919db7e23ffab3d92d41a", null ],
    [ "MakeCoefficientMatrix", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a87271344dd6de5f174a2a0644c1fc754", null ],
    [ "MakeSolution", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a0019633e1a14fe7cbfef73e2254eab53", null ],
    [ "Run", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a420348223a6e35fb846c1167b29a92c9", null ],
    [ "SolveLS", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a38b1e77617d92ac4b8d05a24edc20959", null ],
    [ "SolveMinimal", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a122afefe0cdded816f06b3df8227dba2", null ],
    [ "ValidateObservations", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a6deee6621515c4de72d0f96e2f7ce185", null ],
    [ "iIndex1", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a7a16b0a078b20c1c49bbc2483b1209a9", null ],
    [ "iIndex2", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a7a6471505d86299c6f6ef2d24ad5d5be", null ],
    [ "iRotation", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#ad37a34072ee37475946bd3f8af8da329", null ],
    [ "iWeight", "classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#aabd59e2b2dc97ecb9311600e9ebed25a", null ]
];