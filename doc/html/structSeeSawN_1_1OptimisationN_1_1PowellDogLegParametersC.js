var structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC =
[
    [ "PowellDogLegParametersC", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a08ff6bbd2a82e58ca08bad66dcd0abcd", null ],
    [ "delta0", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#af457597ce5452fae3e4b3565827eda9c", null ],
    [ "epsilon1", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a0c69b1c264c8acfec0766ff540c8d7c5", null ],
    [ "epsilon2", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a1c6e381aea84c5edb84ac2226aaff32e", null ],
    [ "flagCovariance", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a2b47ca143e6065bb209c90fbf4ba0cdd", null ],
    [ "lambdaGrowth", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a1d631780420f0f0df8edb73a6b7c92dc", null ],
    [ "lambdaShrink", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a95969d5d065def2d3a4052d016599b19", null ],
    [ "maxIteration", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html#a779f85670b083772865685046f9732b5", null ]
];