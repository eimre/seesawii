var dir_f2a5f3e0ee72e9f772347deaa8f421f3 =
[
    [ "KalmanFilter.h", "KalmanFilter_8h.html", "KalmanFilter_8h" ],
    [ "KalmanFilter.ipp", "KalmanFilter_8ipp.html", "KalmanFilter_8ipp" ],
    [ "KalmanFilterProblemConcept.h", "KalmanFilterProblemConcept_8h.html", null ],
    [ "KalmanFilterProblemConcept.ipp", "KalmanFilterProblemConcept_8ipp.html", "KalmanFilterProblemConcept_8ipp" ],
    [ "KFFeatureTrackingProblem.cpp", "KFFeatureTrackingProblem_8cpp.html", null ],
    [ "KFFeatureTrackingProblem.h", "KFFeatureTrackingProblem_8h.html", "KFFeatureTrackingProblem_8h" ],
    [ "KFFeatureTrackingProblem.ipp", "KFFeatureTrackingProblem_8ipp.html", "KFFeatureTrackingProblem_8ipp" ],
    [ "KFSimpleProblemBase.h", "KFSimpleProblemBase_8h.html", null ],
    [ "KFSimpleProblemBase.ipp", "KFSimpleProblemBase_8ipp.html", "KFSimpleProblemBase_8ipp" ],
    [ "KFVectorMeasurementFusionProblem.cpp", "KFVectorMeasurementFusionProblem_8cpp.html", null ],
    [ "KFVectorMeasurementFusionProblem.h", "KFVectorMeasurementFusionProblem_8h.html", null ],
    [ "KFVectorMeasurementFusionProblem.ipp", "KFVectorMeasurementFusionProblem_8ipp.html", "KFVectorMeasurementFusionProblem_8ipp" ],
    [ "TestStateEstimation.cpp", "TestStateEstimation_8cpp.html", "TestStateEstimation_8cpp" ],
    [ "UKFCameraTrackingProblemBase.h", "UKFCameraTrackingProblemBase_8h.html", null ],
    [ "UKFCameraTrackingProblemBase.ipp", "UKFCameraTrackingProblemBase_8ipp.html", "UKFCameraTrackingProblemBase_8ipp" ],
    [ "UKFOrientationTrackingProblem.cpp", "UKFOrientationTrackingProblem_8cpp.html", null ],
    [ "UKFOrientationTrackingProblem.h", "UKFOrientationTrackingProblem_8h.html", null ],
    [ "UKFOrientationTrackingProblem.ipp", "UKFOrientationTrackingProblem_8ipp.html", "UKFOrientationTrackingProblem_8ipp" ],
    [ "UnscentedKalmanFilter.h", "UnscentedKalmanFilter_8h.html", "UnscentedKalmanFilter_8h" ],
    [ "UnscentedKalmanFilter.ipp", "UnscentedKalmanFilter_8ipp.html", "UnscentedKalmanFilter_8ipp" ],
    [ "UnscentedKalmanFilterProblemConcept.h", "UnscentedKalmanFilterProblemConcept_8h.html", null ],
    [ "UnscentedKalmanFilterProblemConcept.ipp", "UnscentedKalmanFilterProblemConcept_8ipp.html", "UnscentedKalmanFilterProblemConcept_8ipp" ],
    [ "Viterbi.h", "Viterbi_8h.html", null ],
    [ "Viterbi.ipp", "Viterbi_8ipp.html", "Viterbi_8ipp" ],
    [ "ViterbiFeatureSequenceMatchingProblem.h", "ViterbiFeatureSequenceMatchingProblem_8h.html", null ],
    [ "ViterbiFeatureSequenceMatchingProblem.ipp", "ViterbiFeatureSequenceMatchingProblem_8ipp.html", "ViterbiFeatureSequenceMatchingProblem_8ipp" ],
    [ "ViterbiNormalisedHistogramMatchingProblem.cpp", "ViterbiNormalisedHistogramMatchingProblem_8cpp.html", null ],
    [ "ViterbiNormalisedHistogramMatchingProblem.h", "ViterbiNormalisedHistogramMatchingProblem_8h.html", null ],
    [ "ViterbiNormalisedHistogramMatchingProblem.ipp", "ViterbiNormalisedHistogramMatchingProblem_8ipp.html", "ViterbiNormalisedHistogramMatchingProblem_8ipp" ],
    [ "ViterbiProblemConcept.h", "ViterbiProblemConcept_8h.html", null ],
    [ "ViterbiProblemConcept.ipp", "ViterbiProblemConcept_8ipp.html", "ViterbiProblemConcept_8ipp" ],
    [ "ViterbiRelativeSynchronisationProblem.cpp", "ViterbiRelativeSynchronisationProblem_8cpp.html", null ],
    [ "ViterbiRelativeSynchronisationProblem.h", "ViterbiRelativeSynchronisationProblem_8h.html", null ],
    [ "ViterbiRelativeSynchronisationProblem.ipp", "ViterbiRelativeSynchronisationProblem_8ipp.html", "ViterbiRelativeSynchronisationProblem_8ipp" ]
];