var structSeeSawN_1_1ElementsN_1_1LensDistortionC =
[
    [ "real_type", "structSeeSawN_1_1ElementsN_1_1LensDistortionC.html#a924990c312a997b11f2d5f57ab611826", null ],
    [ "LensDistortionC", "structSeeSawN_1_1ElementsN_1_1LensDistortionC.html#a189cdd14c62ef3772082e7520984aade", null ],
    [ "centre", "structSeeSawN_1_1ElementsN_1_1LensDistortionC.html#a641fc1e9758c2f07fd550ecfb04e2768", null ],
    [ "kappa", "structSeeSawN_1_1ElementsN_1_1LensDistortionC.html#a89f2a5c35246bb3fae1d921d1846c397", null ],
    [ "modelCode", "structSeeSawN_1_1ElementsN_1_1LensDistortionC.html#a6d5e59e223bf2a1381de073f1810123f", null ]
];