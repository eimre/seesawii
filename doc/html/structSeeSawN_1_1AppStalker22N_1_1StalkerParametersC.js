var structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC =
[
    [ "StalkerParametersC", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a9a18edd2ac40e4ddc13caef6a3590396", null ],
    [ "boundingBox", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a5e79aaae83e13614685c13c9b03a4af0", null ],
    [ "distortion", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a75850478b21c2be0b1cbdba942dbde07", null ],
    [ "featureFilePattern", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a5e20fb430f749627098bd231e2151342", null ],
    [ "firstFrame", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a3a464a9f4b7a939cfa50515545469337", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a7fec422d179bcde5b5e1ec9827127e48", null ],
    [ "imageNoiseVariance", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a12dec6a1948b1486705ed6d2b37d099a", null ],
    [ "initialUncertaintyFactor", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a0c8dd739d3947d6db2e5ff747feaab1a", null ],
    [ "lastFrame", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a4ec3883b5cb078f9ab6596e41d8d6d67", null ],
    [ "logFile", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#adfb8b8c5416d7be50319138cbc916bfe", null ],
    [ "minDynamicMeasurementRatio", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a6537a9854f5c031309ad3f5181bf3007", null ],
    [ "nThreads", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a690695b51139e6323523d2ce9f795553", null ],
    [ "outputPath", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a240e3578b16bbb3963565f92017bdf96", null ],
    [ "pipelineParameters", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a1e45596a567fe2f0f546b09d9f6aae00", null ],
    [ "processNoise", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#ad1cb556e3f9db7072007f449a5a2d376", null ],
    [ "trajectoryFile", "structSeeSawN_1_1AppStalker22N_1_1StalkerParametersC.html#a607429ab5113759bba7ded032921d226", null ]
];