var group__Numerical =
[
    [ "ApplyHouseholderTransformationM", "group__Numerical.html#gaf8981c499f373d3a4e7caed3d2e9dcc5", null ],
    [ "ApplyHouseholderTransformationV", "group__Numerical.html#ga91ab45579422de71a4cd450f61cedfd6", null ],
    [ "ComputeClosestRotationMatrix", "group__Numerical.html#ga661653ede76130891830b08e0c88fd68", null ],
    [ "ComputeHouseholderVector", "group__Numerical.html#ga4a9ef69c7f3ada7b888fc985eb1a0482", null ],
    [ "ComputePseudoInverse", "group__Numerical.html#ga1876ce5895280585fbe9f23cce4e23e0", null ],
    [ "ComputeSquareRootSVD", "group__Numerical.html#ga9ec0bbbe56480d1b4f3abfd6871b5261", null ],
    [ "ConstructGraphLaplacian", "group__Numerical.html#gaf75f0cdc87835db1925d24a694b012f3", null ],
    [ "CubicPolynomialRoots", "group__Numerical.html#ga13d6b74e9873c12fce684a20b1705061", null ],
    [ "GenerateCombinations", "group__Numerical.html#ga9991ef445481a51a051b3ae2d61aa7c0", null ],
    [ "GenerateRandomCombination", "group__Numerical.html#ga3cd2efc78de3ae0f11a9a8967b09a071", null ],
    [ "GetCombinationIndex", "group__Numerical.html#ga5b1601df863f9f0a86c8eedb451fd0bb", null ],
    [ "MakeRankN", "group__Numerical.html#ga8435ad576d8f05ea39f2257d566a576f", null ],
    [ "ProjectHomogeneousToTangentPlane", "group__Numerical.html#ga261a5ace4a57d6feabeceef3456ce798", null ],
    [ "QuadraticPolynomialRoots", "group__Numerical.html#gaeb0c1b9709fce53c9a70f1163d938d3a", null ],
    [ "RQDecomposition33", "group__Numerical.html#ga7ed55f6758d97ba759f5dc5ba2827337", null ],
    [ "UpperCholeskyDecomposition33", "group__Numerical.html#ga1b5a3a278ea4296b86d3e42ca2c44ed4", null ]
];