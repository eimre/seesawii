var searchData=
[
  ['quadraticpolynomialroots',['QuadraticPolynomialRoots',['../group__Numerical.html#gaeb0c1b9709fce53c9a70f1163d938d3a',1,'SeeSawN::NumericN']]],
  ['quadric3dt',['Quadric3DT',['../group__Elements.html#gada960bbc6d205f583435a1161e89faad',1,'SeeSawN::ElementsN']]],
  ['quantisationstrategy',['quantisationStrategy',['../structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html#abe7b60d2ee26103ef1122526e7f8d818',1,'SeeSawN::ElementsN::CorrespondenceDecimatorParametersC']]],
  ['quantisealpha',['QuantiseAlpha',['../classSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationC.html#aeb8cf045023aeb0532a972f71b3e5ea1',1,'SeeSawN::SynchronisationN::MulticameraSynchonisationC']]],
  ['quantiseoffsets',['QuantiseOffsets',['../classSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationC.html#aacb14f4a57df4da22b21cf9227a43470',1,'SeeSawN::SynchronisationN::RelativeSynchronisationC']]],
  ['quaternion_5fobservation_5ftype',['quaternion_observation_type',['../classSeeSawN_1_1GeometryN_1_1RelativeRotationRegistrationC.html#a9fc0fc3d122f10e023d6cb093926a393',1,'SeeSawN::GeometryN::RelativeRotationRegistrationC']]],
  ['quaterniont',['QuaternionT',['../group__Elements.html#gac64548038cbbb75c2c8d459944285548',1,'SeeSawN::ElementsN']]],
  ['quaterniontorotationvector',['QuaternionToRotationVector',['../group__Utility.html#ga4252c59455232597d1b7f1cfc1199064',1,'SeeSawN::GeometryN']]],
  ['quaterniontovector',['QuaternionToVector',['../namespaceSeeSawN_1_1WrappersN.html#a54a51c45d116657bc4879fb4e29f59c9',1,'SeeSawN::WrappersN::QuaternionToVector(const Quaterniond &amp;)'],['../group__Utility.html#ga4a40f754da43e1b218a1cd5609c1f05c',1,'SeeSawN::WrappersN::QuaternionToVector(const Quaternion&lt; ValueT &gt; &amp;q)']]],
  ['queuecontainert',['QueueContainerT',['../classSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherC.html#adc1fc13f0cf6714477414340217bbbff',1,'SeeSawN::MatcherN::NearestNeighbourMatcherC']]],
  ['queuet',['QueueT',['../classSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherC.html#a06aaef5da569adf1e5ff99b4151296fb',1,'SeeSawN::MatcherN::NearestNeighbourMatcherC']]]
];
