var searchData=
[
  ['hammingdistance',['HammingDistance',['../classboost_1_1dynamic__bitset.html#adb5eff0ced73dc785c2933aecaa7b5a9',1,'boost::dynamic_bitset']]],
  ['hijackfindindexcorrespondences',['HijackFindIndexCorrespondences',['../classSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationC.html#a80e01237cb8f51ca34ac158698672dc4',1,'SeeSawN::SynchronisationN::RelativeSynchronisationC']]],
  ['histogrammatchingparametersc',['HistogramMatchingParametersC',['../structSeeSawN_1_1SignalProcessingN_1_1HistogramMatchingParametersC.html#a1d45675521341d889ce2857ee59b6797',1,'SeeSawN::SignalProcessingN::HistogramMatchingParametersC']]],
  ['homogeneousproject',['HomogeneousProject',['../classSeeSawN_1_1GeometryN_1_1CoordinateTransformationsC.html#a692a734f919badc09c4e1c55ea04114a',1,'SeeSawN::GeometryN::CoordinateTransformationsC::HomogeneousProject(const CoordinateRangeT &amp;coordinates, const MatrixT &amp;mP)'],['../classSeeSawN_1_1GeometryN_1_1CoordinateTransformationsC.html#a37177cd69c4710f1f3b2e4a717af6bdd',1,'SeeSawN::GeometryN::CoordinateTransformationsC::HomogeneousProject(const CoordinateRangeT &amp;coordinates, const MatrixT &amp;mP) -&gt; vector&lt; Matrix&lt; ValueT, RowsM&lt; MatrixT &gt;::value, 1 &gt; &gt;']]]
];
