var searchData=
[
  ['quadraticpolynomialroots',['QuadraticPolynomialRoots',['../group__Numerical.html#gaeb0c1b9709fce53c9a70f1163d938d3a',1,'SeeSawN::NumericN']]],
  ['quantisealpha',['QuantiseAlpha',['../classSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationC.html#aeb8cf045023aeb0532a972f71b3e5ea1',1,'SeeSawN::SynchronisationN::MulticameraSynchonisationC']]],
  ['quantiseoffsets',['QuantiseOffsets',['../classSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationC.html#aacb14f4a57df4da22b21cf9227a43470',1,'SeeSawN::SynchronisationN::RelativeSynchronisationC']]],
  ['quaterniontorotationvector',['QuaternionToRotationVector',['../group__Utility.html#ga4252c59455232597d1b7f1cfc1199064',1,'SeeSawN::GeometryN']]],
  ['quaterniontovector',['QuaternionToVector',['../namespaceSeeSawN_1_1WrappersN.html#a54a51c45d116657bc4879fb4e29f59c9',1,'SeeSawN::WrappersN::QuaternionToVector(const Quaterniond &amp;)'],['../group__Utility.html#ga4a40f754da43e1b218a1cd5609c1f05c',1,'SeeSawN::WrappersN::QuaternionToVector(const Quaternion&lt; ValueT &gt; &amp;q)']]]
];
