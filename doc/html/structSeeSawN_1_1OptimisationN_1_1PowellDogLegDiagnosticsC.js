var structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC =
[
    [ "PowellDogLegDiagnosticsC", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#acfc8db9b38c5acd36577be84731bc04b", null ],
    [ "error", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a22a737d2871966bc294d602053ef0f1d", null ],
    [ "errorHistory", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a6209c8d398b08187c1c7e3cbe416cb1b", null ],
    [ "finalDelta", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a6c9277d4dec0521ed5b248370563718e", null ],
    [ "finalEpsilon1", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a6d392e7ec4b4b1cdd0b3cb20f72c62f3", null ],
    [ "finalEpsilon2", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a22f220e1b4eb425ac1e4d17dea6f3541", null ],
    [ "flagSuccess", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a10444651e6eec142d0ed42b9be8a0dad", null ],
    [ "nIterations", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#adf067c938bffae5b2e9933d96036fef1", null ]
];