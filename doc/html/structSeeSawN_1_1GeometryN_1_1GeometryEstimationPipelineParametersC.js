var structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC =
[
    [ "GeometryEstimationPipelineParametersC", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#ac63ada7d7aa08d4ccbeb0dad782c882b", null ],
    [ "binDensity1", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a8100a1a9c099f4c4d322160116149248", null ],
    [ "binDensity2", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a4fc0edbf7b9560a50a6fed3147bb7765", null ],
    [ "flagCovariance", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#accfffbb431106293a40972d7155547b2", null ],
    [ "flagVerbose", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a30b3ad9cc04dd169253441d1d7cb90e5", null ],
    [ "geometryEstimatorParameters", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#aaa187ab7adbfe36ffff663e35242c47d", null ],
    [ "matcherParameters", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a07587a7364cd7f0211cdb9c89c2a74e8", null ],
    [ "maxAmbiguity", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a61c665d49f82fe955b9670e3886b48c3", null ],
    [ "maxIteration", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#ae05863b316f0107690ba188001129021", null ],
    [ "maxObservationDensity", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a111f8d1aac6e5c8e28f00732fbaec4ea", null ],
    [ "maxObservationRatio", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a9de36dbb3d15807080b0af975c66c6a1", null ],
    [ "minObservationRatio", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a84d5c70ddfb5396701ff82a13881117e", null ],
    [ "minRelativeDifference", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a7e7185469ed8d448d1614f89085441a5", null ],
    [ "nThreads", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a85cbc0418547fcc4f7da06527dae7368", null ],
    [ "refreshThreshold", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#aa20a75f5b65aa5adfe0abedac3893774", null ],
    [ "sutParameters", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#ae2e7b3e89c200bb1c9249230693893d1", null ],
    [ "validationRatio", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineParametersC.html#a888f7a6ac95de8bb0321d49545a282cc", null ]
];