var CorrespondenceDecimator_8ipp =
[
    [ "CorrespondenceDecimatorParametersC", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC.html", "structSeeSawN_1_1ElementsN_1_1CorrespondenceDecimatorParametersC" ],
    [ "CORRESPONDENCE_DECIMATOR_IPP_0901234", "CorrespondenceDecimator_8ipp.html#a70e8db43c262849c6d2702be6d0a3872", null ],
    [ "DecimatorQuantisationStrategyT", "CorrespondenceDecimator_8ipp.html#ac605c56bc12998b2103c9d0e2d4a4117", [
      [ "NONE", "CorrespondenceDecimator_8ipp.html#ac605c56bc12998b2103c9d0e2d4a4117ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "LEFT", "CorrespondenceDecimator_8ipp.html#ac605c56bc12998b2103c9d0e2d4a4117a684d325a7303f52e64011467ff5c5758", null ],
      [ "RIGHT", "CorrespondenceDecimator_8ipp.html#ac605c56bc12998b2103c9d0e2d4a4117a21507b40c80068eda19865706fdc2403", null ],
      [ "BOTH", "CorrespondenceDecimator_8ipp.html#ac605c56bc12998b2103c9d0e2d4a4117a6328e5e3186c227a021ef2ff77e40197", null ]
    ] ]
];