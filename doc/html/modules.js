var modules =
[
    [ "Applications", "group__Application.html", "group__Application" ],
    [ "Utilities", "group__Utility.html", "group__Utility" ],
    [ "Experiments and evaluations", "group__Experiment.html", null ],
    [ "Algorithms", "group__Algorithm.html", "group__Algorithm" ],
    [ "Concepts", "group__Concept.html", "group__Concept" ],
    [ "Tags", "group__Tag.html", "group__Tag" ],
    [ "Elements", "group__Elements.html", "group__Elements" ],
    [ "Metrics and distances", "group__Metrics.html", "group__Metrics" ],
    [ "Geometry", "group__Geometry.html", "group__Geometry" ],
    [ "Numerical operations", "group__Numerical.html", "group__Numerical" ],
    [ "Jacobians", "group__Jacobian.html", "group__Jacobian" ],
    [ "Statistical operations", "group__Statistics.html", "group__Statistics" ],
    [ "Wrappers and extensions for external libraries", "group__Wrappers.html", null ],
    [ "Input/Output", "group__IO.html", "group__IO" ],
    [ "Data generators", "group__DataGeneration.html", "group__DataGeneration" ]
];