var structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC =
[
    [ "PfMPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a9daa213b74e7e588446f9586992624dd", null ],
    [ "estimatedNoiseVariance", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a85e36b14c0edbf303fbe3499397644a6", null ],
    [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a7067e5556cd21875d02e4710adf20239", null ],
    [ "geometryEstimatorDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#adf912cfd6da9e90b5982d4d096422177", null ],
    [ "nInlierEdge", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a90f6737c678e7be893e23e8f778f1fbc", null ],
    [ "nRegistered", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a5d153694b8220fc35d552ef961eb8db8", null ],
    [ "panoramaDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a0225288e1100608cccad43473126dfe6", null ],
    [ "rotationRegistrationDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a3df0578b9d2c858cd54539165d708556", null ],
    [ "sPointCloud", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a6ef877d4bcb29ed7ccd0b8e18202c4e9", null ],
    [ "visionGraphDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a80c78dc335fc3b8b37f8fd8717e8fc63", null ]
];