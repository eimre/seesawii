var structSeeSawN_1_1StateEstimationN_1_1UnscentedKalmanFilterParametersC =
[
    [ "UnscentedKalmanFilterParametersC", "structSeeSawN_1_1StateEstimationN_1_1UnscentedKalmanFilterParametersC.html#aa2748534bd72210c689d13338318088c", null ],
    [ "alpha", "structSeeSawN_1_1StateEstimationN_1_1UnscentedKalmanFilterParametersC.html#a33360741971f8f69d7d50aeca65001ca", null ],
    [ "beta", "structSeeSawN_1_1StateEstimationN_1_1UnscentedKalmanFilterParametersC.html#aa52f70da3fd2571a7ea196b1671aa1db", null ],
    [ "kappa", "structSeeSawN_1_1StateEstimationN_1_1UnscentedKalmanFilterParametersC.html#a895b2442a57b1af175efca310a6ea35f", null ],
    [ "nThreads", "structSeeSawN_1_1StateEstimationN_1_1UnscentedKalmanFilterParametersC.html#a843d8bed44beb0aae3142827104cfa38", null ]
];