var classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC =
[
    [ "real_type", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a1d0b0b938e7f71462b4dc8080a20c67b", null ],
    [ "GroupMatchingProblemC", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a30d94d020fc7295e65c657edea8a51c3", null ],
    [ "GroupMatchingProblemC", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a3bfcb52a70495ac5993d0e6e624a25cc", null ],
    [ "Clear", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a89762caae15a0abf6a50410387e8a369", null ],
    [ "ComputeSimilarity", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a642c8f19bc1f65e9388be2e880adf183", null ],
    [ "FilterInput", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a3349ae96e02151f5b2aa674db6a47f29", null ],
    [ "GetSize1", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#ae3ea63640e9c6744dca6de73ed6921af", null ],
    [ "GetSize2", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a25e83d8a1bac6b13a41cef9c36410e62", null ],
    [ "IsValid", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a557c84b419e5a80b25cb5db2c6acb1e0", null ],
    [ "Preprocess", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a585cd98662ce77f4ceedff1b9152054b", null ],
    [ "flagValid", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a2295bafb44124d7c212d6202e7f1976c", null ],
    [ "histogram1", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#aa4f3f35392a125efa0d03cf83335610a", null ],
    [ "histogram2", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a9165a2c590e20dd995692a9c2687b0d4", null ],
    [ "indexMap1", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a6abdbebb65dd2b5f17b022976f0db894", null ],
    [ "indexMap2", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a7a13b868ab21d7a3a2d750f61b2cd1fb", null ],
    [ "minQuorum", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#acf4e98e5e1b266fa9ca13db431b20a97", null ],
    [ "scoreArray", "classSeeSawN_1_1MatcherN_1_1GroupMatchingProblemC.html#a1fb586e3082ccf4c1c03cb3e23616df0", null ]
];