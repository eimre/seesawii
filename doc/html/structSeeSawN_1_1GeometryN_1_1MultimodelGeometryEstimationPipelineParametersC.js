var structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC =
[
    [ "AssignmentStrategyT", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a8d7e07280766a7f1a70032e2186ffe87", [
      [ "SOFT", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a8d7e07280766a7f1a70032e2186ffe87a52e743143106c12bfd73aae547fc6e2e", null ],
      [ "HARD", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a8d7e07280766a7f1a70032e2186ffe87a7c144eae2e08db14c82e376603cc01f4", null ],
      [ "UNIQUE", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a8d7e07280766a7f1a70032e2186ffe87a88e3e8040c7cd11b9faffdf34372fa2a", null ]
    ] ],
    [ "MultimodelGeometryEstimationPipelineParametersC", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a65999c69043e89085c00dc7307c1d232", null ],
    [ "assignmentType", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a3e427a74c92cb7581686f0fca7e701b3", null ],
    [ "binDensity1", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#aa7483bc5c1eefdc6a7ba860e7369f791", null ],
    [ "binDensity2", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#af427a87281055b06b825f513dd1e5d06", null ],
    [ "flagVerbose", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a7b3c78f531ff0e8d0cd1bcb3a878387a", null ],
    [ "geometryEstimatorParameters", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#ae40ceed50696801e8c067860f7092a76", null ],
    [ "matcherParameters", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#aca60733023d7f3e71885b6533fdce1b4", null ],
    [ "maxObservationDensity", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a5c339f2cdc9046b8b427e9b89787aea8", null ],
    [ "minInlierRatio", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a0b7832a8e2e1257bb4f69ded7ce98b08", null ],
    [ "nThreads", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#ad1ed07b73cfd556be455ead7102e0b62", null ],
    [ "observationRatio", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a3f338536523b418d25aae61ae97572a1", null ],
    [ "ransacParameters", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a95c6e1a9e6b5a7555ea98a2978a643ab", null ],
    [ "validatorRatio", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineParametersC.html#a03a863c370c44085d4fbf98115954b98", null ]
];