var structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC =
[
    [ "displacement", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a1e179b1c308d812b34e63588d7790423", null ],
    [ "focalLength", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a1b9b9f12c3e3730a9db6ccfa77d64938", null ],
    [ "orientation", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a5fca02948117007e783faa41effa9ed4", null ],
    [ "position", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a22d46db3c322b5a459b8633f82ca789e", null ],
    [ "rotation", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a18a2bc7707669bb0c51bb2143fd67250", null ],
    [ "trackingMode", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a36bc90f536081e81134a924a2f11f0a7", null ],
    [ "zoomRate", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC_1_1TrackerStateC.html#a7c84e4f4e4ef43bc4a69f3a8b50e1be1", null ]
];