var FeatureUtility_8ipp =
[
    [ "FEATURE_UTILITY_IPP_5431356", "FeatureUtility_8ipp.html#aa7e4c69c2a229652baedd6c8f461de4e", null ],
    [ "ApplyFeatureCoordinateTransformation", "FeatureUtility_8ipp.html#af5bfeadc3188a3a2d3f854644ba4fc42", null ],
    [ "ComputeBoundingBox2D", "FeatureUtility_8ipp.html#ga58fc9cd16b6263ea0d1eff6dc70ad799", null ],
    [ "DecimateFeatures2D", "FeatureUtility_8ipp.html#ga6db6c445bf9b07138dbb9cf279c6dce3", null ],
    [ "FindContained", "FeatureUtility_8ipp.html#a5fe8cf1215670b45d13f3129cebb5f7b", null ],
    [ "MakeCoordinateVector", "FeatureUtility_8ipp.html#ga5a616c8cf1341a0fb9ed9c101e349f14", null ]
];