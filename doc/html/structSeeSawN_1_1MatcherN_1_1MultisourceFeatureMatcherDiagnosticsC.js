var structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC =
[
    [ "MultisourceFeatureMatcherDiagnosticsC", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#ac1dc3bee4181a42df4cbd2421ec135bc", null ],
    [ "flagSuccess", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#a20d68ebe4d72611bac9589e64f9d4c58", null ],
    [ "matcherDiagnostics", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#ad8c0b77d20eb0c90ea9eeda5bc3826be", null ],
    [ "nClusters", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#a4bb4012e029745bcd0e42c4f0e7746cc", null ],
    [ "nImplied", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#a1d6678c8f612a7187c6cdc0211186ee7", null ],
    [ "nObserved", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#abae7ffb609dd0d6f0c6814a6363bee07", null ],
    [ "nSourcePairs", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#ac5b282a05d33ffc8f4870409887fa2f2", null ]
];