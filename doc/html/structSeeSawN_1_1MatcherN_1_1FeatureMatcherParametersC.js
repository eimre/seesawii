var structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC =
[
    [ "FeatureMatcherParametersC", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a0f1174a15915b530fcc317b0f6df43e6", null ],
    [ "bucketDimensions", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a828129361eca195f0dc8b8de406d32d7", null ],
    [ "bucketSimilarityThreshold", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a8efb4d7fe5a5b1be066bdc67a2112c0e", null ],
    [ "flagBucketFilter", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a272c82d6c1f13f5d3b9993f15c47b02c", null ],
    [ "flagStatic", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a04d5c52ffd94f479364a58d32f50b696", null ],
    [ "minQuorum", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a7beccf007d7d00591bc25e8f86ff961a", null ],
    [ "nnParameters", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a94b389c09271f845411cbd263783256b", null ],
    [ "nThreads", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherParametersC.html#a324373f03463d18501365a76acc69691", null ]
];