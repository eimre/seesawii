var structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC =
[
    [ "AppCalibrationVerificationParametersC", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a7c9b9ecd38c150e402fb9a60a54703e6", null ],
    [ "cameraFile", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#ac1db967690b3170fbd8199a73ccfbcc2", null ],
    [ "featureFilePattern", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a095a1f0092ec010fd7a53ec01dcce243", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a1b3e0d3dfc5a978a134c0ba7b7c446c4", null ],
    [ "gridSpacing", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#ae0c662ac440a01319545bfbb42c69d2f", null ],
    [ "indexCorrect", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a414cf35bff5d3951c107110493463126", null ],
    [ "logFile", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a9b149b27898346d5c89177f1fc99656c", null ],
    [ "nThreads", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a2b18e024e5a0b390d190c919b4c7366d", null ],
    [ "outputPath", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#aaaaaf6748a1b795ab1507425cc772494", null ],
    [ "pipelineParameters", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a1d8f9432c66ae1a714c0ce5f3b175ae5", null ],
    [ "resultFile", "structSeeSawN_1_1AppCalibrationVerificationN_1_1AppCalibrationVerificationParametersC.html#a622d133fbae5a1a77f3e568dc8a3773c", null ]
];