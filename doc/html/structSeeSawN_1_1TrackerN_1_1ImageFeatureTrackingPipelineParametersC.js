var structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC =
[
    [ "ImageFeatureTrackingPipelineParametersC", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#aadc112376ed09b2f0afa6fd901c91a1c", null ],
    [ "flagPrediction", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a4f89bf033213cbe52ebacf5d6cc76f67", null ],
    [ "flagStaticCamera", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a64ef0179995bfdab212ff00ae365a452", null ],
    [ "flagTrackDynamic", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a0e08e124552208d07e1f71d353338b98", null ],
    [ "flagTrackStatic", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#ad54040a20893c7d5634d56ace4431bb8", null ],
    [ "homographyParameters", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#ab54c9485516480622362f162d9d98fc8", null ],
    [ "loGeneratorSize1", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#ae86a7074e4ed82bded7093debbc8ac6a", null ],
    [ "loGeneratorSize2", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a93a52099bef495ef331d87cc70d11590", null ],
    [ "matcherParameters", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a352001acf2bc7081e5b549c13ce38145", null ],
    [ "noiseVariance", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a3bd5f672fe386a3818887f591796bf8a", null ],
    [ "nThreads", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a6faff4869cb6cff2515232b8d97e3997", null ],
    [ "predictionErrorNoise", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a36ab4ed8ded8bcb0f7b6ce97331f8549", null ],
    [ "predictionFailureTh", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#afd689fd9e240bfe7cc1cfe42826894c7", null ],
    [ "pRejection", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a9f27e57e4dacd0eb02736e7381c606e3", null ],
    [ "seed", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#ad44f9f4c8cfa2ccb3ce4e40290727b2d", null ],
    [ "trackerParameters", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineParametersC.html#a4c5befbe2f53cfecbb276f8207a7c9f4", null ]
];