var structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC =
[
    [ "WitnessCoverageStatisticsParametersC", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a59d68f7eeb3981f08b1a92eae95b14dc", null ],
    [ "affinityMatrixFile", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a3087bc0d9f5604f376a630a625ae54f9", null ],
    [ "cameraFile", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#ad937a8236edf6307cfed7094ca04f3ea", null ],
    [ "captureVolume", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#ab0d5297ceb0c13680388372d35d7978e", null ],
    [ "density", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a6a9f8b61acc1d4322d6be3623356dc4c", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a1546fe0c634e5c90aa457efe07cf3428", null ],
    [ "framing", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#addab86dc27add654f129d1684a3276cf", null ],
    [ "logFile", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#ae224243d01603b75425099e2334e7cb4", null ],
    [ "maxRelativeScale", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#aada51a36d1b7b94f3e0c072a155361e1", null ],
    [ "maxViewpointDifference", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#af41d8df1fad3b019535554e448a6a697", null ],
    [ "minAffinity", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a0a1956f0f7e8e14b6639ade554c06290", null ],
    [ "minRedundancy", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#ac36834474f8e0ede3030b7caa7ea7ec8", null ],
    [ "minSize", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a3e96d973ee1b9f32899880a5218e9574", null ],
    [ "outputPath", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a7b2e647ccd7fe67442ade2d12cde3c55", null ],
    [ "pointCloudFile", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a0ad12a5ddb2bf86e552c9863ae9ff3d5", null ],
    [ "sceneCoverageFile", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#af14bf31be4997af260af4c8e31f24c0e", null ],
    [ "sceParameters", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#ad3e9d24e9242a85b78b225275b4766b7", null ],
    [ "viewContributionFile", "structSeeSawN_1_1AppWitnessCoverageStatisticsN_1_1WitnessCoverageStatisticsParametersC.html#a7dae4192e6f56c078c50935e366fef76", null ]
];