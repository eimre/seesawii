var structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC =
[
    [ "AppSparseUnitSphereBuilderParametersC", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#acf647d108860686e9f723ab31533e390", null ],
    [ "cameraFile", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a2c9ac3b62872a5eb2ef82f579f0c0eb4", null ],
    [ "featureFilePattern", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a13149c7d1571ac31e0d6b77be55c7b43", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a765743b8b5998051b0e15ae25542bc92", null ],
    [ "gridSpacing", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a2104a4c6ab0d253525d9c240658d3b00", null ],
    [ "logFile", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a9f71993e8ea0fcc7f14878e3ae633aaf", null ],
    [ "nThreads", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a236a91448836a60eef106978686dd422", null ],
    [ "outputPath", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a2f3f499d8f0b829620ab398175f4a502", null ],
    [ "pipelineParameters", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#ac587c530732ed61e4b3d25d245b78456", null ],
    [ "pointCloudFile", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a1af5a792e9daae83a18580eb9b31966e", null ],
    [ "sceneFeatureFile", "structSeeSawN_1_1AppSparseUnitSphereBuilderN_1_1AppSparseUnitSphereBuilderParametersC.html#a355bc8a20993046ebee486620224aea2", null ]
];