var Numeric =
[
    [ "Jacobian of matrix inversion", "Jacobian_MatrixInverse.html", null ],
    [ "Jacobian of matrix normalisation", "Jacobian_MatrixNormalise.html", null ],
    [ "d AB dA", "Jacobian_dABdA.html", null ],
    [ "dAB dB", "Jacobian_dABdB.html", null ],
    [ "d x^t(Ax) dx", "Jacobian_dxtAxdx.html", null ]
];