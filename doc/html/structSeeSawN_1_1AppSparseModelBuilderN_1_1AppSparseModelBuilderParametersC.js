var structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC =
[
    [ "AppSparseModelBuilderParametersC", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a8366fdf3df09a4676554500249ebe319", null ],
    [ "cameraFile", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#af39580c71aba94ee9228e42a5ccd8685", null ],
    [ "covarianceFile", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a32609bd84dca5ccf43045b77ca0d8842", null ],
    [ "featureFilePattern", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#ae1787c293cee80afcd75693525f5a50b", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a9ab154457fb082fcb2d0984e7f582b62", null ],
    [ "logFile", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a29c8188b7aaac11ffab5968531036256", null ],
    [ "modelFeatureFile", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a022a697e1e818e18f639604f2e29e128", null ],
    [ "modelFile", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#aaa8338f251fe204f6a530f4a0789233a", null ],
    [ "nThreads", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a0b08c6895e66cc10b4163f9161516f17", null ],
    [ "outputPath", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#abbb47f10fcec75ab450691cf7c0c3acf", null ],
    [ "spacing", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#abba2207275d5ab5e2bcff493ff3e4bea", null ],
    [ "sparse3DParameters", "structSeeSawN_1_1AppSparseModelBuilderN_1_1AppSparseModelBuilderParametersC.html#a43ede496608ba6fb47abd0e653190a19", null ]
];