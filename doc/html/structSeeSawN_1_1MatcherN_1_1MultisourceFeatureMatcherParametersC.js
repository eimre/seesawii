var structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC =
[
    [ "MultisourceFeatureMatcherParametersC", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC.html#a69c1eceda6f5f0dabc22368e40829642", null ],
    [ "flagConsistency", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC.html#ab09f95644d27717bd42323adcf07a527", null ],
    [ "flagSequentialOperation", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC.html#a683122bd08da64ad542b4a839198dd67", null ],
    [ "matcherParameters", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC.html#ac0b7ce98c5f978e90f79f49171809411", null ],
    [ "minCardinality", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC.html#a3ed00529a7787bd530c3a470a8c9e535", null ],
    [ "nThreads", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherParametersC.html#ae3181fc26c4f26cbd414251b0ec7a7a1", null ]
];