var structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC =
[
    [ "MulticameraSynchonisationParametersC", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a3a39eeb58f8fa9e60fd9ef3fc7bd0a76", null ],
    [ "alphaTolerance", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a57915c1512ba28207f9e789af3783435", null ],
    [ "flagVerbose", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#ac65704ebe43720666192f1c174ccf59c", null ],
    [ "maxAttitudeDifference", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#aba4f2d4ada3c5697a57957db569a7630", null ],
    [ "measurementInlierTh", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a6d6691de148f812852dcfbb553712b3d", null ],
    [ "nHypothesis", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#ace41d5d0f9d50e76412be4bb5d8c018d", null ],
    [ "nThreads", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a313310561a1b285f67df63c84297f64a", null ],
    [ "referenceFrameRate", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a8574c17a91c6595c25b918c379a31d05", null ],
    [ "referenceSequence", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a5d4ef9ae77f2664edd23f216dbd20f31", null ],
    [ "relativeSynchronisationParameters", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a47684084415347b13090255d6f5a3e49", null ],
    [ "seed", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#a6a9a69885ac71bd0dd11986daa8bd7a1", null ],
    [ "tauTolerance", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationParametersC.html#aadab9f117124b9a55cb08e0137f12411", null ]
];