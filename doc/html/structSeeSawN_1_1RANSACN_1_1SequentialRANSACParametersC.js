var structSeeSawN_1_1RANSACN_1_1SequentialRANSACParametersC =
[
    [ "SequentialRANSACParametersC", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACParametersC.html#a5950c41e9ab44f582e0096798cbe4d62", null ],
    [ "maxIteration", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACParametersC.html#ae1b6f588d4df9e7d3a7a69f5b45e207a", null ],
    [ "minInlierRatio", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACParametersC.html#a6b9ad32943d6bb9b1f009c0f7442f0b3", null ],
    [ "nThreads", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACParametersC.html#a3f407f8f652af7c3fbbd21fb2e91fff7", null ],
    [ "ransacParameters", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACParametersC.html#abf8f7be4db6080ebc14356dd78c78dc5", null ]
];