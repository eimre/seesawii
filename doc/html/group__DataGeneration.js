var group__DataGeneration =
[
    [ "RandomGeometryProblemGeneratorC", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html", [
      [ "camera_bound_type", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a63f0ccd99f019feb081260e64b4044c7", null ],
      [ "CameraBoundT", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#aafb91b3b3a667e202784a42476633547", null ],
      [ "RealT", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#af77ec951b8ba342bfc06bebe5cd0bd0e", null ],
      [ "structure_parameter_type", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a48e6f7bc2bc30d0a16cd5337100d2127", null ],
      [ "StructureT", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a4c47172f2ad472f3af6a3dd01e1d8974", null ],
      [ "Vector3T", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a50d457b00ac0e43bb05bad7bbf264a0f", null ],
      [ "ApplyRandom3DTransformation", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a0cbfc538bcba79d05ef76c2756d604ab", null ],
      [ "GenerateProblem22", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a02f28a9c107605bbfef571a1bcd8dbad", null ],
      [ "GenerateProblem22", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#ae7a2561c381cbf92e51830af304b0736", null ],
      [ "MakePlane", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#ae2aa6bd883eab0b93e2b491098bdf9ac", null ],
      [ "iDensity", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a8a84e43ae9ff76b3661314e27ef858a8", null ],
      [ "iDistance", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a7d4b9fae67b54d9ab1c2b878edc1799b", null ],
      [ "iIntrinsics", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#ace71a1a1d6da0da6eaa08b9537d565ef", null ],
      [ "iLensDistortion", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a0e55848344c7a36b1a5dc0ec08f63ce1", null ],
      [ "iMaxScale", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#ac132c6cb3879e429a90fd95d18c05a60", null ],
      [ "iMinScale", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#ae0ed87d50b96620ba98f27f470fbe8cd", null ],
      [ "iOrientation", "classSeeSawN_1_1DataGeneratorN_1_1RandomGeometryProblemGeneratorC.html#a0c07c9b9dfaad9916a1a1eb0b5335bd3", null ]
    ] ],
    [ "GenerateRandomCamera", "group__DataGeneration.html#ga9594d6ab0487b32e5e58cb31215edbde", null ],
    [ "GenerateRandomCameraPair", "group__DataGeneration.html#ga6571e0b6db5e17d674cec260307d9a0e", null ],
    [ "GenerateRandomDistortion", "group__DataGeneration.html#gae6081f2c66bb8892cfb2742e9b0397bc", null ],
    [ "GenerateRandomIntrinsics", "group__DataGeneration.html#ga59e27c89efd47ab07fe7baeca34d1e4b", null ],
    [ "GenerateRandomOrientation", "group__DataGeneration.html#ga052ea5df2f785288b78f00a696dcd967", null ],
    [ "GenerateRandomRectangularLattice", "group__DataGeneration.html#gab6bc62d792c09bf05289bff1092b2103", null ]
];