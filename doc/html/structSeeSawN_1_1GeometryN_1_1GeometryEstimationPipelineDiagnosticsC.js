var structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC =
[
    [ "GeometryEstimationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a027864f59f4b3f7d4a726e552d0e39a4", null ],
    [ "error", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#ab4a47f857f65c7beaf58b98c2d80ad87", null ],
    [ "featureMatcherDiagnostics", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aed001748badbaee7298e401574f74835", null ],
    [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a43e7e9faf25316f25eddf960fcf6b772", null ],
    [ "generator", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a63aed840ab8362cda63c6a7bab7d6c9c", null ],
    [ "geometryEstimatorDiagnostics", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a6e4cfbb38f4d3958dca689ee16c25265", null ],
    [ "initialObservationSet", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aced32348df5bba8cd2801ace42c08081", null ],
    [ "meanError", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aebe132400c094e72f84280e7b721b519", null ],
    [ "nIteration", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aae172b360892662290364ad835b46148", null ]
];