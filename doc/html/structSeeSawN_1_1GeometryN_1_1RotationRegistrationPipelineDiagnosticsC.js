var structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC =
[
    [ "RotationRegistrationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#acf372bae0a322eb44b31dfac065d02f6", null ],
    [ "error", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#ab0bd1a5dbe46e4c361d8efcb9b3af9cc", null ],
    [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#a48c146159ed6070752d42324efe0397a", null ],
    [ "flagSuccessLS", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#ae80e8d80e0ad5bdd3777b7ceb2c3711a", null ],
    [ "inlierRatio", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#a24fd016d701624b2b037d5a600d81181", null ],
    [ "mstDiagnostics", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#ac7e4fc5d8c98735db93c33ad00ac8f61", null ],
    [ "rstsDiagnostics", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#a96e085be24e27b07a0ff2b17b7bd822b", null ]
];