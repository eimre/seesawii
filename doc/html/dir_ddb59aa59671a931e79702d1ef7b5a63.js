var dir_ddb59aa59671a931e79702d1ef7b5a63 =
[
    [ "RANSAC.h", "RANSAC_8h.html", "RANSAC_8h" ],
    [ "RANSAC.ipp", "RANSAC_8ipp.html", "RANSAC_8ipp" ],
    [ "RANSACDualAbsoluteQuadricProblem.cpp", "RANSACDualAbsoluteQuadricProblem_8cpp.html", null ],
    [ "RANSACDualAbsoluteQuadricProblem.h", "RANSACDualAbsoluteQuadricProblem_8h.html", null ],
    [ "RANSACDualAbsoluteQuadricProblem.ipp", "RANSACDualAbsoluteQuadricProblem_8ipp.html", "RANSACDualAbsoluteQuadricProblem_8ipp" ],
    [ "RANSACGeometryEstimationProblem.h", "RANSACGeometryEstimationProblem_8h.html", "RANSACGeometryEstimationProblem_8h" ],
    [ "RANSACGeometryEstimationProblem.ipp", "RANSACGeometryEstimationProblem_8ipp.html", "RANSACGeometryEstimationProblem_8ipp" ],
    [ "RANSACGeometryEstimationProblemConcept.h", "RANSACGeometryEstimationProblemConcept_8h.html", null ],
    [ "RANSACGeometryEstimationProblemConcept.ipp", "RANSACGeometryEstimationProblemConcept_8ipp.html", "RANSACGeometryEstimationProblemConcept_8ipp" ],
    [ "RANSACProblemConcept.h", "RANSACProblemConcept_8h.html", null ],
    [ "RANSACProblemConcept.ipp", "RANSACProblemConcept_8ipp.html", "RANSACProblemConcept_8ipp" ],
    [ "SequentialRANSAC.h", "SequentialRANSAC_8h.html", "SequentialRANSAC_8h" ],
    [ "SequentialRANSAC.ipp", "SequentialRANSAC_8ipp.html", "SequentialRANSAC_8ipp" ],
    [ "TestRANSAC.cpp", "TestRANSAC_8cpp.html", "TestRANSAC_8cpp" ],
    [ "TwoStageRANSAC.h", "TwoStageRANSAC_8h.html", "TwoStageRANSAC_8h" ],
    [ "TwoStageRANSAC.ipp", "TwoStageRANSAC_8ipp.html", "TwoStageRANSAC_8ipp" ]
];