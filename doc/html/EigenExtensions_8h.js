var EigenExtensions_8h =
[
    [ "MakeCrossProductMatrix", "EigenExtensions_8h.html#a7a58341e8f21a5c2a3a8d55453ec473d", null ],
    [ "MakeCrossProductMatrix", "EigenExtensions_8h.html#ga488f17bc6b9fce547b6cc5e8e24cf5ba", null ],
    [ "QuaternionToVector", "EigenExtensions_8h.html#a54a51c45d116657bc4879fb4e29f59c9", null ],
    [ "QuaternionToVector", "EigenExtensions_8h.html#ga4a40f754da43e1b218a1cd5609c1f05c", null ],
    [ "Reshape", "EigenExtensions_8h.html#a8bbc58fab0e29c27f7ddfc76025c47ed", null ],
    [ "Reshape", "EigenExtensions_8h.html#a926c662e92313ed5ebe113b149b3e04a", null ],
    [ "Reshape", "EigenExtensions_8h.html#abbe9b32d0e03e63c535752304df838f8", null ],
    [ "Reshape", "EigenExtensions_8h.html#ga80a907c6120780799f97a391e128359e", null ]
];