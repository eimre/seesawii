var structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC =
[
    [ "FeatureTrackerParametersC", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a47d28f43def1403bea830ca36b91b41c", null ],
    [ "flagStaticDescriptor", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#afb35aad99d5405b261642511cc071675", null ],
    [ "gracePeriod", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a0f3fa5b817e8a64eab313bf599d12182", null ],
    [ "matcherParameters", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a8a026ad2d7da22732d0dc818a778a296", null ],
    [ "maxConsecutiveMiss", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a9cbd7423bc25b2a1d55592114fc2ecef", null ],
    [ "maxMahalanobisDistance", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a67a3a7095fd1db3b9e9b3ba5a61619d8", null ],
    [ "maxMissRatio", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#ae9fa614c75938718e890e63d36b619c8", null ],
    [ "minLength", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a0b705a5e9431a7aead0bc34022300751", null ],
    [ "minNeighbourDistance", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a78ddceea082faebf001ea41690086f55", null ],
    [ "nFeaturesPerPage", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerParametersC.html#a05796ffea3e10ee7eec053b8274a07c2", null ]
];