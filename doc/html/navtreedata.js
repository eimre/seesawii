var NAVTREE =
[
  [ "SeeSawII", "index.html", [
    [ "Code Organisation", "Code_Organisation.html", null ],
    [ "Notes/How To?", "How_To.html", "How_To" ],
    [ "Implementation Details", "Implementation_Details.html", "Implementation_Details" ],
    [ "Known Issues", "Known_Issues.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", "functions_type" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AppCalibrationVerification_8cpp.html",
"Camera_8cpp.html#aee53701866bdb712ec1d08d8851042a3",
"EssentialComponents_8h.html#a017df612840a12ebc0b84952174db013",
"GeometricEntity_8h.html#ga2dd6e8413d3f0d1b8587f1c92932ac25",
"InterfaceLinearAutocalibrationPipeline_8cpp.html",
"LensDistortion_8h_source.html",
"P2PComponents_8h.html#a2b25995c1a5198ce3e5dea4ca28b1608",
"PerturbationAnalysisProblemConcept_8ipp.html#a962f8d2f1d141d70e96773b00d020a99",
"SceneCoverageEstimation_8ipp_source.html",
"TestOptimisation_8cpp_source.html",
"classSeeSawN_1_1AppGeometryEstimator33N_1_1GeometryEstimator33ImplementationC.html#a88182f6f4bc54291e4077b15813927bd",
"classSeeSawN_1_1ApplicationN_1_1InterfaceNodalCameraTrackingPipelineC.html#a049a7646dc39e30ba89a732839123cb5",
"classSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineC.html#a03e35f1c066b215c85d67a1c6ffe4f84",
"classSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineProblemConceptC.html",
"classSeeSawN_1_1GeometryN_1_1OneSidedFundamentalSolverC.html#a7d586f4a18db56fffb452b6643a0ab1c",
"classSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineC.html#a73ec1d0000c93f306c27876c806f2e0a",
"classSeeSawN_1_1ION_1_1ImageFeatureTrajectoryIOC.html#a3062fda7a97b788f1f119605e1d0805c",
"classSeeSawN_1_1MetricsN_1_1EpipolarSampsonErrorC.html#a9f32492f51db0f8cc783ad3152dd92e4",
"classSeeSawN_1_1NumericN_1_1RatioRegistrationC.html#a991db4191d3121b6c8d4d75643c4c23c",
"classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html",
"classSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RSTSRatioRegistrationProblemC.html#a66c11a7350256e393990c6913df68191",
"classSeeSawN_1_1StateEstimationN_1_1ViterbiFeatureSequenceMatchingProblemC.html#aff7c30771a6e0e394e07d10907775d3f",
"classSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineC.html#a05b99b81393b5d108d76f901248984f9",
"classboost_1_1dynamic__bitset.html#aa4c107cf40fe94b9fbced657c3a811c4",
"group__DataGeneration.html#gab6bc62d792c09bf05289bff1092b2103",
"namespaceSeeSawN_1_1GeometryN_1_1DetailN.html",
"structSeeSawN_1_1AppMatchmakerN_1_1MatchmakerParametersC.html#a59d61fcf75279654945a883637f5d4f7",
"structSeeSawN_1_1GeometryN_1_1FundamentalMinimalDifferenceC.html#ab7e303303b8cdb8fc7da2ac585435a98",
"structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineParametersC.html#a86725476162d1b719bafb18c9c8eeb42",
"structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html#a469fba64899ee8f999ef8d50c8c37820"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';