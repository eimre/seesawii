var classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC =
[
    [ "BaseT", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#af49da36cf5ac95e9e178503627285927", null ],
    [ "GeometricErrorT", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#abf8d96c7ab74d30917ff56c4aa72355e", null ],
    [ "GeometrySolverT", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#af2770e9b02f03306c75d5f43d2815f38", null ],
    [ "ModelT", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#aebde77ff6ffbf86cdba9da3fe3db8214", null ],
    [ "RealT", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#aa45b7cefdc8c03a1f87f3ef048746ce2", null ],
    [ "PDLRotation3DEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#ae8fbe274ea70445ce71e4b8e699054e8", null ],
    [ "PDLRotation3DEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#a80b3977f6ed2c824dd17c325cb326a70", null ],
    [ "ComputeError", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#a548bda46e8cf9e6d3e62b6678eefaf41", null ],
    [ "ComputeJacobian", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#ac25c6d5e32ff045a318aca5cad441b70", null ],
    [ "ComputeRelativeModelDistance", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#abcc81750943ea4d43d66edb4f766d8c9", null ],
    [ "InitialEstimate", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#a49f5a2c4dd68aef1ef45dbb515eb007d", null ],
    [ "MakeResult", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#af0c1e825c7372c39df3c260f070d896c", null ],
    [ "Update", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html#a0c4dbf94677641e2ba4dd4e4d79df6e1", null ]
];