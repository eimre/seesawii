var EigenExtensions_8ipp =
[
    [ "EigenLexicalCompareC", "structSeeSawN_1_1WrappersN_1_1EigenLexicalCompareC.html", "structSeeSawN_1_1WrappersN_1_1EigenLexicalCompareC" ],
    [ "EIGEN_EXTENSIONS_IPP_5871232", "EigenExtensions_8ipp.html#a04a24f4ad8efa086add89965a7917a3a", null ],
    [ "MakeCrossProductMatrix", "EigenExtensions_8ipp.html#ga488f17bc6b9fce547b6cc5e8e24cf5ba", null ],
    [ "QuaternionToVector", "EigenExtensions_8ipp.html#ga4a40f754da43e1b218a1cd5609c1f05c", null ],
    [ "Reshape", "EigenExtensions_8ipp.html#ga80a907c6120780799f97a391e128359e", null ]
];