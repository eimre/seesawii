var structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC =
[
    [ "RelativeSynchronisationParametersC", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#abda0ec5b14595eef0ae047ba9f5bc07d", null ],
    [ "admissibleAlpha", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#ac892ab7a8ba9f456a76d6cd4018a85a5", null ],
    [ "alphaRange", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a7be01a1c05cfbf3ffd566c5621b8c351", null ],
    [ "flagFixedCamera", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#ad8aa951374baac1d9685a38421de5767", null ],
    [ "flagNoFrameDrop", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a97a6de4a2d8c1139bc62af744e6b0dda", null ],
    [ "flagPropagateFeatureCorrespondences", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a8053775230b018f00c4b0ecebf035efa", null ],
    [ "guidedSearchRegion", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#ad1cf27b2b37265871c139cd3e286e3cb", null ],
    [ "iAlphaExtent", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#ac9d05743005b8606f3fbb2159856a8c5", null ],
    [ "isolationTh", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#ada7491d449eaf158a5e7f3364a8f88bc", null ],
    [ "iTauExtent", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a2a774e8e3da88571c2a23c25415141d4", null ],
    [ "matcherParameters", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#afac58801037a88d5b574372a0f751010", null ],
    [ "maxOverlap", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a857a221f03002e5490e8eabd5ad86411", null ],
    [ "minFeatureCorrespondence", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#ab7e3902aadf95e0247f1202d07db0e10", null ],
    [ "minSegmentSupport", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#abb4569ffa8d4721e3ca1c3e6b791e7fb", null ],
    [ "minSpan", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a7432779ca6f1937deb690774a69b3d35", null ],
    [ "noiseVariance", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a9fca13b981c51bc70b1ef9192bdc035d", null ],
    [ "nThreads", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#af74579de6859b6fadaf4dfc83b4aae0a", null ],
    [ "pdlParameters", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#adbc03397ab8a9864653aa4605ab6218f", null ],
    [ "segmentInlierTh", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a4d91d36195db58151a335b7f680452d8", null ],
    [ "tauQuantisationTh", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a55129d53dedabea6ae4d28b8c28f232a", null ],
    [ "tauRange", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationParametersC.html#a45c7cd20a4791e392b8658701ee36b9f", null ]
];