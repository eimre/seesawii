var namespaceSeeSawN_1_1OptimisationN =
[
    [ "PDLEssentialMatrixEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLEssentialMatrixEstimationProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLEssentialMatrixEstimationProblemC" ],
    [ "PDLFrameAlignmentProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLFrameAlignmentProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLFrameAlignmentProblemC" ],
    [ "PDLFundamentalMatrixEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLFundamentalMatrixEstimationProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLFundamentalMatrixEstimationProblemC" ],
    [ "PDLGeometryEstimationProblemBaseC", "classSeeSawN_1_1OptimisationN_1_1PDLGeometryEstimationProblemBaseC.html", "classSeeSawN_1_1OptimisationN_1_1PDLGeometryEstimationProblemBaseC" ],
    [ "PDLHomographyKRKDecompositionProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLHomographyKRKDecompositionProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLHomographyKRKDecompositionProblemC" ],
    [ "PDLHomographyXDEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLHomographyXDEstimationProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLHomographyXDEstimationProblemC" ],
    [ "PDLOneSidedFundamentalMatrixEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLOneSidedFundamentalMatrixEstimationProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLOneSidedFundamentalMatrixEstimationProblemC" ],
    [ "PDLP2PfProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLP2PfProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLP2PfProblemC" ],
    [ "PDLP2PProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLP2PProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLP2PProblemC" ],
    [ "PDLP3PProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLP3PProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLP3PProblemC" ],
    [ "PDLP4PProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLP4PProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLP4PProblemC" ],
    [ "PDLPoseGraphProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLPoseGraphProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLPoseGraphProblemC" ],
    [ "PDLRotation3DEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLRotation3DEstimationProblemC" ],
    [ "PDLSimilarity3DEstimationProblemC", "classSeeSawN_1_1OptimisationN_1_1PDLSimilarity3DEstimationProblemC.html", "classSeeSawN_1_1OptimisationN_1_1PDLSimilarity3DEstimationProblemC" ],
    [ "PowellDogLegC", "classSeeSawN_1_1OptimisationN_1_1PowellDogLegC.html", "classSeeSawN_1_1OptimisationN_1_1PowellDogLegC" ],
    [ "PowellDogLegDiagnosticsC", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC" ],
    [ "PowellDogLegParametersC", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC.html", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegParametersC" ],
    [ "PowellDogLegProblemConceptC", "classSeeSawN_1_1OptimisationN_1_1PowellDogLegProblemConceptC.html", null ]
];