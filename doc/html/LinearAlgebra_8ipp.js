var LinearAlgebra_8ipp =
[
    [ "LINEAR_ALGEBRA_IPP_0803332", "LinearAlgebra_8ipp.html#a0aa6acfc02e52c4d8e8b3933abb1d26c", null ],
    [ "ApplyHouseholderTransformationM", "LinearAlgebra_8ipp.html#gaf8981c499f373d3a4e7caed3d2e9dcc5", null ],
    [ "ApplyHouseholderTransformationV", "LinearAlgebra_8ipp.html#ga91ab45579422de71a4cd450f61cedfd6", null ],
    [ "CartesianToSpherical", "LinearAlgebra_8ipp.html#a5f76fb6c5a7b8b2f586ebcdb03965617", null ],
    [ "ComputeClosestRotationMatrix", "LinearAlgebra_8ipp.html#ga661653ede76130891830b08e0c88fd68", null ],
    [ "ComputeHouseholderVector", "LinearAlgebra_8ipp.html#ga4a9ef69c7f3ada7b888fc985eb1a0482", null ],
    [ "ComputePseudoInverse", "LinearAlgebra_8ipp.html#ga1876ce5895280585fbe9f23cce4e23e0", null ],
    [ "ComputeSquareRootSVD", "LinearAlgebra_8ipp.html#ga9ec0bbbe56480d1b4f3abfd6871b5261", null ],
    [ "MakeRankN", "LinearAlgebra_8ipp.html#ga8435ad576d8f05ea39f2257d566a576f", null ],
    [ "ProjectHomogeneousToTangentPlane", "LinearAlgebra_8ipp.html#ga261a5ace4a57d6feabeceef3456ce798", null ],
    [ "RQDecomposition33", "LinearAlgebra_8ipp.html#ga7ed55f6758d97ba759f5dc5ba2827337", null ],
    [ "SphericalToCartesian", "LinearAlgebra_8ipp.html#a6a60557e5bb71ac0bf99987a92c1f8b0", null ],
    [ "UpperCholeskyDecomposition33", "LinearAlgebra_8ipp.html#ga1b5a3a278ea4296b86d3e42ca2c44ed4", null ]
];