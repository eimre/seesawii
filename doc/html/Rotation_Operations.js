var Rotation_Operations =
[
    [ "Jacobian of quaternion-to-rotation conversion", "Jacobian_Q2R.html", null ],
    [ "Jacobian of the quaternion multiplication operation", "Jacobian_Q1Q2.html", null ],
    [ "Jacobian of the rotation vector-quaternion conversion", "Jacobian_Rot2Q.html", null ],
    [ "Jacobian of the quaternion-rotation vector conversion", "Jacobian_Q2Rot.html", null ]
];