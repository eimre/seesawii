var Similarity_8h =
[
    [ "InverseChiSquaredIDConverterT", "Similarity_8h.html#a127db8d541dd61ed5694fbb31ab98770", null ],
    [ "InverseEuclideanIDConverterT", "Similarity_8h.html#afb9b6b646569c92293936b890de144ca", null ],
    [ "InverseEuclideanSceneFeatureDistanceConverterT", "Similarity_8h.html#a68f916394bcc5b43fca46896fa96260c", null ],
    [ "InverseEuclideanSceneImageFeatureDistanceConverterT", "Similarity_8h.html#a584b2720347146d88b3969122316959a", null ],
    [ "InverseHammingBIDConverterT", "Similarity_8h.html#a3dfa18a226a63942192f4e90510e2e00", null ],
    [ "InverseHammingSceneFeatureDistanceConverterT", "Similarity_8h.html#a206c33b2bb9e5a9b3e3d4d192f0b57e0", null ],
    [ "InverseHammingSceneImageFeatureDistanceConverterT", "Similarity_8h.html#a0cac862b6234170713fbca0ea1bee7ea", null ]
];