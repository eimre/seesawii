var structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC =
[
    [ "Geometry22ProblemT", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a314c66b656fc2ef99ab4e92fd880b7cc", [
      [ "AUTO", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a314c66b656fc2ef99ab4e92fd880b7ccae1f2d5134ed2543d38a0de9751cf75d9", null ],
      [ "HMG2", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a314c66b656fc2ef99ab4e92fd880b7cca578464f7a7ab98c528950b741e3046cf", null ],
      [ "FND", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a314c66b656fc2ef99ab4e92fd880b7cca7e6de3bff1505426eeb8ee3f89e5cea3", null ],
      [ "ESS", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a314c66b656fc2ef99ab4e92fd880b7ccaffea021a2ef02d5098bb8ae2f868d64e", null ]
    ] ],
    [ "AppMultimodelGeometryEstimator22ParametersC", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#aa7f33c07edafbd4579a4790d6285326f", null ],
    [ "cameraFile1", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a97ddc06e92073798481c79993270cbe7", null ],
    [ "cameraFile2", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a7c3e887b2f577a26ae425ba82a843a2f", null ],
    [ "correspondenceRoot", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#ad950314c3b61f50485e5d89789b6084f", null ],
    [ "covarianceRoot", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a800385f65f905f965991f2f45a61f7e5", null ],
    [ "featureFile1", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a2e895a9d9e38082f880bbfab769df6dd", null ],
    [ "featureFile2", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a3e89ab7ed03ef972e775cc709bc3cf80", null ],
    [ "flagVerbose", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a7c02dc0f1b9db497033b2e6c18c8b616", null ],
    [ "gridSpacing", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a0bd9586f1760ddddf600a419d36811de", null ],
    [ "inlierRejectionProbability", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a1679900947a9818e95ce66fe65b4ff5b", null ],
    [ "loGeneratorRatioI", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a3afec3c80770ba92b6c3cee70674df7c", null ],
    [ "loGeneratorRatioR", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a182f002424b85d4b80710afd9ed1d302", null ],
    [ "logFile", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#ab78e09b7c043ec40928f3615de78d805", null ],
    [ "modelRoot", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a31d570f5370dd49a0c7003c8ac46278d", null ],
    [ "moMaxAlgebraicDistanceI", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a25cdced78e5ea6ef7be26e89fff122db", null ],
    [ "moMaxAlgebraicDistanceR", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a68845e6b26560600167bfb0dc4e7bcb1", null ],
    [ "noiseVariance", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#afb307cb8a68e66a0aab7124222b0a4b9", null ],
    [ "nThreads", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a4963747a68f84297782fbe5afd413e24", null ],
    [ "outputPath", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#af56497bc1c1e8276473819977a68217e", null ],
    [ "pipelineParameters", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a177b612cd8184879fe1ac1204517e47c", null ],
    [ "problemType", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#ad8c27653bf058603fb1ca195b6e7c6a7", null ],
    [ "roi", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a9b35ae47dc9c54272f7140486075b4cd", null ],
    [ "seed", "structSeeSawN_1_1AppMultimodelGeometryEstimator22N_1_1AppMultimodelGeometryEstimator22ParametersC.html#a8227c31e1998b4b1b3c7cbc7fad2fa79", null ]
];