var structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC =
[
    [ "SparseUnitSphereReconstructionPipelineParametersC", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#ae25c5d1ff688148a3d736f85737dc84a", null ],
    [ "flagCompact", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#ad8a0d362cb5643e2afe3feb40af83ec5", null ],
    [ "flagVerbose", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#aefb8d01d307b6951d3257f2d1c41cffd", null ],
    [ "matcherParameters", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#a8f4258bad2adb0f01811a62f6674e5d0", null ],
    [ "noiseVariance", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#a7eecb6f8dc6df6271a4069cfb73b8ccf", null ],
    [ "panoramaParameters", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#a8f931093048b04f970c558607c7159f8", null ],
    [ "pRejection", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineParametersC.html#a2893d90cc136b0a7a35f33625e15135e", null ]
];