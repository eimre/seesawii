var structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC =
[
    [ "flagPose", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#af1864d9a2d674e391a100e005932294f", null ],
    [ "flagRelocalise", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#ac4104565c270672bcb538155a9fe4e0b", null ],
    [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a7481b569f2bb41c77686a3ed66069af4", null ],
    [ "geDiagnostics", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a2bdb2639296509efc0917cc475df2280", null ],
    [ "nInliers", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a767746f29b4227408d805f1938b465f9", null ],
    [ "nRegistration", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a09b770874ded1ec603f987400ac25bc5", null ],
    [ "supportSet", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#aa088a4d920bc34e1d6bed2ad554185d0", null ]
];