var Rotation_8cpp =
[
    [ "ComputeQuaternionSampleStatistics", "Rotation_8cpp.html#acea7f8dd5822c48c2150078dddabe2c1", null ],
    [ "JacobianQuaternionConjugation", "Rotation_8cpp.html#ad6adcec82e04b58b5a5c2c7608919f1b", null ],
    [ "JacobianQuaternionMultiplication", "Rotation_8cpp.html#a4bf482717b7b84cbf0865e01e7cedbd6", null ],
    [ "JacobianQuaternionToRotationMatrix", "Rotation_8cpp.html#ga4573f986eeb5d160384fdeaa6ab1cc85", null ],
    [ "JacobianQuaternionToRotationVector", "Rotation_8cpp.html#a85a879c7a70cd220a7c13cdaf58db6fc", null ],
    [ "JacobianRotationVectorToQuaternion", "Rotation_8cpp.html#gacabc62c859cd51349e9f2decf1d8ea0e", null ],
    [ "QuaternionToRotationVector", "Rotation_8cpp.html#ga4252c59455232597d1b7f1cfc1199064", null ],
    [ "RotationMatrixToRotationVector", "Rotation_8cpp.html#gaf507482302c08ea77f18b7a8081521e4", null ],
    [ "RotationVectorToAxisAngle", "Rotation_8cpp.html#gaf5928fe176f13d5147a804449d60e1e6", null ],
    [ "RotationVectorToQuaternion", "Rotation_8cpp.html#ga7b7d4c0b0b31905d15b7ec119cd1103b", null ]
];