var structSeeSawN_1_1UncertaintyEstimationN_1_1ScaledUnscentedTransformationParametersC =
[
    [ "ScaledUnscentedTransformationParametersC", "structSeeSawN_1_1UncertaintyEstimationN_1_1ScaledUnscentedTransformationParametersC.html#a3d805cb09ca8497282a61e5cb67e75a0", null ],
    [ "alpha", "structSeeSawN_1_1UncertaintyEstimationN_1_1ScaledUnscentedTransformationParametersC.html#aa7ba6c9aa01fb615ee5ecb08ddceb2da", null ],
    [ "beta", "structSeeSawN_1_1UncertaintyEstimationN_1_1ScaledUnscentedTransformationParametersC.html#a0951e9e3776e5bb2e3311ff05d800ce2", null ],
    [ "kappa", "structSeeSawN_1_1UncertaintyEstimationN_1_1ScaledUnscentedTransformationParametersC.html#a2910064137d8ef4ad5750578ef716d26", null ],
    [ "nThreads", "structSeeSawN_1_1UncertaintyEstimationN_1_1ScaledUnscentedTransformationParametersC.html#a149bc1ef2e8fa4f34c30541b1c34f6b5", null ]
];