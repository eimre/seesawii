var group__Diagnostics =
[
    [ "MultiviewPanoramaBuilderDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1MultiviewPanoramaBuilderDiagnosticsC.html", [
      [ "MultiviewPanoramaBuilderDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1MultiviewPanoramaBuilderDiagnosticsC.html#ac8ac994a60eb8a8168086e5b624d891f", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1MultiviewPanoramaBuilderDiagnosticsC.html#a02844236f48e0bdf64822b1e1c6fce67", null ],
      [ "nPoints", "structSeeSawN_1_1GeometryN_1_1MultiviewPanoramaBuilderDiagnosticsC.html#afd39087e9811bdb75d6401b7a47cd5eb", null ]
    ] ],
    [ "MultiviewTriangulationDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1MultiviewTriangulationDiagnosticsC.html", [
      [ "MultiviewTriangulationDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1MultiviewTriangulationDiagnosticsC.html#af6d9f6ab8224f9fa2008e5880ce62d3b", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1MultiviewTriangulationDiagnosticsC.html#a9401736fabfb4fcad64e3b71b41f1d93", null ],
      [ "nFiltered3D", "structSeeSawN_1_1GeometryN_1_1MultiviewTriangulationDiagnosticsC.html#aa1f9333d9d8952082f6ac32065a08a5d", null ],
      [ "nMeasurements", "structSeeSawN_1_1GeometryN_1_1MultiviewTriangulationDiagnosticsC.html#a7eacc541e6597b8db75dc8352565a6a3", null ],
      [ "nOriginal3D", "structSeeSawN_1_1GeometryN_1_1MultiviewTriangulationDiagnosticsC.html#ac3173aa1a13312819d5d654e20be189b", null ]
    ] ],
    [ "CalibrationVerificationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineDiagnosticsC.html", [
      [ "CalibrationVerificationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineDiagnosticsC.html#aeb33cea27cabe2c8d1ec6b335cec5658", null ],
      [ "bestScore", "structSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineDiagnosticsC.html#accf7f3fd4e6ae0ae166e62a5e959c573", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineDiagnosticsC.html#a17acceef0c966c93d1f6e43fbbcb20d7", null ],
      [ "nConsistent", "structSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineDiagnosticsC.html#a7f61b4ca1ca0d7c9a180b948f9770d8f", null ],
      [ "nPairs", "structSeeSawN_1_1GeometryN_1_1CalibrationVerificationPipelineDiagnosticsC.html#a7d721d9d3afe8e47cd74d536dfeb89bd", null ]
    ] ],
    [ "GeometryEstimationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html", [
      [ "GeometryEstimationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a027864f59f4b3f7d4a726e552d0e39a4", null ],
      [ "error", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#ab4a47f857f65c7beaf58b98c2d80ad87", null ],
      [ "featureMatcherDiagnostics", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aed001748badbaee7298e401574f74835", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a43e7e9faf25316f25eddf960fcf6b772", null ],
      [ "generator", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a63aed840ab8362cda63c6a7bab7d6c9c", null ],
      [ "geometryEstimatorDiagnostics", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#a6e4cfbb38f4d3958dca689ee16c25265", null ],
      [ "initialObservationSet", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aced32348df5bba8cd2801ace42c08081", null ],
      [ "meanError", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aebe132400c094e72f84280e7b721b519", null ],
      [ "nIteration", "structSeeSawN_1_1GeometryN_1_1GeometryEstimationPipelineDiagnosticsC.html#aae172b360892662290364ad835b46148", null ]
    ] ],
    [ "GeometryEstimatorDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html", [
      [ "GeometryEstimatorDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#ada37a4d7dfa86556559da73a88dd7953", null ],
      [ "error", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#ad76fbda1424425bc3a9bde47493cc1ab", null ],
      [ "flagLO", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#a175bf0ec6774c2e2ee061774882967e1", null ],
      [ "flagStage2", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#a8f32bc2b91737158c2a61ff3323a9213", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#af717685ceb8fa1e8c7d01e3d02355311", null ],
      [ "pdlDiagnostics", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#a67f32673393efc1ebf336f70492318d1", null ],
      [ "ransacDiagnostics", "structSeeSawN_1_1GeometryN_1_1GeometryEstimatorDiagnosticsC.html#a3bae5544afd51185250e27d01886b3e0", null ]
    ] ],
    [ "LinearAutocalibrationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1LinearAutocalibrationPipelineDiagnosticsC.html", [
      [ "LinearAutocalibrationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1LinearAutocalibrationPipelineDiagnosticsC.html#a011c281b14381ae9651e46f1c14b62c9", null ],
      [ "error", "structSeeSawN_1_1GeometryN_1_1LinearAutocalibrationPipelineDiagnosticsC.html#a1dc9f52bb6feadf7b32efec0aee214f1", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1LinearAutocalibrationPipelineDiagnosticsC.html#ad20d2aab8d55d688074454195d689f9c", null ],
      [ "ransacDiagnostics", "structSeeSawN_1_1GeometryN_1_1LinearAutocalibrationPipelineDiagnosticsC.html#a30562224d6aa155aa7a338e2fd3644c2", null ]
    ] ],
    [ "MultimodelGeometryEstimationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html", [
      [ "MultimodelGeometryEstimationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#a92695278a735e424931aa5c223da2d9c", null ],
      [ "error", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#a46d984137c90833b18b591668342f73d", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#a7950f49211c23c9b78f169801a0b2b6f", null ],
      [ "geometryEstimatorDiagnostics", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#a22f7e99b690d327f7a841dd16de535de", null ],
      [ "matcherDiagnostics", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#ad4b2861c7c8129c08dd7e77c0c57e09b", null ],
      [ "ransacDiagnostics", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#ab1d4d90a16136555eddc6e90668750bd", null ],
      [ "support", "structSeeSawN_1_1GeometryN_1_1MultimodelGeometryEstimationPipelineDiagnosticsC.html#adc12f069c2b142720b578b88899c7047", null ]
    ] ],
    [ "NodalCameraTrackingPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html", [
      [ "NodalCameraTrackingPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#a874b73a8fccd075e8a390b2aec5e536e", null ],
      [ "deltaSupport", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#acb07c6a8a60241cce2b30e9b2cefde30", null ],
      [ "flagNodal", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#aeb3e1d608796d660817ca4beb02c39da", null ],
      [ "flagRelocalise", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#a0ce6c2b4745aa8562886d7aede0da1fc", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#a944129abb49076cebde81ab6d076ed0b", null ],
      [ "geDiagnostics", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#a8f494c3dc1a169882b232b330203146a", null ],
      [ "nInliers", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#a245ea4e8dc3567244ebc9f9b92d4e8bf", null ],
      [ "nRegistration", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#a88780e7604409f9924057082e9a8c1f9", null ],
      [ "supportSet", "structSeeSawN_1_1GeometryN_1_1NodalCameraTrackingPipelineDiagnosticsC.html#ac258f69f9ff3d16bc092ea671d47858a", null ]
    ] ],
    [ "PfMPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html", [
      [ "PfMPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a9daa213b74e7e588446f9586992624dd", null ],
      [ "estimatedNoiseVariance", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a85e36b14c0edbf303fbe3499397644a6", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a7067e5556cd21875d02e4710adf20239", null ],
      [ "geometryEstimatorDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#adf912cfd6da9e90b5982d4d096422177", null ],
      [ "nInlierEdge", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a90f6737c678e7be893e23e8f778f1fbc", null ],
      [ "nRegistered", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a5d153694b8220fc35d552ef961eb8db8", null ],
      [ "panoramaDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a0225288e1100608cccad43473126dfe6", null ],
      [ "rotationRegistrationDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a3df0578b9d2c858cd54539165d708556", null ],
      [ "sPointCloud", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a6ef877d4bcb29ed7ccd0b8e18202c4e9", null ],
      [ "visionGraphDiagnostics", "structSeeSawN_1_1GeometryN_1_1PfMPipelineDiagnosticsC.html#a80c78dc335fc3b8b37f8fd8717e8fc63", null ]
    ] ],
    [ "PoseGraphPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineDiagnosticsC.html", [
      [ "PoseGraphPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineDiagnosticsC.html#ad2a93fbc65f78aca033f9b4679dcf83f", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1PoseGraphPipelineDiagnosticsC.html#a495a451beb10f702b369323b90857b3d", null ]
    ] ],
    [ "RoamingCameraTrackingPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html", [
      [ "flagPose", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#af1864d9a2d674e391a100e005932294f", null ],
      [ "flagRelocalise", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#ac4104565c270672bcb538155a9fe4e0b", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a7481b569f2bb41c77686a3ed66069af4", null ],
      [ "geDiagnostics", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a2bdb2639296509efc0917cc475df2280", null ],
      [ "nInliers", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a767746f29b4227408d805f1938b465f9", null ],
      [ "nRegistration", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#a09b770874ded1ec603f987400ac25bc5", null ],
      [ "supportSet", "structSeeSawN_1_1GeometryN_1_1RoamingCameraTrackingPipelineDiagnosticsC.html#aa088a4d920bc34e1d6bed2ad554185d0", null ]
    ] ],
    [ "RotationRegistrationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html", [
      [ "RotationRegistrationPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#acf372bae0a322eb44b31dfac065d02f6", null ],
      [ "error", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#ab0bd1a5dbe46e4c361d8efcb9b3af9cc", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#a48c146159ed6070752d42324efe0397a", null ],
      [ "flagSuccessLS", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#ae80e8d80e0ad5bdd3777b7ceb2c3711a", null ],
      [ "inlierRatio", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#a24fd016d701624b2b037d5a600d81181", null ],
      [ "mstDiagnostics", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#ac7e4fc5d8c98735db93c33ad00ac8f61", null ],
      [ "rstsDiagnostics", "structSeeSawN_1_1GeometryN_1_1RotationRegistrationPipelineDiagnosticsC.html#a96e085be24e27b07a0ff2b17b7bd822b", null ]
    ] ],
    [ "SfMPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1SfMPipelineDiagnosticsC.html", [
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1SfMPipelineDiagnosticsC.html#af56485143e643d66bc3148e0509a01ea", null ]
    ] ],
    [ "Sparse3DReconstructionPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1Sparse3DReconstructionPipelineDiagnosticsC.html", [
      [ "Sparse3DReconstructionPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1Sparse3DReconstructionPipelineDiagnosticsC.html#a89e0b5cc94a95cd8a09d68486f64b3bb", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1Sparse3DReconstructionPipelineDiagnosticsC.html#af1a90f4e34bad55eeb61a2b3285630c7", null ],
      [ "matcherDiagnostics", "structSeeSawN_1_1GeometryN_1_1Sparse3DReconstructionPipelineDiagnosticsC.html#ad8b2b14c2d67875e33481b2fc40555f0", null ],
      [ "triangulationDiagnositics", "structSeeSawN_1_1GeometryN_1_1Sparse3DReconstructionPipelineDiagnosticsC.html#a1286414cb6dbf4e3b3e11ef8a476f40b", null ]
    ] ],
    [ "SparseUnitSphereReconstructionPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineDiagnosticsC.html", [
      [ "SparseUnitSphereReconstructionPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineDiagnosticsC.html#a212c206e8cad74168a59df6332abc83c", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineDiagnosticsC.html#a485aec8654e86d64041eb4d4cd9bd346", null ],
      [ "matcherDiagnostics", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineDiagnosticsC.html#a33c34bc5521f325c3686a9aadc6b514d", null ],
      [ "panoramaDiagnostics", "structSeeSawN_1_1GeometryN_1_1SparseUnitSphereReconstructionPipelineDiagnosticsC.html#ade81e2dfcc180bfc845e950df04bae7f", null ]
    ] ],
    [ "VisionGraphPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1VisionGraphPipelineDiagnosticsC.html", [
      [ "VisionGraphPipelineDiagnosticsC", "structSeeSawN_1_1GeometryN_1_1VisionGraphPipelineDiagnosticsC.html#afb9f8fcf570e261f19476b205700a5e2", null ],
      [ "flagSuccess", "structSeeSawN_1_1GeometryN_1_1VisionGraphPipelineDiagnosticsC.html#a632b75e908bb2072d4d68c64686923b9", null ]
    ] ],
    [ "FeatureMatcherDiagnosticsC", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherDiagnosticsC.html", [
      [ "bfDiagnostics", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherDiagnosticsC.html#a1d1aa1fb990cd260a9529c46fd8cdbc0", null ],
      [ "flagSuccess", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherDiagnosticsC.html#aface3929f3858615175edf6183335155", null ],
      [ "nnDiagnostics", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherDiagnosticsC.html#a307861b833c0680909d28ad7bad0aa9d", null ],
      [ "sCorrespondences", "structSeeSawN_1_1MatcherN_1_1FeatureMatcherDiagnosticsC.html#ab3ffa6cafa168a68a8ed7f9ae8b423c2", null ]
    ] ],
    [ "MultisourceFeatureMatcherDiagnosticsC", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html", [
      [ "MultisourceFeatureMatcherDiagnosticsC", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#ac1dc3bee4181a42df4cbd2421ec135bc", null ],
      [ "flagSuccess", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#a20d68ebe4d72611bac9589e64f9d4c58", null ],
      [ "matcherDiagnostics", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#ad8c0b77d20eb0c90ea9eeda5bc3826be", null ],
      [ "nClusters", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#a4bb4012e029745bcd0e42c4f0e7746cc", null ],
      [ "nImplied", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#a1d6678c8f612a7187c6cdc0211186ee7", null ],
      [ "nObserved", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#abae7ffb609dd0d6f0c6814a6363bee07", null ],
      [ "nSourcePairs", "structSeeSawN_1_1MatcherN_1_1MultisourceFeatureMatcherDiagnosticsC.html#ac5b282a05d33ffc8f4870409887fa2f2", null ]
    ] ],
    [ "NearestNeighbourMatcherDiagnosticsC", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherDiagnosticsC.html", [
      [ "NearestNeighbourMatcherDiagnosticsC", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherDiagnosticsC.html#a18871ebd7255542556ae7fa52a787cbd", null ],
      [ "flagSuccess", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherDiagnosticsC.html#aec0eea372911bd8f5f5d63193e9f6516", null ],
      [ "sCorrespondences", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherDiagnosticsC.html#a4915cbf4fada92d260174d66874b214f", null ],
      [ "sSet1", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherDiagnosticsC.html#a8d60eb6a315debcfe004c07babfc42eb", null ],
      [ "sSet2", "structSeeSawN_1_1MatcherN_1_1NearestNeighbourMatcherDiagnosticsC.html#a190f853fc069da15affb381b3e62936d", null ]
    ] ],
    [ "NumericalJacobianDiagnosticsC", "structSeeSawN_1_1NumericN_1_1NumericalJacobianDiagnosticsC.html", [
      [ "flagSuccess", "structSeeSawN_1_1NumericN_1_1NumericalJacobianDiagnosticsC.html#a5a302b950caca7c07e88ef3c2fcc89eb", null ],
      [ "perturbations", "structSeeSawN_1_1NumericN_1_1NumericalJacobianDiagnosticsC.html#a8dcb20c9baf2b460dcaddb4d9ed8caca", null ]
    ] ],
    [ "PowellDogLegDiagnosticsC", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html", [
      [ "PowellDogLegDiagnosticsC", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#acfc8db9b38c5acd36577be84731bc04b", null ],
      [ "error", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a22a737d2871966bc294d602053ef0f1d", null ],
      [ "errorHistory", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a6209c8d398b08187c1c7e3cbe416cb1b", null ],
      [ "finalDelta", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a6c9277d4dec0521ed5b248370563718e", null ],
      [ "finalEpsilon1", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a6d392e7ec4b4b1cdd0b3cb20f72c62f3", null ],
      [ "finalEpsilon2", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a22f220e1b4eb425ac1e4d17dea6f3541", null ],
      [ "flagSuccess", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#a10444651e6eec142d0ed42b9be8a0dad", null ],
      [ "nIterations", "structSeeSawN_1_1OptimisationN_1_1PowellDogLegDiagnosticsC.html#adf067c938bffae5b2e9933d96036fef1", null ]
    ] ],
    [ "RandomSpanningTreeSamplerDiagnosticsC", "structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html", [
      [ "RandomSpanningTreeSamplerDiagnosticsC", "structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html#ad433eb86a5e99fa4a4f01f120b3977c4", null ],
      [ "error", "structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html#a4a745904004357bbd91fa81d0b500817", null ],
      [ "flagSuccess", "structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html#a17cc1c23fae7b501edc9cf7e27aa5477", null ],
      [ "inlierRatio", "structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html#a4f6c97748d4e910bf87a85a5b212b7ab", null ],
      [ "nIteration", "structSeeSawN_1_1RandomSpanningTreeSamplerN_1_1RandomSpanningTreeSamplerDiagnosticsC.html#a469fba64899ee8f999ef8d50c8c37820", null ]
    ] ],
    [ "RANSACDiagnosticsC", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html", [
      [ "IterationT", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ab9429ca93a0170f414d12069c92d3f7e", null ],
      [ "RANSACDiagnosticsC", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#a000eec7f3791a7e3cdfafbd40001eb07", null ],
      [ "error", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#a387e73d978996656254b38ff6744fba2", null ],
      [ "finalDecisionTh", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#a11729743905f190dcfc6476cc670d801", null ],
      [ "finalDelta", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#af484affebe6c76dcbb99169050de30b3", null ],
      [ "finalEpsilonOb", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ab32c5b06b965c6e7c80677c107e41527", null ],
      [ "finalEpsilonVl", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#a8f746c79688d1f03a934b0d07f42d995", null ],
      [ "flagSuccess", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ad6e894828d641de1f47bcfc43dcbf020", null ],
      [ "history", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ad49425453bd6693b6179f7843704cd83", null ],
      [ "iError", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ad04c4d35b1700d206237c980990a9567", null ],
      [ "iEta", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#a876cd23ca14edb83d53dd6f3db6a6d15", null ],
      [ "iGenerator", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ae944499b16a6de116a2aa6366de86f94", null ],
      [ "iIteration", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ae106e13c855009257dd2bc6d6fcbdef0", null ],
      [ "inlierRatio", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#aabeb41e9521b14f8031b5fa5c9c822b8", null ],
      [ "nIterations", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#a7d9e29ea01332ba92829b893f30c8225", null ],
      [ "nLOCalls", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ae72ba0b5b6647c5a8063c2a27c63ff5f", null ],
      [ "nValidatorCalls", "structSeeSawN_1_1RANSACN_1_1RANSACDiagnosticsC.html#ac47e4969d78cf78f6ecffd30bed70f9f", null ]
    ] ],
    [ "SequentialRANSACDiagnosticsC", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACDiagnosticsC.html", [
      [ "SequentialRANSACDiagnosticsC", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACDiagnosticsC.html#ae8a00993ad985b724e8311b8d8521841", null ],
      [ "flagSuccess", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACDiagnosticsC.html#ab3d97390f59acda7f9f291de51ce33a0", null ],
      [ "ransacDiagnostics", "structSeeSawN_1_1RANSACN_1_1SequentialRANSACDiagnosticsC.html#a760c640adbec996d01a99f52fcb76878", null ]
    ] ],
    [ "TwoStageRANSACDiagnosticsC", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACDiagnosticsC.html", [
      [ "TwoStageRANSACDiagnosticsC", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACDiagnosticsC.html#a3cafeb917208eebf721a6c56e892c76c", null ],
      [ "diagnostics1", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACDiagnosticsC.html#aa6e712053886795017f7436a65e65b03", null ],
      [ "diagnostics2", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACDiagnosticsC.html#a8fde17d8ff5c254c40a4efc094e3dbba", null ],
      [ "flagSuccess", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACDiagnosticsC.html#a09a6fce36168ede6356dc28bfebd371f", null ],
      [ "iStage1Solution", "structSeeSawN_1_1RANSACN_1_1TwoStageRANSACDiagnosticsC.html#a9e69814a2dfebd79f20d34cd1f8d02bf", null ]
    ] ],
    [ "MulticameraSynchonisationDiagnosticsC", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationDiagnosticsC.html", [
      [ "MulticameraSynchonisationDiagnosticsC", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationDiagnosticsC.html#a5db99f55a5e88a38c44f1cc73e4a1c17", null ],
      [ "flagSuccess", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationDiagnosticsC.html#ac9980cd3acd84222fd6c863475ee249f", null ],
      [ "score", "structSeeSawN_1_1SynchronisationN_1_1MulticameraSynchonisationDiagnosticsC.html#a2e9834151d627796518c285933f68df2", null ]
    ] ],
    [ "RelativeSynchronisationDiagnosticsC", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationDiagnosticsC.html", [
      [ "RelativeSynchronisationDiagnosticsC", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationDiagnosticsC.html#a113bf1f4ea183930ff0f971c1ed05900", null ],
      [ "flagSuccess", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationDiagnosticsC.html#a547cd90817b5cb9007d7b399236d1971", null ],
      [ "score", "structSeeSawN_1_1SynchronisationN_1_1RelativeSynchronisationDiagnosticsC.html#a12a9a470fa1097db37207c86efbd5aef", null ]
    ] ],
    [ "FeatureTrackerDiagnosticsC", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html", [
      [ "associations", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html#a149dfceaa88d399a3312968cc8f67156", null ],
      [ "flagSuccess", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html#a2a4460aa0525ed7d7495378b0f0758fa", null ],
      [ "fmDiagnostics", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html#a0fc36924ba5e16ae0037dd2410c88665", null ],
      [ "nActiveTracks", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html#aed710bc76f00f8b98e59d0ee8f469bbc", null ],
      [ "nInactiveTracks", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html#a73b45283961ce32510f1e7b227361fff", null ],
      [ "removed", "structSeeSawN_1_1TrackerN_1_1FeatureTrackerDiagnosticsC.html#a30381fa806b52ab2027c5e55dac91dab", null ]
    ] ],
    [ "ImageFeatureTrackingPipelineDiagnosticsC", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineDiagnosticsC.html", [
      [ "flagSuccess", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineDiagnosticsC.html#ace4fab2ca7ea333ab5188991d48f97c7", null ],
      [ "homographyDiagnostics", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineDiagnosticsC.html#afe66d2b4e4c60cd3d94f3d3c9e67da79", null ],
      [ "matcherDiagnostics", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineDiagnosticsC.html#a48f9881ec334e8b3b7687ac067e979c8", null ],
      [ "trackerDiagnostics", "structSeeSawN_1_1TrackerN_1_1ImageFeatureTrackingPipelineDiagnosticsC.html#a559802d6536bea5bf38c573c205f5842", null ]
    ] ]
];