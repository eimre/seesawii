/**
 * @file LinearAutocalibrationPipeline.cpp Implementation of \c LinearAutocalibrationPipelineC
 * @author Evren Imre
 * @date 7 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "LinearAutocalibrationPipeline.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the parameters
 * @param[in] initialEstimateList Initial estimates
 * @param[in] parameters Parameters
 * @param[in] nCameras Number of cameras
 * @throws invalid_argument If any parameter has an invalid value
 */
void LinearAutocalibrationPipelineC::ValidateParameters(const vector<IntrinsicC>& initialEstimateList, LinearAutocalibrationPipelineParametersC& parameters, size_t nCameras)
{
	if(parameters.sGenerator < DualAbsoluteQuadricSolverC::GeneratorSize())
		throw(invalid_argument(string("LinearAutocalibrationPipelineC::ValidateParameters : parameters.sGenerator must be >=" ) + lexical_cast<string>(DualAbsoluteQuadricSolverC::GeneratorSize()) + string(". Value=") + lexical_cast<string>(parameters.sGenerator)) );

	if(parameters.sLOGenerator < DualAbsoluteQuadricSolverC::GeneratorSize())
		throw(invalid_argument(string("LinearAutocalibrationPipelineC::ValidateParameters : parameters.sLOGenerator must be >=" ) + lexical_cast<string>(DualAbsoluteQuadricSolverC::GeneratorSize()) + string(". Value=") + lexical_cast<string>(parameters.sLOGenerator)) );

	if(parameters.nLACIteration==0)
		throw(invalid_argument(string("LinearAutocalibrationPipelineC::ValidateParameters : parameters.nLACIteration must be positive. Value=") + lexical_cast<string>(parameters.nLACIteration)) );

	if(parameters.inlierTh<=0)
		throw(invalid_argument(string("LinearAutocalibrationPipelineC::ValidateParameters : parameters.inlierTh must be positive. Value=") + lexical_cast<string>(parameters.inlierTh)) );

	//Verifying the initial estimates
	size_t nInitialEstimate=initialEstimateList.size();
	if(nInitialEstimate!=1 && nInitialEstimate!=nCameras)
		throw(invalid_argument(string("LinearAutocalibrationPipelineC::ValidateParameters : initialEstimateList must have 1 or ")+lexical_cast<string>(nCameras)+string(" elements. Value=")+lexical_cast<string>(nInitialEstimate) ) );

	parameters.ransacParameters.flagPROSAC=false;	//No PROSAC
	parameters.ransacParameters.maxSolution=1;	//No MORANSAC
	parameters.ransacParameters.flagHistory=false;
}	//void ValidateParameters(const LinearAutocalibrationPipelineParametersC& parameters)


/**
 * @brief Computes the normaliser corresponding to the initial estimate
 * @param[in] initialEstimate Initial estimate
 * @param[in] mP Camera matrix
 * @return Normaliser
 */
IntrinsicCalibrationMatrixT LinearAutocalibrationPipelineC::ComputeNormaliser(const IntrinsicC& initialEstimate, const CameraMatrixT& mP)
{
	IntrinsicCalibrationMatrixT mK;

	bool flagFocal=!!initialEstimate.focalLength;
	bool flagPrincipal=!!initialEstimate.principalPoint;

	//Decompose the camera matrix to recover the missing intrinsics
	if(!flagFocal || !flagPrincipal)
	{
		RotationMatrix3DT mR;
		Coordinate3DT vC;
		tie(mK, vC, mR)=DecomposeCamera(mP, false);
	}	//if(!flagFocal || !flagPrincipal)
	else
		mK=IntrinsicCalibrationMatrixT::Zero();

	mK(0,0) = flagFocal ? *initialEstimate.focalLength : 0.5*(mK(0,0)+mK(1,1));
	mK(1,1) = initialEstimate.aspectRatio * mK(0,0);
	mK(0,2) = flagPrincipal ? (*initialEstimate.principalPoint)[0] : mK(0,2);
	mK(1,2) = flagPrincipal ? (*initialEstimate.principalPoint)[1] : mK(1,2);
	mK(0,1) = initialEstimate.skewness;
	mK(2,2) = 1;

	return mK.inverse();
}	//IntrinsicCalibrationMatrixT ComputeNormaliser(const IntrinsicC& initialEstimate)

/**
 * @brief Normalises the camera matrices
 * @tparam CameraMatrixRangeT A range of camera matrices
 * @param[in] cameraList Cameras to be normalised
 * @param[in] initialEstimateList Initial estimates for the calibration parameters
 * @return A tuple: normalised camera matrices; normalisers
 */
tuple<vector<CameraMatrixT>, vector<IntrinsicCalibrationMatrixT> > LinearAutocalibrationPipelineC::NormaliseCameras(const vector<CameraMatrixT>& cameraList, const vector<IntrinsicC>& initialEstimateList)
{
	//initialEstimateList is already validated

	bool flagUniform = (initialEstimateList.size()==1) && (initialEstimateList[0].focalLength) && (initialEstimateList[0].principalPoint);	//Uniform normaliser?
	optional<IntrinsicCalibrationMatrixT> mN;

	size_t nCameras=cameraList.size();

	vector<CameraMatrixT> normalised(nCameras);
	vector<IntrinsicCalibrationMatrixT> normalisers(nCameras);

	for(size_t c=0; c<nCameras; ++c)
	{
		if(!flagUniform || !mN)
			mN=ComputeNormaliser(initialEstimateList[c], cameraList[c]);

		normalised[c]= (*mN) *cameraList[c];
		normalisers[c]= *mN;
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(move(normalised), move(normalisers));
}	//vector<CameraMatrixT> NormaliseCameras(const CameraMatrixRangeT& cameraList, const vector<IntrinsicC>& initialEstimateList)

/**
 * @brief Evaluates a solution
 * @param[in] mQ DAQ to be evaluated
 * @param[in] cameraList Cameras
 * @param[in] inlierTh
 * @return Error
 */
double LinearAutocalibrationPipelineC::Evaluate(const Quadric3DT& mQ, const vector<CameraMatrixT>& cameraList, double inlierTh)
{
	double error=0;
	for(const auto& current : cameraList)
		error += min(inlierTh, DualAbsoluteQuadricSolverC::Evaluate(mQ, current));

	return error;
}	//double Evaluate(const Homography3DT& mH, const vector<CameraMatrixT>& cameraList)

/**
 * @brief Imposes hard calibration constraints
 * @param[in] mP Camera matrix
 * @param constraints Constraints. Any valid constraints are substituted into the intrinsic calibration matrix
 * @return Modified camera matrix
 */
CameraMatrixT LinearAutocalibrationPipelineC::ImposeConstraints(const CameraMatrixT& mP, const IntrinsicC& constraints)
{
	IntrinsicCalibrationMatrixT mK;
	RotationMatrix3DT mR;
	Coordinate3DT vC;
	tie(mK, vC, mR)=DecomposeCamera(mP, false);

	mK=ImposeConstraints(mK, constraints);

	return ComposeCameraMatrix(mK, vC, mR);
}	//CameraMatrixT ImposeConstraints(const CameraMatrixT& mP, const IntrinsicC& constraints)

/**
 * @brief Imposes hard calibration constraints
 * @param[in] mK Intrinsic calibration matrix
 * @param constraints Constraints. Any valid constraints are substituted into the intrinsic calibration matrix
 * @return Modified camera matrix
 */
IntrinsicCalibrationMatrixT LinearAutocalibrationPipelineC::ImposeConstraints(const IntrinsicCalibrationMatrixT& mK, const IntrinsicC& constraints)
{
	IntrinsicCalibrationMatrixT output=IntrinsicCalibrationMatrixT::Identity();

	output(0,0) = constraints.focalLength ? *constraints.focalLength : mK(0,0);
	output(1,1) = constraints.aspectRatio * mK(0,0);
	output(0,1) = constraints.skewness;
	output(0,2) = constraints.principalPoint ? (*constraints.principalPoint)[0] : mK(0,2);
	output(1,2) = constraints.principalPoint ? (*constraints.principalPoint)[1] : mK(1,2);

	return output;
}	//CameraMatrixT ImposeConstraints(const CameraMatrixT& mP, const IntrinsicC& constraints)

/********** EXPLICIT INSTANTIATIONS **********/
template LinearAutocalibrationPipelineDiagnosticsC LinearAutocalibrationPipelineC::Run(Homography3DT&, map<size_t, IntrinsicCalibrationMatrixT>&, mt19937_64&, const vector<CameraMatrixT>&, const vector<IntrinsicC>&, LinearAutocalibrationPipelineParametersC);
}	//GeometryN
}	//SeeSawN

