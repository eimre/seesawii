/**
 * @file GeometryEstimationPipelineProblemConcept.ipp Geometry estimation problem concept checker
 * @author Evren Imre
 * @date 17 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_CONCEPT_IPP_9871233
#define GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_CONCEPT_IPP_9871233

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <vector>
#include "../Elements/Correspondence.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Geometry/GeometrySolverConcept.h"
#include "../RANSAC/RANSACGeometryEstimationProblemConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::CopyConstructible;
using boost::Assignable;
using boost::optional;
using Eigen::Matrix;
using std::vector;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemConceptC;

/**
 * @brief Checker for the geometry estimation pipeline problem concept
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class GeometryEstimationPipelineProblemConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<typename TestT::matching_problem_type>));
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<typename TestT::geometry_estimation_problem_type::ransac_problem_type2>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename TestT::geometry_estimation_problem_type::ransac_problem_type2::minimal_solver_type>));

	public:

		BOOST_CONCEPT_USAGE(GeometryEstimationPipelineProblemConceptC)
		{
			typedef typename TestT::matching_problem_type MatchingProblemT;
			typedef typename TestT::geometry_estimation_problem_type GeometryEstimationProblemT;
			typedef typename TestT::covariance_type1 CovarianceMatrix1T;
			typedef typename TestT::covariance_type2 CovarianceMatrix2T;

			TestT tested;

			MatchingProblemT* pMatchingProblem; (void)pMatchingProblem;
			GeometryEstimationProblemT* pGeometryEstimationProblem; (void)pGeometryEstimationProblem;

			bool flagValid=tested.IsValid(); (void)flagValid;

			typename MatchingProblemT::similarity_type similarityMetric=tested.SimilarityMetric(); (void)similarityMetric;
			optional<typename MatchingProblemT::constraint_type> initialConstraint=tested.InitialMatchingConstraint(); (void)initialConstraint;
			optional<typename MatchingProblemT::constraint_type> constraint=tested.MatchingConstraint(); (void)constraint;
			vector<typename MatchingProblemT::feature_type1> featureSet1=tested.FeatureSet1(); (void)featureSet1;
			vector<typename MatchingProblemT::feature_type2> featureSet2=tested.FeatureSet2(); (void)featureSet2;

			boost::empty(tested.FeatureSet1());
			boost::empty(tested.FeatureSet2());

			typedef typename GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type GeometrySolverT;
			CoordinateCorrespondenceListT<typename GeometrySolverT::coordinate_type1, typename GeometrySolverT::coordinate_type2> ccList;
			tested.UpdateGeometryEstimationProblem( optional<typename GeometrySolverT::model_type>(), ccList, ccList);
			tested.UpdateConstraint(typename GeometrySolverT::model_type());

			unsigned int sGenerator=TestT::GetMinimalGeneratorSize(); (void)sGenerator;

			CovarianceMatrix1T m1; (void)m1;
			CovarianceMatrix2T m2; (void)m2;
			Matrix<typename GeometrySolverT::real_type, Eigen::Dynamic, Eigen::Dynamic> mGen=tested.MakeGeneratorCovariance(CorrespondenceListT());
		}	//BOOST_CONCEPT_USAGE(GeometryEstimationPipelineProblemConceptC)

	///@endcond
};	//class GeometryEstimationProblemConceptC

}	//GeometryN
}	//SeeSawN
#endif /* GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_CONCEPT_IPP_9871233 */
