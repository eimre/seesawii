/**
 * @file PoseGraphPipeline.h Public interface for the pose graph pipeline
 * @author Evren Imre
 * @date 8 Nov 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef POSE_GRAPH_PIPELINE_H_6689092
#define POSE_GRAPH_PIPELINE_H_6689092

#include "PoseGraphPipeline.ipp"

namespace SeeSawN
{
namespace GeometryN
{

struct PoseGraphPipelineParametersC;	///< Parameters for the pose graph pipeline
struct PoseGraphPipelineDiagnosticsC;	///< Diagnostics for the pose graph pipeline

template<class ProblemT> class PoseGraphPipelineC;	///< Pipeline for building a pose graph for a set of point clouds

}	//GeometryN
}	//SeeSawN


#endif /* POSE_GRAPH_PIPELINE_H_6689092 */
