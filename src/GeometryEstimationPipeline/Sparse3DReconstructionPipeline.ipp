/**
 * @file Sparse3DReconstructionPipeline.ipp Implementation of the sparse 3D reconstruction pipeline
 * @author Evren Imre
 * @date 16 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SPARSE_3D_RECONSTRUCTION_PIPELINE_IPP_9210843
#define SPARSE_3D_RECONSTRUCTION_PIPELINE_IPP_9210843

#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm.hpp>
#include <vector>
#include <map>
#include <tuple>
#include <cstddef>
#include <stdexcept>
#include <string>
#include <iostream>
#include "../Elements/Camera.h"
#include "../Elements/Feature.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/FeatureUtility.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Matcher/MultisourceFeatureMatcher.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Geometry/MultiviewTriangulation.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::lexical_cast;
using boost::for_each;
using std::vector;
using std::map;
using std::tuple;
using std::tie;
using std::size_t;
using std::string;
using std::cout;
using std::invalid_argument;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::MatcherN::MultisourceFeatureMatcherC;
using SeeSawN::MatcherN::MultisourceFeatureMatcherParametersC;
using SeeSawN::MatcherN::MultisourceFeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::MatcherN::BinaryImageFeatureMatchingProblemC;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::GeometryN::MultiviewTriangulationC;
using SeeSawN::GeometryN::MultiviewTriangulationParametersC;
using SeeSawN::GeometryN::MultiviewTriangulationDiagnosticsC;

/**
 * @brief Parameters for \c Sparse3DReconstructionPipelineC
 * @ingroup Parameters
 */
struct Sparse3DReconstructionPipelineParametersC
{
	double noiseVariance=4;	///< Variance of the noise on the image coordinates. >0
	double pRejection=0.05;	///< Probability of rejection for an inlier correspondence.(0,1)
	bool flagVerbose=false;	///< Verbose output
	bool flagCompact=false;	///< If \c true the scene feature vectors are compressed to a single most representative descriptor.

	MultisourceFeatureMatcherParametersC matcherParameters;	///< Parameters for the feature matcher
	MultiviewTriangulationParametersC triangulationParameters;	///< Parameters for triangulation
};	//struct Sparse3DReconstructionPipelineParametersC

/**
 * @brief Diagnostics for \c Sparse3DReconstructionPipelineC
 * @ingroup Diagnostics
 */
struct Sparse3DReconstructionPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully

	MultisourceFeatureMatcherDiagnosticsC matcherDiagnostics;	///< Diagnostics for the feature matcher
	MultiviewTriangulationDiagnosticsC triangulationDiagnositics;	///< Diagnostics for triangulation

	Sparse3DReconstructionPipelineDiagnosticsC() : flagSuccess(false)
	{}
};	//struct Sparse3DReconstructionPipelineDiagnosticsC

/**
 * @brief Pipeline for sparse 3D reconstruction from images taken by calibrated cameras
 * @ingroup Algorithm
 * @nosubgrouping
 */
class Sparse3DReconstructionPipelineC
{
	private:

		typedef tuple<size_t, size_t> SizePairT;	///< A pair of size_t variables
		typedef MultiviewTriangulationC::covariance_matrix_type CovarianceMatrixT;	///< A covariance matrix

		/** @name Implementation Details */ //@{
		static void ValidateParameters(Sparse3DReconstructionPipelineParametersC& parameters);	///< Verifies the validity of the parameters

		static map<SizePairT, EpipolarSampsonConstraintT> MakePairwiseConstraints(const vector<CameraMatrixT>& cameras, const Sparse3DReconstructionPipelineParametersC& parameters);	///< Initialises the pairwise epipolar constraints

		typedef MultisourceFeatureMatcherC::unified_view_type UnifiedViewT;
		typedef MultisourceFeatureMatcherC::pairwise_view_type PairwiseViewT;

		template<class SceneFeatureT, class ImageFeatureT> static vector<SceneFeatureT> MakeSceneFeature(const vector<Coordinate3DT>& reconstruction, const PairwiseViewT& correspondences, const vector<vector<ImageFeatureT> >& features);	///< Attaches descriptors to the scene points

		static void PrintResult(const PairwiseViewT& observed, const PairwiseViewT& implied, size_t nCameras);	///< Prints the results
		template<class FeatureMatchingProblemT, class SceneFeatureT> static Sparse3DReconstructionPipelineDiagnosticsC Reconstruct(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<SceneFeatureT>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<typename FeatureMatchingProblemT::feature_type1> >& features, const typename FeatureMatchingProblemT::similarity_type & similarityMetric, Sparse3DReconstructionPipelineParametersC parameters);	///< Runs the reconstruction pipeline
		//@}

	public:

		template<class SimilarityT> static Sparse3DReconstructionPipelineDiagnosticsC Run(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<SceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<ImageFeatureC> >& features, const SimilarityT& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters);	///< Runs the algorithm
		template<class SimilarityT> static Sparse3DReconstructionPipelineDiagnosticsC Run(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const SimilarityT& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters);	///< Runs the algorithm
};	//class Sparse3DReconstructionPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Attaches descriptors to the scene points
 * @tparam SceneFeatureT A scene feature
 * @tparam ImageFeatureT An image feature
 * @param[in] reconstruction 3D reconstruction
 * @param[in] correspondences Image correspondences
 * @param[in] features Features
 * @return Scene features
 * @pre \c SceneFeatureT is a model of \c FeatureConceptC
 * @pre \c ImageFeatureT is a model of \c FeatureConceptC
 */
template<class SceneFeatureT, class ImageFeatureT>
vector<SceneFeatureT> Sparse3DReconstructionPipelineC::MakeSceneFeature(const vector<Coordinate3DT>& reconstruction, const PairwiseViewT& correspondences, const vector<vector<ImageFeatureT> >& features)
{
	//Preliminaries
	BOOST_CONCEPT_ASSERT((FeatureConceptC<SceneFeatureT>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<ImageFeatureT>));

	size_t nPoints=reconstruction.size();
	vector<SceneFeatureT> output(nPoints);

	//For each image pair
	for(const auto& currentPair : correspondences)
	{
		size_t i1;
		size_t i2;
		tie(i1, i2)=currentPair.first;

		const auto& pFeatureList1=features[i1];
		const auto& pFeatureList2=features[i2];

		//For each correspondence
		for(const auto& currentCorrespondence : currentPair.second)
		{
			size_t iPoint=currentCorrespondence.info;	//Id of the scene point

			//If no descriptor for the current view, add
			if(output[iPoint].RoI().find(i1)==output[iPoint].RoI().cend())
			{
				output[iPoint].Coordinate()=reconstruction[iPoint];
				output[iPoint].RoI().insert(i1);
				output[iPoint].Descriptor()[i1]=pFeatureList1[currentCorrespondence.left];
			}	//if(output.RoI().find(i1)==output.RoI().cend())

			if(output[iPoint].RoI().find(i2)==output[iPoint].RoI().cend())
			{
				output[iPoint].RoI().insert(i2);
				output[iPoint].Descriptor()[i2]=pFeatureList2[currentCorrespondence.right];
			}	//if(output.RoI().find(i1)==output.RoI().cend())

		}	//for(const auto& currentCorrespondence : currentPair)
	}	//for(const auto& currentPair : correspondences)

	return output;
}	//vector<SceneFeatureC> MakeSceneFeature(const vector<Coordinate3DT>& reconstruction, const PairwiseViewT& correspondences, const vector<vector<ImageFeatureC> >& features)

/**
 * @brief Runs the reconstruction pipeline
 * @tparam FeatureMatchingProblemT A feature matching problem
 * @tparam SceneFeatureT A scene feature type
 * @param[out] pointCloud 3D reconstruction
 * @param[out] covarianceList Covariance for each scene point
 * @param[out] sceneFeatures Scene features
 * @param[in] cameras Cameras
 * @param[in] features Image features
 * @param[in] similarityMetric Image feature similarity metric
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 * @pre \c features and \c cameras have the same number of elements
 * @pre \c SceneFeatureT is a model of \c FeatureConceptC
 * @pre \c FeatureMatchingProblemT is a model of \c FeatureMatchingProblemConceptC
 * @remarks Algorithm forces consistent and 1:1 matching
 * @remarks Algorithm assigns a suitable inlier threshold for triangulation, and employs \c parameters.noiseVariance as the image coordinate noise variance
 */
template<class FeatureMatchingProblemT, class SceneFeatureT>
Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Reconstruct(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<SceneFeatureT>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<typename FeatureMatchingProblemT::feature_type1> >& features, const typename FeatureMatchingProblemT::similarity_type& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<FeatureMatchingProblemT>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<SceneFeatureT>));

	assert(cameras.size()==features.size());

	ValidateParameters(parameters);

	Sparse3DReconstructionPipelineDiagnosticsC diagnostics;

	//Constraints
	map<SizePairT, EpipolarSampsonConstraintT> constraints=MakePairwiseConstraints(cameras, parameters);

	//Matching
	UnifiedViewT clusters;
	PairwiseViewT observedCorrespondences;
	PairwiseViewT impliedCorrespondences;
	diagnostics.matcherDiagnostics=MultisourceFeatureMatcherC::Run<FeatureMatchingProblemT>(clusters, observedCorrespondences, impliedCorrespondences, features, constraints, similarityMetric, parameters.matcherParameters);

	if(!diagnostics.matcherDiagnostics.flagSuccess)
		return diagnostics;

	PairwiseViewT correspondences=CorrespondenceFusionC::SplicePairwiseViews(observedCorrespondences, impliedCorrespondences);

	size_t nCameras=cameras.size();
	vector<vector<typename FeatureMatchingProblemT::feature_type1::coordinate_type> > coordinates(nCameras);
	for(size_t c=0; c<nCameras; ++c)
		coordinates[c]=MakeCoordinateVector(features[c]);

	//Triangulation
	vector<typename SceneFeatureT::coordinate_type> reconstruction;
	vector<CovarianceMatrixT> covariances;
	PairwiseViewT inliers;
	diagnostics.triangulationDiagnositics=MultiviewTriangulationC::Run(reconstruction, covariances, inliers, correspondences, cameras, coordinates, parameters.triangulationParameters);

	if(!diagnostics.triangulationDiagnositics.flagSuccess)
		return diagnostics;

	//Attach descriptors
	vector<SceneFeatureT> features3D=MakeSceneFeature<SceneFeatureT>(reconstruction, inliers, features);

	//Make compact
	if(parameters.flagCompact)
		for_each(features3D, [](SceneFeatureT& current){current.MakeCompact();});

	if(parameters.flagVerbose)
		PrintResult(observedCorrespondences, impliedCorrespondences, nCameras);

	//Output
	pointCloud.swap(reconstruction);
	covarianceList.swap(covariances);
	sceneFeatures.swap(features3D);

	diagnostics.flagSuccess=true;
	return diagnostics;
}	//Sparse3DReconstructionPipelineDiagnosticsC Reconstruct(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<SceneFeatureT>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<ImageFeatureT> >& features, const SimilarityT& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters)

/**
 * @brief Runs the algorithm
 * @param[out] pointCloud 3D reconstruction
 * @param[out] covarianceList Covariance for each scene point
 * @param[out] sceneFeatures Scene features
 * @param[in] cameras Cameras
 * @param[in] features Image features
 * @param[in] similarityMetric Image feature similarity metric
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 * @pre \c features and \c cameras have the same number of elements
 * @remarks Algorithm forces consistent and 1:1 matching
 * @remarks Algorithm assigns a suitable inlier threshold for triangulation, and employs \c parameters.noiseVariance as the image coordinate noise variance
 */
template<class SimilarityT>
Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<SceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<ImageFeatureC> >& features, const SimilarityT& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters)
{
	typedef ImageFeatureMatchingProblemC<SimilarityT, EpipolarSampsonConstraintT> MatchingProblemT;	//Type of the feature matching problem
	return Reconstruct<MatchingProblemT, SceneFeatureC>(pointCloud, covarianceList, sceneFeatures, cameras, features, similarityMetric, parameters);
}	//void Run(const vector<CameraMatrixT>& cameras, const vector<vector<ImageFeatureC> >& features, const SimilarityT& similarityMetric)

/**
 * @brief Runs the algorithm
 * @param[out] pointCloud 3D reconstruction
 * @param[out] covarianceList Covariance for each scene point
 * @param[out] sceneFeatures Scene features
 * @param[in] cameras Cameras
 * @param[in] features Image features
 * @param[in] similarityMetric Image feature similarity metric
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 * @remarks Algorithm forces consistent and 1:1 matching
 * @remarks Algorithm assigns a suitable inlier threshold for triangulation, and employs \c parameters.noiseVariance as the image coordinate noise variance
 */
template<class SimilarityT>
Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const SimilarityT& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters)
{
	typedef BinaryImageFeatureMatchingProblemC<SimilarityT, EpipolarSampsonConstraintT> MatchingProblemT;	//Type of the feature matching problem
	return Reconstruct<MatchingProblemT, OrientedBinarySceneFeatureC>(pointCloud, covarianceList, sceneFeatures, cameras, features, similarityMetric, parameters);
}	//Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>& pointCloud, vector<CovarianceMatrixT>& covarianceList, vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const SimilarityT& similarityMetric, Sparse3DReconstructionPipelineParametersC parameters)
}	//GeometryN
}	//SeeSawN

#endif /* SPARSE_3D_RECONSTRUCTION_PIPELINE_IPP_9210843 */
