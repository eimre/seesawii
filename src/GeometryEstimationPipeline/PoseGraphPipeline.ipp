/**
 * @file PoseGraphPipeline.ipp Implementation of the pose graph pipeline
 * @author Evren Imre
 * @date 8 Nov 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef POSE_GRAPH_PIPELINE_IPP_7012908
#define POSE_GRAPH_PIPELINE_IPP_7012908


#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/property_map/vector_property_map.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <map>
#include <vector>
#include <utility>
#include <cstddef>
#include <tuple>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <functional>
#include <iterator>
#include "GeometryEstimationPipeline.h"
#include "GeometryEstimationPipelineProblemConcept.h"
#include "GeometryEstimatorProblemConcept.h"
#include "../GeometryEstimationComponents/Similarity3DComponents.h"
#include "../GeometryEstimationComponents/Homography3DComponents.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Numeric/NumericalApproximations.h"
#include "../RANSAC/RANSACProblemConcept.h"
#include "../Geometry/Similarity3DSolver.h"
#include "../Geometry/Homography3DSolver.h"
#include "../Geometry/GeometrySolverConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::adjacency_matrix;
using boost::undirectedS;
using boost::no_property;
using boost::vertex;
using boost::add_edge;
using boost::connected_components;
using boost::vector_property_map;
using boost::vertex_name_t;
using boost::edge_weight_t;
using boost::property;
using boost::num_vertices;
using boost::lexical_cast;
using boost::range::copy;
using boost::adaptors::map_keys;
using boost::range::for_each;
using boost::range::transform;
using Eigen::MatrixXd;
using std::map;
using std::vector;
using std::pair;
using std::size_t;
using std::tuple;
using std::tie;
using std::ignore;
using std::invalid_argument;
using std::string;
using std::max_element;
using std::greater;
using std::advance;
using std::distance;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineProblemConceptC;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;
using SeeSawN::GeometryN::Similarity3DSolverC;
using SeeSawN::GeometryN::Homography3DSolverC;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::GeometryN::MakeRANSACHomography3DProblem;
using SeeSawN::GeometryN::MakeRANSACSimilarity3DProblem;
using SeeSawN::GeometryN::MakePDLHomography3DProblem;
using SeeSawN::GeometryN::MakePDLSimilarity3DProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineHomography3DProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineSimilarity3DProblem;
using SeeSawN::GeometryN::RANSACHomography3DProblemT;
using SeeSawN::GeometryN::RANSACSimilarity3DProblemT;
using SeeSawN::GeometryN::PDLSimilarity3DEstimationProblemC;
using SeeSawN::GeometryN::PDLHomography3DProblemT;
using SeeSawN::GeometryN::Similarity3DEstimatorProblemT;
using SeeSawN::GeometryN::Homography3DEstimatorProblemT;
using SeeSawN::GeometryN::Similarity3DPipelineProblemT;
using SeeSawN::GeometryN::Homography3DPipelineProblemT;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::ElementsN::ComposeSimilarity3D;
using SeeSawN::NumericN::ApplyDHondt;
using SeeSawN::RANSACN::RANSACProblemConceptC;
using SeeSawN::RANSACN::ComputeBinSize;


/**
 * @brief Parameters for the pose graph pipeline
 * @ingroup Parameters
 * @nosubgrouping
 */
struct PoseGraphPipelineParametersC
{
	size_t nThreads;	///< Number of threads available to the pipeline

	//Geometry estimation
	double initialEstimateNoiseVariance;	///< Variance of the coordinate noise due to the errors in the initial estimate, in multiples of \c noiseVariance. Added to \c noiseVariance. >=0

	//RANSAC
	double inlierRejectionProbability;	///< Probability of rejecting an inlier. (0,1)
	unsigned int loGeneratorRatio1;	///< Size of the generator set for the local optimisation stage in RANSAC-Stage 1, as a multiple of the minimal generator size. >=1
	unsigned int loGeneratorRatio2;	///< Size of the generator set for the local optimisation stage in RANSAC-Stage 2, as a multiple of the minimal generator size. >=1

	GeometryEstimationPipelineParametersC geometryEstimationPipelineParameters;

	//Vision graph
	size_t minEdgeStrength;	///< Minimum number of correspondences for a valid edge
	bool flagConnected;	///< If \c true the graph only includes the largest connected component

	PoseGraphPipelineParametersC() : nThreads(1), initialEstimateNoiseVariance(4), inlierRejectionProbability(0.05), loGeneratorRatio1(3), loGeneratorRatio2(3), minEdgeStrength(50), flagConnected(true)
	{}

};	//struct VisionGraphPipelineParametersC

/**
 * @brief Diagnostics for the pose graph pipeline
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct PoseGraphPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation is completed successfully

	PoseGraphPipelineDiagnosticsC() : flagSuccess(false)
	{}

};	//struct VisionGraphPipelineDiagnosticsC

/**
 * @brief Pipeline for building a pose graph for a set of point clouds
 * @tparam ProblemT A generic geometry estimation pipeline problem
 * @tparam RNGT
 * @pre \c ProblemT is one of the following problems: Similarity3DPipelineProblemC, Homography3DPipelineProblemT, or their equivalents (unenforced)
 * @pre \c ProblemT satisfies \c GeometryEstimationPipelineProblemConceptC
 * @pre \c ProblemT::geometry_estimation_problem_type satisfies \c GeometryEstimatorProblemConceptC
 * @pre \c ProblemT::matching_problem_type satisfies \c FeatureMatcherProblemConceptC
 * @pre \c ProblemT::geometry_estimation_problem_type::ransac_problem_type1 satisfies \c RANSACProblemConceptC
 * @pre \c ProblemT::geometry_estimation_problem_type::ransac_problem_type2 satisfies \c RANSACProblemConceptC
 * @pre \c ProblemT::geometry_estimation_problem_type::ransac_problem_type1::minimal_solver_type satisfies \c GeometrySolverConceptC
 * @pre \c ProblemT::geometry_estimation_problem_type::ransac_problem_type2::minimal_solver_type satisfies \c GeometrySolverConceptC
 * @remarks As far as the algorithm is concerned, a general projective homography encodes a "projective pose"
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class PoseGraphPipelineC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometryEstimationPipelineProblemConceptC<ProblemT>));
	BOOST_CONCEPT_ASSERT((GeometryEstimatorProblemConceptC<typename ProblemT::geometry_estimation_problem_type>));
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<typename ProblemT::matching_problem_type>));
	BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<typename ProblemT::geometry_estimation_problem_type::ransac_problem_type1> ));
	BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<typename ProblemT::geometry_estimation_problem_type::ransac_problem_type2> ));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename ProblemT::geometry_estimation_problem_type::ransac_problem_type1::minimal_solver_type>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename ProblemT::geometry_estimation_problem_type::ransac_problem_type2::minimal_solver_type>));
	///@endcond

	private:

		typedef typename ProblemT::matching_problem_type MatchingProblemT;	///< Type of the scene feature matching problem
		typedef typename MatchingProblemT::feature_type1 FeatureT;	///< Scene feature type
		typedef vector<FeatureT> FeatureListT;	///< A feature list

		typedef GeometryEstimationPipelineC<ProblemT> GeometryEstimationPipelineT;	///< Type of the geometry estimation pipeline
		typedef typename ProblemT::geometry_estimation_problem_type GeometryEstimationProblemT;	///< Type of the geometry estimation problem
		typedef typename GeometryEstimationProblemT::ransac_problem_type1 RANSACProblem1T;	///< Type of the Stage 1 RANSAC problem
		typedef typename GeometryEstimationProblemT::ransac_problem_type2 RANSACProblem2T;	///< Type of the Stage 2 RANSAC problem
		typedef typename RANSACProblem2T::minimal_solver_type SolverT;	///< Type of the geometry solver
		typedef typename SolverT::model_type ModelT;	///< Type of the geometric entities for the pairwise relationships
		typedef typename RANSACProblem2T::dimension_type1 BinT;	///< RANSAC bin type
		typedef typename GeometryEstimationPipelineT::rng_type RNGT;	///< Type of the random number generator

		typedef typename SolverT::coordinate_type1 CoordinateT;	///< Type for image coordinates
		typedef vector<CoordinateT> CoordinateListT;	///< Type for a list of image coordinates

		typedef pair<size_t, size_t> IndexPairT;	///< An index pair

		typedef tuple<ModelT, optional<MatrixXd>, CorrespondenceListT>	 EdgeT;	///<An edge, the geometric relation linking a point cloud pair

		typedef adjacency_matrix <undirectedS,  property<vertex_name_t, size_t> ,  property<edge_weight_t, double> > PoseGraphT;	///< Type representing a pose graph

		/** @name Task definition */ //@{
		typedef tuple<IndexPairT, unsigned int, optional<ModelT>, double > TaskT;	///< An edge construction task, estimating the pairwise relationship between to cameras
		static constexpr size_t iEdgeId=0;	///< Index for the edge id component
		static constexpr size_t iNThreads=1;	///< Index for the thread count component
		static constexpr size_t iInitialEstimate=2;	///< Index for the initial estimate component
		static constexpr size_t iNoise=3;	///< Index of the noise component
		//@}

		/** @name Implementation details */ //@{
		static void ValidateInput(const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, PoseGraphPipelineParametersC& parameters);	///< Validates the input

		static typename Similarity3DSolverC::model_type MakeInitialEstimate(const Homography3DT& mH1, const Homography3DT& mH2, const Similarity3DSolverC& dummy);	///< Makes an initial estimate from two homographies, similarity case
		static typename Homography3DSolverC::model_type MakeInitialEstimate(const Homography3DT& mH1, const Homography3DT& mH2, const Homography3DSolverC& dummy);	///< Makes an initial estimate from two homographies, projective homography case

		static vector<TaskT> MakeTasks(const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, const PoseGraphPipelineParametersC& parameters);	///< Prepares the tasks

		static vector<BinT> ComputeBinSizes(const vector<CoordinateListT>& coordinateStack, const PoseGraphPipelineParametersC& parameters);	///< Computes the bin sizes for RANSAC

		static tuple<typename Similarity3DSolverC::model_type, optional<MatrixXd>, CorrespondenceListT, GeometryEstimationPipelineDiagnosticsC> EstimateGeometry(RNGT& rng, const TaskT& task, const vector<FeatureListT>& featureStack, const vector<BinT>& binSizeList, const PoseGraphPipelineParametersC& parameters, const Similarity3DSolverC& dummy);	///< Similarity matrix estimation
		static tuple<typename Homography3DSolverC::model_type, optional<MatrixXd>, CorrespondenceListT, GeometryEstimationPipelineDiagnosticsC> EstimateGeometry(RNGT& rng, const TaskT& task, const vector<FeatureListT>& featureStack, const vector<BinT>& binSizeList, const PoseGraphPipelineParametersC& parameters, const Homography3DSolverC& dummy);	///< 3D homography estimation

		typedef map<IndexPairT, EdgeT> EdgeContainerT;	///< Type for holding an edge list
		static PoseGraphT BuildGraph(const EdgeContainerT& edgeList);	///< Builds a graph from the pairwise relationships
		static tuple<PoseGraphT, EdgeContainerT> FilterGraph(const PoseGraphT& graph, const EdgeContainerT& edgeList);	///< Returns the largest connected component in a graph

		//@}

	public:

		typedef PoseGraphT pose_graph_type;	///< Type for the pose graph
		typedef EdgeContainerT edge_container_type;	///< Type for the edge container
		typedef FeatureListT feature_container_type;	///< A container for scene features
		typedef RNGT rng_type;	///< Random number generator type

		/** @name Pairwise model definition */ //@{
		typedef EdgeT edge_type;	///< A pairwise model estimate
		static constexpr size_t iModel=0;	///< Index of the model component
		static constexpr size_t iCovariance=1;	///< Index of the covariance component
		static constexpr size_t iCorrespondenceList=2;	///< Index of the correspondence list component
		//@}

		static PoseGraphPipelineDiagnosticsC Run(PoseGraphT& poseGraph, EdgeContainerT& edgeList, RNGT& rng, const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, PoseGraphPipelineParametersC parameters);	///< Runs the pipeline
};	//class PoseGraphPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the input
 * @param[in] featureStack Image features
 * @param[in] poseList Existing camera matrices
 * @param[in] noiseVariance Noise variance, per point cloud
 * @param[in] parameters Parameters
 */
template<class ProblemT>
void PoseGraphPipelineC<ProblemT>::ValidateInput(const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, PoseGraphPipelineParametersC& parameters)
{
	//Parameters
	if(parameters.nThreads==0)
		throw(invalid_argument("PoseGraphPipelineC::ValidateInput : nThreads must be a positive value") );

	if(parameters.initialEstimateNoiseVariance<0)
		throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : initialEstimateNoiseVariance must be >=0. Value= ") + lexical_cast<string>(parameters.initialEstimateNoiseVariance) ) );

	if(parameters.inlierRejectionProbability<=0 || parameters.inlierRejectionProbability>=1)
		throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : inlierRejectionProbability must be in (0,1). Value= ") + lexical_cast<string>(parameters.inlierRejectionProbability) ) );

	if(parameters.loGeneratorRatio1<1)
		throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : loGeneratorRatio1 must be >=1. Value= ") + lexical_cast<string>(parameters.loGeneratorRatio1) ) );

	if(parameters.loGeneratorRatio2<1)
		throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : loGeneratorRatio2 must be >=1. Value= ") + lexical_cast<string>(parameters.loGeneratorRatio2) ) );

	//Noise variance

	//Either one element, or one element per image
	if(!(noiseVariance.size()==1 || noiseVariance.size()==featureStack.size()))
		throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : noiseVariance holds either one element, or one element per point cloud. Value= ") + lexical_cast<string>(noiseVariance.size()) ));

	//All elements are positive
	for(const auto current : noiseVariance)
		if(current<=0)
			throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : noiseVariance must hold positive values. Value= ") + lexical_cast<string>(current) ) );

	//No more cameras than the images
	if(!poseList.empty())
	{
		size_t maxId=poseList.rbegin()->first;
		if(maxId>=featureStack.size())
			throw(invalid_argument(string("PoseGraphPipelineC::ValidateInput : No matching pose for the pair ")+lexical_cast<string>(maxId)));
	}	//if(!cameraList.empty())

	parameters.geometryEstimationPipelineParameters.flagVerbose=false;
	parameters.geometryEstimationPipelineParameters.nThreads=1;
}	//void ValidateInput(const vector<FeatureListT>& featureStack, const map<size_t, CameraMatrixT>& cameraList, VisionGraphPipelineParametersC parameters)

/**
 * @brief Makes an initial estimate from two homographies, similarity case
 * @param[in] mH1 First homography
 * @param[in] mH2 Second homography
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Similarity transformation mapping a point in the second reference frame to the first
 */
template<class ProblemT>
typename Similarity3DSolverC::model_type PoseGraphPipelineC<ProblemT>::MakeInitialEstimate(const Homography3DT& mH1, const Homography3DT& mH2, const Similarity3DSolverC& dummy)
{
	double scale;
	Coordinate3DT origin;
	RotationMatrix3DT mR;
	tie(scale, origin, mR)=DecomposeSimilarity3D(MakeInitialEstimate(mH1, mH2, Homography3DSolverC()));

	return ComposeSimilarity3D(scale, origin, mR);
}	//typename Similarity3DSolverC::model_type MakeInitialEstimate(const Homography3DT& mH1, const Homography3DT& mH2, const Similarity3DSolverC& dummy)

/**
 * @brief Makes an initial estimate from two homographies, projective homography case
 * @param[in] mH1 First homography
 * @param[in] mH2 Second homography
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Homography mapping a point in the second reference frame to the first
 */
template<class ProblemT>
typename Homography3DSolverC::model_type PoseGraphPipelineC<ProblemT>::MakeInitialEstimate(const Homography3DT& mH1, const Homography3DT& mH2, const Homography3DSolverC& dummy)
{
	return mH1*mH2.inverse();
}	//typename Homography3DSolverC::model_type MakeInitialEstimate(const Homography3DT& mH1, const Homography3DT& mH2, const Homography3DSolverC& dummy)

/**
 * @brief Prepares the tasks
 * @param[in] featureStack Scene features
 * @param[in] poseList Existing homographies
 * @param[in] noiseVariance Noise variance vector
 * @param[in] parameters Parameters
 * @return A vector of tasks
 */
template<class ProblemT>
auto PoseGraphPipelineC<ProblemT>::MakeTasks(const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, const PoseGraphPipelineParametersC& parameters) -> vector<TaskT>
{
	//No features? Return
	if(featureStack.empty())
		return vector<TaskT>();

	//Sort the tasks wrt estimated costs
	size_t noGuidePenalty=pow<2>(max_element(featureStack.begin(), featureStack.end(), [](const FeatureListT& val1, const FeatureListT& val2){ return val1.size()<val2.size();})->size());

	bool flagUniformVariance=noiseVariance.size()==1;
	size_t nCloud=featureStack.size();
	auto itE=poseList.end();
	multimap<double, TaskT, greater<double>> sortedTasks;
	for(size_t c1=0; c1<nCloud; ++c1)
	{
		//Skip hopeless cases
		if(featureStack[c1].size() < parameters.minEdgeStrength)
			continue;

		auto it1=poseList.find(c1);
		for(size_t c2=c1+1; c2<nCloud; ++c2)
		{
			auto it2= (it1!=itE) ? poseList.find(c2) : itE;

			//Initial estimate
			optional<ModelT> model;
			if(it2!=itE)
				model=MakeInitialEstimate(it1->second, it2->second, SolverT());

			//Skip hopeless cases
			if(featureStack[c2].size() < parameters.minEdgeStrength)
				continue;

			double noise= flagUniformVariance ? noiseVariance[0] : 0.5*(noiseVariance[c1]+noiseVariance[c2]);

			//Cost
			double cost= (featureStack[c1].size()*featureStack[c2].size()) + ( model ? 0 : noGuidePenalty);	//Tasks with no initial estimate ranked higher
			sortedTasks.emplace(cost, TaskT(IndexPairT(c1,c2), 1, model, noise));
		}	//for(size_t c2=c1+1; c2<nImage; ++c2)
	}	//for(size_t c1=0; c1<nImage; ++c1)

	//If there are more threads than tasks, allocate the excess threads
	size_t nTasks=sortedTasks.size();
	if(parameters.nThreads > nTasks)
	{
		vector<double> costs(nTasks);
		copy(sortedTasks | map_keys, costs.begin());

		vector<unsigned int> threadDistribution=ApplyDHondt(costs, parameters.nThreads-nTasks);

		auto it=sortedTasks.begin();
		for(size_t c=0; c<nTasks; ++c, std::advance(it,1))
			get<iNThreads>(it->second)=threadDistribution[c];
	}	//if(parameters.nThreads > nTasks)

	//Build the task vector
	vector<TaskT> output(nTasks);
	auto it=sortedTasks.begin();
	for(size_t c=0; c<nTasks; ++c, std::advance(it,1))
		output[c].swap(it->second);

	return output;
}	//vector<TaskT> MakeTasks(const vector<FeatureListT>& featureStack, const map<size_t, CameraMatrixT>& cameraList, const VisionGraphPipelineParametersC& parameters)

/**
 * @brief Computes the bin sizes for RANSAC
 * @param[in] coordinateStack Image feature coordinate stack
 * @param[in] parameters Parameters
 * @return A vector of bin dimensions for each image
 */
template<class ProblemT>
auto PoseGraphPipelineC<ProblemT>::ComputeBinSizes(const vector<CoordinateListT>& coordinateStack, const PoseGraphPipelineParametersC& parameters) -> vector<BinT>
{
	vector<BinT> output; output.reserve(coordinateStack.size());
	for(const auto& current : coordinateStack)
		output.push_back( *ComputeBinSize<CoordinateT>(current, parameters.geometryEstimationPipelineParameters.binDensity1) );

	return output;
}	//vector<BinT> ComputeBinSizes(const vector<FeatureListT>& featureStack, const VisionGraphPipelineParametersC& parameters)

/**
 * @brief Similarity matrix estimation
 * @param[in, out] rng Random number generator
 * @param[in] task Estimation task
 * @param[in] featureStack Scene feature stack
 * @param[in] binSizeList RANSAC bin sizes
 * @param[in] parameters Parameters
 * @param[in] dummy Dummy parameter for overload resolution
 * @return A tuple: Estimated model, covariance, inlier set, diagnostics object
 */
template<class ProblemT>
auto PoseGraphPipelineC<ProblemT>::EstimateGeometry(RNGT& rng, const TaskT& task, const vector<FeatureListT>& featureStack, const vector<BinT>& binSizeList, const PoseGraphPipelineParametersC& parameters, const Similarity3DSolverC& dummy) -> tuple<typename Similarity3DSolverC::model_type, optional<MatrixXd>, CorrespondenceListT, GeometryEstimationPipelineDiagnosticsC>
{
	size_t id1=get<iEdgeId>(task).first;
	size_t id2=get<iEdgeId>(task).second;

	//RANSAC problem
	unsigned int loGeneratorSize1=floor(Similarity3DSolverC::GeneratorSize() * parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2=floor(Similarity3DSolverC::GeneratorSize() * parameters.loGeneratorRatio2);

	typename RANSACProblem2T::data_container_type dummyList;
	RANSACSimilarity3DProblemT ransacProblem1=MakeRANSACSimilarity3DProblem(dummyList, dummyList, get<iNoise>(task), parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSizeList[id1], binSizeList[id2]);
	RANSACSimilarity3DProblemT ransacProblem2=MakeRANSACSimilarity3DProblem(dummyList, dummyList, get<iNoise>(task), parameters.inlierRejectionProbability, 1, loGeneratorSize2, binSizeList[id1], binSizeList[id2]);

	//Optimisation problem
	PDLSimilarity3DProblemT optimisationProblem=MakePDLSimilarity3DProblem(ModelT::Zero(), dummyList, optional<MatrixXd>());

	//Geometry estimator problem
	Similarity3DEstimatorProblemT estimatorProblem(ransacProblem1, ransacProblem2, optimisationProblem, get<iInitialEstimate>(task));

	//Geometry estimation pipeline problem
	double initialEstimateNoiseVariance=get<iNoise>(task)*(1+parameters.initialEstimateNoiseVariance);

	vector<typename Similarity3DPipelineProblemT::covariance_type1> covarianceList(1);	//FIXME Stopgap, we need the covariance ranges
	covarianceList[0].setIdentity(); covarianceList[0]*=get<iNoise>(task);
	Similarity3DPipelineProblemT pipelineProblem=MakeGeometryEstimationPipelineSimilarity3DProblem(featureStack[id1], featureStack[id2], estimatorProblem, covarianceList, covarianceList, get<iNoise>(task), parameters.inlierRejectionProbability, get<iInitialEstimate>(task), initialEstimateNoiseVariance, parameters.inlierRejectionProbability, get<iNThreads>(task));

	GeometryEstimationPipelineParametersC localParameters=parameters.geometryEstimationPipelineParameters;
	localParameters.nThreads=parameters.nThreads;

	//Solve
	Homography3DT model;
	optional<MatrixXd> covariance;
	CorrespondenceListT inliers;
	GeometryEstimationPipelineDiagnosticsC diagnostics=GeometryEstimationPipelineC<Similarity3DPipelineProblemT>::Run(model, covariance, inliers, pipelineProblem, rng, localParameters);

	return make_tuple(model, covariance, inliers, diagnostics);
}	//typename EssentialSolverC::model_type EstimateGeometry(const TaskT& task, const vector<FeatureListT>& featureStack, const VisionGraphPipelineParametersC& parameters, const EssentialSolverC& dummy)

/**
 * @brief Homography estimation
 * @param[in, out] rng Random number generator
 * @param[in] task Estimation task
 * @param[in] featureStack Image feature stack
 * @param[in] binSizeList RANSAC bin sizes
 * @param[in] parameters Parameters
 * @param[in] dummy Dummy parameter for overload resolution
 * @return A tuple: Estimated model, covariance, inlier set, diagnostics object
 */
template<class ProblemT>
auto PoseGraphPipelineC<ProblemT>::EstimateGeometry(RNGT& rng, const TaskT& task, const vector<FeatureListT>& featureStack, const vector<BinT>& binSizeList, const PoseGraphPipelineParametersC& parameters, const Homography3DSolverC& dummy) -> tuple<typename Homography3DSolverC::model_type, optional<MatrixXd>, CorrespondenceListT, GeometryEstimationPipelineDiagnosticsC>
{
	size_t id1=get<iEdgeId>(task).first;
	size_t id2=get<iEdgeId>(task).second;

	//RANSAC problem
	unsigned int loGeneratorSize1=floor(Homography3DSolverC::GeneratorSize() * parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2=floor(Homography3DSolverC::GeneratorSize() * parameters.loGeneratorRatio2);

	typename RANSACProblem2T::data_container_type dummyList;
	RANSACHomography3DProblemT ransacProblem1=MakeRANSACHomography3DProblem(dummyList, dummyList, get<iNoise>(task), parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSizeList[id1], binSizeList[id2]);
	RANSACHomography3DProblemT ransacProblem2=MakeRANSACHomography3DProblem(dummyList, dummyList, get<iNoise>(task), parameters.inlierRejectionProbability, 1, loGeneratorSize2, binSizeList[id1], binSizeList[id2]);

	//Optimisation problem
	PDLHomography3DProblemT optimisationProblem=MakePDLHomography3DProblem(ModelT::Zero(), dummyList, optional<MatrixXd>());

	//Geometry estimator problem
	Homography3DEstimatorProblemT estimatorProblem(ransacProblem1, ransacProblem2, optimisationProblem, get<iInitialEstimate>(task));

	//Geometry estimation pipeline problem
	double initialEstimateNoiseVariance=get<iNoise>(task)*(1+parameters.initialEstimateNoiseVariance);

	vector<typename Homography3DPipelineProblemT::covariance_type1> covarianceList(1);	//FIXME Stopgap, we need the covariance ranges
	covarianceList[0].setIdentity(); covarianceList[0]*=get<iNoise>(task);
	Homography3DPipelineProblemT pipelineProblem=MakeGeometryEstimationPipelineHomography3DProblem(featureStack[id1], featureStack[id2], estimatorProblem, covarianceList, covarianceList, get<iNoise>(task), parameters.inlierRejectionProbability, get<iInitialEstimate>(task), initialEstimateNoiseVariance, parameters.inlierRejectionProbability, get<iNThreads>(task));

	GeometryEstimationPipelineParametersC localParameters=parameters.geometryEstimationPipelineParameters;
	localParameters.nThreads=parameters.nThreads;

	//Solve
	Homography3DT model;
	optional<MatrixXd> covariance;
	CorrespondenceListT inliers;
	GeometryEstimationPipelineDiagnosticsC diagnostics=GeometryEstimationPipelineC<Homography3DPipelineProblemT>::Run(model, covariance, inliers, pipelineProblem, rng, localParameters);

	return make_tuple(model, covariance, inliers, diagnostics);
}	//typename Homography2DSolverC::model_type EstimateGeometry(const TaskT& task, const vector<FeatureListT>& featureStack, const VisionGraphPipelineParametersC& parameters, const Homography2DSolverC& dummy)

/**
 * @brief Builds a graph from the pairwise relationships
 * @param[in, out] edgeList Edge list. At the output, edges included in the graph
 * @return Vision graph
 */
template<class ProblemT>
auto PoseGraphPipelineC<ProblemT>::BuildGraph(const EdgeContainerT& edgeList) -> PoseGraphT
{
	//Map for transferring the point cloud indices to vertex indices
	map<size_t, size_t> cloudToVertexMap;
	for(const auto& current : edgeList)
	{
		cloudToVertexMap.emplace(current.first.first, cloudToVertexMap.size());
		cloudToVertexMap.emplace(current.first.second, cloudToVertexMap.size());
	}	//for(const auto& current : edgeList)

	//Build the graph

	PoseGraphT graph(cloudToVertexMap.size());

	for(const auto& current : edgeList)
	{
		size_t edgeStrength=get<iCorrespondenceList>(current.second).size();
		size_t id1=cloudToVertexMap[current.first.first];
		size_t id2=cloudToVertexMap[current.first.second];

		add_edge(id1, id2, edgeStrength, graph);
	}	//for(auto it=edgeList.begin(); it!=itE; )

	//Add the vertex properties
	for(const auto& current : cloudToVertexMap)
		boost::put(vertex_name_t(), graph, vertex(current.second, graph), current.first);

	return graph;
}	//tuple<VisionGraphT, EdgeContainerT> BuildGraph(const EdgeContainerT& initalEdgeList, const VisionGraphPipelineParametersC& parameters)

/**
 * @brief Returns the largest connected component in a graph
 * @param[in, out] graph Graph to be filtered. Consumed by the function
 * @param[in] edgeList Edge list corresponding for the graph
 * @return A tuple: Filtered graph and edge list
 */
template<class ProblemT>
auto PoseGraphPipelineC<ProblemT>::FilterGraph(const PoseGraphT& graph, const EdgeContainerT& edgeList) -> tuple<PoseGraphT, EdgeContainerT>
{
	//Connected components
	size_t nVertex=num_vertices(graph);
	vector_property_map<size_t> vertexToComponentMap(nVertex);
	size_t nComponents=connected_components(graph, vertexToComponentMap);

	//No filtering, return the original graph
	if(nComponents==1)
		return tuple<PoseGraphT, EdgeContainerT>(graph, edgeList);

	//Find the largest component
	vector<unsigned int> accumulator(nComponents, 0);
	for(size_t c=0; c<nVertex; ++c)
		++accumulator[ vertexToComponentMap[c] ];

	size_t iLargest= distance(accumulator.begin(), max_element(accumulator.begin(), accumulator.end()));

	//Filter the edge List
	EdgeContainerT filteredEdgeList;
	for(const auto& current : edgeList)
		if(vertexToComponentMap[current.first.first]==iLargest)
			filteredEdgeList.emplace(current.first, current.second);

	//Rebuild the map
	PoseGraphT filteredGraph=BuildGraph(filteredEdgeList);

	return make_tuple(filteredGraph, filteredEdgeList);
}	//tuple<VisionGraphT, EdgeContainerT> FilterGraph(const VisionGraphT& graph, const EdgeContainerT& edgeList, const VisionGraphPipelineParametersC& parameters)

/**
 * @brief Runs the pipeline
 * @param[out] poseGraph Pose graph. Each vertex holds its original id in \c featureStack
 * @param[out] edgeList Edges in the vision graph. The frame indices refer to \c featureStack , not the vertex indices in the graph
 * @param[in, out] rng Random number generator
 * @param[in] featureStack Scene feature stack
 * @param[in] poseList Available pose homographues
 * @param[in] noiseVariance Noise variance. If the noise is identical for all point clouds, holds one element. Otherwise, one element per point cloud.
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @pre \c noiseVariance holds either one element, or one element per image. All elements are positive
 */
template<class ProblemT>
PoseGraphPipelineDiagnosticsC PoseGraphPipelineC<ProblemT>::Run(PoseGraphT& poseGraph, EdgeContainerT& edgeList, RNGT& rng, const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, PoseGraphPipelineParametersC parameters)
{
	PoseGraphPipelineDiagnosticsC diagnostics;

	//Validate the input and the parameters
	ValidateInput(featureStack, poseList, noiseVariance, parameters);

	//Generate the tasks
	vector<TaskT> taskList=MakeTasks(featureStack, poseList, noiseVariance, parameters);

	//Compute the bin sizes
	size_t nImage=featureStack.size();
	vector<CoordinateListT> coordinateStack(nImage);
	transform(featureStack, coordinateStack.begin(), [](const FeatureListT& current){return MakeCoordinateVector(current);});

	vector<BinT> binSizeList=ComputeBinSizes(coordinateStack, parameters);

	//Estimate the pairwise relations
	map<IndexPairT, EdgeT> initialEdgeList;
	size_t nTasks=taskList.size();
	size_t c;
#pragma omp parallel for if(parameters.nThreads>1) schedule(dynamic) private(c)  num_threads(parameters.nThreads)
	for(c=0; c<nTasks; ++c)
	{
		//Geometry estimation
		ModelT model;
		optional<MatrixXd> covariance;
		CorrespondenceListT inliers;
		GeometryEstimationPipelineDiagnosticsC geDiagnostics;
		std::tie(model, covariance, inliers, geDiagnostics)=EstimateGeometry(rng, taskList[c], featureStack, binSizeList, parameters, SolverT());

	#pragma omp critical(VGP_R1)
		if(geDiagnostics.flagSuccess && inliers.size()>=parameters.minEdgeStrength)
			initialEdgeList.emplace(get<iEdgeId>(taskList[c]), EdgeT(model, covariance, inliers) );
	}	//for(c=0; c<nTasks; ++c)

	//Build the graph

	if(initialEdgeList.empty())
		return diagnostics;

	poseGraph=BuildGraph(initialEdgeList);

	if(parameters.flagConnected)
		std::tie(poseGraph, edgeList)=FilterGraph(poseGraph, initialEdgeList);
	else
		edgeList.swap(initialEdgeList);

	diagnostics.flagSuccess=true;

	return diagnostics;

}	//PoseGraphPipelineDiagnosticsC Run(PoseGraphT& poseGraph, EdgeContainerT& edgeList, RNGT& rng, const vector<FeatureListT>& featureStack, const map<size_t, Homography3DT>& poseList, const vector<double>& noiseVariance, PoseGraphPipelineParametersC parameters)

}	//GeometryN
}	//SeeSawN


#endif /* POSE_GRAPH_PIPELINE_IPP_7012908 */
