/**
 * @file RotationRegistrationPipeline.cpp Implementation of the relative rotation registration pipeline
 * @author Evren Imre
 * @date 29 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RotationRegistrationPipeline.h"

namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the input
 * @param observations Observations
 * @param parameters Parameters
 * @throws invalid_argument If any of the input parameters is invalid
 */
void RotationRegistrationPipelineC::ValidateInput(const vector<ObservationT>& observations, const RotationRegistrationPipelineParametersC& parameters)
{
	//Parameters
	if(parameters.inlierThreshold<0 || parameters.inlierThreshold>pi<RealT>())
		throw(invalid_argument(string("RotationRegistrationPipelineC::ValidateInput : parameters.inlierThreshold must be in [0,pi]. Value= ") + lexical_cast<string>(parameters.inlierThreshold)));

	//Connected graph?
	if(!RelativeRotationRegistrationC::ValidateObservations(observations))
		throw(invalid_argument("RotationRegistrationPipelineC::ValidateInput : The observations must form a connected graph"));
}	//void ValidateInput(const vector<ObservationT>& observations, const RSTSRotationRegistrationProblemC& problem)

/**
 * @brief Runs the algorithm
 * @param[out] model Estimated model
 * @param[out] inliers Inliers to the estimated model. Each element is an index into \c observations
 * @param[in, out] rng Random number generator
 * @param[in] observations Relative rotation observations
 * @param[in] parameters Parameters
 * @return Diagnostics object
 */
RotationRegistrationPipelineDiagnosticsC RotationRegistrationPipelineC::Run(ModelT& model, IndexContainerT& inliers, RNGT& rng, const vector<ObservationT>& observations, const RotationRegistrationPipelineParametersC& parameters)
{
	//Validate the input
	ValidateInput(observations, parameters);

	RotationRegistrationPipelineDiagnosticsC diagnostics;

	//Random spanning tree sampling pass
	RSTSRotationRegistrationProblemC problem(observations, parameters.inlierThreshold);

	RSTSEngineT::result_type robustModel;
	diagnostics.rstsDiagnostics=RSTSEngineT::Run(robustModel, problem, rng, parameters.rstsParameters);

	//If fails, minimum spanning tree
	if(!diagnostics.rstsDiagnostics.flagSuccess)
		diagnostics.mstDiagnostics=RSTSEngineT::ComputeMSTSolution(robustModel, problem);

	//Form the inlier list
	vector<size_t> robustInliers=problem.RetrieveObservationIndexList(get<RSTSEngineT::iInlierList>(robustModel));
	vector<ObservationT> robustInlierEdges; robustInlierEdges.reserve(robustInliers.size());
	for_each(robustInliers, [&](size_t c){ robustInlierEdges.push_back(observations[c]); } );

	//Refinement pass
	vector<RotationMatrix3DT> lsModelR=RelativeRotationRegistrationC::Run(robustInlierEdges, parameters.flagWeightedLS);
	diagnostics.flagSuccessLS=!lsModelR.empty();

	//If successful, evaluate
	RealT lsError=numeric_limits<RealT>::infinity();
	IndexContainerT lsInliers;
	ModelT lsModelQ; lsModelQ.reserve(lsModelR.size());
	if(diagnostics.flagSuccessLS)
	{
		for_each(lsModelR, [&](const RotationMatrix3DT& mR){lsModelQ.emplace_back(mR);});
		tie(lsInliers, lsError)=problem.EvaluateModel(lsModelQ);
	}	//if(diagnostics.flagSuccessLS)

	//Output
	if(lsError<get<RSTSEngineT::iModelError>(robustModel))
	{
		model=lsModelQ;
		inliers=lsInliers;
		diagnostics.error=lsError;
	}
	else
	{
		model=get<RSTSEngineT::iModel>(robustModel);
		inliers=problem.RetrieveObservationIndexList(get<RSTSEngineT::iInlierList>(robustModel));
		diagnostics.error=get<RSTSEngineT::iModelError>(robustModel);
	}	//if(lsError<get<RSTSEngineT::iModelError>(robustModel))

	diagnostics.flagSuccess=true;
	diagnostics.inlierRatio=(double)inliers.size()/observations.size();

	return diagnostics;
}	//RotationRegistrationPipelineDiagnosticsC Run(ModelT& model, IndexContainerT& inliers, RNGT& rng, const vector<ObservationT>& observations, const RotationRegistrationPipelineParametersC& parameters)

}	//GeometryN
}	//SeeSawN

