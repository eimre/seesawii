/**
 * @file NodalCameraTrackingPipeline.ipp Implementation of the nodal camera tracker
 * @author Evren Imre
 * @date 5 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef NODAL_CAMERA_TRACKING_PIPELINE_IPP_6581009
#define NODAL_CAMERA_TRACKING_PIPELINE_IPP_6581009

#include <boost/optional.hpp>
#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <vector>
#include <stdexcept>
#include <string>
#include <tuple>
#include <cmath>
#include <map>
#include <functional>
#include <limits>
#include "../GeometryEstimationComponents/GenericPnPComponents.h"
#include "../GeometryEstimationPipeline/GeometryEstimatorProblemConcept.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../Geometry/Rotation.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Camera.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Wrappers/BoostRange.h"
#include "../Wrappers/BoostBimap.h"
#include "../Metrics/Distance.h"
#include "../Optimisation/PowellDogLeg.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::lexical_cast;
using boost::ForwardRangeConcept;
using boost::dynamic_bitset;
using boost::for_each;
using boost::find_if;
using Eigen::MatrixXd;
using Eigen::Matrix3d;
using Eigen::Vector3d;
using std::vector;
using std::string;
using std::invalid_argument;
using std::tie;
using std::ignore;
using std::cos;
using std::fabs;
using std::map;
using std::greater;
using std::numeric_limits;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelinePnPProblem;
using SeeSawN::GeometryN::MakePDLPnPProblem;
using SeeSawN::GeometryN::MakeRANSACPnPProblem;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::ComputeFoV;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::WrappersN::MakePermutationRange;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::MakeBitset;
using SeeSawN::MetricsN::HammingDistanceC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PowellDogLegParametersC;

/**
 * @brief Parameters for the nodal camera tracking pipeline
 * @ingroup Parameters
 * @nosubgrouping
 */
struct NodalCameraTrackingPipelineParametersC
{
	unsigned int nThreads;	///< Number of threads. >0

	//Tracker parameters
	bool flagFixedFocalLength;	///< \c true if the focal length is fixed (but not necessarily known)
	optional<double> focalLength;	///< Focal length for the first frame. Invalid if not known. >0

	double supportLossTrackingFailure;	///< Minimum support loss to trigger a tracker failure. [0,1]

	unsigned int minSupport;	///< Minimum support for a valid registration

	double minFocalLength;	///< Minimum value of the focal length, >0.
	double maxFocalLength;	///< Maximum value of the focal length, <0 & <=minFocalLength

	//Mode change triggers
	double supportLossModeSwitch;	///< Minimum support loss to trigger a mode switch to NODAL_ZOOM [0,1]
	double focalLengthSensitivity;	///< If the relative change in focal length is below this value, mode switch to NODAL. >0

	double generatorUpdateThreshold=0.9;    ///< Update the generator if the inlier ratio between the solutions proposed by the existing and the new generator is below this value. [0,1]

	double zoomMeasurementWeight;	///< Measurement weight for the zoom rate parameter in state update. [0,1]
	double rotationMeasurementWeight;	///< Measurement weight for the rotation parameter in state update. [0,1]

	bool flagApproximateNeighbourhoods;	///< If \c true guided matching uses approximate neighbourhoods when verifying the constraint. Can skip some viable match candidates

	double imageNoiseVariance;	///< Image noise variance. >0
	double predictionNoise;	///< Variance of the additive image noise due to the prediction error, in terms of the noise variance. >=0
	double worldNoiseVariance;	///< Variance of the coordinate noise on the scene points. >0
	double inlierRejectionProbability;	///< Inlier rejection probability. (0,1)

	unsigned int loGeneratorRatio1;	///< Generator size for LO-RANSAC, as a multiple of the minimal generator set. First stage. >=1
	unsigned int loGeneratorRatio2;	///< Generator size for LO-RANSAC, as a multiple of the minimal generator set. Second stage. >=1

	double imageDiagonal;	///< Size of the image diagonal, in pixels. For P2P, should be normalised with the focal length. >0
	GeometryEstimationPipelineParametersC pipelineParameters;	///< Geometry estimation pipeline parameters

	//Tracker
	NodalCameraTrackingPipelineParametersC() : nThreads(1), flagFixedFocalLength(false), supportLossTrackingFailure(0.3), minSupport(28), minFocalLength(1), maxFocalLength(numeric_limits<double>::infinity()),  supportLossModeSwitch(0.1), focalLengthSensitivity(0.005), zoomMeasurementWeight(0.25), rotationMeasurementWeight(0.25), flagApproximateNeighbourhoods(true), imageNoiseVariance(4), predictionNoise(256), worldNoiseVariance(1e-4), inlierRejectionProbability(0.05), loGeneratorRatio1(1), loGeneratorRatio2(1), imageDiagonal(2200)
	{}
};	//NodalCameraTrackingPipelineParametersC

/**
 * @brief Diagnostics for the nodal camera tracking pipeline
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct NodalCameraTrackingPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the pipeline terminates successfully
	size_t nInliers;	///< Number of inliers
	bool flagRelocalise;	///< \c true if relocalisation triggered
	bool flagNodal;	///< \c true if the registration for the current frame is via the nodal solver

	double deltaSupport;
	unsigned int nRegistration;	///< Number of calls to the registration module

	CorrespondenceListT supportSet;    ///< 3D-2D correspondences supporting the last registration

	GeometryEstimationPipelineDiagnosticsC geDiagnostics;	///< Diagnostics for the most recent call to the geometry estimation pipeline

	NodalCameraTrackingPipelineDiagnosticsC() : flagSuccess(false), nInliers(0), flagRelocalise(false), flagNodal(false), deltaSupport(0), nRegistration(0)
	{}
};	//NodalCameraTrackingPipelineDiagnosticsC

/**
 * @brief Nodal camera tracking pipeline
 * @tparam NodalGeometryEstimatorProblemT  A geometry estimator problem for orientation estimation
 * @tparam NodalZoomGeometryEstimatorProblemT A geometry estimator problem for orientation and zoom estimation
 * @tparam MatchingProblemT A matching problem
 * @pre \c MatchingProblemT is a model of \c FeatureMatcherProblemConceptC
 * @pre \c NodalGeometryEstimatorProblemT is a model of GeometryEstimatorProblemConceptC
 * @pre \c NodalZoomGeometryEstimatorProblemT is a model of GeometryEstimatorProblemConceptC
 * @remarks The algorithm assumes that
 * 	- the camera is located at the world origin
 * 	- the image features are normalised with the known intrinsics (all except for the focal length)
 * @remarks Generator jitter:
 *  - Problem: RANSAC usually picks a new generator at each frame, causing a jitter.
 *  - Solution: Stick to the same generator as long as it is available, and has a similar support to the new generator
 * @ingroup Algorithm
 * @nosubgrouping
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
class NodalCameraTrackingPipelineC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT( (GeometryEstimatorProblemConceptC<NodalGeometryEstimatorProblemT>) );
	BOOST_CONCEPT_ASSERT( (GeometryEstimatorProblemConceptC<NodalZoomGeometryEstimatorProblemT>) );
	BOOST_CONCEPT_ASSERT( (FeatureMatcherProblemConceptC<MatchingProblemT>) );
	//@endcond

	public:

		enum class TrackerModeT{NODAL, NODAL_ZOOM};	///< Tracker mode indicator

		struct TrackerStateC
		{
			QuaternionT orientation=QuaternionT(1,0,0,0);	///< Orientation of the camera
			double focalLength=0;	///< Focal length of the camera

			Vector3d rotation=Vector3d::Zero();	///< Rotation rate
			double zoomRate=0;	///< Focal length change rate

			TrackerModeT trackingMode=TrackerModeT::NODAL_ZOOM;	///< Current tracking mode

			vector<size_t> generator;   ///< Generator set for the current estimate. Empty if the estimation fails
 		};	//struct TrackerStateC

	private:

		typedef typename GeometryEstimatorC<NodalZoomGeometryEstimatorProblemT>::rng_type RNGT;
		typedef typename NodalGeometryEstimatorProblemT::ransac_problem_type2::minimal_solver_type MainNodalSolverT;
		typedef typename NodalZoomGeometryEstimatorProblemT::ransac_problem_type2::minimal_solver_type MainNodalZoomSolverT;

		typedef typename MatchingProblemT::feature_type1 SceneFeatureT;	///< Scene feature type
		typedef typename MatchingProblemT::feature_type2 ImageFeatureT;	///< Image feature type

		/**@name Configuration */ //@{
		vector<SceneFeatureT> world;	///< Scene model
		NodalCameraTrackingPipelineParametersC parameters;	///< Parameters

		typename NodalZoomGeometryEstimatorProblemT::ransac_problem_type2::dimension_type1 binSize1;	///< RANSAC bin size for the world
		//@}

		struct RecordC
		{
			size_t sSupport=0;
			unsigned int nRegistration=0;
		} record;	//< Auxiliary variables

		/**@name State */ //@{
		bool flagValid;	///< \c true if the object is initialised
		bool flagInitialised;	///< \c true if the tracker is initialised, i.e. has a valid state

		TrackerStateC trackerState;	///< State of the tracker
		//@}

		/**@name Implementation details */ //@{
		void Validate();	///< Validates the parameters

		vector<size_t> ApplyVisibilityFilter(const RotationMatrix3DT& mR, double focalLength) const;	///< Filters out the world points that are unlikely to be visible in the current frame

		bool DetectTrackerFailure(NodalCameraTrackingPipelineDiagnosticsC& diagnostics, const CameraMatrixT& estimate, const CorrespondenceListT& correspondences) const;	///< Verifies the consistency of the current estimate with the state

		template<class GeometryEstimationProblemT> tuple<GeometryEstimationPipelineDiagnosticsC, CameraMatrixT, CorrespondenceListT> RegisterCamera(RNGT& rng, optional<MatchingProblemT>& matchingProblem, const vector<ImageFeatureT>& observations, const optional<CameraMatrixT>& initialEstimate, const optional<vector<size_t> >& visibleLandmarks);	///< Registers the current image to the world model

		void AssertGenerator(tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>& solution, const vector<ImageFeatureT>& observations); ///< Dispatcher for asserting the generator in the state
		template<class GeometryEstimatorProblemT> bool AssertGenerator(tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>& solution, const vector<Coordinate2DT>& observations, double imageNoise);  ///< Attempt to replace the best solution with one created from the current generator in the state

		TrackerModeT SetTrackerMode(double focalLengthEstimate, const CorrespondenceListT& correspondences, TrackerModeT currentMode, bool flagRelocalisation) const;	///< Sets a new tracker mode if necessary

		void UpdateState(const CameraMatrixT& measurement);		///< Updates the state
		TrackerStateC Predict() const; ///< Predicts the next measurement
		//@}

	public:

		typedef RNGT rng_type;	///< Random number generator type

		NodalCameraTrackingPipelineC();	///< Default constructor
		template<class SceneFeatureRangeT> NodalCameraTrackingPipelineC(const SceneFeatureRangeT& wworld, const NodalCameraTrackingPipelineParametersC& pparameters);	///< Constructor

		NodalCameraTrackingPipelineDiagnosticsC Update(RNGT& rng, vector<ImageFeatureT>& observations);	///<Updates the tracker state with the observation

		const TrackerStateC& GetState() const;	///< Returns the state
		TrackerStateC PropagateState(const TrackerStateC& src) const;	///< Propagates a given state object
};	//NodalCameraTrackingPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @throws invalid_argument If any of the input parameters is invalid
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
void NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT >::Validate()
{
	size_t minGeneratorSize=min(MainNodalSolverT::GeneratorSize(), MainNodalZoomSolverT::GeneratorSize());
	if(world.size()<minGeneratorSize)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate : world must have at least ")+lexical_cast<string>(minGeneratorSize)+" elements. Value= "+lexical_cast<string>(world.size())  ) );

	if(parameters.nThreads==0)
		throw(invalid_argument("NodalCameraTrackingPipelineC::Validate: parameters.nThreads must be a positive value") );

	if(parameters.imageNoiseVariance<=0)
		throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.imageNoiseVariance must be >0. Value=") + lexical_cast<string>(parameters.imageNoiseVariance)));

	if(parameters.predictionNoise<0)
		throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.predictionNoise must be >=0. Value=") + lexical_cast<string>(parameters.predictionNoise)));

	if(parameters.worldNoiseVariance<=0)
		throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.worldNoiseVariance must be >0. Value=") + lexical_cast<string>(parameters.worldNoiseVariance)));

	if(parameters.inlierRejectionProbability<=0 || parameters.inlierRejectionProbability>=1)
		throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.inlierRejectionProbability must be in (0,1). Value=") + lexical_cast<string>(parameters.inlierRejectionProbability)));

	if(parameters.loGeneratorRatio1<1)
		throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.loGeneratorRatio1 must be >=1. Value=") + lexical_cast<string>(parameters.loGeneratorRatio1)));

	if(parameters.loGeneratorRatio2<1)
		throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.loGeneratorRatio2 must be >=1. Value=") + lexical_cast<string>(parameters.loGeneratorRatio2)));

	if(parameters.focalLength && *parameters.focalLength<=0)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.focalLength must be positive. Value=") + lexical_cast<string>(*parameters.focalLength) ));

	if(parameters.focalLength && parameters.flagFixedFocalLength)
	{
		parameters.minFocalLength=*parameters.focalLength;
		parameters.maxFocalLength=*parameters.focalLength;
	}	//if(parameters.focalLength && parameters.flagFixedFocalLength)

	if(parameters.minFocalLength<=0)
		throw(invalid_argument ( string("NodalCameraTrackingPipelineC::Validate: parameters.minFocalLength must be positive. Value=") + lexical_cast<string>(parameters.minFocalLength)) );

	if(parameters.maxFocalLength<parameters.minFocalLength)
		throw(invalid_argument ( string("NodalCameraTrackingPipelineC::Validate: parameters.maxFocalLength must be >= minFocalLength. Value=") + lexical_cast<string>(parameters.maxFocalLength)) );

	if(parameters.supportLossTrackingFailure <0 || parameters.supportLossTrackingFailure>1)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.supportLossTrackingFailure must be in [0,1]. Value=") + lexical_cast<string>(parameters.supportLossTrackingFailure) ));

	if(parameters.supportLossModeSwitch <0 || parameters.supportLossModeSwitch>1)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.supportLossModeSwitch must be in [0,1]. Value=") + lexical_cast<string>(parameters.supportLossModeSwitch) ));

	if(parameters.focalLengthSensitivity<=0)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.focalLengthSensitivity must be positive. Value=") + lexical_cast<string>(parameters.focalLengthSensitivity) ));

	if(parameters.generatorUpdateThreshold<0 || parameters.generatorUpdateThreshold>1)
	    throw(invalid_argument(string("NodalCameraTrackingPipelineC::Validate: parameters.generatorUpdateThreshold  must be in [0,1]. Value=") + lexical_cast<string>(parameters.generatorUpdateThreshold) ) );

	if(parameters.zoomMeasurementWeight <0 || parameters.zoomMeasurementWeight>1)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.zoomMeasurementWeight must be in [0,1]. Value=") + lexical_cast<string>(parameters.zoomMeasurementWeight) ));

	if(parameters.rotationMeasurementWeight <0 || parameters.rotationMeasurementWeight>1)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.rotationMeasurementWeight must be in [0,1]. Value=") + lexical_cast<string>(parameters.rotationMeasurementWeight) ));

	if(parameters.imageDiagonal<=0)
		throw(invalid_argument( string("NodalCameraTrackingPipelineC::Validate: parameters.imageDiagonal must be positive. Value=") + lexical_cast<string>(parameters.imageDiagonal) ));

	parameters.pipelineParameters.nThreads=parameters.nThreads;
	parameters.pipelineParameters.flagVerbose=false;
	parameters.pipelineParameters.flagCovariance=false;
}	//void Validate(const vector<Feature1T>& world, const NodalCameraTrackingPipelineParametersC& parameters)

/**
 * @brief Filters out the world points that are unlikely to be visible in the current frame
 * @param[in] mR Rotation matrix
 * @param[in] focalLength Focal length
 * @return Indices to the filtered world points
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
auto NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::ApplyVisibilityFilter(const RotationMatrix3DT& mR, double focalLength) const -> vector<size_t>
{

	//Estimate the field-of-view
	double fov=ComputeFoV(parameters.imageDiagonal, focalLength);

	//Effective FoV is larger, due to the prediction noise
	double padding=MainNodalZoomSolverT::error_type::ComputeOutlierThreshold(parameters.inlierRejectionProbability, parameters.imageNoiseVariance*(1+parameters.predictionNoise));
	fov*=1+(padding/parameters.imageDiagonal);

	double threshold=fabs(cos(fov/2));

	RotationVectorT principalAxis=mR.row(2);

	size_t sWorld=world.size();
	vector<size_t> output; output.reserve(sWorld);
	for(size_t c=0; c<sWorld; ++c)
		if(world[c].Coordinate().dot(principalAxis) > threshold)
			output.push_back(c);

	output.shrink_to_fit();
	return output;
}	//vector<SceneFeatureT> ApplyVisibilityFilter() const

/**
 * @brief Verifies the consistency of the current estimate with the state
 * @param[in, out] diagnostics Diagnostics
 * @param[in] estimate Estimated camera
 * @param[in] correspondences Inliers to the estimate
 * @return \c true to indicate a tracker failure
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
bool NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::DetectTrackerFailure(NodalCameraTrackingPipelineDiagnosticsC& diagnostics, const CameraMatrixT& estimate, const CorrespondenceListT& correspondences) const
{
	double deltaSupport= (double)correspondences.size()/record.sSupport-1.0;
	diagnostics.deltaSupport=deltaSupport;

	return deltaSupport < -parameters.supportLossTrackingFailure;
}	//bool DetectTrackerFailure(const CameraMatrixT& estimate, const CorrespondenceListT& correspondences)

/**
 * @brief Default constructor
 * @post The object is invalid
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::NodalCameraTrackingPipelineC() : flagValid(false), flagInitialised(false)
{}

/**
 * @brief Constructor
 * @tparam SceneFeatureRangeT A range of scene features
 * @param[in] wworld 3D point cloud
 * @param[in] pparameters Parameters
 * @pre \c SceneFeatureRangeT is a forward range
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
template<class SceneFeatureRangeT>
NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::NodalCameraTrackingPipelineC(const SceneFeatureRangeT& wworld, const NodalCameraTrackingPipelineParametersC& pparameters) : parameters(pparameters)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<SceneFeatureRangeT>));

	size_t nFeatures=boost::distance(wworld);
	world.reserve(nFeatures);
	for(const auto& current : wworld)
		world.push_back(current);

	vector<Coordinate3DT> coordinateList=MakeCoordinateVector(world);
	binSize1=*ComputeBinSize<Coordinate3DT>(coordinateList, parameters.pipelineParameters.binDensity1);

	Validate();

	trackerState.trackingMode= parameters.focalLength ? TrackerModeT::NODAL : TrackerModeT::NODAL_ZOOM;

	if(parameters.focalLength)
		trackerState.focalLength=*parameters.focalLength;

	flagValid=true;
	flagInitialised=false;

	record.sSupport=0;
}	//NodalCameraTrackingPipelineC(const SceneFeatureRangeT& wworld, const NodalCameraTrackingPipelineParametersC& pparameters)

/**
 * @brief Registers the current image to the world model
 * @param[in,out] rng Random number generator
 * @param[in, out] matchingProblem 3D-2D matching problem. Valid after the first call to this function when performing an update
 * @param[in] observations Image observations
 * @param[in] initialEstimate Initial estimate. Invalid if there is no valid state
 * @param[in] visibleLandmarks Indices of visible landarks. Invalid if no visibility analysis was made
 * @return A tuple: Diagnostics, estimated camera matrix, inliers
 */
template<class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
template<class GeometryEstimatorProblemT>
tuple<GeometryEstimationPipelineDiagnosticsC, CameraMatrixT, CorrespondenceListT> NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::RegisterCamera(RNGT& rng, optional<MatchingProblemT>& matchingProblem, const vector<ImageFeatureT>& observations, const optional<CameraMatrixT>& initialEstimate, const optional<vector<size_t> >& visibleLandmarks)
{
	//Reset the matching problem
	if(!visibleLandmarks)
		matchingProblem=optional<MatchingProblemT>();	//If relocalisation, reset the problem

	//RANSAC problem
	typedef typename GeometryEstimatorProblemT::ransac_problem_type1 RANSACProblem1T;
	typedef typename GeometryEstimatorProblemT::ransac_problem_type2 RANSACProblem2T;

	unsigned int loGeneratorSize1=RANSACProblem1T::minimal_solver_type::GeneratorSize()*parameters.loGeneratorRatio1;
	unsigned int loGeneratorSize2=RANSACProblem2T::minimal_solver_type::GeneratorSize()*parameters.loGeneratorRatio2;

	vector<Coordinate2DT> coordinateList=MakeCoordinateVector(observations);
	typename RANSACProblem2T::dimension_type2 binSize2=*ComputeBinSize<Coordinate2DT>(coordinateList, parameters.pipelineParameters.binDensity2);

	typename RANSACProblem2T::data_container_type dummyList;
	RANSACProblem1T ransacProblem1=MakeRANSACPnPProblem<typename GeometryEstimatorProblemT::ransac_problem_type1::minimal_solver_type, typename GeometryEstimatorProblemT::ransac_problem_type1::lo_solver_type>(dummyList, dummyList, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSize1, binSize2);
	RANSACProblem2T ransacProblem2=MakeRANSACPnPProblem<typename GeometryEstimatorProblemT::ransac_problem_type2::minimal_solver_type, typename GeometryEstimatorProblemT::ransac_problem_type2::lo_solver_type>(dummyList, dummyList, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, 1, loGeneratorSize2, binSize1, binSize2);

	//PDL problem
	typedef typename GeometryEstimatorProblemT::optimisation_problem_type PDLProblemT;
	PDLProblemT pdlProblem=MakePDLPnPProblem<PDLProblemT>(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	//Geometry estimator problem
	GeometryEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, pdlProblem, initialEstimate);

	//Pipeline problem
	vector<Matrix3d> worldCovariance(1, parameters.worldNoiseVariance*Matrix3d::Identity());
	typedef GenericGeometryEstimationPipelineProblemC<GeometryEstimatorProblemT, MatchingProblemT> PipelineProblemT;
	PipelineProblemT problem;
	if(visibleLandmarks)
		problem=MakeGeometryEstimationPipelinePnPProblem<GeometryEstimatorProblemT, MatchingProblemT>(MakePermutationRange(world, *visibleLandmarks), observations, geometryEstimationProblem, worldCovariance, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, initialEstimate , parameters.imageNoiseVariance*(1+parameters.predictionNoise), parameters.inlierRejectionProbability, parameters.nThreads, parameters.flagApproximateNeighbourhoods);
	else
		problem=MakeGeometryEstimationPipelinePnPProblem<GeometryEstimatorProblemT, MatchingProblemT>(world, observations, geometryEstimationProblem, worldCovariance, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, initialEstimate , parameters.imageNoiseVariance*(1+parameters.predictionNoise), parameters.inlierRejectionProbability, parameters.nThreads, parameters.flagApproximateNeighbourhoods);

	CameraMatrixT estimated;
	optional<MatrixXd> covariance;
	CorrespondenceListT correspondences;
	GeometryEstimationPipelineDiagnosticsC diagnostics=GeometryEstimationPipelineC<PipelineProblemT>::Run(estimated, covariance, correspondences, problem, matchingProblem, rng,parameters.pipelineParameters);

	//If there is a valid visibility filter, adjust the inliers indices so that they refer to the world indices
	if(visibleLandmarks)
	{
	    for(auto& current : correspondences)
	        current.left=(*visibleLandmarks)[current.left];

	    //Generator indices has to be altered as well
	   for(auto& current : diagnostics.generator)
	       current.left = (*visibleLandmarks)[current.left];
	}

	++record.nRegistration;
	return make_tuple(diagnostics, estimated, correspondences);
}	//tuple<NodalCameraTrackingPipelineDiagnosticsC, CameraMatrixT, CorrespondenceListT> RegisterCamera(RNGT& rng, const vector<ImageFeatureT>& observations)


/**
 * @brief Dispatcher for asserting the generator in the state
 * @param[in] solution Current best solution
 * @param[in] observations Image features
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
void NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::AssertGenerator(tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>& solution, const vector<ImageFeatureT>& observations)
{
    //Feature to coordinate conversion
    vector<Coordinate2DT> coordinates = MakeCoordinateVector(observations);

    //PT or PTZ?
    if(get<3>(solution))
    {
        //If we are here, we already have a valid focal length estimate

        //Normalise the coordinates
        double scale=1.0/trackerState.focalLength;
        for_each(coordinates, [&](Coordinate2DT& current){current*=scale;});

        bool flagPropagated=AssertGenerator<NodalGeometryEstimatorProblemT>(solution, coordinates, parameters.imageNoiseVariance*pow<2>(scale));

        //If the previous generator is propagated
        if(flagPropagated)
        {
            //Multiply the pose with the intrinsics
            IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity(); mK(0,0)=trackerState.focalLength; mK(1,1)=mK(0,0);
            get<0>(solution) = mK*get<0>(solution);
        }
    }
    else
        AssertGenerator<NodalZoomGeometryEstimatorProblemT>(solution, coordinates, parameters.imageNoiseVariance);

}   //void AssertGenerator(tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>& currentSolution, const vector<ImageFeatureT>& observations)

/**
 * @brief Attempt to replace the best solution with one created from the current generator in the state
 * @tparam GeometryEstimatorProblemT Geometry estimation pipeline problem used for the current solution
 * @param[in,out] solution Current best solution. If the solution based on the landmarks of the previous generator is reasonable, updated with it
 * @param[in] observations Image observations
 * @param[in] imageNoise Image noise variance
 * @return \c true if the current solution is replaced by the generators of the previous solution
 * @pre \c GeometryEstimationProblemT is a model of \c GeometryEstimatorProblemConceptC
 * @remarks Switching landmarks at each iteration causes a jitter at the tracker. So, unless there is a substantial improvement, it is better to stick to the same landmarks
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
template<class GeometryEstimatorProblemT>
bool NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::AssertGenerator(tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>& solution, const vector<Coordinate2DT>& observations, double imageNoise)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT( (GeometryEstimatorProblemConceptC<GeometryEstimatorProblemT>) );

    //Decompose
    CameraMatrixT mP;
    GeometryEstimationPipelineDiagnosticsC geDiagnostics;
    CorrespondenceListT correspondences;
    tie(mP, geDiagnostics, correspondences, ignore) = solution;

    //Compute the valid generator size
   typedef typename GeometryEstimatorProblemT::ransac_problem_type2::minimal_solver_type SolverT;
   size_t sGenerator = SolverT::GeneratorSize(); //For simplicity, this ignores the possibility of a non-minimal LO solver, or a first and second stage solvers with different generator sizes

   //Search for the generator among the inliers
   //It is a list_of, so linear search...

   CoordinateCorrespondenceList32DT generatorCoordinates;

   for(const auto current : trackerState.generator)
   {
       auto it=find_if(correspondences, [&](const typename CorrespondenceListT::value_type& item){ return item.left==current;} );

       //Search successful
       if(it!=correspondences.end() )
          generatorCoordinates.push_back(typename CoordinateCorrespondenceList32DT::value_type(world[it->left].Coordinate(), observations[it->right], it->info));

       //Clip the generator to the minimal. Basically, elements of a non-minimal generator should also work well for a minimal generator, unless the partial set happens to be degenerate.
       if(generatorCoordinates.size()==sGenerator)
           break;
   }    //for(const auto current : trackerState.generator)

   //This happens if the state does not have a generator, or some generator elements are not identified in the correspondence set
   if(generatorCoordinates.size()!=sGenerator)
   {
        //Accept the current solution, update the generator, and return
        tie(trackerState.generator, ignore) = BimapToVector<size_t, size_t>(geDiagnostics.generator);
        return false;
   }    //if(generatorCoordinates.size()!=sGenerator)

   //Solve for the camera matrix with the generator
   SolverT solver;
   list<CameraMatrixT> generatorSolutions=solver(generatorCoordinates);

   if(generatorSolutions.empty())
   {
       //Accept the current solution, update the generator, and return
       tie(trackerState.generator, ignore) = BimapToVector<size_t, size_t>(geDiagnostics.generator);
       return false;
   }    //if(solutionSet.empty())

   //Find the best generator solution- largest inlier count

   vector<Coordinate3DT> worldCoordinates=MakeCoordinateVector(world);

   double inlierTh= SolverT::error_type::ComputeOutlierThreshold(parameters.inlierRejectionProbability, imageNoise);
   CoordinateCorrespondenceList32DT bestInliersC;
   CorrespondenceListT bestInliersI;
   CameraMatrixT mPBest;
   for(const auto& current : generatorSolutions)
   {
       typename SolverT::constraint_type constraint( typename SolverT::error_type(current), inlierTh);

       CorrespondenceListT currentInliersI;
       for(const auto& currentCorrespondence : correspondences)
           if(constraint(worldCoordinates[currentCorrespondence.left], observations[currentCorrespondence.right]))
               currentInliersI.push_back(currentCorrespondence);

       //Update
       if(currentInliersI.size() > bestInliersI.size())
       {
           mPBest=current;
           bestInliersI.swap(currentInliersI);
           bestInliersC=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList32DT>(bestInliersI, worldCoordinates, observations);
       }    //if(currentInliersI.size() > bestInliersI.size())
   }    //for(const auto& current : generatorSolutions)

    //Check against the solution with the new generator
    //Since we are checking against the inliers of the current solution, the resulting set will be smaller!
    if( (double)bestInliersC.size()/correspondences.size() < parameters.generatorUpdateThreshold )
    {
       tie(trackerState.generator, ignore) = BimapToVector<size_t, size_t>(geDiagnostics.generator);
       return false;
    }    //if( (double)bestInliersC.size()/correspondences.size() < parameters.generatorUpdateThreshold )

    //If the result is not substantially worse than the current solution, refine and replace
    typedef typename GeometryEstimatorProblemT::optimisation_problem_type PDLProblemT;
    PDLProblemT pdlProblem(mPBest, bestInliersC, SolverT(), typename SolverT::error_type(), optional<MatrixXd>());
    CameraMatrixT refinedSolution;
    optional<MatrixXd> dummy;
    PowellDogLegDiagnosticsC pdlDiagnostics= PowellDogLegC<PDLProblemT>::Run(refinedSolution, dummy, pdlProblem, parameters.pipelineParameters.geometryEstimatorParameters.pdlParameters);

    get<0>(solution)=refinedSolution;
    get<2>(solution).swap(bestInliersI);

    return true;
}   //void AssertGenerator(tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>& bestSolution)

/**
 * @brief Sets a new tracker mode if necessary
 * @param[in] focalLengthEstimate Focal length
 * @param[in] correspondences Correspondences
 * @param[in] currentMode Current tracker mode
 * @param[in] flagRelocalisation \c true if there was a relocalisation event
 * @return New tracker mode
 * @pre \c flagInitialised=true
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
auto  NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::SetTrackerMode(double focalLengthEstimate, const CorrespondenceListT& correspondences, TrackerModeT currentMode, bool flagRelocalisation) const -> TrackerModeT
{
	//Preconditions
	assert(flagInitialised);

	if(parameters.flagFixedFocalLength)
		return TrackerModeT::NODAL;

	//If relocalisation triggered and zoom is not fixed, release the zoom
	if(!parameters.flagFixedFocalLength && flagRelocalisation)
		return TrackerModeT::NODAL_ZOOM;

	//Check support loss
	if(currentMode==TrackerModeT::NODAL)
	{
		double deltaSupport= (double)correspondences.size()/record.sSupport-1.0;

		if(deltaSupport < -parameters.supportLossModeSwitch)
			return TrackerModeT::NODAL_ZOOM;
	}	//if(currentMode==TrackerModeT::NODAL)

	//Check whether the focal length stabilised
	if(currentMode==TrackerModeT::NODAL_ZOOM)
		if(flagInitialised)
		{
			double deltaF=1-focalLengthEstimate/trackerState.focalLength;

			if(fabs(deltaF)<parameters.focalLengthSensitivity)
				return TrackerModeT::NODAL;
		}	//if(focalLength)

	return currentMode;
}	//TrackerModeT SetTrackerMode(const CameraMatrixT& estimate, const CorrespondenceListT& correspondences)

/**
 * @brief Updates the state
 * @param[in] measurement Measurement
 * @post \c trackerState is modified
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
void NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::UpdateState(const CameraMatrixT& measurement)
{
	RotationMatrix3DT mR;
	IntrinsicCalibrationMatrixT mK;
	std::tie(mK, ignore, mR)=DecomposeCamera(measurement);

	QuaternionT q(mR);

	if(flagInitialised)
	{
		if(trackerState.trackingMode==TrackerModeT::NODAL)
			trackerState.zoomRate=0;
		else
		{
			double zoomRateMeasurement=mK(0,0) - trackerState.focalLength;
			trackerState.zoomRate = (1-parameters.zoomMeasurementWeight)*trackerState.zoomRate + parameters.zoomMeasurementWeight*zoomRateMeasurement;
		}

		QuaternionT deltaQ= q * trackerState.orientation.conjugate();
		RotationVectorT rotationMeasurement = QuaternionToRotationVector(deltaQ);
		trackerState.rotation = (1-parameters.rotationMeasurementWeight)*trackerState.rotation + parameters.rotationMeasurementWeight * rotationMeasurement;	//In this context, each element represents the rate of rotation around the corresponding axis
	}	//if(trackerState)

	trackerState.orientation=q;
	trackerState.focalLength=max(min(mK(0,0), parameters.maxFocalLength), parameters.minFocalLength);
}	//void UpdateState(const CameraMatrixT& measurement)

/**
 * @brief Predicts the next measurement
 * @return Predicted state
 * @pre \c flagInitialised=true
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
auto NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::Predict() const -> TrackerStateC
{
	//Preconditions
	assert(flagInitialised);
	return PropagateState(trackerState);
}	//optional<CameraMatrixT> Predict() const

/**
 * @brief Updates the tracker state with the observation
 * @param[in,out] rng Random number generator
 * @param[in] observations Image features. Temporarily modified during processing
 * @return Diagnostics
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
NodalCameraTrackingPipelineDiagnosticsC NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::Update(RNGT& rng, vector<ImageFeatureT>& observations)
{
	//TODO What happens if the first frame fails?
	NodalCameraTrackingPipelineDiagnosticsC diagnostics;
	record.nRegistration=0;

	TrackerModeT trackingMode=trackerState.trackingMode;

	//Prediction, if there is a valid state
	optional<TrackerStateC> prediction;
	optional<vector<size_t> > visibleLandmarks;
	if(flagInitialised)
	{
		//Prediction
		prediction=Predict();
		visibleLandmarks=ApplyVisibilityFilter(prediction->orientation.matrix(), prediction->focalLength);
	}	//if(flagInitialised)

	//Registration
	map< size_t, tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool >, greater<size_t> > solutions;
	auto solutionInserter=[&](const CameraMatrixT& mP, const GeometryEstimationPipelineDiagnosticsC& solDiagnostics, const CorrespondenceListT& inliers, bool flagNodal){ if(solDiagnostics.flagSuccess && inliers.size()>parameters.minSupport) solutions.emplace(inliers.size(), make_tuple(mP, solDiagnostics, inliers, flagNodal));  };
	GeometryEstimationPipelineDiagnosticsC geDiagnostics;	//Keeps the diagnostics for the last call

	bool flagRelocalisation=false;

	//Logic:
	//NODAL
		// P2P with initial estimate
		// If fails and fixed focal length, P2P without initial estimate
		// If fails and variable focal length, NODAL_ZOOM
	//NODAL_ZOOM
		//P2Pf with initial estimate
		//If fails, P2Pf without initial estimate

	//Initial estimate
	optional<CameraMatrixT> initialEstimate;
	if(flagInitialised)
	{
		initialEstimate=CameraMatrixT::Zero();
		initialEstimate->leftCols(3)=prediction->orientation.matrix();
	}	//if(flagInitialised)

	//Nodal
	optional<MatchingProblemT> matchingProblem;	// Holds the feature similarity matrix
	if(trackingMode==TrackerModeT::NODAL)
	{
		//If we are here, there must be a valid focal length estimate
		double focalLength = prediction ? prediction->focalLength : trackerState.focalLength;

		IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity(); mK(0,0)=focalLength; mK(1,1)=mK(0,0);

		//Normalise
		auto scalingFunctor=[&](double scale){for_each(observations, [&](ImageFeatureT& current){current.Coordinate()*=scale;});
												parameters.imageNoiseVariance*=pow<2>(scale);};

		scalingFunctor(1.0/focalLength);

		//Estimate
		CameraMatrixT orientationEstimate;
		CorrespondenceListT inliers;
		std::tie(geDiagnostics, orientationEstimate, inliers)=RegisterCamera<NodalGeometryEstimatorProblemT>(rng, matchingProblem, observations, initialEstimate, visibleLandmarks);

		solutionInserter(mK*orientationEstimate, geDiagnostics, inliers, true);

		//Relocalise?
		flagRelocalisation=!geDiagnostics.flagSuccess || DetectTrackerFailure(diagnostics, mK*orientationEstimate, inliers);

		//If fixed focal length, relocalise with the nodal solver
		if(flagRelocalisation && parameters.flagFixedFocalLength)
		{
			std::tie(geDiagnostics, orientationEstimate, inliers)=RegisterCamera<NodalGeometryEstimatorProblemT>(rng, matchingProblem, observations, optional<CameraMatrixT>(), optional<vector<size_t> >());
			solutionInserter(mK*orientationEstimate, geDiagnostics, inliers, true);
		}

		if(!parameters.flagFixedFocalLength)
		{
			//If there is a large support loss, try the zoom solver
			trackingMode=SetTrackerMode(focalLength, inliers, trackingMode, flagRelocalisation);

			//If there is a mode change, reset the matching problem
			if(trackingMode==TrackerModeT::NODAL_ZOOM)
			{
				matchingProblem=optional<MatchingProblemT>();	//Clear the matching problem for the nodal-zoom registration attempt
																	//The denormalisation does not affect the similarity scores, so, in theory, a complete reset is wasteful
																	//But experiments suggest that denormalising the coordinates is slightly slower.
			}
		}

		//Denormalise
		scalingFunctor(focalLength);
	}	//if(trackerState.trackingMode==TrackerModeT::NODAL)

	//Nodal and zoom
    if(trackingMode==TrackerModeT::NODAL_ZOOM)
	{
		if(initialEstimate)
		{
			IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity(); mK(0,0)=prediction->focalLength; mK(1,1)=mK(0,0);
			initialEstimate= mK * (*initialEstimate);
		}

		CameraMatrixT calibrationEstimate;
		CorrespondenceListT inliers;
		std::tie(geDiagnostics, calibrationEstimate, inliers)=RegisterCamera<NodalZoomGeometryEstimatorProblemT>(rng, matchingProblem, observations, initialEstimate, visibleLandmarks);
		solutionInserter(calibrationEstimate, geDiagnostics, inliers, false);

		//Trigger relocalisation?
		//Conditions:
			//The best solution in the set warrants a relocalisation. This implies that a NODAL->NODAL_ZOOM promotion cannot trigger a relocalisation call
			//Or the solution set is empty
		flagRelocalisation = solutions.empty() || DetectTrackerFailure(diagnostics, get<0>(solutions.begin()->second), get<2>(solutions.begin()->second));
	}	//if(currentMode==TrackerModeT::NODAL_ZOOM)

	if(flagRelocalisation && !parameters.flagFixedFocalLength)
	{
		CameraMatrixT calibrationEstimate;
		CorrespondenceListT inliers;
		std::tie(geDiagnostics, calibrationEstimate, inliers)=RegisterCamera<NodalZoomGeometryEstimatorProblemT>(rng, matchingProblem, observations, optional<CameraMatrixT>(), optional<vector<size_t> >());
		solutionInserter(calibrationEstimate, geDiagnostics, inliers, false);

		trackingMode=TrackerModeT::NODAL_ZOOM;	//If relocalisation fails, the mode remains as NODAL_ZOOM. Else, updated by SetTrackerMode
	}	//if(flagRelocalisation && !parameters.flagFixedFocalLength)


	//Update
	if(solutions.empty())
	{
		diagnostics.geDiagnostics=geDiagnostics;
		flagInitialised=false;	//Relocalisation has failed. Lost!
	}
	else
	{
        //Attempt to replace the best solution with a solution from the current generator in the state
        //This is for stabilising the tracker: Switching generators cause jitter
	    AssertGenerator(solutions.begin()->second, observations);

		//Get the best solution
		CameraMatrixT bestEstimate;
		CorrespondenceListT bestInliers;
		std::tie(bestEstimate, diagnostics.geDiagnostics, bestInliers, diagnostics.flagNodal)=solutions.begin()->second;

		//Backup
		TrackerStateC prevState(trackerState);

		//Update the state
		UpdateState(bestEstimate);

		//Toggle mode
		trackerState.trackingMode=SetTrackerMode(prevState.focalLength, bestInliers, trackingMode, flagRelocalisation);

		record.sSupport=bestInliers.size();

		diagnostics.flagSuccess=true;
		diagnostics.nInliers=bestInliers.size();
		diagnostics.supportSet.swap(bestInliers);
		flagInitialised=true;
	}	//if(solutions.empty())

	diagnostics.flagRelocalise=flagRelocalisation;
	diagnostics.nRegistration=record.nRegistration;

	return diagnostics;
}	//NodalCameraTrackingPipelineDiagnosticsC Update(const vector<ImageFeatureT>& observations)

/**
 * @brief Returns the state
 * @return Returns the state
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
auto NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::GetState() const -> const TrackerStateC&
{
	return trackerState;
}	//const optional<CameraMatrixT>&GetState()

/**
 * @brief Propagates a given state object
 * @param[in] src State to be propagated
 * @return Propagated state
 * @remarks The function does not change the state of the tracker
 */
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT>
auto NodalCameraTrackingPipelineC<NodalGeometryEstimatorProblemT, NodalZoomGeometryEstimatorProblemT, MatchingProblemT>::PropagateState(const TrackerStateC& src) const -> TrackerStateC
{
	TrackerStateC propagated(src);

	if(propagated.trackingMode==TrackerModeT::NODAL_ZOOM)
		propagated.focalLength = max(min(src.focalLength + src.zoomRate, parameters.maxFocalLength), parameters.minFocalLength);

	propagated.orientation = RotationVectorToQuaternion(src.rotation) * src.orientation;

	return propagated;
}	//TrackerStateC PropagateState(const TrackerStateC& src)

}	//namespace GeometryN
}	//namespace SeeSawN





#endif /* NODAL_CAMERA_TRACKING_PIPELINE_IPP_6581009 */
