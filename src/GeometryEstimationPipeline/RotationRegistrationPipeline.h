/**
 * @file RotationRegistrationPipeline.h Public interface for the relative rotation registration pipeline
 * @author Evren Imre
 * @date 29 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef ROTATION_REGISTRATION_PIPELINE_H_1806482
#define ROTATION_REGISTRATION_PIPELINE_H_1806482

#include "RotationRegistrationPipeline.ipp"

namespace SeeSawN
{
namespace GeometryN
{

struct RotationRegistrationPipelineParametersC;	///< Parameter object for the rotation registration pipeline
struct RotationRegistrationPipelineDiagnosticsC;	///< Diagnostics object for the rotation registration pipeline
class RotationRegistrationPipelineC;	///< Pipeline for registration of relative rotations to an absolute rotation model
}	//GeometryN
}	//SeeSawN


#endif /* ROTATION_REGISTRATION_PIPELINE_H_1806482 */
