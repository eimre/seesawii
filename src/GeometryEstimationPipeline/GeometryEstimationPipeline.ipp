/**
 * @file GeometryEstimationPipeline.ipp Implementation of GeometryEstimationPipelineC
 * @author Evren Imre
 * @date 17 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_ESTIMATION_PIPELINE_IPP_0343123
#define GEOMETRY_ESTIMATION_PIPELINE_IPP_0343123

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/algorithm/transform.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <vector>
#include <list>
#include <climits>
#include <cmath>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <type_traits>
#include <string>
#include <tuple>
#include <map>
#include <iostream>
#include "GeometryEstimationPipelineProblemConcept.h"
#include "GeometryEstimator.h"
#include "GeometryEstimationPipelineProblemConcept.ipp"
#include "../Geometry/CoordinateStatistics.h"
#include "../Geometry/GeometrySolverConcept.h"
#include "../Elements/Correspondence.h"
#include "../Elements/CorrespondenceDecimator.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Wrappers/BoostBimap.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../RANSAC/RANSACGeometryEstimationProblemConcept.h"
#include "../Numeric/Combinatorics.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::transform;
using boost::ForwardRangeConcept;
using boost::lexical_cast;
using Eigen::MatrixXd;
using std::vector;
using std::list;
using std::multimap;
using std::numeric_limits;
using std::fabs;
using std::invalid_argument;
using std::copy_n;
using std::back_inserter;
using std::next;
using std::min;
using std::max;
using std::tie;
using std::ceil;
using std::floor;
using std::string;
using std::is_convertible;
using std::tuple;
using std::make_tuple;
using std::cout;
using SeeSawN::GeometryN::GeometryEstimationPipelineProblemConceptC;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::SortByAmbiguity;
using SeeSawN::ElementsN::CorrespondenceDecimatorC;
using SeeSawN::ElementsN::CorrespondenceDecimatorParametersC;
using SeeSawN::ElementsN::DecimatorQuantisationStrategyT;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GeometryEstimatorDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimatorParametersC;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::GeometryN::CoordinateStatisticsT;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationParametersC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemConceptC;

/**
 * @brief Parameters for GeometryEstimationPipelineC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct GeometryEstimationPipelineParametersC
{
	unsigned int nThreads;	///< Number of threads available to the pipeline

	unsigned int maxIteration;	///< Maximum number of iterations
	double minRelativeDifference;	///< If the relative difference in error between two successive iterations is below this value, the algorithm is terminated. >=0

	FeatureMatcherParametersC matcherParameters;	///< Parameters for the matcher
	GeometryEstimatorParametersC geometryEstimatorParameters;	///< Parameters for the geometry estimator
	ScaledUnscentedTransformationParametersC sutParameters;	///< Parameters for SUT

	//Decimation
	double binDensity1;	///< Number of bins per standard deviation in the first domain. >0
	double binDensity2;	///< Number of bins per standard deviation in the second domain. >0
	unsigned int maxObservationDensity;	///< Maximum number of observations per bin.
	double minObservationRatio;	///< Minimum ratio of the size of the observation set to that of the generator. >=1
	double maxObservationRatio;	///< Maximum ratio of the size of the observation set to that of the generator. >=1
	double validationRatio;	///< Ratio of the size of the validation set to that of the generator. >=0
	double maxAmbiguity;	///< For the first guided matching iteration, maximum ambiguity for a member of the decimated observation set. Ignored when an initial matching criterion is available. [0,1]

	double refreshThreshold;	///< If the ratio of the number of inliers for the current solution to that of the reference set is above this value, refresh the reference and the validation sets >=1

	bool flagCovariance;	///< If \c false , covariance estimation is skipped

	bool flagVerbose;	///< If \c true diagnostics messages are printed
	GeometryEstimationPipelineParametersC() : nThreads(1), maxIteration(10), minRelativeDifference(0.01), binDensity1(4), binDensity2(4), maxObservationDensity(3), minObservationRatio(14), maxObservationRatio(numeric_limits<double>::infinity()), validationRatio(14), maxAmbiguity(0.85), refreshThreshold(1.1), flagCovariance(true), flagVerbose(false)
	{}
};	//struct GeometryEstimationPipelineParametersC

/**
 * @brief Diagnostics for GeometryEstimationPipelineC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct GeometryEstimationPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully
	double error;	///< A measure of the quality of the estimated model
	double meanError;	///< Error/ |referenceSet|
	unsigned int nIteration;	///< Number of iterations

	list<FeatureMatcherDiagnosticsC> featureMatcherDiagnostics;	///< Diagnostics for the feature matcher at each iteration
	list<GeometryEstimatorDiagnosticsC> geometryEstimatorDiagnostics;	///< Diagnostics for the geometry estimator at each iteration

//	typedef tuple<double, vector<size_t> > SolutionT;	///< [Error; generator]
//	list<SolutionT> history;	///< Solutions proposed at each iteration
//	unsigned int iBest=0;	///< Index of the best solution

	CorrespondenceListT generator;  ///< Generator set for the solution

	optional<CorrespondenceListT> initialObservationSet;	///< The set of observations for the first pass (i.e. after the first call to the matcher). Decimated. Valid even if the operation fails, as long as the initial call to the matcher is successful
	GeometryEstimationPipelineDiagnosticsC() : flagSuccess(false), error(numeric_limits<double>::infinity()), meanError(numeric_limits<double>::infinity()),  nIteration(0)
	{}
};	//struct GeometryEstimationPipelineDiagnosticsC

/**
 * @brief Geometry estimation pipeline with guided matching
 * @tparam ProblemT A geometry estimation pipeline problem
 * @pre \c ProblemT is a geometry estimation pipeline problem
 * @pre \c ProblemT::matching_problem_type is a model of \c FeatureMatchingProblemConceptC
 * @pre \c ProblemT::ransac_problem_type2 is a model of \c RANSACGeometryEstimationProblemConceptC
 * @pre \c ProblemT::ransac_problem_type2::minimal_solver_type is a model of \c GeometrySolverConceptC
 * @remarks Pipeline:
 * 		- Establish putative correspondences (guided or unguided matching, depending on whether a model is available)
 * 		- Then loop:
 * 			- Assess ambiguity
 * 			- Estimate the model
 * 			- Update and termination
 * 			- Perform guided matching
 * 		- Estimate the covariance
 * 	@remarks Usage notes
 * 		- The initial match constraint should be more permissive, to compensate for the inaccuracies in the initial estimate
 * 		- In problems with low inlier ratio, a larger validation set is advisable
 * 		- Validation/Evaluation:
 * 			- Reference set: Decimated correspondences after the first call to the matcher
 * 			- Validation within an iteration: The validation set is constructed by regularly downsampling the reference set
 * 			- The size of the validation set is limited by that of the observation set
 * 			- Evaluation at the end of an iteration: The estimated model is evaluated against the reference set
 * 			- If there is a significant increase in the number of correspondences, the reference and the validation sets are refreshed
 * 	@remarks Too complicated for a unit test. Tested via the application.
 * @ingroup Algorithm
 * @nosubgrouping
 */
//TODO Update the reference set if there is a significant improvement in the inlier ratio?
template<class ProblemT>
class GeometryEstimationPipelineC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometryEstimationPipelineProblemConceptC<ProblemT>));
	BOOST_CONCEPT_ASSERT((GeometryEstimatorProblemConceptC<typename ProblemT::geometry_estimation_problem_type>));
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<typename ProblemT::matching_problem_type>));
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<typename ProblemT::geometry_estimation_problem_type::ransac_problem_type2>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename ProblemT::geometry_estimation_problem_type::ransac_problem_type2::minimal_solver_type>));
	//@endcond

	private:

		typedef typename ProblemT::geometry_estimation_problem_type GeometryProblemT;	///< A geometry estimation problem
		typedef GeometryEstimatorC<GeometryProblemT> GeometryEstimatorT;	///< A geometry estimator
		typedef typename GeometryEstimatorT::rng_type RNGT;	///< Random number generator

		typedef typename GeometryProblemT::ransac_problem_type2::minimal_solver_type MainSolverT;	///< Type of the main solver for the operation
		typedef typename MainSolverT::model_type ModelT;	///< Type of the geometric model
		typedef typename MainSolverT::coordinate_type1 Coordinate1T;	///< Type of the first coordinate
		typedef typename MainSolverT::coordinate_type2 Coordinate2T;	///< Type of the second coordinate

		typedef typename ProblemT::matching_problem_type MatchingProblemT;	///< Type of the matching problem

		/** @name Implementation details */ //@{
		static void ValidateParameters(GeometryEstimationPipelineParametersC& parameters);	///< Validates the parameters
		template<class DimensionT, class CoordinateT, class FeatureRangeT> static tuple<vector<CoordinateT>, DimensionT> PreprocessFeatureSet(const FeatureRangeT& featureSet, double density);	///< Preprocesses a feature set

		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CoordinateCorrespondenceContainerT;	///< A container for corresponding coordinate pairs
		typedef CorrespondenceDecimatorC<Coordinate1T, Coordinate2T> CorrespondenceDecimatorT;	///< Type of the correspondence decimator
		static bool DecimateCorrespondences(CoordinateCorrespondenceContainerT& validationSet, CoordinateCorrespondenceContainerT& observationSet, vector<size_t>& indexMap, const ProblemT& problem, const CoordinateCorrespondenceContainerT& correspondences, const typename CorrespondenceDecimatorT::dimension_type1& binSize1, const typename CorrespondenceDecimatorT::dimension_type2& binSize2, DecimatorQuantisationStrategyT quantisationStrategy, double maxAmbiguity, const GeometryEstimationPipelineParametersC& parameters);	///< Decimates the correspondences to form the observation and the validation sets

		static optional<MatrixXd> ComputeCovarianceWrapper(const ProblemT& problem, const ModelT& model, const CorrespondenceListT& generator, const CorrespondenceListT& inlierObservations, const vector<Coordinate1T>& coordinates1, const vector<Coordinate2T>& coordinates2, const GeometryEstimationPipelineParametersC& parameters, bool flagLO, bool flagStage2);	///< Wrapper for ComputeCovariance, with automatic type selection
		template<class GeneratorCovarianceT, class SUTSolverT> static optional<MatrixXd> ComputeCovariance(const CoordinateCorrespondenceContainerT& generator, const GeneratorCovarianceT& generatorCovariance, const CoordinateCorrespondenceContainerT& validator, const SUTSolverT& solver, const typename SUTSolverT::error_type& errorMetric, const GeometryEstimationPipelineParametersC& parameters);	///< Computes the estimate covariance
		//@}

	public:

		typedef RNGT rng_type;	///< Random number generator type
		typedef ModelT model_type;	///< Model type

		static GeometryEstimationPipelineDiagnosticsC Run(ModelT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, ProblemT& problem, RNGT& rng, GeometryEstimationPipelineParametersC parameters);	///< Solves a geometry estimation problem
		static GeometryEstimationPipelineDiagnosticsC Run(ModelT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, ProblemT& problem, optional<MatchingProblemT>& matchingProblem, RNGT& rng, GeometryEstimationPipelineParametersC parameters);	///< Solves a geometry estimation problem
};	//class GeometryEstimationPipelineC

/**
 * @brief Validates the parameters
 * @param[in,out] parameters Parameter set
 * @throws invalid_argument If there are any invalid parameters
 */
template<class ProblemT>
void GeometryEstimationPipelineC<ProblemT>::ValidateParameters(GeometryEstimationPipelineParametersC& parameters)
{
	if(parameters.minRelativeDifference<0)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : minRelativeDifference must be non-negative. Value:")+lexical_cast<string>(parameters.minRelativeDifference)));

	if(parameters.binDensity1<=0)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : binDensity1 must be positive. Value:")+lexical_cast<string>(parameters.binDensity1)));

	if(parameters.binDensity2<=0)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : binDensity2 must be positive. Value:")+lexical_cast<string>(parameters.binDensity2)));

	if(parameters.minObservationRatio<1)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : minObservationRatio cannot be <1. Value:")+lexical_cast<string>(parameters.minObservationRatio)));

	if(parameters.maxObservationRatio<1)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : maxObservationRatio cannot be <1. Value:")+lexical_cast<string>(parameters.maxObservationRatio)));

	if(parameters.validationRatio<1)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : validationRatio cannot be <1. Value:")+lexical_cast<string>(parameters.validationRatio)));

	if(parameters.maxAmbiguity<0 || parameters.maxAmbiguity>1)
		throw(invalid_argument(string("GeometryEstimationPipelineC : ValidateParameters : maxAmbiguity must be in [0,1]. Value:")+lexical_cast<string>(parameters.maxAmbiguity)));

	parameters.matcherParameters.flagStatic=false;
	parameters.matcherParameters.nThreads=parameters.nThreads;
	parameters.geometryEstimatorParameters.ransacParameters.nThreads=parameters.nThreads;
	parameters.geometryEstimatorParameters.ransacParameters.parameters1.flagHistory=false;
	parameters.geometryEstimatorParameters.ransacParameters.parameters2.flagHistory=false;
	parameters.sutParameters.nThreads=parameters.nThreads;
}	//void ValidateParameters(GeometryEstimationPipelineParametersC& parameters)

/**
 * @brief Preprocesses a feature set
 * @tparam DimensionT Bin dimension type
 * @tparam CoordinateT A coordinate
 * @tparam FeatureRangeT A feature range
 * @param[in] featureSet Feature set
 * @param[in] density Number of bins per standard-deviation
 * @return A 2-tuple: Feature coordinates, bin dimensions
 */
template<class ProblemT>
template<class DimensionT, class CoordinateT, class FeatureRangeT>
auto GeometryEstimationPipelineC<ProblemT>::PreprocessFeatureSet(const FeatureRangeT& featureSet, double density) -> tuple<vector<CoordinateT>, DimensionT>
{
	//Preconditions
	typedef CoordinateStatisticsT<CoordinateT> StatisticsT;
	static_assert(is_convertible<typename StatisticsT::ArrayD, DimensionT>::value, "GeometryEstimationPipeline::PreprocessFeatureSet : CoordinateStatisticsT<CoordinateT>::ArrayD is not convertible to DimensionT ");

	tuple<vector<CoordinateT>, DimensionT > output;

	get<0>(output)=MakeCoordinateVector(featureSet);

	typename StatisticsT::ArrayD meanC;
	typename StatisticsT::ArrayD stdC;
	tie(meanC, stdC)=*StatisticsT::ComputeMeanAndStD(get<0>(output));	//Guaranteed to be non-empty
	get<1>(output)=stdC/density;

	return output;
}	//auto PreprocessFeatureSet(const FeatureRangeT& featureSet, double density) -> tuple<vector<CoordinateT>, DimensionT>

/**
 * @brief Computes the estimate covariance
 * @tparam GeneratorCovarianceT Type of the generator covariance matrix
 * @tparam SUTSolverT Geometry estimator to be used for the covariance estimation
 * @param generator Model generator
 * @param generatorCovariance Generator covariance matrix
 * @param validator SUT validators
 * @param solver Solver
 * @param errorMetric Error metric
 * @param parameters Parameters
 * @return Covariance matrix. Invalid if SUT fails
 */
template<class ProblemT>
template<class GeneratorCovarianceT, class SUTSolverT>
optional<MatrixXd> GeometryEstimationPipelineC<ProblemT>::ComputeCovariance(const CoordinateCorrespondenceContainerT& generator, const GeneratorCovarianceT& generatorCovariance, const CoordinateCorrespondenceContainerT& validator, const SUTSolverT& solver, const typename SUTSolverT::error_type& errorMetric, const GeometryEstimationPipelineParametersC& parameters)
{
	optional<MatrixXd> output;

	typedef SUTGeometryEstimationProblemC<SUTSolverT> SUTProblemT;
	SUTProblemT sutProblem(generator, generatorCovariance, solver, validator, errorMetric);
	typename ScaledUnscentedTransformationC<SUTProblemT>::result_type sutOutput=ScaledUnscentedTransformationC<SUTProblemT>::Run(sutProblem, parameters.sutParameters);

	if(sutOutput)
		output=sutOutput->GetCovariance();

	return output;
}	//optional<MatrixXd> ComputeCovariance(const CoordinateCorrespondenceContainerT& generator, const CoordinateCorrespondenceContainerT& validator, const SUTSolverT& solver, const SUTErrorMetricT& errorMetric, const GeometryEstimationPipelineParametersC& parameters)

/**
 * @brief Wrapper for ComputeCovariance, with automatic type selection
 * @param[in] problem Problem
 * @param[in] model Estimated model
 * @param[in] generator Generator set
 * @param[in] inlierObservations Inlier observations
 * @param[in] coordinates1 Feature coordinates for the first set
 * @param[in] coordinates2 Feature coordinates for the second set
 * @param[in] parameters Parameters
 * @param[in] flagLO \c true , if \c model is generated by a LO solver
 * @param[in] flagStage2 \c true , if \c model is generated by the second stage of RANSAC
 * @return Covariance matrix. Invalid if SUT fails
 */
template<class ProblemT>
optional<MatrixXd> GeometryEstimationPipelineC<ProblemT>::ComputeCovarianceWrapper(const ProblemT& problem, const ModelT& model, const CorrespondenceListT& generator, const CorrespondenceListT& inlierObservations, const vector<Coordinate1T>& coordinates1, const vector<Coordinate2T>& coordinates2, const GeometryEstimationPipelineParametersC& parameters, bool flagLO, bool flagStage2)
{
	//Generator coordinates
	CoordinateCorrespondenceContainerT generatorCoordinates=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceContainerT>(generator, coordinates1, coordinates2);

	//SUT model validator coordinates
	CoordinateCorrespondenceContainerT sutValidatorCoordinates;
	size_t sGenerator=generator.size();
	size_t nInliers=inlierObservations.size();

	if(sGenerator<nInliers)
	{
		//Generator, in set form
		typedef typename ValueTypeM<Coordinate1T>::type RealT;
		set<tuple<RealT, RealT> > generatorSet;
		for(const auto& current : generator)
			generatorSet.emplace(current.left, current.right);

		size_t nSUTValidators=min(sGenerator, nInliers-sGenerator);
		auto itE=generatorSet.end();
		size_t index=0;
		for(const auto& current : inlierObservations)
		{
			//If not in the generator set, add to the validators
			if(generatorSet.find(make_tuple(current.left, current.right))==itE)
			{
				sutValidatorCoordinates.push_back(typename CoordinateCorrespondenceContainerT::value_type(coordinates1[current.left], coordinates2[current.right]));
				++index;

				if(index==nSUTValidators)
					break;
			}	//if(generatorSet.find(make_tuple(current.left, current.right))==itE)
		}	//for(const auto& current : inliers)
	}	//if(sGenerator<nInliers)

	//Select the correct solver
	const typename GeometryProblemT::ransac_problem_type1& p1=const_cast<ProblemT&>(problem).GeometryEstimationProblem().RANSACProblem1();
	const typename GeometryProblemT::ransac_problem_type2& p2=const_cast<ProblemT&>(problem).GeometryEstimationProblem().RANSACProblem2();

	if(flagStage2 && flagLO)
		return ComputeCovariance(generatorCoordinates, problem.MakeGeneratorCovariance(generator), sutValidatorCoordinates, p2.GetLOSolver(), p2.GetConstraint().GetErrorMetric(), parameters);

	if(!flagStage2 && !flagLO)
		return ComputeCovariance(generatorCoordinates, problem.MakeGeneratorCovariance(generator), sutValidatorCoordinates, p1.GetMinimalSolver(), p1.GetConstraint().GetErrorMetric(), parameters);

	if(!flagStage2 && flagLO)
		return ComputeCovariance(generatorCoordinates, problem.MakeGeneratorCovariance(generator), sutValidatorCoordinates, p1.GetLOSolver(), p1.GetConstraint().GetErrorMetric(), parameters);

	//Default: Stage 2, no LO
	return ComputeCovariance(generatorCoordinates, problem.MakeGeneratorCovariance(generator), sutValidatorCoordinates, p2.GetMinimalSolver(), p2.GetConstraint().GetErrorMetric(), parameters);
}	//optional<MatrixXd> ComputeCovarianceWrapper(const ProblemT& problem, const ModelT& model, const CorrespondenceListT& generator, const CorrespondenceListT& inlierObservations, const vector<Coordinate1T>& coordinates1, const vector<Coordinate2T>& coordinates2, const GeometryEstimationPipelineParametersC& parameters, bool flagLO, bool flagStage2)

/**
 * @brief Decimates the correspondences to form the observation and the validation sets
 * @param[in,out] validationSet Validation set. Modified only if empty
 * @param[in,out] observationSet Observation set
 * @param[in,out] indexMap Indices of the observations in the full, undecimated set
 * @param[in] problem Problem
 * @param[in] correspondences Correspondence set to be decimated
 * @param[in] binSize1 Bin size for the first set
 * @param[in] binSize2 Bin size for the second set
 * @param[in] quantisationStrategy Quantisation type
 * @param[in] maxAmbiguity Maximum ambiguity for a member of the decimated set
 * @param[in] parameters Parameters
 * @returns \c false if the minimum observation or minimum validation conditions are not met
 * @remarks \c validationSet.empty() triggers a rebuilding of \c validationSet
 * @remarks Even if the operation fails, the output arrays are valid
 */
template<class ProblemT>
bool GeometryEstimationPipelineC<ProblemT>::DecimateCorrespondences(CoordinateCorrespondenceContainerT& validationSet, CoordinateCorrespondenceContainerT& observationSet, vector<size_t>& indexMap, const ProblemT& problem, const CoordinateCorrespondenceContainerT& correspondences, const typename CorrespondenceDecimatorT::dimension_type1& binSize1, const typename CorrespondenceDecimatorT::dimension_type2& binSize2, DecimatorQuantisationStrategyT quantisationStrategy, double maxAmbiguity, const GeometryEstimationPipelineParametersC& parameters)
{
	unsigned int sGenerator=problem.GetMinimalGeneratorSize();
	unsigned int minObservation=min(floor(sGenerator*max((double)1.0, parameters.minObservationRatio)), (double)numeric_limits<unsigned int>::max());	//(unsigned int)floor(inf)=0!!!
	unsigned int maxObservation=min(floor(sGenerator*max((double)1.0, parameters.maxObservationRatio)), (double)numeric_limits<unsigned int>::max());	//(unsigned int)floor(inf)=0!!!
	unsigned int sValidation=max((unsigned int)1, (unsigned int)min(ceil(sGenerator*parameters.validationRatio), (double)numeric_limits<unsigned int>::max()) );

	CorrespondenceDecimatorParametersC decimatorParameters;
	decimatorParameters.quantisationStrategy=quantisationStrategy;
	decimatorParameters.maxDensity=parameters.maxObservationDensity;
	decimatorParameters.minElements=minObservation;
	decimatorParameters.maxElements=maxObservation;
	decimatorParameters.maxAmbiguity=maxAmbiguity;

	//Decimate the initial correspondence set
	tuple<CoordinateCorrespondenceContainerT, vector<size_t> > decimated=CorrespondenceDecimatorT::Run(correspondences, binSize1, binSize2, decimatorParameters);	//Decimate to obtain the observation set
	unsigned int sDecimated=get<0>(decimated).size();

	//Observation set
	observationSet.swap(get<0>(decimated));	//Observation set, as decimated correspondences
	indexMap.swap(get<1>(decimated));	//Indices into the original, undecimated set

	//Validation set
	if(validationSet.empty())
	{
		sValidation=min(sValidation, sDecimated);	//Validation set cannot be larger than the observation set
		vector<size_t> validationIndices(sValidation);	// Indices for the members of the validation set

		double step=(double)(sDecimated-1)/sValidation;
		double index=0;
		for(size_t c=0; c<sValidation; ++c, index+=step)
			validationIndices[c]=floor(index);

		validationSet=FilterBimap(observationSet, validationIndices);
	}	//if(validationSet.empty())

	return sDecimated>=minObservation;
}	//void DecimateCorrespondences(CoordinateCorrespondenceContainerT& validationSet, CoordinateCorrespondenceContainerT& observationSet, vector<size_t>& indexMap, const ProblemT& problem, const CoordinateCorrespondenceContainerT& correspondences, const typename CorrespondenceDecimatorT::dimension_type1& binSize1, const typename CorrespondenceDecimatorT::dimension_type2& binSize2, const GeometryEstimationPipelineParametersC& parameters)
/**
 * @brief Solves a geometry estimation problem
 * @param[out] model Estimated model
 * @param[out] covariance Model covariance. Invalid, if covariance estimation fails
 * @param[out] correspondences Indices of the corresponding features
 * @param[in,out] problem Geometry estimation problem
 * @param[in,out] rng Random number generation engine
 * @param[in] parameters Parameters. Passed by value on purpose.
 * @return Diagnostics object
 */
template<class ProblemT>
GeometryEstimationPipelineDiagnosticsC GeometryEstimationPipelineC<ProblemT>::Run(ModelT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, ProblemT& problem, RNGT& rng, GeometryEstimationPipelineParametersC parameters)
{
	optional<MatchingProblemT> matchingProblem;
	return Run(model, covariance, correspondences, problem, matchingProblem, rng, parameters);
}	//GeometryEstimationPipelineDiagnosticsC Run(ModelT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, ProblemT& problem, RNGT& rng, GeometryEstimationPipelineParametersC parameters)

/**
 * @brief Solves a geometry estimation problem
 * @param[out] model Estimated model
 * @param[out] covariance Model covariance. Invalid, if covariance estimation fails
 * @param[out] correspondences Indices of the corresponding features
 * @param[in,out] problem Geometry estimation problem
 * @param[in,out] matchingProblem Matching problem. If invalid, initialised within the function
 * @param[in,out] rng Random number generation engine
 * @param[in] parameters Parameters. Passed by value on purpose.
 * @return Diagnostics object
 * @remarks A valid \c matchingProblem conveys the similarity scores already computed elsewhere
 */
template<class ProblemT>
GeometryEstimationPipelineDiagnosticsC GeometryEstimationPipelineC<ProblemT>::Run(ModelT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, ProblemT& problem, optional<MatchingProblemT>& matchingProblem, RNGT& rng, GeometryEstimationPipelineParametersC parameters)
{
	//Validation
	ValidateParameters(parameters);	//Validate the input parameters
	if(!problem.IsValid())
		throw(invalid_argument("GeometryEstimationPipelineC::Run : Invalid problem."));

	//Initialisation
	GeometryEstimationPipelineDiagnosticsC diagnostics;	//Diagnostics
	correspondences.clear();

	//If the problem does not have any associated features, the operation fails
	if( boost::empty(problem.FeatureSet1()) || boost::empty(problem.FeatureSet2()) )
		return diagnostics;

	//Initialise the decimator parameters

	vector<Coordinate1T> coordinates1;
	typename CorrespondenceDecimatorT::dimension_type1 binDimensions1;
	tie(coordinates1, binDimensions1)=PreprocessFeatureSet<typename CorrespondenceDecimatorT::dimension_type1, Coordinate1T>(problem.FeatureSet1(), parameters.binDensity1);

	vector<Coordinate2T> coordinates2;
	typename CorrespondenceDecimatorT::dimension_type2 binDimensions2;
	tie(coordinates2, binDimensions2)=PreprocessFeatureSet<typename CorrespondenceDecimatorT::dimension_type2, Coordinate2T>(problem.FeatureSet2(), parameters.binDensity2);

	//Initialise the matcher parameters
	FeatureMatcherParametersC matcherParameters(parameters.matcherParameters);

	//Try guided matching
	typedef FeatureMatcherC<MatchingProblemT> FeatureMatcherT;

	CorrespondenceListT currentCorrespondences;
	FeatureMatcherDiagnosticsC fmDiagnostics;

	if(problem.InitialMatchingConstraint())
		fmDiagnostics=FeatureMatcherT::Run(currentCorrespondences, matchingProblem, matcherParameters, problem.SimilarityMetric(), problem.InitialMatchingConstraint(), problem.FeatureSet1(), problem.FeatureSet2());

	//If the initial guide fails, or does not exist, fall back to unguided matching
	if(!problem.InitialMatchingConstraint() || !fmDiagnostics.flagSuccess)
	{
		typedef typename MatchingProblemT::constraint_type MatchingConstraintT;
		optional<MatchingConstraintT> invalidConstraint;
		fmDiagnostics=FeatureMatcherT::Run(currentCorrespondences, matchingProblem, matcherParameters, problem.SimilarityMetric(), invalidConstraint, problem.FeatureSet1(), problem.FeatureSet2());
	}	//if(!problem.InitialMatchingConstraint() || !fmDiagnostics.flagSuccess)

	diagnostics.featureMatcherDiagnostics.push_back(fmDiagnostics);

	//If the initial matching fails, the algorithm terminates
	if(!fmDiagnostics.flagSuccess)
	{
		diagnostics.flagSuccess=fmDiagnostics.flagSuccess;
		return diagnostics;
	}	//if(!fmDiagnostics.flagSuccess)

	//Now, the main guided matching loop

	double maxAmbiguity=parameters.maxAmbiguity;	//Maximum ambiguity for the first pass
	DecimatorQuantisationStrategyT quantisationStrategy=DecimatorQuantisationStrategyT::BOTH;	//Quantisation strategy. NONE is just a placeholder
	CoordinateCorrespondenceContainerT validationSet;	//Validation set for the individual iterations
	CoordinateCorrespondenceContainerT referenceSet;	//Reference correspondence set, for hypothesis evaluation
	size_t sReference=0;	//Size of the reference set
	size_t nReferenceInlier=0;	// Number of inliers in the reference set
	bool flagReevaluate=false;	// If true, bestModel is reevaluated against the new reference set

	optional<ModelT> bestModel;	//Best model so far
	CorrespondenceListT bestCorrespondences;	//The correspondence set that spawned the best model
	CorrespondenceListT bestGenerator;	//Generator of the best model
	double bestError=numeric_limits<double>::infinity();	//Error for the best model
	double bestMeanError=numeric_limits<double>::infinity();	//Mean error for the best model
	unsigned int bestIndex=0;	//Index of the best model

	double prevError=numeric_limits<double>::infinity();	//Error for the previous iteration
	unsigned int cIteration=0;	//Iteration counter

	do
	{
		//Construct the observation and the validation sets

		//If no matching constraint, assess the reliability via the ambiguity
		//Even when an initial estimate is available, it is better to use the match ambiguity. In the tracker, this yields fewer relocalisation events- so, apparently the initial estimate is not good enough
		if(!problem.MatchingConstraint())
		{
			matchingProblem->AssessAmbiguity(currentCorrespondences, parameters.nThreads);
			quantisationStrategy= ( binDimensions1.size()<=binDimensions2.size() ) ? DecimatorQuantisationStrategyT::LEFT : DecimatorQuantisationStrategyT::RIGHT;	//Appearance ambiguity leaves many outliers, BOTH is not advisable
		}	//if(!problem.MatchingConstraint())

		//If there is a guide, use the constraint error, instead of match ambiguity, as the reliability metric

		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CoordinateCorrespondenceContainerT;
		CoordinateCorrespondenceContainerT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceContainerT>(currentCorrespondences, coordinates1, coordinates2);	//Index to coordinate conversion

		if(problem.MatchingConstraint())
		{
			for(auto& current : coordinateCorrespondences)
				current.info.Ambiguity()=get<1>(problem.MatchingConstraint()->Enforce(current.left, current.right));

			quantisationStrategy=DecimatorQuantisationStrategyT::BOTH;
			maxAmbiguity=numeric_limits<double>::infinity();	//Max ambiguity is disabled when a geometric constraint is available
		}	//if(problem.MatchingConstraint())

		CoordinateCorrespondenceContainerT observationSet;	//Observation set
		vector<size_t> indexMap;	//Indices of the observations in the full correspondence set
		bool flagDecimate=DecimateCorrespondences(validationSet, observationSet, indexMap, problem, coordinateCorrespondences, binDimensions1, binDimensions2, quantisationStrategy, maxAmbiguity, parameters);

		//Save the initial observation set. Saves a redundant call to the matcher in certain applications
		if(!diagnostics.initialObservationSet)
			diagnostics.initialObservationSet=FilterBimap(currentCorrespondences, indexMap);

		if(parameters.flagVerbose)
		{
			if(flagDecimate)
				cout<<"GEP: Iteration: "<<cIteration<<" #Correspondences: "<<currentCorrespondences.size()<<" #Observations: "<<observationSet.size()<<" "<<"#Validators: "<<validationSet.size()<<"\n";
			else
				cout<<"GEP: Not enough correspondences to meet the minimum observation and/or validation set size requirements. Value: "<<coordinateCorrespondences.size()<<"\n";
		}	//if(parameters.flagVerbose)

		if(!flagDecimate)
			break;

		//Geometry estimation

		problem.UpdateGeometryEstimationProblem(bestModel, observationSet, validationSet);

		ModelT currentModel;	//Estimated model
		vector<size_t> indexInliers;	//Inlier indices, wrt observationSet
		vector<size_t> indexGenerator;	//Indices of the members of the generator set, wrt observationSet
		GeometryEstimatorDiagnosticsC geDiagnostics=GeometryEstimatorT::Run(currentModel, indexInliers, indexGenerator, problem.GeometryEstimationProblem(), rng, parameters.geometryEstimatorParameters);
		diagnostics.geometryEstimatorDiagnostics.push_back(geDiagnostics);

		//If the geometry estimator fails, terminate
		if(!geDiagnostics.flagSuccess)
			break;

		//If a better model is found, update

		//Update the reference set
		if(referenceSet.empty() || ((double)indexInliers.size()/nReferenceInlier >= parameters.refreshThreshold) )
		{
			referenceSet=observationSet;
			sReference=referenceSet.size();
			nReferenceInlier=indexInliers.size();
			validationSet.clear();
			flagReevaluate=(bool)bestModel;
		}	//if(referenceSet.empty())
		else
			flagReevaluate=false;

		//If there is already a best model, re-evaluate
		if(flagReevaluate)
			bestError=MainSolverT::EvaluateModel(*bestModel, referenceSet, problem.GeometryEstimationProblem().RANSACProblem2().GetConstraint().GetErrorMetric(), problem.GeometryEstimationProblem().RANSACProblem2().GetLossMap());

		double currentError=MainSolverT::EvaluateModel(currentModel, referenceSet, problem.GeometryEstimationProblem().RANSACProblem2().GetConstraint().GetErrorMetric(), problem.GeometryEstimationProblem().RANSACProblem2().GetLossMap());

		//Average error: To prevent the undesirable case of a solution with few validators having lower error
		bool flagBestUpdated=false;	//Indicates whether the best model is updated
		if(currentError/sReference < bestError/sReference)
		{
			flagBestUpdated=true;
			bestError=currentError;
			bestMeanError=currentError/sReference;
			bestModel=currentModel;

			transform(indexGenerator, indexGenerator.begin(), [&](size_t v){return indexMap[v];});	//Revert back to the original indices
			bestGenerator=FilterBimap(currentCorrespondences, indexGenerator);

			bestIndex=cIteration;
		}	//if(geDiagnostics.score > bestScore)

		if(parameters.flagVerbose) cout<<"GEP:             "<<" Inlier ratio: "<<(double)indexInliers.size()/observationSet.size()<<" Avg. error: "<<currentError/sReference<<" Best avg. error: "<<bestError/sReference<<(flagReevaluate ? " Reference updated":"")<<"\n";

//		diagnostics.history.emplace_back(geDiagnostics.error, indexGenerator);

		++cIteration;

		//Check for termination

		bool flagTerminate=(cIteration+1>parameters.maxIteration); //Number of iterations. Offsetting for the do-while loop. Otherwise, an extra iteration is performed.
		flagTerminate|=(currentError==0); //Convergence
		flagTerminate|=( !flagReevaluate && fabs( (currentError-prevError)/currentError) <= parameters.minRelativeDifference);		//If there is a new reference set, postpone convergence

		prevError=currentError;

		//Guided matching: If the process is not terminated, or the best model is updated (so that we have the correspondences guided by the best model)
		if(!flagTerminate || flagBestUpdated)
		{
			problem.UpdateConstraint(currentModel);
			matchingProblem->SetConstraint(*problem.MatchingConstraint());
			fmDiagnostics=FeatureMatcherT::Run(currentCorrespondences, matchingProblem, matcherParameters, problem.SimilarityMetric(), problem.MatchingConstraint(), problem.FeatureSet1(), problem.FeatureSet2());
			diagnostics.featureMatcherDiagnostics.push_back(fmDiagnostics);
		}

		if(!fmDiagnostics.flagSuccess)
			break;

		if(flagBestUpdated)
			bestCorrespondences=currentCorrespondences;	//Correspondences obtained by the use of bestModel as the guide

		if(flagTerminate)
			break;
	}while(true);

	//Inliers to the bestModel, in the original correspondence set
	CorrespondenceListT bestInliers;
	for(const auto& current : bestCorrespondences)
		if(problem.GeometryEstimationProblem().RANSACProblem2().GetConstraint()(*bestModel, coordinates1[current.left], coordinates2[current.right]))
			bestInliers.push_back(current);

	//Covariance estimation
	if(bestModel && parameters.flagCovariance)
	{
		auto itBestGEDiag=next(diagnostics.geometryEstimatorDiagnostics.begin(), bestIndex);
		covariance=ComputeCovarianceWrapper(problem, *bestModel, bestGenerator, bestInliers, coordinates1, coordinates2, parameters, itBestGEDiag->flagLO, itBestGEDiag->flagStage2);
	}	//if(bestModel)

	//Output

	diagnostics.flagSuccess=(bool)bestModel;

	if(!diagnostics.flagSuccess)
		return diagnostics;

	model=*bestModel;
	correspondences.swap(bestInliers);
	diagnostics.error=bestError;
	diagnostics.meanError=bestMeanError;
	diagnostics.nIteration=cIteration;
	//diagnostics.iBest=bestIndex;
	diagnostics.generator=bestGenerator;

	return diagnostics;
}	//GeometryEstimationPipelineDiagnosticsC Run(ModelT& model, CorrespondenceListT& correspondences, ProblemT& problem, optional<MatchingProblemT>& matchingProblem, GeometryEstimationPipelineParametersC parameters)


}	//GeometryN
}	//SeeSawN

#endif /* GEOMETRY_ESTIMATION_PIPELINE_IPP_0343123 */
