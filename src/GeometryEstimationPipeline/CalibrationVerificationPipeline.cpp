/**
 * @file CalibrationVerificationPipeline.cpp Implementation of the calibration verification pipeline
 * @author Evren Imre
 * @date 30 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CalibrationVerificationPipeline.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the input parameters
 * @param[in,out] parameters Parameters. Modified to customise some components to the application
 * @param[in] indexCorrect Indices of the cameras which are known to be unperturbed
 * @param[in] nCameras Number of cameras
 * @throws invalid_argument If the parameter validation fails
 */
void CalibrationVerificationPipelineC::ValidateInput(CalibrationVerificationPipelineParametersC& parameters, const set<size_t>& indexCorrect, unsigned int nCameras)
{
	parameters.nThreads=min(parameters.nThreads, (unsigned int)omp_get_max_threads());

	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: noiseVariance must be >0. Value=")+lexical_cast<string>(parameters.noiseVariance)));

	if(parameters.inlierRejectionProbability<=0 || parameters.inlierRejectionProbability>=1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: inlierRejectionProbability must be in (0,1). Value=")+lexical_cast<string>(parameters.inlierRejectionProbability)));

	if(parameters.loGeneratorRatio1<1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: loGeneratorRatio1 must be >=1. Value=")+lexical_cast<string>(parameters.loGeneratorRatio1)));

	if(parameters.loGeneratorRatio2<1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: loGeneratorRatio2 must be >=1. Value=")+lexical_cast<string>(parameters.loGeneratorRatio2)));

	if(parameters.moMaxAlgebraicDistance<0 || parameters.moMaxAlgebraicDistance>1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: moMaxAlgebraicDistance must be in [0,1]. Value=")+lexical_cast<string>(parameters.moMaxAlgebraicDistance)));

	if(parameters.initialEstimateNoiseVariance<0)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: initialEstimateNoiseVariance must be >=0. Value=")+lexical_cast<string>(parameters.initialEstimateNoiseVariance)));

	if(parameters.minInlierRatio<0 || parameters.minInlierRatio>1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: minInlierRatio must be in [0,1]. Value=")+lexical_cast<string>(parameters.minInlierRatio)));

	if(parameters.pairwiseDecisionTh<=1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: pairwiseDecisionTh must be >1. Value=")+lexical_cast<string>(parameters.pairwiseDecisionTh)));

	if(parameters.minPairwiseRelation<1)
		throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput: minPairwiseRelation must be >=1. Value=")+lexical_cast<string>(parameters.minPairwiseRelation)));

	for(auto c : indexCorrect)
		if(c>=nCameras)
			throw(invalid_argument(string("CalibrationVerificationPipelineC::ValidateInput : Invalid camera index in indexCamera. Value=")+lexical_cast<string>(c)));

	parameters.geometryEstimationPipelineParameters.flagCovariance=false;
	parameters.geometryEstimationPipelineParameters.flagVerbose=false;
}	//vector<CameraMatrixT> ValidateInput(const vector<CameraC>& cameraList, CalibrationVerificationPipelineParametersC& parameters)

/**
 * @brief Preprocesses the input data
 * @param[in] featureStack Feature stack
 * @param[in] parameters Parameters
 * @return A tuple: A vector of bin dimensions, and the coordinate stack corresponding to the features
 */
auto CalibrationVerificationPipelineC::PreprocessInput(const vector<vector<ImageFeatureC> >& featureStack, const CalibrationVerificationPipelineParametersC& parameters) -> tuple<vector<BinT>, vector<vector<Coordinate2DT> > >
{
	size_t nCameras=featureStack.size();
	vector< vector<Coordinate2DT> > coordinateStack(nCameras);
	vector<BinT> binSizeList(nCameras);
	for(size_t c=0; c<nCameras; ++c)
	{
		coordinateStack[c]=MakeCoordinateVector(featureStack[c]);

		optional<BinT> binSize=ComputeBinSize<Coordinate2DT>(coordinateStack[c], parameters.geometryEstimationPipelineParameters.binDensity1);
		if(!binSize)	//binSizes[c] remains uninitialised, however, an empty coordinate vector signals a problem to the main loop, which skips the corresponding camera
			continue;

		binSizeList[c]=*binSize;
	}	//for(size_t c=0; c<nCameras; ++c)

	tuple<vector<BinT>, vector<vector<Coordinate2DT> > > output;
	get<1>(output).swap(coordinateStack);
	get<0>(output).swap(binSizeList);

	return output;
}	//tuple<vector<dimension_type>, vector<vector<Coordinate2DT> > > PreprocessInput(const vector<vector<ImageFeatureC> >& featureStack, const CalibrationVerificationPipelineParametersC& parameters)

/**
 * @brief Generates the pairwise verification tasks
 * @param[in] cameraList Cameras to be verified
 * @param[in] featureStack Image features for the cameras
 * @param[in] indexCorrect Index of cameras that are known to be unperturbed
 * @param[in] parameters Parameters
 * @return A vector describing the pairwise verification tasks
 */
auto CalibrationVerificationPipelineC::GenerateTasks( const vector<CameraMatrixT>& cameraList, const vector<vector<ImageFeatureC> >& featureStack, const set<size_t>& indexCorrect, const CalibrationVerificationPipelineParametersC& parameters) -> vector<TaskT>
{
	vector<TaskT> output;

	size_t nCameras=cameraList.size();
	IndicatorArrayT flagStatus=MakeBitset<IndicatorArrayT>(indexCorrect, nCameras);	//Perturbed cameras are marked as false

	//Complexity
	map< double, PairIdT, greater<double> > ordered;	//Tasks ordered wrt/complexity. Complexity is approximated as O(MN), in the sizes of the feature sets
	double totalComplexity=0;
	for(size_t c1=0; c1<nCameras; ++c1)
		for(size_t c2=c1+1; c2<nCameras; ++c2)
			if(! (flagStatus[c1]&&flagStatus[c2]))	//Unless both cameras are known to be correct
			{
				double complexity=featureStack[c1].size()*featureStack[c2].size();
				ordered.emplace(complexity, make_tuple(c1,c2));
				totalComplexity+=complexity;
			}	//if(! (flagStatus[c1]&&flagStatus[c2]))

	//Thread allocation
	size_t nTasks=ordered.size();
	unsigned int nAllocated=min(nTasks, (size_t)parameters.nThreads);
	vector<unsigned int> threadAllocation(nTasks,1);
	if(parameters.nThreads>nAllocated)
	{
		double threadPerComplexity=(parameters.nThreads-nAllocated)/totalComplexity;

		size_t index=0;
		for(const auto& current : ordered)
		{
			unsigned int tmp=floor(threadPerComplexity*current.first);
			threadAllocation[index]+=tmp;
			nAllocated+=tmp;
			++index;
		}	//for(const auto& current : ordered)

		//Allocate the remaining threads
		for(size_t c=0; nAllocated<parameters.nThreads; ++c, ++nAllocated)
			++threadAllocation[c];
	}	//if(parameters.nThreads>nAllocated)

	//Task generation
	size_t index=0;
	output.reserve(nTasks);
	for(const auto& current : ordered)
	{
		EpipolarMatrixT mF=CameraToFundamental(cameraList[get<0>(current.second)], cameraList[get<1>(current.second)]);
		output.emplace_back(current.second, mF, threadAllocation[index]);
		++index;
	}	//for(const auto& current : ordered)

	return output;
}	//vector<TaskT> GenerateTasks( const vector<CameraC>& cameraList, const vector<ImageFeatureC>& featureStack, const vector<size_t>& indexCorrect, const CalibrationVerificationPipelineParametersC& parameters)

/**
 * @brief Evaluates a fundamental matrix
 * @param[in] mF Estimated F
 * @param[in] correspondences Reference set for evaluation
 * @param[in] inlierTh Inlier threshold
 * @return A tuple: Inlier count; score
 */
tuple<unsigned int, double> CalibrationVerificationPipelineC::EvaluateF(const EpipolarMatrixT& mF, const CoordinateCorrespondenceList2DT& correspondences, double inlierTh)
{
	EpipolarSampsonConstraintT::error_type errorMetric(mF);
	EpipolarSampsonConstraintT constraint(errorMetric, inlierTh, 1);
	Fundamental7SolverC::loss_type lossMap(inlierTh, inlierTh);

	unsigned int inlierCount=0;
	double totalLoss=0;
	for(const auto& current : correspondences)
	{
		bool flagInlier;
		double error;
		tie(flagInlier, error)=constraint.Enforce(current.left, current.right);
		totalLoss+=lossMap(error);
		inlierCount+= (flagInlier ? 1:0);
	}	//for(const auto& current : correspondences)

	double score=lossMap(inlierTh)*correspondences.size()-totalLoss;	//Score= max total loss - incurred total loss
	return make_tuple(inlierCount, score);
}	//tuple<unsigned int, double>  EvaluateF(const EpipolarMatrixT mF, const CoordinateCorrespondenceList2DT& correspondences, const CalibrationVerificationPipelineParametersC& parameters)

/**
 * @brief Makes a pairwise evaluation record
 * @param[in] mFEstimated Estimated fundamental matrix. Invalid, if the estimation fails
 * @param[in] mFPredicted Predicted fundamental matrix
 * @param[in] correspondences Reference correspondence set
 * @param[in] parameters Parameters
 * @return A record. Invalid, if rejected
 */
auto CalibrationVerificationPipelineC::MakeRecord(const optional<EpipolarMatrixT>& mFEstimated, const EpipolarMatrixT& mFPredicted, const CoordinateCorrespondenceList2DT& correspondences, const CalibrationVerificationPipelineParametersC& parameters) -> optional<RecordT>
{
	optional<RecordT> output;

	//When F estimation fails, the prediction wins by default
	if(!mFEstimated)
	    return output;

	double inlierTh=EpipolarSampsonConstraintT::error_type::ComputeOutlierThreshold(parameters.inlierRejectionProbability, parameters.noiseVariance);

	double scoreE=0;
	unsigned int inlierE=0;
	if(mFEstimated)
		tie(inlierE, scoreE)=EvaluateF(*mFEstimated, correspondences, inlierTh);

	double scoreP;
	unsigned int inlierP;
	tie(inlierP, scoreP)=EvaluateF(mFPredicted, correspondences, inlierTh);

	//Reliability test
	unsigned int inlierCount=max(inlierE, inlierP);
	double inlierRatio=(double)inlierCount/correspondences.size();
	if(inlierRatio<parameters.minInlierRatio || inlierCount<parameters.minSupport)
		return output;

	//bool flagPerturbed=(scoreE/scoreP >= parameters.pairwiseDecisionTh);
	bool flagPerturbed=((double)inlierE/inlierP >= parameters.pairwiseDecisionTh);

	output=RecordT(inlierE, scoreE, inlierP, scoreP, flagPerturbed);

	return output;
}	//optional<RecordT> MakeRecord(const EpipolarMatrixT& mFEstimated, const EpipolarMatrixT& mFPredicted, const CoordinateCorrespondenceList2DT& correspondences, const CalibrationVerificationPipelineC& parameters)

/**
 * @brief Labels the cameras as perturbed, intact or undetermined
 * @param[in,out] diagnostics Diagnostics object to be updated
 * @param[in] pairwiseRecords Pairwise records
 * @param[in] indexCorrect Indices of the cameras that are known to be unperturbed
 * @param[in] nCameras Number of cameras
 * @param[in] parameters Parameters
 * @return A vector of flags. \c true indicates an intact camera, \c false a perturbed camera, and \c indeterminate , one with no decision
 */
auto CalibrationVerificationPipelineC::LabelCameras(CalibrationVerificationPipelineDiagnosticsC& diagnostics, const map<PairIdT, RecordT>& pairwiseRecords, const set<size_t>& indexCorrect, size_t nCameras, const CalibrationVerificationPipelineParametersC& parameters) -> vector<result_type>
{
	//Identify the undetermined cameras
	vector<unsigned int> recordPerCamera(nCameras,0);
	for(const auto& current : pairwiseRecords)
	{
		++recordPerCamera[get<0>(current.first)];
		++recordPerCamera[get<1>(current.first)];
	}	//for(const auto& current : pairwiseRecords)

	IndicatorArrayT flagCorrect(nCameras);	// Indicates the cameras that are known to be correct
	for(size_t c : indexCorrect)
		flagCorrect[c]=true;

	IndicatorArrayT flagUndetermined(nCameras);	//Indicates the cameras lacking the necessary number of pairwise records
	for(size_t c=0; c<nCameras; ++c)
		flagUndetermined[c]= (recordPerCamera[c]<parameters.minPairwiseRelation) && !flagCorrect[c];

	//Exhaustive search over the remaining cameras
	size_t nActive=nCameras-(flagCorrect.count()+flagUndetermined.count());	//Number of cameras which might be perturbed (i.e. not marked as correct, or undetermined)
	size_t iActive=0;
	vector<int> indexMap(nCameras, -1);
	for(size_t c=0; c<nCameras; ++c)
		if( !(flagCorrect[c]||flagUndetermined[c]) )
		{
			indexMap[c]=iActive;
			++iActive;
		}	//if( !(flagCorrect[c]||flagUndetermined[c]) )

	size_t nLabellings= nActive>0 ? 1<<nActive : 0;
	double bestScore=0;
	unsigned int bestConsistentCount=0;
	IndicatorArrayT bestLabels(nActive);
	for(size_t c=0; c<nLabellings; ++c)
	{
		//Extract the labels
		IndicatorArrayT flagActive(nActive);
		size_t iCamera=0;
		for(size_t c2=c; c2>0; c2>>=1, ++iCamera)
			flagActive[iCamera] = (c2%2==1);

		//No more perturbed cameras than maxPerturbedCamera
		if(nActive-flagActive.count() > parameters.maxPerturbed)
			continue;

		//Evaluate the consistency with the pairwise measurements
		double currentScore=0;
		unsigned int nConsistent=0;
		for(const auto& current : pairwiseRecords)
		{
			size_t i1;
			size_t i2;
			tie(i1,i2)=current.first;

			//At least one camera should be active. This is indicated by a non-negative entry in indexMap
			if( indexMap[i1]<0 && indexMap[i2]<0 )
				continue;

			//Consistency check
			bool flagPerturbed1= (indexMap[i1]>=0) && (!flagActive[indexMap[i1]]);
			bool flagPerturbed2= (indexMap[i2]>=0) && (!flagActive[indexMap[i2]]);
			bool flagPerturbed = flagPerturbed1 || flagPerturbed2;

			//If in agreement with the pairwise record, update the score
			if( flagPerturbed==get<iPerturbed>(current.second) )
			{
				//currentScore+=max(get<iSupportE>(current.second), get<iSupportP>(current.second));
				currentScore+=max(get<iScoreE>(current.second), get<iScoreP>(current.second));
				++nConsistent;
			}
		}	//for(const auto& current : pairwiseRecords)

		if(currentScore>=bestScore)
		{
			bestScore=currentScore;
			bestConsistentCount=nConsistent;
			bestLabels=flagActive;
		}	//if(currentScore>bestScore)
	}	//for(size_t c=0; c<nLabels; ++c)

	//Form the output

	vector<tribool> cameraStatus(nCameras);
	for(size_t c=0; c<nCameras; ++c)
	{
		if(indexMap[c]>-1)
			cameraStatus[c]= (bestLabels[indexMap[c]] ? true : false);
		else
		{
			if(flagCorrect[c])
				cameraStatus[c]=true;
			else
				cameraStatus[c]=indeterminate;
		}
	}	//for(size_t c=0; c<nCameras; ++c)

	//Confidence
	ArrayX2d confidenceArray(nCameras,2); confidenceArray.setZero();
	for(const auto& current : pairwiseRecords)
	{
		size_t i1;
		size_t i2;
		tie(i1,i2)=current.first;

		//Is the relation consistent with the final labelling
		double weight=max(get<iScoreE>(current.second), get<iScoreP>(current.second));
		tribool flagConsistent = (!(cameraStatus[i1] && cameraStatus[i2])) == get<iPerturbed>(current.second);
		confidenceArray(i1, flagConsistent ? 0:1)+=weight;
		confidenceArray(i2, flagConsistent ? 0:1)+=weight;
	}	//for(const auto& current : pairwiseRecords)

	//Output
	vector<result_type> output(nCameras);
	for(size_t c=0; c<nCameras; ++c)
		output[c]=result_type(cameraStatus[c], confidenceArray(c,0)/(confidenceArray.row(c).sum()));

	diagnostics.nConsistent=bestConsistentCount;
	diagnostics.bestScore=bestScore;

	return output;
}	//vector<tribool> LabelCameras(const vector<RecordT>& pairwiseRecords, size_t nCameras)

}	//GeometryN
}	//SeeSawN

