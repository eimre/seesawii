/**
 * @file CalibrationVerificationPipeline.h Public interface for the calibration verification pipeline
 * @author Evren Imre
 * @date 30 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CALIBRATION_VERIFICATION_PIPELINE_H_0809123
#define CALIBRATION_VERIFICATION_PIPELINE_H_0809123

#include "CalibrationVerificationPipeline.ipp"
namespace SeeSawN
{
namespace GeometryN
{

struct CalibrationVerificationPipelineParametersC;	///< Parameters for \c CalibrationVerificationPipelineC
struct CalibrationVerificationPipelineDiagnosticsC;	///< Diagnostics object for \c CalibrationVerificationPipelineC

class CalibrationVerificationPipelineC;	///< Verifies the validity of the calibration information for a multicamera setup

}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, std::size_t>(const std::size_t&);
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template class std::vector<unsigned int>;


#endif /* CALIBRATIONVERIFICATIONPIPELINE_H_ */
