/**
 * @file SparseUnitSphereReconstructionPipeline.h Public interface for the sparse unit sphere reconstruction pipeline
 * @author Evren Imre
 * @date 21 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef SPARSE_UNIT_SPHERE_RECONSTRUCTION_PIPELINE_H_2280901
#define SPARSE_UNIT_SPHERE_RECONSTRUCTION_PIPELINE_H_2280901

#include "SparseUnitSphereReconstructionPipeline.ipp"
#include "../Metrics/Similarity.h"

namespace SeeSawN
{
namespace GeometryN
{

struct SparseUnitSphereReconstructionPipelineParametersC;	///< Sparse unit sphere reconstruction pipeline parameters
struct SparseUnitSphereReconstructionPipelineDiagnosticsC;	///< Sparse unit sphere reconstruction pipeline diagnostics
class SparseUnitSphereReconstructionPipelineC;	///<Sparse unit sphere reconstruction pipeline


/********** EXTERN TEMPLATES *********/
using SeeSawN::MetricsN::InverseHammingBIDConverterT;
extern template SparseUnitSphereReconstructionPipelineDiagnosticsC SparseUnitSphereReconstructionPipelineC::Run(vector<Coordinate3DT>& pointCloud,  vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const InverseHammingBIDConverterT& similarityMetric, SparseUnitSphereReconstructionPipelineParametersC parameters);

}	//namespace GeometryN
}	//namespace SeeSawN

/********** EXTERN TEMPLATES *********/
extern template class std::tuple<std::size_t, std::size_t>;



#endif /* SPARSE_UNIT_SPHERE_RECONSTRUCTION_PIPELINE_H_2280901 */
