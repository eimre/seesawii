/**
 * @file RotationRegistrationPipeline.ipp Implementation details for the relative rotation registration pipeline
 * @author Evren Imre
 * @date 29 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef ROTATION_REGISTRATION_PIPELINE_IPP_6919024
#define ROTATION_REGISTRATION_PIPELINE_IPP_6919024

#include <boost/math/constants/constants.hpp>
#include <boost/lexical_cast.hpp>
#include <vector>
#include <cstddef>
#include <string>
#include <stdexcept>
#include <tuple>
#include <map>
#include <utility>
#include "../RandomSpanningTreeSampler/RandomSpanningTreeSampler.h"
#include "../RandomSpanningTreeSampler/RSTSRotationRegistrationProblem.h"
#include "../Geometry/RelativeRotationRegistration.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::math::constants::pi;
using boost::lexical_cast;
using std::string;
using std::invalid_argument;
using std::vector;
using std::size_t;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::map;
using std::make_pair;
using std::pair;
using RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerC;
using RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerParametersC;
using RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerDiagnosticsC;
using RandomSpanningTreeSamplerN::RSTSRotationRegistrationProblemC;
using GeometryN::RelativeRotationRegistrationC;

/**
 * @brief Parameter object for the rotation registration pipeline
 * @ingroup Parameters
 * @nosubgrouping
 */
struct RotationRegistrationPipelineParametersC
{
	double inlierThreshold;	///< Inlier threshold, as the angle between an observation and its prediction, in radians. [0, pi]
	bool flagWeightedLS;	///< If \c true, the refinement stage uses weighted least squares (instead of least squares)

	RandomSpanningTreeSamplerParametersC rstsParameters;	///< Parameters for the random spanning tree sampler stage

	RotationRegistrationPipelineParametersC() : inlierThreshold(0.1), flagWeightedLS(false)
	{}

};	//struct RotationRegistrationPipelineParametersC

/**
 * @brief Diagnostics object for the rotation registration pipeline
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct RotationRegistrationPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation is completed successfully

	RandomSpanningTreeSamplerDiagnosticsC rstsDiagnostics;	///< Diagnostics for the RSTS stage
	RandomSpanningTreeSamplerDiagnosticsC mstDiagnostics;	///< Diagnostics for the MST stage
	bool flagSuccessLS;	///< If \c true if the LS refinement is successful

	double error;	///< Model error
	double inlierRatio;	///< Inlier ratio

	RotationRegistrationPipelineDiagnosticsC() : flagSuccess(false), flagSuccessLS(false), error(numeric_limits<double>::infinity()), inlierRatio(0)
	{}
};	//struct RotationRegistrationPipelineDiagnosticsC

/**
 * @brief Pipeline for registration of relative rotations to an absolute rotation model
 * @remarks Flow
 * 	- Initial estimate via random spanning tree sampling
 * 	- If fails, back-up estimate by minimal spanning tree
 * 	- LS solution over inliers
 * @ingroup Algorithm
 * @nosubgrouping
 */
class RotationRegistrationPipelineC
{
	private:

		typedef RelativeRotationRegistrationC::real_type RealT;

		typedef RandomSpanningTreeSamplerC<RSTSRotationRegistrationProblemC> RSTSEngineT;	///< Random spanning tree sampler engine
		typedef RSTSRotationRegistrationProblemC::model_type ModelT;	///< Type for the absolute rotation model
		typedef RSTSEngineT::rng_type RNGT;	///< Type for the random number generator

		typedef vector<size_t> IndexContainerT;	///< An index container
		typedef RelativeRotationRegistrationC::observation_type ObservationT;	///< An observation

		/** @name Implementation details *///@{
		static void ValidateInput(const vector<ObservationT>& observations, const RotationRegistrationPipelineParametersC& parameters);	///< Validates the input
		//@}

	public:

		typedef ModelT result_type;	///< Type for the absolute rotation model
		typedef IndexContainerT index_container_type;	///< Type of the index container
		typedef ObservationT observation_type;	///< Observation type

		static RotationRegistrationPipelineDiagnosticsC Run(ModelT& model, IndexContainerT& inliers, RNGT& rng, const vector<ObservationT>& observations, const RotationRegistrationPipelineParametersC& parameters);	///< Runs the algorithm
};	//class RotationRegistrationPipelineC

}	//GeometryN
}	//SeeSawN

#endif /* ROTATION_REGISTRATION_PIPELINE_IPP_6919024 */
