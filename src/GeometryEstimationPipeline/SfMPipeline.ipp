/**
 * @file SfMPipeline.ipp Implementation of the SfM pipeline
 * @author Evren Imre
 * @date 31 Oct 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef SFM_PIPELINE_IPP_6709012
#define SFM_PIPELINE_IPP_6709012

#include <boost/range/algorithm.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/vector_of.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/math/distributions.hpp>
#include <Eigen/Eigenvalues>
#include <Eigen/Dense>
#include <algorithm>
#include <vector>
#include <map>
#include <cstddef>
#include <tuple>
#include <cmath>
#include <utility>
#include <iterator>
#include <unordered_set>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Camera.h"
#include "../Elements/Correspondence.h"
#include "../GeometryEstimationComponents/Fundamental7Components.h"
#include "../GeometryEstimationComponents/EssentialComponents.h"
#include "../GeometryEstimationComponents/Homography3DComponents.h"
#include "../GeometryEstimationComponents/Similarity3DComponents.h"
#include "../GeometryEstimationComponents/P3PComponents.h"
#include "../GeometryEstimationComponents/P4PComponents.h"
#include "../GeometryEstimationComponents/GenericPnPComponents.h"
#include "../GeometryEstimationPipeline/VisionGraphPipeline.h"
#include "../GeometryEstimationPipeline/PoseGraphPipeline.h"
#include "../GeometryEstimationPipeline/LinearAutocalibrationPipeline.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GeometryEstimatorProblemConcept.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../GeometryEstimationPipeline/Sparse3DReconstructionPipeline.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../Geometry/MultiviewTriangulation.h"
#include "../Geometry/Triangulation.h"
#include "../Geometry/P3PSolver.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLPoseGraphProblem.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../Matcher/SceneImageFeatureMatchingProblem.h"
#include "../Metrics/CheiralityConstraint.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/BoostGraph.h"
#include "../Wrappers/EigenExtensions.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTTriangulationProblem.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::for_each;
using boost::transform;
using boost::range::nth_element;
using boost::range_value;
using boost::copy;
using boost::accumulate;
using boost::counting_range;
using boost::adaptors::map_values;
using boost::adaptors::map_keys;
using boost::bimap;
using boost::bimaps::vector_of;
using boost::bimaps::set_of;
using boost::bimaps::left_based;
using boost::math::pow;
using boost::graph_traits;
using boost::add_vertex;
using boost::add_edge;
using boost::vertex_name_t;
using boost::vertex;
using boost::vertices;
using boost::num_vertices;
using boost::dijkstra_shortest_paths;
using boost::get;
using boost::make_iterator_property_map;
using boost::math::chi_squared_distribution;
using boost::math::quantile;
using boost::math::complement;
using Eigen::EigenSolver;
using Eigen::Matrix3d;
using std::all_of;
using std::any_of;
using std::vector;
using std::map;
using std::size_t;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::get;
using std::max;
using std::move;
using std::advance;
using std::cbrt;
using std::unordered_set;
using std::pair;
using std::lexicographical_compare;
using std::make_pair;
using std::inserter;
using std::back_inserter;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::Viewpoint3DC;
using SeeSawN::ElementsN::ComputeRelativePose;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::CameraFromFundamental;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::MapToCanonical;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::ElementsN::TranslationToPosition;
using SeeSawN::ElementsN::CameraFromEssential;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::GeometryN::Fundamental7PipelineProblemT;
using SeeSawN::GeometryN::EssentialPipelineProblemT;
using SeeSawN::GeometryN::Homography3DPipelineProblemT;
using SeeSawN::GeometryN::Similarity3DPipelineProblemT;
using SeeSawN::GeometryN::P3PPipelineProblemT;
using SeeSawN::GeometryN::P3PEstimatorProblemT;
using SeeSawN::GeometryN::P4PEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACPnPProblem;
using SeeSawN::GeometryN::MakePDLPnPProblem;
using SeeSawN::GeometryN::VisionGraphPipelineC;
using SeeSawN::GeometryN::VisionGraphPipelineDiagnosticsC;
using SeeSawN::GeometryN::VisionGraphPipelineParametersC;
using SeeSawN::GeometryN::PoseGraphPipelineC;
using SeeSawN::GeometryN::PoseGraphPipelineDiagnosticsC;
using SeeSawN::GeometryN::PoseGraphPipelineParametersC;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineC;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineParametersC;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineDiagnosticsC;
using SeeSawN::GeometryN::TriangulatorC;
using SeeSawN::GeometryN::P3PSolverC;
using SeeSawN::GeometryN::PoseMinimalDifferenceC;
using SeeSawN::GeometryN::MultiviewTriangulationC;
using SeeSawN::GeometryN::MultiviewTriangulationDiagnosticsC;
using SeeSawN::GeometryN::MultiviewTriangulationParametersC;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineC;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineParametersC;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::MatcherN::SceneImageFeatureMatchingProblemC;
using SeeSawN::MetricsN::CheiralityConstraintC;
using SeeSawN::NumericN::ComputePseudoInverse;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::WrappersN::FindConnectedComponents;
using SeeSawN::WrappersN::EigenLexicalCompareC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationParametersC;
using SeeSawN::UncertaintyEstimationN::SUTTriangulationProblemC;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::OptimisationN::PDLPoseGraphProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PowellDogLegDiagnosticsC;
using SeeSawN::OptimisationN::PowellDogLegParametersC;

//Notes
//TODO k-NN matching and vision graph: k-nn matching is good for estimating the epipolar geometry, but interferes with the reconstruction of the 3D models.
//	- Current solution: disable k-nn matching
//  - Ideal solution: run the vision graph with k-nn matching, and perform a subsequent 1:1 matching with the estimated epipolar geometry. Replacing MultiviewTriangulation with Sparse3DReconstruction could do the trick


//TODO Building the pose graph takes far too long for more than 10 images.
// - Current solution: Work with relatively small keyframe sets
// - Ideal solution: Use a subset of the keyframes, calibrate them and build a 3D model. then register all keyframes to that model
//  - Alternative solution: Ditch matching, use the correspondences from the vision graph to build the pose graph

//FIXME Bundle adjustment has been removed due to stability issues with G2O.

/**
 * @brief Parameters for the SfM pipeline
 * @ingroup Parameters
 * @nosubgrouping
 */
struct SfMPipelineParametersC
{
	unsigned int nThreads=1;	///< Number of threads available to the pipeline
	bool flagEuclidean=true;	///< If \c true, and if there is an initial estimate for all intrinsic calibration parameters, perform Eucludean SfM

	//Pose graph
	double imageNoiseVariance=4;	///< Image coordinate noise variance, in pixels. >0
	double maxMedianDepth=1e3;	///< Maximum median depth, as a multiple of the baseline, for a valid pairwise reconstruction. >0

	VisionGraphPipelineParametersC vgParameters;	///< Parameters for the vision graph
	ScaledUnscentedTransformationParametersC sutTriangulationParameters;	///< SUT parameters for triangulation
	PoseGraphPipelineParametersC pgParameters;	///< Parameters for the pose graph

	double edgeTraversalPenalty=0.75;	///< When forming the camera chains, minimum cost for traversing an edge, in terms of the maximum edge strength. Higher values favour shorter chains- less error due to accumulation, but poor homographies are more likely to be utilised. >=0

	//Intrinsic calibration
	LinearAutocalibrationPipelineParametersC intrinsicCalibrationParameters;	///< Parameters for the linear autocalibration pipeline
	bool flagUniformFocalLength=false;	///< True if the cameras are forced to have the same intrinsic calibration. This does not prevent any variations introduced at the robust refinement stage

	//Pose graph optimisation
	double maxOrientationError=2e-1;	///< Maximum error in the relative pose orientation estimates, used for deriving the orientation noise. rad. >0
	double positionPrecision=1e-1;		///< Precision of the relative pose position estimates, used for deriving the position noise. >0
	double poseGraphInlierThreshold=quantile(complement(chi_squared_distribution<double>(6), 0.05) );	///< Inlier threshold to identify the inlier edges in the pose graph, Mahalanobis distance. The default value is the p-0.05 point for 6 DoF. >0
	PowellDogLegParametersC poseGraphOptimisationParameters;	///< PDL parameters for pose graph optimisation

	//Scene model
	double referenceTransferError=0;	///< Additive error introduced by transferring cameras to a different reference frame across the pose graph, in terms of the image noise. >=0
	Sparse3DReconstructionPipelineParametersC sceneReconstructionParameters;	///< Parameters for building the scene model

	//TODO to the interface

	//Robust refinement
	//TODO To the interface
	GeometryEstimationPipelineParametersC registration32Parameters;	///< Parameters for the 3D-2D registration pipeline
	double inlierRejectionProbability=0.05;		///< Probability of rejecting an inlier sample. (0,1)
	double loGeneratorRatio1=3;	///< Number of generators for LO for the first RANSAC stage, as a multiple of the minimal set. >=1
	double loGeneratorRatio2=3;	///< Number of generators for LO for the second RANSAC stage, as a multiple of the minimal set. >=1
	unsigned int minSupport=25;	///< Minimum support. >=0
	double predictionNoise=4;	///< Additive prediction noise (due to the pose estimation), as a multiple of the image noise. >=0

	SfMPipelineParametersC()
	{}

};	//struct SfMPipelineParametersC

/**
 * @brief Diagnostics object for the SfM pipeline
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct SfMPipelineDiagnosticsC
{
	bool flagSuccess=false;	///< \c true if the operation is completed successfully

};	//struct SfMPipelineDiagnosticsC

/**
 * @brief Structure-from-motion pipeline
 * @remarks Usage notes
 * 	- Initial estimates
 * 		- Pose: Initial estimate for the vision graph
 * 		- Intrinsics:
 * 			- A metric reconstruction is possible only if intrinsics are known for all cameras
 * 			- If the intrinsics are incomplete, the available intrinsics are treated as initial estimates
 * 			- If the intrinsics are complete, how they are treated are informed by a flag in the parameters
 * 	- Features are assumed to be undistorted. Any lens distortion parameters are ignored
 * 	- Algorithm: Favours accuracy over efficiency
 * 		- Intrinsic calibration
 * 			- Vision graph (fundamental)
 * 			- Projective pose graph
 * 			- For each spanning tree originating from a camera, solve for intrinsic parameters
 * 		- Extrinsic calibration
 * 			- Vision graph (essential)
 * 			- Pose graph
 * 			- Spanning tree
 * 			- Pose graph optimisation
 * 		- Refinement
 * 			- Multiview triangulation from registered keyframes
 * 			- Registration of all frames via RANSAC
 * 			- Bundle adjustment
 * 	- Degeneracy test for pairwise reconstructions: If median point distance to both cameras is much larger than the baseline, the test fails- potentially, narrow baseline
 * 	- If there is lens distortion present in the image features, this is reflected in the focal length. For example, barrel distortion is compensated with an increase in the focal length
 * @ingroup Algorithm
 * @nosubgrouping
 */
//@remarks Estimation of the noise from the point clouds does not work, as the pose graph edge strength is determined by the inlier count. A high scene noise means a high inlier threshold, and a strong edge, instead of a weak one.
// 			 Using the estimate covariance for edge strength does not work, as the point clouds (and the point cloud pairs) are at different scales
//Or is it?
class SfMPipelineC
{
	private:

		typedef PoseGraphPipelineC<Homography3DPipelineProblemT> PoseGraphEngineT;	///< A projective pose graph solver
		typedef PoseGraphEngineT::pose_graph_type PoseGraphT;	///< A pose graph
		typedef PoseGraphEngineT::edge_container_type PoseGraphEdgeContainerT;	///< An edge container for a pose graph
		typedef PoseGraphEngineT::feature_container_type SceneFeatureListT;	///< A list of scene features

		typedef VisionGraphPipelineC<Fundamental7PipelineProblemT>::feature_container_type ImageFeatureListT;	///< A container for image features
		typedef VisionGraphPipelineC<Fundamental7PipelineProblemT>::edge_container_type EpipolarGraphEdgeListT;	///< An edge container for a vision graph
		typedef VisionGraphPipelineC<Fundamental7PipelineProblemT>::rng_type RNGT;	///< Random number generator type

		typedef CorrespondenceFusionC::pair_id_type PairIdT;	///< A pair of image ids
		typedef CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> PairwiseViewT;	///< A set of pairwise correspondences
		typedef CorrespondenceFusionC::cluster_type FeatureTrackT;	///< Ids for a collection of features belonging to the same 3D point
		typedef CorrespondenceFusionC::unified_view_type FeatureTrackStackT;	///< A stack of feature tracks


		typedef MultivariateGaussianC<PoseMinimalDifferenceC, P3PSolverC::DoF()>::covariance_type PoseCovarianceMatrixT;	///< Pose covariance matrix
		typedef MultiviewTriangulationC::covariance_matrix_type PointCovarianceMatrixT;	///< Covariance matrix for a scene point

		/** @name Implementation details */ //@{
		static void ValidateInput(const vector<ImageFeatureListT>& featureStack, const vector<CameraC>& cameraList,  SfMPipelineParametersC& parameters);	///< Validates the algorithm input
		static tuple<map<size_t, CameraMatrixT>, vector<IntrinsicCalibrationMatrixT>, vector<bool>> ProcessCameras(const vector<CameraC>& cameraList, const SfMPipelineParametersC& parameters);	///< Processes the cameras

		static tuple<FeatureTrackStackT, PairwiseViewT> PerformCorrespondenceFusion(const CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT>& correspondenceStack);	///< Performs correspondence fusion for a set of pairwise correspondences
		static vector<size_t> TestCheiralityAndDegeneracy(const vector<Coordinate3DT>& pointCloud, const CameraMatrixT& mP1, const CameraMatrixT& mP2, bool flagMetric, const SfMPipelineParametersC& parameters);	///< Applies the cheirality and degeneracy tests to a point cloud
		static SceneFeatureC MakeSceneFeature(const Coordinate3DT& coordinate, const FeatureTrackT& featureTrack, const vector<ImageFeatureListT>& featureStack);	///< Attaches appearance descriptors to a scene point
		static double EstimateSceneNoise(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const CoordinateCorrespondenceList2DT& correspondences, double imageNoiseVariance, const SfMPipelineParametersC& parameters);	///< Estimates the variance of the coordinate noise affecting a point cloud

		typedef bimap<vector_of<size_t>, vector_of<PairIdT>, left_based > SceneEdgeIndexMapT;	///< An index map between pairwise reconstruction ids and edge ids
		static tuple<vector<SceneFeatureListT>, vector<double>, SceneEdgeIndexMapT, map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT>> > FormPairwiseSceneModels(const EpipolarGraphEdgeListT& edgeList, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoiseVector, bool flagMetric,  const SfMPipelineParametersC& parameters);	///< Builds a scene model from each epipolar geometry estimate
		static PoseGraphT MakeGraph(const PoseGraphEdgeContainerT& pgEdgeList, double maxStrength,  const SfMPipelineParametersC& parameters);	///< Makes a pose graph from an edge list
		static tuple<PoseGraphT, PoseGraphEdgeContainerT> AugmentPoseGraph(const PoseGraphEdgeContainerT& pgEdgeList, const EpipolarGraphEdgeListT& egEdgeList, const SceneEdgeIndexMapT& sceneEdgeIndexMap, const map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, size_t offset, bool flagMetric,  const SfMPipelineParametersC& parameters);	///< Augments a pose graph with camera vertices
		static vector<map<size_t, CameraMatrixT> > RegisterToScenes(const PoseGraphT& poseGraph, const PoseGraphEdgeContainerT& pgEdgeList, const EpipolarGraphEdgeListT& egEdgeList, const SceneEdgeIndexMapT& sceneEdgeIndexMap, const map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, size_t lastSceneVertexId, size_t offset);	///< Registers the camera nodes to the references frames represented by the scene nodes
		template<class EpipolarGeometryEstimationPipelineProblemT, class PoseEstimationPipelineProblemT> static tuple< vector<map<size_t, CameraMatrixT>>, PoseGraphEdgeContainerT, EpipolarGraphEdgeListT, map<pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT>> > FormCameraSequences(RNGT& rng, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoise, bool flagMetric, const SfMPipelineParametersC& parameters);	///< Forms a set of projective or metric camera sequences

		static map<size_t, IntrinsicCalibrationMatrixT> EstimateIntrinsics(RNGT& rng, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack, const vector<IntrinsicCalibrationMatrixT>& mKList, const SfMPipelineParametersC& parameters );	///< Estimates the intrinsic calibration parameters

		static tuple<map<size_t, CameraMatrixT>, vector<ImageFeatureListT> > PrepareMetricSfM(const map<size_t, IntrinsicCalibrationMatrixT>& mKList, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack);	///< Prepares the input data for metric SfM
		static PoseCovarianceMatrixT MakePoseCovariance(const Coordinate3DT& vC, const SfMPipelineParametersC& parameters);	///< Makes an approximate pose covariance matrix

		static PDLPoseGraphProblemC::observation_set_type MakePoseObservations(const EpipolarGraphEdgeListT& egEdgeList, const map<size_t, CameraMatrixT>& cameraList, const map<pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT>>& cameraPairStack, const SfMPipelineParametersC& parameters);	///< Makes pose observations for the pose graph optimiser

		static map<size_t, CameraMatrixT> RefinePose(const EpipolarGraphEdgeListT& egEdgeList, const map<size_t, CameraMatrixT>& cameraList, const map<pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, const SfMPipelineParametersC& parameters);	///< Refines the estimated camera poses via pose graph optimisation
		static tuple<vector<Coordinate3DT>, map<size_t, CoordinateCorrespondenceList32DT>> PerformMultiviewTriangulation(const map<size_t, CameraMatrixT>& cameraList, const EpipolarGraphEdgeListT& egEdgeList, const vector<vector<Coordinate2DT> >& imageCoordinateStack, const vector<double>& imageNoiseVector, const SfMPipelineParametersC& parameters );	///< Performs multiview triangulation
		static  tuple<map<size_t, CameraMatrixT>, vector<Coordinate3DT> >  EstimatePose(RNGT& rng, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoise, const SfMPipelineParametersC& parameters );	///< Estimates the camera poses

		static tuple<vector<SceneFeatureC>, vector<PointCovarianceMatrixT> > BuildGlobalSceneModel(const map<size_t, CameraMatrixT>& cameraList, const vector<ImageFeatureListT>& featureStack, const SfMPipelineParametersC& parameters);	///< Builds a multiview 3D scene model with appearance descriptors, for robust registration
		template<class GeometryEstimationProblemT> static tuple<map<size_t, CameraMatrixT>, map<size_t, CoordinateCorrespondenceList32DT> > PerformRobustRegistration(const map<size_t, CameraMatrixT>& cameraList, const vector<SceneFeatureC>& sceneModel, const vector<PointCovarianceMatrixT>& sceneCovariance, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoiseVector, const SfMPipelineParametersC& parameters);	///< Robust PnP estimation
		static tuple<map<size_t, CameraMatrixT>, map<size_t, CoordinateCorrespondenceList32DT> > DenormaliseGeometry(const map<size_t, CameraMatrixT>& cameraList, const map<size_t, CoordinateCorrespondenceList32DT>& correspondences, const map<size_t, IntrinsicCalibrationMatrixT>& mKList);	///< Denormalises the cameras and the correspondences

		static tuple<map<size_t, CameraMatrixT>, vector<Coordinate3DT> > ApplyBundleAdjustment(const map<size_t, CameraMatrixT>& cameraList, const map<size_t, CoordinateCorrespondenceList32DT>& correspondenceStack, const SfMPipelineParametersC& parameters);	///< Applies bundle adjustment
		//@}

	public:

		typedef RNGT rng_type;	///< RNG type
		static SfMPipelineDiagnosticsC Run(map<size_t, CameraMatrixT>& result, vector<Coordinate3DT>& world, RNGT& rng, const vector<ImageFeatureListT>& featureStack, const vector<CameraC>& cameraList, SfMPipelineParametersC parameters);	///< Runs the algorithm
};	//class SfMPipelineC

/**
 * @brief Forms a set of projective or metric camera sequences
 * @tparam EpipolarGeometryEstimationPipelineProblemT	Type of the epipolar geometry estimation problem
 * @tparam PoseEstimationPipelineProblemT Type of the pose estimation problem
 * @param[in, out] rng Random number generator
 * @param[in] mPList Initial camera estimates
 * @param[in] featureStack Image features
 * @param[in] imageNoise Image noise corrupting the coordinates
 * @param[in] flagMetric If called for metric pose estimation, \c true
 * @param[in] parameters Parameters
 * @return A tuple: Camera sequences;pose graph edges; epipolar graph edges; camera pairs
 */
template<class EpipolarGeometryEstimationPipelineProblemT, class PoseEstimationPipelineProblemT>
auto SfMPipelineC::FormCameraSequences(RNGT& rng,const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoise, bool flagMetric, const SfMPipelineParametersC& parameters) -> tuple< vector<map<size_t, CameraMatrixT>>, PoseGraphEdgeContainerT, EpipolarGraphEdgeListT, map<pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT>> >
{
	//Vision graph
	typedef VisionGraphPipelineC<EpipolarGeometryEstimationPipelineProblemT> EpipolarGraphPipelineT;
	typename EpipolarGraphPipelineT::vision_graph_type epipolarGraph(0);
	EpipolarGraphEdgeListT egEdgeList;
	VisionGraphPipelineDiagnosticsC vgDiagnostics=EpipolarGraphPipelineT::Run(epipolarGraph, egEdgeList, rng, featureStack, mPList, imageNoise, parameters.vgParameters);


	tuple< vector<map<size_t, CameraMatrixT>>, PoseGraphEdgeContainerT, EpipolarGraphEdgeListT, map<pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT>> > dummy;
	if(!vgDiagnostics.flagSuccess || egEdgeList.empty())
		return dummy;

	//Pairwise scene models
	vector<SceneFeatureListT> sceneFeatureStack;
	vector<double> sceneNoise;
	SceneEdgeIndexMapT sceneEdgeIndexMap;
	map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> > cameraPairStack;
	std::tie(sceneFeatureStack, sceneNoise, sceneEdgeIndexMap, cameraPairStack)=FormPairwiseSceneModels(egEdgeList, featureStack, imageNoise, flagMetric, parameters);

	//Pose graph
	typedef PoseGraphPipelineC<PoseEstimationPipelineProblemT> PoseGraphPipelineT;
	PoseGraphT poseGraph(0);
	PoseGraphEdgeContainerT pgEdgeList;
	PoseGraphPipelineDiagnosticsC pgDiagnostics=PoseGraphPipelineT::Run(poseGraph, pgEdgeList, rng, sceneFeatureStack, map<size_t, Homography3DT>(), sceneNoise, parameters.pgParameters);

	if(!pgDiagnostics.flagSuccess || pgEdgeList.empty())
		return dummy;

	PoseGraphT augmentedPoseGraph(0);
	PoseGraphEdgeContainerT augmentedPGEdgeList;
	std::tie(augmentedPoseGraph, augmentedPGEdgeList)=AugmentPoseGraph(pgEdgeList, egEdgeList, sceneEdgeIndexMap, cameraPairStack, sceneFeatureStack.size(), flagMetric, parameters);

	//For each scene, form the corresponding camera sequences
	vector<map<size_t, CameraMatrixT> > cameraSequences=RegisterToScenes(augmentedPoseGraph, augmentedPGEdgeList, egEdgeList, sceneEdgeIndexMap, cameraPairStack, num_vertices(poseGraph)-1, sceneFeatureStack.size());

	return make_tuple(move(cameraSequences), std::move(pgEdgeList), std::move(egEdgeList), move(cameraPairStack));
}	//auto BuildPoseGraph(const map<size_t, CameraMatrixT>& mPList, const vector<FeatureListT>& featureStack, const SfMPipelineParametersC& parameters) -> tuple<PoseGraphT, PoseGraphEdgeContainerT>

/**
 * @brief Robust PnP estimation
 * @tparam GeometryEstimationProblemT Geometry estimation problem type
 * @param[in] cameraList Camera list
 * @param[in] sceneModel Scene model
 * @param[in] sceneCovariance Covariance matrices for the scene points
 * @param[in] featureStack Image features
 * @param[in] imageNoiseVector Image noise corrupting the coordinates
 * @param[in] parameters Paremters
 * @pre \c GeometryEstimationProblemT is a model of \c GeometryEstimatorProblemConceptC
 * @return A tuple: Estimated cameras; inliers
 */
template<class GeometryEstimationProblemT>
tuple<map<size_t, CameraMatrixT>, map<size_t, CoordinateCorrespondenceList32DT> > SfMPipelineC::PerformRobustRegistration(const map<size_t, CameraMatrixT>& cameraList, const vector<SceneFeatureC>& sceneModel, const vector<PointCovarianceMatrixT>& sceneCovariance, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoiseVector, const SfMPipelineParametersC& parameters)
{

	size_t nCameras=featureStack.size();
	map<size_t, CameraMatrixT> registeredCameras;
	map<size_t, CoordinateCorrespondenceList32DT> supportStack;

	auto itE = cameraList.end();

	vector<Coordinate3DT> coordinateList3=MakeCoordinateVector(sceneModel);
	typename GeometryEstimationProblemT::ransac_problem_type2::dimension_type1 binSize1=*ComputeBinSize<Coordinate3DT>(coordinateList3, parameters.registration32Parameters.binDensity1);

	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(dynamic) private(c)  num_threads(parameters.nThreads)
	for(c=0; c<nCameras; ++c)
	{
		typedef typename GeometryEstimationProblemT::ransac_problem_type1 RANSACProblem1T;
		typedef typename GeometryEstimationProblemT::ransac_problem_type2 RANSACProblem2T;

		unsigned int loGeneratorSize1=RANSACProblem1T::minimal_solver_type::GeneratorSize()*parameters.loGeneratorRatio1;
		unsigned int loGeneratorSize2=RANSACProblem2T::minimal_solver_type::GeneratorSize()*parameters.loGeneratorRatio2;

		vector<Coordinate2DT> coordinateList=MakeCoordinateVector(featureStack[c]);
		typename RANSACProblem2T::dimension_type2 binSize2=*ComputeBinSize<Coordinate2DT>(coordinateList, parameters.registration32Parameters.binDensity2);

		typename RANSACProblem2T::data_container_type dummyList;
		RANSACProblem1T ransacProblem1=MakeRANSACPnPProblem<typename GeometryEstimationProblemT::ransac_problem_type1::minimal_solver_type, typename GeometryEstimationProblemT::ransac_problem_type1::lo_solver_type>(dummyList, dummyList, imageNoiseVector[c], parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSize1, binSize2);
		RANSACProblem2T ransacProblem2=MakeRANSACPnPProblem<typename GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type, typename GeometryEstimationProblemT::ransac_problem_type2::lo_solver_type>(dummyList, dummyList, imageNoiseVector[c], parameters.inlierRejectionProbability, 1, loGeneratorSize2, binSize1, binSize2);

		//PDL problem
		typedef typename GeometryEstimationProblemT::optimisation_problem_type PDLProblemT;
		PDLProblemT pdlProblem=MakePDLPnPProblem<PDLProblemT>(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

		//Geometry estimator problem
		optional<CameraMatrixT> initialEstimate;
		auto itCL=cameraList.find(c);
		if(itCL!=itE)
			initialEstimate=itCL->second;

		GeometryEstimationProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, pdlProblem, initialEstimate);

		//Geometry estimation pipeline problem
		typedef typename P3PPipelineProblemT::matching_problem_type MatchingProblemT;
		typedef GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, MatchingProblemT> PipelineProblemT;
		PipelineProblemT problem=MakeGeometryEstimationPipelinePnPProblem<GeometryEstimationProblemT, MatchingProblemT>(sceneModel, featureStack[c], geometryEstimationProblem, sceneCovariance, imageNoiseVector[c], parameters.inlierRejectionProbability, initialEstimate , imageNoiseVector[c]*(1+parameters.predictionNoise), parameters.inlierRejectionProbability, 1, false);

		CameraMatrixT mP;
		optional<MatrixXd> covariance;
		CorrespondenceListT inliers;
		optional<MatchingProblemT> matchingProblem;
		RNGT rng; rng.seed(c*100);
		GeometryEstimationPipelineDiagnosticsC diagnostics=GeometryEstimationPipelineC<PipelineProblemT>::Run(mP, covariance, inliers, problem, matchingProblem, rng,parameters.registration32Parameters);

		CoordinateCorrespondenceList32DT inlierCoordinates=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList32DT>(inliers, coordinateList3, coordinateList);

	#pragma omp critical(SfM_PRR1)
		if(diagnostics.flagSuccess)
		{
			registeredCameras.emplace(c, mP);
			supportStack.emplace(c, inlierCoordinates);
		}	//if(diagnostics.flagSuccess)
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(move(registeredCameras), move(supportStack));

}	//tuple<map<size_t, CameraMatrixT>, map<size_t, CoordinateCorrespondenceList32DT> > PerformRobustRegistration(const map<size_t, CameraMatrixT>& cameraList, vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoiseVector, const SfMPipelineParametersC& parameters)

}	//namespace GeometryN
}	//namespace SeeSawN



#endif /* SFM_PIPELINE_IPP_6709012 */
