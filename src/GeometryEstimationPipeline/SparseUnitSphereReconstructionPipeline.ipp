/**
 * @file SparseUnitSphereReconstructionPipeline.ipp Implementation details for the sparse unit sphere reconstruction pipeline
 * @author Evren Imre
 * @date 21 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef SPARSE_UNIT_SPHERE_RECONSTRUCTION_PIPELINE_IPP_7780021
#define SPARSE_UNIT_SPHERE_RECONSTRUCTION_PIPELINE_IPP_7780021

#include <Eigen/Dense>
#include <boost/lexical_cast.hpp>
#include <cstddef>
#include <vector>
#include <map>
#include <tuple>
#include <iostream>
#include <stdexcept>
#include <string>
#include <queue>
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Elements/Correspondence.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Matcher/MultisourceFeatureMatcher.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/Similarity.h"
#include "../Geometry/MultiviewPanoramaBuilder.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::lexical_cast;
using Eigen::Matrix3d;
using std::size_t;
using std::vector;
using std::map;
using std::tuple;
using std::get;
using std::tie;
using std::ignore;
using std::cout;
using std::invalid_argument;
using std::string;
using std::priority_queue;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraToHomography;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::MatcherN::MultisourceFeatureMatcherC;
using SeeSawN::MatcherN::MultisourceFeatureMatcherParametersC;
using SeeSawN::MatcherN::MultisourceFeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::BinaryImageFeatureMatchingProblemC;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::MetricsN::SymmetricTransferErrorConstraint2DT;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderC;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderParametersC;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderDiagnosticsC;
using SeeSawN::GeometryN::CartesianToSpherical;

/**
 * @brief Sparse unit sphere reconstruction pipeline parameters
 * @ingroup Parameters
 */
struct SparseUnitSphereReconstructionPipelineParametersC
{
	double noiseVariance;	///< Variance of the noise on the image coordinates. >0
	double pRejection;	///< Probability of rejection for an inlier correspondence.(0,1)
	bool flagVerbose;	///< Verbose operation
	bool flagCompact;	///< If \c true, compact descriptors

	MultisourceFeatureMatcherParametersC matcherParameters;	///< Parameters for the matcher
	MultiviewPanoramaBuilderParametersC panoramaParameters;	///< Parameters for the panorama builder

	SparseUnitSphereReconstructionPipelineParametersC() : noiseVariance(4), pRejection(0.05),flagVerbose(false), flagCompact(false)
	{}
};	//struct SparseUnitSphereReconstructionPipelineParametersC

/**
 * @brief Sparse unit sphere reconstruction pipeline diagnostics
 * @ingroup Diagnostics
 */
struct SparseUnitSphereReconstructionPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation is completed successfully

	MultisourceFeatureMatcherDiagnosticsC matcherDiagnostics;	///< Diagnostics for the matcher
	MultiviewPanoramaBuilderDiagnosticsC panoramaDiagnostics;	///< Diagnostics for the panorama builder

	SparseUnitSphereReconstructionPipelineDiagnosticsC() : flagSuccess(false)
	{}
};	//struct SparseUnitSphereReconstructionPipelineDiagnosticsC

/**
 * @brief Sparse unit sphere reconstruction pipeline
 * @remarks Compact descriptor: The 3D point is described by the image observation that is closest to the median desriptor
 * @ingroup Algorithm
 * @nosubgrouping
 */
class SparseUnitSphereReconstructionPipelineC
{
	private:

		typedef tuple<size_t, size_t> SizePairT;	///< A pair of size_t variables

		/** @name Implementation Details */ //@{
		static void ValidateParameters(SparseUnitSphereReconstructionPipelineParametersC& parameters);	///< Verifies the validity of the parameters

		static map<SizePairT, SymmetricTransferErrorConstraint2DT> MakePairwiseConstraints(const vector<CameraMatrixT>& cameras, const SparseUnitSphereReconstructionPipelineParametersC& parameters);	///< Initialises the pairwise epipolar constraints

		typedef MultisourceFeatureMatcherC::unified_view_type UnifiedViewT;
		typedef MultisourceFeatureMatcherC::pairwise_view_type PairwiseViewT;

		static vector<OrientedBinarySceneFeatureC> MakeSceneFeature(const vector<Coordinate3DT>& reconstruction, const UnifiedViewT& correspondences, const vector<vector<BinaryImageFeatureC> >& features);	///< Attaches descriptors to the scene points
		static void MakeOriented(vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameraList);	///< Conversion from binary scene features to oriented scene features
		//@}

	public:

		template<class SimilarityT> static SparseUnitSphereReconstructionPipelineDiagnosticsC Run(vector<Coordinate3DT>& pointCloud,  vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const SimilarityT& similarityMetric, SparseUnitSphereReconstructionPipelineParametersC parameters);	///< Runs the algorithm

};	//class SparseUnitSphereReconstructionPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Runs the algorithm
 * @param[out] pointCloud 3D reconstruction
 * @param[out] sceneFeatures Scene features
 * @param[in] cameras Cameras
 * @param[in] features Image features
 * @param[in] similarityMetric Image feature similarity metric
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 * @pre \c features and \c cameras have the same number of elements
 * @remarks Algorithm forces consistent and 1:1 matching
 */
template<class SimilarityT>
SparseUnitSphereReconstructionPipelineDiagnosticsC SparseUnitSphereReconstructionPipelineC::Run(vector<Coordinate3DT>& pointCloud,  vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const SimilarityT& similarityMetric, SparseUnitSphereReconstructionPipelineParametersC parameters)
{
	//Preconditions
	assert(cameras.size()==features.size());

	ValidateParameters(parameters);

	SparseUnitSphereReconstructionPipelineDiagnosticsC diagnostics;

	if(cameras.empty() || features.empty())
		return diagnostics;

	//Constraints
	map<SizePairT, SymmetricTransferErrorConstraint2DT> constraints=MakePairwiseConstraints(cameras, parameters);

	//Matching

	UnifiedViewT clusters;
	PairwiseViewT observedCorrespondences;
	PairwiseViewT impliedCorrespondences;
	typedef BinaryImageFeatureMatchingProblemC<SimilarityT, SymmetricTransferErrorConstraint2DT> MatchingProblemT;	// Type of the feature matching problem
	diagnostics.matcherDiagnostics=MultisourceFeatureMatcherC::Run<MatchingProblemT>(clusters, observedCorrespondences, impliedCorrespondences, features, constraints, similarityMetric, parameters.matcherParameters);

	if(!diagnostics.matcherDiagnostics.flagSuccess)
		return diagnostics;

	if(parameters.flagVerbose)
	{
		size_t nCameras=cameras.size();
		auto itE1=observedCorrespondences.end();
		auto itE2=impliedCorrespondences.end();

		for(size_t c1=0; c1<nCameras; ++c1)
			for(size_t c2=c1+1; c2<nCameras; ++c2)
			{
				auto it1=observedCorrespondences.find(SizePairT(c1,c2));
				auto it2=impliedCorrespondences.find(SizePairT(c1,c2));

				if(it1!=itE1 || it2!=itE2)
				{
					cout<<"Pair "<<c1<<" "<<c2;

					if(it1!=itE1)
						cout<<" Observed: "<<it1->second.size();

					if(it2!=itE2)
						cout<<" Implied: "<<it2->second.size();

					cout<<"\n";
				}	//if(it1!=itE1 || it2!=itE2)
			}	//for(size_t c2=c1+1; c2<nCameras; ++c2)
	}	//if(parameters.flagVerbose)

	PairwiseViewT spliced=CorrespondenceFusionC::SplicePairwiseViews(observedCorrespondences, impliedCorrespondences);	//Merge the observed and the implied correspondences

	//Panorama

	size_t nCameras=cameras.size();
	vector<vector<Coordinate2DT> > coordinates(nCameras);
	for(size_t c=0; c<nCameras; ++c)
		coordinates[c]=MakeCoordinateVector(features[c]);

	MultiviewPanoramaBuilderC::correspondence_stack_type correspondences;
	for(const auto& currentPair : spliced)
	{
		CorrespondenceListT tmp;
		for(const auto& current : currentPair.second)
			tmp.push_back(CorrespondenceListT::value_type(current.left, current.right, MatchStrengthC(1,0)) );

		correspondences[currentPair.first]=tmp;
	}	//for(const auto& currentPair : spliced)

	vector<Coordinate3DT> reconstruction;
	UnifiedViewT inliers;
	diagnostics.panoramaDiagnostics=MultiviewPanoramaBuilderC::Run(reconstruction, inliers, correspondences, coordinates, cameras, parameters.panoramaParameters);

	if(!diagnostics.panoramaDiagnostics.flagSuccess)
		return diagnostics;

	//Attach descriptors
	vector<OrientedBinarySceneFeatureC> features3D=MakeSceneFeature(reconstruction, inliers, features);

	//Orientations
	MakeOriented(features3D, cameras);

	//Compact
	if(parameters.flagCompact)
		for_each(features3D, [](OrientedBinarySceneFeatureC& current){current.MakeCompact();});

	//Output
	pointCloud.swap(reconstruction);
	sceneFeatures.swap(features3D);

	diagnostics.flagSuccess=true;
	return diagnostics;
}	//void Run(const vector<CameraMatrixT>& cameras, const vector<vector<ImageFeatureC> >& features, const SimilarityT& similarityMetric)

}	//namespace GeometryN
}	//namespace SeeSawN




#endif /* SPARSE_UNIT_SPHERE_RECONSTRUCTION_PIPELINE_IPP_7780021 */
