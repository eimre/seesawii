/**
 * @file GeometryEstimator.ipp Implementation of GeometryEstimatorC
 * @author Evren Imre
 * @date 24 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_ESTIMATOR_IPP_9790212
#define GEOMETRY_ESTIMATOR_IPP_9790212

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <cstddef>
#include <vector>
#include <tuple>
#include <climits>
#include <stdexcept>
#include <iterator>
#include "GeometryEstimatorProblemConcept.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../Optimisation/PowellDogLeg.h"

namespace SeeSawN
{
namespace GeometryN
{

using std::vector;
using std::size_t;
using std::tie;
using std::get;
using std::numeric_limits;
using std::invalid_argument;
using std::next;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::RANSACN::TwoStageRANSACDiagnosticsC;
using SeeSawN::RANSACN::TwoStageRANSACParametersC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PowellDogLegDiagnosticsC;
using SeeSawN::OptimisationN::PowellDogLegParametersC;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;

/**
 * @brief Parameters for GeometryEstimatorC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct GeometryEstimatorParametersC
{
	TwoStageRANSACParametersC ransacParameters;	///< Parameters for RANSAC
	PowellDogLegParametersC pdlParameters;	///< Parameters for nonlinear optimisation
};	//struct GeometryEstimatorParametersC

/**
 * @brief Diagnostics for GeometryEstimatorC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct GeometryEstimatorDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully
	double error;	///< A measure of the quality of the estimated model

	TwoStageRANSACDiagnosticsC ransacDiagnostics;	///< Diagnostics for RANSAC
	bool flagLO;	///< \c true if the resulting model is computed via the LO solver
	bool flagStage2;	///< \c true if the resulting model is computed in the second stage of RANSAC

	PowellDogLegDiagnosticsC pdlDiagnostics;	///< Diagnostics for nonlinear optimisation

	GeometryEstimatorDiagnosticsC() : flagSuccess(false), error(numeric_limits<double>::infinity()), flagLO(false), flagStage2(false)
	{}
};	//struct GeometryEstimatorDiagnosticsC

/**
 * @brief Estimation of the geometry relating a set of correspondences
 * @tparam ProblemT A robust geometry estimation problem
 * @pre \c ProblemT is an instance of GeometryEstimatorProblemC
 * @remarks Pipeline:
 * - RANSAC for outlier elimination
 * - RANSAC for model estimation
 * - Nonlinear minimisation
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class GeometryEstimatorC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometryEstimatorProblemConceptC<ProblemT>));
	//@endcond

	private:

		typedef TwoStageRANSACC<typename ProblemT::ransac_problem_type1, typename ProblemT::ransac_problem_type2> RANSACT;	///< RANSAC type
		typedef typename RANSACT::rng_type RNGT;	///< Random number generator type

	public:

		typedef RNGT rng_type;	///< Random number generator type

		typedef typename ProblemT::ransac_problem_type2::minimal_solver_type::model_type result_type;	///< A model
		static GeometryEstimatorDiagnosticsC Run(result_type& model, vector<size_t>& inliers, vector<size_t>& generator, ProblemT& problem, RNGT& rng, GeometryEstimatorParametersC parameters);	///< Runs the algorithm

};	//class GeometryEstimatorC

/**
 * @brief Runs the algorithm
 * @param[out] model Estimated model
 * @param[out] inliers Inlier observation indices
 * @param[out] generator Indices of the correspondences that generated the best RANSAC model
 * @param[in,out] problem Problem
 * @param[in,out] rng Random number generator
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 */
template<class ProblemT>
GeometryEstimatorDiagnosticsC GeometryEstimatorC<ProblemT>::Run(result_type& model, vector<size_t>& inliers, vector<size_t>& generator, ProblemT& problem, RNGT& rng, GeometryEstimatorParametersC parameters)
{
	if(!problem.IsValid())
		throw(invalid_argument("GeometryEstimator::Run: Invalid problem."));

	//Parameters
	parameters.pdlParameters.flagCovariance=false;

	GeometryEstimatorDiagnosticsC diagnostics;

	//RANSAC
	typename RANSACT::result_type resultRANSAC;
	diagnostics.ransacDiagnostics=RANSACT::Run(resultRANSAC, problem.RANSACProblem1(), problem.RANSACProblem2(), rng, parameters.ransacParameters);

	if(!diagnostics.ransacDiagnostics.flagSuccess)
		return diagnostics;
	else
		diagnostics.flagSuccess=true;

	result_type ransacModel;
	unsigned int chosenIndex;	//Index of the chosen model
	tie(ransacModel, chosenIndex, inliers, generator)=problem.ProcessRANSACResult(resultRANSAC);

	//Save the properties of the original RANSAC solution
	diagnostics.flagStage2= (!diagnostics.ransacDiagnostics.iStage1Solution) || (chosenIndex!=*diagnostics.ransacDiagnostics.iStage1Solution);
	diagnostics.flagLO = get<RANSACT::iFlagLO>(next(resultRANSAC.begin(), chosenIndex)->second);

	//Nonlinear minimisation

	typename ProblemT::optimisation_problem_type pdlProblem=problem.MakeOptimisationProblem(ransacModel, inliers);

	typedef PowellDogLegC<typename ProblemT::optimisation_problem_type> PDLT;
	result_type pdlModel;
	typename PDLT::covariance_type dummy;

	if(pdlProblem.IsValid())	//If the correspondence set is too small, the problem might be invalid
		diagnostics.pdlDiagnostics=PDLT::Run(pdlModel, dummy, pdlProblem, parameters.pdlParameters);

	//Evaluation

	double errorPDL= (diagnostics.pdlDiagnostics.flagSuccess) ? problem.EvaluateModel(pdlModel) : numeric_limits<double>::infinity();
	double errorRANSAC=problem.EvaluateModel(ransacModel);

	model= (errorPDL<errorRANSAC) ? pdlModel : ransacModel;
	diagnostics.error = (errorPDL<errorRANSAC) ? errorPDL : errorRANSAC;

	return diagnostics;
}	//GeometryEstimatorDiagnosticsC Run(ModelT& model, list<size_t>& inliers, list<size_t>& generators, ProblemT& problem, GeometryEstimatorParametersC parameters);	///< Runs the algorithm

}	//GeometryN
}	//SeeSawN

#endif /* GEOMETRY_ESTIMATOR_IPP_9790212 */
