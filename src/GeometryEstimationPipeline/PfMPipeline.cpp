/**
 * @file PfMPipeline.cpp Implementation of the panorama-from-motion pipeline
 * @author Evren Imre
 * @date 12 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PfMPipeline.h"

namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the input parameters
 * @param[in] featureStack Image features
 * @param[in] cameraList Camera list
 * @param[in] parameters Parameters
 * @pre \c cameraList.size()==featureStack.size();
 * @pre \c cameraList[x].FrameBox is valid
 * @pre \c noiseVariance.size()==1 or \c noiseVariance.size()==featureStack.size()
 * @pre \c noiseVariance does not have any non-positive elements
 * @throws invalid_argument If the preconditions are violated
 */
void PfMPipelineC::ValidateInput(const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList, PfMPipelineParametersC& parameters)
{
	size_t nCameras=featureStack.size();

	//cameraList
	if(cameraList.size()!=nCameras)
		throw(invalid_argument(string("PfMPipelineC::ValidateInput : cameraList must have an element for each item in featureStack. Value=") + lexical_cast<string>(nCameras) ));

	if(any_of(cameraList.begin(), cameraList.end(), [](const CameraC& current){return !current.FrameBox();} ))
		throw(invalid_argument("PfMPipelineC::ValidateInput : Each element of cameraList must have a valid FrameBox"));

	//noiseVariance
	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("PfMPipelineC::ValidateInput : noiseVariance must be positive. Value=") + lexical_cast<string>(parameters.noiseVariance) ));

	//Parameters
	if(parameters.nThreads==0)
		throw(invalid_argument("PfMPipelineC::ValidateInput : parameters.nThreads must be a positive value") );

	if(parameters.geLOGeneratorRatio1<1)
		throw(invalid_argument(string("PfMPipelineC::ValidateInput : parameters.geLOGeneratorRatio1 must be >=1. Value=")+lexical_cast<string>(parameters.geLOGeneratorRatio1)) );

	if(parameters.geLOGeneratorRatio2<1)
		throw(invalid_argument(string("PfMPipelineC::ValidateInput : parameters.geLOGeneratorRatio2 must be >=1. Value=")+lexical_cast<string>(parameters.geLOGeneratorRatio2)) );

	if(parameters.maxFocalLengthRatioError<=0)
		throw(invalid_argument(string("PfMPipelineC::ValidateInput : parameters.maxFocalLengthRatioError must be a positive value. Value=")+lexical_cast<string>(parameters.maxFocalLengthRatioError)) );

	//Components
	parameters.visionGraphParameters.flagConnected=true;
	parameters.visionGraphParameters.nThreads=parameters.nThreads;
	parameters.visionGraphParameters.geometryEstimationPipelineParameters.flagCovariance=false;

	parameters.focalLengthRegistrationPipeline.nThreads=1;

	parameters.rotationRegistrationParameters.rstsParameters.nThreads=parameters.nThreads;

	parameters.panoramaBuilderParameters.noiseVariance=0;	//Automatically determined
	parameters.panoramaBuilderParameters.pReject=parameters.inlierRejectionProbability;

	parameters.geometryEstimatorParameters.ransacParameters.nThreads=1;
	parameters.geometryEstimatorParameters.ransacParameters.parameters1.flagHistory=false;
	parameters.geometryEstimatorParameters.ransacParameters.parameters2.flagHistory=false;
	parameters.geometryEstimatorParameters.pdlParameters.flagCovariance=false;

}	//void ValidateInput(const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList, const vector<double>& noiseVariance,  PfMPipelineParametersC& parameters)

/**
 * @brief Processes the cameras
 * @param[in] cameraList Camera list
 * @param[in] parameters Parameters
 * @return A tuple: Camera matrices, intrinsic calibration matrices, indicator array for known focal length values
 */
tuple<map<size_t, CameraMatrixT>, vector<IntrinsicCalibrationMatrixT>, vector<bool> > PfMPipelineC::ProcessCameras(const vector<CameraC>& cameraList, const PfMPipelineParametersC& parameters)
{
	Coordinate3DT origin(0,0,0);

	size_t nCameras=cameraList.size();
	vector<IntrinsicCalibrationMatrixT> mKList(nCameras);
	vector<bool> flagFArray(nCameras, false);
	map<size_t, CameraMatrixT> mPList;
	for(size_t c=0; c<nCameras; ++c)
	{
		if(!parameters.flagInitialEstimate)
		{
			mKList[c]=IntrinsicCalibrationMatrixT::Identity();
			mKList[c].topRightCorner(2,1)=cameraList[c].FrameBox()->center();
			continue;
		}	//if(!parameters.flagInitialEstimate)

		//Intrinsics
		bool flagIntrinsics= !!cameraList[c].Intrinsics();
		double f= (flagIntrinsics && cameraList[c].Intrinsics()->focalLength) ? *cameraList[c].Intrinsics()->focalLength : max( (cameraList[c].FrameBox()->sizes())[0], (cameraList[c].FrameBox()->sizes())[1]);
		double a= flagIntrinsics ? cameraList[c].Intrinsics()->aspectRatio : 1;
		double s= flagIntrinsics ? cameraList[c].Intrinsics()->skewness : 0;
		Coordinate2DT principalPoint = (flagIntrinsics &&  cameraList[c].Intrinsics()->principalPoint) ? *cameraList[c].Intrinsics()->principalPoint : cameraList[c].FrameBox()->center();

		mKList[c]<< f, s, principalPoint[0], 0, a*f, principalPoint[1], 0, 0, 1;

		flagFArray[c]=flagIntrinsics && parameters.flagKnownF;

		//Camera matrix
		if(flagIntrinsics && cameraList[c].Extrinsics()->orientation )
			mPList.emplace(c, ComposeCameraMatrix(mKList[c], origin, cameraList[c].Extrinsics()->orientation->toRotationMatrix()));
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(mPList, mKList, flagFArray);
}	//tuple<map<size_t, CameraMatrixT>, vector<IntrinsicCalibrationMatrixT> > ProcessCameras(const vector<CameraC>& cameraList, const PfMPipelineParametersC& parameters)

/**
 * @brief Lazy evaluation of local direction vectors
 * @param[in,out] directionVectors Direction vectors in the local coordinate frame
 * @param[in] imageCoordinates Image coordinates
 * @param[in] normalisedCoordinates Normalised image coordinates
 * @param[in] normaliser Normaliser
 */
void PfMPipelineC::ComputeLocalDirectionVectors(optional<vector<Coordinate3DT> >& directionVectors, const vector<Coordinate2DT>& imageCoordinates, const optional<vector<Coordinate2DT> >& normalisedCoordinates, const Matrix3d& normaliser)
{
	//Already evaluated
	if(directionVectors)
		return;

#pragma omp critical(PfM_CLDV1)
{
	directionVectors=vector<Coordinate3DT>(imageCoordinates.size());//Allocate memory

	if(normalisedCoordinates)
		transform(*normalisedCoordinates, directionVectors->begin(), [](const Coordinate2DT& current){return current.homogeneous().normalized();} );
	else
		transform(imageCoordinates, directionVectors->begin(), [&](const Coordinate2DT& current){return (normaliser * current.homogeneous()).normalized();});
}
}	//void ComputeLocalDirectionVectors(optional<vector<Coordinate3DT> >& directionVectors, const optional<vector<Coordinate2DT> >& normalisedCoordinates, const Matrix3d& normaliser)

/**
 * @brief Lazy evaluation of image coordinate normalisation
 * @param[in,out] normalisedCoordinates Normalised image coordinates
 * @param[in] imageCoordinates Image coordinates
 * @param[in] directionVectors Direction vectors in the local coordinate frame
 * @param[in] normaliser Normaliser
 */
void PfMPipelineC::ComputeNormalisedCoordinates(optional<vector<Coordinate2DT> >& normalisedCoordinates, const vector<Coordinate2DT>& imageCoordinates, const optional<vector<Coordinate3DT> >& directionVectors, const Matrix3d& normaliser)
{
	//Already evaluated
	if(normalisedCoordinates)
		return;

#pragma omp critical(PfM_CNC1)
	if(directionVectors)
	{
		normalisedCoordinates=vector<Coordinate2DT>(imageCoordinates.size());//Allocate memory
		transform(*directionVectors, normalisedCoordinates->begin(), [](const Coordinate3DT& current){return current.hnormalized();} );
	}
	else
	{
		CoordinateTransformations2DT::affine_transform_type mN; mN.matrix()=normaliser;
		normalisedCoordinates=CoordinateTransformations2DT::ApplyAffineTransformation(imageCoordinates, mN);
	}	//if(directionVectors)
}	//void ComputeNormalisedCoordinates(optional<vector<Coordinate2DT> >& normalisedCoordinates, const optional<vector<Coordinate3DT> >& directionVectors, const Matrix3d& normaliser)

/**
 * @brief Extracts a relative rotation measurement from a homography
 * @param[in] mHn Normalised homography
 * @param[in] correspondences Correspondences
 * @param directionVectors1 Direction vectors for the first image
 * @param directionVectors2 Direction vectors for the second image
 * @return 3D rotation matrix between two cameras
 */
RotationMatrix3DT PfMPipelineC::ExtractRelativeRotation(const Homography2DT& mHn, const CorrespondenceListT& correspondences, const vector<Coordinate3DT>& directionVectors1, const vector<Coordinate3DT>& directionVectors2)
{
	//Try the geometric solution

	CoordinateCorrespondenceList3DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList3DT >(correspondences, directionVectors1, directionVectors2);

	Rotation3DSolverC solver;
	list<Rotation3DSolverC::model_type> solution=solver(coordinateCorrespondences);

	if(!solution.empty())
		return solution.begin()->topLeftCorner(3,3);

	//Algebraic decomposition
	//Since both focal lengths are known and factored out, f1 and f2 should 1. This step compansates for imperfect H estimation
	optional<tuple<double, double, RotationMatrix3DT> > decomposition=DecomposeK2RK1i(mHn);

	if(decomposition)
		return get<2>(*decomposition);

	//Last resort, find the closest rotation matrix
	return ComputeClosestRotationMatrix(mHn);
}	//RotationMatrix3DT ExtractRelativeRotation(const Homography2DT& mHn, const CorrespondenceListT& correspondences, const vector<Coordinate3DT>& directionVectors1, const vector<Coordinate3DT>& directionVectors2)

/**
 * @brief Evaluates the fitness of a recomposed homography to the original correspondences
 * @param[in] mH Homography to be evaluated
 * @param[in] correspondences Correspondences
 * @param[in] coordinates1 Image coordinates for the first cameras
 * @param[in] coordinates2 Image coordinates for the second camera
 * @param[in] parameters Parameters
 * @return Percentage of correspondences that are retained by \c mH as inliers
 */
double PfMPipelineC::EvaluateRecomposedHomography(const Homography2DT& mH, const CorrespondenceListT& correspondences, const vector<Coordinate2DT>& coordinates1, const vector<Coordinate2DT>& coordinates2, const PfMPipelineParametersC& parameters)
{
	double inlierTh=SymmetricTransferErrorConstraint2DT::error_type::ComputeOutlierThreshold(parameters.visionGraphParameters.inlierRejectionProbability, parameters.noiseVariance);

	unsigned int inlierCount=0;
	SymmetricTransferErrorConstraint2DT validator(mH, inlierTh);
	for(const auto& current : correspondences)
		if(validator(coordinates1[current.left], coordinates2[current.right]))
			++inlierCount;

	return max(10*numeric_limits<double>::epsilon(), (double)inlierCount/correspondences.size());	//to ensure a positive weight
}	//double EvaluateRecomposedHomography(const Homography2DT& mH, const CorrespondenceListT& correspondences, const vector<Coordinate2DT>& coordinates1, const vector<Coordinate2DT>& coordinates2)

/**
 * @brief Extracts the focal length and relative orientation measurements
 * @param[in] edgeList Edge list
 * @param[in] mKList Initial estimates for the intrinsic calibration parameters
 * @param[in] fixedFArray Indicator array for known focal lengths
 * @param[in] coordinateStack Coordinate stack
 * @param[in] parameters Parameters
 * @return A vector of measurements
 */
auto PfMPipelineC::ExtractMeasurements(const EdgeContainerT& edgeList, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const CoordinateStackT& coordinateStack, const PfMPipelineParametersC& parameters) -> vector<MeasurementT>
{
	static constexpr unsigned int iModel=VisionGraphEngineT::iModel;
	static constexpr unsigned int iCorrespondenceList=VisionGraphEngineT::iCorrespondenceList;

	//Create a vector of pointers for parallel operation
	size_t nEdges=edgeList.size();
	vector<decltype(edgeList.cbegin()) > pEdgeList; pEdgeList.reserve(nEdges);
	auto itE=edgeList.cend();
	for(auto it=edgeList.cbegin(); it!=itE; std::advance(it,1))
		pEdgeList.push_back(it);

	//Normalisers
	size_t nCameras=mKList.size();
	vector<IntrinsicCalibrationMatrixT> normalisers; normalisers.reserve(nCameras);
	for_each(mKList, [&](const IntrinsicCalibrationMatrixT& current){normalisers.emplace_back(current.inverse());} );

	//Normalised coordinates, and local direction vectors, lazy evaluation
	vector<optional<vector<Coordinate3DT> > > localDirectionVectors(nCameras);	// Direction vectors in the local reference frame
	vector<optional<vector<Coordinate2DT> > > normalisedCoordinates(nCameras);	// Normalised image coordinates

	//For each edge
	vector<MeasurementT> output; output.reserve(nEdges);
	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(dynamic) private(c)  num_threads(parameters.nThreads)
	for(c=0; c<nEdges; ++c)
	{
		size_t i1=pEdgeList[c]->first.first;
		size_t i2=pEdgeList[c]->first.second;

		//Normalise the homography
		Homography2DT mH=get<iModel>(pEdgeList[c]->second);
		Homography2DT mHn=normalisers[i2] * mH * mKList[i1];

		//The homography has a sign ambiguity, but the determinant of K2RK1i must be positive
		if(mHn.determinant()<0)
			mHn*=-1;

		MeasurementT measurement(i1, i2, mKList[i1](0,0), mKList[i2](0,0), RotationMatrix3DT::Identity(), 0);
		Homography2DT mHRecomposed;	//The homography recomposed from the measurement

		//If both focal lengths are known
		if(fixedFArray[i1] && fixedFArray[i2])
		{
			//Lazy evaluation of direction vectors
			ComputeLocalDirectionVectors(localDirectionVectors[i1], coordinateStack[i1], normalisedCoordinates[i1], normalisers[i1]);
			ComputeLocalDirectionVectors(localDirectionVectors[i2], coordinateStack[i2], normalisedCoordinates[i2], normalisers[i2]);

			get<iRelativeRotation>(measurement)=ExtractRelativeRotation(mHn, get<iCorrespondenceList>(pEdgeList[c]->second), *localDirectionVectors[i1], *localDirectionVectors[i2]);

			mHRecomposed=mKList[i2]*get<iRelativeRotation>(measurement)*normalisers[i1];
		}	//if(fixedFArray[i1] && fixedFArray[i2])
		else
		{
			//Decompose the homography

			//Lazy evaluation of normalised coordinates
			ComputeNormalisedCoordinates(normalisedCoordinates[i1], coordinateStack[i1], localDirectionVectors[i1], normalisers[i1]);
			ComputeNormalisedCoordinates(normalisedCoordinates[i2], coordinateStack[i2], localDirectionVectors[i2], normalisers[i2]);

			optional<tuple<double, double, RotationMatrix3DT> > decomposition=DecomposeK2RK1i(mHn);

			if(!decomposition)
				continue;

			//Focal length measurements
			get<iFocalLength1>(measurement)=get<0>(*decomposition)*mKList[i1](0,0);
			get<iFocalLength2>(measurement)=get<1>(*decomposition)*mKList[i2](0,0);

			//Rotation estimation
			//Directly using the output of the decomposition results in a rotation matrix inconsistent with the correspondences
			//So, normalise the image coordinates with the estimated focal lengths, and then fit the optimal 3D rotation

			//Current intrinsic calibration matrix estimates
			IntrinsicCalibrationMatrixT mK1=mKList[i1]; mK1.topRows(2)*=get<0>(*decomposition);
			IntrinsicCalibrationMatrixT mK2=mKList[i2]; mK2.topRows(2)*=get<1>(*decomposition);

			IntrinsicCalibrationMatrixT mK1i=mK1.inverse();
			optional<vector<Coordinate3DT> > localDirections1;
			ComputeLocalDirectionVectors(localDirections1, coordinateStack[i1], optional<vector<Coordinate2DT>>(), mK1i);

			optional<vector<Coordinate3DT> > localDirections2;
			ComputeLocalDirectionVectors(localDirections2, coordinateStack[i2], optional<vector<Coordinate2DT>>(), mK2.inverse());

			get<iRelativeRotation>(measurement)=ExtractRelativeRotation(get<2>(*decomposition), get<iCorrespondenceList>(pEdgeList[c]->second), *localDirections1, *localDirections2);

			//Recompose the homography
			mHRecomposed=mK2*get<iRelativeRotation>(measurement)*mK1i;
		}	//if(fixedFArray[i1] && fixedFArray[i2])

		//Evaluate
		//Focal length measurement accuracy does not seem to correlate with the retention rate, actually. So, no filtering
		get<iQuality>(measurement)=EvaluateRecomposedHomography(mHRecomposed, get<iCorrespondenceList>(pEdgeList[c]->second), coordinateStack[i1], coordinateStack[i2], parameters);

	#pragma omp critical(PfM_EM1)
		output.push_back(measurement);
	}	//for(c=0; c<nEdges; ++c)

	return output;
}	//vector<MeasurementT> ExtractMeasurements(const EdgeContainerT& edgeList, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const CoordinateStackT& coordinateStack, const PfMPipelineParametersC& parameters)

/**
 * @brief Removes any measurements that is not a part of the largest connected component
 * @param[in] measurements Source
 * @param[in] nCameras Number of cameras
 * @return A tuple: [Filtered measurements; indices of the surviving cameras in the original set]
 */
auto PfMPipelineC::RemoveUnconnected(const vector<MeasurementT>& measurements, size_t nCameras) -> tuple<vector<MeasurementT>, IndexContainerT>
{
	//Find the largest connected component

	size_t nMeasurements=measurements.size();
	vector<pair<size_t, size_t> > edgeList(nMeasurements);
	transform(measurements, edgeList.begin(), [&](const MeasurementT& current){return make_pair(get<iCamera1>(current), get<iCamera2>(current)); } );

	vector<list<pair<size_t, size_t> > > components=FindConnectedComponents(edgeList, true);

	//Index maps
	set<size_t> survivors;
	for_each(components[0], [&](const pair<size_t, size_t>& current){survivors.insert(current.first); survivors.insert(current.second);}  );
	IndexContainerT indexMap(survivors.begin(), survivors.end());
	size_t nSurvivors=indexMap.size();

	vector<int> reverseIndexMap( nCameras, -1);
	for(size_t c=0; c<nSurvivors; ++c)
		reverseIndexMap[indexMap[c] ]=c;

	//Filter the measurements
	vector<MeasurementT> filtered; filtered.reserve(nMeasurements);
	for(const auto& current : measurements)
	{
		int i1=reverseIndexMap[get<iCamera1>(current)];
		int i2=reverseIndexMap[get<iCamera2>(current)];

		if(i1==-1 || i2==-1)
			continue;

		filtered.push_back(current);
		get<iCamera1>(*filtered.rbegin())=i1;
		get<iCamera2>(*filtered.rbegin())=i2;
	}	//for(const auto& current : measurements)

	filtered.shrink_to_fit();
	return make_tuple(filtered, indexMap);
}	//tuple<vector<MeasurementT>, IndexContainerT> RemoveUnconnected(const vector<MeasurementT>& measurements)

/**
 * @brief Estimates the reference focal length
 * @param[in] measurements Measurements
 * @param[in] mKList Initial estimates for the intrinsic calibration parameters
 * @param[in] fixedFArray Indicator array for fixed focal length values
 * @return A tuple: [Camera index; focal length]
 */
tuple<size_t, double> PfMPipelineC::EstimateReferenceFocalLength(const vector<MeasurementT>& measurements, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray)
{
	if(measurements.empty())
		return tuple<size_t, double>();

	//Get the individual focal length estimates for each camera
	size_t nCameras=fixedFArray.size();
	vector< multimap<double, double, greater<double>  > >fMeasurements(nCameras);
	vector<unsigned int> measurementCount(nCameras,0);

	for(const auto& current : measurements)
	{
		size_t i1=get<iCamera1>(current);
		size_t i2=get<iCamera2>(current);

		//If the focal length is known, ignore the measurement, and set the quality to 1
		double f1 = fixedFArray[i1] ? mKList[i1](0,0) : get<iFocalLength1>(current);
		double q1=  fixedFArray[i1] ? 1 : get<iQuality>(current);

		double f2 = fixedFArray[i2] ? mKList[i2](0,0) : get<iFocalLength2>(current);
		double q2 = fixedFArray[i2] ? 1 : get<iQuality>(current);

		fMeasurements[get<iCamera1>(current)].emplace(q1,f1);
		fMeasurements[get<iCamera2>(current)].emplace(q2,f2);

		++measurementCount[i1];
		++measurementCount[i2];
	}	//for(const auto& current : measurements)

	//Sort the cameras wrt their median error
	//Rules: 1. Any cameras with a measurement count less than half of the maximum measurement count is omitted
	//		 2. A camera with a max quality of 1 is never removed

	unsigned int maxCount=*max_element(measurementCount.begin(), measurementCount.end());

	map<pair<double, unsigned int>, pair<size_t,double>, greater<pair<double, unsigned int>> > sorted;	//[ [quality; measurement count]; [camera id; best focal length] ]
	for(size_t c=0; c<nCameras; ++c)
		{
			//Too few measurements, and focal length is not fixed
			if(measurementCount[c] < maxCount/2 && !fixedFArray[c])
				continue;

			double score=next(fMeasurements[c].begin(), floor(measurementCount[c])/2)->first;
			sorted.emplace(make_pair(score, measurementCount[c]), make_pair(c, fMeasurements[c].begin()->second) );
		}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(sorted.begin()->second.first, sorted.begin()->second.second);
}	//tuple<size_t, double> EstimateReferenceFocalLength(const vector<MeasurementT>& measurements, const vector<bool>& fixedFArray)

/**
 * @brief Estimates the relative focal lengths
 * @param[in,out] rng Random number generator
 * @param[in] measurements Measurements
 * @param[in] fixedFArray Indicator array for fixed focal length values
 * @param[in] parameters Parameters
 * @return Relative focal lengths
 */
vector<double> PfMPipelineC::EstimateRelativeFocalLengths(RNGT& rng, const vector<MeasurementT>& measurements, const vector<bool>& fixedFArray, const PfMPipelineParametersC& parameters)
{
	//Observation list
	vector<RatioRegistrationC::observation_type> observations; observations.reserve(measurements.size());
	for(const auto& current : measurements)
		observations.emplace_back(get<iCamera1>(current), get<iCamera2>(current), get<iFocalLength2>(current)/get<iFocalLength1>(current), get<iQuality>(current));

	//The internal solver should be unweighted, as the retention rates do not seem to correlate with the quality metric. Still, keep the edge weighting
	RandomSpanningTreeSamplerParametersC rstsParameters;

	RSTSRatioRegistrationProblemC problem(observations, parameters.maxFocalLengthRatioError);

	typedef RandomSpanningTreeSamplerC<RSTSRatioRegistrationProblemC> RSTSEngineT;
	RSTSEngineT::result_type robustModel;
	RandomSpanningTreeSamplerDiagnosticsC diagnostics=RSTSEngineT::Run(robustModel, problem, rng, parameters.focalLengthRegistrationPipeline);

	//If fails, minimum spanning tree
	if(!diagnostics.flagSuccess)
		diagnostics=RSTSEngineT::ComputeMSTSolution(robustModel, problem);

	//Find the inliers, and return an LS solution
	vector<RatioRegistrationC::observation_type> inliers; inliers.reserve(measurements.size());
	for(const auto& current : observations)
	{
		RSTSRatioRegistrationProblemC::edge_type edge;
		get<RSTSRatioRegistrationProblemC::iVertex1>(edge)=get<RatioRegistrationC::iIndex1>(current);
		get<RSTSRatioRegistrationProblemC::iVertex2>(edge)=get<RatioRegistrationC::iIndex2>(current);
		get<RSTSRatioRegistrationProblemC::iWeight>(edge)=get<RatioRegistrationC::iWeight>(current);

		if(get<0>(problem.EvaluateEdge(get<RSTSEngineT::iModel>(robustModel), edge)) )
			inliers.push_back(current);
	}	//for(const auto& current : observations)

	return RatioRegistrationC::Run(inliers, false);
}	//vector<double> EstimateRelativeFocalLengths(const vector<MeasurementT>& measurements, const vector<bool>& fixedFArray, const PfMPipelineParametersC& parameters)

/**
 * @brief Estimates the focal lengths
 * @param[in,out] rng Random number generator
 * @param[in] measurements Measurements
 * @param[in] mKList Initial estimates for the intrinsic calibration parameters
 * @param[in] fixedFArray Indicator array for fixed focal length values
 * @param[in] parameters Parameters
 * @return Focal lengths
 */
vector<double> PfMPipelineC::EstimateFocalLengths(RNGT& rng, const vector<MeasurementT>& measurements, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const PfMPipelineParametersC& parameters)
{
	//Relative focal lengths
	vector<double> relativeF=EstimateRelativeFocalLengths(rng, measurements, fixedFArray, parameters);

	//Reference focal length
	size_t referenceCamera;
	double referenceF;
	tie(referenceCamera, referenceF)=EstimateReferenceFocalLength(measurements, mKList, fixedFArray);

	vector<double> output(relativeF.size(), referenceF);

	//If uniform focal length, ignore the relative F calculations
	if(!parameters.flagUniformFocalLength)
		transform(relativeF, output.begin(), [&](double current){return referenceF*current/relativeF[referenceCamera]; });

	return output;
}	//vector<double> EstimateFocalLengths(const vector<MeasurementT>& measurements, const vector<bool>& fixedFArray)

/**
 * @brief Reindexes the edge list and removes the edges belonging to eliminated cameras
 * @param[in] edgeList Edge list
 * @param[in] indexMap Index map
 * @param[in] nCameras Number of cameras
 * @return Purged edge list
 */
auto PfMPipelineC::PurgeEdgeList(const EdgeContainerT& edgeList, const IndexContainerT& indexMap, size_t nCameras) -> EdgeContainerT
{
	//Reverse index map
	vector<int> reverseIndexMap(nCameras,-1);
	size_t nSurvivor=indexMap.size();
	for(size_t c=0; c<nSurvivor; ++c)
		reverseIndexMap[indexMap[c]] = c;

	//Purge
	EdgeContainerT purged;
	for(const auto& current : edgeList)
	{
		size_t i1=current.first.first;
		size_t i2=current.first.second;

		if(reverseIndexMap[i1]!=-1 && reverseIndexMap[i2]!=-1)
			purged[make_pair(i1,i2)]=current.second;
	}	//for(const auto& current : edgeList)

	return purged;
}	//EdgeContainerT PurgeEdgeList(const EdgeContainerT& edgeList, const IndexContainerT& indexMap)

/**
 * @brief Refines the rotation measurements after the estimation of focal lengths
 * @param[in] measurements Measurements
 * @param[in] edgeList Edges
 * @param[in] coordinateStack Coordinate stack
 * @param[in] mKList Estimated intrinsic calibration matrices
 * @param[in] fixedFArray Indicator array for cameras with fixed focal lengths
 * @return Measurements with updated relative rotations
 */
auto PfMPipelineC::RefineRotationMeasurements(const vector<MeasurementT>& measurements, const EdgeContainerT& edgeList, const CoordinateStackT& coordinateStack, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray) -> vector<MeasurementT>
{
	//Direction vectors in the local reference frame
	size_t nCameras=mKList.size();
	vector<vector<Coordinate3DT> > localDirectionVectors(nCameras);
	for(size_t c=0; c<nCameras; ++c)
	{
		Matrix3d normaliser=mKList[c].inverse();
		localDirectionVectors[c].resize(coordinateStack[c].size());
		transform(coordinateStack[c], localDirectionVectors[c].begin(), [&](const Coordinate2DT& current){return (normaliser * current.homogeneous()).normalized();});
	}	//for(size_t c=0; c<nCameras; ++c)

	vector<MeasurementT> refined; refined.reserve(measurements.size());
	for(const auto& current : measurements)
	{
		size_t i1=get<iCamera1>(current);
		size_t i2=get<iCamera2>(current);
		refined.push_back(current);

		//No change in focal length, keep as it is
		if(fixedFArray[i1] && fixedFArray[i2])
			continue;

		//Else, re-estimate the rotation
		CoordinateCorrespondenceList3DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList3DT >(get<VisionGraphEngineT::iCorrespondenceList>(edgeList.find(make_pair(i1,i2))->second), localDirectionVectors[i1], localDirectionVectors[i2]);

		Rotation3DSolverC solver;
		list<Rotation3DSolverC::model_type> solution=solver(coordinateCorrespondences);

		if(!solution.empty())
			get<iRelativeRotation>(*refined.rbegin())=solution.begin()->topLeftCorner(3,3);
	}	//for(const auto& current : measurements)

	return refined;
}	//vector<MeasurementT> RefineRotationMeasurements(const vector<MeasurementT>& measurements, const CoordinateStackT& coordinateStack, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray)

/**
 * @brief Estimates the calibration parameters for the cameras
 * @param[in,out] rng Random number generator
 * @param[in] edgeList Edge list
 * @param[in] mKList Initial estimates for the intrinsic calibration parameters
 * @param[in] fixedFArray Indicator array for known focal lengths
 * @param[in] coordinateStack Coordinate stack
 * @param[in] parameters Parameters
 * @return  A tuple: [Calibration parameters; index map for the original indices of the surviving cameras; surviving edges; survivor coordinates]
 */
auto PfMPipelineC::CalibrateCameras(RNGT& rng, const EdgeContainerT& edgeList, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const CoordinateStackT& coordinateStack, const PfMPipelineParametersC& parameters) -> tuple<vector<CameraMatrixT>, IndexContainerT, EdgeContainerT, CoordinateStackT>
{
	tuple<vector<CameraMatrixT>, IndexContainerT, EdgeContainerT, CoordinateStackT> output;

	//Measurement decomposition
	vector<MeasurementT> measurements=ExtractMeasurements(edgeList, mKList, fixedFArray, coordinateStack, parameters);

	if(measurements.empty())
		return output;

	//Eliminate the unconnected cameras
	size_t nCameras=mKList.size();
	IndexContainerT indexMap;
	vector<MeasurementT> filtered;
	std::tie(filtered, indexMap)=RemoveUnconnected(measurements, nCameras);	//This cannot be empty

	//Purge

	size_t nSurvivors=indexMap.size();

	vector<IntrinsicCalibrationMatrixT> survivormKList; survivormKList.reserve(nSurvivors);
	for_each(indexMap, [&](size_t c){survivormKList.push_back(mKList[c]);} );

	CoordinateStackT survivorCoordinates; survivorCoordinates.reserve(nSurvivors);
	for_each(indexMap, [&](size_t c){survivorCoordinates.push_back(coordinateStack[c]);} );

	vector<bool> survivorFixedFArray; survivorFixedFArray.reserve(nSurvivors);
	for_each(indexMap, [&](size_t c){survivorFixedFArray.push_back(fixedFArray[c]);} );

	EdgeContainerT survivorEdges=PurgeEdgeList(edgeList, indexMap, nCameras);

	//Focal length estimation
	vector<double> focalLengths=EstimateFocalLengths(rng, filtered, survivormKList, survivorFixedFArray, parameters);

	//Intrinsic calibration matrices
	vector<IntrinsicCalibrationMatrixT> mKEstimated(survivormKList);
	for(size_t c=0; c<nSurvivors; ++c)
	{
		mKEstimated[c](0,0)=focalLengths[c];
		mKEstimated[c](1,1)=focalLengths[c] * survivormKList[c](1,1)/survivormKList[c](0,0);
	}	//for(size_t c=0; c<nSurvivors; ++c)

	//Refine the relative orientation estimates
	vector<MeasurementT> refined=RefineRotationMeasurements(filtered, survivorEdges, survivorCoordinates, mKEstimated, survivorFixedFArray);

	//Rotation estimation
	vector<RotationObservationT> orientationMeasurements; orientationMeasurements.reserve(refined.size());
	for(const auto& current : refined)
		orientationMeasurements.emplace_back(get<iCamera1>(current), get<iCamera2>(current), get<iRelativeRotation>(current), get<iQuality>(current));

	RotationRegistrationPipelineC::result_type orientationEstimates;
	RotationRegistrationPipelineC::index_container_type inlierMeasurements;
	RotationRegistrationPipelineDiagnosticsC rotationRegistrationDiagnostics=RotationRegistrationPipelineC::Run(orientationEstimates, inlierMeasurements, rng, orientationMeasurements, parameters.rotationRegistrationParameters );

	if(!rotationRegistrationDiagnostics.flagSuccess)
		return output;

	//Inlier edges
	EdgeContainerT inlierEdges;
	for(size_t c: inlierMeasurements)
	{
		auto index=make_pair(get<RelativeRotationRegistrationC::iIndex1>(orientationMeasurements[c]), get<RelativeRotationRegistrationC::iIndex2>(orientationMeasurements[c]) );
		inlierEdges[index]=survivorEdges[ make_pair(indexMap[index.first], indexMap[index.second]) ];
	}	//for(size_t c: inlierMeasurements)

	//Compose the camera matrices
	vector<CameraMatrixT> cameraList(nSurvivors);
	for(size_t c=0; c<nSurvivors; ++c)
		cameraList[c]=ComposeCameraMatrix(mKEstimated[c], Coordinate3DT(0,0,0), orientationEstimates[c]);

	//Output
	return make_tuple(cameraList, indexMap, inlierEdges, survivorCoordinates);
}	//tuple<vector<CameraMatrixT>, IndexContainerT> CalibrateCameras(const EdgeContainerT& edgeList, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const CoordinateStackT& coordinateStack, const PfMPipelineParametersC& parameters)

/**
 * @brief Estimates the noise parameter for the panorama builder
 * @param[in] cameraList Camera list
 * @param[in] correspondences Correspondence stack
 * @param[in] coordinates Image coordinate stack
 * @param[in] pReject Probability of rejecting an inlier
 * @return Estimated noise variance
 */
double PfMPipelineC::EstimateNoise(const vector<CameraMatrixT>& cameraList, const CorrespondenceStackT& correspondences, const CoordinateStackT& coordinates, double pReject)
{
	//Compute the threshold for a noise variance of 1
	chi_squared_distribution<double> referenceDist(4);
	double referenceThreshold=quantile(referenceDist, 1-pReject);

	set<double> noiseList;
	for(const auto& current : correspondences)
	{
		size_t i1;
		size_t i2;
		tie(i1,i2)=current.first;

		//Compute the homography
		Homography2DT mH= cameraList[i2].block(0,0,3,3) * cameraList[i1].block(0,0,3,3).inverse();

		//Compute the (1-pReject)th percentile point in the error distribution

		SymmetricTransferErrorH2DT errorMetric(mH);

		size_t nCorrespondences=current.second.size();
		vector<double> errorList; errorList.reserve(nCorrespondences);

		const auto& pCoordinates1=coordinates[i1];
		const auto& pCoordinates2=coordinates[i2];

		for(const auto& currentCorr : current.second)
			errorList.push_back(errorMetric(pCoordinates1[currentCorr.left], pCoordinates2[currentCorr.right]));

		sort(errorList.begin(), errorList.end());

		double threshold=pow<2>(errorList[floor(nCorrespondences*(1-pReject))]);

		//Find the corresponding noise variance
		noiseList.insert(threshold/referenceThreshold);	//Reference is for the sigma=1 cases
	}	//for(const auto& current : correspondences)

	return *next(noiseList.begin(), floor(noiseList.size()/2)  );
}	//double EstimateNoise(const vector<CameraMatrixT>& cameraList, const CorrespondenceStackT& correspondences, const CoordinateStackT& coordinates, double pReject)

/**
 * @brief Prepares the 3D-2D correspondence lists for each camera
 * @param[in] tracks Tracks
 * @param[in] nCameras Number of cameras
 * @return Correspondence lists
 */
vector<CorrespondenceListT> PfMPipelineC::TrackTo3D2DCorrespondence(const TrackListT& tracks, size_t nCameras)
{
	vector<CorrespondenceListT> output(nCameras);

	size_t index=0;
	for(const auto& currentTrack : tracks)
	{
		for(const auto& current : currentTrack)
			output[get<CorrespondenceFusionC::iSourceId>(current)].push_back(CorrespondenceListT::value_type(index, get<CorrespondenceFusionC::iElementId>(current), MatchStrengthC(1,0)));

		++index;
	}	//for(const auto& currentTrack : tracks)

	return output;
}	//vector<CorrespondenceListT> TrackTo3D2DCorrespondence(const TrackListT& tracks, size_t nCameras)

/**
 * @brief Refines the estimates via the robust geometry estimation pipeline
 * @param[in,out] rng Random number generator
 * @param[in] estimated Estimated cameras
 * @param[in] tracks 2D point tracks
 * @param[in] coordinates 2D coordinate stack
 * @param[in] panorama 3D panorama
 * @param[in] flagFixedFArray Flags indicating whether the focal length is known for a camera
 * @param[in] parameters Parameters
 * @return A tuple: A vector of refined camera matrices; diagnostics
 */
tuple<vector<CameraMatrixT>, vector<GeometryEstimatorDiagnosticsC> >  PfMPipelineC::RefineEstimates(RNGT& rng, const vector<CameraMatrixT>& estimated, const TrackListT& tracks, const CoordinateStackT& coordinates, const vector<Coordinate3DT>& panorama, const vector<bool>& flagFixedFArray, const PfMPipelineParametersC& parameters)
{
	size_t nCameras=estimated.size();
	vector<CameraMatrixT> refinedCameras(nCameras);
	vector<GeometryEstimatorDiagnosticsC> diagnostics(nCameras);

	vector<CorrespondenceListT> sceneImageCorrespondences=TrackTo3D2DCorrespondence(tracks, nCameras);	//3D-2D correspondences

	RANSACP2PfProblemT::dimension_type1 binSize1=*ComputeBinSize<Coordinate3DT>(panorama, 4);	//Bin sizes for the 3D points

	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(dynamic) private(c)  num_threads(parameters.nThreads)
	for(c=0; c<nCameras; ++c)
	{
		//Normalisation
		//The camera centre is already at the origin
		IntrinsicCalibrationMatrixT mK;
		tie(mK, ignore, ignore)=DecomposeCamera(estimated[c]);

		IntrinsicCalibrationMatrixT mKi=mK.inverse();
		CameraMatrixT mPn=mKi*estimated[c];
		vector<Coordinate2DT> normalisedCoordinates=CoordinateTransformations2DT::ApplyAffineTransformation(coordinates[c], CoordinateTransformations2DT::affine_transform_type(mKi) );

		CoordinateCorrespondenceList32DT constraints3D2D=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList32DT>(sceneImageCorrespondences[c], panorama, normalisedCoordinates);

		//Geometry estimator

		RANSACP2PfProblemT::dimension_type2 binSize2=*ComputeBinSize<Coordinate2DT>(normalisedCoordinates, 4);
		vector<size_t> inliers;
		vector<size_t> generator;
		CameraMatrixT refined;

		if(!flagFixedFArray[c] && !parameters.flagUniformFocalLength )
		{
			PDLP2PfProblemT pdlProblem=MakePDLP2PfProblem(mPn, constraints3D2D, optional<MatrixXd>());
			RANSACP2PfProblemT ransacProblem=MakeRANSACP2PfProblem(constraints3D2D, constraints3D2D, parameters.noiseVariance*pow<2>(mKi(0,0)), parameters.inlierRejectionProbability, 1, parameters.geLOGeneratorRatio1, binSize1, binSize2);
			P2PfEstimatorProblemT estimatorProblem(ransacProblem, ransacProblem, pdlProblem, optional<CameraMatrixT>());

			diagnostics[c]=P2PfEstimatorT::Run(refined, inliers, generator, estimatorProblem, rng, parameters.geometryEstimatorParameters);
		}	//if(flagFixedFArray[c])
		else
		{
			PDLP2PProblemT pdlProblem=MakePDLP2PProblem(mPn, constraints3D2D, optional<MatrixXd>());
			RANSACP2PProblemT ransacProblem=MakeRANSACP2PProblem(constraints3D2D, constraints3D2D, parameters.noiseVariance*pow<2>(mKi(0,0)), parameters.inlierRejectionProbability, 1, parameters.geLOGeneratorRatio2, binSize1, binSize2);
			P2PEstimatorProblemT estimatorProblem(ransacProblem, ransacProblem, pdlProblem, optional<CameraMatrixT>());

			diagnostics[c]=P2PEstimatorT::Run(refined, inliers, generator, estimatorProblem, rng, parameters.geometryEstimatorParameters);
		}	//if(flagFixedFArray[c])

	#pragma omp critical(PfM_RE1)
		refinedCameras[c] = diagnostics[c].flagSuccess ? mK * refined : estimated[c];
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(refinedCameras, diagnostics);
}	//vector<CameraMatrixT> RefineEstimates(RNGT& rng, const vector<CameraMatrixT>& estimated, const TrackListT& tracks, const CoordinateStackT& coordinates, const vector<Coordinate3DT>& panorama, const vector<bool>& flagFixedFArray, const PfMPipelineParametersC& parameters)

/**
 * @brief Runs the algorithm
 * @param[out] result Output. [Camera Id, Camera matrix]
 * @param[out] panorama Estimated point cloud on the unit sphere
 * @param[in, out] rng Random number generator
 * @param[in] featureStack Feature stack. Unnormalised features
 * @param[in] cameraList Cameras. Each element should have at least the frame size defined. Any other information is used for deriving initial estimates
 * @param[in] parameters Parameters
 * @return Diagnostics object
 */
PfMPipelineDiagnosticsC PfMPipelineC::Run( map<size_t, CameraMatrixT>& result, vector<Coordinate3DT>& panorama, RNGT& rng, const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList, PfMPipelineParametersC parameters)
{
	//TODO: Pairwise homography decomposition is not robust. When there is a scale change, the high decompostion error
	// Solution: 1. Nonlinear minimisation after each homogprahy, enforcing a KRK transformation (DONE, changes only the rotation)
	// Solution: 2. Minimal solvers that directly estimate a KRK model (or a rotation, when K is known- this would be the Rotation3D pipeline, with homogeneous coordinates)

	PfMPipelineDiagnosticsC diagnostics;
	result.clear();

	//Return if no features
	if(featureStack.empty())
	{
		diagnostics.flagSuccess=true;
		return diagnostics;
	}//	if(featureStack.empty())

	//Preconditions
	ValidateInput(featureStack, cameraList, parameters);

	//Process the cameras
	vector<IntrinsicCalibrationMatrixT> mKListInitial;
	map<size_t, CameraMatrixT> mPList;
	vector<bool> fixedFArray;
	tie(mPList, mKListInitial, fixedFArray)=ProcessCameras(cameraList, parameters);

	//Vision graph
	VisionGraphEngineT::vision_graph_type visionGraph(0);
	EdgeContainerT graphEdgeList;
	diagnostics.visionGraphDiagnostics=VisionGraphEngineT::Run(visionGraph, graphEdgeList, rng, featureStack, mPList, vector<double>(1,parameters.noiseVariance), parameters.visionGraphParameters);

	if(!diagnostics.visionGraphDiagnostics.flagSuccess)
		return diagnostics;

	//Coordinate stack
	CoordinateStackT coordinateStack; coordinateStack.reserve(featureStack.size());
	for_each(featureStack, [&](const FeatureListT& current){coordinateStack.push_back(MakeCoordinateVector(current));});

	//Camera calibration
	vector<CameraMatrixT> estimatedCameras;
	IndexContainerT indexMap;
	EdgeContainerT inlierEdgeList;
	CoordinateStackT survivorCoordinateStack;
	std::tie(estimatedCameras, indexMap, inlierEdgeList, survivorCoordinateStack)=CalibrateCameras(rng, graphEdgeList, mKListInitial, fixedFArray, coordinateStack, parameters);

	if(estimatedCameras.empty())
		return diagnostics;

	//Panorama

	CorrespondenceStackT imageCorrespondences;
	for(const auto& current : inlierEdgeList)
		imageCorrespondences.emplace( IndexTupleT(current.first.first, current.first.second), get<VisionGraphEngineT::iCorrespondenceList>(current.second));

	parameters.panoramaBuilderParameters.noiseVariance=EstimateNoise(estimatedCameras, imageCorrespondences, survivorCoordinateStack, parameters.panoramaBuilderParameters.pReject);	//Error both from the image noise and projection of the homographies onto orientation and intrinsic calibration parameters

	TrackListT inlierImageTracks;
	diagnostics.panoramaDiagnostics=MultiviewPanoramaBuilderC::Run(panorama, inlierImageTracks, imageCorrespondences, survivorCoordinateStack, estimatedCameras, parameters.panoramaBuilderParameters);

	if(!diagnostics.panoramaDiagnostics.flagSuccess)
		return diagnostics;

	//Refinement
	vector<CameraMatrixT> refinedCameras;
	tie(refinedCameras, diagnostics.geometryEstimatorDiagnostics)=RefineEstimates(rng, estimatedCameras, inlierImageTracks, survivorCoordinateStack, panorama, fixedFArray, parameters);

	size_t nSurvivors=indexMap.size();
	for(size_t c=0; c<nSurvivors; ++c)
		result.emplace(indexMap[c], refinedCameras[c]);

	diagnostics.flagSuccess=true;
	diagnostics.nRegistered=nSurvivors;
	diagnostics.sPointCloud=panorama.size();
	diagnostics.nInlierEdge=inlierEdgeList.size();
	diagnostics.estimatedNoiseVariance=parameters.panoramaBuilderParameters.noiseVariance;

	return diagnostics;
}	//PfMPipelineDiagnosticsC Run( map<size_t, CameraMatrixT>& result, RNGT& rng, const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList, const vector<double>& noiseVariance, const PfMPipelineParametersC& parameters)

}	//GeometryN
}	//SeeSawN
