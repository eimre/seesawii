/**
 * @file LinearAutocalibrationPipeline.ipp Implementation of \c LinearAutocalibrationPipelineC
 * @author Evren Imre
 * @date 7 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LINEARAUTOCALIBRATIONPIPELINE_IPP_3793012
#define LINEARAUTOCALIBRATIONPIPELINE_IPP_3793012

#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <cstddef>
#include <tuple>
#include <type_traits>
#include <cmath>
#include <limits>
#include <list>
#include <map>
#include <stdexcept>
#include <string>
#include <tuple>
#include <iterator>
#include "../RANSAC/RANSAC.h"
#include "../RANSAC/RANSACDualAbsoluteQuadricProblem.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Geometry/DualAbsoluteQuadricSolver.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::begin;
using boost::copy;
using boost::transform;
using boost::make_zip_iterator;
using boost::make_iterator_range;
using boost::lexical_cast;
using boost::optional;
using std::vector;
using std::get;
using std::is_same;
using std::min;
using std::list;
using std::map;
using std::string;
using std::invalid_argument;
using std::numeric_limits;
using std::tuple;
using std::get;
using std::tie;
using std::advance;
using std::back_inserter;
using SeeSawN::RANSACN::RANSACC;
using SeeSawN::RANSACN::RANSACParametersC;
using SeeSawN::RANSACN::RANSACDiagnosticsC;
using SeeSawN::RANSACN::RANSACDualAbsoluteQuadricProblemC;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::Quadric3DT;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::ExtractRectifyingHomography;
using SeeSawN::ElementsN::IntrinsicsFromDAQ;
using SeeSawN::GeometryN::DualAbsoluteQuadricSolverC;

/**
 * @brief Parameters for \c LinearAutocalibrationPipelineC
 * @ingroup Parameters
 */
struct LinearAutocalibrationPipelineParametersC
{
	unsigned int nLACIteration; ///< Number of focal length weight iterations in the linear autocalibration solver. >0, ~50 is recommended

	double inlierTh;	///< Inlier threshold. >0
	unsigned int sGenerator;	///< Generator size for the minimal solver.
	unsigned int sLOGenerator;	///< Generator size for the LO solver.

	RANSACParametersC ransacParameters;	///< RANSAC parameters

	LinearAutocalibrationPipelineParametersC() : nLACIteration(50), inlierTh(1e-3), sGenerator( DualAbsoluteQuadricSolverC::GeneratorSize() ), sLOGenerator(DualAbsoluteQuadricSolverC::GeneratorSize())
	{}
};	//struct LinearAutocalibrationPipelineParametersC

/**
 * @brief Diagnostics for \c LinearAutocalibrationPipelineC
 * @ingroup Diagnostics
 */
struct LinearAutocalibrationPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully
	double error;	///< Error indicating the quality of the estimates
	RANSACDiagnosticsC ransacDiagnostics;	///< RANSAC diagnostics

	LinearAutocalibrationPipelineDiagnosticsC() : flagSuccess(false), error(0)
	{}

};	//struct LinearAutocalibrationPipelineDiagnosticsC

//TESTME Tests pending projective cameras
/**
 * @brief Linear autocalibration pipeline
 * @remarks Algorithm
 * 	- Normalise the cameras
 * 	- Estimate the dual absolute quadric via RANSAC
 * 	- Estimate the rectifying homography via RANSAC
 * 	- Use the inliers to the RANSAC, to re-estimate the DAQ with a non-minimal set
 * 	- DAQ can be decomposed to yield the rectifying homography, and also be used to estimate the intrinsics per camera, via DIAQ
 * @remarks Usage notes:
 * 	- The algorithm uses the DIAQ for the equation system and evluation. Therefore, of the two output types
 * 		- where possible, it is best to use the intrinsic calibration matrices
 * 		- if this is not possible,  one has to use the rectifying homography. However, the intrinsic calibration matrices extracted from the rectified cameras are not the same as the estimates from DIAQ
 * 	- In the absence of an initial estimate for the focal length or a principal point, the algorithm computes one by decomposing the camera matrix. But not the recommended practice.
 * 	- Even when the input is a set of metric cameras, the rectifying homography may involve a rotation and scale
 * @ingroup Algorithm
 * @nosubgrouping
 */
class LinearAutocalibrationPipelineC
{
	private:

		/** @name Implementation details */ ///@{
		static void ValidateParameters(const vector<IntrinsicC>& initialEstimateList, LinearAutocalibrationPipelineParametersC& parameters, size_t nCameras);	///< Validates the parameters

		static IntrinsicCalibrationMatrixT ComputeNormaliser(const IntrinsicC& initialEstimate, const CameraMatrixT& mP);	///< Computes the normaliser corresponding to the initial estimate
		static tuple<vector<CameraMatrixT>, vector<IntrinsicCalibrationMatrixT> > NormaliseCameras(const vector<CameraMatrixT>& cameraList, const vector<IntrinsicC>& initialEstimateList);	///< Normalises the camera matrices

		static double Evaluate(const Quadric3DT& mQ, const vector<CameraMatrixT>& cameraList, double inlierTh);	///< Evaluates a solution
		template<class RNGT> static tuple<Homography3DT, Quadric3DT, vector<size_t>, LinearAutocalibrationPipelineDiagnosticsC> EstimateRectifyingHomography(RNGT& rng, const vector<CameraMatrixT>& mPList, const LinearAutocalibrationPipelineParametersC& parameters);	///< Estimates the rectifying homography
		///@}
	public:

		/** @name Operations*/ //@{
		template<class RNGT, class CameraMatrixRangeT> static LinearAutocalibrationPipelineDiagnosticsC Run(Homography3DT& mHr, map<size_t, IntrinsicCalibrationMatrixT>& inliers, RNGT& rng, const CameraMatrixRangeT& cameraList, const vector<IntrinsicC>& initialEstimateList, LinearAutocalibrationPipelineParametersC parameters);	///< Runs the algorithm

		static CameraMatrixT ImposeConstraints(const CameraMatrixT& mP, const IntrinsicC& constraints);	///< Imposes hard calibration constraints
		static IntrinsicCalibrationMatrixT ImposeConstraints(const IntrinsicCalibrationMatrixT& mK, const IntrinsicC& constraints);	///< Imposes hard calibration constraints

		//@}
};	//class LinearAutocalibrationPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Estimates the rectifying homography
 * @tparam RNGT A random number generator
 * @param[in,out] rng Random number generator
 * @param[in] mPList Normalised camera matrices
 * @param[in] parameters Parameters
 * @return A tuple: [Rectifying homography; DAQ; inliers; diagnostics]
 */
template<class RNGT>
tuple<Homography3DT, Quadric3DT, vector<size_t>, LinearAutocalibrationPipelineDiagnosticsC> LinearAutocalibrationPipelineC::EstimateRectifyingHomography(RNGT& rng, const vector<CameraMatrixT>& mPList, const LinearAutocalibrationPipelineParametersC& parameters)
{
	tuple<Homography3DT, Quadric3DT, vector<size_t>, LinearAutocalibrationPipelineDiagnosticsC> output;

	//Estimate DAQ via RANSAC
	DualAbsoluteQuadricSolverC solver(parameters.nLACIteration);
	RANSACDualAbsoluteQuadricProblemC problem(mPList, mPList, solver, solver, parameters.sGenerator, parameters.sLOGenerator, parameters.inlierTh, 0);
	typedef RANSACC<RANSACDualAbsoluteQuadricProblemC> RANSACT;
	RANSACT::result_type ransacOutput;
	LinearAutocalibrationPipelineDiagnosticsC diagnostics;
	diagnostics.ransacDiagnostics=RANSACT::Run(ransacOutput, problem, rng, parameters.ransacParameters);

	diagnostics.flagSuccess=diagnostics.ransacDiagnostics.flagSuccess;

	if(!diagnostics.flagSuccess)
		return output;

	vector<size_t> inliers=get<RANSACT::iInlierList>(ransacOutput.begin()->second);
	Quadric3DT mQ1=get<RANSACT::iModel>(ransacOutput.begin()->second);

	//Evaluate the output of RANSAC on the inliers set
	double error1=Evaluate(mQ1, mPList, parameters.inlierTh);

	//DAQ via non-minimal solver
	vector<CameraMatrixT> inlierCameras=problem.GetInlierObservations(mQ1);
	list<Quadric3DT> mQ2List=problem.GetLOSolver()(inlierCameras);
	double error2=numeric_limits<double>::infinity();
	Quadric3DT mQ2=Quadric3DT::Zero();

	if(!mQ2List.empty())
	{
		mQ2=*mQ2List.begin();
		error2=Evaluate(mQ2, mPList, parameters.inlierTh);
	}	//if(!mH2List.empty())

	diagnostics.error = min(error1, error2);

	//Rectifying homography
	Quadric3DT mQ= (error1<=error2) ? mQ1 : mQ2;
	optional<Homography3DT> mHr=ExtractRectifyingHomography(mQ);

	if(!mHr)
		return output;

	return make_tuple(*mHr, mQ, inliers, diagnostics);
}	//tuple<optional<Homography3DT>, vector<size_t>, LinearAutocalibrationPipelineDiagnosticsC> EstimateRectifyingHomography(const vector<CameraMatrixT>& mPList, const LinearAutocalibrationPipelineParametersC& parameters)

/**
 * @brief Runs the algorithm
 * @tparam RNGT A random number generator
 * @tparam CameraMatrixRangeT A range of camera matrices
 * @param[out] mHr Rectifying homography
 * @param[out] inliers Inliers. [camera index; intrinsic calibration matrix]
 * @param[in,out] rng Random number generator
 * @param[in] cameraList Cameras to be upgraded
 * @param[in] initialEstimateList Initial estimate for the calibration parameters. If it has 1 element, the same normaliser is applied to all cameras
 * @param[in] parameters Parameters
 * @pre \c CameraMatrixRangeT is a forward range of \c CameraMatrixT
 * @pre \c initalEstimateList has either 1 element, or as many as \c cameraList
 * @remarks If an initial estimate does not have a valid focal length, it is estimated by decomposing the corresponding camera matrix. Not recommended, though
 * @return Diagnostics object
 */
template<class RNGT, class CameraMatrixRangeT>
LinearAutocalibrationPipelineDiagnosticsC LinearAutocalibrationPipelineC::Run(Homography3DT& mHr, map<size_t, IntrinsicCalibrationMatrixT>& inliers, RNGT& rng, const CameraMatrixRangeT& cameraList, const vector<IntrinsicC>& initialEstimateList, LinearAutocalibrationPipelineParametersC parameters)
{
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CameraMatrixRangeT>));
	static_assert(is_same<CameraMatrixT, typename range_value<CameraMatrixRangeT>::type >::value, "LinearAutocalibrationPipelineC::Run : CameraMatrixRangeT must have elements of type CameraMatrixT");

	LinearAutocalibrationPipelineDiagnosticsC diagnostics;
	size_t nCameras=boost::distance(cameraList);
	vector<CameraMatrixT> cameraContainer(nCameras);
	copy(cameraList, cameraContainer.begin());

	//Too few cameras
	if(nCameras<parameters.sGenerator)
		return diagnostics;

	//Validate the parameters

	ValidateParameters(initialEstimateList, parameters, nCameras);

	//Normalise the cameras
	vector<CameraMatrixT> normalised;
	vector<IntrinsicCalibrationMatrixT> normaliser;
	std::tie(normalised, normaliser)=NormaliseCameras(cameraContainer, initialEstimateList);

	//Run the estimator
	Quadric3DT mQ;
	vector<size_t> inlierIndices;
	std::tie(mHr, mQ, inlierIndices, diagnostics)=EstimateRectifyingHomography(rng, normalised, parameters);

	if(!diagnostics.flagSuccess)
		return diagnostics;

	//Extract the intrinsic calibration parameters
	for(auto current : inlierIndices)
	{
		optional<IntrinsicCalibrationMatrixT> mK=IntrinsicsFromDAQ(mQ, normalised[current]);

		if(mK)
			inliers.emplace(current, normaliser[current].inverse() * (*mK));

	}	//for(auto current : inlierIndices)

	return diagnostics;
}	//LinearAutocalibrationPipelineDiagnosticsC Run(Homography3DT& mHr, map<size_t, CameraMatrixT>& inliers, RNGT& rng, const CameraMatrixRangeT& cameraList, const vector<IntrinsicC>& initalEstimate)

}	//GeometryN
}	//SeeSawN

#endif /* LINEARAUTOCALIBRATIONPIPELINE_IPP_3793012 */
