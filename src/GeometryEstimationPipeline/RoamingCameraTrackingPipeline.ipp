/**
 * @file RoamingCameraTrackingPipeline.ipp Implementation details for the roaming camera tracker
 * @author Evren Imre
 * @date 27 Jan 2017
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef ROAMING_CAMERA_TRACKING_PIPELINE_IPP_9901290
#define ROAMING_CAMERA_TRACKING_PIPELINE_IPP_9901290

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <map>
#include <cmath>
#include <functional>
#include <tuple>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Camera.h"
#include "../Geometry/Rotation.h"
#include "../GeometryEstimationComponents/GenericPnPComponents.h"
#include "../GeometryEstimationPipeline/GeometryEstimatorProblemConcept.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Wrappers/BoostRange.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::Vector3d;
using std::vector;
using std::map;
using std::fabs;
using std::cos;
using std::greater;
using std::ignore;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ComputeFoV;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::GeometryEstimationPipelineProblemConceptC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::WrappersN::MakePermutationRange;

/**
 * @brief Parameters for the roaming camera tracking pipeline
 * @ingroup Parameters
 * @nosubgrouping
 */
struct RoamingCameraTrackingPipelineParametersC
{
	unsigned int nThreads=1;	///< Number of threads. >0

	//Tracker parameters
	bool flagFixedFocalLength=true;	///< \c true if the focal length is fixed (but not necessarily known)
	optional<double> focalLength;	///< Focal length for the first frame. Invalid if not known. >0

	double supportLossTrackingFailure=0.3;	///< Minimum support loss to trigger a tracker failure. [0,1]

	unsigned int minSupport=40;	///< Minimum support for a valid registration

	double minFocalLength=1;	///< Minimum value of the focal length, >0.
	double maxFocalLength=1e6;	///< Maximum value of the focal length, <0 & <=minFocalLength

	//Mode change triggers
	double supportLossModeSwitch=0.1;	///< Minimum support loss to trigger a mode switch to NODAL_ZOOM [0,1]
	double focalLengthSensitivity=5e-3;	///< If the relative change in focal length is below this value, mode switch to NODAL. >0

	double zoomMeasurementWeight=0.25;	///< Measurement weight for the zoom rate parameter in state update. [0,1]
	double rotationMeasurementWeight=0.25;	///< Measurement weight for the rotation parameter in state update. [0,1]
	double displacementMeasurementWeight=0.25;	///< Measurement weight for the displacement parameter in state update. [0,1]

	bool flagApproximateNeighbourhoods=true;	///< If \c true guided matching uses approximate neighbourhoods when verifying the constraint. Can skip some viable match candidates

	double imageNoiseVariance=4;	///< Image noise variance. >0
	double predictionNoise=256;	///< Variance of the additive image noise due to the prediction error, in terms of the noise variance. >=0
	double worldNoiseVariance=1e-4;	///< Variance of the coordinate noise on the scene points. >0
	double inlierRejectionProbability=0.05;	///< Inlier rejection probability. (0,1)

	unsigned int loGeneratorRatio1=3;	///< Generator size for LO-RANSAC, as a multiple of the minimal generator set. First stage. >=1
	unsigned int loGeneratorRatio2=3;	///< Generator size for LO-RANSAC, as a multiple of the minimal generator set. Second stage. >=1

	double imageDiagonal=2202;	///< Size of the image diagonal, in pixels. For P3P, should be normalised with the focal length. >0

	GeometryEstimationPipelineParametersC pipelineParameters;	///< Geometry estimation pipeline parameters
};	//struct RoamingCameraTrackingPipelineParametersC

/**
 * @brief Diagnostics for the roaming camera tracking pipeline
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct RoamingCameraTrackingPipelineDiagnosticsC
{
	bool flagSuccess=false;	///< \c true if the pipeline terminates successfully
	size_t nInliers=0;	///< Number of inliers
	bool flagRelocalise=false;	///< \c true if relocalisation triggered
	bool flagPose=false;	///< \c true if the registration for the current frame is via the localisation solver

	unsigned int nRegistration=0;	///< Number of calls to the registration module
	CorrespondenceListT supportSet;    ///< 3D-2D correspondences supporting the last registration

	GeometryEstimationPipelineDiagnosticsC geDiagnostics;	///< Diagnostics for the most recent call to the geometry estimation pipeline
};	//struct RoamingCameraTrackingPipelineDiagnosticsC

/**
 * @brief Roaming camera tracking pipeline
 * @tparam PoseEstimationProblemT Pose estimation problem
 * @tparam CalibrationEstimationProblemT Pose and focal length estimation problem
 * @pre \c CalibrationEstimationProblemT is a model of \c GeometryEstimatorProblemConceptC
 * @pre \c PoseEstimationProblemT is a model of \c GeometryEstimatorProblemConceptC
 * @pre \c MatchingProblemT is a model of \c FeatureMatcherProblemConceptC
 * @remarks Usage notes
 * - The image features are normalised with the known intrinsics (all except for the focal length)
 * @ingroup Algorithm
 * @nosubgrouping
 */
//TESTME
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
class RoamingCameraTrackingPipelineC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT( (GeometryEstimatorProblemConceptC<PoseEstimationProblemT>) );
	BOOST_CONCEPT_ASSERT( (GeometryEstimatorProblemConceptC<CalibrationEstimationProblemT>) );
	BOOST_CONCEPT_ASSERT( (FeatureMatcherProblemConceptC<MatchingProblemT>) );
	//@endcond
	public:

		enum class TrackerModeT{POSE, POSE_ZOOM};	///< Tracker mode indicator

		struct TrackerStateC
		{
			QuaternionT orientation=QuaternionT(1,0,0,0);	///< Orientation of the camera
			Coordinate3DT position=Coordinate3DT(0,0,0);	///< Position of the camera
			double focalLength=0;	///< Focal length of the camera

			Vector3d rotation=Vector3d::Zero();	///< Rotation rate
			Vector3d displacement=Vector3d::Zero();	///< Displacement rate
			double zoomRate=0;	///< Focal length change rate

			TrackerModeT trackingMode=TrackerModeT::POSE_ZOOM;	///< Current tracking mode
		};	//struct TrackerStateC


	private:

		typedef typename GeometryEstimatorC<CalibrationEstimationProblemT>::rng_type RNGT;
		typedef typename PoseEstimationProblemT::ransac_problem_type2::minimal_solver_type MainPoseSolverT;
		typedef typename CalibrationEstimationProblemT::ransac_problem_type2::minimal_solver_type MainCalibrationSolverT;

		typedef typename MatchingProblemT::feature_type1 SceneFeatureT;	///< Scene feature type
		typedef typename MatchingProblemT::feature_type2 ImageFeatureT;	///< Image feature type

		/**@name Configuration */ //@{
		vector<SceneFeatureT> world;	///< Scene model
		RoamingCameraTrackingPipelineParametersC parameters;	///< Parameters

		typename CalibrationEstimationProblemT::ransac_problem_type2::dimension_type1 binSize1;	///< RANSAC bin size for the world
		//@}

		struct RecordC
		{
			size_t sSupport=0;
			unsigned int nRegistration=0;
		} record;	//< Auxiliary variables

		/**@name State */ //@{
		bool flagValid;	///< \c true if the object is initialised
		bool flagInitialised;	///< \c true if the tracker is initialised, i.e. has a valid state

		TrackerStateC trackerState;	///< State of the tracker
		//@}

		/**@name Implementation details */ //@{
		void Validate();	///< Validates the parameters

		vector<size_t> ApplyVisibilityFilter(const TrackerStateC& prediction) const;	///< Filters out the world points that are unlikely to be visible in the current frame

		bool DetectTrackerFailure(const CameraMatrixT& estimate, const CorrespondenceListT& correspondences) const;	///< Verifies the consistency of the current estimate with the state

		template<class GeometryEstimationProblemT> tuple<GeometryEstimationPipelineDiagnosticsC, CameraMatrixT, CorrespondenceListT> RegisterCamera(RNGT& rng, optional<MatchingProblemT>& matchingProblem, const vector<ImageFeatureT>& observations, const optional<CameraMatrixT>& initialEstimate, const optional<vector<size_t> >& visibleLandmarks);	///< Registers the current image to the world model

		TrackerModeT SetTrackerMode(double focalLengthEstimate, const CorrespondenceListT& correspondences, TrackerModeT currentMode, bool flagRelocalisation) const;	///< Sets a new tracker mode if necessary

		void UpdateState(const CameraMatrixT& measurement);		///< Updates the state
		TrackerStateC Predict() const; ///< Predicts the next measurement
		//@}


	public:

		typedef RNGT rng_type;	///< Random number generator type

		RoamingCameraTrackingPipelineC();	///< Default constructor
		template<class SceneFeatureRangeT> RoamingCameraTrackingPipelineC(const SceneFeatureRangeT& wworld, const RoamingCameraTrackingPipelineParametersC& pparameters);	///< Constructor

		RoamingCameraTrackingPipelineDiagnosticsC Update(RNGT& rng, vector<ImageFeatureT>& observations);	///<Updates the tracker state with the observation

		const TrackerStateC& GetState() const;	///< Returns the state
		TrackerStateC PropagateState(const TrackerStateC& src) const;	///< Propagates a given state object
};	//class RoamingCameraTrackingPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @throws invalid_argument If any of the input parameters is invalid
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
void RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::Validate()
{
	size_t minGeneratorSize=min(MainPoseSolverT::GeneratorSize(), MainCalibrationSolverT::GeneratorSize());
	if(world.size()<minGeneratorSize)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate : world must have at least ")+lexical_cast<string>(minGeneratorSize)+" elements. Value= "+lexical_cast<string>(world.size())  ) );

	if(parameters.nThreads==0)
		throw(invalid_argument("RoamingCameraTrackingPipelineC::Validate: parameters.nThreads must be a positive value") );

	if(parameters.imageNoiseVariance<=0)
		throw(invalid_argument(string("RoamingCameraTrackingPipelineC::Validate: parameters.imageNoiseVariance must be >0. Value=") + lexical_cast<string>(parameters.imageNoiseVariance)));

	if(parameters.predictionNoise<0)
		throw(invalid_argument(string("RoamingCameraTrackingPipelineCRoamingCameraTrackingPipelineCRoamingCameraTrackingPipelineC::Validate: parameters.predictionNoise must be >=0. Value=") + lexical_cast<string>(parameters.predictionNoise)));

	if(parameters.worldNoiseVariance<=0)
		throw(invalid_argument(string("RoamingCameraTrackingPipelineC::Validate: parameters.worldNoiseVariance must be >0. Value=") + lexical_cast<string>(parameters.worldNoiseVariance)));

	if(parameters.inlierRejectionProbability<=0 || parameters.inlierRejectionProbability>=1)
		throw(invalid_argument(string("RoamingCameraTrackingPipelineC::Validate: parameters.inlierRejectionProbability must be in (0,1). Value=") + lexical_cast<string>(parameters.inlierRejectionProbability)));

	if(parameters.loGeneratorRatio1<1)
		throw(invalid_argument(string("RoamingCameraTrackingPipelineC::Validate: parameters.loGeneratorRatio1 must be >=1. Value=") + lexical_cast<string>(parameters.loGeneratorRatio1)));

	if(parameters.loGeneratorRatio2<1)
		throw(invalid_argument(string("RoamingCameraTrackingPipelineC::Validate: parameters.loGeneratorRatio2 must be >=1. Value=") + lexical_cast<string>(parameters.loGeneratorRatio2)));

	if(parameters.focalLength && *parameters.focalLength<=0)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.focalLength must be positive. Value=") + lexical_cast<string>(*parameters.focalLength) ));

	if(parameters.focalLength && parameters.flagFixedFocalLength)
	{
		parameters.minFocalLength=*parameters.focalLength;
		parameters.maxFocalLength=*parameters.focalLength;
	}	//if(parameters.focalLength && parameters.flagFixedFocalLength)

	if(parameters.minFocalLength<=0)
		throw(invalid_argument ( string("RoamingCameraTrackingPipelineC::Validate: parameters.minFocalLength must be positive. Value=") + lexical_cast<string>(parameters.minFocalLength)) );

	if(parameters.maxFocalLength<parameters.minFocalLength)
		throw(invalid_argument ( string("RoamingCameraTrackingPipelineC::Validate: parameters.maxFocalLength must be >= minFocalLength. Value=") + lexical_cast<string>(parameters.maxFocalLength)) );

	if(parameters.supportLossTrackingFailure <0 || parameters.supportLossTrackingFailure>1)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.supportLossTrackingFailure must be in [0,1]. Value=") + lexical_cast<string>(parameters.supportLossTrackingFailure) ));

	if(parameters.supportLossModeSwitch <0 || parameters.supportLossModeSwitch>1)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.supportLossModeSwitch must be in [0,1]. Value=") + lexical_cast<string>(parameters.supportLossModeSwitch) ));

	if(parameters.focalLengthSensitivity<=0)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.focalLengthSensitivity must be positive. Value=") + lexical_cast<string>(parameters.focalLengthSensitivity) ));

	if(parameters.zoomMeasurementWeight <0 || parameters.zoomMeasurementWeight>1)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.zoomMeasurementWeight must be in [0,1]. Value=") + lexical_cast<string>(parameters.zoomMeasurementWeight) ));

	if(parameters.rotationMeasurementWeight <0 || parameters.rotationMeasurementWeight>1)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.rotationMeasurementWeight must be in [0,1]. Value=") + lexical_cast<string>(parameters.rotationMeasurementWeight) ));

	if(parameters.displacementMeasurementWeight <0 || parameters.displacementMeasurementWeight>1)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.displacementMeasurementWeight must be in [0,1]. Value=") + lexical_cast<string>(parameters.displacementMeasurementWeight) ));

	if(parameters.imageDiagonal<=0)
		throw(invalid_argument( string("RoamingCameraTrackingPipelineC::Validate: parameters.imageDiagonal must be positive. Value=") + lexical_cast<string>(parameters.imageDiagonal) ));

	parameters.pipelineParameters.nThreads=parameters.nThreads;
	parameters.pipelineParameters.flagVerbose=false;
	parameters.pipelineParameters.flagCovariance=false;
}	//void Validate()

/**
 * @brief Filters out the world points that are unlikely to be visible in the current frame
 * @param[in] prediction Predicted state
 * @return Indices to the filtered world points
 * @remarks FoV and viewing angle instead of projection into a frame. This preserves consistency with the nodal camera tracker, and simplifies the input parameters. A frame would need to be normalised with the known principal point, for example
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
auto RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::ApplyVisibilityFilter(const TrackerStateC& prediction) const -> vector<size_t>
{
	//Estimate the field-of-view
	double fov=ComputeFoV(parameters.imageDiagonal, prediction.focalLength);

	//Effective FoV is larger, due to the prediction noise
	double padding=MainCalibrationSolverT::error_type::ComputeOutlierThreshold(parameters.inlierRejectionProbability, parameters.imageNoiseVariance*(1+parameters.predictionNoise));
	fov*=1+(padding/parameters.imageDiagonal);

	double threshold=fabs(cos(fov/2));

	RotationVectorT principalAxis=prediction.orientation.matrix().row(2);

	size_t sWorld=world.size();
	vector<size_t> output; output.reserve(sWorld);
	for(size_t c=0; c<sWorld; ++c)
		if((world[c].Coordinate()-prediction.position).normalized().dot(principalAxis) > threshold)
			output.push_back(c);

	output.shrink_to_fit();
	return output;
}	//vector<SceneFeatureT> ApplyVisibilityFilter() const

/**
 * @brief Verifies the consistency of the current estimate with the state
 * @param[in] estimate Estimated camera
 * @param[in] correspondences Inliers to the estimate
 * @return \c true to indicate a tracker failure
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
bool RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::DetectTrackerFailure(const CameraMatrixT& estimate, const CorrespondenceListT& correspondences) const
{
	double deltaSupport= (double)correspondences.size()/record.sSupport-1.0;

	return deltaSupport < -parameters.supportLossTrackingFailure;
}	//bool DetectTrackerFailure(const CameraMatrixT& estimate, const CorrespondenceListT& correspondences)

/**
 * @brief Registers the current image to the world model
 * @param[in,out] rng Random number generator
 * @param[in, out] matchingProblem 3D-2D matching problem. Valid after the first call to this function when performing an update
 * @param[in] observations Image observations
 * @param[in] initialEstimate Initial estimate. Invalid if there is no valid state
 * @param[in] visibleLandmarks Indices of visible landarks. Invalid if no visibility analysis was made
 * @return A tuple: Diagnostics, estimated camera matrix, inliers
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
template<class GeometryEstimatorProblemT>
tuple<GeometryEstimationPipelineDiagnosticsC, CameraMatrixT, CorrespondenceListT> RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::RegisterCamera(RNGT& rng, optional<MatchingProblemT>& matchingProblem, const vector<ImageFeatureT>& observations, const optional<CameraMatrixT>& initialEstimate, const optional<vector<size_t> >& visibleLandmarks)
{
	//Reset the matching problem
	if(!visibleLandmarks)
		matchingProblem=optional<MatchingProblemT>();	//If relocalisation, reset the problem
/*	else
		if(matchingProblem)
			matchingProblem->SetConstraint(optional<typename MatchingProblemT::constraint_type>());	//Reset the constraints
*/
	//RANSAC problem
	typedef typename GeometryEstimatorProblemT::ransac_problem_type1 RANSACProblem1T;
	typedef typename GeometryEstimatorProblemT::ransac_problem_type2 RANSACProblem2T;

	unsigned int loGeneratorSize1=RANSACProblem1T::minimal_solver_type::GeneratorSize()*parameters.loGeneratorRatio1;
	unsigned int loGeneratorSize2=RANSACProblem2T::minimal_solver_type::GeneratorSize()*parameters.loGeneratorRatio2;

	vector<Coordinate2DT> coordinateList=MakeCoordinateVector(observations);
	typename RANSACProblem2T::dimension_type2 binSize2=*ComputeBinSize<Coordinate2DT>(coordinateList, parameters.pipelineParameters.binDensity2);

	typename RANSACProblem2T::data_container_type dummyList;
	RANSACProblem1T ransacProblem1=MakeRANSACPnPProblem<typename GeometryEstimatorProblemT::ransac_problem_type1::minimal_solver_type, typename GeometryEstimatorProblemT::ransac_problem_type1::lo_solver_type>(dummyList, dummyList, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSize1, binSize2);
	RANSACProblem2T ransacProblem2=MakeRANSACPnPProblem<typename GeometryEstimatorProblemT::ransac_problem_type2::minimal_solver_type, typename GeometryEstimatorProblemT::ransac_problem_type2::lo_solver_type>(dummyList, dummyList, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, 1, loGeneratorSize2, binSize1, binSize2);

	//PDL problem
	typedef typename GeometryEstimatorProblemT::optimisation_problem_type PDLProblemT;
	PDLProblemT pdlProblem=MakePDLPnPProblem<PDLProblemT>(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	//Geometry estimator problem
	GeometryEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, pdlProblem, initialEstimate);

	//Pipeline problem
	vector<Matrix3d> worldCovariance(1, parameters.worldNoiseVariance*Matrix3d::Identity());
	typedef GenericGeometryEstimationPipelineProblemC<GeometryEstimatorProblemT, MatchingProblemT> PipelineProblemT;
	PipelineProblemT problem;
	if(visibleLandmarks)
		problem=MakeGeometryEstimationPipelinePnPProblem<GeometryEstimatorProblemT, MatchingProblemT>(MakePermutationRange(world, *visibleLandmarks), observations, geometryEstimationProblem, worldCovariance, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, initialEstimate , parameters.imageNoiseVariance*(1+parameters.predictionNoise), parameters.inlierRejectionProbability, parameters.nThreads, parameters.flagApproximateNeighbourhoods);
	else
		problem=MakeGeometryEstimationPipelinePnPProblem<GeometryEstimatorProblemT, MatchingProblemT>(world, observations, geometryEstimationProblem, worldCovariance, parameters.imageNoiseVariance, parameters.inlierRejectionProbability, initialEstimate , parameters.imageNoiseVariance*(1+parameters.predictionNoise), parameters.inlierRejectionProbability, parameters.nThreads, parameters.flagApproximateNeighbourhoods);

	CameraMatrixT estimated;
	optional<MatrixXd> covariance;
	CorrespondenceListT correspondences;
	GeometryEstimationPipelineDiagnosticsC diagnostics=GeometryEstimationPipelineC<PipelineProblemT>::Run(estimated, covariance, correspondences, problem, matchingProblem, rng,parameters.pipelineParameters);

	++record.nRegistration;
	return make_tuple(diagnostics, estimated, correspondences);
}	//tuple<NodalCameraTrackingPipelineDiagnosticsC, CameraMatrixT, CorrespondenceListT> RegisterCamera(RNGT& rng, const vector<ImageFeatureT>& observations)

/**
 * @brief Sets a new tracker mode if necessary
 * @param[in] focalLengthEstimate Focal length
 * @param[in] correspondences Correspondences
 * @param[in] currentMode Current tracker mode
 * @param[in] flagRelocalisation \c true if there was a relocalisation event
 * @return New tracker mode
 * @pre \c flagInitialised=true
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
auto  RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::SetTrackerMode(double focalLengthEstimate, const CorrespondenceListT& correspondences, TrackerModeT currentMode, bool flagRelocalisation) const -> TrackerModeT
{
	//Preconditions
	assert(flagInitialised);

	if(parameters.flagFixedFocalLength)
		return TrackerModeT::POSE;

	//If relocalisation triggered and zoom is not fixed, release the zoom
	if(!parameters.flagFixedFocalLength && flagRelocalisation)
		return TrackerModeT::POSE_ZOOM;

	//Check support loss
	if(currentMode==TrackerModeT::POSE)
	{
		double deltaSupport= (double)correspondences.size()/record.sSupport-1.0;

		if(deltaSupport < -parameters.supportLossModeSwitch)
			return TrackerModeT::POSE_ZOOM;
	}	//if(currentMode==TrackerModeT::NODAL)

	//Check whether the focal length stabilised
	if(currentMode==TrackerModeT::POSE_ZOOM)
		if(flagInitialised)
		{
			double deltaF=1-focalLengthEstimate/trackerState.focalLength;

			if(fabs(deltaF)<parameters.focalLengthSensitivity)
				return TrackerModeT::POSE;
		}	//if(focalLength)

	return currentMode;
}	//TrackerModeT SetTrackerMode(const CameraMatrixT& estimate, const CorrespondenceListT& correspondences)

/**
 * @brief Updates the state
 * @param[in] measurement Measurement
 * @post \c trackerState is modified
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
void RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::UpdateState(const CameraMatrixT& measurement)
{
	RotationMatrix3DT mR;
	Coordinate3DT vC;
	IntrinsicCalibrationMatrixT mK;
	std::tie(mK, vC, mR)=DecomposeCamera(measurement);

	QuaternionT q(mR);

	if(flagInitialised)
	{
		if(trackerState.trackingMode==TrackerModeT::POSE)
			trackerState.zoomRate=0;
		else
		{
			double zoomRateMeasurement=mK(0,0) - trackerState.focalLength;
			trackerState.zoomRate = (1-parameters.zoomMeasurementWeight)*trackerState.zoomRate + parameters.zoomMeasurementWeight*zoomRateMeasurement;
		}

		QuaternionT deltaQ= q * trackerState.orientation.conjugate();
		RotationVectorT rotationMeasurement = QuaternionToRotationVector(deltaQ);
		trackerState.rotation = (1-parameters.rotationMeasurementWeight)*trackerState.rotation + parameters.rotationMeasurementWeight * rotationMeasurement;	//In this context, each element represents the rate of rotation around the corresponding axis

		Coordinate3DT deltaC = vC - trackerState.position;
		trackerState.displacement = (1-parameters.displacementMeasurementWeight)*trackerState.displacement + parameters.displacementMeasurementWeight * deltaC;
	}	//if(trackerState)

	trackerState.orientation=q;
	trackerState.position=vC;
	trackerState.focalLength=max(min(mK(0,0), parameters.maxFocalLength), parameters.minFocalLength);

}	//void UpdateState(const CameraMatrixT& measurement)

/**
 * @brief Default constructor
 * @post The object is invalid
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::RoamingCameraTrackingPipelineC() : flagValid(false), flagInitialised(false)
{}

/**
 * @brief Constructor
 * @tparam SceneFeatureRangeT A range of scene features
 * @param[in] wworld 3D point cloud
 * @param[in] pparameters Parameters
 * @pre \c SceneFeatureRangeT is a forward range
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
template<class SceneFeatureRangeT>
RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::RoamingCameraTrackingPipelineC(const SceneFeatureRangeT& wworld, const RoamingCameraTrackingPipelineParametersC& pparameters) : parameters(pparameters)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<SceneFeatureRangeT>));

	size_t nFeatures=boost::distance(wworld);
	world.reserve(nFeatures);
	for(const auto& current : wworld)
		world.push_back(current);

	vector<Coordinate3DT> coordinateList=MakeCoordinateVector(world);
	binSize1=*ComputeBinSize<Coordinate3DT>(coordinateList, parameters.pipelineParameters.binDensity1);

	Validate();

	trackerState.trackingMode= parameters.focalLength ? TrackerModeT::POSE : TrackerModeT::POSE_ZOOM;

	if(parameters.focalLength)
		trackerState.focalLength=*parameters.focalLength;

	flagValid=true;
	flagInitialised=false;

	record.sSupport=0;
}	//NodalCameraTrackingPipelineC(const SceneFeatureRangeT& wworld, const NodalCameraTrackingPipelineParametersC& pparameters)


/**
 * @brief Predicts the next measurement
 * @return Predicted state
 * @pre \c flagInitialised=true
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
auto RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::Predict() const -> TrackerStateC
{
	//Preconditions
	assert(flagInitialised);
	return PropagateState(trackerState);
}	//optional<CameraMatrixT> Predict() const

/**
 * @brief Updates the tracker state with the observation
 * @param[in,out] rng Random number generator
 * @param[in] observations Image features. Temporarily modified during processing
 * @return Diagnostics
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
RoamingCameraTrackingPipelineDiagnosticsC RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::Update(RNGT& rng, vector<ImageFeatureT>& observations)
{
	//TODO What happens if the first frame fails?
	RoamingCameraTrackingPipelineDiagnosticsC diagnostics;
	record.nRegistration=0;

	TrackerModeT trackingMode=trackerState.trackingMode;

	//Prediction, if there is a valid state
	optional<TrackerStateC> prediction;
	optional<vector<size_t> > visibleLandmarks;
	if(flagInitialised)
	{
		//Prediction
		prediction=Predict();
		visibleLandmarks=ApplyVisibilityFilter(*prediction);
	}	//if(flagInitialised)


	//Registration
	map< size_t, tuple<CameraMatrixT, GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, bool>, greater<size_t> > solutions;
	auto solutionInserter=[&](const CameraMatrixT& mP, const GeometryEstimationPipelineDiagnosticsC& solDiagnostics, const CorrespondenceListT& inliers, bool flagPose){ if(solDiagnostics.flagSuccess && inliers.size()>parameters.minSupport) solutions.emplace(inliers.size(), make_tuple(mP, solDiagnostics, inliers, flagPose));  };
	GeometryEstimationPipelineDiagnosticsC geDiagnostics;	//Keeps the diagnostics for the last call

//	GeometryEstimationPipelineDiagnosticsC geDiagnostics;
//	CameraMatrixT estimated;
//	CorrespondenceListT inliers;
	bool flagRelocalisation=false;

//trackingMode=(parameters.flagFixedFocalLength && parameters.focalLength) ? TrackerModeT::NODAL : TrackerModeT::NODAL_ZOOM;
										//EXPERIMENTAL This clamps the tracker in the nodal or nodal-zoom mode.
										//Bimodal tracker is slower due to:
										//- False tracking mode switch alarms: even a 10% drop in support triggers a second, nodal-zoom registration. If it fails, this leads to a relocalisation action. The nodal-zoom tracker can issue at most two calls to the registration module
										//- Even modest occlusions trigger tracking mode switch. The algorithm cannot distinguish between support loss due to zoom and support loss due to occlusions


	//Logic:
	//POSE
		// P3P with initial estimate
		// If fails and fixed focal length, P3P without initial estimate
		// If fails and variable focal length, POSE_ZOOM
	//POSE_ZOOM
		//P4P with initial estimate
		//If fails, P4P without initial estimate

	//Initial estimate
	optional<CameraMatrixT> initialEstimate;
	if(flagInitialised)
		initialEstimate=ComposeCameraMatrix(prediction->position, prediction->orientation.matrix());	//Normalised

	//Nodal
	optional<MatchingProblemT> matchingProblem;	// Holds the feature similarity matrix
	if(trackingMode==TrackerModeT::POSE)
	{
		//If we are here, there must be a valid focal length estimate
		double focalLength = prediction ? prediction->focalLength : trackerState.focalLength;

		IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity(); mK(0,0)=focalLength; mK(1,1)=mK(0,0);

		//Normalise
		auto scalingFunctor=[&](double scale){for_each(observations, [&](ImageFeatureT& current){current.Coordinate()*=scale;});
												parameters.imageNoiseVariance*=pow<2>(scale);};

		scalingFunctor(1.0/focalLength);

		//Estimate
		CameraMatrixT poseEstimate;
		CorrespondenceListT inliers;
		std::tie(geDiagnostics, poseEstimate, inliers)=RegisterCamera<PoseEstimationProblemT>(rng, matchingProblem, observations, initialEstimate, visibleLandmarks);

		CameraMatrixT mP=mK*poseEstimate;
		solutionInserter(mP, geDiagnostics, inliers, true);

		//Relocalise?
		flagRelocalisation=!geDiagnostics.flagSuccess || DetectTrackerFailure(mP, inliers);

		//If fixed focal length, relocalise with the nodal solver
		if(flagRelocalisation && parameters.flagFixedFocalLength)
		{
			std::tie(geDiagnostics, poseEstimate, inliers)=RegisterCamera<PoseEstimationProblemT>(rng, matchingProblem, observations, optional<CameraMatrixT>(), optional<vector<size_t> >());
			solutionInserter(mK*poseEstimate, geDiagnostics, inliers, true);
		}

		//If there is a large support loss, try the full calibration solver
		if(!parameters.flagFixedFocalLength)
		{
			trackingMode=SetTrackerMode(focalLength, inliers, trackingMode, flagRelocalisation);

			//If there is a mode change, reset the matching problem
			if(trackingMode==TrackerModeT::POSE_ZOOM)
				matchingProblem=optional<MatchingProblemT>();	//Clear the matching problem for the pose-zoom registration attempt
																	//The denormalisation does not affect the similarity scores, so, in theory, a complete reset is wasteful
																	//But experiments suggest that denormalising the coordinates is slightly slower.
		}	//if(!flagRelocalisation)

		//Denormalise
		scalingFunctor(focalLength);
	}	//if(trackerState.trackingMode==TrackerModeT::NODAL)

	//Pose and zoom
	if(trackingMode==TrackerModeT::POSE_ZOOM)
	{
		if(initialEstimate)
		{
			IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity(); mK(0,0)=prediction->focalLength; mK(1,1)=mK(0,0);
			initialEstimate= mK * (*initialEstimate);
		}

		CameraMatrixT calibrationEstimate;
		CorrespondenceListT inliers;
		std::tie(geDiagnostics, calibrationEstimate, inliers)=RegisterCamera<CalibrationEstimationProblemT>(rng, matchingProblem, observations, initialEstimate, visibleLandmarks);
		solutionInserter(calibrationEstimate, geDiagnostics, inliers, false);

		//Trigger relocalisation?
		//Conditions:
			//The best solution in the set warrants a relocalisation. This implies that a NODAL->NODAL_ZOOM promotion cannot trigger a relocalisation call
			//Or the solution set is empty
		flagRelocalisation = solutions.empty() || DetectTrackerFailure(get<0>(solutions.begin()->second), get<2>(solutions.begin()->second));
	}	//if(currentMode==TrackerModeT::NODAL_ZOOM)

	if(flagRelocalisation && !parameters.flagFixedFocalLength)
	{
		CameraMatrixT calibrationEstimate;
		CorrespondenceListT inliers;
		std::tie(geDiagnostics, calibrationEstimate, inliers)=RegisterCamera<CalibrationEstimationProblemT>(rng, matchingProblem, observations, optional<CameraMatrixT>(), optional<vector<size_t> >());
		solutionInserter(calibrationEstimate, geDiagnostics, inliers, false);

		trackingMode=TrackerModeT::POSE_ZOOM;	//If relocalisation fails, the mode remains as NODAL_ZOOM. Else, updated by SetTrackerMode
	}	//if(flagRelocalisation && !parameters.flagFixedFocalLength)

	//Update
	if(solutions.empty())
	{
		diagnostics.geDiagnostics=geDiagnostics;
		flagInitialised=false;	//Relocalisation has failed. Lost!
	}
	else
	{
		//Get the best solution
		CameraMatrixT bestEstimate;
		CorrespondenceListT bestInliers;
		std::tie(bestEstimate, diagnostics.geDiagnostics, bestInliers, diagnostics.flagPose)=solutions.begin()->second;

		//FIXME bestInliers are pointing to the visible points, rather than world
		//Backup
		TrackerStateC prevState(trackerState);

		//Update the state
		UpdateState(bestEstimate);

		//Toggle mode
		trackerState.trackingMode=SetTrackerMode(prevState.focalLength, bestInliers, trackingMode, flagRelocalisation);

		record.sSupport=bestInliers.size();

		diagnostics.flagSuccess=true;
		diagnostics.nInliers=bestInliers.size();
		diagnostics.supportSet.swap(bestInliers);
		flagInitialised=true;
	}	//if(solutions.empty())

	diagnostics.flagRelocalise=flagRelocalisation;
	diagnostics.nRegistration=record.nRegistration;

	return diagnostics;
}	//NodalCameraTrackingPipelineDiagnosticsC Update(const vector<ImageFeatureT>& observations)

/**
 * @brief Returns the state
 * @return Returns the state
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
auto RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::GetState() const -> const TrackerStateC&
{
	return trackerState;
}	//const optional<CameraMatrixT>&GetState()

/**
 * @brief Propagates a given state object
 * @param[in] src State to be propagated
 * @return Propagated state
 * @remarks The function does not change the state of the tracker
 */
template <class PoseEstimationProblemT, class CalibrationEstimationProblemT, class MatchingProblemT>
auto RoamingCameraTrackingPipelineC<PoseEstimationProblemT, CalibrationEstimationProblemT, MatchingProblemT >::PropagateState(const TrackerStateC& src) const -> TrackerStateC
{
	TrackerStateC propagated(src);

	if(propagated.trackingMode==TrackerModeT::POSE_ZOOM)
		propagated.focalLength = max(min(src.focalLength + src.zoomRate, parameters.maxFocalLength), parameters.minFocalLength);

	propagated.orientation = RotationVectorToQuaternion(src.rotation) * src.orientation;
	propagated.position = src.position + src.displacement;

	return propagated;
}	//TrackerStateC PropagateState(const TrackerStateC& src)

}	//GeometryN
}	//SeeSawN



#endif /* ROAMING_CAMERA_TRACKING_PIPELINE_IPP_9901290 */
