/**
 * @file CalibrationVerificationPipeline.ipp Implementation of the calibration verification pipeline
 * @author Evren Imre
 * @date 30 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CALIBRATION_VERIFICATION_PIPELINE_IPP_1659821
#define CALIBRATION_VERIFICATION_PIPELINE_IPP_1659821

//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/counting_range.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <vector>
#include <cstddef>
#include <tuple>
#include <functional>
#include <cmath>
#include <map>
#include <set>
#include <string>
#include <stdexcept>
#include <climits>
#include <iostream>
#include "../Elements/Camera.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../Geometry/FundamentalSolver.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Optimisation/PDLFundamentalMatrixEstimationProblem.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Wrappers/BoostDynamicBitset.h"
#include "../GeometryEstimationComponents/Fundamental7Components.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::lexical_cast;
using boost::dynamic_bitset;
using boost::optional;
using boost::logic::tribool;
using boost::logic::indeterminate;
using boost::range::for_each;
using boost::counting_range;
using Eigen::Matrix3d;
using Eigen::MatrixXd;
using Eigen::ArrayX2d;
using std::vector;
using std::size_t;
using std::tuple;
using std::get;
using std::make_tuple;
using std::tie;
using std::greater;
using std::floor;
using std::map;
using std::set;
using std::string;
using std::invalid_argument;
using std::numeric_limits;
using std::cout;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineFundamental7Problem;
using SeeSawN::GeometryN::MakeRANSACFundamental7Problem;
using SeeSawN::GeometryN::MakePDLFundamentalProblem;
using SeeSawN::GeometryN::RANSACFundamental7ProblemT;
using SeeSawN::GeometryN::PDLFundamentalProblemT;
using SeeSawN::GeometryN::Fundamental7EstimatorProblemT;
using SeeSawN::GeometryN::Fundamental7PipelineProblemT;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::OptimisationN::PDLFundamentalMatrixEstimationProblemC;
using SeeSawN::GeometryN::Fundamental7SolverC;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::WrappersN::MakeBitset;

/**
 * @brief Parameters for \c CalibrationVerificationPipelineC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct CalibrationVerificationPipelineParametersC
{
	unsigned int nThreads;	///< Number of threads available to the algorithm
	int seed;	///< Random number seed. If <0, default

	double noiseVariance;	///< Noise variance, in pixel^2. >0
	double inlierRejectionProbability;	///< Probability that an inlier is falsely rejected. (0,1)
	double loGeneratorRatio1;	///< Size of the generator set for local optimisation, in multiples of the minimal generator, for the first RANSAC stage. >=1
	double loGeneratorRatio2;	///< Size of the generator set for local optimisation, in multiples of the minimal generator, for the second RANSAC stage. >=1
	double moMaxAlgebraicDistance;	///< Maximum algebraic distance between two similar models. [0,1]

	double initialEstimateNoiseVariance;	///< Additive noise to compensate for the errors in the initial estimate, as a multiple of the noise variance. >=0

	double minInlierRatio;	///< If the best inlier ratio for a camera pair is below this value, the it is discarded. [0,1]
	unsigned int minSupport;	///< If the best support for a camera pair is below this value, the it is discarded
	double pairwiseDecisionTh;	///< If the total loss for the predicted fundamental matrix to that for the estimated is above this value, the pair is flagged as perturbed >1

	unsigned int minPairwiseRelation;	///< If the number of pairs a camera participates is fewer than this threshold, its status is undetermined
	unsigned int maxPerturbed;	///< Maximum number of perturbed cameras

	bool flagVerbose;	///< \c Verbosity flag
	GeometryEstimationPipelineParametersC geometryEstimationPipelineParameters;	///< Parameters for the geometry estimation pipeline

	/** @brief Default constructor */
	CalibrationVerificationPipelineParametersC() : nThreads(1), seed(0), noiseVariance(4), inlierRejectionProbability(0.05), loGeneratorRatio1(3), loGeneratorRatio2(3), moMaxAlgebraicDistance(1), initialEstimateNoiseVariance(4), minInlierRatio(0.2), minSupport(20), pairwiseDecisionTh(1.05), minPairwiseRelation(2), maxPerturbed(numeric_limits<unsigned int>::max()), flagVerbose(false)
	{}
};	//struct CalibrationVerificationPipelineParametersC

/**
 * @brief Diagnostics object for \c CalibrationVerificationPipelineC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct CalibrationVerificationPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully
	unsigned int nPairs;	///< Number of valid pairwise relations
	unsigned int nConsistent;	///< Number of relations consistent with the best hypothesis
	double bestScore;	///< Score for the best labelling hypothesis

	/** @brief Default constructor */
	CalibrationVerificationPipelineDiagnosticsC() : flagSuccess(false), nPairs(0), nConsistent(0), bestScore(-1)
	{}

};	//struct CalibrationVerificationPipelineDiagnosticsC

//TODO There should be a way to disable the initial estimate facility
/**
 * @brief Verifies the validity of the calibration information for a multicamera setup
 * @remarks Too complex for a dedicated unit test, beyond a compile test. Tested via the application
 * @remarks Algorithm
 * 	- Estimate the fundamental matrix corresponding to each camera pair. Compare the fitness score to the fundamental matrix predicted from the provided calibration
 * 	- For a pair, if the score favours the estimated F, the pair contains at least one perturbed camera
 *  - Exhaustively search all labellings to find the perturbation hypothesis that is most consistent with the pairwise decisions
 * 	- Confidence score: Total weight of the pairs which involve a camera, and consistent with the final labelling, to the total weight of all pairs involving the camera. Often [0.5,1], but could go below 0.5 occasionally.
 * @remarks Usage notes
 * 	- If the initial estimation is bad (large perturbation), the algorithm discards it, and attempts to estimate the F again, without the initial estimate.
 * 	- If the initial estimate noise is set too low and the perturbation is small, the algorithm might be locked into the predicted F, and miss perturbed cameras.
 * 	- For an isolated camera, such a terminal cameras in a setup, the algorithm may fail to get sufficiently many pairs for a reliable estimation.
 * @ingroup Algorithm
 * @nosubgrouping
 */
class CalibrationVerificationPipelineC
{
	public:

		/** @name Result type */ //@{
		typedef tuple<tribool, double> result_type;	///< Result type
		static constexpr unsigned int iStatus=0;	///< Index of the status of the camera
		static constexpr unsigned int iConfidence=1;	///< Index of the confidence score
		///@}

	private:

		typedef dynamic_bitset<> IndicatorArrayT;	///< Array of indicator flags

		/** @name RecordT structure */ //@{
		typedef tuple<unsigned int, double, unsigned int, double, bool> RecordT;	///< Evaluation record for a camera pair
		static constexpr unsigned int iSupportE=0;	///< Index of the component for the support of the estimated fundamental matrix
		static constexpr unsigned int iScoreE=1;	///< Index of the component for the fitness score for the estimated fundamental matrix
		static constexpr unsigned int iSupportP=2;	///< Index of the component for the support of the predicted fundamental matrix
		static constexpr unsigned int iScoreP=3;	///< Index of the component for the fitness score for the predicted fundamental matrix
		static constexpr unsigned int iPerturbed=4;	///< Index of the component indicating whether the pair includes any perturbed cameras
		//@}

		/** @name Implementation details */ //@{

		static void ValidateInput(CalibrationVerificationPipelineParametersC& parameters, const set<size_t>& indexCorrect, unsigned int nCameras);	///< Validates the input parameters

		typedef RANSACGeometryEstimationProblemC<Fundamental7SolverC, Fundamental7SolverC>::dimension_type1 BinT;	///< Bin dimension structure type
		static tuple<vector<BinT>, vector<vector<Coordinate2DT> > > PreprocessInput(const vector<vector<ImageFeatureC> >& featureStack, const CalibrationVerificationPipelineParametersC& parameters);	///< Preprocesses the input data

		typedef tuple<size_t, size_t> PairIdT;	///< A pair id
		typedef tuple<PairIdT, EpipolarMatrixT, unsigned int> TaskT;	///< A task. [Camera ids; Fundamental matrix; no. threads]
		static vector<TaskT> GenerateTasks( const vector<CameraMatrixT>& cameraList, const vector<vector<ImageFeatureC>>& featureStack, const set<size_t>& indexCorrect, const CalibrationVerificationPipelineParametersC& parameters);	///< Generates the pairwise verification tasks

		template<class SimilarityT> static GeometryEstimationPipelineDiagnosticsC ComputeFundamentalMatrix(EpipolarMatrixT& mF, const vector<ImageFeatureC>& set1, const vector<ImageFeatureC>& set2, const optional<EpipolarMatrixT>& initialEstimate, const BinT binSize1, const BinT binSize2, SimilarityT similarityMetric, unsigned int nThreads, int seed, const CalibrationVerificationPipelineParametersC& parameters);	///< Computes the fundamental matrix from the image features

		static tuple<unsigned int, double> EvaluateF(const EpipolarMatrixT& mF, const CoordinateCorrespondenceList2DT& correspondences, double inlierTh);	///< Evaluates a fundamental matrix
		static optional<RecordT> MakeRecord(const optional<EpipolarMatrixT>& mFEstimated, const EpipolarMatrixT& mFPredicted, const CoordinateCorrespondenceList2DT& correspondences, const CalibrationVerificationPipelineParametersC& parameters);	///< Makes a pairwise evaluation record

		static vector<result_type> LabelCameras(CalibrationVerificationPipelineDiagnosticsC& diagnostics, const map<PairIdT, RecordT>& pairwiseRecords, const set<size_t>& indexCorrect, size_t nCameras, const CalibrationVerificationPipelineParametersC& parameters);	///< Labels the cameras as perturbed, intact or undetermined
		//@}

	public:

		template<class SimilarityT> static CalibrationVerificationPipelineDiagnosticsC Run(vector<result_type>& labels, const vector<CameraMatrixT>& cameraList, const vector<vector<ImageFeatureC> >& featureStack, const set<size_t>& indexCorrect, const SimilarityT& similarityMetric, CalibrationVerificationPipelineParametersC parameters);	///< Runs the algorithm

};	//class CalibrationVerificationPipelineC

/**
 * @brief Computes the fundamental matrix from the image features
 * @tparam SimilarityT A similarity metric
 * @param[out] mF Estimated fundamental matrix
 * @param[in] set1 Features in the first image
 * @param[in] set2 Features in the second image
 * @param[in] initialEstimate Fundamental matrix predicted from the input. Invalid if no initial estimate is specified
 * @param[in] binSize1 Dimensions of the bins for spatial quantisatoin, first set
 * @param[in] binSize2 Dimensions of the bins for spatial quantisation, second set
 * @param[in] similarityMetric Image feature similarity metric. Passed by value on purpose
 * @param[in] nThreads Number of threads
 * @param[in] seed Random number seed
 * @param[in] parameters Parameters
 * @return Diagnostics object
 */
template<class SimilarityT>
GeometryEstimationPipelineDiagnosticsC CalibrationVerificationPipelineC::ComputeFundamentalMatrix(EpipolarMatrixT& mF, const vector<ImageFeatureC>& set1, const vector<ImageFeatureC>& set2, const optional<EpipolarMatrixT>& initialEstimate, const BinT binSize1, const BinT binSize2, SimilarityT similarityMetric, unsigned int nThreads, int seed, const CalibrationVerificationPipelineParametersC& parameters)
{
	//Initialise the problem

	Fundamental7SolverC solver;
	unsigned int loGeneratorSize1=floor(Fundamental7SolverC::GeneratorSize()*parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2=floor(Fundamental7SolverC::GeneratorSize()*parameters.loGeneratorRatio2);

	typename RANSACFundamental7ProblemT::data_container_type dummyList;


	//RANSAC problem

/*
	typedef typename Fundamental7SolverC::error_type GeometricErrorT;
	double inlierTh=GeometricErrorT::ComputeOutlierThreshold(parameters.inlierRejectionProbability, parameters.noiseVariance);
	typename Fundamental7SolverC::loss_type lossFunction(inlierTh, inlierTh);
	GeometricErrorT errorMetric;
	EpipolarSampsonConstraintT constraint(errorMetric, inlierTh, 1);

	Fundamental7SolverC solver;
	unsigned int loGeneratorSize1=floor(Fundamental7SolverC::GeneratorSize()*parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2=floor(Fundamental7SolverC::GeneratorSize()*parameters.loGeneratorRatio2);

	typename RANSACProblemT::data_container_type dummyList;
	RANSACProblemT ransacProblem1(dummyList, dummyList, constraint, lossFunction, solver, solver, 1, loGeneratorSize1, binSize1, binSize2);
	RANSACProblemT ransacProblem2(dummyList, dummyList, constraint, lossFunction, solver, solver, 1, loGeneratorSize2, binSize1, binSize2);

	//PDL problem
	PDLFundamentalMatrixEstimationProblemC pdlProblem(Matrix3d::Zero(), dummyList, solver, errorMetric, optional<MatrixXd>());

	//Geometry estimator problem
	typedef GenericGeometryEstimationProblemC<RANSACProblemT, RANSACProblemT, PDLFundamentalMatrixEstimationProblemC> GeometryEstimatorProblemT;
	GeometryEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, pdlProblem, initialEstimate);

	//Geometry estimator pipeline problem

	typedef GenericGeometryEstimationPipelineProblemC<GeometryEstimatorProblemT, ImageFeatureMatchingProblemC<SimilarityT, EpipolarSampsonConstraintT> > ProblemT;

	vector<typename ProblemT::covariance_type1> covarianceList(1);
	covarianceList[0]<<parameters.noiseVariance,0, 0, parameters.noiseVariance;

	optional<EpipolarSampsonConstraintT> initialConstraint;
	if(initialEstimate)
	{
		double initialInlierTh=GeometricErrorT::ComputeOutlierThreshold(parameters.inlierRejectionProbability, (1+parameters.initialEstimateNoiseVariance)*parameters.noiseVariance);
		initialConstraint=EpipolarSampsonConstraintT(GeometricErrorT(*initialEstimate), initialInlierTh, nThreads);
	}

	EpipolarSampsonConstraintT shellConstraint=geometryEstimationProblem.RANSACProblem2().GetConstraint();

	ProblemT problem(set1, set2, covarianceList, covarianceList, similarityMetric, shellConstraint, initialConstraint, geometryEstimationProblem);
*/
	RANSACFundamental7ProblemT ransacProblem1=MakeRANSACFundamental7Problem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSize1, binSize2);
	RANSACFundamental7ProblemT ransacProblem2=MakeRANSACFundamental7Problem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, 1, loGeneratorSize1, binSize1, binSize2);

	PDLFundamentalMatrixEstimationProblemC pdlProblem=MakePDLFundamentalProblem(Matrix3d::Zero(), dummyList, optional<MatrixXd>());

	Fundamental7EstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, pdlProblem, initialEstimate);

	Fundamental7PipelineProblemT problem=MakeGeometryEstimationPipelineFundamental7Problem(set1, set2, geometryEstimationProblem, parameters.noiseVariance, parameters.inlierRejectionProbability,initialEstimate, (1+parameters.initialEstimateNoiseVariance)*parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.nThreads);

	//Estimate the fundamental matrix
	optional<MatrixXd> covariance;
	CorrespondenceListT correspondences;

	typedef GeometryEstimationPipelineC<Fundamental7PipelineProblemT> PipelineT;
	typename PipelineT::rng_type rng;
		if(seed>=0)
			rng.seed(seed);

	GeometryEstimationPipelineDiagnosticsC gepDiagnostics;
	return PipelineT::Run(mF, covariance, correspondences, problem, rng, parameters.geometryEstimationPipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeFundamentalMatrix(EpipolarMatrixT& mF, const vector<ImageFeatureC>& set1, const vector<ImageFeatureC>& set2, const optional<EpipolarMatrixT>& initialEstimate, const CalibrationVerificationPipelineParametersC& parameters)

/**
 * @brief Runs the algorithm
 * @tparam SimilarityT An image feature similarity metric
 * @param[out] labels Indicates whether the calibration information for a camera is correct. [Label; Confidence]
 * @param[in] cameraList Camera matrices to be validated
 * @param[in] featureStack Image features observed by the camera matrices
 * @param[in] indexCorrect Indices of the cameras that are known to be correct
 * @param[in] similarityMetric Image feature similarity metric
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @pre \c cameraList and \c featureStack have the same number of elements
 */
template<class SimilarityT>
CalibrationVerificationPipelineDiagnosticsC CalibrationVerificationPipelineC::Run(vector<result_type>& labels, const vector<CameraMatrixT>& cameraList, const vector<vector<ImageFeatureC>>& featureStack, const set<size_t>& indexCorrect, const SimilarityT& similarityMetric, CalibrationVerificationPipelineParametersC parameters  )
{
	//Preconditions
	assert(cameraList.size()==featureStack.size());

	CalibrationVerificationPipelineDiagnosticsC diagnostics;

	//Validate the input
	ValidateInput(parameters, indexCorrect, cameraList.size());

	//Empty camera set
	if(cameraList.empty())
	{
		diagnostics.flagSuccess=true;
		return diagnostics;
	}

	//Preprocessing
	vector< vector<Coordinate2DT> > coordinateStack;
	vector<BinT> binSizes;
	tie(binSizes, coordinateStack)=PreprocessInput(featureStack, parameters);

	//Task generation
	vector<TaskT> taskList=GenerateTasks(cameraList, featureStack, indexCorrect, parameters);

	//Process the tasks
	map<PairIdT, RecordT> pairwiseRecords;	//Pairwise evaluation results
	unsigned int nThreads=parameters.nThreads;
	size_t nTasks=taskList.size();
	CalibrationVerificationPipelineParametersC localParameters=parameters;	//Local copy of the parameters object
	size_t c=0;
#pragma omp parallel for if(nThreads>1) schedule(dynamic) private(c) firstprivate(localParameters) num_threads(nThreads)
	for(c=0; c<nTasks; ++c)
	{
		//Camera indices
		size_t i1;
		size_t i2;
		tie(i1,i2)=get<0>(taskList[c]);

		EpipolarMatrixT mFPredicted=get<1>(taskList[c]);

		if(coordinateStack[i1].empty() || coordinateStack[i2].empty())
			continue;	//This signals a problem at the preprocessing

		//Estimate the fundamental matrix from the image measurements
		optional<EpipolarMatrixT> initialEstimate=mFPredicted;	//Since most of the cameras are unperturbed, this is likely to accelerate the operation
		localParameters.nThreads=get<2>(taskList[c]);
		EpipolarMatrixT mF;
		GeometryEstimationPipelineDiagnosticsC gepDiagnostics=ComputeFundamentalMatrix(mF, featureStack[i1], featureStack[i2], initialEstimate, binSizes[i1], binSizes[i2], similarityMetric, get<2>(taskList[c]), parameters.seed+c, localParameters);

		//If the algorithm fails due to bad initial estimate (large perturbation), try again without it
		if(!gepDiagnostics.flagSuccess)
		{
			initialEstimate=optional<EpipolarMatrixT>();
			gepDiagnostics=ComputeFundamentalMatrix(mF, featureStack[i1], featureStack[i2], initialEstimate, binSizes[i1], binSizes[i2], similarityMetric, get<2>(taskList[c]), parameters.seed+(2*c+1), localParameters);
		}	//if(!gepDiagnostics.flagSuccess && initialEstimate)

		if(!gepDiagnostics.initialObservationSet)
			continue;

		optional<EpipolarMatrixT> mFEstimated;
		if(gepDiagnostics.flagSuccess)
			mFEstimated=mF;

		//Compare the estimated F with the prediction
		CoordinateCorrespondenceList2DT referenceSet=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(*gepDiagnostics.initialObservationSet, coordinateStack[i1], coordinateStack[i2]);	//Reference correspondences
		optional<RecordT> currentRecord=MakeRecord(mFEstimated, mFPredicted, referenceSet, parameters);

	#pragma omp critical (CVP_R1)
		{
		cout<<"CVP: "<<"Pair "<<i1<<" "<<i2<<". ";

		if(currentRecord)
		{
			pairwiseRecords.emplace(PairIdT(i1,i2), *currentRecord);
			cout<< (get<iPerturbed>(*currentRecord)? "Perturbed":"Unperturbed")<<". Weight: " << max(get<iScoreE>(*currentRecord), get<iScoreP>(*currentRecord))<<". Decision metric: "<< (double)get<iSupportE>(*currentRecord)/get<iSupportP>(*currentRecord)<<"\n";
		}
		else
			cout<<"Failed\n";

		}
	}	//for(c=0; c<nTasks; ++c)


	//Global labelling
	size_t nCameras= cameraList.size();
	labels=LabelCameras(diagnostics, pairwiseRecords, indexCorrect, nCameras, parameters);

	diagnostics.flagSuccess=true;
	diagnostics.nPairs=pairwiseRecords.size();

	return diagnostics;
}	//CalibrationVerificationPipelineDiagnosticsC Run(vector<size_t> indexFailed, const vector<CameraC>& cameraList, const vector<ImageFeatureC>& featureStack, const vector<size_t>& indexCorrect )


}	//GeometryN
}	//SeeSawN

#endif /* CALIBRATION_VERIFICATION_PIPELINE_IPP_1659821 */
