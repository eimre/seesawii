/**
 * @file MultimodelGeometryEstimationPipeline.ipp Implementation of the multimodel geometry estimation pipeline
 * @author Evren Imre
 * @date 13 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_IPP_9123092
#define MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_IPP_9123092

#include <boost/optional.hpp>
#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include <cstddef>
#include <cmath>
#include <stdexcept>
#include <string>
#include <limits>
#include <map>
#include <utility>
#include "GeometryEstimationPipeline.h"
#include "GeometryEstimationPipelineProblemConcept.h"
#include "GeometryEstimatorProblemConcept.h"
#include "../RANSAC/SequentialRANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblemConcept.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Elements/Correspondence.h"
#include "../Elements/CorrespondenceDecimator.h"
#include "../Elements/FeatureConcept.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Metrics/GeometricConstraintConcept.h"
#include "../Metrics/RobustLossFunctionConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::lexical_cast;
using Eigen::MatrixXd;
using std::vector;
using std::tuple;
using std::tie;
using std::size_t;
using std::min;
using std::floor;
using std::string;
using std::invalid_argument;
using std::cout;
using std::numeric_limits;
using std::map;
using std::pair;
using std::make_pair;
using std::back_inserter;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimationPipelineProblemConceptC;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;
using SeeSawN::RANSACN::SequentialRANSACC;
using SeeSawN::RANSACN::SequentialRANSACParametersC;
using SeeSawN::RANSACN::SequentialRANSACDiagnosticsC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemConceptC;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::ElementsN::CorrespondenceDecimatorC;
using SeeSawN::ElementsN::CorrespondenceDecimatorParametersC;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::ElementsN::DecimatorQuantisationStrategyT;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::MetricsN::GeometricConstraintConceptC;
using SeeSawN::MetricsN::RobustLossFunctionConceptC;

/**
 * @brief Parameters for MultimodelGeometryEstimationPipelineC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct MultimodelGeometryEstimationPipelineParametersC
{
	enum class AssignmentStrategyT{SOFT, HARD, UNIQUE};	///< Strategy for assigning the correspondences to the models: SOFT, shared assignments. HARD, 1:1 assignment. UNIQUE: correspondences that are inliers to multiple models are discarded

	unsigned int nThreads;	///< Number of threads available to the pipeline

	double minInlierRatio;	///< Minimum inlier ratio in the initial correspondence list for an acceptable model

	double maxObservationDensity;	///< Maximum number of observations per bin >0
	double observationRatio;	///< Minimum number of observations, as a multiple of the generator size. >0
	double validatorRatio;	///< Number of validators, as a multiple of the generator size. >0
	double binDensity1;	///< Number of bins per std for the first set
	double binDensity2;	///< Number of bins per std for the second set

	AssignmentStrategyT assignmentType;	///< Assignment strategy for determining the support

	bool flagVerbose;	///< If \c true verbose output

	FeatureMatcherParametersC matcherParameters;	///< Parameters for the feature matcher
	SequentialRANSACParametersC ransacParameters;	///< Parameters for the sequential ransac
	GeometryEstimationPipelineParametersC geometryEstimatorParameters;	///< Parameters for the single-model geometry estimation pipeline

	MultimodelGeometryEstimationPipelineParametersC() : nThreads(1), minInlierRatio(ransacParameters.minInlierRatio), maxObservationDensity(2), observationRatio(14), validatorRatio(14), binDensity1(4), binDensity2(4), assignmentType(AssignmentStrategyT::HARD), flagVerbose(false)
	{
	    minInlierRatio=ransacParameters.minInlierRatio; //FIXME Initialiser list does not initialise. Why?
	}

};	//struct MultimodelGeometryEstimationPipelineParametersC

/**
 * @brief Diagnostics for MultimodelGeometryEstimationPipelineC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct MultimodelGeometryEstimationPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully

	FeatureMatcherDiagnosticsC matcherDiagnostics;	///< Diagnostics for the feature matcher
	SequentialRANSACDiagnosticsC ransacDiagnostics;	///< Diagnostics for the sequential RANSAC stage
	vector<GeometryEstimationPipelineDiagnosticsC> geometryEstimatorDiagnostics;	///< Diagnostics for the single-model geometry estimation pipeline

	double error;	///< Total error over the initial correspondence list
	unsigned int support;	///< Number of correspondences in the initial list that is assigned to at least one model

	MultimodelGeometryEstimationPipelineDiagnosticsC() : flagSuccess(false), error(numeric_limits<double>::infinity()), support(0)
	{}
};	//struct MultimodelGeometryEstimationPipelineDiagnosticsC

/**
 * @brief Pipeline for simultaneous estimation of multiple geometric entities supported by a correspondence set
 * @tparam GeometryProblemT A geometry estimation pipeline problem
 * @remarks Usage notes:
 * 	- Algorithm:
 * 		- Find feature correspondences
 * 		- Identify the models supported by the correspondence set
 * 		- Refine each model by guided matching on the original correspondence set
 * 	- The final solution set is evaluated over the initial correspondences.
 * 	- The postprocessing set prunes the solution set, by testing the support over the original correspondence set
 * 	- Guided matching discovers correspondences that do not exist in the initial list. So, the total support for the individual models could be higher than that reported in diagnostics
 * 	- Segmentation
 * 		- Assignment strategy: HARD or UNIQUE. UNIQUE may suppress weaker models, especially if the support sets are not well-separated.
 * @remarks The unit test only covers compilation, not the operation
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class GeometryProblemT>
class MultimodelGeometryEstimationPipelineC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometryEstimationPipelineProblemConceptC<GeometryProblemT>));
	BOOST_CONCEPT_ASSERT((GeometryEstimatorProblemConceptC<typename GeometryProblemT::geometry_estimation_problem_type>));
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<typename GeometryProblemT::matching_problem_type>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<typename GeometryProblemT::matching_problem_type::feature_type1>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<typename GeometryProblemT::matching_problem_type::feature_type2>));
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<typename GeometryProblemT::geometry_estimation_problem_type::ransac_problem_type2>));
	BOOST_CONCEPT_ASSERT((GeometricConstraintConceptC<typename GeometryProblemT::geometry_estimation_problem_type::ransac_problem_type2::constraint_type>));
	BOOST_CONCEPT_ASSERT((RobustLossFunctionConceptC<typename GeometryProblemT::geometry_estimation_problem_type::ransac_problem_type2::loss_type>));
	//@endcond

	private:

		typedef typename GeometryProblemT::matching_problem_type MatchingProblemT;	///< Type for the feature matching problem
		typedef typename GeometryProblemT::geometry_estimation_problem_type::ransac_problem_type2 RANSACProblemT;	///< Type of the RANSAC problem

		typedef GeometryEstimationPipelineC<GeometryProblemT> GeometryEstimationPipelineT;	///< Single model geometry estimation pipeline
		typedef typename GeometryEstimationPipelineT::model_type ModelT;	///< Model type

	public:

		/**@name Problem tuple */ //{@
		typedef tuple<RANSACProblemT, GeometryProblemT> problem_type;	///< Structure holding the associated problems
		static constexpr unsigned int iRANSACProblem=0;	///< Index for the RANSAC problem component
		static constexpr unsigned int iGeometryProblem=1;	///< Index for the geometry problem component
		//@}

		/**@name Definition of the solution type */ //@{
		typedef tuple<ModelT, optional<MatrixXd>, CorrespondenceListT> solution_type;	///< A solution
		static constexpr unsigned int iModel=0;	///< Index of the component for the estimated model
		static constexpr unsigned int iCovariance=1;	///< Index of the component for the estimated covariance
		static constexpr unsigned int iInlierList=2;	///< Index of the component for the inlier observations
		//@}


	private:

		typedef MultimodelGeometryEstimationPipelineParametersC::AssignmentStrategyT AssignmentStrategyT;

		typedef problem_type ProblemT;

		typedef typename GeometryEstimationPipelineT::rng_type RNGT;	///< Random number generator type

		typedef typename MatchingProblemT::feature_type1 Feature1T;	///< Type of the features in the first set
		typedef typename MatchingProblemT::feature_type2 Feature2T;	///< Type of the features in the second set
		typedef typename Feature1T::coordinate_type Coordinate1T;	///< Type for the first coordinate
		typedef typename Feature2T::coordinate_type Coordinate2T;	///< Type for the second coordinate

		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CorrespondenceContainerT;	///< Type for the container for corresponding coordinates

		typedef typename RANSACProblemT::constraint_type GeometricConstraintT;	///< Type of the geometric constraint
		typedef typename RANSACProblemT::loss_type LossMapT;	///< Type of the loss function

		/** @name Implementation details */ //@{
		static void ValidateParameters(const ProblemT& problem, MultimodelGeometryEstimationPipelineParametersC& parameters);	///< Validates the parameters

		static tuple<CorrespondenceContainerT, vector<size_t> > DecimateCorrespondences(const CorrespondenceContainerT&  correspondences, const MultimodelGeometryEstimationPipelineParametersC& parameters, unsigned int sGenerator);	///< Decimates the correspondence set
		static CorrespondenceContainerT MakeValidationSet(const CorrespondenceContainerT& observations, const MultimodelGeometryEstimationPipelineParametersC& parameters, unsigned int sGenerator);	///< Makes a validation set
		static vector<solution_type> PostprocesSolutions(const vector<solution_type>& src, const vector<unsigned int>& modelSupport, const vector<Coordinate1T>& coordinates1, const vector<Coordinate2T>& coordinates2, const GeometricConstraintT& constraint, AssignmentStrategyT strategy, unsigned int minSupport);	///< Performs postprocessing operations on the solution set
		//@}

	public:

		typedef RNGT rng_type;	///< Type of the random number generator
		typedef ModelT model_type;	///< Type of the estimated model

		static MultimodelGeometryEstimationPipelineDiagnosticsC Run(vector<solution_type>& solutions, ProblemT& problem, RNGT& rng, MultimodelGeometryEstimationPipelineParametersC parameters);	///< Solves a geometry estimation problem

		/** @name Utilities */ ///@{
		static tuple<double, unsigned int, vector<unsigned int> > EvaluateModels(const vector<ModelT>& models, const CorrespondenceContainerT& correspondences, const GeometricConstraintT& constraint, const LossMapT& lossMap, AssignmentStrategyT strategy);		///< Evaluates a model set over a correspondence set
		///@}

};	//class MultimodelGeometryEstimationPipelineC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @param[in] problem A tuple of problems for the algorithm
 * @param[in] parameters Parameters
 * @throw invalid_argument If any parameters have invalid values
 */
template<class GeometryProblemT>
void MultimodelGeometryEstimationPipelineC<GeometryProblemT>::ValidateParameters(const ProblemT& problem, MultimodelGeometryEstimationPipelineParametersC& parameters)
{
	if(!get<iRANSACProblem>(problem).IsValid())
		throw(invalid_argument(string("MultimodelGeometryEstimationPipeline::ValidateParameters: Invalid RANSAC problem")));

	if(!get<iGeometryProblem>(problem).IsValid())
		throw(invalid_argument(string("MultimodelGeometryEstimationPipeline::ValidateParameters: Invalid geometry estimation pipeline problem")));

	if(parameters.observationRatio<=1)
		throw(invalid_argument(string("MultimodelGeometryEstimationPipeline::ValidateParameters: parameters.observationRatio must be >=1. Value:") + lexical_cast<string>(parameters.observationRatio)) );

	if(parameters.validatorRatio<=1)
		throw(invalid_argument(string("MultimodelGeometryEstimationPipeline::ValidateParameters: parameters.validatorRatio must be >=1. Value:") + lexical_cast<string>(parameters.validatorRatio)) );

	if(parameters.binDensity1 <=0 )
		throw(invalid_argument(string("MultimodelGeometryEstimationPipeline::ValidateParameters: parameters.binDensity1 must be positive. Value:") + lexical_cast<string>(parameters.binDensity1)) );

	if(parameters.binDensity2 <=0 )
		throw(invalid_argument(string("MultimodelGeometryEstimationPipeline::ValidateParameters: parameters.binDensity2 must be positive. Value:") + lexical_cast<string>(parameters.binDensity2)) );

	parameters.matcherParameters.nThreads=parameters.nThreads;
	parameters.matcherParameters.flagStatic=false;	//The problem will be reused in guided matching

	parameters.ransacParameters.nThreads=parameters.nThreads;
	parameters.ransacParameters.minInlierRatio=parameters.minInlierRatio;
	parameters.ransacParameters.ransacParameters.flagHistory=false;

	parameters.geometryEstimatorParameters.nThreads=parameters.nThreads;
	parameters.geometryEstimatorParameters.flagVerbose=parameters.flagVerbose;
}	//void ValidateParameters(const ProblemT& problem, MultimodelGeometryEstimationPipelineParametersC& parameters)

/**
 * @brief Decimates the correspondence set
 * @param[in] correspondences Correspondence list
 * @param[in] parameters Parameters
 * @param[in] sGenerator Generator size
 * @return A tuple: [Decimated set; Indices of the survivors]
 */
template<class GeometryProblemT>
auto MultimodelGeometryEstimationPipelineC<GeometryProblemT>::DecimateCorrespondences(const CorrespondenceContainerT&  correspondences, const MultimodelGeometryEstimationPipelineParametersC& parameters, unsigned int sGenerator) -> tuple<CorrespondenceContainerT, vector<size_t> >
{

	//Compute the bin size
	typedef CorrespondenceDecimatorC<Coordinate1T, Coordinate2T> DecimatorT;
	typename DecimatorT::dimension_type1 binSize1;
	typename DecimatorT::dimension_type2 binSize2;
	tie(binSize1, binSize2)=DecimatorT::ComputeBinSize(correspondences, parameters.binDensity1, parameters.binDensity2);

	//Decimate
	CorrespondenceDecimatorParametersC decimatorParameters;
	decimatorParameters.flagSort=true;
	decimatorParameters.quantisationStrategy= ColsM<Coordinate1T>::value < ColsM<Coordinate2T>::value ? DecimatorQuantisationStrategyT::LEFT : DecimatorQuantisationStrategyT::RIGHT;
	decimatorParameters.minElements= floor(parameters.observationRatio * sGenerator);
	decimatorParameters.maxElements=correspondences.size();
	decimatorParameters.maxDensity=parameters.maxObservationDensity;

	return DecimatorT::Run(correspondences, binSize1, binSize2, decimatorParameters);
}	//auto DecimateCorrespondences(const CorrespondenceListT&  correspondences, const vector<Feature1T>& features1, const vector<Feature2T>& features2, const MultimodelGeometryEstimationPipelineParametersC& parameters) -> tuple<CorrespondenceContainerT, vector<size_t> >

/**
 * @brief Makes a validation set
 * @param[in] observations Observations
 * @param[in] parameters Parameters
 * @param[in] sGenerator Generator size
 * @return Validation set
 */
template<class GeometryProblemT>
auto MultimodelGeometryEstimationPipelineC<GeometryProblemT>::MakeValidationSet(const CorrespondenceContainerT& observations, const MultimodelGeometryEstimationPipelineParametersC& parameters, unsigned int sGenerator) -> CorrespondenceContainerT
{
	size_t sObservation=observations.size();
	size_t sValidation=min((size_t)(sGenerator*parameters.validatorRatio), sObservation);
	double step=(double)(sObservation)/sValidation;

	vector<size_t> indexList; indexList.reserve(sValidation);
	for(double c=0; c<sObservation; c+=step)
		indexList.push_back(floor(c));

	return FilterBimap(observations, indexList);
}	//CorrespondenceContainerT MakeValidationSet(const CorrespondenceContainerT& observations, const MultimodelGeometryEstimationPipelineParametersC& parameters)

/**
 * @brief Evaluates a model set over a correspondence set
 * @param[in] models Models
 * @param[in] correspondences Correspondences
 * @param[in] constraint Geometric constraint to compute the loss and identify the inliers
 * @param[in] lossMap Loss map
 * @param[in] strategy Assignment strategy
 * @return A tuple: Total loss, number of unique inliers, support for each model
 * @remarks The error for a correspondence is the minimum error over all models, regardless of whether it is assigned to a model or not
 */
template<class GeometryProblemT>
tuple<double, unsigned int, vector<unsigned int> > MultimodelGeometryEstimationPipelineC<GeometryProblemT>::EvaluateModels(const vector<ModelT>& models, const CorrespondenceContainerT& correspondences, const GeometricConstraintT& constraint, const LossMapT& lossMap, AssignmentStrategyT strategy)
{
	size_t nModels=models.size();
	double totalError=0;
	unsigned int uniqueInliers=0;
	vector<unsigned int> support(nModels,0);
	for(const auto& current : correspondences)
	{
		int minIndex=-1;	//Unassigned
		double minError=numeric_limits<double>::infinity();
		unsigned int hitCount=0;
		bool flagAssigned=false;
		for(size_t c=0; c<nModels; ++c)
		{
			bool flagInlier;
			double error;
			tie(flagInlier, error)=constraint.Enforce(models[c], current.left, current.right);

			if(flagInlier)
			{
				++hitCount;

				if(strategy==AssignmentStrategyT::SOFT)
				{
					++support[c];
					flagAssigned=true;
				}	//if(strategy==AssignmentStrategyT::SOFT)
			}	//if(flagInlier)

			if(error<minError)
			{
				minError=error;
				minIndex=c;
			}
		}	//for(const auto& current : models)

		//SOFT and HARD allows for multiple hits. UNIQUE disallows a correspondence being inlier to multiple models
		if((strategy==AssignmentStrategyT::UNIQUE && hitCount==1) || (strategy==AssignmentStrategyT::HARD && hitCount>0) )
		{
			++support[minIndex];
			flagAssigned=true;
		}

		if(flagAssigned)
			++uniqueInliers;

		totalError+=lossMap(minError);
	}	//for(const auto& current : correspondences)

	return make_tuple(totalError, uniqueInliers, support);
}	//tuple<double, vector<set<unsigned int>> > EvaluateModels(const vector<ModelT>& models, const CorrespondenceContainerT& correspondences, AssignmentStrategyT strategy, double inlierT)

/**
 * @brief Performs postprocessing operations on the solution set
 * @param[in] src Original solution set
 * @param[in] modelSupport Support for each model
 * @param[in] coordinates1 Coordinates for the first set
 * @param[in] coordinates2 Coordinates for the second set
 * @param[in] constraint Geometry constraint
 * @param[in] strategy Assignment strategy
 * @param[in] minSupport Minimum support
 * @return Postprocessed solutions
 * @remarks Postprocessing
 * 	- Eliminate the solutions with a weak support in the reference set
 * 	- Enforce the assignment strategy
 */
template<class GeometryProblemT>
auto MultimodelGeometryEstimationPipelineC<GeometryProblemT>::PostprocesSolutions(const vector<solution_type>& src, const vector<unsigned int>& modelSupport, const vector<Coordinate1T>& coordinates1, const vector<Coordinate2T>& coordinates2, const GeometricConstraintT& constraint, AssignmentStrategyT strategy, unsigned int minSupport) -> vector<solution_type>
{
	//Eliminate the weak models
	size_t sSrc=src.size();
	vector<solution_type> output; output.reserve(sSrc);
	for(size_t c=0; c<sSrc; ++c)
		if(modelSupport[c]>=minSupport)
			output.push_back(src[c]);

	output.shrink_to_fit();

	//Reassignment of correspondences

	//If multimodel assignments are allowed, nothing to do here
	if(strategy==AssignmentStrategyT::SOFT)
		return output;

	//Reorganise the correspondence sets
	typedef map<unsigned int, typename CorrespondenceListT::value_type> ValueT;	//Keyed wrt model index
	map<pair<unsigned int, unsigned int>, ValueT>  correspondenceMap;	//Each entry is a map holding the instances of a correspondence across multiple entries

	size_t nSolutions=output.size();
	for(size_t c=0; c<nSolutions; ++c)
	{
		for(const auto& current : get<iInlierList>(output[c]))
			correspondenceMap[make_pair(current.left, current.right)].emplace(c, current);

		//Clear the list- to be repopulated
		get<iInlierList>(output[c]).clear();
	}	//for(size_t c=0; c<nSolutions; ++c)

	//Reassign
	for(auto& currentList : correspondenceMap)
	{
		int index=-1;	//Each correspondence is an inlier to at least one model
		double minError=numeric_limits<double>::infinity();
		unsigned int hitCount=0;
		for(const auto& current : currentList.second)
		{
			bool flagInlier;
			double error;
			tie(flagInlier, error)=constraint.Enforce(get<iModel>(output[current.first]), coordinates1[current.second.left], coordinates2[current.second.right]);

			if(error<minError)
			{
				minError=error;
				index=current.first;
			}

			if(flagInlier)
				++hitCount;
		}	//for(const auto& currentList : current)

		//UNIQUE rejects correspondences that are inliers to multiple models
		if((strategy==AssignmentStrategyT::UNIQUE && hitCount==1) || (strategy==AssignmentStrategyT::HARD && hitCount>0) )
			get<iInlierList>(output[index]).push_back(currentList.second[index]);
	}	//for(const auto& current : correspondenceMap)

	return output;
}	//vector<solution_type> PostprocesSolutions(const vector<solution_type>& src, const vector<unsigned int>& modelSupport, const vector<Coordinate1T>& coordinates1, const vector<Coordinate2T>& coordinates2, const GeometricConstraintT& constraint, AssignmentStrategyT strategy)

/**
 * @brief Solves a geometry estimation problem
 * @param[out] solutions Solution set
 * @param[in, out] problem RANSAC and geometry estimation pipeline problems
 * @param[in, out] rng Random number generator
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 */
template<class GeometryProblemT>
MultimodelGeometryEstimationPipelineDiagnosticsC MultimodelGeometryEstimationPipelineC<GeometryProblemT>::Run(vector<solution_type>& solutions, ProblemT& problem, RNGT& rng, MultimodelGeometryEstimationPipelineParametersC parameters)
{
	//Validate and initialise the parameters
	ValidateParameters(problem, parameters);

	MultimodelGeometryEstimationPipelineDiagnosticsC diagnostics;	//Output

	//Find the feature correspondences

	const vector<Feature1T>& features1=get<iGeometryProblem>(problem).FeatureSet1();
	const vector<Feature1T>& features2=get<iGeometryProblem>(problem).FeatureSet2();

	if(features1.empty() || features2.empty() )
		return diagnostics;

	CorrespondenceListT correspondences;
	typedef typename MatchingProblemT::constraint_type MatchingConstraintT;
	optional<MatchingProblemT> matchingProblem;
	diagnostics.matcherDiagnostics=FeatureMatcherC<MatchingProblemT>::Run(correspondences, matchingProblem, parameters.matcherParameters, get<iGeometryProblem>(problem).SimilarityMetric(), optional<MatchingConstraintT>(), features1, features2);

	if(!diagnostics.matcherDiagnostics.flagSuccess)
		return diagnostics;

	//Index->Coordinate
	vector<Coordinate1T> coordinates1=MakeCoordinateVector(features1);
	vector<Coordinate2T> coordinates2=MakeCoordinateVector(features2);
	CorrespondenceContainerT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CorrespondenceContainerT>(correspondences, coordinates1, coordinates2);

	//Decimation
	matchingProblem->AssessAmbiguity(correspondences, parameters.nThreads);

	unsigned int sGenerator=get<iGeometryProblem>(problem).GetMinimalGeneratorSize();
	CorrespondenceContainerT observations;
	vector<size_t> indexMap;
	std::tie(observations, indexMap)=DecimateCorrespondences(coordinateCorrespondences, parameters, sGenerator);

	//Validation set
	CorrespondenceContainerT validators=MakeValidationSet(observations, parameters, sGenerator);

	get<iRANSACProblem>(problem).SetObservations(observations);
	get<iRANSACProblem>(problem).SetValidators(validators);

	if(parameters.flagVerbose)
		cout<<"MGEP: #Correspondences: "<<correspondences.size()<<" #Observations: "<<observations.size()<<" #Validators: "<<validators.size()<<"\n";

	//Identify the models via sequential RANSAC
	typedef SequentialRANSACC<RANSACProblemT> SequentialRANSACT;
	typename SequentialRANSACT::result_type initialModels;	//Models estimated by sequential RANSAC
	diagnostics.ransacDiagnostics=SequentialRANSACT::Run(initialModels, get<iRANSACProblem>(problem), rng, parameters.ransacParameters);

	if(!diagnostics.ransacDiagnostics.flagSuccess)
		return diagnostics;

	//Refine the individual models via single-model guided matching
	//Parallelisation strategy: The number of models will usually be far fewer than that of the processors. So, sequential operation probably offers better utilisation
	size_t nModels=initialModels.size();
	solutions.reserve(nModels);
	vector<ModelT> models; models.reserve(nModels);
	diagnostics.geometryEstimatorDiagnostics.reserve(nModels);
	size_t index=1;
	for(const auto& current : initialModels)
	{
		if(parameters.flagVerbose)
			cout<<"MGEP Refining models: "<<index<<"/"<<nModels<<"\n";

		GeometryProblemT currentProblem=get<iGeometryProblem>(problem);	//Local copy of the problem
		currentProblem.UpdateConstraint(get<SequentialRANSACT::iModel>(current.second));

		optional<MatchingProblemT> currentMatchingProblem = matchingProblem;
		currentMatchingProblem->SetConstraint(*currentProblem.MatchingConstraint());

		ModelT currentModel;
		optional<MatrixXd> currentCovariance;
		CorrespondenceListT currentInliers;
		GeometryEstimationPipelineDiagnosticsC currentDiagnostics=GeometryEstimationPipelineT::Run(currentModel, currentCovariance, currentInliers, currentProblem, currentMatchingProblem, rng, parameters.geometryEstimatorParameters);

		if(!currentDiagnostics.flagSuccess)
			continue;

		solutions.emplace_back(currentModel, currentCovariance, currentInliers);
		models.push_back(currentModel);
		diagnostics.geometryEstimatorDiagnostics.push_back(currentDiagnostics);
		++index;

		if(parameters.flagVerbose)
			cout<<"\n";
	}	//for(const auto& current : initialModels)

	//Postprocessing
	vector<unsigned int> modelSupport;
	tie(diagnostics.error, diagnostics.support, modelSupport)=EvaluateModels(models, coordinateCorrespondences, get<iRANSACProblem>(problem).GetConstraint(), get<iRANSACProblem>(problem).GetLossMap(), parameters.assignmentType );

	unsigned int minSupport=parameters.ransacParameters.minInlierRatio * correspondences.size();
	solutions=PostprocesSolutions(solutions, modelSupport, coordinates1, coordinates2, get<iRANSACProblem>(problem).GetConstraint(), parameters.assignmentType, minSupport);

	//If any models are discarded, re-evaluate
	if(solutions.size()!=models.size())
	{
		vector<unsigned int> dummy;
		tie(diagnostics.error, diagnostics.support, dummy)=EvaluateModels(models, coordinateCorrespondences, get<iRANSACProblem>(problem).GetConstraint(), get<iRANSACProblem>(problem).GetLossMap(), parameters.assignmentType );
	}	//if(solutions.size()!=models.size())

	diagnostics.flagSuccess=true;
	solutions.shrink_to_fit();
	diagnostics.geometryEstimatorDiagnostics.shrink_to_fit();

	return diagnostics;
}	//GeometryEstimationPipelineDiagnosticsC Run(vector<ModelT>& model, vector<optional<MatrixXd> >& covariance, vector<CorrespondenceListT>& correspondences, ProblemT& problem, RNGT& rng, MultimodelGeometryEstimationPipelineParametersC parameters)

}	//GeometryN
}	//SeeSawN

#endif /* MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_IPP_9123092 */
