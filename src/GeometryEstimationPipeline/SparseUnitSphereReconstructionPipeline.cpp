/**
 * @file SparseUnitSphereReconstructionPipeline.cpp Implementation of the sparse unit sphere reconstruction pipeline
 * @author Evren Imre
 * @date 21 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SparseUnitSphereReconstructionPipeline.h"

namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Verifies the validity of the parameters
 * @param[in,out] parameters Parameters
 * @throws invalid_argument If any of the parameters is invalid
 */
void SparseUnitSphereReconstructionPipelineC::ValidateParameters(SparseUnitSphereReconstructionPipelineParametersC& parameters)
{
	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("SparseUnitSphereReconstructionPipelineC::ValidateParameters : noiseVariance must be positive. Value=")+lexical_cast<string>(parameters.noiseVariance)));

	if(parameters.pRejection<=0 || parameters.pRejection>=1)
		throw(invalid_argument(string("SparseUnitSphereReconstructionPipelineC::ValidateParameters : pRejection must be in (0,1). Value=")+lexical_cast<string>(parameters.pRejection)));

	//k-NN matching introduces links between clusters, which leads to failed consistency checks
	parameters.matcherParameters.matcherParameters.nnParameters.neighbourhoodSize12=1;
	parameters.matcherParameters.matcherParameters.nnParameters.neighbourhoodSize21=1;

	parameters.matcherParameters.flagConsistency=true;

	parameters.panoramaParameters.noiseVariance=parameters.noiseVariance;
	parameters.panoramaParameters.pReject=parameters.pRejection;
}	//void ValidateParameters(const Sparse3DReconstructionPipelineC& parameters)

/**
 * @brief Initialises the pairwise epipolar constraints
 * @param[in] cameras Cameras
 * @param[in] parameters Parameters
 * @return Epipolar constraints: [Pair id; constraint]
 */
auto SparseUnitSphereReconstructionPipelineC::MakePairwiseConstraints(const vector<CameraMatrixT>& cameras, const SparseUnitSphereReconstructionPipelineParametersC& parameters) -> map<SizePairT, SymmetricTransferErrorConstraint2DT >
{
	map<SizePairT, SymmetricTransferErrorConstraint2DT> output;

	//Compute the inlier threshold
	double inlierTh=SymmetricTransferErrorConstraint2DT::error_type::ComputeOutlierThreshold(parameters.pRejection, parameters.noiseVariance);

	//Decompose the cameras
	size_t nCameras=cameras.size();
	vector<IntrinsicCalibrationMatrixT> mKList(nCameras);
	vector<RotationMatrix3DT> mRList(nCameras);
	for(size_t c=0; c<nCameras; ++c)
		tie(mKList[c], ignore, mRList[c])=DecomposeCamera(cameras[c]);

	//Iterate over the pairs
	for(size_t c1=0; c1<nCameras; ++c1)
		for(size_t c2=c1+1; c2<nCameras; ++c2)
		{
			Homography2DT mH=CameraToHomography(mRList[c1], mKList[c1], mRList[c2], mKList[c2]);
			output[SizePairT(c1,c2)]=SymmetricTransferErrorConstraint2DT(SymmetricTransferErrorConstraint2DT::error_type(mH), inlierTh, 1);

		}	//for(size_t c2=c1+1; c2<nCameras; ++c2)

	return output;
}	//vector<optional<EpipolarSampsonConstraintT> > MakePairwiseConstraints(const vector<CameraMatrixT>& cameras, const Sparse3DReconstructionPipelineParametersC& parameters)

/**
 * @brief Attaches descriptors to the scene points
 * @param[in] reconstruction 3D reconstruction
 * @param[in] correspondences Image correspondences
 * @param[in] features Features
 * @return Scene features
 */
vector<OrientedBinarySceneFeatureC> SparseUnitSphereReconstructionPipelineC::MakeSceneFeature(const vector<Coordinate3DT>& reconstruction, const UnifiedViewT& correspondences, const vector<vector<BinaryImageFeatureC> >& features)
{
	size_t nPoints=reconstruction.size();
	vector<OrientedBinarySceneFeatureC> output; output.reserve(nPoints);

	size_t index=0;
	for(const auto& currentTrajectory : correspondences)
	{
		OrientedBinarySceneFeatureC::descriptor_type descriptor;
		OrientedBinarySceneFeatureC::roi_type roi;
		for(const auto& current : currentTrajectory)
		{
			descriptor[get<CorrespondenceFusionC::iSourceId>(current)]=features[get<CorrespondenceFusionC::iSourceId>(current)][get<CorrespondenceFusionC::iElementId>(current)];
			roi.insert(get<CorrespondenceFusionC::iSourceId>(current));
		}	//for(const auto& current : currentTrajectory)

		output.emplace_back(reconstruction[index], descriptor, roi);

		++index;
	}	//for(const auto& current : correspondences)

	return output;
}	//vector<SceneFeatureC> MakeSceneFeature(const vector<Coordinate3DT>& reconstruction, const PairwiseViewT& correspondences, const vector<vector<ImageFeatureC> >& features)

/**
 * @brief Conversion from binary scene features to oriented scene features
 * @param[in] sceneFeatures Scene features
 * @param[in] cameraList Camera list
 */
void SparseUnitSphereReconstructionPipelineC::MakeOriented(vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameraList)
{
	//Image coordinate to direction vector converters
	size_t nCameras=cameraList.size();
	vector<Matrix3d> normalisers; normalisers.reserve(nCameras);
	for(const auto& current : cameraList)
		normalisers.push_back(current.leftCols(3).inverse());

	//Scene features
	Coordinate3DT direction;
	Coordinate3DT spherical;
	for(auto& currentFeature : sceneFeatures)
		for(auto& current : currentFeature.Descriptor())
		{
			direction = normalisers[current.first] * current.second.Coordinate().homogeneous();
			current.second.Coordinate() = CartesianToSpherical(direction).tail(2);
		}	//for(auto& current : currentFeature.Descriptor())

}	//void MaketOriented(vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameraList)

/********** EXPLICIT INSTANTIATIONS **********/
template SparseUnitSphereReconstructionPipelineDiagnosticsC SparseUnitSphereReconstructionPipelineC::Run(vector<Coordinate3DT>& pointCloud,  vector<OrientedBinarySceneFeatureC>& sceneFeatures, const vector<CameraMatrixT>& cameras, const vector<vector<BinaryImageFeatureC> >& features, const InverseHammingBIDConverterT& similarityMetric, SparseUnitSphereReconstructionPipelineParametersC parameters);

}	//namespace GeometryN
}	//namespace SeeSawN

