/**
 * @file GeometryEstimatorProblemConcept.ipp Implementation of the concept checker for GeometryEstimatorProblemConceptC
 * @author Evren Imre
 * @date 24 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_ESTIMATOR_PROBLEM_CONCEPT_IPP_3489432
#define GEOMETRY_ESTIMATOR_PROBLEM_CONCEPT_IPP_3489432

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <cstddef>
#include <tuple>
#include "../RANSAC/TwoStageRANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblemConcept.h"
#include "../Elements/Correspondence.h"
#include "../Geometry/GeometrySolverConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::Assignable;
using boost::CopyConstructible;
using boost::optional;
using std::vector;
using std::size_t;
using std::tie;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemConceptC;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::GeometryN::GeometrySolverConceptC;

/**
 * @brief Concept checker for geometry estimation problems
 * @tparam TestT Class to be tested
 * @remarks Assumption: The problem does not assume that the constraint has an associated geometric model
 * @ingroup Concept
 */
template<class TestT>
class GeometryEstimatorProblemConceptC
{
	///@cond CONCEPT_CHECK

	BOOST_CONCEPT_ASSERT((Assignable<TestT>));
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<typename TestT::ransac_problem_type2>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename TestT::ransac_problem_type2::minimal_solver_type>));

	public:

		BOOST_CONCEPT_USAGE(GeometryEstimatorProblemConceptC)
		{
			TestT tested;

			bool flagValid=tested.IsValid(); (void)flagValid;

			typename TestT::ransac_problem_type1 problem1=tested.RANSACProblem1();
			typename TestT::ransac_problem_type2 problem2=tested.RANSACProblem2();

			typedef typename TestT::ransac_problem_type2::minimal_solver_type MainSolverT;
			typedef typename MainSolverT::model_type ModelT;

			typedef TwoStageRANSACC<typename TestT::ransac_problem_type1, typename TestT::ransac_problem_type2> TSRANSACT;
			vector<size_t> inlierObservations;
			vector<size_t> generator;
			unsigned int modelIndex;
			ModelT model;
			typename TSRANSACT::result_type ransacResult;
			tie(model, modelIndex, inlierObservations, generator)=tested.ProcessRANSACResult(ransacResult);

			typename TestT::optimisation_problem_type pdlProblem=tested.MakeOptimisationProblem(model, inlierObservations);

			double score=tested.EvaluateModel(model); (void)score;

			typedef CoordinateCorrespondenceListT<typename MainSolverT::coordinate_type1, typename MainSolverT::coordinate_type2> CorrespondenceContainerT;
			CorrespondenceContainerT observationSet; (void)observationSet;
			CorrespondenceContainerT validationSet; (void)validationSet;
			tested.Configure(observationSet, validationSet, optional<ModelT>());
		}	//BOOST_CONCEPT_USAGE(GeometryEstimatorProblemConceptC)

	///@endcond
};

}	//GeometryN
}	//SeeSawN

#endif /* GEOMETRY_ESTIMATOR_PROBLEM_CONCEPT_IPP_3489432 */
