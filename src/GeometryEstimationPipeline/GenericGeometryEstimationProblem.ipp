/**
 * @file GenericGeometryEstimationProblem.ipp Implementation of GenericGeometryEstimationProblemC
 * @author Evren Imre
 * @date 28 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GENERIC_GEOMETRY_ESTIMATION_PROBLEM_IPP_9034532
#define GENERIC_GEOMETRY_ESTIMATION_PROBLEM_IPP_9034532

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <tuple>
#include <vector>
#include <cstddef>
#include <iterator>
#include <set>
#include <list>
#include <type_traits>
#include "../Elements/Correspondence.h"
#include "../Metrics/GeometricConstraintConcept.h"
#include "../RANSAC/RANSACGeometryEstimationProblemConcept.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../Optimisation/PowellDogLegProblemConcept.h"
#include "../Wrappers/BoostBimap.h"
#include "../Geometry/GeometrySolverConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::MatrixXd;
using std::tuple;
using std::get;
using std::vector;
using std::size_t;
using std::advance;
using std::next;
using std::set_intersection;
using std::list;
using std::back_inserter;
using std::is_same;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::MetricsN::GeometricConstraintConceptC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemConceptC;
using SeeSawN::OptimisationN::PowellDogLegProblemConceptC;
using SeeSawN::GeometryN::GeometrySolverConceptC;

/**
 * @brief Geometry estimation problem for a generic model
 * @tparam RANSACProblem1T A RANSAC problem for the first stage of TwoStageRANSACC
 * @tparam RANSACProblem2T A RANSAC problem for the second stage of TwoStageRANSACC
 * @tparam NLSOptimisationProblemT An optimisation problem for PowellDogLegC
 * @pre \c RANSACProblem2T::minimal_solver_type::constraint_type is a model of GeometricConstraintConceptC
 * @pre \c RANSACProblem1T and \c RANSACProblem2T are models of RANSACProblemConceptC
 * @pre The solvers of \c RANSACProblem1T and \c RANSACProblem2T are models of GeometrySolverConceptC
 * @pre \c NLSOptimisationProblemT is a model of PowellDogLegProblemConceptC
 * @pre \c NLSOptimisationProblemT::observation_set_type is a bimap of coordinates (unenforced)
 * @remarks The similarity between the reference model and a solution candidate is measured as the cardinality of the intersection between the inlier validators
 * @remarks The second RANSAC problem is the main entity: it is the source for the error and the loss function, plus many of the types
 * @remarks The first RANSAC problem stores the observations and the validators
 * @ingroup Problem
 * @nosubgrouping
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
class GenericGeometryEstimationProblemC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<RANSACProblem1T>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename RANSACProblem1T::minimal_solver_type>));

	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<RANSACProblem2T>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename RANSACProblem2T::minimal_solver_type>));
	BOOST_CONCEPT_ASSERT((GeometricConstraintConceptC<typename RANSACProblem2T::minimal_solver_type::constraint_type>));

	BOOST_CONCEPT_ASSERT((PowellDogLegProblemConceptC<NLSOptimisationProblemT>));

	static_assert(is_same< typename NLSOptimisationProblemT::observation_set_type::left_key_type, typename RANSACProblem2T::minimal_solver_type::constraint_type::first_argument_type >::value, "GenericGeometryEstimationProblemC : Incompatible coordinate types");
	static_assert(is_same< typename NLSOptimisationProblemT::observation_set_type::right_key_type, typename RANSACProblem2T::minimal_solver_type::constraint_type::second_argument_type >::value, "GenericGeometryEstimationProblemC : Incompatible coordinate types");
	//@endcond

	private:

		typedef vector<size_t> IndexListT;	///< An index vector
		typedef typename RANSACProblem2T::minimal_solver_type MainSolverT;	///< Type of the main solver
		typedef typename MainSolverT::model_type ModelT;	///< Type of the estimated model

		/** @name Configuration */ //@{
		typedef typename MainSolverT::coordinate_type1 CoordinateLT;	///< Type of the first coordinate in a correspondence
		typedef typename MainSolverT::coordinate_type2 CoordinateRT;	///< Type of the second coordinate in a correspondence
		typedef CoordinateCorrespondenceListT<CoordinateLT, CoordinateRT> CorrespondenceContainerT;	///< A container for holding correspondences

		RANSACProblem1T ransacProblem1;	///< RANSAC problem for the first stage
		RANSACProblem2T ransacProblem2;	///< RANSAC problem for the second stage
		NLSOptimisationProblemT optimisationProblem;	///< Optimisation problem

		optional<typename MainSolverT::model_type> referenceModel;	///< In case there are multiple models, the one closest to an external reference is chosen
		IndexListT referenceInliers;	///< Indices of the validators that are inliers to the reference model
		//@}

		/** @name State */ //@{
		bool flagValid;	///< \c true if the object is valid
		//@}

		typedef TwoStageRANSACC<RANSACProblem1T, RANSACProblem2T> TSRANSACT;	///< A two-stage RANSAC engine
		typedef typename TSRANSACT::result_type RANSACResultT;	///< Result of the RANSAC stage

		/** @name Implementation details */ //@{
		IndexListT MakeInlierList(const ModelT& model, const CorrespondenceContainerT& correspondences) const;	///< Identifies the indices of the inlier elements for a model
		//@}

	public:

		/** @name GeometryEstimationProblemConceptC interface */ //@{
		typedef RANSACProblem1T ransac_problem_type1;	///< Type of the first RANSAC problem
		typedef RANSACProblem2T ransac_problem_type2;	///< Type of the second RANSAC problem
		typedef NLSOptimisationProblemT optimisation_problem_type;	///< Type of the optimisation problem

		GenericGeometryEstimationProblemC();	///< Default constructor

		void Configure(const CorrespondenceContainerT& oobservationSet, const CorrespondenceContainerT& vvalidationSet, const optional<ModelT>& rreferenceModel);	///< Reconfigures the problem

		bool IsValid() const;	///< Returns \c flagValid

		RANSACProblem1T& RANSACProblem1();	///< Returns a reference to the RANSAC problem for the first RANSAC stage
		RANSACProblem2T& RANSACProblem2();	///< Returns a reference to the RANSAC problem for the second RANSAC stage
		tuple<ModelT, unsigned int, IndexListT, IndexListT> ProcessRANSACResult(const RANSACResultT& ransacResult) const;	///< Processes the RANSAC result and returns a model for refinement

		NLSOptimisationProblemT& MakeOptimisationProblem(const ModelT& model, const IndexListT& inliers);	///< Prepares an optimisation problem

		double EvaluateModel(const ModelT& model) const;	///< Computes the loss for a model
		//@}

		/** @name Constructors */ //@{
		GenericGeometryEstimationProblemC(const RANSACProblem1T& rransacProblem1, const RANSACProblem2T& rransacProblem2, const NLSOptimisationProblemT& ooptimisationProblem, const optional<ModelT>& rreferenceModel);	///< Constructor
		//@}
};	//class GenericGeometryEstimationProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::GenericGeometryEstimationProblemC() : flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] rransacProblem1 RANSAC problem for the first stage
 * @param[in] rransacProblem2 RANSAC problem for the second stage
 * @param[in] ooptimisationProblem Nonlinear least squares optimisation problem
 * @param[in] rreferenceModel If RANSAC returns multiple results, pick the one closest to this
 * @post Valid object
 * @remarks \c observationSet and \c validationSet are initialised from \c rransacProblem1
 * @remarks \c lossMap and \c constraint are initialised from \c rransacProblem2
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::GenericGeometryEstimationProblemC(const RANSACProblem1T& rransacProblem1, const RANSACProblem2T& rransacProblem2, const NLSOptimisationProblemT& ooptimisationProblem, const optional<ModelT>& rreferenceModel) : ransacProblem1(rransacProblem1), ransacProblem2(rransacProblem2), optimisationProblem(ooptimisationProblem)
{
	if(rreferenceModel)
	{
		referenceModel=*rreferenceModel;
		referenceInliers=MakeInlierList(*rreferenceModel, rransacProblem1.GetObservations());
	}	//if(rreferenceModel)

	flagValid=true;
}	//GenericGeometryEstimationProblemC(const RANSACProblem1T& rransacProblem1, const RANSACProblem2T& rransacProblem2, const NLSOptimisationProblemT& ooptimisationProblem, const GeometricConstraintT& cconstraint, const MapT eerrorToFitnessMap, const CorrespondenceRangeT& oobservationSet, const CorrespondenceRangeT& vvalidationSet, const optional<result_type>& rreferenceModel)

/**
 * @brief Reconfigures the problem
 * @param[in] oobservationSet New observation set
 * @param[in] vvalidationSet New validation set
 * @param[in] rreferenceModel New reference model. Invalid if not available.
 * @pre \c flagValid=true
 * @remarks Modifies the RANSAC problems and the optimisation problem as well
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
void GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::Configure(const CorrespondenceContainerT& oobservationSet, const CorrespondenceContainerT& vvalidationSet, const optional<ModelT>& rreferenceModel)
{
	//Preconditions
	assert(flagValid);

	if(rreferenceModel)
	{
		referenceModel=*rreferenceModel;
		referenceInliers=MakeInlierList(*rreferenceModel, oobservationSet);
	}	//if(rreferenceModel)

	//The observation and the validation sets for ransacProblem2 are assigned within TwoStageRANSACC
	ransacProblem1.SetObservations(oobservationSet);
	ransacProblem1.SetValidators(vvalidationSet);

//	optimisationProblem.Configure(ModelT(), oobservationSet, optional<MatrixXd>()); //A dummy update: a full update is within geometryEstimator. However, an uninitialised ModelT() upsets valgrind. So, commented out
																					//Another alternative would be invalidation. However, optimisationProblem could have some members that are set outside of the pipeline
}	//void Configure(const CorrespondenceContainerT& oobservationSet, const CorrespondenceContainerT& vvalidationSet, const optional<ModelT>& rreferenceModel)

/**
 * @brief Identifies the indices of the inlier elements for a model
 * @param[in] model Model
 * @param[in] correspondences Correspondence list
 * @return Indices of inlier correspondences
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
auto GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::MakeInlierList(const ModelT& model, const CorrespondenceContainerT& correspondences) const -> IndexListT
{
	IndexListT output; output.reserve(correspondences.size());
	size_t index=0;
	for(const auto& current : correspondences)
	{
		if(ransacProblem2.GetConstraint()(model, current.left, current.right))
			output.push_back(index);

		++index;
	}	//for(const auto& current : observationSet)

	output.shrink_to_fit();
	return output;
}	//IndexListT MakeInlierList(const typename RANSACProblem2T::model_type& model, const CorrespondenceContainerT& correspondences)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
bool GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief  Returns a reference to the RANSAC problem for the first RANSAC stage
 * @return A reference to \c ransacProblem1
 * @pre \c flagValid=true
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
RANSACProblem1T& GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::RANSACProblem1()
{
	//Precondition
	assert(flagValid);
	return ransacProblem1;
}	//const RANSACProblem1T& RANSACProblem1() const

/**
 * @brief  Returns a reference to the RANSAC problem for the second RANSAC stage
 * @return A reference to \c ransacProblem2
 * @pre \c flagValid=true
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
RANSACProblem2T& GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::RANSACProblem2()
{
	//Precondition
	assert(flagValid);
	return ransacProblem2;
}	//const RANSACProblem2T& RANSACProblem2() const

/**
 * @brief Processes the RANSAC result and chooses a model for refinement
 * @param[in] ransacResult Result of the second stage RANSAC
 * @return A tuple containing the chosen model, its index, the indices of the inlier observations, and that of the generators
 * @pre \c flagValid=true
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
auto GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::ProcessRANSACResult(const RANSACResultT& ransacResult) const -> tuple<ModelT, unsigned int, IndexListT, IndexListT>
{
	//Precondition
	assert(flagValid);

	//If there is a reference model
	typename RANSACResultT::const_iterator itBest=ransacResult.begin();	//By default, pick the model with the smallest error
	unsigned int index=1;
	unsigned int bestIndex=0;
	IndexListT bestInliers= MakeInlierList(get<TSRANSACT::iModel>(itBest->second), ransacProblem1.GetObservations());	//Inliers;
	if(referenceModel && ransacResult.size()>1)
	{
		//Criterion: Largest overlap in inlier validators
		size_t bestOverlap=0;
		auto itEnd=ransacResult.cend();
		for(auto it=next(ransacResult.cbegin(),1); it!=itEnd; advance(it,1))
		{
			IndexListT inliers=MakeInlierList(get<TSRANSACT::iModel>(it->second), ransacProblem1.GetObservations());

			list<size_t> cupAB;	///< Intersection set
			set_intersection(inliers.cbegin(), inliers.cend(), referenceInliers.cbegin(), referenceInliers.cend(), back_inserter(cupAB));
			size_t nOverlap=cupAB.size();

			if(nOverlap>bestOverlap)
			{
				itBest=it;
				bestIndex=index;
				bestOverlap=nOverlap;
				bestInliers=inliers;
			}

			++index;
		}	//for(auto it=next(ransacResult.cbegin(),1); it!=itEnd; advance(it,1))
	}	//if(referenceModel && ransacResult.size()>1)

	tuple<ModelT, unsigned int, IndexListT, IndexListT> output;

	get<0>(output)=get<TSRANSACT::iModel>(itBest->second);	//Model
	get<1>(output)=bestIndex;
	get<2>(output).swap(bestInliers);
	get<3>(output)=get<TSRANSACT::iGenerator>(itBest->second);	//Generator

	return output;
}	//tuple<result_type, IndexListT, IndexListT> ProcessRANSACResult(const RANSACResultT& ransacResult) const

/**
 * @brief Prepares an optimisation problem
 * @param[in] model Initial estimate
 * @param[in] inliers Indices of the observations to optimise over
 * @return A Powell's Dog Leg optimisation problem
 * @pre \c flagValid=true
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
auto GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::MakeOptimisationProblem(const ModelT& model, const IndexListT& inliers) -> NLSOptimisationProblemT&
{
	//Precondition
	assert(flagValid);

	CorrespondenceContainerT filtered=FilterBimap(ransacProblem1.GetObservations(), inliers);
	typename NLSOptimisationProblemT::observation_set_type nlsObservations;
	for(const auto& current : filtered)
		nlsObservations.push_back(typename NLSOptimisationProblemT::observation_set_type::value_type(current.left, current.right));

	optimisationProblem.Configure(model, nlsObservations, optional<MatrixXd>());

	return optimisationProblem;
}	//NLSOptimisationProblemT MakeOptimisationProblem(const result_type& model, const IndexListT& inliers) const

/**
 * @brief Computes the loss for a model
 * @param[in] model Model to be evaluated
 * @return Loss for the model
 * @pre \c flagValid=true
 */
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT>
double GenericGeometryEstimationProblemC<RANSACProblem1T, RANSACProblem2T, NLSOptimisationProblemT>::EvaluateModel(const ModelT& model) const
{
	//Precondition
	assert(flagValid);
	return MainSolverT::EvaluateModel(model, ransacProblem1.GetValidators(), ransacProblem2.GetConstraint().GetErrorMetric(), ransacProblem2.GetLossMap());
}	//double EvaluateModel(const result_type& model)

}	//GeometryN
}	//SeeSawN

#endif /* GENERIC_GEOMETRY_ESTIMATION_PROBLEM_IPP_9034532 */
