/**
 * @file NodalCameraTrackingPipeline.h Public interface for the nodal camera tracker
 * @author Evren Imre
 * @date 5 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef NODAL_CAMERA_TRACKING_PIPELINE_H_2209019
#define NODAL_CAMERA_TRACKING_PIPELINE_H_2209019

#include "NodalCameraTrackingPipeline.ipp"
namespace SeeSawN
{
namespace GeometryN
{

struct NodalCameraTrackingPipelineParametersC;	///< Parameters for the nodal camera tracking pipeline
struct NodalCameraTrackingPipelineDiagnosticsC;	///< Diagnostics for the nodal camera tracking pipeline
template <class NodalGeometryEstimatorProblemT, class NodalZoomGeometryEstimatorProblemT, class MatchingProblemT> class NodalCameraTrackingPipelineC;	///< Nodal camera tracking pipeline

}	//namespace GeometryN
}	//namespace SeeSawN




#endif /* NODAL_CAMERA_TRACKING_PIPELINE_H_2209019 */
