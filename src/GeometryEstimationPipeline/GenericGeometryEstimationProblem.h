/**
 * @file GenericGeometryEstimationProblem.h Public interface for GenericGeometryEstimationProblemC
 * @author Evren Imre
 * @date 28 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GENERIC_GEOMETRY_ESTIMATION_PROBLEM_H_0934212
#define GENERIC_GEOMETRY_ESTIMATION_PROBLEM_H_0934212

#include "GenericGeometryEstimationProblem.ipp"


namespace SeeSawN
{
namespace GeometryN
{
template<class RANSACProblem1T, class RANSACProblem2T, class NLSOptimisationProblemT> class GenericGeometryEstimationProblemC;	///< Geometry estimation problem for a generic model
}	//GeometryN
}	//SeeSawN

/*********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template class std::list<std::size_t>;
#endif /* GENERIC_GEOMETRY_ESTIMATION_PROBLEM_H_0934212 */
