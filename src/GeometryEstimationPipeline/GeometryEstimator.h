/**
 * @file GeometryEstimator.h Public interface for GeometryEstimatorC
 * @author Evren Imre
 * @date 24 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_ESTIMATOR_H_7980213
#define GEOMETRY_ESTIMATOR_H_7980213

#include "GeometryEstimator.ipp"

namespace SeeSawN
{
namespace GeometryN
{

struct GeometryEstimatorParametersC;	///< Parameters for GeometryEstimatorC
struct GeometryEstimatorDiagnosticsC;	///< Diagnostics for GeometryEstimatorC

template<class ProblemT> class GeometryEstimatorC;	///< Estimation of the geometry relating a set of correspondences

}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
#endif /* GEOMETRY_ESTIMATOR_H_7980213 */
