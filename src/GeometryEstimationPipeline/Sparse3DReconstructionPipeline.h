/**
 * @file Sparse3DReconstructionPipeline.h Public interface for the sparse 3D reconstruction pipeline
 * @author Evren Imre
 * @date 16 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SPARSE_3D_RECONSTRUCTION_PIPELINE_H_8037122
#define SPARSE_3D_RECONSTRUCTION_PIPELINE_H_8037122

#include "Sparse3DReconstructionPipeline.ipp"
#include "../Metrics/Similarity.h"

namespace SeeSawN
{
namespace GeometryN
{

struct Sparse3DReconstructionPipelineParametersC;	///< Parameters for \c Sparse3DReconstructionPipelineC
struct Sparse3DReconstructionPipelineDiagnosticsC;	///< Diagnostics for \c Sparse3DReconstructionPipelineC
class Sparse3DReconstructionPipelineC;	///< Pipeline for sparse 3D reconstruction from images taken by calibrated cameras

/********** EXTERN TEMPLATES *********/
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
extern template Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>&, vector<MultiviewTriangulationC::covariance_matrix_type>&, vector<SceneFeatureC>&, const vector<CameraMatrixT>&, const vector<vector<ImageFeatureC> >&, const InverseEuclideanIDConverterT&, Sparse3DReconstructionPipelineParametersC);

using SeeSawN::MetricsN::InverseHammingBIDConverterT;
extern template Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>&, vector<MultiviewTriangulationC::covariance_matrix_type>&, vector<OrientedBinarySceneFeatureC>&, const vector<CameraMatrixT>&, const vector<vector<BinaryImageFeatureC> >&, const InverseHammingBIDConverterT&, Sparse3DReconstructionPipelineParametersC);

}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::tuple<std::size_t, std::size_t>;
extern template double boost::lexical_cast<double, std::string>(const std::string&);

#endif /* SPARSE3DRECONSTRUCTIONPIPELINE_H_ */
