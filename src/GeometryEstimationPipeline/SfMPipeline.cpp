/**
 * @file SfMPipeline.cpp Implementation of the SfM pipeline
 * @author Evren Imre
 * @date 31 Oct 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SfMPipeline.h"

namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the algorithm input
 * @param[in] featureStack Features
 * @param[in] cameraList Camera list
 * @param[in, out] parameters Parameters
 * @throws invalid_argument If any input parameters are valid
 */
void SfMPipelineC::ValidateInput(const vector<ImageFeatureListT>& featureStack, const vector<CameraC>& cameraList, SfMPipelineParametersC& parameters)
{
	size_t nCameras=featureStack.size();

	//cameraList
	if(cameraList.size()!=nCameras)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : cameraList must have an element for each item in featureStack. Value=") + lexical_cast<string>(nCameras) ));

	if(any_of(cameraList.begin(), cameraList.end(), [](const CameraC& current){return !current.FrameBox();} ))
		throw(invalid_argument("SfMPipelineC::ValidateInput : Each element of cameraList must have a valid FrameBox"));

	//General
	if(parameters.imageNoiseVariance<=0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : imageNoiseVariance must be positive. Value=") + lexical_cast<string>(parameters.imageNoiseVariance)));

	if(parameters.maxMedianDepth<=0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : maxMedianDepth must be positive. Value=") + lexical_cast<string>(parameters.maxMedianDepth)));

	//Pose graph optimisation

	if(parameters.maxOrientationError<=0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : maxOrientationError must be positive. Value=") + lexical_cast<string>(parameters.maxOrientationError)));

	if(parameters.positionPrecision<=0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : positionPrecision must be positive. Value=") + lexical_cast<string>(parameters.positionPrecision)));

	if(parameters.poseGraphInlierThreshold<=0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : poseGraphInlierThreshold must be positive. Value=") + lexical_cast<string>(parameters.poseGraphInlierThreshold)));

	if(parameters.edgeTraversalPenalty<0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : edgeTraversalPenalty must be non-negative. Value=") + lexical_cast<string>(parameters.edgeTraversalPenalty)));

	//Robust refinement
	if(parameters.referenceTransferError<0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : referenceTransferError must be non-negative. Value=") + lexical_cast<string>(parameters.referenceTransferError)));

	if(parameters.inlierRejectionProbability<=0 || parameters.inlierRejectionProbability>=1)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : inlierRejectionProbability must be in (0,1). Value=") + lexical_cast<string>(parameters.inlierRejectionProbability)));

	if(parameters.loGeneratorRatio1<1)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : loGeneratorRatio1 must be >=1. Value=") + lexical_cast<string>(parameters.loGeneratorRatio1)));

	if(parameters.loGeneratorRatio2<1)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : loGeneratorRatio2 must be >=1. Value=") + lexical_cast<string>(parameters.loGeneratorRatio2)));

	if(parameters.predictionNoise<0)
		throw(invalid_argument(string("SfMPipelineC::ValidateInput : PredictionNoise must be non-negative. Value=") + lexical_cast<string>(parameters.predictionNoise)));

	parameters.vgParameters.nThreads=parameters.nThreads;
	parameters.vgParameters.flagConnected=true;
	parameters.vgParameters.geometryEstimationPipelineParameters.flagCovariance=false;
	parameters.vgParameters.geometryEstimationPipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12=1;
	parameters.vgParameters.geometryEstimationPipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21=1;

	parameters.sutTriangulationParameters.nThreads=1;

	parameters.pgParameters.nThreads=parameters.nThreads;
	parameters.pgParameters.flagConnected=true;
	parameters.pgParameters.geometryEstimationPipelineParameters.flagCovariance=false;

	parameters.intrinsicCalibrationParameters.ransacParameters.nThreads=parameters.nThreads;
	parameters.intrinsicCalibrationParameters.ransacParameters.flagHistory=false;

	//This section is initialised because triangulationParameters are used for PerformMultiviewTriangulation as well
	parameters.sceneReconstructionParameters.triangulationParameters.sutParameters.nThreads=1;
	parameters.sceneReconstructionParameters.triangulationParameters.flagFastTriangulation=true;
	parameters.sceneReconstructionParameters.triangulationParameters.noiseVariance=parameters.imageNoiseVariance;	//Updated within the algorithm
	parameters.sceneReconstructionParameters.triangulationParameters.inlierTh= P3PSolverC::error_type::ComputeOutlierThreshold(parameters.inlierRejectionProbability, parameters.imageNoiseVariance);	//Updated within the algorithm
	parameters.sceneReconstructionParameters.triangulationParameters.flagCheirality=true;

	parameters.sceneReconstructionParameters.flagCompact=false;
	parameters.sceneReconstructionParameters.flagVerbose=false;
	parameters.sceneReconstructionParameters.noiseVariance=parameters.imageNoiseVariance;
	parameters.sceneReconstructionParameters.matcherParameters.nThreads=parameters.nThreads;

	parameters.registration32Parameters.flagCovariance=false;
	parameters.registration32Parameters.flagVerbose=false;
	parameters.registration32Parameters.nThreads=1;
	parameters.registration32Parameters.geometryEstimatorParameters.ransacParameters.parameters1.flagHistory=false;
	parameters.registration32Parameters.geometryEstimatorParameters.ransacParameters.parameters2.flagHistory=false;
	parameters.registration32Parameters.geometryEstimatorParameters.pdlParameters.flagCovariance=false;
}	//void ValidateInput(const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList,  SfMPipelineParametersC& parameters)

/**
 * @brief Processes the cameras
 * @param[in] cameraList Cameras
 * @param[in] parameters Parameters
 * @return A tuple: camera matrices; intrinsic calibration matrices; indicator array for focal lengths
 */
tuple<map<size_t, CameraMatrixT>, vector<IntrinsicCalibrationMatrixT>, vector<bool>> SfMPipelineC::ProcessCameras(const vector<CameraC>& cameraList, const SfMPipelineParametersC& parameters)
{
	size_t nCameras=cameraList.size();

	vector<IntrinsicCalibrationMatrixT> mKList(nCameras);
	vector<bool> flagFocals(nCameras);
	map<size_t, CameraMatrixT> mPList;

	for(size_t c=0; c<nCameras; ++c)
	{
		bool flagIntrinsics=!!cameraList[c].Intrinsics();
		flagFocals[c]= ( flagIntrinsics && cameraList[c].Intrinsics()->focalLength );

		//Pull out the known intrinsic calibration parameters
		double a = flagIntrinsics ? cameraList[c].Intrinsics()->aspectRatio : 1;
		double s = flagIntrinsics ? cameraList[c].Intrinsics()->skewness : 1;
		Coordinate2DT p = flagIntrinsics && cameraList[c].Intrinsics()->principalPoint ? *cameraList[c].Intrinsics()->principalPoint : cameraList[c].FrameBox()->center();
		double f = (flagIntrinsics && cameraList[c].Intrinsics()->focalLength) ? *cameraList[c].Intrinsics()->focalLength :  max( (cameraList[c].FrameBox()->sizes())[0], (cameraList[c].FrameBox()->sizes())[1]);

		mKList[c]<< f, s, p[0], 0, a*f, p[1], 0, 0, 1;

		optional<CameraMatrixT> mP=cameraList[c].MakeCameraMatrix();
		if(mP)
			mPList[c]=*mP;
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(move(mPList), move(mKList), move(flagFocals));
}	//tuple<map<size_t, CameraMatrixT>, vector<IntrinsicCalibrationMatrixT>, vector<bool>> ProcessCameras(const vector<CameraC>& cameraList, const SfMPipelineParametersC& parameters)

/**
 * @brief Performs correspondence fusion for a set of pairwise correspondences
 * @param[in] correspondenceStack Correspondence stack
 * @return A tuple: feature track; pairwise correspondences
 */
auto SfMPipelineC::PerformCorrespondenceFusion(const CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT>& correspondenceStack) -> tuple<FeatureTrackStackT, PairwiseViewT>
{
	PairwiseViewT observed;
	PairwiseViewT inferred;
	FeatureTrackStackT tracks;
	tie(tracks, observed, inferred)=CorrespondenceFusionC::Run(correspondenceStack, 2, true);
	PairwiseViewT spliced=CorrespondenceFusionC::SplicePairwiseViews(observed, inferred);

	return make_tuple(move(tracks), move(spliced));
}	//tuple<FeatureTrackT, PairwiseViewT> PerformCorrespondenceFusion(const CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT>&& correspondenceStack)

/**
 * @brief Applies the cheirality and degeneracy tests to a point cloud
 * @param[in] pointCloud Point cloud
 * @param[in] mP1 First camera
 * @param[in] mP2 Second camera
 * @param[in] flagMetric \c true if the camera matrices are metric
 * @param[in] parameters Parameters
 * @return Indices of successful points. Empty, if the degeneracy test fails
 * @remarks If the median depth for the point cloud is significantly larger than the baseline between the cameras, this is likely to be a degenerate edge
 */
vector<size_t> SfMPipelineC::TestCheiralityAndDegeneracy(const vector<Coordinate3DT>& pointCloud, const CameraMatrixT& mP1, const CameraMatrixT& mP2, bool flagMetric, const SfMPipelineParametersC& parameters)
{
	if(pointCloud.empty())
		return vector<size_t>();

	CheiralityConstraintC cheirality1(mP1);
	CheiralityConstraintC cheirality2(mP2);

	size_t nPoints=pointCloud.size();
	vector<double> depthAccumulator; depthAccumulator.reserve(2*nPoints);
	vector<size_t> filtered; filtered.reserve(nPoints);

	for(size_t c=0; c<nPoints; ++c)
	{
		double dist1 = flagMetric ? cheirality1.Distance(pointCloud[c]) : cheirality1.AbsoluteDistance(pointCloud[c]);
		double dist2 = flagMetric ? cheirality2.Distance(pointCloud[c]) : cheirality2.AbsoluteDistance(pointCloud[c]);

		if(dist1<=0 || dist2<=0)	//If not metric, this is guaranteed to fail
			continue;

		depthAccumulator.push_back(dist1);
		depthAccumulator.push_back(dist2);
		filtered.push_back(c);
	}	//for(size_t c=0; c<nPoints; ++c)

	if(depthAccumulator.empty() )
		return vector<size_t>();

	auto itMedian=depthAccumulator.begin()+depthAccumulator.size()/2;
	nth_element(depthAccumulator, itMedian);
	if(*itMedian > parameters.maxMedianDepth)
		filtered.clear();

	return filtered;
}	//vector<size_t> TestCheiralityAndDegeneracy(const vector<Coordinate3DT>& pointCloud, const CameraMatrixT& mP1, const CameraMatrixT& mP2, const SfMPipelineParametersC& parameters)

/**
 * @brief Attaches appearance descriptors to a scene point
 * @param[in] coordinate Coordinate
 * @param[in] featureTrack Image feature track
 * @param[in] featureStack Image features
 * @return A scene feature
 */
SceneFeatureC SfMPipelineC::MakeSceneFeature(const Coordinate3DT& coordinate, const FeatureTrackT& featureTrack, const vector<ImageFeatureListT>& featureStack)
{
	SceneFeatureC output;
	output.Coordinate()=coordinate;

	for(const auto& current : featureTrack)
	{
		size_t iSource;
		size_t iPoint;
		std::tie(iSource, iPoint)=current;

		output.RoI().insert(iSource);
		output.Descriptor().emplace(iSource, featureStack[iSource][iPoint]);
	}	//for(const auto& current : featureTrack)

	return output;
}	//SceneFeatureC MakeSceneFeature(const Coordinate3DT& coordinate, const FeatureTrackT& featureTrack, const vector<ImageFeatureListT>& featureStack)

/**
 * @brief Estimates the variance of the coordinate noise affecting a point cloud
 * @param[in] mP1 First camera
 * @param[in] mP2 Second camera
 * @param[in] correspondences Image correspondences
 * @param[in] imageNoiseVariance Image noise corrupting the feature coordinates
 * @param[in] parameters Parameters
 * @return Noise variance
 * @remarks The noise variance for each point is approximated as the squared radius of the sphere with the same volume as the covariance ellipsoid
 */
double SfMPipelineC::EstimateSceneNoise(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const CoordinateCorrespondenceList2DT& correspondences, double imageNoiseVariance, const SfMPipelineParametersC& parameters)
{
	typedef ScaledUnscentedTransformationC<SUTTriangulationProblemC> SUTEngineT;

	vector<double> acc; acc.reserve(correspondences.size());
	for(const auto& current : correspondences)
	{
		SUTTriangulationProblemC problem(current.left, current.right, imageNoiseVariance, mP1, mP2);
		SUTEngineT::result_type result=SUTEngineT::Run(problem, parameters.sutTriangulationParameters);

		//The volume bounded by the covariance matrix satisfies xC^-1 x^t < 1 (1-sigma). We are seeking the sphere approximating this relation
		//So, we need to work with the inverse of C. Instead of a potentially ill-conditioned inversion, we can directly invert the eigenvalues
		EigenSolver<SUTEngineT::result_type::value_type::covariance_type> evd(result->GetCovariance());

		double abc = sqrt( evd.eigenvalues().real().prod());	//product of evds = 1/ square of the product of the semiaxes
																//Eigenvalues of the inverse = inverse of the eigenvalues
		double rad= cbrt(abc);	//4pi/3 abc = 4pi/3 r^3
		acc.push_back(rad);
	}	//for(const auto& current : correspondences)

	size_t index=correspondences.size()/2;
	nth_element(acc, acc.begin()+index);

	return acc[index];	//No squaring: we are dealing with the covariance ellipsoid not the std ellipsoid
}	//double EstimateSceneNoise(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const CoordinateCorrespondenceList2DT& correpondences, const SfMPipelineParametersC& parameters)

/**
 * @brief Builds a scene model from each epipolar geometry estimate
 * @param[in] edgeList Edge list
 * @param[in] featureStack Image features
 * @param[in] imageNoiseVector Image noise corrupting the feature coordinates
 * @param[in] flagMetric \c true if the function produces metric scene models
 * @param[in] parameters Parameters
 * @return A tuple: Pairwise scene models, scene noise, index map between pairwise scenes and epipolar graph edges, camera pairs for each scene
 */
auto SfMPipelineC::FormPairwiseSceneModels(const EpipolarGraphEdgeListT& edgeList, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoiseVector, bool flagMetric, const SfMPipelineParametersC& parameters) -> tuple<vector<SceneFeatureListT>, vector<double>, SceneEdgeIndexMapT, map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT>>>
{
	//Feature tracks

	//Extract the correspondence lists
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT> egCorrespondenceStack;
	static constexpr unsigned int iCorrespondenceList=VisionGraphPipelineC<Fundamental7PipelineProblemT>::iCorrespondenceList;
	for_each(edgeList, [&](const typename  range_value<EpipolarGraphEdgeListT>::type& current){egCorrespondenceStack.emplace(PairIdT(current.first.first, current.first.second), get<iCorrespondenceList>(current.second));});

	//Corresponence fusion
	FeatureTrackStackT featureTracks;
	PairwiseViewT correspondenceStack;
	tie(featureTracks, correspondenceStack)=PerformCorrespondenceFusion(egCorrespondenceStack);

	//Extract the image coordinates
	size_t nCameras=featureStack.size();
	vector<vector<Coordinate2DT>> imageCoordinateStack; imageCoordinateStack.reserve(nCameras);
	for_each(featureStack, [&](const ImageFeatureListT& current){imageCoordinateStack.push_back(MakeCoordinateVector(current));} );

	//Scene features for each pair

	CameraMatrixT mP1=CameraMatrixT::Zero(); mP1(0,0)=1; mP1(1,1)=1; mP1(2,2)=1;
	TriangulatorC triangulator;
	constexpr unsigned int iModel=VisionGraphPipelineC<Fundamental7PipelineProblemT>::iModel;

	size_t nEdges=edgeList.size();
	vector< SceneFeatureListT > sceneFeatureStack; sceneFeatureStack.reserve(nEdges);
	vector<double> sceneNoise; sceneNoise.reserve(nEdges);

	map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> > cameraPairStack;

	SceneEdgeIndexMapT indexMap;
	size_t cScene=0;
	for(const auto& current : edgeList)
	{
		size_t i1=current.first.first;
		size_t i2=current.first.second;

		//Triangulation
		const CorrespondenceFusionC::correspondence_container_type& correspondenceList=correspondenceStack[PairIdT(i1,i2)];
		CorrespondenceListT tmpList;
		for(const auto& current : correspondenceList)
			tmpList.push_back(CorrespondenceListT::value_type(current.left, current.right, MatchStrengthC(1,0)));

		CoordinateCorrespondenceList2DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(tmpList, imageCoordinateStack[i1], imageCoordinateStack[i2]);
		CameraMatrixT mP2;
		if(!flagMetric)
			mP2=CameraFromFundamental( get<iModel>(current.second) );
		else
		{
			optional<CameraMatrixT> mPTemp=CameraFromEssential(get<iModel>(current.second), coordinateCorrespondences);

			if(!mPTemp)
				continue;

			mP2=*mPTemp;
		}	//if(!flagMetric)

		cameraPairStack.emplace(current.first, make_pair(mP1, mP2) );

		vector<Coordinate3DT> pointCloud=triangulator(mP1, mP2, coordinateCorrespondences, true);

		//Cheirality and median depth tests
		vector<size_t> filteredIndices=TestCheiralityAndDegeneracy(pointCloud, mP1, mP2, flagMetric, parameters);

		if(filteredIndices.empty())
			continue;

		//For points satisfying the cheirality constraints, build the corresponding scene features

		CorrespondenceFusionC::correspondence_container_type filteredCorrespondenceList=FilterBimap(correspondenceList, filteredIndices);

		size_t nFiltered=filteredIndices.size();
		SceneFeatureListT sceneFeatures(nFiltered);
		size_t cFeature=0;
		for(const auto& currentCorrespondence : filteredCorrespondenceList)
		{
			sceneFeatures[cFeature]=MakeSceneFeature(pointCloud[filteredIndices[cFeature]], featureTracks[currentCorrespondence.info], featureStack);

			++cFeature;
		}	//for(const auto& current : filteredCorrespondenceList)

		sceneFeatureStack.push_back(move(sceneFeatures));

		//Noise level estimation
		double effectiveImageNoise= 0.5*(imageNoiseVector[i1] + imageNoiseVector[i2]);
		sceneNoise.push_back(EstimateSceneNoise(mP1, mP2, coordinateCorrespondences, effectiveImageNoise, parameters));

		indexMap.push_back(SceneEdgeIndexMapT::value_type(cScene, current.first) );
		++cScene;
	}	//for(const auto& current : edgeList)

	return make_tuple(move(sceneFeatureStack), move(sceneNoise), move(indexMap), move(cameraPairStack));
}	//auto FormPairwiseSceneModels(const EpipolarGraphEdgeListT& edgeList) -> tuple<vector<SceneFeatureListT>, PairwiseViewT, SceneEdgeIndexMapT>

/**
 * @brief Makes a pose graph from an edge list
 * @param[in] pgEdgeList Pose graph edge list
 * @param[in] maxStrength Maximum edge strength, for strength-to-distance conversion
 * @param[in] parameters Parameters
 * @return Pose graph corresponding to the edge list
 */
auto SfMPipelineC::MakeGraph(const PoseGraphEdgeContainerT& pgEdgeList, double maxStrength, const SfMPipelineParametersC& parameters) -> PoseGraphT
{
	//Get the ordered unique indices. Ordered, so that the camera vertex indices follow those of the scenes
	set<size_t> indexSet;
	for(const auto& current : pgEdgeList)
	{
		indexSet.insert(current.first.first);
		indexSet.insert(current.first.second);
	}	//for(const auto& current : pgEdgeList)

	map<size_t, size_t> indexMap;	//Maps the vertex indentifiers in the edge list to vertex indices in the graph
	for_each(indexSet, [&](size_t current){ indexMap.emplace(current, indexMap.size());} );

	//Build the graph
	PoseGraphT graph(indexMap.size() );
	for(const auto& current : pgEdgeList)
	{
		size_t id1=indexMap[current.first.first];
		size_t id2=indexMap[current.first.second];
		add_edge(id1, id2, 1+(1+parameters.edgeTraversalPenalty)*maxStrength-get<PoseGraphPipelineC<Homography3DPipelineProblemT>::iCorrespondenceList>(current.second).size(), graph );	//So, the distances are strictly positive
																																								//Traversing an edge has a minimum penalty of 0.75*maxStrength+1. This encourages shorter paths
																																							//The camera-point cloud edges have the max distance, but this should not be a problem, as it would not alter the shortest paths
	}	//for(const auto& current : pgEdgeList)

	//Add the vertex properties
	for(const auto& current : indexMap)
		boost::put(vertex_name_t(), graph, vertex(current.second, graph), current.first);	//So the vertex knows its original identifier

	return graph;
}	//PoseGraphT MakeGraph(const PoseGraphEdgeContainerT& pgEdgeList, double maxStrength)

/**
 * @brief Augments a pose graph with camera vertices
 * @param[in] pgEdgeList Edge list for the pose graph
 * @param[in] egEdgeList Edge list for the epipolar graph
 * @param[in] sceneEdgeIndexMap Scene-edge index map
 * @param[in] cameraPairStack Camera pairs for each scene
 * @param[in] offset The offset used to obtain a vertex id from a camera id
 * @param[in] flagMetric \c true if a metric pose graph
 * @param[in] parameters Parameters
 * @return A tuple: augmented pose graph; augmented edges
 */
auto SfMPipelineC::AugmentPoseGraph(const PoseGraphEdgeContainerT& pgEdgeList, const EpipolarGraphEdgeListT& egEdgeList, const SceneEdgeIndexMapT& sceneEdgeIndexMap, const map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, size_t offset, bool flagMetric,  const SfMPipelineParametersC& parameters) -> tuple<PoseGraphT, PoseGraphEdgeContainerT>
{
	//Get the ids of the point clouds still in the graph
	unordered_set<size_t> pointCloudIds;
	double maxStrength=0;
	constexpr unsigned int iCorrespondence=PoseGraphEngineT::iCorrespondenceList;
	for(const auto& current : pgEdgeList)
	{
		pointCloudIds.insert(current.first.first);
		pointCloudIds.insert(current.first.second);

		if(get<iCorrespondence>(current.second).size()>maxStrength)
			maxStrength=get<iCorrespondence>(current.second).size();
	}	//for(const auto& current : pgEdgeList)

	//Augment the edge list with the camera edges
	PoseGraphEdgeContainerT augmentedEdgeList(pgEdgeList);
	for(auto current : pointCloudIds)
	{
		PairIdT cameraPair=sceneEdgeIndexMap.left.at(current).second;	//Fetch the camera ids

		augmentedEdgeList[make_pair(current, get<0>(cameraPair)+offset) ]=PoseGraphEngineT::edge_type(Homography3DT::Identity(), optional<MatrixXd>(), CorrespondenceListT());	//The reference frame of the first camera coincides with that of the scene
		augmentedEdgeList[make_pair(current, get<1>(cameraPair)+offset) ]=PoseGraphEngineT::edge_type(Homography3DT::Identity(), optional<MatrixXd>(), CorrespondenceListT());	//Both cameras are in the same reference frame
	}	//for(auto current : pointCloudIds)

	//Build the new graph
	PoseGraphT augmentedGraph=MakeGraph(augmentedEdgeList, maxStrength, parameters);

	return make_tuple(augmentedGraph, augmentedEdgeList);
}	//tuple<PoseGraphT, PoseGraphEdgeContainerT> AugmentPoseGraph(const PoseGraphT& poseGraph, const PoseGraphEdgeContainerT& pgEdgeList, const EpipolarGraphEdgeListT& egEdgeList)

/**
 * @brief Registers the camera nodes to the references frames represented by the scene nodes
 * @param[in] poseGraph Pose graph
 * @param[in] pgEdgeList Edge list for the pose graph
 * @param[in] egEdgeList Edge list for the epipolar graph
 * @param[in] sceneEdgeIndexMap Scene- epipolar edge index map
 * @param[in] cameraPairStack Camera pairs for the epipolar graph edges
 * @param[in] lastSceneVertexId Highest vertex id signifying a scene node (not vertex name!)
 * @param[in] offset Offset for converting an augmented graph vertex id to a camera id
 * @return A camera sequence for each scene node
 * @remarks Registration: for each scene node, find the shortest path to the all camera nodes. Compose the homographies across the path
 */
vector<map<size_t, CameraMatrixT> > SfMPipelineC::RegisterToScenes(const PoseGraphT& poseGraph, const PoseGraphEdgeContainerT& pgEdgeList, const EpipolarGraphEdgeListT& egEdgeList, const SceneEdgeIndexMapT& sceneEdgeIndexMap, const map< pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, size_t lastSceneVertexId, size_t offset)
{
	vector<map<size_t, CameraMatrixT> >  output; output.reserve(pgEdgeList.size());

	//Augment the edge list with the reverse edges
	PoseGraphEdgeContainerT augmentedEdgeList(pgEdgeList);
	for(const auto& current : pgEdgeList)
	{
		typename PoseGraphEngineT::edge_type invertedEdge=current.second;
		get<PoseGraphEngineT::iModel>(invertedEdge)=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(get<PoseGraphEngineT::iModel>(current.second));	//mH is not guaranteed to be invertible for the edges generated by CameraFromFundamental
		augmentedEdgeList.emplace(make_pair(current.first.second, current.first.first),  invertedEdge);
	}	//for(const auto& current : pgEdgeList)


	//Find the shortest paths for each camera to the reference frame of the current scene node
	size_t nVertex=num_vertices(poseGraph);
	graph_traits<PoseGraphT>::vertex_iterator itV;
	graph_traits<PoseGraphT>::vertex_iterator itVE;
	tie(itV, itVE)=vertices(poseGraph);
	for(;itV!=itVE; advance(itV,1))
	{
		//If a camera vertex, skip
		if(*itV > lastSceneVertexId)
			continue;

		//Trace the shortest path to each node
		vector<unsigned int> predMap(nVertex);
		vector<double> distMap(nVertex);
		dijkstra_shortest_paths(poseGraph, *itV, predecessor_map(make_iterator_property_map(predMap.begin(), get(boost::vertex_index, poseGraph))).distance_map(make_iterator_property_map(distMap.begin(), get(boost::vertex_index, poseGraph))));

		//Only for the camera nodes, compose the homographies
		map<size_t, CameraMatrixT> cameraSequence;
		for(size_t c=lastSceneVertexId+1; c<nVertex; ++c)
		{
			//Backtrace
			unsigned int currentId=c;
			list<size_t> track;
			do
			{
				track.push_front(currentId);
				currentId=predMap[currentId];
			}while(currentId != *track.begin());

			//Recover the original ids from the vertex ids
			for(auto& current : track)
				current=get(vertex_name_t(), poseGraph, current);

			//Compose the homography that takes the origin node to the camera node (and not the other way!)
			auto itCurrent=track.begin();
			auto itNext=next(itCurrent,1);
			auto itE=track.end();
			Homography3DT mH=Homography3DT::Identity();
			do
			{
				mH=get<PoseGraphEngineT::iModel>(augmentedEdgeList[PoseGraphEdgeContainerT::key_type(*itCurrent, *itNext)])*mH;
				advance(itCurrent,1);
				advance(itNext,1);
			}while(itNext!=itE);
			//Get back to the corresponding edge in the epipolar graph

			//Get the scene and camera indices for the epipolar graph
			unsigned int sceneId=get(vertex_name_t(), poseGraph, predMap[c]);
			unsigned int cameraId=get(vertex_name_t(), poseGraph, c) - offset;

			auto temp=sceneEdgeIndexMap.left.at(sceneId).second;
			typename EpipolarGraphEdgeListT::key_type epipolarEdgeId= make_pair(get<0>(temp), get<1>(temp));
			CameraMatrixT mP;
			if(cameraId==epipolarEdgeId.first)
				mP=cameraPairStack.at(epipolarEdgeId).first;
			else
				mP=cameraPairStack.at(epipolarEdgeId).second;

			//Transfer
			cameraSequence.emplace(cameraId, mP*mH);	//mH.inverse() takes the camera reference frame to the origin node. So, the camera is multiplied with mH
		}	//for(size_t c=lastSceneVertexId+1; c<nVertex; ++c)

		output.push_back(cameraSequence);
	}	//for(itV; itV!=itVE; advance(itV,1))

	return output;
}	//vector<map<size_t, CameraMatrixT> > FormCameraChains(const PoseGraphT& poseGraph, const PoseGraphEdgeContainerT& pgEdgeList, const EpipolarGraphEdgeListT& egEdgeList, const SceneEdgeIndexMapT& sceneEdgeIndexMap, size_t lastSceneVertexId)

/**
 * @brief Estimates the intrinsic calibration parameters
 * @param[in, out] rng Random number generator
 * @param[in] mPList Camera matrices
 * @param[in] featureStack Image features
 * @param[in] mKList Intrinsic calibration estimates
 * @param[in] parameters Parameters
 * @return Intrinsic calibration parameters for the cameras for which the process was successfu
 */
map<size_t, IntrinsicCalibrationMatrixT> SfMPipelineC::EstimateIntrinsics(RNGT& rng, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack, const vector<IntrinsicCalibrationMatrixT>& mKList, const SfMPipelineParametersC& parameters )
{
	//Form the camera chains
	vector<map<size_t, CameraMatrixT> > cameraSequences;
	PoseGraphEdgeContainerT pgEdgeList;
	std::tie(cameraSequences, pgEdgeList, ignore, ignore)=FormCameraSequences<Fundamental7PipelineProblemT, Homography3DPipelineProblemT>(rng, mPList, featureStack, vector<double>(mKList.size(), parameters.imageNoiseVariance), false, parameters);

	if(cameraSequences.empty())
		return map<size_t, IntrinsicCalibrationMatrixT>();

	//Conversion to intrinsics
	size_t nCameras=mKList.size();
	vector<IntrinsicC> initialEstimates(nCameras);
	for(size_t c=0; c<nCameras; ++c)
	{
		initialEstimates[c].aspectRatio=mKList[c](1,1)/mKList[c](0,0);
		initialEstimates[c].focalLength=mKList[c](0,0);
		initialEstimates[c].skewness=mKList[c](0,1);
		initialEstimates[c].principalPoint=Coordinate2DT(mKList[c](0,2), mKList[c](1,2));
	}	//for(size_t c=0; c<nCameras; ++c)

	//Solve for the intrinsics
	//Run the linear autocalibration pipeline for each sequence, and return the best
	optional<pair<double, map<size_t, IntrinsicCalibrationMatrixT> > > best;
	for(const auto& current : cameraSequences)
	{
		//Initialisation
		vector<CameraMatrixT> cameraList(current.size());
		transform(current | map_values, cameraList.begin(), [](const CameraMatrixT& mP){return mP.normalized();});

		vector<IntrinsicC> currentInitialEstimates; currentInitialEstimates.reserve(cameraList.size());
		for_each(current | map_keys, [&](size_t index){currentInitialEstimates.push_back(initialEstimates[index]); });

		Homography3DT mHr;	//Rectifying homography
		map<size_t, IntrinsicCalibrationMatrixT> estimated;	//Rectified cameras
		LinearAutocalibrationPipelineDiagnosticsC diagnostics=LinearAutocalibrationPipelineC::Run(mHr, estimated, rng, cameraList, currentInitialEstimates, parameters.intrinsicCalibrationParameters);

		if(!diagnostics.flagSuccess)
			continue;

		if(!best || best->first > diagnostics.error)
			best = make_pair(diagnostics.error, estimated);
	}	//for(const auto& current : cameraSequences)

	if(!best)
		return map<size_t, IntrinsicCalibrationMatrixT>();

	//Uniform focal length constraint
	optional<double> uniformFocal;
	if(parameters.flagUniformFocalLength)
	{
		size_t nEstimated=best->second.size();
		vector<double> acc(nEstimated);
		transform(best->second | map_values, acc.begin(), [](const IntrinsicCalibrationMatrixT& mK){return mK(0,0);});
		nth_element(acc, acc.begin()+nEstimated/2);
		uniformFocal=acc[nEstimated/2];
	}	//if(parameters.flagUniformFocalLength)

	//Only retain the focal length, the rest are same as the initial estimate- i.e. 0 skewness, unity aspect ratio, principal point at the centre
	for(auto& current : best->second)
	{
		IntrinsicC constraint=initialEstimates[current.first];
		constraint.focalLength=uniformFocal;

		current.second = LinearAutocalibrationPipelineC::ImposeConstraints(current.second, constraint);
	}	//for(auto& current : best->second)

	return  best->second;
}	//map<size_t, IntrinsicCalibrationMatrixT> EstimateIntrinsics(const map<size_t, CameraMatrixT>& mPList, const vector<FeatureListT>& featureStack, const vector<IntrinsicCalibrationMatrixT>& mKList, const SfMPipelineParametersC& parameters )

/**
 * @brief Prepares the input data for metric SfM
 * @param[in] mKList Intrinsic calibration matrices
 * @param[in] mPList Available camera matrix estimates
 * @param[in] featureStack Feature stack
 * @return Filtered camera matrix list and feature stack
 */
auto SfMPipelineC::PrepareMetricSfM(const map<size_t, IntrinsicCalibrationMatrixT>& mKList, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack) -> tuple<map<size_t, CameraMatrixT>, vector<ImageFeatureListT> >
{
	//Data belonging to cameras without an intrinsic calibration estimate is culled
	size_t nFiltered=mKList.size();
	vector<ImageFeatureListT> filteredFeatureStack(nFiltered);
	map<size_t, CameraMatrixT> mPListFiltered;

	auto itE=mPList.end();

	size_t index=0;
	for(const auto& current : mKList)
	{
		IntrinsicCalibrationMatrixT mN=current.second.inverse();

		//Copy and normalise the camera matrix estimate, if one exists
		auto it=mPList.find(current.first);
		if(it!=itE)
			mPListFiltered[index]=mN*it->second;

		//Copy and normalise the features
		filteredFeatureStack[index]=featureStack[current.first];
		for_each(filteredFeatureStack[index], [&](ImageFeatureC& currentFeature){ currentFeature.Coordinate()= (mN*currentFeature.Coordinate().homogeneous()).eval().hnormalized(); } );

		++index;
	}	//for(const auto& current : mKList)

	return make_tuple(move(mPListFiltered), move(filteredFeatureStack));
}	//tuple<map<size_t, CameraMatrixT>, vector<FeatureListT> > PrepareMetricSfM(const map<size_t, IntrinsicCalibrationMatrixT>& mKList, const map<size_t, CameraMatrixT>& mPList, const vector<FeatureListT>& featureStack)

/**
 * @brief Makes an approximate pose covariance matrix
 * @param[in] vC Position
 * @param[in] parameters Parameters
 * @return Covariance matrix
 */
auto SfMPipelineC::MakePoseCovariance(const Coordinate3DT& vC, const SfMPipelineParametersC& parameters) ->  PoseCovarianceMatrixT
{
	PoseCovarianceMatrixT mC=PoseCovarianceMatrixT::Identity();

	double orientationVariance=pow<2>(parameters.maxOrientationError/3);	//The maximum error is the 3-sigma point
	double poseVariance=pow<2>(parameters.positionPrecision/3) * vC.squaredNorm();	//The maximum error is the 3-sigma point

	mC.diagonal()<<orientationVariance, orientationVariance, orientationVariance, poseVariance, poseVariance, poseVariance;

	return mC;
}	//PoseCovarianceMatrixT MakePoseCovariance(const Coordinate3DT& vC, const SfMPipelineParametersC& parameters)

/**
 * @brief Makes pose observations for the pose graph optimiser
 * @param[in] egEdgeList Edge list
 * @param[in] cameraList Cameras
 * @param[in] parameters Parameters
 * @param[in] cameraPairStack Camera matrices for the epipolar graph edges
 * @return Inlier edges belonging to the largest connected component
 * @remarks There are no direct measurements of the scale, only the direction of the translation vector between the cameras. So, the scale is derived from \c cameraList
 */
auto SfMPipelineC::MakePoseObservations(const EpipolarGraphEdgeListT& egEdgeList, const map<size_t, CameraMatrixT>& cameraList, const map<pair<size_t, size_t> , pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, const SfMPipelineParametersC& parameters) -> PDLPoseGraphProblemC::observation_set_type
{
	//Absolute poses, viewpoint form
	map<size_t, Viewpoint3DC> viewpoints;
	transform(cameraList, inserter(viewpoints, viewpoints.end()), [](const map<size_t, CameraMatrixT>::value_type& current){ return make_pair(current.first, Viewpoint3DC(current.second));} );

	//Relative pose measurements from the epipolar graph
	auto itE = cameraList.end();
	list<pair<unsigned int, unsigned int> > inlierEdgeList;
	map<pair<unsigned int, unsigned int>, PDLPoseGraphProblemC::pose_type> inlierObservations;
	for(const auto& current : cameraPairStack)
	{
		size_t i1;
		size_t i2;
		tie(i1, i2)=current.first;

		//Are both cameras associated with the edge still alive?
		if(cameraList.find(i1)==itE || cameraList.find(i2)==itE)
			continue;

		//Predicted and measured viewpoints
		Viewpoint3DC prediction=ComputeRelativePose(viewpoints[i1], viewpoints[i2]);

		Viewpoint3DC measurement(current.second.second);	//The distance is 1
		measurement.position *= prediction.position.norm()/measurement.position.norm();	//Correct the scale

		//Measurement covariance
		PoseCovarianceMatrixT mCObs=2*MakePoseCovariance(prediction.position, parameters);	//2x as both camera pose estimates are noisy
		PoseCovarianceMatrixT mCMeas=MakePoseCovariance(measurement.position, parameters);	//The relative pose is estimated directly from the correspondences

		//Gaussian for the Mahalonobis distance
		typename P3PSolverC::minimal_model_type mMeasurement=P3PSolverC::MakeMinimal(ComposeCameraMatrix(measurement.position, measurement.orientation.matrix()));
		MultivariateGaussianC<PoseMinimalDifferenceC> gaussian(mMeasurement, mCObs+mCMeas);

		//Inlier wrt Mahalanobis error?
		typename P3PSolverC::minimal_model_type mPrediction=P3PSolverC::MakeMinimal(ComposeCameraMatrix(prediction.position, prediction.orientation.matrix()));

		if( *gaussian.ComputeMahalonobisDistance(mPrediction) >parameters.poseGraphInlierThreshold)
			continue;

		inlierEdgeList.emplace_back(current.first);
		inlierObservations.emplace(current.first, PDLPoseGraphProblemC::pose_type(measurement, mCMeas));
	}	//for(const auto& current : egEdgeList)

	PDLPoseGraphProblemC::observation_set_type output;
	if(inlierEdgeList.empty())
		return output;

	//Find the largest connected component, and retain only the measurements connected to it
	vector<list<pair<unsigned int, unsigned int> > > connected=FindConnectedComponents(inlierEdgeList, true);
	for(const auto& current : connected[0])
		output.emplace(current, inlierObservations[current]);

	return output;
}	//auto FilterEpipolarGraph(const EpipolarGraphEdgeListT& egEdgeList, const map<size_t, CameraMatrixT>& cameraList, const SfMPipelineParametersC& parameters) -> EpipolarGraphEdgeListT

/**
 * @brief Refines the estimated camera poses via pose graph optimisation
 * @param[in] egEdgeList Epipolar graph edges
 * @param[in] cameraList Cameras
 * @param[in] cameraPairStack Camera pairs for each edge on the epipolar graph
 * @param[in] parameters Parameters
 * @return Refined camera matrix estimates. If not enough observations, or the optimisation process fails, original cameras
 */
map<size_t, CameraMatrixT> SfMPipelineC::RefinePose(const EpipolarGraphEdgeListT& egEdgeList, const map<size_t, CameraMatrixT>& cameraList, const map<pair<size_t, size_t>, pair<CameraMatrixT, CameraMatrixT> >& cameraPairStack, const SfMPipelineParametersC& parameters)
{
	//Constraints for the optimisation problem. It also acts as a filter to eliminate the cameras inconsistent with their pairwise observations
	PDLPoseGraphProblemC::observation_set_type observations=MakePoseObservations(egEdgeList, cameraList, cameraPairStack, parameters);

	if(observations.empty())
		return map<size_t, CameraMatrixT>();

	//Remove any cameras that are not in the pose observations
	set<size_t> survivorIndices;
	for_each(observations|map_keys, [&](const PDLPoseGraphProblemC::observation_set_type::key_type& current){ survivorIndices.insert(current.first); survivorIndices.insert(current.second); } );

	//Initial estimate
	PDLPoseGraphProblemC::model_type initialEstimate;
	for_each(survivorIndices, [&](size_t c){initialEstimate.emplace(c, Viewpoint3DC(cameraList.at(c)));});

	//Do we have enough constraints?
	PowellDogLegC<PDLPoseGraphProblemC>::result_type refinedViewpoints;
	PowellDogLegDiagnosticsC diagnostics;
	if(observations.size() >= 7*survivorIndices.size())
	{
		//Refinement
		PDLPoseGraphProblemC problem(initialEstimate, observations, optional<MatrixXd>());

		PowellDogLegC<PDLPoseGraphProblemC> poseGraphEngine;
		PowellDogLegC<PDLPoseGraphProblemC>::covariance_type dummy;
		diagnostics=poseGraphEngine.Run(refinedViewpoints, dummy, problem, parameters.poseGraphOptimisationParameters);
	}	//if(observations.size() >= 7*survivorIndices.size())

	if(!diagnostics.flagSuccess)
		refinedViewpoints=initialEstimate;

	//Map back to cameras
	map<size_t, CameraMatrixT> output;
	for(const auto& current : refinedViewpoints)
		output.emplace(current.first, ComposeCameraMatrix(current.second.position, current.second.orientation.matrix()));

	return output;
}	//map<size_t, CameraMatrixT> RefinePose(const PoseGraphEdgeContainerT& pgEdgeList, const map<size_t, CameraMatrixT>& cameraList, const SfMPipelineParametersC& parameters)

/**
 * @brief Performs multiview triangulation
 * @param[in] cameraList Camera list
 * @param[in] egEdgeList Epipolar graph edge list
 * @param[in] imageCoordinateStack Image coordinate stack
 * @param[in] imageNoiseVector Image noise corrupting the coordinates
 * @param[in] parameters Parameters
 * @return Point cloud; 3D-2D correspondences for each camera
 */
auto SfMPipelineC::PerformMultiviewTriangulation(const map<size_t, CameraMatrixT>& cameraList, const EpipolarGraphEdgeListT& egEdgeList, const vector<vector<Coordinate2DT> >& imageCoordinateStack, const vector<double>& imageNoiseVector, const SfMPipelineParametersC& parameters ) -> tuple<vector<Coordinate3DT>, map<size_t, CoordinateCorrespondenceList32DT> >
{
	//Get the camera id's in the set
	set<size_t> cameraIds;
	copy(cameraList | map_keys, inserter(cameraIds, cameraIds.end()));

	bimap<vector_of<size_t>, set_of<size_t>, left_based> indexMap;
	size_t nCameras=cameraIds.size();
	auto it=cameraIds.begin();
	for(size_t c=0; c<nCameras; ++c, advance(it,1))
		indexMap.push_back(bimap<vector_of<size_t>, set_of<size_t>, left_based>::value_type(c, *it));	//New index; original index

	//Filter the edge list
	auto itE=cameraIds.end();
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT> filteredCorrespondences1;
	for(const auto& current : egEdgeList)
	{
		size_t i1;
		size_t i2;
		tie(i1, i2)=current.first;

		if(cameraIds.find(i1)!=itE && cameraIds.find(i2)!=itE)
			filteredCorrespondences1.emplace(PairIdT(indexMap.right[i1], indexMap.right[i2]),  get<VisionGraphPipelineC<Fundamental7PipelineProblemT>::iCorrespondenceList>(current.second));
	}	//for(const auto& current : egEdgeList)

	PairwiseViewT filteredCorrespondences2;
	std::tie(ignore, filteredCorrespondences2)=PerformCorrespondenceFusion(filteredCorrespondences1);

	//Filter the image coordinate stack
	vector<vector<Coordinate2DT>> filteredCoordinates; filteredCoordinates.reserve(cameraIds.size());
	for_each(cameraIds, [&](size_t c){filteredCoordinates.push_back(imageCoordinateStack[c]);} );

	//Filter the cameras
	vector<CameraMatrixT> filteredCameras(nCameras);
	copy(cameraList | map_values, filteredCameras.begin());

	//Triangulation
	double meanNoise=0;	//Multiview triangulation assumes uniform noise
	for(size_t c : cameraIds)
		meanNoise+=imageNoiseVector[c];

	meanNoise/=nCameras;

	MultiviewTriangulationParametersC localParameters=parameters.sceneReconstructionParameters.triangulationParameters;
	localParameters.noiseVariance=meanNoise;
	localParameters.inlierTh=(1+parameters.referenceTransferError)*P3PSolverC::error_type::ComputeOutlierThreshold(parameters.inlierRejectionProbability, meanNoise);	//TODO Derive the error by reprojecting the associated pairwise reconstruction to the cameras

	vector<Coordinate3DT> pointCloud;
	vector<PointCovarianceMatrixT> covariances;
	PairwiseViewT inliers;
	MultiviewTriangulationC::Run(pointCloud, covariances, inliers, filteredCorrespondences2, filteredCameras, filteredCoordinates, localParameters);

	//Rearrange the correspondences to get the 3D-2D matches
	FeatureTrackStackT featureTracks=CorrespondenceFusionC::ConvertToUnified(inliers);
	map<size_t, CoordinateCorrespondenceList32DT> correspondences32;
	for_each(cameraIds, [&](size_t c){correspondences32[c]=CoordinateCorrespondenceList32DT();});

	size_t nPoints=pointCloud.size();
	for(size_t c=0; c<nPoints; ++c)
		for(const auto& current : featureTracks[c])
		{
			size_t iSource=get<CorrespondenceFusionC::iSourceId>(current);
			correspondences32[  indexMap.left[iSource].second ].push_back(CoordinateCorrespondenceList32DT::value_type(pointCloud[c], filteredCoordinates[iSource][get<CorrespondenceFusionC::iElementId>(current)], MatchStrengthC(1,0) ));
		}	//for(const auto& current : currentTrack)

	return make_tuple(move(pointCloud), move(correspondences32));
}	//tuple<vector<Coordinate3DT>, map<size_t, CoordinateCorrespondenceList32DT> > PerformMultiviewTriangulation(const map<size_t, CameraMatrixT>& cameraList, const EpipolarGraphEdgeListT& egEdgeList, const vector<vector<Coordinate2DT> >& imageCoordinateStack )

/**
 * @brief Estimates the camera poses
 * @param[in, out] rng Random number generator
 * @param[in] mPList Available camera matrix estimates. Normalised
 * @param[in] featureStack Image features
 * @param[in] imageNoise Image noise corrupting the coordinates
 * @param[in] parameters Parameters
 * @return A tuple: Estimated camera matrices; Point cloud
 */
 tuple<map<size_t, CameraMatrixT>, vector<Coordinate3DT> > SfMPipelineC::EstimatePose(RNGT& rng, const map<size_t, CameraMatrixT>& mPList, const vector<ImageFeatureListT>& featureStack, const vector<double>& imageNoise, const SfMPipelineParametersC& parameters )
{
	//Pose graph
	vector<map<size_t, CameraMatrixT> > cameraSequences;
	PoseGraphEdgeContainerT pgEdgeList;
	EpipolarGraphEdgeListT egEdgeList;
	map<pair<size_t, size_t> , pair<CameraMatrixT, CameraMatrixT> > cameraPairStack;
	std::tie(cameraSequences, pgEdgeList, egEdgeList, cameraPairStack)=FormCameraSequences<EssentialPipelineProblemT, Similarity3DPipelineProblemT>(rng, mPList, featureStack, imageNoise, true, parameters);

	if(cameraSequences.empty())
		return make_tuple(map<size_t, CameraMatrixT>(), vector<Coordinate3DT>());

	//Image coordinate stack
	vector<vector<Coordinate2DT>> imageCoordinateStack(featureStack.size());
	transform(featureStack, imageCoordinateStack.begin(), [](const ImageFeatureListT& current){return MakeCoordinateVector(current);});

	//Pose graph optimisation for each sequence
	optional<tuple<double, map<size_t, CameraMatrixT>, vector<Coordinate3DT> > > bestSequence;
	size_t nSequences=cameraSequences.size();
	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(dynamic) private(c)  num_threads(parameters.nThreads)
	for(c=0; c<nSequences; ++c)
	{
		//Pose graph optimisation
		map<size_t, CameraMatrixT> refined=RefinePose(egEdgeList, cameraSequences[c], cameraPairStack, parameters);

		if(refined.empty())
			continue;

		//Evaluation
		vector<Coordinate3DT> pointCloud;
		map<size_t, CoordinateCorrespondenceList32DT> sceneImageCorrespondenceStack;
		std::tie(pointCloud, sceneImageCorrespondenceStack)=PerformMultiviewTriangulation(refined, egEdgeList, imageCoordinateStack, imageNoise, parameters);

		//Score:
		//Simply going for the size of the point cloud can be misleading: we want a good spread of points across all cameras, so that the subsequent robust registration method can work
		//So, sum of square-roots of scene-image correspondences
		double score=accumulate(sceneImageCorrespondenceStack | map_values, 0.0, [](double acc, CoordinateCorrespondenceList32DT& current){return acc+sqrt(current.size());});

	#pragma omp critical(SFMP_EP1)
		if(!bestSequence || get<0>(*bestSequence)<score )
			bestSequence=make_tuple(score, refined, pointCloud);
	}	//for(const auto& current : cameraSequences)

	if(!bestSequence)
		return make_tuple(map<size_t, CameraMatrixT>(), vector<Coordinate3DT>());

	return make_tuple(move(get<1>(*bestSequence)), move(get<2>(*bestSequence)) );
}	//map<size_t, Viewpoint3DC> EstimatePose(const map<size_t, CameraMatrixT>& mPList, const vector<FeatureListT>& featureStack, const SfMPipelineParametersC& parameters )

/**
 * @brief Denormalises the cameras and the correspondences
 * @param[in] cameraList Camera list
 * @param[in] correspondences Correspondences
 * @param[in] mKList Intrinsic calibration matrices
 * @return A tuple: Denormalised cameras; denormalised correspondences
 */
tuple<map<size_t, CameraMatrixT>, map<size_t, CoordinateCorrespondenceList32DT> > SfMPipelineC::DenormaliseGeometry(const map<size_t, CameraMatrixT>& cameraList, const map<size_t, CoordinateCorrespondenceList32DT>& correspondences, const map<size_t, IntrinsicCalibrationMatrixT>& mKList)
{
	map<size_t, CameraMatrixT> denormalisedCameras;
	map<size_t, CoordinateCorrespondenceList32DT> denormalisedCorrespondences;

	for(const auto& currentCamera : cameraList)
	{
		size_t i=currentCamera.first;
		IntrinsicCalibrationMatrixT mK=mKList.at(i);

		denormalisedCameras[i]=mK*currentCamera.second;

		CoordinateCorrespondenceList32DT currentCorr=correspondences.at(i);
		for(auto& current : currentCorr)
			current.right = (mK*current.right.homogeneous()).eval().hnormalized();

		denormalisedCorrespondences[i]=currentCorr;
	}	//for(const auto& current : cameraList)

	return make_tuple(move(denormalisedCameras), move(denormalisedCorrespondences));
}	//tuple<map<size_t, CameraMatrixT>, map<size_t, CoordinateCorrespondenceList32DT> > DenormaliseGeometry(const map<size_t, CameraMatrixT>& cameraList, const map<size_t, CoordinateCorrespondenceList32DT>& correspondences, const map<size_t, IntrinsicCalibrationMatrixT>& mKList)

/**
 * @brief Builds a mutiview 3D scene model with appearance descriptors, for robust registration
 * @param[in] cameraList Unnormalised cameras
 * @param[in] featureStack Unnormalised image features
 * @param[in] parameters Parameters
 * @return A tuple: 3D scene model, point covariances
 * @remarks In the scene descriptors, the camera indices should be ignored
 */
auto SfMPipelineC::BuildGlobalSceneModel(const map<size_t, CameraMatrixT>& cameraList, const vector<ImageFeatureListT>& featureStack, const SfMPipelineParametersC& parameters) -> tuple<vector<SceneFeatureC>, vector<PointCovarianceMatrixT> >
{
	//Prepare the input
	size_t nCameras=cameraList.size();
	vector<CameraMatrixT> cameras; cameras.reserve(nCameras);
	vector<ImageFeatureListT> features; features.reserve(nCameras);
	for(const auto& current : cameraList)
	{
		cameras.push_back(current.second);
		features.push_back(featureStack[current.first]);
	}	//for(const auto& current : cameraList)

	vector<Coordinate3DT> pointCloud;
	vector<PointCovarianceMatrixT> covariances;
	vector<SceneFeatureC> model;
	Sparse3DReconstructionPipelineDiagnosticsC diagnostics=Sparse3DReconstructionPipelineC::Run(pointCloud, covariances, model, cameras, features, EssentialPipelineProblemT::matching_problem_type::similarity_type(), parameters.sceneReconstructionParameters);

	return make_tuple(move(model), move(covariances));
}	//vector<SceneFeatureC> BuildSceneModel(const map<size_t, CameraMatrixT>& cameraList, const vector<ImageFeatureListT>& featureStack, const SfMPipelineParametersC& parameters)

/**
 * @brief Runs the algorithm
 * @param[out] result Output. [Camera Id, Camera matrix]
 * @param[out] world Point cloud
 * @param[in, out] rng Random number generator
 * @param[in] featureStack Feature stack. Unnormalised features
 * @param[in] cameraList Cameras. Each element should have at least the frame size defined. Any other information is used for deriving initial estimates
 * @param[in] parameters Parameters
 * @return Diagnostics object
 */
SfMPipelineDiagnosticsC SfMPipelineC::Run(map<size_t, CameraMatrixT>& result, vector<Coordinate3DT>& world, RNGT& rng, const vector<ImageFeatureListT>& featureStack, const vector<CameraC>& cameraList, SfMPipelineParametersC parameters)
{
	SfMPipelineDiagnosticsC diagnostics;
	result.clear();

	//Return if no features
	if(featureStack.empty())
	{
		diagnostics.flagSuccess=true;
		return diagnostics;
	}//	if(featureStack.empty())

	//Preconditions
	ValidateInput(featureStack, cameraList, parameters);

	//Process the cameras
	vector<IntrinsicCalibrationMatrixT> mKList1;	// Intrinsic calibration matrix with the initial estimates for the known calibration parameters
	map<size_t, CameraMatrixT> mPList1;	//Initial camera matrix estimates, where one is available
	vector<bool> flagFocals;	// For each camera, indicates whether a focal length estimate is available
	tie(mPList1, mKList1, flagFocals)=ProcessCameras(cameraList, parameters);

	//If necessary, estimate the intrinsic calibration parameters
	map<size_t, IntrinsicCalibrationMatrixT> mKList2;	//Intrinsic calibration matrices with all calibration parameters
	bool flagPoseOnly=false;	//Operation mode for the robust registration stage
	if( !parameters.flagEuclidean || any_of(flagFocals.begin(), flagFocals.end(), [](bool current){return !current;}))
	{
		mKList2=EstimateIntrinsics(rng, mPList1, featureStack, mKList1, parameters);
		flagPoseOnly=parameters.flagUniformFocalLength;
	}
	else
	{
		transform(counting_range((size_t)0, featureStack.size()), mKList1, inserter(mKList2, mKList2.end()), [](size_t i, const IntrinsicCalibrationMatrixT& current){return make_pair(i, current);} );
		flagPoseOnly=true;
	}	//if( !parameters.flagEuclidean || any_of(flagFocals.begin(), flagFocals.end(), [](bool current){return !current;}))

	if(mKList2.empty())
		return diagnostics;

	//Extrinsics

    vector<ImageFeatureListT> featureStack2n;	//Normalised image features belonging to the surviving cameras
	map<size_t, CameraMatrixT> mPList2n;	//Normalised camera matrix estimates for the surviving cameras. Loses the original indices!
	tie(mPList2n, featureStack2n)=PrepareMetricSfM(mKList2, mPList1, featureStack);	//Prepares the data for metric calibration

	vector<double> normalisedImageNoise2; normalisedImageNoise2.reserve(mKList2.size());
	transform(mKList2|map_values, back_inserter(normalisedImageNoise2), [&](const IntrinsicCalibrationMatrixT& current){return parameters.imageNoiseVariance/pow<2>(current(0,0));} );

	map<size_t, CameraMatrixT> mPList3n;
	vector<Coordinate3DT> pointCloud3;	//Initial estimate for the point cloud, by multiview triangulation with the camera matrices
	std::tie(mPList3n, pointCloud3)=EstimatePose(rng, mPList2n, featureStack2n, normalisedImageNoise2, parameters);

	if(mPList3n.empty())
		return diagnostics;

	//Robust registration

	//Back to metric cameras
	map<size_t, CameraMatrixT> mPList3;	//Calibration estimates for the cameras. Back to original indices
	map<size_t, CameraMatrixT> mPList3nO;	//Normalised cameras, back to original indices

	vector<size_t> indexMap; indexMap.reserve(mKList2.size());
	copy(mKList2 | map_keys, back_inserter(indexMap));
	for(const auto& current : mPList3n)
	{
		size_t indexO=indexMap[current.first];
		mPList3nO[indexO]=current.second;
		mPList3[indexO] = mKList2[indexO]*current.second;
	}	//for(const auto& current : mPList3n)

	//Rebuild the 3D scene model, with descriptors
	vector<SceneFeatureC> sceneModel;
	vector<PointCovarianceMatrixT> sceneCovariance;
	std::tie(sceneModel, sceneCovariance)=BuildGlobalSceneModel(mPList3, featureStack, parameters);

	//Registration: Uses the initial estimate for the survivors, and for the others, starts from scratch

	size_t nCameras=featureStack.size();
	auto itEK2=mKList2.end();
	vector<IntrinsicCalibrationMatrixT> combinedIntrinsics(mKList1);
	vector<double> normalisedImageNoise3; normalisedImageNoise3.reserve(nCameras);
	vector<ImageFeatureListT> normalisedFeatureStack(featureStack);
	for(size_t c=0; c<nCameras; ++c)
	{
		if(parameters.flagUniformFocalLength)
			combinedIntrinsics[c]=mKList2.begin()->second;
		else
		{
			auto it=mKList2.find(c);
			if(it!=itEK2)
				combinedIntrinsics[c]=it->second;
		}	//if(parameters.flagUniformFocalLength)

		normalisedImageNoise3.push_back(parameters.imageNoiseVariance/pow<2>(combinedIntrinsics[c](0,0)));

		IntrinsicCalibrationMatrixT mN=combinedIntrinsics[c].inverse();
		for_each(normalisedFeatureStack[c], [&](ImageFeatureC& currentFeature){ currentFeature.Coordinate()= (mN*currentFeature.Coordinate().homogeneous()).eval().hnormalized(); } );
	}	//for(size_t c=0; c<nCameras; ++c)

	map<size_t, CameraMatrixT> mPList4n;	//Calibration estimates, normalised
	map<size_t, CoordinateCorrespondenceList32DT> sceneImageCorrespondences4n;	//3D-2D correspondences, normalised
	map<size_t, IntrinsicCalibrationMatrixT> mKList4;
	if(flagPoseOnly)
	{
		std::tie(mPList4n, sceneImageCorrespondences4n)=PerformRobustRegistration<P3PEstimatorProblemT>(mPList3nO, sceneModel, sceneCovariance, normalisedFeatureStack, normalisedImageNoise3, parameters);
		for_each(mPList4n, [&](const map<size_t, CameraMatrixT>::value_type& current) {mKList4[current.first]=combinedIntrinsics[current.first];} );
	}
	else
	{

		std::tie(mPList4n, sceneImageCorrespondences4n)=PerformRobustRegistration<P4PEstimatorProblemT>(mPList3nO, sceneModel, sceneCovariance, normalisedFeatureStack, normalisedImageNoise3, parameters);

		//Remove the corrections to the intrinsic calibration matrices, and renormalise
		map<size_t, Matrix3d> normalisers;
		for(const auto& current : mPList4n)
		{
			IntrinsicCalibrationMatrixT mK;
			tie(mK, ignore, ignore)=DecomposeCamera(current.second);

			mKList4[current.first] = combinedIntrinsics[current.first] * mK;	//Apply the correction: mKo * mKc * mPn
			normalisers[current.first] = mK.inverse();
		}	//for(const auto& current : mPList4)

		tie(mPList4n, sceneImageCorrespondences4n) = DenormaliseGeometry(mPList4n, sceneImageCorrespondences4n, normalisers);	//The name is misleading- with inverse mK, this can be used for normalisation as well
	}

	world=MakeCoordinateVector(sceneModel);
	map<size_t, CameraMatrixT> mPList5n=mPList4n;

	//Denormalise cameras
	for(const auto& current : mPList5n)
		result[current.first] = mKList4[current.first] * current.second;

	diagnostics.flagSuccess=true;

	return diagnostics;
}	//SfMPipelineDiagnosticsC Run(map<size_t, CameraMatrixT>& result, vector<Coordinate3DT>& world, RNGT& rng, const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList, SfMPipelineParametersC parameters)

}	//namespace GeometryN
}	//namespace SeeSawN
