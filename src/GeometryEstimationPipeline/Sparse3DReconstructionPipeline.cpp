/**
 * @file Sparse3DReconstructionPipeline.cpp Implementation of \c Sparse3DReconstructionPipelineC
 * @author Evren Imre
 * @date 16 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Sparse3DReconstructionPipeline.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Prints the results
 * @param[in] observed Observed correspondences
 * @param[in] implied Implierd correspondences
 * @param[in] nCameras Number of cameras
 */
void Sparse3DReconstructionPipelineC::PrintResult(const PairwiseViewT& observed, const PairwiseViewT& implied, size_t nCameras)
{
	auto itE1=observed.end();
	auto itE2=implied.end();

	for(size_t c1=0; c1<nCameras; ++c1)
		for(size_t c2=c1+1; c2<nCameras; ++c2)
		{
			auto it1=observed.find(SizePairT(c1,c2));
			auto it2=implied.find(SizePairT(c1,c2));

			if(it1!=itE1 || it2!=itE2)
			{
				cout<<"Pair "<<c1<<" "<<c2;

				if(it1!=itE1)
					cout<<" Observed: "<<it1->second.size();

				if(it2!=itE2)
					cout<<" Implied: "<<it2->second.size();

				cout<<"\n";
			}	//if(it1!=itE1 || it2!=itE2)
		}	//for(size_t c2=c1+1; c2<nCameras; ++c2)
}	//void PrintResult(const PairwiseViewT& observed, const PairwiseViewT& implied, size_t nCameras)

/**
 * @brief Verifies the validity of the parameters
 * @param[in,out] parameters Parameters
 * @throws invalid_argument If any of the parameters is invalid
 */
void Sparse3DReconstructionPipelineC::ValidateParameters(Sparse3DReconstructionPipelineParametersC& parameters)
{
	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("Sparse3DReconstructionPipelineC::ValidateParameters : noiseVariance must be positive. Value=")+lexical_cast<string>(parameters.noiseVariance)));

	if(parameters.pRejection<=0 || parameters.pRejection>=1)
		throw(invalid_argument(string("Sparse3DReconstructionPipelineC::ValidateParameters : pRejection must be in (0,1). Value=")+lexical_cast<string>(parameters.pRejection)));

	//k-NN matching introduces links between clusters, which leads to failed consistency checks
	parameters.matcherParameters.matcherParameters.nnParameters.neighbourhoodSize12=1;
	parameters.matcherParameters.matcherParameters.nnParameters.neighbourhoodSize21=1;

	parameters.matcherParameters.flagConsistency=true;

	parameters.triangulationParameters.noiseVariance=parameters.noiseVariance;
	parameters.triangulationParameters.inlierTh=TransferErrorH32DT::ComputeOutlierThreshold(parameters.pRejection, parameters.noiseVariance);
}	//void ValidateParameters(const Sparse3DReconstructionPipelineC& parameters)

/**
 * @brief Initialises the pairwise epipolar constraints
 * @param[in] cameras Cameras
 * @param[in] parameters Parameters
 * @return Epipolar constraints: [Pair id; constraint]
 */
auto Sparse3DReconstructionPipelineC::MakePairwiseConstraints(const vector<CameraMatrixT>& cameras, const Sparse3DReconstructionPipelineParametersC& parameters) -> map<SizePairT, EpipolarSampsonConstraintT >
{
	map<SizePairT, EpipolarSampsonConstraintT> output;

	//Compute the inlier threshold
	double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(parameters.pRejection, parameters.noiseVariance);

	//Iterate over the pairs
	size_t nCameras=cameras.size();
	for(size_t c1=0; c1<nCameras; ++c1)
		for(size_t c2=c1+1; c2<nCameras; ++c2)
		{
			EpipolarMatrixT mF=CameraToFundamental(cameras[c1], cameras[c2]);
			output[SizePairT(c1,c2)]=EpipolarSampsonConstraintT(EpipolarSampsonErrorC(mF), inlierTh, 1);
		}	//for(size_t c2=c1+1; c2<nCameras; ++c2)

	return output;
}	//vector<optional<EpipolarSampsonConstraintT> > MakePairwiseConstraints(const vector<CameraMatrixT>& cameras, const Sparse3DReconstructionPipelineParametersC& parameters)

/********** EXPLICIT INSTANTIATIONS **********/
template Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>&, vector<MultiviewTriangulationC::covariance_matrix_type>&, vector<SceneFeatureC>&, const vector<CameraMatrixT>&, const vector<vector<ImageFeatureC> >&, const InverseEuclideanIDConverterT&, Sparse3DReconstructionPipelineParametersC);
template Sparse3DReconstructionPipelineDiagnosticsC Sparse3DReconstructionPipelineC::Run(vector<Coordinate3DT>&, vector<MultiviewTriangulationC::covariance_matrix_type>&, vector<OrientedBinarySceneFeatureC>&, const vector<CameraMatrixT>&, const vector<vector<BinaryImageFeatureC> >&, const InverseHammingBIDConverterT&, Sparse3DReconstructionPipelineParametersC);
}	//GeometryN
}	//SeeSawN


