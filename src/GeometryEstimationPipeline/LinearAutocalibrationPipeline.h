/**
 * @file LinearAutocalibrationPipeline.h Public interface for \c LinearAutocalibrationPipelineC
 * @author Evren Imre
 * @date 7 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LINEAR_AUTOCALIBRATION_PIPELINE_H_6490390
#define LINEAR_AUTOCALIBRATION_PIPELINE_H_6490390

#include "LinearAutocalibrationPipeline.ipp"
#include <random>

namespace SeeSawN
{
namespace GeometryN
{

struct LinearAutocalibrationPipelineParametersC;	///< Parameters for \c LinearAutocalibrationPipelineC
struct LinearAutocalibrationPipelineDiagnosticsC;	///< Diagnostics for \c LinearAutocalibrationPipelineC
class LinearAutocalibrationPipelineC;	///< Linear autocalibration pipeline

/********** EXTERN TEMPLATES **********/
using std::mt19937_64;
extern template LinearAutocalibrationPipelineDiagnosticsC LinearAutocalibrationPipelineC::Run(Homography3DT&, map<size_t, IntrinsicCalibrationMatrixT>&, mt19937_64&, const vector<CameraMatrixT>&, const vector<IntrinsicC>&, LinearAutocalibrationPipelineParametersC);;

}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
extern template class std::vector<std::size_t>;

#endif /* LINEAR_AUTOCALIBRATION_PIPELINE_H_6490390 */
