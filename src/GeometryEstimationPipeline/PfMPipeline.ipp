/**
 * @file PfMPipeline.ipp Implementation of the panorama-from-motion pipeline
 * @author Evren Imre
 * @date 12 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef PFM_PIPELINE_IPP_4819902
#define PFM_PIPELINE_IPP_4819902

#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <vector>
#include <map>
#include <cstddef>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <cmath>
#include <list>
#include <set>
#include <iterator>
#include <functional>
#include "VisionGraphPipeline.h"
#include "RotationRegistrationPipeline.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../GeometryEstimationComponents/Homography2DComponents.h"
#include "../GeometryEstimationComponents/P2PfComponents.h"
#include "../GeometryEstimationComponents/P2PComponents.h"
#include "../Geometry/RelativeRotationRegistration.h"
#include "../Geometry/MultiviewPanoramaBuilder.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/Rotation3DSolver.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../RANSAC/RANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Wrappers/BoostGraph.h"
#include "../RandomSpanningTreeSampler/RSTSRatioRegistrationProblem.h"
#include "../RandomSpanningTreeSampler/RandomSpanningTreeSampler.h"
#include "../Numeric/RatioRegistration.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::lexical_cast;
using boost::for_each;
using boost::transform;
using boost::math::quantile;
using boost::math::chi_squared_distribution;
using boost::math::pow;
using std::vector;
using std::map;
using std::size_t;
using std::tuple;
using std::get;
using std::tie;
using std::string;
using std::invalid_argument;
using std::any_of;
using std::max_element;
using std::max;
using std::floor;
using std::list;
using std::multiset;
using std::sort;
using std::next;
using std::advance;
using std::ignore;
using std::multiset;
using std::greater;
using std::multiset;
using SeeSawN::GeometryN::VisionGraphPipelineC;
using SeeSawN::GeometryN::VisionGraphPipelineDiagnosticsC;
using SeeSawN::GeometryN::VisionGraphPipelineParametersC;
using SeeSawN::GeometryN::RotationRegistrationPipelineC;
using SeeSawN::GeometryN::RotationRegistrationPipelineDiagnosticsC;
using SeeSawN::GeometryN::RotationRegistrationPipelineParametersC;
using SeeSawN::GeometryN::RelativeRotationRegistrationC;
using SeeSawN::GeometryN::Homography2DPipelineProblemT;
using SeeSawN::GeometryN::PDLP2PfEngineT;
using SeeSawN::GeometryN::PDLP2PfProblemC;
using SeeSawN::GeometryN::RANSACP2PfProblemT;
using SeeSawN::GeometryN::P2PfEstimatorT;
using SeeSawN::GeometryN::P2PfEstimatorProblemT;
using SeeSawN::GeometryN::MakePDLP2PfProblem;
using SeeSawN::GeometryN::PDLP2PEngineT;
using SeeSawN::GeometryN::PDLP2PProblemC;
using SeeSawN::GeometryN::RANSACP2PProblemT;
using SeeSawN::GeometryN::P2PEstimatorT;
using SeeSawN::GeometryN::P2PEstimatorProblemT;
using SeeSawN::GeometryN::MakePDLP2PProblem;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderC;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderDiagnosticsC;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderParametersC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GeometryEstimatorDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimatorParametersC;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::DecomposeK2RK1i;
using SeeSawN::ElementsN::ComputeClosestRotationMatrix;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::WrappersN::FindConnectedComponents;
using SeeSawN::RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerC;
using SeeSawN::RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerDiagnosticsC;
using SeeSawN::RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerParametersC;
using SeeSawN::RandomSpanningTreeSamplerN::RSTSRatioRegistrationProblemC;
using SeeSawN::NumericN::RatioRegistrationC;

/**
 * @brief Parameters for the panorama-from-motion pipeline
 * @ingroup Parameters
 */
struct PfMPipelineParametersC
{
	double noiseVariance;	///< Image noise variance. >0
	double inlierRejectionProbability;	///< Probability of rejection for a true inlier

	size_t nThreads;	///< Number of threads >0
	bool flagInitialEstimate;	///< If \c false, the existing camera parameters are ignored
	bool flagKnownF;	///< If \c true , the existing focal length values are frozen. Ignored if \c flagInitialEstimate=false

	bool flagUniformFocalLength=false;	///< If \c true any cameras with an unknown focal length will have an identical focal length value

	VisionGraphPipelineParametersC visionGraphParameters;	///< Parameters for the vision graph pipeline

	double maxFocalLengthRatioError;	///< Maximum permissible prediction error for a focal length ratio measurement, as relative error between the predicted and measured ratio. >0
	RandomSpanningTreeSamplerParametersC focalLengthRegistrationPipeline;	///< Parameters for the RSTS engine registering the focal length ratios

	RotationRegistrationPipelineParametersC rotationRegistrationParameters;	///< Parameters for the rotation registration pipeline

	PowellDogLegParametersC krkRefinementParameters;	///< Parameters for the refinement of the KRK decomposition

	MultiviewPanoramaBuilderParametersC panoramaBuilderParameters;	///< Parameters for the multiview panorama builder

	GeometryEstimatorParametersC geometryEstimatorParameters;	///< Parameters for the geometry estimator pipeline
	double geLOGeneratorRatio1;	///< LO-RANSAC generator ratio, for estimate refinement, RANSAC stage 1
	double geLOGeneratorRatio2;	///< LO-RANSAC generator ratio, for estimate refinement, RANSAC stage 1

	PfMPipelineParametersC() : noiseVariance(4), inlierRejectionProbability(0.05), nThreads(1), flagInitialEstimate(false), flagKnownF(false), maxFocalLengthRatioError(0.01), geLOGeneratorRatio1(4), geLOGeneratorRatio2(4)
	{}
};	//struct PfMPipelineParametersC

/**
 * @brief Diagnostics for the panorama-from-motion pipeline
 * @ingroup Diagnostics
 */
struct PfMPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the pipeline terminates successfully
	size_t nRegistered;	///< Number of registered cameras
	size_t sPointCloud;	///< Number of points in the estimated point cloud
	size_t nInlierEdge;	///< Number of inlier edges
	double estimatedNoiseVariance;	///< Estimated noise variance for building the panorama

	VisionGraphPipelineDiagnosticsC visionGraphDiagnostics;	///< Diagnostics for the vision graph stage
	RotationRegistrationPipelineDiagnosticsC rotationRegistrationDiagnostics;	///< Diagnostics for the rotation registration stage
	MultiviewPanoramaBuilderDiagnosticsC panoramaDiagnostics;	///< Diagnostics for the multiview panorama builder
	vector<GeometryEstimatorDiagnosticsC> geometryEstimatorDiagnostics;	///< Diagnostics for the refinement pass

	PfMPipelineDiagnosticsC() : flagSuccess(false), nRegistered(0), sPointCloud(0), nInlierEdge(0), estimatedNoiseVariance(0)
	{}
};	//struct PfMPipelineDiagnosticsC

/**
 * @brief Panorama-from-motion pipeline
 * @remarks Usage notes
 * 	- Features
 * 		- Unnormalised (to be performed within the algorithm)
 * 		- Undistorted
 * 	- Cameras
 * 		- Frame size component must be defined
 * 		- Camera centre and distortion components are ignored
 * 		- Any other components are used to derive initial estimates
 * 		- At the output, all cameras are at the origin of a new reference frame with an arbitrary orientation with respect to the original frame
 * @remarks Algorithm flow
 * 	- Normalise the feature coordinates to remove the effect of all intrinsic calibration parameters, except for focal length
 * 	- Compute the pairwise homographies
 * 	- Normalise the homographies, by removing the effect of all intrinsic calibration parameters, except for focal length
 * 	- Decompose the homographies into focal length and orientation components
 * 	- Normalise
 * 	- Register the orientation estimates to a global reference frame
 * 	- Fuse the per camera focal length estimates
 * @ingroup Algorithm
 * @nosubgrouping
 */
//TODO Refine the orientation measurements?
class PfMPipelineC
{
	private:

		typedef VisionGraphPipelineC<Homography2DPipelineProblemT> VisionGraphEngineT;	///< Vision graph engine
		typedef VisionGraphEngineT::feature_container_type FeatureListT;	///< A container for image features
		typedef VisionGraphEngineT::rng_type RNGT;	///< Random number generator type

		typedef CorrespondenceFusionC::pair_id_type IndexTupleT;
		typedef MultiviewPanoramaBuilderC::coordinate_stack_type CoordinateStackT;
		typedef VisionGraphEngineT::edge_container_type EdgeContainerT;

		typedef RelativeRotationRegistrationC::observation_type RotationObservationT;	///< A relative rotation observation
		typedef vector<size_t> IndexContainerT;	///< An index container
		typedef MultiviewPanoramaBuilderC::correspondence_stack_type CorrespondenceStackT;	///< A stack of pairwise image correspondences

		/** @name Implementation details */ //@{
		static void ValidateInput(const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList,  PfMPipelineParametersC& parameters);	///< Validates the input parameters
		static tuple<map<size_t, CameraMatrixT>, vector<IntrinsicCalibrationMatrixT>, vector<bool> > ProcessCameras(const vector<CameraC>& cameraList, const PfMPipelineParametersC& parameters);	///< Processes the cameras

		typedef tuple<size_t, size_t, double, double, RotationMatrix3DT, double> MeasurementT;	///< A measurement
		static constexpr size_t iCamera1=0;	///< Index for the id of the first camera
		static constexpr size_t iCamera2=1;	///< Index for the id of the second camera
		static constexpr size_t iFocalLength1=2;	///< Index for the component for the focal length of the first camera
		static constexpr size_t iFocalLength2=3;	///< Index for the component for the focal length of the second camera
		static constexpr size_t iRelativeRotation=4;	///< Index for the component for the rotation mapping the reference frame of the first camera to that of the second
		static constexpr size_t iQuality=5;	///< Index for the component indicating the quality of the esitmate, as the retention rate of the inliers to the original homography. [0-1]

		static void ComputeLocalDirectionVectors(optional<vector<Coordinate3DT> >& directionVectors, const vector<Coordinate2DT>& imageCoordinates, const optional<vector<Coordinate2DT> >& normalisedCoordinates, const Matrix3d& normaliser);	///< Lazy evaluation of local direction vectors
		static void ComputeNormalisedCoordinates(optional<vector<Coordinate2DT> >& normalisedCoordinates, const vector<Coordinate2DT>& imageCoordinates, const optional<vector<Coordinate3DT> >& directionVectors, const Matrix3d& normaliser);	///< Lazy evaluation of image coordinate normalisation
		static RotationMatrix3DT ExtractRelativeRotation(const Homography2DT& mHn, const CorrespondenceListT& correspondences, const vector<Coordinate3DT>& directionVectors1, const vector<Coordinate3DT>& directionVectors2);	///< Extracts a relative rotation measurement from a homography
		static double EvaluateRecomposedHomography(const Homography2DT& mH, const CorrespondenceListT& correspondences, const vector<Coordinate2DT>& coordinates1, const vector<Coordinate2DT>& coordinates2, const PfMPipelineParametersC& parameters);	///< Evaluates the fitness of a recomposed homography to the original correspondences

		static vector<MeasurementT> ExtractMeasurements(const EdgeContainerT& edgeList, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const CoordinateStackT& coordinateStack, const PfMPipelineParametersC& parameters);	///< Extracts the focal length and relative orientation measurements
		static tuple<vector<MeasurementT>, IndexContainerT> RemoveUnconnected(const vector<MeasurementT>& measurements, size_t nCameras);	///< Removes any measurements that is not a part of the largest connected component

		static tuple<size_t, double> EstimateReferenceFocalLength(const vector<MeasurementT>& measurements, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray);	///< Estimates the reference focal length
		static vector<double> EstimateRelativeFocalLengths(RNGT& rng, const vector<MeasurementT>& measurements, const vector<bool>& fixedFArray, const PfMPipelineParametersC& parameters);	///< Estimates the relative focal lengths
		static vector<double> EstimateFocalLengths(RNGT& rng, const vector<MeasurementT>& measurements, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const PfMPipelineParametersC& parameters);	///< Estimates the focal lengths

		static EdgeContainerT PurgeEdgeList(const EdgeContainerT& edgeList, const IndexContainerT& indexMap, size_t nCameras);	///< Reindexes the edge list and removes the edges belonging to eliminated cameras

		static vector<MeasurementT> RefineRotationMeasurements(const vector<MeasurementT>& measurements, const EdgeContainerT& edgeList, const CoordinateStackT& coordinateStack, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray);	///< Refines the rotation measurements after the estimation of focal lengths

		static tuple<vector<CameraMatrixT>, IndexContainerT, EdgeContainerT, CoordinateStackT> CalibrateCameras(RNGT& rng, const EdgeContainerT& edgeList, const vector<IntrinsicCalibrationMatrixT>& mKList, const vector<bool>& fixedFArray, const CoordinateStackT& coordinateStack, const PfMPipelineParametersC& parameters);	///< Estimates the calibration parameters for the cameras

		static double EstimateNoise(const vector<CameraMatrixT>& cameraList, const CorrespondenceStackT& correspondences, const CoordinateStackT& coordinates, double pReject);	///< Estimates the noise parameter for the panorama builder

		typedef CorrespondenceFusionC::unified_view_type TrackListT;
		static vector<CorrespondenceListT> TrackTo3D2DCorrespondence(const TrackListT& tracks, size_t nCameras);	///< Prepares the 3D-2D correspondence lists for each camera

		static tuple<vector<CameraMatrixT>, vector<GeometryEstimatorDiagnosticsC> > RefineEstimates(RNGT& rng, const vector<CameraMatrixT>& estimated, const TrackListT& tracks, const CoordinateStackT& coordinates, const vector<Coordinate3DT>& panorama, const vector<bool>& flagFixedFArray, const PfMPipelineParametersC& parameters);	///< Refines the estimates via the robust geometry estimation pipeline
		//@}

	public:

		typedef RNGT rng_type;	///< Random number generator type
		static PfMPipelineDiagnosticsC Run( map<size_t, CameraMatrixT>& result, vector<Coordinate3DT>& panorama, RNGT& rng, const vector<FeatureListT>& featureStack, const vector<CameraC>& cameraList, PfMPipelineParametersC parameters);	///< Runs the algorithm

};	//class PfMPipelineC

}	//GeometryN
}	//SeeSawN




#endif /* PFMPIPELINE_IPP_ */
