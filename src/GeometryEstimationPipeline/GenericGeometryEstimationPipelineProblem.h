/**
 * @file GenericGeometryEstimationPipelineProblem.h Public interface for GenericGeometryEstimationPipelineProblemC
 * @author Evren Imre
 * @date 29 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GENERIC_GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_H_1908302
#define GENERIC_GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_H_1908302

#include "GenericGeometryEstimationPipelineProblem.ipp"

namespace SeeSawN
{
namespace GeometryN
{

template<class GeometryEstimationProblemT, class FeatureMatchingProblemT> class GenericGeometryEstimationPipelineProblemC;	///<Problem for the generic geometry estimation pipeline

}	//GeometryN
}	//SeeSawN

#endif /* GENERIC_GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_H_1908302 */
