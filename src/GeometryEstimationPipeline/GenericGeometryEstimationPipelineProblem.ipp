/**
 * @file GenericGeometryEstimationPipelineProblem.ipp Implementation of GenericGeometryEstimationPipelineProblemC
 * @author Evren Imre
 * @date 29 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GENERIC_GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_IPP_9023673
#define GENERIC_GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_IPP_9023673

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/range/concepts.hpp>
#include <Eigen/Dense>
#include <vector>
#include <cmath>
#include <type_traits>
#include "../Elements/Correspondence.h"
#include "../Elements/CorrespondenceDecimator.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../GeometryEstimationPipeline/GeometryEstimatorProblemConcept.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Geometry/GeometrySolverConcept.h"
#include "../RANSAC/RANSACGeometryEstimationProblemConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::ForwardRangeConcept;
using boost::SinglePassRangeConcept;
using Eigen::MatrixXd;
using Eigen::Matrix;
using std::vector;
using std::max;
using std::is_same;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemConceptC;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Problem for the generic geometry estimation pipeline
 * @tparam GeometryEstimationProblemT A geometry estimation problem
 * @tparam FeatureMatchingProblemT A feature matching problem
 * @pre \c GeometryEstimationProblemT is a model of \c GeometryEstimatorProblemConceptC
 * @pre \c FeatureMatchingProblemT is a model of \c FeatureMatcherProblemConceptC
 * @pre \c GeometryEstimationProblemT::ransac_problem_type1 is a model of \c RANSACGeometryEstimationProblemConceptC
 * @pre \c GeometryEstimationProblemT::ransac_problem_type2 is a model of \c RANSACGeometryEstimationProblemConceptC
 * @pre \c GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type \c GeometrySolverConceptC
 * @remarks Usage notes
 * 	- \c GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type determines many of the necessary types
 * 	- Many of the error/constraint objects are passed with the configuration parameters. The actual geometric relation is assigned when available
 * 	- \c initialMatchingConstraint should have a more relaxed threshold then \c matching constraint, to compensate for any inaccuracies in the initial estimate
 * @remarks Too complicated for a unit test. Tested via the application.
 * @ingroup Problem
 * @nosubgrouping
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
class GenericGeometryEstimationPipelineProblemC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<FeatureMatchingProblemT>));
	BOOST_CONCEPT_ASSERT((GeometryEstimatorProblemConceptC<GeometryEstimationProblemT>));
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<typename GeometryEstimationProblemT::ransac_problem_type1>));
	BOOST_CONCEPT_ASSERT((RANSACGeometryEstimationProblemConceptC<typename GeometryEstimationProblemT::ransac_problem_type2>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<typename GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type>));
	//@endcond

	private:

		typedef typename GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type MainSolverT;	///< Type of the main solver
		typedef typename MainSolverT::model_type ModelT;	///< Type of the model
		typedef typename MainSolverT::real_type RealT;	///< Floating point type

		typedef typename FeatureMatchingProblemT::constraint_type MatchingConstraintT;	///< Type of the constraint applied to feature matching
		typedef typename FeatureMatchingProblemT::similarity_type SimilarityT;	///< Type of the feature similarity metric
		typedef typename FeatureMatchingProblemT::feature_type1 Feature1T;	///< Type of a feature in the first domain
		typedef typename FeatureMatchingProblemT::feature_type2 Feature2T;	///< Type of a feature in the second domain

		typedef typename MainSolverT::coordinate_type1 Coordinate1T;	///< Type of a coordinate in the first domain
		typedef typename MainSolverT::coordinate_type2 Coordinate2T;	///< Type of a coordinate in the second domain
		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CoordinateCorrespondenceContainerT;	///< A coordinate correspondence container

		constexpr static unsigned int nDim1=RowsM<Coordinate1T>::value;	///< Dimensionality of the first domain
		constexpr static unsigned int nDim2=RowsM<Coordinate2T>::value;	///< Dimensionality of the second domain
		typedef Matrix<RealT, nDim1, nDim1> Covariance1T;	///< Type of the covariance matrix for the first coordinate
		typedef Matrix<RealT, nDim2, nDim2> Covariance2T;	///< Type of the covariance matrix for the second coordinate
		typedef Matrix<RealT, nDim1+nDim2, nDim1+nDim2> CorrespondenceCovarianceT;	///< Type of the covariance for a correspondence

		typedef Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> MatrixT;	///< A floating point matrix

		/** @name Configuration */ //@{
		vector<Feature1T> featureSet1;	///< Features from the first domain
		vector<Covariance1T> covarianceList1;	///< Coordinate covariances for the features in the first domain

		vector<Feature2T> featureSet2;	///< Features from the second domain
		vector<Covariance2T> covarianceList2;	///< Coordinate covariances for the features in the second domain

		SimilarityT similarityMetric;	///< Feature similarity metric
		MatchingConstraintT shellMatchingConstraint;	///< Matching constraint without a constraining model
		optional<MatchingConstraintT> initialMatchingConstraint;	///< Matching constraint for the first pass
		//@}

		/** @name State */ //@{
		bool flagValid;	///< \c true if the object is valid
		optional<MatchingConstraintT> matchingConstraint;	///< Matching constraint
		GeometryEstimationProblemT geometryEstimationProblem;	///< Geometry estimation problem
		//@}

	public:

		/** @name GeometryEstimationPipelineProblemConceptC interface */ //@{

		typedef FeatureMatchingProblemT matching_problem_type;	///<  Type of the feature matching problem
		typedef GeometryEstimationProblemT geometry_estimation_problem_type;	///< Type of the geometry estimation problem

		typedef Covariance1T covariance_type1;	///< Type of the covariance matrix for the first coordinate
		typedef Covariance2T covariance_type2;	///< Type of the covariance matrix for the second coordinate

		GenericGeometryEstimationPipelineProblemC();	///< Default constructor

		bool IsValid() const;	///< Returns \c flagValid
		const vector<Feature1T>& FeatureSet1() const;	///< Returns a constant reference to \c featureSet1
		const vector<Feature2T>& FeatureSet2() const;	///< Returns a constant reference to \c featureSet2
		optional<MatchingConstraintT>& InitialMatchingConstraint();	///< Returns a reference to \c initialMatchingConstraint
		optional<MatchingConstraintT>& MatchingConstraint();	///< Returns a reference to \c matchingConstraint
		GeometryEstimationProblemT& GeometryEstimationProblem();	///< Returns a reference to \c geometryEstimationProblem
		SimilarityT& SimilarityMetric();	///< Returns a reference to \c similarityMetric

		void UpdateGeometryEstimationProblem(const optional<ModelT>& currentModel, const CoordinateCorrespondenceContainerT& oobservationSet, const CoordinateCorrespondenceContainerT& vvalidationSet);	///< Updates the geometry estimation problem
		void UpdateConstraint(const ModelT& model);	///< Updates the matching constraint

		static unsigned int GetMinimalGeneratorSize();	///< Returns the size of the minimal generator

		//SUT
		MatrixT MakeGeneratorCovariance(const CorrespondenceListT& generator) const;	///< Makes a generator covariance matrix
		//@}

		template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T, class CovarianceRange2T> GenericGeometryEstimationPipelineProblemC(const FeatureRange1T& ffeatureSet1, const FeatureRange2T& ffeatureSet2, const CovarianceRange1T& ccovarianceList1, const CovarianceRange2T& ccovarianceList2, const SimilarityT& ssimilarityMetric, const MatchingConstraintT& sshellMatchingConstraint, const optional<MatchingConstraintT>& iinitialMatchingConstraint, const GeometryEstimationProblemT& ggeometryEstimationProblem);	///< Constructor
};	//class GenericGeometryEstimationPipelineProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::GenericGeometryEstimationPipelineProblemC() : flagValid(false)
{}

/**
 * @brief Constructor
 * @tparam FeatureRange1T A feature range
 * @tparam FeatureRange2T A feature range
 * @tparam CovarianceRange1T A covariance range for the first set
 * @tparam CovarianceRange2T A covariance range for the second set
 * @param[in] ffeatureSet1 Features from the first domain
 * @param[in] ffeatureSet2 Features from the second domain
 * @param[in] ccovarianceList1 Coordinate covariances for the features in the first domain. If a single-element range, all elements are assumed to have the same covariance
 * @param[in] ccovarianceList2 Coordinate covariances for the features in the second domain. If a single-element range, all elements are assumed to have the same covariance
 * @param[in] ssimilarityMetric Feature similarity metric
 * @param[in] sshellMatchingConstraint Feature matching constraint. The relevant geometric entity is assigned within the algorithm.
 * @param[in] iinitialMatchingConstraint Initial matching constraint. Invalid, if none is available
 * @param[in] ggeometryEstimationProblem Geometry estimation problem
 * @pre \c FeatureRange1T is a forward range, holding objects of type \c feature_type1
 * @pre \c FeatureRange2T is a forward range, holding objects of type \c feature_type2
 * @pre \c CovarianceRange1T is a single-pass range, holding objects of type \c covariance_type1
 * @pre \c CovarianceRange2T is a single-pass range, holding objects of type \c covariance_type2
 * @pre \c ccovarianceRange1 either has a single element, or as many elements as \c ffeatureSet1
 * @pre \c ccovarianceRange2 either has a single element, or as many elements as \c ffeatureSet2
 * @post Valid object
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T, class CovarianceRange2T>
GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::GenericGeometryEstimationPipelineProblemC(const FeatureRange1T& ffeatureSet1, const FeatureRange2T& ffeatureSet2, const CovarianceRange1T& ccovarianceList1, const CovarianceRange2T& ccovarianceList2, const SimilarityT& ssimilarityMetric, const MatchingConstraintT& sshellMatchingConstraint, const optional<MatchingConstraintT>& iinitialMatchingConstraint, const GeometryEstimationProblemT& ggeometryEstimationProblem) : shellMatchingConstraint(sshellMatchingConstraint), initialMatchingConstraint(iinitialMatchingConstraint), geometryEstimationProblem(ggeometryEstimationProblem)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRange1T>));
	static_assert(is_same<typename boost::range_value<FeatureRange1T>::type, Feature1T >::value, "GenericGeometryEstimationPipelineProblemC::GenericGeometryEstimationPipelineProblemC : FeatureRange1T must hold elements of type feature_type1");

	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRange2T>));
	static_assert(is_same<typename boost::range_value<FeatureRange2T>::type, Feature2T >::value, "GenericGeometryEstimationPipelineProblemC::GenericGeometryEstimationPipelineProblemC : FeatureRange2T must hold elements of type feature_type2");

	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<CovarianceRange1T>));
	static_assert(is_same<typename boost::range_value<CovarianceRange1T>::type, covariance_type1 >::value, "GenericGeometryEstimationPipelineProblemC::GenericGeometryEstimationPipelineProblemC : CovarianceRange1T must hold elements of type covariance_type1");

	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<CovarianceRange2T>));
	static_assert(is_same<typename boost::range_value<CovarianceRange2T>::type, covariance_type2 >::value, "GenericGeometryEstimationPipelineProblemC::GenericGeometryEstimationPipelineProblemC : CovarianceRange2T must hold elements of type covariance_type2");

	assert(boost::distance(ccovarianceList1)==1 || boost::distance(ccovarianceList1)==boost::distance(ffeatureSet1));
	assert(boost::distance(ccovarianceList2)==1 || boost::distance(ccovarianceList2)==boost::distance(ffeatureSet2));

	size_t nFeature1=boost::distance( ffeatureSet1);
	featureSet1.resize(nFeature1);
	boost::copy(ffeatureSet1, featureSet1.begin());

	size_t nFeature2=boost::distance( ffeatureSet2);
	featureSet2.resize(nFeature2);
	boost::copy(ffeatureSet2, featureSet2.begin());

	covarianceList1.resize(boost::distance(ccovarianceList1));
	boost::copy(ccovarianceList1, covarianceList1.begin());

	covarianceList2.resize(boost::distance(ccovarianceList2));
	boost::copy(ccovarianceList2, covarianceList2.begin());

	flagValid=true;
}	//GenericGeometryEstimationPipelineProblemC(const FeatureRange1T& ffeatureSet1, const FeatureRange2T& ffeatureSet2, const feature_similarity_type& ssimilarityMetric, const matching_constraint_type& sshellMatchingConstraint, const optional<matching_constraint_type>& iinitialMatchingConstraint, const MapT& eerrorToScoreMap, const GeometricConstraintT& ggeometricConstraint, const RANSACProblem1T& rransacProblem1, const RANSACProblem2T& rransacProblem2, const OptimisationProblemT& ooptimisationProblem)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
bool GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Returns a constant reference to \c featureSet1
 * @return A constant reference to \c featureSet1
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::FeatureSet1() const -> const vector<Feature1T>&
{
	assert(flagValid);
	return featureSet1;
}	//const vector<feature_type1>& FeatureSet1() const

/**
 * @brief Returns a constant reference to \c featureSet2
 * @return A constant reference to \c featureSet2
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::FeatureSet2() const -> const vector<Feature2T>&
{
	assert(flagValid);
	return featureSet2;
}	//const vector<feature_type1>& FeatureSet2() const

/**
 * @brief Returns a reference to \c initialMatchingConstraint
 * @return A reference to \c initialMatchingConstraint
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::InitialMatchingConstraint() -> optional<MatchingConstraintT>&
{
	assert(flagValid);
	return initialMatchingConstraint;
}	//optional<matching_constraint_type>& InitialMatchingConstraint()

/**
 * @brief Returns a reference to \c matchingConstraint
 * @return A reference to \c matchingConstraint
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::MatchingConstraint() -> optional<MatchingConstraintT>&
{
	assert(flagValid);
	return matchingConstraint;
}	//optional<matching_constraint_type>& MatchingConstraint()

/**
 * @brief Returns a reference to \c geometryEstimationProblem
 * @return A reference to \c geometryEstimationProblem
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::GeometryEstimationProblem() -> GeometryEstimationProblemT&
{
	assert(flagValid);
	return geometryEstimationProblem;
}	//GeometryEstimationProblemT& GeometryEstimationProblem()

/**
 * @brief Returns a reference to \c similarityMetric
 * @return A reference to \c similarityMetric
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::SimilarityMetric() ->  SimilarityT&
{
	assert(flagValid);
	return similarityMetric;
}	//const feature_similarity_type& SimilarityMetric()

/**
 * @brief Updates the geometry estimation problem
 * @param[in] currentModel Current model. Invalid, if there is no estimate yet
 * @param[in] oobservationSet New observation set
 * @param[in] vvalidationSet New validation set
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
void GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::UpdateGeometryEstimationProblem(const optional<ModelT>& currentModel, const CoordinateCorrespondenceContainerT& oobservationSet, const CoordinateCorrespondenceContainerT& vvalidationSet)
{
	assert(flagValid);
	geometryEstimationProblem.Configure(oobservationSet, vvalidationSet, currentModel);
}	//void MakeGeometryEstimationProblem(const optional<result_type>& currentModel, const coordinate_correspondence_list_type& oobservationSet, const coordinate_correspondence_list_type& vvalidationSet)

/**
 * @brief Updates the matching constraint
 * @param[in] model Model for the constraint
 * @pre \c flagValid=true
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
void GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::UpdateConstraint(const ModelT& model)
{
	assert(flagValid);

	if(!matchingConstraint)
		matchingConstraint=shellMatchingConstraint;	//A new matching constraint

	matchingConstraint->SetProjector(model);	//Set the projector
}	//void  UpdateConstraint(const result_type& model)

/**
 * @brief Returns the size of the minimal generator
 * @return Size of the minimal generator
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
unsigned int GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::GetMinimalGeneratorSize()
{
	constexpr unsigned int sGenerator1=GeometryEstimationProblemT::ransac_problem_type1::minimal_solver_type::GeneratorSize();
	constexpr unsigned int sGenerator2=GeometryEstimationProblemT::ransac_problem_type2::minimal_solver_type::GeneratorSize();
	return max(sGenerator1, sGenerator2);
}	//unsigned int GetMinimalGeneratorSize()

/**
 * @brief Makes a generator covariance matrix
 * @param[in] generator Generator indices
 * @return Generator covariance matrix
 */
template<class GeometryEstimationProblemT, class FeatureMatchingProblemT>
auto GenericGeometryEstimationPipelineProblemC<GeometryEstimationProblemT, FeatureMatchingProblemT>::MakeGeneratorCovariance(const CorrespondenceListT& generator) const -> MatrixT
{
	size_t sGenerator=generator.size();
	size_t sOutput=sGenerator*(nDim1+nDim2);
	MatrixT output(sOutput, sOutput);

	bool flagUniform1=covarianceList1.size()==1; 	//Uniform covariance for all features in the first set
	bool flagUniform2=covarianceList2.size()==1; 	//Uniform covariance for all features in the second set

	size_t index=0;
	for(const auto& current : generator)
	{
		output.block(index, index, nDim1, nDim1)=covarianceList1[flagUniform1? 0:current.left];
		index+=nDim1;

		output.block(index, index, nDim2, nDim2)=covarianceList2[flagUniform2? 0:current.right];
		index+=nDim2;
	}	//for(size_t c=0; c<sGenerator; ++c)

	return output;
}	//auto MakeGeneratorCovariance(const CorrespondenceListT& generator) -> MatrixT


}	//GeometryN
}	//SeeSawN

#endif /* GENERIC_GEOMETRY_ESTIMATION_PIPELINE_PROBLEM_IPP_9023673 */
