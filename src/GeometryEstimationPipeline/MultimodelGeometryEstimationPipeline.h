/**
 * @file MultimodelGeometryEstimationPipeline.h Public interface for the multimodel geometry estimation pipeline
 * @author Evren Imre
 * @date 13 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_H_1701231
#define MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_H_1701231

#include "MultimodelGeometryEstimationPipeline.ipp"
namespace SeeSawN
{
namespace GeometryN
{

	struct MultimodelGeometryEstimationPipelineParametersC;	///< Parameters for MultimodelGeometryEstimationPipelineC
	struct MultimodelGeometryEstimationPipelineDiagnosticsC;	///< Diagnostics for MultimodelGeometryEstimationPipelineC

	template<class GeometryProblemT> class MultimodelGeometryEstimationPipelineC;	///<Pipeline for simultaneous estimation of multiple geometric entities supported by a correspondence set
}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template class std::vector<std::size_t>;
extern template class std::set<unsigned int>;

#endif /* MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_H_1701231 */
