/**
 * @file GeometryEstimationPipeline.h Public interface for GeometryEstimationPipelineC
 * @author Evren Imre
 * @date 17 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_ESTIMATION_PIPELINE_H_1230322
#define GEOMETRY_ESTIMATION_PIPELINE_H_1230322

#include "GeometryEstimationPipeline.ipp"
#include "GenericGeometryEstimationPipelineProblem.h"

namespace SeeSawN
{
namespace GeometryN
{

struct GeometryEstimationPipelineParametersC;	///< Parameters for GeometryEstimationPipelineC
struct GeometryEstimationPipelineDiagnosticsC;	///< Diagnostics for GeometryEstimationPipelineC

template<class ProblemT> class GeometryEstimationPipelineC;	///< Geometry estimation pipeline with guided matching

}	//GeometryN
}	//SeeSawN


/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template class std::vector<std::size_t>;
#endif /* GEOMETRY_ESTIMATION_PIPELINE_H_1230322 */
