/*
 * TestGeometryEstimationPipeline.cpp Unit tests for GeometyEstimationPipeline
 * @author Evren Imre
 * @date 29 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE GEOMETRY_ESTIMATION_PIPELINE

#include <boost/test/unit_test.hpp>
#include <boost/optional.hpp>
#include <boost/logic/tribool.hpp>
#include <Eigen/Dense>
#include <vector>
#include <cstddef>
#include <tuple>
#include <random>
#include "../GeometryEstimationComponents/Homography2DComponents.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Coordinate.h"
#include "../Elements/CorrespondenceDecimator.h"
#include "../Elements/GeometricEntity.h"
#include "../RANSAC/RANSAC.h"
#include "../Metrics/Similarity.h"
#include "CalibrationVerificationPipeline.h"
#include "MultimodelGeometryEstimationPipeline.h"
#include "VisionGraphPipeline.h"
#include "RotationRegistrationPipeline.h"
#include "SparseUnitSphereReconstructionPipeline.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestGeometryN
{

using namespace SeeSawN::GeometryN;

using boost::optional;
using std::vector;
using std::size_t;
using std::tie;
using std::mt19937_64;
using SeeSawN::GeometryN::RANSACHomography2DProblemT;
using SeeSawN::GeometryN::PDLHomography2DProblemT;
using SeeSawN::GeometryN::Homography2DEstimatorProblemT;
using SeeSawN::GeometryN::Homography2DEstimatorT;
using SeeSawN::GeometryN::Homography2DPipelineProblemT;
using SeeSawN::GeometryN::Homography2DPipelineT;
using SeeSawN::GeometryN::MakePDLHomography2DProblem;
using SeeSawN::GeometryN::MakeRANSACHomography2DProblem;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::CorrespondenceDecimatorC;
using SeeSawN::ElementsN::CorrespondenceDecimatorParametersC;
using SeeSawN::ElementsN::DecimatorQuantisationStrategyT;
using SeeSawN::RANSACN::RANSACParametersC;
using SeeSawN::RANSACN::RANSACC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;

BOOST_AUTO_TEST_SUITE(Geometry_Estimation_Problems)

BOOST_AUTO_TEST_CASE(Generic_Geometry_Estimation_Problem)
{
	//Data
	CoordinateCorrespondenceList2DT observationSet;
	CoordinateCorrespondenceList2DT validationSet;

	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;

	//Model1 : 2x+3, y+5
	//Clean inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(5, 6), MatchStrengthC(0.1, 0.1)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.3)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.35)));

	//Noisy inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));

	//Outliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,60), Coordinate2DT(11, 45), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,0), Coordinate2DT(30, 5), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,14), Coordinate2DT(90, 14), MatchStrengthC(0.1, 0.75)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5,0), Coordinate2DT(50, 0), MatchStrengthC(0.1, 0.85)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(310,65), Coordinate2DT(24,13), MatchStrengthC(0.1, 0.6)));

	//Validation
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(3,9), Coordinate2DT(9, 14), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(5,2), Coordinate2DT(13, 7), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(11,10), Coordinate2DT(25, 15), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(25,16), Coordinate2DT(53, 22), MatchStrengthC(0.1, 0.65)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(30,32), Coordinate2DT(63, 38), MatchStrengthC(0.1, 0.35)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.26)));	//Outlier
	validationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.61)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));	//Noisy

	//Sort the observations wrt/ambiguity

	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT> decimator;
	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>::dimension_type1 dummy1;
	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>::dimension_type2 dummy2;
	CorrespondenceDecimatorParametersC decimatorParameters;

	auto decimatedO=decimator.Run(observationSet, dummy1, dummy2, decimatorParameters);
	observationSet.swap(get<0>(decimatedO));

	auto decimatedV=decimator.Run(validationSet, dummy1, dummy2, decimatorParameters);
	validationSet.swap(get<0>(decimatedV));

	//Constructors

	Homography2DEstimatorProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	Homography2DSolverC::loss_type lossFunction(sqrt(5.99), sqrt(5.99));
	SymmetricTransferErrorH2DT errorMetric;
	SymmetricTransferErrorConstraint2DT constraint(errorMetric, sqrt(5.99), 1);

	optional<Homography2DT> referenceModel;

	Homography2DSolverC solver;
	Homography2DSolverC loSolver;
	RANSACHomography2DProblemT::dimension_type1 binSize1(10, 10);
	RANSACHomography2DProblemT::dimension_type2 binSize2(50, 50);
	RANSACHomography2DProblemT ransacProblem1(observationSet, validationSet, constraint, lossFunction, solver, loSolver, 0.25, 6, binSize1, binSize2);
	RANSACHomography2DProblemT ransacProblem2(ransacProblem1);

	PDLHomography2DProblemT pdlProblem(Homography2DT::Identity(), observationSet, solver, errorMetric);

	Homography2DEstimatorProblemT problem2(ransacProblem1, ransacProblem2, pdlProblem, referenceModel);
	BOOST_CHECK(problem2.IsValid());

	//Operations
	BOOST_CHECK(problem2.RANSACProblem1().IsValid());
	BOOST_CHECK(problem2.RANSACProblem2().IsValid());

	typedef RANSACC<RANSACHomography2DProblemT> RANSACT;
	RANSACT::result_type ransacResult;
	RANSACParametersC ransacParameters;
	RANSACT::rng_type rng;
	RANSACT::Run(ransacResult, problem2.RANSACProblem2(), rng, ransacParameters);

	vector<size_t> generator;
	vector<size_t> inliers;
	unsigned int bestIndex;
	RANSACHomography2DProblemT::model_type ransacModel;
	tie(ransacModel, bestIndex, inliers, generator)=problem2.ProcessRANSACResult(ransacResult);
	BOOST_CHECK_EQUAL(inliers.size(), 12);
	BOOST_CHECK_EQUAL(generator.size(), 4);
	BOOST_CHECK_EQUAL(bestIndex,0);

	pdlProblem=problem2.MakeOptimisationProblem(ransacModel, inliers);
	BOOST_CHECK(pdlProblem.IsValid());

	Homography2DT mH; mH<<2, 0, 3, 0, 1, 5, 0, 0, 1;
	double error=problem2.EvaluateModel(ransacModel);
	BOOST_CHECK_CLOSE(error, 16.33108, 1e-3);

	//Configure

	CoordinateCorrespondenceList2DT observationSet2;
	observationSet2.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(5, 6), MatchStrengthC(0.1, 0.1)));
	observationSet2.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.3)));
	observationSet2.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.15)));
	observationSet2.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.35)));

	CoordinateCorrespondenceList2DT validationSet2(observationSet2);
	validationSet2.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));

	problem2.Configure(observationSet2, validationSet2, mH);
	BOOST_CHECK_EQUAL(problem2.RANSACProblem1().GetObservationSetSize(),4);
	BOOST_CHECK_EQUAL(problem2.RANSACProblem1().GetValidationSetSize(),5);
}	//BOOST_AUTO_TEST_CASE(Generic_Geometry_Estimation_Problem)

BOOST_AUTO_TEST_SUITE_END()	//GeometryEstimationProblems

BOOST_AUTO_TEST_SUITE(Geometry_Estimation)

BOOST_AUTO_TEST_CASE(Geometry_Estimator)
{
	//Data
	CoordinateCorrespondenceList2DT observationSet;
	CoordinateCorrespondenceList2DT validationSet;

	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;

	//Model1 : 2x+3, y+5
	//Clean inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(5, 6), MatchStrengthC(0.1, 0.1)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.3)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.35)));

	//Noisy inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));

	//Outliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,60), Coordinate2DT(11, 45), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,0), Coordinate2DT(30, 5), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,14), Coordinate2DT(90, 14), MatchStrengthC(0.1, 0.75)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5,0), Coordinate2DT(50, 0), MatchStrengthC(0.1, 0.85)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(310,65), Coordinate2DT(24,13), MatchStrengthC(0.1, 0.6)));

	//Validation
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(3,9), Coordinate2DT(9, 14), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(5,2), Coordinate2DT(13, 7), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(11,10), Coordinate2DT(25, 15), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(25,16), Coordinate2DT(53, 22), MatchStrengthC(0.1, 0.65)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(30,32), Coordinate2DT(63, 38), MatchStrengthC(0.1, 0.35)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.26)));	//Outlier
	validationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.61)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));	//Noisy

	//Sort the observations wrt/ambiguity

	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT> decimator;
	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>::dimension_type1 dummy1;
	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>::dimension_type2 dummy2;
	CorrespondenceDecimatorParametersC decimatorParameters;

	auto decimatedO=decimator.Run(observationSet, dummy1, dummy2, decimatorParameters);
	observationSet.swap(get<0>(decimatedO));

	auto decimatedV=decimator.Run(validationSet, dummy1, dummy2, decimatorParameters);
	validationSet.swap(get<0>(decimatedV));

	//Problem

	Homography2DSolverC::loss_type lossFunction(sqrt(5.99), sqrt(5.99));
	SymmetricTransferErrorH2DT errorMetric;
	SymmetricTransferErrorConstraint2DT constraint(errorMetric, sqrt(5.99), 1);

	typedef Homography2DSolverC::model_type ModelT;
	optional<ModelT> referenceModel;

	Homography2DSolverC solver;
	Homography2DSolverC loSolver;
	RANSACHomography2DProblemT::dimension_type1 binSize1(10, 10);
	RANSACHomography2DProblemT::dimension_type2 binSize2(50, 50);
	RANSACHomography2DProblemT ransacProblem1(observationSet, validationSet, constraint, lossFunction, solver, loSolver, 0.25, 6, binSize1, binSize2);
	RANSACHomography2DProblemT ransacProblem2(ransacProblem1);

	PDLHomography2DProblemT pdlProblem(ModelT::Identity(), observationSet, solver, errorMetric);

	Homography2DEstimatorProblemT problem2(ransacProblem1, ransacProblem2, pdlProblem, referenceModel);

	//Solver

	Homography2DEstimatorT::rng_type rng;
	GeometryEstimatorParametersC parameters;
	Homography2DEstimatorT::result_type model;
	vector<size_t> inliers;
	vector<size_t> generator;
	GeometryEstimatorDiagnosticsC diagnostics=Homography2DEstimatorT::Run(model, inliers, generator, problem2, rng, parameters);

	BOOST_CHECK(diagnostics.flagSuccess);
	BOOST_CHECK_CLOSE(diagnostics.error, 13.019255732217296, 1e-3);
	BOOST_CHECK_EQUAL(inliers.size(), 12);
	BOOST_CHECK_EQUAL(generator.size(), 4);
}	//BOOST_AUTO_TEST_CASE(Geometry_Estimator)

BOOST_AUTO_TEST_SUITE_END()	//Geometry_Estimation


BOOST_AUTO_TEST_SUITE(Geometry_Estimation_Pipeline)

BOOST_AUTO_TEST_CASE(Geometry_Estimation_Pipeline_Call)
{
	//This is not a unit test. Just gauges how messy it is to set up a pipeline
	typedef typename Homography2DPipelineProblemT::geometry_estimation_problem_type GeometryEstimationProblemT;

	vector<Homography2DPipelineProblemT::matching_problem_type::feature_type1> featureSet1;
	vector<Homography2DPipelineProblemT::matching_problem_type::feature_type2> featureSet2;
	SymmetricTransferErrorConstraint2DT::projector_type initialH; initialH.setIdentity();

	RANSACHomography2DProblemT::dimension_type1 binSize1(10, 10);
	RANSACHomography2DProblemT::dimension_type2 binSize2(50, 50);

	GeometryEstimationProblemT::optimisation_problem_type optimisationProblem=MakePDLHomography2DProblem(initialH, CoordinateCorrespondenceList2DT());
	GeometryEstimationProblemT::ransac_problem_type1 ransacProblem1=MakeRANSACHomography2DProblem(CoordinateCorrespondenceList2DT(), CoordinateCorrespondenceList2DT(), 4, 0.05, 0.25, 8, binSize1, binSize2);
	GeometryEstimationProblemT::ransac_problem_type2 ransacProblem2(ransacProblem1);
	GeometryEstimationProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialH);

	Homography2DPipelineProblemT problem=MakeGeometryEstimationPipelineHomography2DProblem(featureSet1, featureSet2, geometryEstimationProblem, 1, 0.05, initialH, 4, 0.05, 1);

	GeometryEstimationPipelineDiagnosticsC diagnostics;
	GeometryEstimationPipelineParametersC parameters;
	Homography2DEstimatorT::result_type model;
	CorrespondenceListT inliers;
	optional<MatrixXd> covariance;
	Homography2DPipelineT::rng_type rng;
	diagnostics=Homography2DPipelineT::Run(model, covariance, inliers, problem, rng, parameters);
}	//BOOST_AUTO_TEST_CASE(Geometry_Estimation_Pipeline_Call)

BOOST_AUTO_TEST_SUITE_END()	//Geometry_Estimation_Pipeline

BOOST_AUTO_TEST_SUITE(Multimodel_Geometry_Estimation_Pipeline)

BOOST_AUTO_TEST_CASE(MGEP_Compilation_Test)
{
	//Initialise the problems

	vector<Homography2DPipelineProblemT::matching_problem_type::feature_type1> featureSet1;
	vector<Homography2DPipelineProblemT::matching_problem_type::feature_type2> featureSet2;
	SymmetricTransferErrorConstraint2DT::projector_type initialH; initialH.setIdentity();

	RANSACHomography2DProblemT::dimension_type1 binSize1(10, 10);
	RANSACHomography2DProblemT::dimension_type2 binSize2(50, 50);

	typedef typename Homography2DPipelineProblemT::geometry_estimation_problem_type GeometryEstimationProblemT;
	GeometryEstimationProblemT::optimisation_problem_type optimisationProblem=MakePDLHomography2DProblem(initialH, CoordinateCorrespondenceList2DT());
	GeometryEstimationProblemT::ransac_problem_type1 ransacProblem1=MakeRANSACHomography2DProblem(CoordinateCorrespondenceList2DT(), CoordinateCorrespondenceList2DT(), 4, 0.05, 0.25, 8, binSize1, binSize2);
	GeometryEstimationProblemT::ransac_problem_type2 ransacProblem2(ransacProblem1);
	GeometryEstimationProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialH);

	Homography2DPipelineProblemT geometryEstimationPipelineProblem=MakeGeometryEstimationPipelineHomography2DProblem(featureSet1, featureSet2, geometryEstimationProblem, 1, 0.05, initialH, 4, 0.05, 1);

	typedef MultimodelGeometryEstimationPipelineC<Homography2DPipelineProblemT> PipelineT;

	MultimodelGeometryEstimationPipelineParametersC parameters;

	MultimodelGeometryEstimationPipelineDiagnosticsC diagnostics;
	vector<PipelineT::solution_type> solutions;
	PipelineT::problem_type problem=make_tuple(ransacProblem2, geometryEstimationPipelineProblem);
	PipelineT::rng_type rng;

	diagnostics=PipelineT::Run(solutions, problem, rng, parameters);
}	//BOOST_AUTO_TEST_CASE(MGEP_Compilation_Test)

BOOST_AUTO_TEST_SUITE_END()	//BOOST_AUTO_TEST_SUITE(Multimodel_Geometry_Estimation_Pipeline)

BOOST_AUTO_TEST_SUITE(Calibration_Verification_Pipeline)

BOOST_AUTO_TEST_CASE(CVP_Compilation_Test)
{
	CalibrationVerificationPipelineC engine1;
	vector<CalibrationVerificationPipelineC::result_type> labels;
	CalibrationVerificationPipelineDiagnosticsC diagnostics=engine1.Run(labels, vector<CameraMatrixT>(), vector<vector<ImageFeatureC>>(), set<size_t>(), InverseEuclideanIDConverterT(), CalibrationVerificationPipelineParametersC());
	(void)diagnostics;
}	//BOOST_AUTO_TEST_CASE(Compilation_Test)

BOOST_AUTO_TEST_SUITE_END()	//Calibration_Verification_Pipeline

BOOST_AUTO_TEST_SUITE(PfM_Pipeline)

BOOST_AUTO_TEST_CASE(Vision_Graph_Pipeline)
{
	typedef VisionGraphPipelineC<Homography2DPipelineProblemT> EngineHT;

	mt19937_64 rng;
	typename EngineHT::vision_graph_type visionGraph(0);
	typename EngineHT::edge_container_type edgeList;
	VisionGraphPipelineDiagnosticsC diagnostics=EngineHT::Run( visionGraph, edgeList, rng, vector<vector<ImageFeatureC>>(),  map<size_t, CameraMatrixT>(), vector<double>(), VisionGraphPipelineParametersC() );

}	//BOOST_AUTO_TEST_CASE(Vision_Graph_Pipeline)

BOOST_AUTO_TEST_CASE(Rotation_Registration_Pipeline)
{
	vector<RotationMatrix3DT> mRList(4);
	mRList[0]=QuaternionT(0.85154,0.0616971,-0.519809,-0.029535).matrix();
	mRList[1]=QuaternionT(0.953864, 0.0283951, -0.298819, -0.00665323).matrix();
	mRList[2]=QuaternionT(0.988633, 0.0457148, -0.143168, -0.00410147).matrix();
	mRList[3]=QuaternionT(0.996071, 0.0144022, 0.0872254, -0.00511926 ).matrix();

	vector<RelativeRotationRegistrationC::observation_type> observations; observations.reserve(6);
	for(size_t c1=0; c1<4; ++c1)
		for(size_t c2=c1+1; c2<4; ++c2)
		{
			RotationMatrix3DT mR=mRList[c2]*mRList[c1].transpose();
			mR+=(1e-5)*RotationMatrix3DT::Random();

			if(c1==0 && c2==1)	//Outlier
				mR+=RotationMatrix3DT::Random();

			mR=ComputeClosestRotationMatrix(mR);
			observations.emplace_back(c1, c2, mR, c1*c2+c2+1);
		}	//for(size_t c2=c1+1; c2<4; ++c2)

	//Default operation
	RotationRegistrationPipelineC::result_type model;
	RotationRegistrationPipelineC::index_container_type inliers;
	RandomSpanningTreeSamplerC<RSTSRotationRegistrationProblemC>::rng_type rng;

	RotationRegistrationPipelineParametersC parameters;
	RotationRegistrationPipelineDiagnosticsC diagnostics=RotationRegistrationPipelineC::Run(model, inliers, rng, observations, parameters);

	BOOST_CHECK_EQUAL(model.size(),4);
	BOOST_CHECK(diagnostics.flagSuccess);
	BOOST_CHECK(diagnostics.flagSuccessLS);
	BOOST_CHECK_CLOSE(diagnostics.error, 0.20011, 1e-2);
	BOOST_CHECK_EQUAL(diagnostics.inlierRatio, 5.0/6.0);

	vector<size_t> inliersR{1,2,3,4,5};
	BOOST_CHECK_EQUAL_COLLECTIONS(inliers.begin(), inliers.end(), inliersR.begin(), inliersR.end());

	//Invalid input
	vector<RelativeRotationRegistrationC::observation_type> observations2;
	BOOST_CHECK_THROW(RotationRegistrationPipelineC::Run(model, inliers, rng, observations2, parameters), invalid_argument);
}	//BOOST_AUTO_TEST_CASE(Rotation_Registration_Pipeline)

BOOST_AUTO_TEST_SUITE_END()	//PfM_Pipeline

BOOST_AUTO_TEST_SUITE(Sparse_Unit_Sphere_Reconstruction_Pipeline)

BOOST_AUTO_TEST_CASE(Sparse_Unit_Sphere_Reconstruction_Pipeline_Compilation_Test)
{
	vector<Coordinate3DT> pointCloud;
	vector<OrientedBinarySceneFeatureC> sceneFeatures;
	SparseUnitSphereReconstructionPipelineC::Run<InverseHammingBIDConverterT>(pointCloud, sceneFeatures, vector<CameraMatrixT>(), vector<vector<BinaryImageFeatureC>>(), InverseHammingBIDConverterT(), SparseUnitSphereReconstructionPipelineParametersC());
}	//Sparse_Unit_Sphere_Reconstruction_Pipeline_Compilation_Test

BOOST_AUTO_TEST_SUITE_END()//Sparse_Unit_Sphere_Reconstruction_Pipeline


}	//TestGeometryN
}	//UnitTestN
}	//SeeSawN

