/**
 * @file Rotation.ipp Implementation of various rotation-related functions
 * @author Evren Imre
 * @date 9 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROTATION_IPP_3093243
#define ROTATION_IPP_3093243

#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/optional.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <type_traits>
#include <tuple>
#include <iterator>
#include <cmath>
#include <climits>
#include "../Elements/GeometricEntity.h"
#include "../Numeric/LinearAlgebraJacobian.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::optional;
using boost::make_zip_iterator;
using boost::math::pow;
using boost::math::constants::two_pi;
using boost::math::constants::pi;
using Eigen::Matrix;
using Eigen::Matrix3d;
using Eigen::SelfAdjointEigenSolver;
using std::tuple;
using std::make_tuple;
using std::is_arithmetic;
using std::is_same;
using std::advance;
using std::remainder;
using std::numeric_limits;
using std::fabs;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::AxisAngleT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::QuaternionToVector;
using SeeSawN::NumericN::JacobianNormalise;

//Forward declarations
RotationVectorT QuaternionToRotationVector(QuaternionT q);

/**
 * @brief Computes the sample statistics for a set of quaternions
 * @tparam QuaternionRangeT A range of quaternions
 * @tparam WeightRangeT A range of arithmetic values
 * @param[in] quaternions Quaternions
 * @param[in] wMean Sample weights for the computation of the mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @return A tuple, mean and covariance
 * @pre \c QuaternionRangeT is a forward range of elements of type \c QuaternionT
 * @pre \c WeightRangeT is a single pass range of arithmetic elements
 * @pre Elements of \c wMean and \c wCovariance are non-negative. \c wCovariance sums up to 1 (unenforced)
 * @pre \c quaternions , \c wMean and \c wCovariance have the same number of elements
 * @post Mean is a unit quaternion
 * @remarks For the purposes of covariance computation, the rotations are represented as rotation vectors.
 * @remarks F. L. Markley, Y. Cheng, J. L. Crassidis, Y. Oshman, "Quaternion Averaging, " Technical Report 20070017182, NASA Goddard Space Flight Center, Greenbelt, MD, 2007, ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.../20070017872_2007014421.pdf (visited 08.03.2010)
 * @remarks F. L. Markley, "Attitude Error Representations for Kalman Filtering, " Journal of Guidance, Control, and Dynamics, Vol. 26, No. 2, pp. 311-317 March 2003
 * @warning Unless \c quaternions has 3 distinct elements, the resulting covariance matrix is rank-deficient
 * @ingroup Geometry
 */
template<class QuaternionRangeT, class WeightRangeT>
tuple<QuaternionT, typename QuaternionT::Matrix3> ComputeQuaternionSampleStatistics(const QuaternionRangeT& quaternions, const WeightRangeT& wMean, const WeightRangeT& wCovariance)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<QuaternionRangeT>));
	static_assert(is_same<QuaternionT, typename range_value<QuaternionRangeT>::type>::value, "ComputeQuaternionSampleStatistics: QuaternionRangeT holds elements of type QuaternionT");
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<WeightRangeT>));
	static_assert(is_arithmetic< typename range_value<WeightRangeT>::type>::value, "ComputeQuaternionSampleStatistics: WeightRangeT holds arithmetic elements" );

	assert(boost::distance(quaternions)==boost::distance(wMean));
	assert(boost::distance(quaternions)==boost::distance(wCovariance));

	//Mean

	//Compute B (Eq.13)
	typedef typename ValueTypeM<typename QuaternionT::Matrix3>::type RealT;
	Matrix<RealT, 4, 4> mB; mB.setZero();
	auto itM=make_zip_iterator(boost::make_tuple(boost::const_begin(quaternions), boost::const_begin(wMean) ) );
	auto itEM=make_zip_iterator(boost::make_tuple(boost::const_end(quaternions), boost::const_end(wMean) ) );
	for(; itM!=itEM; advance(itM,1))
	{
		assert(*boost::get<1>(itM.get_iterator_tuple()) >=0 );

		Matrix<RealT, 4, 1> temp=QuaternionToVector(*boost::get<0>(itM.get_iterator_tuple())) ; temp.normalize();
		mB+= *boost::get<1>(itM.get_iterator_tuple())* (temp * temp.transpose());
	}	//for(; it!=itE; advance(it,1))

	//Maximum eigenvector
	SelfAdjointEigenSolver<Matrix<RealT, 4, 4> > eigenSolver(mB, Eigen::ComputeEigenvectors);
	Matrix<RealT, 4, 1> maxEigenvector =eigenSolver.eigenvectors().col(3) * ( (eigenSolver.eigenvectors().col(3)[0]) ? 1:-1 );

	maxEigenvector.normalize();	//Ensure that the quaternion has unit norm
	QuaternionT qMean(maxEigenvector(0), maxEigenvector(1), maxEigenvector(2), maxEigenvector(3));

	//Covariance
	Matrix<RealT, 3, 3> mCov; mCov.setZero();
	auto itC=make_zip_iterator(boost::make_tuple(boost::const_begin(quaternions), boost::const_begin(wCovariance) ) );
	auto itEC=make_zip_iterator(boost::make_tuple(boost::const_end(quaternions), boost::const_end(wCovariance) ) );
	for(; itC!=itEC; advance(itC,1))
	{
		assert(*boost::get<1>(itC.get_iterator_tuple())>=0);
		QuaternionT qDev=boost::get<0>(itC.get_iterator_tuple())->normalized() * qMean.conjugate();	//Deviation from the mean, R1, Eq.14
		typename QuaternionT::Vector3 vRot=QuaternionToRotationVector(qDev);
		mCov+= *boost::get<1>(itC.get_iterator_tuple()) * (vRot*vRot.transpose());
	}	//for(; itC!=itEC; advance(itC,1))

	return make_tuple(qMean, mCov);
}	//tuple<QuaternionT, typename QuaternionT::Matrix3> ComputeQuaternionSampleStatistics(const QuaternionRangeT& quaternions, const WeightRangeT& wMean, const WeightRangeT& wCovariance)


}	//GeometryN
}	//SeeSawN

#endif /* ROTATION_IPP_3093243 */
