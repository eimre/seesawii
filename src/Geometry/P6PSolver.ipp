/**
 * @file P6PSolver.ipp Implementation of the 6-point projective camera solver
 * @author Evren Imre
 * @date 4 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P6PSOLVER_IPP_3879123
#define P6PSOLVER_IPP_3879123

#include <Eigen/Dense>
#include <list>
#include <tuple>
#include <vector>
#include "GeometrySolverBase.h"
#include "DLT.h"
#include "Rotation.h"
#include "Rotation3DSolver.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricError.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Wrappers/BoostRange.h"

namespace SeeSawN
{
namespace GeometryN
{

struct CameraMinimalDifferenceC;	//Forward declaration

using Eigen::Matrix;
using std::list;
using std::tuple;
using std::tie;
using std::vector;
using SeeSawN::GeometryN::DLTC;
using SeeSawN::GeometryN::DLTProblemC;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::GeometryN::Rotation3DMinimalDifferenceC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::WrappersN::MakeBinaryZipRange;

/**
 * @brief 6-point projective camera solver
 * @remarks R. Hartley, A. Zisserman, Multiple View Geometry, 2nd Ed, 2003, pp.181
 * @remarks 6-point normalised DLT
 * @remarks Degeneracy: Twisted cubic;  union of a plane and a line including the camera centre ( Haryley and Zisserman, Chapter 22)
 * @remarks Distance measure: 1-Absolute value of the dot-product between the unit vectors representing the homographies. Range=[0,1]
 * @remarks Minimal parameterisation: Decomposition into intrinsic and extrinsic calibration parameters.
 * @warning A valid problem has at least 6 constraints
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class P6PSolverC : public GeometrySolverBaseC<P6PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 6, true, 1, 12, 11>
{
	private:

		typedef GeometrySolverBaseC<P6PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 6, true, 1, 12, 11> BaseT;	///< Type of the base

		typedef Matrix<double, BaseT::nParameter, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
																										// 12x12, so that we do not waste a constraint that is already available
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 3x4 homography

		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::Homography32Tag, BaseT::sGenerator, 3, 3> DLTProblemT;	///< DLT problem for 3D homography estimation

		typedef MultivariateGaussianC<CameraMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the 3D-2D homography that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<IntrinsicC, Coordinate3DT, RotationVectorT> minimal_model_type;	///< A minimally parameterised model
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class CameraMatrixRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const CameraMatrixRangeT& cameraMatrixList, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of camera matrices
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iIntrinsics=0;	///< Index of the intrinsic calibration parameters
		static constexpr unsigned int iPosition=1;	///< Index of the camera centre
		static constexpr unsigned int iOrientation=2;	///< Index of the orientation
		//@}
};	//class P6PSolverC

/**
 * @brief  Difference functor for minimally-represented camera matrices
 * @remarks A model of adaptable binary function concept
 * @remarks Algorithm: Decompose the camera matrix into intrinsic and extrinsic calibration elements. Then, rotation difference for the orientation component, and normal vector difference for the rest
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct CameraMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef P6PSolverC::minimal_model_type first_argument_type;
	typedef P6PSolverC::minimal_model_type second_argument_type;
	typedef Matrix<P6PSolverC::real_type, P6PSolverC::DoF(), 1> result_type;	///< Difference

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2) const;	///< Computes the difference between two minimally-represented 3D homographies
	//@}
};	//struct Homography3DMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre \c model has non-zero norm
 * @return Minimal form
 */
template<class DummyRangeT>
auto P6PSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	//Decompose the camera matrix
	IntrinsicCalibrationMatrixT mK;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(mK, vC, mR)=DecomposeCamera(model, false);

	CameraC camera;
	camera.SetIntrinsics(mK);	//mK to IntrinsicsC conversion

	return minimal_model_type( *camera.Intrinsics(), vC, RotationMatrixToRotationVector(mR));
}	//auto Homography3DSolverC::MakeMinimal(const ModelT& model)

/**
 * @brief Computes the sample statistics for a set of camera matrices
 * @tparam CameraMatrixRangeT A range of camera matrices
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] cameraMatrixList Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c CameraMatrixRangeT is a forward range of elements of type \c CameraMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c cameraMatrixList should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c cameraMatrixList has 11 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class CameraMatrixRangeT, class WeightRangeT, class DummyRangeT>
auto P6PSolverC::ComputeSampleStatistics(const CameraMatrixRangeT& cameraMatrixList, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CameraMatrixRangeT>));
	static_assert(is_same<ModelT, typename range_value<CameraMatrixRangeT>::type >::value, "P6PSolverC::ComputeSampleStatistics : CameraMatrixRangeT must hold elements of type CameraMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "P6PSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(cameraMatrixList)==boost::distance(wMean));
	assert(boost::distance(cameraMatrixList)==boost::distance(wCovariance));

	size_t nSamples=boost::distance(cameraMatrixList);

	//Mean: Decompose the camera matrix

	vector<minimal_model_type> minimalList; minimalList.reserve(nSamples);

	IntrinsicC meanIntrinsics;
	meanIntrinsics.focalLength=0; meanIntrinsics.principalPoint=Coordinate2DT::Zero();

	Coordinate3DT meanPosition=Coordinate3DT::Zero();
	vector<QuaternionT> qList; qList.reserve(nSamples);
	for(const auto& current : MakeBinaryZipRange(cameraMatrixList, wMean) )
	{
		minimal_model_type minimalRep=MakeMinimal(boost::get<0>(current));

		qList.push_back(RotationVectorToQuaternion(get<iOrientation>(minimalRep)));
		meanPosition += get<iPosition>(minimalRep) * boost::get<1>(current);

		(*meanIntrinsics.focalLength) += *get<iIntrinsics>(minimalRep).focalLength * boost::get<1>(current);
		meanIntrinsics.aspectRatio += get<iIntrinsics>(minimalRep).aspectRatio * boost::get<1>(current);
		meanIntrinsics.skewness += get<iIntrinsics>(minimalRep).skewness * boost::get<1>(current);
		meanIntrinsics.principalPoint->tail(2) += *get<iIntrinsics>(minimalRep).principalPoint * boost::get<1>(current);

		minimalList.push_back(minimalRep);
	}	//for(const auto& current : MakeBinaryZipRange(cameraMatrixList, wMean) )

	//Mean orientation
	QuaternionT qMean;
	Matrix<RealT, 3, 3> mCovQ;
	tie(qMean, mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	minimal_model_type mean(meanIntrinsics, meanPosition, QuaternionToRotationVector(qMean));

	//Covariance
	CameraMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	for(const auto& current : MakeBinaryZipRange(minimalList, wCovariance) )
	{
		typename CameraMinimalDifferenceC::result_type dif=subtractor(boost::get<0>(current), mean);
		mCov += boost::get<1>(current) * (dif*dif.transpose());
	}	//for(const auto& current : MakeBinaryZipRange(cameraMatrixList, wCovriance) )

	return GaussianT(mean, mCov);
}	//ComputeSampleStatistics(const CameraMatrixRangeT& cameraMatrixList, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT

/**
 * @brief Computes the camera matrix that best explains the correspondence set
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding the camera matrix. An empty set if DLT fails, or \c correspondences has too few elements
 * @post The solution is unit norm
 */
template<class CorrespondenceRangeT>
auto P6PSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	return DLTC<DLTProblemT>::Run(correspondences);
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT
}	//GeometryN
}	//SeeSawN

#endif /* P6PSOLVER_IPP_3879123 */
