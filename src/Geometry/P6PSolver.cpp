/**
 * @file P6PSolver.cpp Implementation of the 6-point projective camera solver
 * @author Evren Imre
 * @date 4 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P6PSolver.h"

namespace SeeSawN
{
namespace GeometryN
{

/********** P6PSolverC **********/

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double P6PSolverC::Cost(unsigned int nCorrespondences)
{
	return DLTC<DLTProblemT>::Cost(max(nCorrespondences, sGenerator));
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Camera matrix matrix
 */
auto P6PSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	ExtrinsicC extrinsic;
	extrinsic.position=get<iPosition>(minimalModel);
	extrinsic.orientation=RotationVectorToQuaternion(get<iOrientation>(minimalModel));

	return *CameraC(extrinsic, get<iIntrinsics>(minimalModel)).MakeCameraMatrix();	//Cannot fail as both the intrinsic and the extrinsic calibration parameters are defined
}	//ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel)

/********** CameraMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented camera matrices
 * @param[in] op1 Minuend Minuend
 * @param[in] op2 Subtrahend Subtrahend
 * @return Difference vector
 * @remarks Difference: Rotation difference for the orientation, vector difference for the rest
 */
auto CameraMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) const -> result_type
{
	result_type output;

	Rotation3DMinimalDifferenceC rDiff;
	output.segment(5,3)=rDiff(get<P6PSolverC::iOrientation>(op1), get<P6PSolverC::iOrientation>(op2) );

	output.tail(3)=get<P6PSolverC::iPosition>(op1)-get<P6PSolverC::iPosition>(op2);

	output[0]= *get<P6PSolverC::iIntrinsics>(op1).focalLength - *get<P6PSolverC::iIntrinsics>(op2).focalLength;
	output[1]= get<P6PSolverC::iIntrinsics>(op1).aspectRatio - get<P6PSolverC::iIntrinsics>(op2).aspectRatio;
	output[2]= get<P6PSolverC::iIntrinsics>(op1).skewness - get<P6PSolverC::iIntrinsics>(op2).skewness;
	output.segment(3,2)= *get<P6PSolverC::iIntrinsics>(op1).principalPoint - *get<P6PSolverC::iIntrinsics>(op2).principalPoint;

	return output;
}	//operator()(const first_argument_type& op1, const second_argument_type& op2) const -> result_type

}	//GeometryN
}	//SeeSawN

