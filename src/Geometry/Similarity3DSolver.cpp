/**
 * @file Similarity3DSolver.cpp Implementation of the solver for a 3D similarity transformation
 * @author Evren Imre
 * @date 26 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Similarity3DSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double Similarity3DSolverC::Cost(unsigned int nCorrespondences)
{
	bool flagDouble=is_same<RealT, double>::value;
	double costRotation=Rotation3DSolverC::Cost(nCorrespondences);
	double costNormaliser= ComplexityEstimatorC::VectorSampleVariance(3, nCorrespondences, flagDouble) +  ComplexityEstimatorC::VectorSampleVariance(3, nCorrespondences, flagDouble);
	double costNormalise= nCorrespondences * (ComplexityEstimatorC::MatrixVectorMultiplication(4, 4, flagDouble) + ComplexityEstimatorC::MatrixVectorMultiplication(4, 4, flagDouble));

	return 2*(costNormaliser+costNormalise)+costRotation;
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return 4x4 Homography matrix
 */
auto Similarity3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	AxisAngleT aa=RotationVectorToAxisAngle(get<iOrientation>(minimalModel));
	return ComposeSimilarity3D(get<iScale>(minimalModel), get<iCenter>(minimalModel), aa.matrix());
}	//auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return \f$ 1- \frac{|<\mathbf{M_1, M_2}>|}{\sqrt{<\mathbf{M_1,M_1}><\mathbf{M_2,M_2}>}} \f$. [0,1]
 */
auto Similarity3DSolverC::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	Matrix<RealT, 3, 4> m1=model1.topRows(3)/model1(3,3);
	Matrix<RealT, 3, 4> m2=model2.topRows(3)/model2(3,3);

	//Since the entities are not homogeneous, the dot-product trick does not work. Schwartz to the rescue!
	return 1-fabs((m1.cwiseProduct(m2)).sum())/(m1.norm()*m2.norm());
}	//auto ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type

/**
 * @brief Converts a model to a vector
 * @param[in] model Model
 * @return First 3x4 submatrix of \c model in vector form
 */
auto Similarity3DSolverC::MakeVector(const ModelT& model) -> VectorT
{
	return Reshape<VectorT>( model.block(0,0,3,4)/model(3,3), 12, 1);
}	//VectorT MakeVector(const ModelT& model)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel Vector
 * @param[in] dummy Dummy parameter to satisfy the concept requirements
 * @return A 4x4 homography
 * @pre \c vModel is a 12-element vector representing a similarity transformation matrix
 */
auto Similarity3DSolverC::MakeModel(const VectorT& vModel, bool dummy) -> ModelT
{
	//Preconditions
	assert(vModel.size()==12);
	ModelT output; output.row(3)<<0,0,0,1;
	size_t i=0;
	for(size_t c=0; c<12; ++i, c+=4)
	{
		output.row(i).segment(0,3)=vModel.segment(c,3).transpose();
		output(i,3)=vModel(c+3);
	}	//for(size_t c=0; c<12; ++i, c+=4)

	return output;
}	//ModelT MakeModel(const VectorT& vModel)

/********** Similarity3DMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented similarity transformations
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector. [scale; Rotation vector(3); origin shift(3) ]
 * @remarks Difference: scale difference (1); orientation and the origin of the transformation that takes \c op2 to \c op1 (6).
 */
auto Similarity3DMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	result_type output;
	output[0]=get<Similarity3DSolverC::iScale>(op1)-get<Similarity3DSolverC::iScale>(op2);

	Rotation3DMinimalDifferenceC rotationDif;
	output.segment(1,3)=rotationDif(get<Similarity3DSolverC::iOrientation>(op1), get<Similarity3DSolverC::iOrientation>(op2));
	output.segment(4,3)=get<Similarity3DSolverC::iCenter>(op1)-get<Similarity3DSolverC::iCenter>(op2);

	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

