/**
 * @file CoordinateTransformation.cpp
 * @author Evren Imre
 * @date 24 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CoordinateTransformation.h"

namespace SeeSawN
{
namespace GeometryN
{

/********** EXPLICIT INSTANTIATIONS **********/

template class CoordinateTransformationsC<typename ValueTypeM<Coordinate2DT>::type, 2>;
template class CoordinateTransformationsC<typename ValueTypeM<Coordinate3DT>::type, 3>;

template typename CoordinateTransformations2DT::AffineTransformT CoordinateTransformations2DT::ComputeNormaliser(const vector<Coordinate2DT>&, bool);
template typename CoordinateTransformations3DT::AffineTransformT CoordinateTransformations3DT::ComputeNormaliser(const vector<Coordinate3DT>&, bool);

template vector<HCoordinate2DT> CoordinateTransformations2DT::ApplyProjectiveTransformation(const vector<Coordinate2DT>&, const typename CoordinateTransformations2DT::ProjectiveTransformT&);

template vector<Coordinate2DT> CoordinateTransformations2DT::ApplyAffineTransformation(const vector<Coordinate2DT>&, const typename CoordinateTransformations2DT::AffineTransformT&);
template vector<Coordinate2DT> CoordinateTransformations2DT::NormaliseHomogeneous(const vector<HCoordinate2DT>&);
template vector<vector<unsigned int> > CoordinateTransformations2DT::Bucketise(const vector<Coordinate2DT>&, const typename CoordinateTransformations2DT::ArrayD&, const typename CoordinateTransformations2DT::ArrayD2&, bool);


template vector<vector<unsigned int> > CoordinateTransformations3DT::Bucketise(const vector<Coordinate3DT>&, const typename CoordinateTransformations3DT::ArrayD&, const typename CoordinateTransformations3DT::ArrayD2&, bool);

}   //GeometryN
}   //SeeSawN

