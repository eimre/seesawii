/**
 * @file OneSidedFundamentalSolver.cpp Implementation of the 6-point one-sided fundamental matrix solver
 * @author Evren Imre
 * @date 13 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "OneSidedFundamentalSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** OneSidedFundamentalMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented one-sided fundamental matrices
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector
 */
auto OneSidedFundamentalMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	//Difference of the essential matrix components
	OneSidedFundamentalSolverC::EssentialMinimalT e1=OneSidedFundamentalSolverC::ExtractMinimalEssential(op1);
	OneSidedFundamentalSolverC::EssentialMinimalT e2=OneSidedFundamentalSolverC::ExtractMinimalEssential(op2);

	EssentialMinimalDifferenceC subE;
	EssentialMinimalDifferenceC::result_type difE=subE(e1,e2);

	result_type output;
	output.head(difE.size())=difE;
	output[difE.size()]=get<OneSidedFundamentalSolverC::iFocalLength>(op1)-get<OneSidedFundamentalSolverC::iFocalLength>(op2);

	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

/********** OneSidedFundamentalSolverC **********/

/**
 * @brief Extracts the essential matrix component from a minimal model
 * @param[in] minimalModel Minimal model
 * @return Minimal model for the essential matrix component
 */
auto OneSidedFundamentalSolverC::ExtractMinimalEssential(const minimal_model_type& minimalModel) -> EssentialMinimalT
{
	EssentialMinimalT output;
	get<EssentialSolverC::iRotation>(output)=get<iRotation>(minimalModel);
	get<EssentialSolverC::iAzimuth>(output)=get<iAzimuth>(minimalModel);
	get<EssentialSolverC::iElevation>(output)=get<iElevation>(minimalModel);

	return output;
}	//EssentialMinimalT ExtractMinimalEssential(const minimal_model_type)

/**
 * @brief Computes the one-sided fundamental matrices and focal lengths from a basis
 * @param[in] basis Basis
 * @return List of models. Empty if the solver fails
 * @remarks The solution does not enforce the internal constrains of the solution vector exactly, i.e., the entry corresponding to x^3 is not necessarily the third power of the entry for x.
 */
auto OneSidedFundamentalSolverC::ModelFromBasis(const list<ModelT>& basis) -> list<SolutionT>
{
	//Preconditions
	assert(basis.size()==3);

	list<SolutionT> output;
	auto itBasis=basis.begin();
	const EpipolarMatrixT &mB1=*itBasis; advance(itBasis,1);
	const EpipolarMatrixT &mB2=*itBasis; advance(itBasis,1);
	const EpipolarMatrixT &mB3=*itBasis;

	Matrix10T mC1;
	Matrix10T mC0;
	tie(mC0, mC1)=ComputeCoefficients(basis);

	//R1 Eq 11: (C1w+C0)v=0
	//GeneralizedEigenSolver<Matrix10T> evd(-mC0, mC1);	//R1 Eq9 //FIXME Eigen's GeneralizedEigenSolver does not extract the eigenvectors

	Matrix10T mC=mC0.inverse()*mC1;
	EigenSolver<Matrix10T> evd(-mC);	//Equivalent problem, but less stable: (-C0)^-1 * C1 v = wv

	if(evd.info()!=Eigen::Success)
		return output;

//	GeneralizedEigenSolver<Matrix10T>::EigenvectorsType mV=evd.eigenvectors();
	const EigenSolver<Matrix10T>::EigenvectorsType& mV=evd.eigenvectors();

	double zero=3*numeric_limits<double>::epsilon();
	//size_t nSolutions=evd.alphas().size();
	size_t nSolutions=evd.eigenvalues().size();
	for(size_t c=0; c<nSolutions; ++c)
	{
		//Focal length is real and positive

		if(evd.eigenvalues()[c].real()<zero || fabs(evd.eigenvalues()[c].imag())>zero )
			continue;
		double f=(double)1/sqrt(evd.eigenvalues()[c].real());

		//Normalisation
		if( abs(mV(c,9))<zero)
			continue;
		Matrix<complex<double>, 10, 1> v=mV.col(c)/mV(9,c);

		//v must be real
		bool flagReal=true;
		for(size_t c2=0; c2<10; ++c2)
			if(fabs(v[c].imag())>zero)
				flagReal=false;

		if(!flagReal)
			continue;

		//Extract the coefficients
		double x=v[7].real();
		double y=v[8].real();

		//Essential matrix
		EpipolarMatrixT mF3=x*mB1 + y*mB2 + mB3;	//Could be rank-3, as the constraints are not enforced exactly

		//Normalise and save
		mF3.normalize();
		output.emplace_back(mF3, f);
	}	//for(size_t c=0; c<nSolutions; ++c)

	return output;
}	//list<ModelT> ModelFromBasis(const list<ModelT>& basis)

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Cost of the solver
 */
double OneSidedFundamentalSolverC::Cost(unsigned int nCorrespondences)
{
	return numeric_limits<double>::infinity();	//Effectively infinite, wrt/a constraint evaluation
}	//double Cost(unsigned int nCorrespondences=sGenerator)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return One-sided fundamental matrix
 * @remarks Minimal representation assumes an intrinsic calibration matrix of the form \c diag(f,f,1)
 */
auto OneSidedFundamentalSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	//Make the essential matrix part
	EssentialMinimalT minimalEssential=ExtractMinimalEssential(minimalModel);
	EpipolarMatrixT mE=EssentialSolverC::MakeModelFromMinimal(minimalEssential);

	//Multiply with the inverse of the intrinsic calibration matrix
	EpipolarMatrixT mF= Vector3T(1, 1, get<iFocalLength>(minimalModel)).asDiagonal() * mE;
	mF.normalize();

	return mF;
}	//ModelT MakeModel(const minimal_model_type& minimalModel)

/**
 * @brief Decomposes a normalised one-sided fundamental matrix into an essential matrix and a focal length
 * @param[in] mF Normalised one-sided fundamental matrix
 * @param[in] flagOverrideTrace If \c true, the trace constraint is not enforced
 * @return A tuple: Essential matrix and focal length. Invalid if no real and positive focal length estimates, or the trace constraint is violated
 * @pre \c mF is the one-sided fundamental matrix estimated via \c operator() , i.e. from normalised coordinates (unenforced)
 * @remarks "One-sided Fundamental Matrix Estimation, ", J. H. Brito, C. Zach, K. Koser, M. J. Ferreira, M. Pollefeys, BMVC 2012
 * @remarks Even a valid one-sided F can violate the trace constraint: a large focal length may cause numerical errors
 */
optional<tuple<EpipolarMatrixT, double> > OneSidedFundamentalSolverC::DecomposeOneSidedF(const ModelT& mF, bool flagOverrideTrace)
{
	//Polynomial coefficients

    double f11=mF(0,0); double f12=mF(0,1); double f13=mF(0,2);
    double f21=mF(1,0); double f22=mF(1,1); double f23=mF(1,2);
    double f31=mF(2,0); double f32=mF(2,1); double f33=mF(2,2);

    double f112=pow<2>(f11); double f113=f112*f11; double f114=f112*f112; double f116=f114*f112;
	double f122=pow<2>(f12); double f123=f122*f12; double f124=f122*f122; double f126=f124*f122;
	double f132=pow<2>(f13); double f133=f132*f13; double f134=f132*f132; double f136=f134*f132;
	double f212=pow<2>(f21); double f213=f212*f21; double f214=f212*f212; double f216=f214*f212;
	double f222=pow<2>(f22); double f223=f222*f22; double f224=f222*f222; double f226=f224*f222;
	double f232=pow<2>(f23); double f233=f232*f23; double f234=f232*f232; double f236=f234*f232;
	double f312=pow<2>(f31); double f313=f312*f31; double f314=f312*f312;
	double f322=pow<2>(f32); double f323=f322*f32; double f324=f322*f322;
	double f332=pow<2>(f33); double f333=f332*f33; double f334=f332*f332;

	array<double, 3> coefficients;
	coefficients[0] =16*f31*f13*f33*f11*f322+16*f31*f13*f333*f11+16*f12*f323*f13*f33+16*f32*f13*f333*f12+16*f21*f313*f23*f33+16*f312*f22*f32*f23*f33+16*f31*f22*f323*f21+16*f31*f22*f32*f21*f332+4*f112*f312*f322+4*f112*f312*f332+4*f212*f312*f322+4*f212*f312*f332+4*f312*f222*f322+4*f312*f232*f332-4*f212*f322*f332+4*f222*f322*f332+4*f322*f232*f332+16*f21*f313*f22*f32+16*f22*f323*f23*f33+16*f32*f23*f333*f22+16*f11*f313*f12*f32+16*f11*f313*f13*f33+16*f312*f12*f32*f13*f33+16*f31*f12*f323*f11+16*f31*f12*f32*f11*f332+16*f31*f23*f33*f21*f322+16*f31*f23*f333*f21-2*f112*f324-2*f112*f334+6*f212*f314+6*f122*f324-2*f122*f334-2*f212*f324-2*f212*f334-2*f314*f122+6*f222*f324-2*f222*f334-2*f324*f132-2*f324*f232+6*f132*f334+6*f232*f334+6*f112*f314-2*f314*f132-2*f314*f222-2*f314*f232+4*f312*f122*f322+4*f312*f132*f332-4*f112*f322*f332-4*f312*f322*f132-4*f312*f322*f232-4*f312*f332*f122-4*f312*f332*f222+4*f122*f322*f332+4*f322*f132*f332;
	coefficients[1] =96*f11*f12*f22*f31*f23*f33+8*f122*f132*f332-8*f112*f312*f222-8*f112*f312*f232+8*f212*f232*f332-8*f212*f312*f132-8*f212*f122*f322+24*f212*f122*f332+24*f212*f132*f322-8*f212*f132*f332+8*f222*f232*f332-8*f222*f322*f132-8*f222*f132*f332-8*f212*f312*f122-8*f212*f232*f322+24*f222*f122*f322-8*f222*f122*f332+8*f112*f132*f312+24*f232*f132*f332-8*f322*f132*f232-8*f112*f132*f322+8*f112*f132*f332+24*f212*f112*f312-8*f212*f112*f322-8*f212*f112*f332-8*f112*f122*f332+96*f11*f13*f23*f31*f22*f32+32*f213*f31*f22*f32+32*f213*f31*f23*f33+32*f21*f223*f31*f32+32*f21*f222*f31*f23*f33+32*f21*f232*f31*f22*f32+32*f21*f233*f31*f33+32*f122*f22*f32*f23*f33+32*f212*f32*f22*f33*f23+32*f223*f32*f23*f33+32*f22*f233*f32*f33-64*f32*f23*f33*f22*f112+32*f12*f133*f32*f33+32*f11*f122*f31*f13*f33+32*f11*f132*f31*f12*f32+32*f11*f133*f31*f33+32*f212*f11*f31*f12*f32+32*f212*f11*f31*f13*f33+32*f21*f12*f22*f11*f312+32*f21*f122*f22*f31*f32+96*f21*f12*f22*f31*f13*f33+32*f21*f12*f22*f11*f322-64*f21*f12*f22*f11*f332+32*f21*f13*f23*f11*f312+96*f21*f13*f23*f31*f12*f32+32*f21*f132*f23*f31*f33-64*f21*f13*f23*f11*f322+32*f21*f13*f23*f11*f332+32*f31*f12*f32*f11*f222-64*f31*f12*f32*f11*f232+96*f11*f33*f13*f21*f32*f22+8*f112*f122*f312+32*f31*f13*f33*f11*f232-64*f32*f13*f33*f12*f212+32*f32*f13*f33*f12*f232+32*f112*f32*f12*f33*f13+96*f11*f32*f12*f21*f33*f23-64*f31*f22*f32*f21*f132-64*f31*f23*f33*f21*f122+32*f112*f21*f31*f22*f32+32*f112*f21*f31*f23*f33-64*f31*f13*f33*f11*f222+32*f113*f31*f12*f32+32*f113*f31*f13*f33+32*f11*f123*f31*f32+32*f123*f32*f13*f33+12*f134*f332-4*f124*f332+12*f224*f322-4*f224*f332-4*f322*f134+12*f234*f332-4*f322*f234+12*f114*f312-4*f114*f322-4*f114*f332-4*f312*f124-4*f312*f134-4*f312*f224+32*f222*f12*f32*f13*f33+32*f22*f13*f23*f12*f322+32*f22*f132*f23*f32*f33-64*f22*f13*f23*f12*f312+32*f22*f13*f23*f12*f332-8*f112*f222*f322+24*f112*f222*f332+24*f112*f232*f322-8*f112*f232*f332+8*f212*f222*f312+8*f212*f222*f322-8*f212*f222*f332+8*f212*f232*f312+8*f222*f232*f322-8*f312*f122*f132-8*f312*f122*f222+24*f312*f122*f232+24*f312*f132*f222-8*f122*f322*f232-8*f122*f232*f332+8*f112*f122*f322-8*f312*f132*f232-8*f312*f222*f232+8*f122*f132*f322-4*f312*f234+12*f124*f322+12*f214*f312-4*f214*f322-4*f214*f332;
	coefficients[2] =12*f212*f132*f232-12*f212*f122*f132+36*f112*f122*f132+12*f222*f122*f232+12*f222*f132*f232-12*f112*f132*f222+12*f112*f132*f232+12*f212*f112*f222+12*f212*f112*f232+12*f212*f122*f222-12*f112*f122*f232+48*f12*f133*f22*f23+48*f11*f122*f21*f13*f23+48*f11*f132*f21*f12*f22+48*f11*f133*f21*f23+48*f213*f11*f12*f22+48*f213*f11*f13*f23+48*f212*f12*f22*f13*f23+48*f21*f12*f223*f11+48*f21*f12*f22*f11*f232+48*f21*f13*f233*f11+48*f21*f13*f23*f11*f222+48*f113*f21*f12*f22+6*f226+6*f116+12*f122*f132*f232+48*f112*f12*f22*f13*f23+6*f136+48*f113*f21*f13*f23+48*f11*f123*f21*f22+48*f123*f22*f13*f23+18*f114*f122-6*f124*f232+18*f122*f134+18*f224*f122-6*f122*f234-6*f212*f124-6*f212*f134+18*f224*f232-6*f224*f132+18*f222*f234-6*f222*f134+18*f114*f132+18*f114*f212-6*f114*f222-6*f114*f232+18*f112*f124+18*f112*f134+18*f214*f112-6*f112*f224+18*f234*f132+48*f223*f12*f13*f23+48*f22*f13*f233*f12-12*f112*f222*f232+6*f126+6*f236+36*f212*f222*f232-12*f212*f222*f132-12*f212*f232*f122+12*f112*f132*f212+12*f112*f122*f212+12*f112*f122*f222+12*f122*f132*f222+6*f216+18*f124*f132+18*f124*f222-6*f112*f234+18*f214*f222+18*f214*f232-6*f214*f122-6*f214*f132+18*f212*f224+18*f212*f234+18*f134*f232;

	array<complex<double>, 2> roots=QuadraticPolynomialRoots(coefficients);	//Solve for f. Derivative of Eq 9.

	//Pick the solution that minimises the trace constraint
	double zero=3*numeric_limits<double>::epsilon();
	double zero2=10*numeric_limits<double>::epsilon();
	optional<tuple<EpipolarMatrixT, double> > bestSolution;
	double bestCost=numeric_limits<double>::infinity();
	for(const auto& current : roots)
	{
		//Positive and real?
		if( fabs(current.imag())>zero || current.real()<zero )
			continue;

		//Double root?
		if(bestSolution && get<1>(*bestSolution)==current.real())
			continue;

		//Make E
		double f=(double)1/sqrt(current.real());//The unknown calibration matrix is diag(1,1,1/f). Detail omitted in the paper
		EpipolarMatrixT mE=Vector3T(f, f, 1).asDiagonal()*mF;	//Since F is rank-2, so is E

		//Evaluate the trace constraint, Eq 9
		EpipolarMatrixT tmp=mE.transpose()*mE;
		double cost=(2*mE*tmp - tmp.trace()*mE).norm();

		if(cost<bestCost && (flagOverrideTrace || cost<zero2) )	//For a valid one-sided F, trace constraint should be satisfied
		{
			bestSolution=make_tuple(mE, f);
			bestCost=cost;
		}
	}	//for(const auto& current : roots)

	return bestSolution;
}	//optional<tuple<EpipolarMatrixT, double> > DecomposeOneSidedF(const ModelT& mFnormalised)

/**
 * @brief Computes the coefficient of the equation system
 * @param[in] basis 3-element basis
 * @return A tuple of 10x10 matrices, C0 and C1w
 * @pre \c basis has 3 elements
 */
auto OneSidedFundamentalSolverC::ComputeCoefficients(const list<EpipolarMatrixT>& basis) -> tuple<Matrix10T, Matrix10T>
{
	assert(basis.size()==3);

	auto itBasis=basis.begin();
	EpipolarMatrixT mF1=*itBasis; advance(itBasis,1);
	EpipolarMatrixT mF2=*itBasis; advance(itBasis,1);
	EpipolarMatrixT mF3=*itBasis;

	//Variables
	double f11=mF1(0,0); double f112=pow<2>(f11); double f113=f112*f11;
	double f12=mF1(0,1); double f122=pow<2>(f12); double f123=f122*f12;
	double f13=mF1(0,2); double f132=pow<2>(f13); double f133=f132*f13;
	double f21=mF1(1,0); double f212=pow<2>(f21); double f213=f212*f21;
	double f22=mF1(1,1); double f222=pow<2>(f22); double f223=f222*f22;
	double f23=mF1(1,2); double f232=pow<2>(f23); double f233=f232*f23;
	double f31=mF1(2,0); double f312=pow<2>(f31); double f313=f312*f31;
	double f32=mF1(2,1); double f322=pow<2>(f32); double f323=f322*f32;
	double f33=mF1(2,2); double f332=pow<2>(f33); double f333=f332*f33;


	double g11=mF2(0,0); double g112=pow<2>(g11); double g113=g112*g11;
	double g12=mF2(0,1); double g122=pow<2>(g12); double g123=g122*g12;
	double g13=mF2(0,2); double g132=pow<2>(g13); double g133=g132*g13;
	double g21=mF2(1,0); double g212=pow<2>(g21); double g213=g212*g21;
	double g22=mF2(1,1); double g222=pow<2>(g22); double g223=g222*g22;
	double g23=mF2(1,2); double g232=pow<2>(g23); double g233=g232*g23;
	double g31=mF2(2,0); double g312=pow<2>(g31); double g313=g312*g31;
	double g32=mF2(2,1); double g322=pow<2>(g32); double g323=g322*g32;
	double g33=mF2(2,2); double g332=pow<2>(g33); double g333=g332*g33;

	double h11=mF3(0,0); double h112=pow<2>(h11); double h113=h112*h11;
	double h12=mF3(0,1); double h122=pow<2>(h12); double h123=h122*h12;
	double h13=mF3(0,2); double h132=pow<2>(h13); double h133=h132*h13;
	double h21=mF3(1,0); double h212=pow<2>(h21); double h213=h212*h21;
	double h22=mF3(1,1); double h222=pow<2>(h22); double h223=h222*h22;
	double h23=mF3(1,2); double h232=pow<2>(h23); double h233=h232*h23;
	double h31=mF3(2,0); double h312=pow<2>(h31); double h313=h312*h31;
	double h32=mF3(2,1); double h322=pow<2>(h32); //double h323=h322*h32;
	double h33=mF3(2,2); double h332=pow<2>(h33); double h333=h332*h33;

	RealT w1=1; //F's are computed from normalised points

	//Equation system: x^3 x^2y xy^2 y^3 x^2 xy y^2 x y 1
	Matrix10T mC0=Matrix10T::Zero();
	Matrix10T mC1=Matrix10T::Zero();

	//det(F)=0
	mC0(0,0)=(f11*f22-f12*f21)*f33+(f13*f21-f11*f23)*f32+(f12*f23-f13*f22)*f31;
	mC0(0,1)=(f11*f22-f12*f21)*g33+(f13*f21-f11*f23)*g32+(f12*f23-f13*f22)*g31+(f12*f31-f11*f32)*g23+(f11*f33-f13*f31)*g22+(f13*f32-f12*f33)*g21+(f21*f32-f22*f31)*g13+(f23*f31-f21*f33)*g12+(f22*f33-f23*f32)*g11;
	mC0(0,2)=(f11*g22-f12*g21-f21*g12+f22*g11)*g33+(-f11*g23+f13*g21+f21*g13-f23*g11)*g32+(f12*g23-f13*g22-f22*g13+f23*g12)*g31+(f31*g12-f32*g11)*g23+(f33*g11-f31*g13)*g22+(f32*g13-f33*g12)*g21;
	mC0(0,3)=(g11*g22-g12*g21)*g33+(g13*g21-g11*g23)*g32+(g12*g23-g13*g22)*g31;
	mC0(0,4)=(f11*f22-f12*f21)*h33+(f13*f21-f11*f23)*h32+(f12*f23-f13*f22)*h31+(f12*f31-f11*f32)*h23+(f11*f33-f13*f31)*h22+(f13*f32-f12*f33)*h21+(f21*f32-f22*f31)*h13+(f23*f31-f21*f33)*h12+(f22*f33-f23*f32)*h11;
	mC0(0,5)=(f11*g22-f12*g21-f21*g12+f22*g11)*h33+(-f11*g23+f13*g21+f21*g13-f23*g11)*h32+(f12*g23-f13*g22-f22*g13+f23*g12)*h31+(-f11*g32+f12*g31+f31*g12-f32*g11)*h23+(f11*g33-f13*g31-f31*g13+f33*g11)*h22+(-f12*g33+f13*g32+f32*g13-f33*g12)*h21+(f21*g32-f22*g31-f31*g22+f32*g21)*h13+(-f21*g33+f23*g31+f31*g23-f33*g21)*h12+(f22*g33-f23*g32-f32*g23+f33*g22)*h11;
	mC0(0,6)=(g11*g22-g12*g21)*h33+(g13*g21-g11*g23)*h32+(g12*g23-g13*g22)*h31+(g12*g31-g11*g32)*h23+(g11*g33-g13*g31)*h22+(g13*g32-g12*g33)*h21+(g21*g32-g22*g31)*h13+(g23*g31-g21*g33)*h12+(g22*g33-g23*g32)*h11;
	mC0(0,7)=(f11*h22-f12*h21-f21*h12+f22*h11)*h33+(-f11*h23+f13*h21+f21*h13-f23*h11)*h32+(f12*h23-f13*h22-f22*h13+f23*h12)*h31+(f31*h12-f32*h11)*h23+(f33*h11-f31*h13)*h22+(f32*h13-f33*h12)*h21;
	mC0(0,8)=(g11*h22-g12*h21-g21*h12+g22*h11)*h33+(-g11*h23+g13*h21+g21*h13-g23*h11)*h32+(g12*h23-g13*h22-g22*h13+g23*h12)*h31+(g31*h12-g32*h11)*h23+(g33*h11-g31*h13)*h22+(g32*h13-g33*h12)*h21;
	mC0(0,9)=(h11*h22-h12*h21)*h33+(h13*h21-h11*h23)*h32+(h12*h23-h13*h22)*h31;

	//S(1,1)=0

	mC0(1,0)=(-f11*f232+2*f13*f21*f23+f11*f132)*w1-f11*f222+2*f12*f21*f22+f11*f212+f11*f122+f113;
	mC0(1,1)=((2*f13*f21-2*f11*f23)*g23+2*f13*f23*g21+(2*f21*f23+2*f11*f13)*g13+(f132-f232)*g11)*w1+(2*f12*f21-2*f11*f22)*g22+(2*f12*f22+2*f11*f21)*g21+(2*f21*f22+2*f11*f12)*g12+(-f222+f212+f122+3*f112)*g11;
	mC0(1,2)=(-f11*g232+(2*f13*g21+2*f21*g13-2*f23*g11)*g23+2*f23*g13*g21+f11*g132+2*f13*g11*g13)*w1-f11*g222+(2*f12*g21+2*f21*g12-2*f22*g11)*g22+f11*g212+(2*f22*g12+2*f21*g11)*g21+f11*g122+2*f12*g11*g12+3*f11*g112;
	mC0(1,3)=(-g11*g232+2*g13*g21*g23+g11*g132)*w1-g11*g222+2*g12*g21*g22+g11*g212+g11*g122+g113;
	mC0(1,4)=((2*f13*f21-2*f11*f23)*h23+2*f13*f23*h21+(2*f21*f23+2*f11*f13)*h13+(f132-f232)*h11)*w1+(2*f12*f21-2*f11*f22)*h22+(2*f12*f22+2*f11*f21)*h21+(2*f21*f22+2*f11*f12)*h12+(-f222+f212+f122+3*f112)*h11;
	mC0(1,5)=((-2*f11*g23+2*f13*g21+2*f21*g13-2*f23*g11)*h23+(2*f13*g23+2*f23*g13)*h21+(2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*h13+(2*f13*g13-2*f23*g23)*h11)*w1+(-2*f11*g22+2*f12*g21+2*f21*g12-2*f22*g11)*h22+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*h21+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*h12+(-2*f22*g22+2*f21*g21+2*f12*g12+6*f11*g11)*h11;
	mC0(1,6)=((2*g13*g21-2*g11*g23)*h23+2*g13*g23*h21+(2*g21*g23+2*g11*g13)*h13+(g132-g232)*h11)*w1+(2*g12*g21-2*g11*g22)*h22+(2*g12*g22+2*g11*g21)*h21+(2*g21*g22+2*g11*g12)*h12+(-g222+g212+g122+3*g112)*h11;
	mC0(1,7)=(-f11*h232+(2*f13*h21+2*f21*h13-2*f23*h11)*h23+2*f23*h13*h21+f11*h132+2*f13*h11*h13)*w1-f11*h222+(2*f12*h21+2*f21*h12-2*f22*h11)*h22+f11*h212+(2*f22*h12+2*f21*h11)*h21+f11*h122+2*f12*h11*h12+3*f11*h112;
	mC0(1,8)=(-g11*h232+(2*g13*h21+2*g21*h13-2*g23*h11)*h23+2*g23*h13*h21+g11*h132+2*g13*h11*h13)*w1-g11*h222+(2*g12*h21+2*g21*h12-2*g22*h11)*h22+g11*h212+(2*g22*h12+2*g21*h11)*h21+g11*h122+2*g12*h11*h12+3*g11*h112;
	mC0(1,9)=+(-h11*h232+2*h13*h21*h23+h11*h132)*w1-h11*h222+2*h12*h21*h22+h11*h212+h11*h122+h113;

	mC1(1,0)=((2*f13*f31*f33-f11*f332)*w1-f11*f322+2*f12*f31*f32+f11*f312);
	mC1(1,1)=(((2*f13*f31-2*f11*f33)*g33+2*f13*f33*g31+2*f31*f33*g13-f332*g11)*w1+(2*f12*f31-2*f11*f32)*g32+(2*f12*f32+2*f11*f31)*g31+2*f31*f32*g12+(f312-f322)*g11);
	mC1(1,2)=((-f11*g332+(2*f13*g31+2*f31*g13-2*f33*g11)*g33+2*f33*g13*g31)*w1-f11*g322+(2*f12*g31+2*f31*g12-2*f32*g11)*g32+f11*g312+(2*f32*g12+2*f31*g11)*g31);
	mC1(1,3)=((2*g13*g31*g33-g11*g332)*w1-g11*g322+2*g12*g31*g32+g11*g312);
	mC1(1,4)=(((2*f13*f31-2*f11*f33)*h33+2*f13*f33*h31+2*f31*f33*h13-f332*h11)*w1+(2*f12*f31-2*f11*f32)*h32+(2*f12*f32+2*f11*f31)*h31+2*f31*f32*h12+(f312-f322)*h11);
	mC1(1,5)=(((-2*f11*g33+2*f13*g31+2*f31*g13-2*f33*g11)*h33+(2*f13*g33+2*f33*g13)*h31+(2*f31*g33+2*f33*g31)*h13-2*f33*g33*h11)*w1+(-2*f11*g32+2*f12*g31+2*f31*g12-2*f32*g11)*h32+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*h31+(2*f31*g32+2*f32*g31)*h12+(2*f31*g31-2*f32*g32)*h11);
	mC1(1,6)=(((2*g13*g31-2*g11*g33)*h33+2*g13*g33*h31+2*g31*g33*h13-g332*h11)*w1+(2*g12*g31-2*g11*g32)*h32+(2*g12*g32+2*g11*g31)*h31+2*g31*g32*h12+(g312-g322)*h11);
	mC1(1,7)=((-f11*h332+(2*f13*h31+2*f31*h13-2*f33*h11)*h33+2*f33*h13*h31)*w1-f11*h322+(2*f12*h31+2*f31*h12-2*f32*h11)*h32+f11*h312+(2*f32*h12+2*f31*h11)*h31);
	mC1(1,8)=((-g11*h332+(2*g13*h31+2*g31*h13-2*g33*h11)*h33+2*g33*h13*h31)*w1-g11*h322+(2*g12*h31+2*g31*h12-2*g32*h11)*h32+g11*h312+(2*g32*h12+2*g31*h11)*h31);
	mC1(1,9)=((2*h13*h31*h33-h11*h332)*w1-h11*h322+2*h12*h31*h32+h11*h312);

	//S(1,2)=0

	mC0(2,0)=(-f12*f232+2*f13*f22*f23+f12*f132)*w1+f12*f222+2*f11*f21*f22-f12*f212+f123+f112*f12;
	mC0(2,1)=((2*f13*f22-2*f12*f23)*g23+2*f13*f23*g22+(2*f22*f23+2*f12*f13)*g13+(f132-f232)*g12)*w1+(2*f12*f22+2*f11*f21)*g22+(2*f11*f22-2*f12*f21)*g21+(f222-f212+3*f122+f112)*g12+(2*f21*f22+2*f11*f12)*g11;
	mC0(2,2)=(-f12*g232+(2*f13*g22+2*f22*g13-2*f23*g12)*g23+2*f23*g13*g22+f12*g132+2*f13*g12*g13)*w1+f12*g222+(2*f11*g21+2*f22*g12+2*f21*g11)*g22-f12*g212+(2*f22*g11-2*f21*g12)*g21+3*f12*g122+2*f11*g11*g12+f12*g112;
	mC0(2,3)=(-g12*g232+2*g13*g22*g23+g12*g132)*w1+g12*g222+2*g11*g21*g22-g12*g212+g123+g112*g12;
	mC0(2,4)=((2*f13*f22-2*f12*f23)*h23+2*f13*f23*h22+(2*f22*f23+2*f12*f13)*h13+(f132-f232)*h12)*w1+(2*f12*f22+2*f11*f21)*h22+(2*f11*f22-2*f12*f21)*h21+(f222-f212+3*f122+f112)*h12+(2*f21*f22+2*f11*f12)*h11;
	mC0(2,5)=((-2*f12*g23+2*f13*g22+2*f22*g13-2*f23*g12)*h23+(2*f13*g23+2*f23*g13)*h22+(2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*h13+(2*f13*g13-2*f23*g23)*h12)*w1+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*h22+(2*f11*g22-2*f12*g21-2*f21*g12+2*f22*g11)*h21+(2*f22*g22-2*f21*g21+6*f12*g12+2*f11*g11)*h12+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*h11;
	mC0(2,6)=((2*g13*g22-2*g12*g23)*h23+2*g13*g23*h22+(2*g22*g23+2*g12*g13)*h13+(g132-g232)*h12)*w1+(2*g12*g22+2*g11*g21)*h22+(2*g11*g22-2*g12*g21)*h21+(g222-g212+3*g122+g112)*h12+(2*g21*g22+2*g11*g12)*h11;
	mC0(2,7)=(-f12*h232+(2*f13*h22+2*f22*h13-2*f23*h12)*h23+2*f23*h13*h22+f12*h132+2*f13*h12*h13)*w1+f12*h222+(2*f11*h21+2*f22*h12+2*f21*h11)*h22-f12*h212+(2*f22*h11-2*f21*h12)*h21+3*f12*h122+2*f11*h11*h12+f12*h112;
	mC0(2,8)=(-g12*h232+(2*g13*h22+2*g22*h13-2*g23*h12)*h23+2*g23*h13*h22+g12*h132+2*g13*h12*h13)*w1+g12*h222+(2*g11*h21+2*g22*h12+2*g21*h11)*h22-g12*h212+(2*g22*h11-2*g21*h12)*h21+3*g12*h122+2*g11*h11*h12+g12*h112;
	mC0(2,9)=(-h12*h232+2*h13*h22*h23+h12*h132)*w1+h12*h222+2*h11*h21*h22-h12*h212+h123+h112*h12;

	mC1(2,0)=((2*f13*f32*f33-f12*f332)*w1+f12*f322+2*f11*f31*f32-f12*f312);
	mC1(2,1)=(((2*f13*f32-2*f12*f33)*g33+2*f13*f33*g32+2*f32*f33*g13-f332*g12)*w1+(2*f12*f32+2*f11*f31)*g32+(2*f11*f32-2*f12*f31)*g31+(f322-f312)*g12+2*f31*f32*g11);
	mC1(2,2)=((-f12*g332+(2*f13*g32+2*f32*g13-2*f33*g12)*g33+2*f33*g13*g32)*w1+f12*g322+(2*f11*g31+2*f32*g12+2*f31*g11)*g32-f12*g312+(2*f32*g11-2*f31*g12)*g31);
	mC1(2,3)=((2*g13*g32*g33-g12*g332)*w1+g12*g322+2*g11*g31*g32-g12*g312);
	mC1(2,4)=(((2*f13*f32-2*f12*f33)*h33+2*f13*f33*h32+2*f32*f33*h13-f332*h12)*w1+(2*f12*f32+2*f11*f31)*h32+(2*f11*f32-2*f12*f31)*h31+(f322-f312)*h12+2*f31*f32*h11);
	mC1(2,5)=(((-2*f12*g33+2*f13*g32+2*f32*g13-2*f33*g12)*h33+(2*f13*g33+2*f33*g13)*h32+(2*f32*g33+2*f33*g32)*h13-2*f33*g33*h12)*w1+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*h32+(2*f11*g32-2*f12*g31-2*f31*g12+2*f32*g11)*h31+(2*f32*g32-2*f31*g31)*h12+(2*f31*g32+2*f32*g31)*h11);
	mC1(2,6)=(((2*g13*g32-2*g12*g33)*h33+2*g13*g33*h32+2*g32*g33*h13-g332*h12)*w1+(2*g12*g32+2*g11*g31)*h32+(2*g11*g32-2*g12*g31)*h31+(g322-g312)*h12+2*g31*g32*h11);
	mC1(2,7)=((-f12*h332+(2*f13*h32+2*f32*h13-2*f33*h12)*h33+2*f33*h13*h32)*w1+f12*h322+(2*f11*h31+2*f32*h12+2*f31*h11)*h32-f12*h312+(2*f32*h11-2*f31*h12)*h31);
	mC1(2,8)=((-g12*h332+(2*g13*h32+2*g32*h13-2*g33*h12)*h33+2*g33*h13*h32)*w1+g12*h322+(2*g11*h31+2*g32*h12+2*g31*h11)*h32-g12*h312+(2*g32*h11-2*g31*h12)*h31);
	mC1(2,9)=((2*h13*h32*h33-h12*h332)*w1+h12*h322+2*h11*h31*h32-h12*h312);

	//S(1,3)=0

	mC0(3,0)=(f13*f232+f133)*w1+(2*f12*f22+2*f11*f21)*f23-f13*f222-f13*f212+(f122+f112)*f13;
	mC0(3,1)=(2*f13*f23*g23+(f232+3*f132)*g13)*w1+(2*f12*f22+2*f11*f21)*g23+(2*f12*f23-2*f13*f22)*g22+(2*f11*f23-2*f13*f21)*g21+(-f222-f212+f122+f112)*g13+(2*f22*f23+2*f12*f13)*g12+(2*f21*f23+2*f11*f13)*g11;
	mC0(3,2)=(f13*g232+2*f23*g13*g23+3*f13*g132)*w1+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*g23-f13*g222+(2*f23*g12-2*f22*g13)*g22-f13*g212+(2*f23*g11-2*f21*g13)*g21+(2*f12*g12+2*f11*g11)*g13+f13*g122+f13*g112;
	mC0(3,3)=(g13*g232+g133)*w1+(2*g12*g22+2*g11*g21)*g23-g13*g222-g13*g212+(g122+g112)*g13;
	mC0(3,4)=(2*f13*f23*h23+(f232+3*f132)*h13)*w1+(2*f12*f22+2*f11*f21)*h23+(2*f12*f23-2*f13*f22)*h22+(2*f11*f23-2*f13*f21)*h21+(-f222-f212+f122+f112)*h13+(2*f22*f23+2*f12*f13)*h12+(2*f21*f23+2*f11*f13)*h11;
	mC0(3,5)=((2*f13*g23+2*f23*g13)*h23+(2*f23*g23+6*f13*g13)*h13)*w1+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*h23+(2*f12*g23-2*f13*g22-2*f22*g13+2*f23*g12)*h22+(2*f11*g23-2*f13*g21-2*f21*g13+2*f23*g11)*h21+(-2*f22*g22-2*f21*g21+2*f12*g12+2*f11*g11)*h13+(2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*h12+(2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*h11;
	mC0(3,6)=(2*g13*g23*h23+(g232+3*g132)*h13)*w1+(2*g12*g22+2*g11*g21)*h23+(2*g12*g23-2*g13*g22)*h22+(2*g11*g23-2*g13*g21)*h21+(-g222-g212+g122+g112)*h13+(2*g22*g23+2*g12*g13)*h12+(2*g21*g23+2*g11*g13)*h11;
	mC0(3,7)=(f13*h232+2*f23*h13*h23+3*f13*h132)*w1+(2*f12*h22+2*f11*h21+2*f22*h12+2*f21*h11)*h23-f13*h222+(2*f23*h12-2*f22*h13)*h22-f13*h212+(2*f23*h11-2*f21*h13)*h21+(2*f12*h12+2*f11*h11)*h13+f13*h122+f13*h112;
	mC0(3,8)=(g13*h232+2*g23*h13*h23+3*g13*h132)*w1+(2*g12*h22+2*g11*h21+2*g22*h12+2*g21*h11)*h23-g13*h222+(2*g23*h12-2*g22*h13)*h22-g13*h212+(2*g23*h11-2*g21*h13)*h21+(2*g12*h12+2*g11*h11)*h13+g13*h122+g13*h112;
	mC0(3,9)=(h13*h232+h133)*w1+(2*h12*h22+2*h11*h21)*h23-h13*h222-h13*h212+(h122+h112)*h13;

	mC1(3,0)=(f13*f332*w1+(2*f12*f32+2*f11*f31)*f33-f13*f322-f13*f312);
	mC1(3,1)=((2*f13*f33*g33+f332*g13)*w1+(2*f12*f32+2*f11*f31)*g33+(2*f12*f33-2*f13*f32)*g32+(2*f11*f33-2*f13*f31)*g31+(-f322-f312)*g13+2*f32*f33*g12+2*f31*f33*g11);
	mC1(3,2)=((f13*g332+2*f33*g13*g33)*w1+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*g33-f13*g322+(2*f33*g12-2*f32*g13)*g32-f13*g312+(2*f33*g11-2*f31*g13)*g31);
	mC1(3,3)=(g13*g332*w1+(2*g12*g32+2*g11*g31)*g33-g13*g322-g13*g312);
	mC1(3,4)=((2*f13*f33*h33+f332*h13)*w1+(2*f12*f32+2*f11*f31)*h33+(2*f12*f33-2*f13*f32)*h32+(2*f11*f33-2*f13*f31)*h31+(-f322-f312)*h13+2*f32*f33*h12+2*f31*f33*h11);
	mC1(3,5)=(((2*f13*g33+2*f33*g13)*h33+2*f33*g33*h13)*w1+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*h33+(2*f12*g33-2*f13*g32-2*f32*g13+2*f33*g12)*h32+(2*f11*g33-2*f13*g31-2*f31*g13+2*f33*g11)*h31+(-2*f32*g32-2*f31*g31)*h13+(2*f32*g33+2*f33*g32)*h12+(2*f31*g33+2*f33*g31)*h11);
	mC1(3,6)=((2*g13*g33*h33+g332*h13)*w1+(2*g12*g32+2*g11*g31)*h33+(2*g12*g33-2*g13*g32)*h32+(2*g11*g33-2*g13*g31)*h31+(-g322-g312)*h13+2*g32*g33*h12+2*g31*g33*h11);
	mC1(3,7)=((f13*h332+2*f33*h13*h33)*w1+(2*f12*h32+2*f11*h31+2*f32*h12+2*f31*h11)*h33-f13*h322+(2*f33*h12-2*f32*h13)*h32-f13*h312+(2*f33*h11-2*f31*h13)*h31);
	mC1(3,8)=((g13*h332+2*g33*h13*h33)*w1+(2*g12*h32+2*g11*h31+2*g32*h12+2*g31*h11)*h33-g13*h322+(2*g33*h12-2*g32*h13)*h32-g13*h312+(2*g33*h11-2*g31*h13)*h31);
	mC1(3,9)=(h13*h332*w1+(2*h12*h32+2*h11*h31)*h33-h13*h322-h13*h312);

	//S(2,1)=0

	mC0(4,0)=(f21*f232+2*f11*f13*f23-f132*f21)*w1+f21*f222+2*f11*f12*f22+f213+(f112-f122)*f21;
	mC0(4,1)=((2*f21*f23+2*f11*f13)*g23+(f232-f132)*g21+(2*f11*f23-2*f13*f21)*g13+2*f13*f23*g11)*w1+(2*f21*f22+2*f11*f12)*g22+(f222+3*f212-f122+f112)*g21+(2*f11*f22-2*f12*f21)*g12+(2*f12*f22+2*f11*f21)*g11;
	mC0(4,2)=(f21*g232+(2*f23*g21+2*f11*g13+2*f13*g11)*g23-2*f13*g13*g21-f21*g132+2*f23*g11*g13)*w1+f21*g222+(2*f22*g21+2*f11*g12+2*f12*g11)*g22+3*f21*g212+(2*f11*g11-2*f12*g12)*g21-f21*g122+2*f22*g11*g12+f21*g112;
	mC0(4,3)=(g21*g232+2*g11*g13*g23-g132*g21)*w1+g21*g222+2*g11*g12*g22+g213+(g112-g122)*g21;
	mC0(4,4)=((2*f21*f23+2*f11*f13)*h23+(f232-f132)*h21+(2*f11*f23-2*f13*f21)*h13+2*f13*f23*h11)*w1+(2*f21*f22+2*f11*f12)*h22+(f222+3*f212-f122+f112)*h21+(2*f11*f22-2*f12*f21)*h12+(2*f12*f22+2*f11*f21)*h11;
	mC0(4,5)=((2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*h23+(2*f23*g23-2*f13*g13)*h21+(2*f11*g23-2*f13*g21-2*f21*g13+2*f23*g11)*h13+(2*f13*g23+2*f23*g13)*h11)*w1+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*h22+(2*f22*g22+6*f21*g21-2*f12*g12+2*f11*g11)*h21+(2*f11*g22-2*f12*g21-2*f21*g12+2*f22*g11)*h12+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*h11;
	mC0(4,6)=((2*g21*g23+2*g11*g13)*h23+(g232-g132)*h21+(2*g11*g23-2*g13*g21)*h13+2*g13*g23*h11)*w1+(2*g21*g22+2*g11*g12)*h22+(g222+3*g212-g122+g112)*h21+(2*g11*g22-2*g12*g21)*h12+(2*g12*g22+2*g11*g21)*h11;
	mC0(4,7)=(f21*h232+(2*f23*h21+2*f11*h13+2*f13*h11)*h23-2*f13*h13*h21-f21*h132+2*f23*h11*h13)*w1+f21*h222+(2*f22*h21+2*f11*h12+2*f12*h11)*h22+3*f21*h212+(2*f11*h11-2*f12*h12)*h21-f21*h122+2*f22*h11*h12+f21*h112;
	mC0(4,8)=(g21*h232+(2*g23*h21+2*g11*h13+2*g13*h11)*h23-2*g13*h13*h21-g21*h132+2*g23*h11*h13)*w1+g21*h222+(2*g22*h21+2*g11*h12+2*g12*h11)*h22+3*g21*h212+(2*g11*h11-2*g12*h12)*h21-g21*h122+2*g22*h11*h12+g21*h112;
	mC0(4,9)=(h21*h232+2*h11*h13*h23-h132*h21)*w1+h21*h222+2*h11*h12*h22+h213+(h112-h122)*h21;

	mC1(4,0)=((2*f23*f31*f33-f21*f332)*w1-f21*f322+2*f22*f31*f32+f21*f312);
	mC1(4,1)=(((2*f23*f31-2*f21*f33)*g33+2*f23*f33*g31+2*f31*f33*g23-f332*g21)*w1+(2*f22*f31-2*f21*f32)*g32+(2*f22*f32+2*f21*f31)*g31+2*f31*f32*g22+(f312-f322)*g21);
	mC1(4,2)=((-f21*g332+(2*f23*g31+2*f31*g23-2*f33*g21)*g33+2*f33*g23*g31)*w1-f21*g322+(2*f22*g31+2*f31*g22-2*f32*g21)*g32+f21*g312+(2*f32*g22+2*f31*g21)*g31);
	mC1(4,3)=((2*g23*g31*g33-g21*g332)*w1-g21*g322+2*g22*g31*g32+g21*g312);
	mC1(4,4)=(((2*f23*f31-2*f21*f33)*h33+2*f23*f33*h31+2*f31*f33*h23-f332*h21)*w1+(2*f22*f31-2*f21*f32)*h32+(2*f22*f32+2*f21*f31)*h31+2*f31*f32*h22+(f312-f322)*h21);
	mC1(4,5)=(((-2*f21*g33+2*f23*g31+2*f31*g23-2*f33*g21)*h33+(2*f23*g33+2*f33*g23)*h31+(2*f31*g33+2*f33*g31)*h23-2*f33*g33*h21)*w1+(-2*f21*g32+2*f22*g31+2*f31*g22-2*f32*g21)*h32+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*h31+(2*f31*g32+2*f32*g31)*h22+(2*f31*g31-2*f32*g32)*h21);
	mC1(4,6)=(((2*g23*g31-2*g21*g33)*h33+2*g23*g33*h31+2*g31*g33*h23-g332*h21)*w1+(2*g22*g31-2*g21*g32)*h32+(2*g22*g32+2*g21*g31)*h31+2*g31*g32*h22+(g312-g322)*h21);
	mC1(4,7)=((-f21*h332+(2*f23*h31+2*f31*h23-2*f33*h21)*h33+2*f33*h23*h31)*w1-f21*h322+(2*f22*h31+2*f31*h22-2*f32*h21)*h32+f21*h312+(2*f32*h22+2*f31*h21)*h31);
	mC1(4,8)=((-g21*h332+(2*g23*h31+2*g31*h23-2*g33*h21)*h33+2*g33*h23*h31)*w1-g21*h322+(2*g22*h31+2*g31*h22-2*g32*h21)*h32+g21*h312+(2*g32*h22+2*g31*h21)*h31);
	mC1(4,9)=((2*h23*h31*h33-h21*h332)*w1-h21*h322+2*h22*h31*h32+h21*h312);

	//S(2,2)=0

	mC0(5,0)=(f22*f232+2*f12*f13*f23-f132*f22)*w1+f223+(f212+f122-f112)*f22+2*f11*f12*f21;
	mC0(5,1)=((2*f22*f23+2*f12*f13)*g23+(f232-f132)*g22+(2*f12*f23-2*f13*f22)*g13+2*f13*f23*g12)*w1+(3*f222+f212+f122-f112)*g22+(2*f21*f22+2*f11*f12)*g21+(2*f12*f22+2*f11*f21)*g12+(2*f12*f21-2*f11*f22)*g11;
	mC0(5,2)=(f22*g232+(2*f23*g22+2*f12*g13+2*f13*g12)*g23-2*f13*g13*g22-f22*g132+2*f23*g12*g13)*w1+3*f22*g222+(2*f21*g21+2*f12*g12-2*f11*g11)*g22+f22*g212+(2*f11*g12+2*f12*g11)*g21+f22*g122+2*f21*g11*g12-f22*g112;
	mC0(5,3)=(g22*g232+2*g12*g13*g23-g132*g22)*w1+g223+(g212+g122-g112)*g22+2*g11*g12*g21;
	mC0(5,4)=((2*f22*f23+2*f12*f13)*h23+(f232-f132)*h22+(2*f12*f23-2*f13*f22)*h13+2*f13*f23*h12)*w1+(3*f222+f212+f122-f112)*h22+(2*f21*f22+2*f11*f12)*h21+(2*f12*f22+2*f11*f21)*h12+(2*f12*f21-2*f11*f22)*h11;
	mC0(5,5)=((2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*h23+(2*f23*g23-2*f13*g13)*h22+(2*f12*g23-2*f13*g22-2*f22*g13+2*f23*g12)*h13+(2*f13*g23+2*f23*g13)*h12)*w1+(6*f22*g22+2*f21*g21+2*f12*g12-2*f11*g11)*h22+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*h21+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*h12+(-2*f11*g22+2*f12*g21+2*f21*g12-2*f22*g11)*h11;
	mC0(5,6)=((2*g22*g23+2*g12*g13)*h23+(g232-g132)*h22+(2*g12*g23-2*g13*g22)*h13+2*g13*g23*h12)*w1+(3*g222+g212+g122-g112)*h22+(2*g21*g22+2*g11*g12)*h21+(2*g12*g22+2*g11*g21)*h12+(2*g12*g21-2*g11*g22)*h11;
	mC0(5,7)=(f22*h232+(2*f23*h22+2*f12*h13+2*f13*h12)*h23-2*f13*h13*h22-f22*h132+2*f23*h12*h13)*w1+3*f22*h222+(2*f21*h21+2*f12*h12-2*f11*h11)*h22+f22*h212+(2*f11*h12+2*f12*h11)*h21+f22*h122+2*f21*h11*h12-f22*h112;
	mC0(5,8)=(g22*h232+(2*g23*h22+2*g12*h13+2*g13*h12)*h23-2*g13*h13*h22-g22*h132+2*g23*h12*h13)*w1+3*g22*h222+(2*g21*h21+2*g12*h12-2*g11*h11)*h22+g22*h212+(2*g11*h12+2*g12*h11)*h21+g22*h122+2*g21*h11*h12-g22*h112;
	mC0(5,9)=(h22*h232+2*h12*h13*h23-h132*h22)*w1+h223+(h212+h122-h112)*h22+2*h11*h12*h21;

	mC1(5,0)=((2*f23*f32*f33-f22*f332)*w1+f22*f322+2*f21*f31*f32-f22*f312);
	mC1(5,1)=(((2*f23*f32-2*f22*f33)*g33+2*f23*f33*g32+2*f32*f33*g23-f332*g22)*w1+(2*f22*f32+2*f21*f31)*g32+(2*f21*f32-2*f22*f31)*g31+(f322-f312)*g22+2*f31*f32*g21);
	mC1(5,2)=((-f22*g332+(2*f23*g32+2*f32*g23-2*f33*g22)*g33+2*f33*g23*g32)*w1+f22*g322+(2*f21*g31+2*f32*g22+2*f31*g21)*g32-f22*g312+(2*f32*g21-2*f31*g22)*g31);
	mC1(5,3)=((2*g23*g32*g33-g22*g332)*w1+g22*g322+2*g21*g31*g32-g22*g312);
	mC1(5,4)=(((2*f23*f32-2*f22*f33)*h33+2*f23*f33*h32+2*f32*f33*h23-f332*h22)*w1+(2*f22*f32+2*f21*f31)*h32+(2*f21*f32-2*f22*f31)*h31+(f322-f312)*h22+2*f31*f32*h21);
	mC1(5,5)=(((-2*f22*g33+2*f23*g32+2*f32*g23-2*f33*g22)*h33+(2*f23*g33+2*f33*g23)*h32+(2*f32*g33+2*f33*g32)*h23-2*f33*g33*h22)*w1+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*h32+(2*f21*g32-2*f22*g31-2*f31*g22+2*f32*g21)*h31+(2*f32*g32-2*f31*g31)*h22+(2*f31*g32+2*f32*g31)*h21);
	mC1(5,6)=(((2*g23*g32-2*g22*g33)*h33+2*g23*g33*h32+2*g32*g33*h23-g332*h22)*w1+(2*g22*g32+2*g21*g31)*h32+(2*g21*g32-2*g22*g31)*h31+(g322-g312)*h22+2*g31*g32*h21);
	mC1(5,7)=((-f22*h332+(2*f23*h32+2*f32*h23-2*f33*h22)*h33+2*f33*h23*h32)*w1+f22*h322+(2*f21*h31+2*f32*h22+2*f31*h21)*h32-f22*h312+(2*f32*h21-2*f31*h22)*h31);
	mC1(5,8)=((-g22*h332+(2*g23*h32+2*g32*h23-2*g33*h22)*h33+2*g33*h23*h32)*w1+g22*h322+(2*g21*h31+2*g32*h22+2*g31*h21)*h32-g22*h312+(2*g32*h21-2*g31*h22)*h31);
	mC1(5,9)=((2*h23*h32*h33-h22*h332)*w1+h22*h322+2*h21*h31*h32-h22*h312);

	//S(2,3)=0

	mC0(6,0)=(f233+f132*f23)*w1+(f222+f212-f122-f112)*f23+2*f12*f13*f22+2*f11*f13*f21;
	mC0(6,1)=((3*f232+f132)*g23+2*f13*f23*g13)*w1+(f222+f212-f122-f112)*g23+(2*f22*f23+2*f12*f13)*g22+(2*f21*f23+2*f11*f13)*g21+(2*f12*f22+2*f11*f21)*g13+(2*f13*f22-2*f12*f23)*g12+(2*f13*f21-2*f11*f23)*g11;
	mC0(6,2)=(3*f23*g232+2*f13*g13*g23+f23*g132)*w1+(2*f22*g22+2*f21*g21-2*f12*g12-2*f11*g11)*g23+f23*g222+(2*f12*g13+2*f13*g12)*g22+f23*g212+(2*f11*g13+2*f13*g11)*g21+(2*f22*g12+2*f21*g11)*g13-f23*g122-f23*g112;
	mC0(6,3)=(g233+g132*g23)*w1+(g222+g212-g122-g112)*g23+2*g12*g13*g22+2*g11*g13*g21;
	mC0(6,4)=((3*f232+f132)*h23+2*f13*f23*h13)*w1+(f222+f212-f122-f112)*h23+(2*f22*f23+2*f12*f13)*h22+(2*f21*f23+2*f11*f13)*h21+(2*f12*f22+2*f11*f21)*h13+(2*f13*f22-2*f12*f23)*h12+(2*f13*f21-2*f11*f23)*h11;
	mC0(6,5)=((6*f23*g23+2*f13*g13)*h23+(2*f13*g23+2*f23*g13)*h13)*w1+(2*f22*g22+2*f21*g21-2*f12*g12-2*f11*g11)*h23+(2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*h22+(2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*h21+(2*f12*g22+2*f11*g21+2*f22*g12+2*f21*g11)*h13+(-2*f12*g23+2*f13*g22+2*f22*g13-2*f23*g12)*h12+(-2*f11*g23+2*f13*g21+2*f21*g13-2*f23*g11)*h11;
	mC0(6,6)=((3*g232+g132)*h23+2*g13*g23*h13)*w1+(g222+g212-g122-g112)*h23+(2*g22*g23+2*g12*g13)*h22+(2*g21*g23+2*g11*g13)*h21+(2*g12*g22+2*g11*g21)*h13+(2*g13*g22-2*g12*g23)*h12+(2*g13*g21-2*g11*g23)*h11;
	mC0(6,7)=(3*f23*h232+2*f13*h13*h23+f23*h132)*w1+(2*f22*h22+2*f21*h21-2*f12*h12-2*f11*h11)*h23+f23*h222+(2*f12*h13+2*f13*h12)*h22+f23*h212+(2*f11*h13+2*f13*h11)*h21+(2*f22*h12+2*f21*h11)*h13-f23*h122-f23*h112;
	mC0(6,8)=(3*g23*h232+2*g13*h13*h23+g23*h132)*w1+(2*g22*h22+2*g21*h21-2*g12*h12-2*g11*h11)*h23+g23*h222+(2*g12*h13+2*g13*h12)*h22+g23*h212+(2*g11*h13+2*g13*h11)*h21+(2*g22*h12+2*g21*h11)*h13-g23*h122-g23*h112;
	mC0(6,9)=(h233+h132*h23)*w1+(h222+h212-h122-h112)*h23+2*h12*h13*h22+2*h11*h13*h21;

	mC1(6,0)=(f23*f332*w1+(2*f22*f32+2*f21*f31)*f33-f23*f322-f23*f312);
	mC1(6,1)=((2*f23*f33*g33+f332*g23)*w1+(2*f22*f32+2*f21*f31)*g33+(2*f22*f33-2*f23*f32)*g32+(2*f21*f33-2*f23*f31)*g31+(-f322-f312)*g23+2*f32*f33*g22+2*f31*f33*g21);
	mC1(6,2)=((f23*g332+2*f33*g23*g33)*w1+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*g33-f23*g322+(2*f33*g22-2*f32*g23)*g32-f23*g312+(2*f33*g21-2*f31*g23)*g31);
	mC1(6,3)=(g23*g332*w1+(2*g22*g32+2*g21*g31)*g33-g23*g322-g23*g312);
	mC1(6,4)=((2*f23*f33*h33+f332*h23)*w1+(2*f22*f32+2*f21*f31)*h33+(2*f22*f33-2*f23*f32)*h32+(2*f21*f33-2*f23*f31)*h31+(-f322-f312)*h23+2*f32*f33*h22+2*f31*f33*h21);
	mC1(6,5)=(((2*f23*g33+2*f33*g23)*h33+2*f33*g33*h23)*w1+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*h33+(2*f22*g33-2*f23*g32-2*f32*g23+2*f33*g22)*h32+(2*f21*g33-2*f23*g31-2*f31*g23+2*f33*g21)*h31+(-2*f32*g32-2*f31*g31)*h23+(2*f32*g33+2*f33*g32)*h22+(2*f31*g33+2*f33*g31)*h21);
	mC1(6,6)=((2*g23*g33*h33+g332*h23)*w1+(2*g22*g32+2*g21*g31)*h33+(2*g22*g33-2*g23*g32)*h32+(2*g21*g33-2*g23*g31)*h31+(-g322-g312)*h23+2*g32*g33*h22+2*g31*g33*h21);
	mC1(6,7)=((f23*h332+2*f33*h23*h33)*w1+(2*f22*h32+2*f21*h31+2*f32*h22+2*f31*h21)*h33-f23*h322+(2*f33*h22-2*f32*h23)*h32-f23*h312+(2*f33*h21-2*f31*h23)*h31);
	mC1(6,8)=((g23*h332+2*g33*h23*h33)*w1+(2*g22*h32+2*g21*h31+2*g32*h22+2*g31*h21)*h33-g23*h322+(2*g33*h22-2*g32*h23)*h32-g23*h312+(2*g33*h21-2*g31*h23)*h31);
	mC1(6,9)=(h23*h332*w1+(2*h22*h32+2*h21*h31)*h33-h23*h322-h23*h312);

	//S(3,1)=0

	mC0(7,0)=((2*f21*f23+2*f11*f13)*f33+(-f232-f132)*f31)*w1+(2*f21*f22+2*f11*f12)*f32+(-f222+f212-f122+f112)*f31;
	mC0(7,1)=((2*f21*f23+2*f11*f13)*g33+(-f232-f132)*g31+(2*f21*f33-2*f23*f31)*g23+2*f23*f33*g21+(2*f11*f33-2*f13*f31)*g13+2*f13*f33*g11)*w1+(2*f21*f22+2*f11*f12)*g32+(-f222+f212-f122+f112)*g31+(2*f21*f32-2*f22*f31)*g22+(2*f22*f32+2*f21*f31)*g21+(2*f11*f32-2*f12*f31)*g12+(2*f12*f32+2*f11*f31)*g11;
	mC0(7,2)=((2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*g33+(-2*f23*g23-2*f13*g13)*g31-f31*g232+2*f33*g21*g23-f31*g132+2*f33*g11*g13)*w1+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*g32+(-2*f22*g22+2*f21*g21-2*f12*g12+2*f11*g11)*g31-f31*g222+2*f32*g21*g22+f31*g212-f31*g122+2*f32*g11*g12+f31*g112;
	mC0(7,3)=((2*g21*g23+2*g11*g13)*g33+(-g232-g132)*g31)*w1+(2*g21*g22+2*g11*g12)*g32+(-g222+g212-g122+g112)*g31;
	mC0(7,4)=((2*f21*f23+2*f11*f13)*h33+(-f232-f132)*h31+(2*f21*f33-2*f23*f31)*h23+2*f23*f33*h21+(2*f11*f33-2*f13*f31)*h13+2*f13*f33*h11)*w1+(2*f21*f22+2*f11*f12)*h32+(-f222+f212-f122+f112)*h31+(2*f21*f32-2*f22*f31)*h22+(2*f22*f32+2*f21*f31)*h21+(2*f11*f32-2*f12*f31)*h12+(2*f12*f32+2*f11*f31)*h11;
	mC0(7,5)=((2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*h33+(-2*f23*g23-2*f13*g13)*h31+(2*f21*g33-2*f23*g31-2*f31*g23+2*f33*g21)*h23+(2*f23*g33+2*f33*g23)*h21+(2*f11*g33-2*f13*g31-2*f31*g13+2*f33*g11)*h13+(2*f13*g33+2*f33*g13)*h11)*w1+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*h32+(-2*f22*g22+2*f21*g21-2*f12*g12+2*f11*g11)*h31+(2*f21*g32-2*f22*g31-2*f31*g22+2*f32*g21)*h22+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*h21+(2*f11*g32-2*f12*g31-2*f31*g12+2*f32*g11)*h12+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*h11;
	mC0(7,6)=((2*g21*g23+2*g11*g13)*h33+(-g232-g132)*h31+(2*g21*g33-2*g23*g31)*h23+2*g23*g33*h21+(2*g11*g33-2*g13*g31)*h13+2*g13*g33*h11)*w1+(2*g21*g22+2*g11*g12)*h32+(-g222+g212-g122+g112)*h31+(2*g21*g32-2*g22*g31)*h22+(2*g22*g32+2*g21*g31)*h21+(2*g11*g32-2*g12*g31)*h12+(2*g12*g32+2*g11*g31)*h11;
	mC0(7,7)=((2*f21*h23+2*f23*h21+2*f11*h13+2*f13*h11)*h33+(-2*f23*h23-2*f13*h13)*h31-f31*h232+2*f33*h21*h23-f31*h132+2*f33*h11*h13)*w1+(2*f21*h22+2*f22*h21+2*f11*h12+2*f12*h11)*h32+(-2*f22*h22+2*f21*h21-2*f12*h12+2*f11*h11)*h31-f31*h222+2*f32*h21*h22+f31*h212-f31*h122+2*f32*h11*h12+f31*h112;
	mC0(7,8)=((2*g21*h23+2*g23*h21+2*g11*h13+2*g13*h11)*h33+(-2*g23*h23-2*g13*h13)*h31-g31*h232+2*g33*h21*h23-g31*h132+2*g33*h11*h13)*w1+(2*g21*h22+2*g22*h21+2*g11*h12+2*g12*h11)*h32+(-2*g22*h22+2*g21*h21-2*g12*h12+2*g11*h11)*h31-g31*h222+2*g32*h21*h22+g31*h212-g31*h122+2*g32*h11*h12+g31*h112;
	mC0(7,9)=((2*h21*h23+2*h11*h13)*h33+(-h232-h132)*h31)*w1+(2*h21*h22+2*h11*h12)*h32+(-h222+h212-h122+h112)*h31;

	mC1(7,0)=(f31*f332*w1+f31*f322+f313);
	mC1(7,1)=((2*f31*f33*g33+f332*g31)*w1+2*f31*f32*g32+(f322+3*f312)*g31);
	mC1(7,2)=((f31*g332+2*f33*g31*g33)*w1+f31*g322+2*f32*g31*g32+3*f31*g312);
	mC1(7,3)=(g31*g332*w1+g31*g322+g313);
	mC1(7,4)=((2*f31*f33*h33+f332*h31)*w1+2*f31*f32*h32+(f322+3*f312)*h31);
	mC1(7,5)=(((2*f31*g33+2*f33*g31)*h33+2*f33*g33*h31)*w1+(2*f31*g32+2*f32*g31)*h32+(2*f32*g32+6*f31*g31)*h31);
	mC1(7,6)=((2*g31*g33*h33+g332*h31)*w1+2*g31*g32*h32+(g322+3*g312)*h31);
	mC1(7,7)=((f31*h332+2*f33*h31*h33)*w1+f31*h322+2*f32*h31*h32+3*f31*h312);
	mC1(7,8)=((g31*h332+2*g33*h31*h33)*w1+g31*h322+2*g32*h31*h32+3*g31*h312);
	mC1(7,9)=(h31*h332*w1+h31*h322+h313);

	//S(3,2)=0

	mC0(8,0)=((2*f22*f23+2*f12*f13)*f33+(-f232-f132)*f32)*w1+(f222-f212+f122-f112)*f32+(2*f21*f22+2*f11*f12)*f31;
	mC0(8,1)=((2*f22*f23+2*f12*f13)*g33+(-f232-f132)*g32+(2*f22*f33-2*f23*f32)*g23+2*f23*f33*g22+(2*f12*f33-2*f13*f32)*g13+2*f13*f33*g12)*w1+(f222-f212+f122-f112)*g32+(2*f21*f22+2*f11*f12)*g31+(2*f22*f32+2*f21*f31)*g22+(2*f22*f31-2*f21*f32)*g21+(2*f12*f32+2*f11*f31)*g12+(2*f12*f31-2*f11*f32)*g11;
	mC0(8,2)=((2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*g33+(-2*f23*g23-2*f13*g13)*g32-f32*g232+2*f33*g22*g23-f32*g132+2*f33*g12*g13)*w1+(2*f22*g22-2*f21*g21+2*f12*g12-2*f11*g11)*g32+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*g31+f32*g222+2*f31*g21*g22-f32*g212+f32*g122+2*f31*g11*g12-f32*g112;
	mC0(8,3)=((2*g22*g23+2*g12*g13)*g33+(-g232-g132)*g32)*w1+(g222-g212+g122-g112)*g32+(2*g21*g22+2*g11*g12)*g31;
	mC0(8,4)=((2*f22*f23+2*f12*f13)*h33+(-f232-f132)*h32+(2*f22*f33-2*f23*f32)*h23+2*f23*f33*h22+(2*f12*f33-2*f13*f32)*h13+2*f13*f33*h12)*w1+(f222-f212+f122-f112)*h32+(2*f21*f22+2*f11*f12)*h31+(2*f22*f32+2*f21*f31)*h22+(2*f22*f31-2*f21*f32)*h21+(2*f12*f32+2*f11*f31)*h12+(2*f12*f31-2*f11*f32)*h11;
	mC0(8,5)=((2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*h33+(-2*f23*g23-2*f13*g13)*h32+(2*f22*g33-2*f23*g32-2*f32*g23+2*f33*g22)*h23+(2*f23*g33+2*f33*g23)*h22+(2*f12*g33-2*f13*g32-2*f32*g13+2*f33*g12)*h13+(2*f13*g33+2*f33*g13)*h12)*w1+(2*f22*g22-2*f21*g21+2*f12*g12-2*f11*g11)*h32+(2*f21*g22+2*f22*g21+2*f11*g12+2*f12*g11)*h31+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*h22+(-2*f21*g32+2*f22*g31+2*f31*g22-2*f32*g21)*h21+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*h12+(-2*f11*g32+2*f12*g31+2*f31*g12-2*f32*g11)*h11;
	mC0(8,6)=((2*g22*g23+2*g12*g13)*h33+(-g232-g132)*h32+(2*g22*g33-2*g23*g32)*h23+2*g23*g33*h22+(2*g12*g33-2*g13*g32)*h13+2*g13*g33*h12)*w1+(g222-g212+g122-g112)*h32+(2*g21*g22+2*g11*g12)*h31+(2*g22*g32+2*g21*g31)*h22+(2*g22*g31-2*g21*g32)*h21+(2*g12*g32+2*g11*g31)*h12+(2*g12*g31-2*g11*g32)*h11;
	mC0(8,7)=((2*f22*h23+2*f23*h22+2*f12*h13+2*f13*h12)*h33+(-2*f23*h23-2*f13*h13)*h32-f32*h232+2*f33*h22*h23-f32*h132+2*f33*h12*h13)*w1+(2*f22*h22-2*f21*h21+2*f12*h12-2*f11*h11)*h32+(2*f21*h22+2*f22*h21+2*f11*h12+2*f12*h11)*h31+f32*h222+2*f31*h21*h22-f32*h212+f32*h122+2*f31*h11*h12-f32*h112;
	mC0(8,8)=((2*g22*h23+2*g23*h22+2*g12*h13+2*g13*h12)*h33+(-2*g23*h23-2*g13*h13)*h32-g32*h232+2*g33*h22*h23-g32*h132+2*g33*h12*h13)*w1+(2*g22*h22-2*g21*h21+2*g12*h12-2*g11*h11)*h32+(2*g21*h22+2*g22*h21+2*g11*h12+2*g12*h11)*h31+g32*h222+2*g31*h21*h22-g32*h212+g32*h122+2*g31*h11*h12-g32*h112;
	mC0(8,9)=((2*h22*h23+2*h12*h13)*h33+(-h232-h132)*h32)*w1+(h222-h212+h122-h112)*h32+(2*h21*h22+2*h11*h12)*h31;

	mC1(8,0)=(f32*f332*w1+f323+f312*f32);
	mC1(8,1)=((2*f32*f33*g33+f332*g32)*w1+(3*f322+f312)*g32+2*f31*f32*g31);
	mC1(8,2)=((f32*g332+2*f33*g32*g33)*w1+3*f32*g322+2*f31*g31*g32+f32*g312);
	mC1(8,3)=(g32*g332*w1+g323+g312*g32);
	mC1(8,4)=((2*f32*f33*h33+f332*h32)*w1+(3*f322+f312)*h32+2*f31*f32*h31);
	mC1(8,5)=(((2*f32*g33+2*f33*g32)*h33+2*f33*g33*h32)*w1+(6*f32*g32+2*f31*g31)*h32+(2*f31*g32+2*f32*g31)*h31);
	mC1(8,6)=((2*g32*g33*h33+g332*h32)*w1+(3*g322+g312)*h32+2*g31*g32*h31);
	mC1(8,7)=((f32*h332+2*f33*h32*h33)*w1+3*f32*h322+2*f31*h31*h32+f32*h312);
	mC1(8,8)=((g32*h332+2*g33*h32*h33)*w1+3*g32*h322+2*g31*h31*h32+g32*h312);
	mC1(8,9)=((f32*h332+2*f33*h32*h33)*w1+3*f32*h322+2*f31*h31*h32+f32*h312);

	//S(3,3)=0

	mC0(9,0)=(f232+f132)*f33*w1+(-f222-f212-f122-f112)*f33+(2*f22*f23+2*f12*f13)*f32+(2*f21*f23+2*f11*f13)*f31;
	mC0(9,1)=((f232+f132)*g33+2*f23*f33*g23+2*f13*f33*g13)*w1+(-f222-f212-f122-f112)*g33+(2*f22*f23+2*f12*f13)*g32+(2*f21*f23+2*f11*f13)*g31+(2*f22*f32+2*f21*f31)*g23+(2*f23*f32-2*f22*f33)*g22+(2*f23*f31-2*f21*f33)*g21+(2*f12*f32+2*f11*f31)*g13+(2*f13*f32-2*f12*f33)*g12+(2*f13*f31-2*f11*f33)*g11;
	mC0(9,2)=((2*f23*g23+2*f13*g13)*g33+f33*g232+f33*g132)*w1+(-2*f22*g22-2*f21*g21-2*f12*g12-2*f11*g11)*g33+(2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*g32+(2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*g31+(2*f32*g22+2*f31*g21)*g23-f33*g222-f33*g212+(2*f32*g12+2*f31*g11)*g13-f33*g122-f33*g112;
	mC0(9,3)=(g232+g132)*g33*w1+(-g222-g212-g122-g112)*g33+(2*g22*g23+2*g12*g13)*g32+(2*g21*g23+2*g11*g13)*g31;
	mC0(9,4)=((f232+f132)*h33+2*f23*f33*h23+2*f13*f33*h13)*w1+(-f222-f212-f122-f112)*h33+(2*f22*f23+2*f12*f13)*h32+(2*f21*f23+2*f11*f13)*h31+(2*f22*f32+2*f21*f31)*h23+(2*f23*f32-2*f22*f33)*h22+(2*f23*f31-2*f21*f33)*h21+(2*f12*f32+2*f11*f31)*h13+(2*f13*f32-2*f12*f33)*h12+(2*f13*f31-2*f11*f33)*h11;
	mC0(9,5)=((2*f23*g23+2*f13*g13)*h33+(2*f23*g33+2*f33*g23)*h23+(2*f13*g33+2*f33*g13)*h13)*w1+(-2*f22*g22-2*f21*g21-2*f12*g12-2*f11*g11)*h33+(2*f22*g23+2*f23*g22+2*f12*g13+2*f13*g12)*h32+(2*f21*g23+2*f23*g21+2*f11*g13+2*f13*g11)*h31+(2*f22*g32+2*f21*g31+2*f32*g22+2*f31*g21)*h23+(-2*f22*g33+2*f23*g32+2*f32*g23-2*f33*g22)*h22+(-2*f21*g33+2*f23*g31+2*f31*g23-2*f33*g21)*h21+(2*f12*g32+2*f11*g31+2*f32*g12+2*f31*g11)*h13+(-2*f12*g33+2*f13*g32+2*f32*g13-2*f33*g12)*h12+(-2*f11*g33+2*f13*g31+2*f31*g13-2*f33*g11)*h11;
	mC0(9,6)=((g232+g132)*h33+2*g23*g33*h23+2*g13*g33*h13)*w1+(-g222-g212-g122-g112)*h33+(2*g22*g23+2*g12*g13)*h32+(2*g21*g23+2*g11*g13)*h31+(2*g22*g32+2*g21*g31)*h23+(2*g23*g32-2*g22*g33)*h22+(2*g23*g31-2*g21*g33)*h21+(2*g12*g32+2*g11*g31)*h13+(2*g13*g32-2*g12*g33)*h12+(2*g13*g31-2*g11*g33)*h11;
	mC0(9,7)=((2*f23*h23+2*f13*h13)*h33+f33*h232+f33*h132)*w1+(-2*f22*h22-2*f21*h21-2*f12*h12-2*f11*h11)*h33+(2*f22*h23+2*f23*h22+2*f12*h13+2*f13*h12)*h32+(2*f21*h23+2*f23*h21+2*f11*h13+2*f13*h11)*h31+(2*f32*h22+2*f31*h21)*h23-f33*h222-f33*h212+(2*f32*h12+2*f31*h11)*h13-f33*h122-f33*h112;
	mC0(9,8)=((2*g23*h23+2*g13*h13)*h33+g33*h232+g33*h132)*w1+(-2*g22*h22-2*g21*h21-2*g12*h12-2*g11*h11)*h33+(2*g22*h23+2*g23*h22+2*g12*h13+2*g13*h12)*h32+(2*g21*h23+2*g23*h21+2*g11*h13+2*g13*h11)*h31+(2*g32*h22+2*g31*h21)*h23-g33*h222-g33*h212+(2*g32*h12+2*g31*h11)*h13-g33*h122-g33*h112;
	mC0(9,9)=(h232+h132)*h33*w1+(-h222-h212-h122-h112)*h33+(2*h22*h23+2*h12*h13)*h32+(2*h21*h23+2*h11*h13)*h31;

	mC1(9,0)=(f333*w1+(f322+f312)*f33);
	mC1(9,1)=(3*f332*g33*w1+(f322+f312)*g33+2*f32*f33*g32+2*f31*f33*g31);
	mC1(9,2)=(3*f33*g332*w1+(2*f32*g32+2*f31*g31)*g33+f33*g322+f33*g312);
	mC1(9,3)=(g333*w1+(g322+g312)*g33);
	mC1(9,4)=(3*f332*h33*w1+(f322+f312)*h33+2*f32*f33*h32+2*f31*f33*h31);
	mC1(9,5)=(6*f33*g33*h33*w1+(2*f32*g32+2*f31*g31)*h33+(2*f32*g33+2*f33*g32)*h32+(2*f31*g33+2*f33*g31)*h31);
	mC1(9,6)=(3*g332*h33*w1+(g322+g312)*h33+2*g32*g33*h32+2*g31*g33*h31);
	mC1(9,7)=(3*f33*h332*w1+(2*f32*h32+2*f31*h31)*h33+f33*h322+f33*h312);
	mC1(9,8)=(3*g33*h332*w1+(2*g32*h32+2*g31*h31)*h33+g33*h322+g33*h312);
	mC1(9,9)=(h333*w1+(h322+h312)*h33);

	return make_tuple(mC0, mC1);
}	//tuple<Matrix10T, Matrix10T> ComputeCoefficients(const list<EpipolarMatrixT>& basis) const
}	//GeometryN
}	//SeeSawN

