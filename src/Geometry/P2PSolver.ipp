/**
 * @file P2PSolver.ipp Implementation of the 2-point camera orientation solver
 * @author Evren Imre
 * @date 7 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2P_SOLVER_IPP_2790321
#define P2P_SOLVER_IPP_2790321

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/algorithm.hpp>
#include <type_traits>
#include <list>
#include "GeometrySolverBase.h"
#include "Rotation3DSolver.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/Complexity.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::range::for_each;
using boost::ForwardRangeConcept;
using std::is_same;
using std::list;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
using SeeSawN::NumericN::ComplexityEstimatorC;

/**
 * @brief 2-point orientation solver
 * @remarks E. Imre, J.-Y. Guillemaut, A. Hilton, "Calibration of Nodal and Free-Moving Cameras in Dynamic Scenes for Post-Production", 3DIMPVT 2011
 * @remarks The solver assumes that
 * 	- The camera centre is at the world origin. Therefore subtract the known camera centre from the scene points
 * 	- The focal length is 1 and the principal point is the origin of the image plane. Therefore, normalise the image coordinates with the inverse of the intrinsic calibration matrix
 * 	- The scene points satisfy the cheirality constraints, i.e. in front of the camera. When this cannot be guaranteed, the solver will still return a valid rotation, however, it is unlikely to gather much support.
 * @remarks Distance measure: Angle between the two orientations, normalised to the range=[0,1]
 * @remarks Minimal parameterisation: Rotation vector
 * @remarks For interoperability with the other calibration estimators, the rotation is embedded into a camera matrix
 * @warning For the normalisation of the coordinate magnitudes (i.e. following the normalisation with the intrinsics), avoid non-isotropic scaling!
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class P2PSolverC : public GeometrySolverBaseC<P2PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 2, true, 1, 12, 3>
{
	private:

		typedef GeometrySolverBaseC<P2PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 2, true, 1, 12, 3> BaseT;	///< Type of the geometry solver base

		typedef typename BaseT::model_type ModelT;	///<  CameraMatrixT which contains the rotation
		typedef Rotation3DSolverC::uncertainty_type GaussianT;	///< Type of the Gaussian for the sample statistics

		/** @name Implementation details */ //@{
		static Rotation3DSolverC::model_type MakeRotation(const ModelT& model);	///< Embeds a camera matrix into a 4x4 identity homography
		//@}

	public:


		/** @name GeometrySolverConceptC interface */ //@{
		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the rotation that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef Rotation3DSolverC::minimal_model_type minimal_model_type;	///< A minimally parameterised model. Axis-angle vector
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class OrientationRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const OrientationRangeT& orientationList, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of rotations

		//@}

		/** @name Overrides */ //@{
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool dummy=true);	///< Converts a vector to a model
		//@}
};	//class P2PSolverC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre First 3x3 submatrix of \c model represents a valid rotation (unenforced)
 * @return Minimal form
 */
template<class DummyRangeT>
auto P2PSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	return Rotation3DSolverC::MakeMinimal(MakeRotation(model), dummy);
}	//auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of rotations
 * @tparam OrientationRangeT A range of 3D orientations
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] orientationList Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c OrientationRangeT is a forward range of elements of type \c CameraMatrixT
 */
template<class OrientationRangeT, class WeightRangeT, class DummyRangeT>
auto P2PSolverC::ComputeSampleStatistics(const OrientationRangeT& orientationList, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<OrientationRangeT>));
	static_assert(is_same<ModelT, typename range_value<OrientationRangeT>::type >::value, "P2PSolverC::ComputeSampleStatistics : OrientationRangeT must hold elements of type CameraMatrixT");

	list<typename Rotation3DSolverC::model_type> rotations;
	for_each(orientationList, [&](const ModelT& current){ rotations.push_back(MakeRotation(current)); } );

	return Rotation3DSolverC::ComputeSampleStatistics(rotations, wMean, wCovariance, dummy);
}	//auto ComputeSampleStatistics(const OrientationRangeT& orientationList, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT

/**
 * @brief Computes the rotation that best explains the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding a camera matrix
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap of elements of type \c Coordinate3DT and \c Coordinate2DT
 */
template<class CorrespondenceRangeT>
auto P2PSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "P2PSolverC::operator() : CorrespondenceRangeT::left_key_type must be Coordinate3DT");
	static_assert(is_same<Coordinate2DT, typename CorrespondenceRangeT::right_key_type>::value, "P2PSolverC::operator() : CorrespondenceRangeT::right_key_type must be Coordinate2DT");

	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	//Coordinates of the 3D points in the camera reference frame
	CoordinateCorrespondenceList3DT sceneCorrespondences;
	for(const auto& current : correspondences)
	{
		//Direction vector : [x,y,1], as the image coordinates are normalised
		//Distance to the camera centre is preserved in both reference frames
		Coordinate3DT point2 = current.left.norm() * Coordinate3DT(current.right[0], current.right[1], 1).normalized();
		sceneCorrespondences.push_back(typename CoordinateCorrespondenceList3DT::value_type(current.left, point2));
	}	//for(const auto& current : correspondences)

	//Compute the rotation
	Rotation3DSolverC rotationSolver;
	list<typename Rotation3DSolverC::model_type> rotationList=rotationSolver(sceneCorrespondences);

	//Output
	list<ModelT> output;
	if(!rotationList.empty())
		output.push_back(rotationList.begin()->block(0,0,3,4));

	return output;
}	//auto operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>

}	//GeometryN
}	//SeeSawN

#endif /* P2PSOLVER_IPP_ */
