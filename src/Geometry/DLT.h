/**
 * @file DLT.h Public interface for DLTC
 * @author Evren Imre
 * @date 11 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DLT_H_5812123
#define DLT_H_5812123

#include "DLT.ipp"
namespace SeeSawN
{
namespace GeometryN
{

template<class DLTProblemT> class DLTC;	///< Normalised discrete linear transformation algorithm
template<class SolutionT, class MCoefficientT, class GCoefficientT, class CategoryTag, unsigned int GENERATOR_SIZE, unsigned int MIN_RANK, unsigned int MAX_RANK> struct DLTProblemC;	///< DLT problem template

namespace DetailN
{
struct Homography2Tag;	///< 2D homography estimation
struct Homography3Tag;	///< 3D homography estimation
struct Homography32Tag;	///< 3D-2D homography estimation
struct EpipolarTag;	///< Epipolar geometry problems
};	//DetailN
}	//GeometryN
}	//SeeSawN

#endif /* DLT_H_5812123 */
