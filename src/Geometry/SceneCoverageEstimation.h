/**
 * @file SceneCoverageEstimation.h Public interface for \c SceneCoverageEstimationC
 * @author Evren Imre
 * @date 7 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_COVERAGE_ESTIMATION_H_3872109
#define SCENE_COVERAGE_ESTIMATION_H_3872109

#include "SceneCoverageEstimation.ipp"
namespace SeeSawN
{
namespace GeometryN
{

struct SceneCoverageEstimationParametersC;	///< Parameters for \c SceneCoverageEstimationC
struct SceneCoverageEstimationDiagnosticsC;	///< Diagnostics for \c SceneCoverageEstimationC
class SceneCoverageEstimationC;	///< Coverage and related statistics for a set of cameras


}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template class std::map<unsigned int, std::size_t, std::greater<unsigned int>>;
extern template class std::map<unsigned int, unsigned int>;
extern template class std::set<unsigned int>;
extern template class std::tuple<unsigned int, unsigned int>;
extern template class std::tuple<double, double>;
#endif /* SCENECOVERAGEESTIMATION_H_ */
