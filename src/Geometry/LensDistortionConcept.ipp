/**
 * @file LensDistortionConcept.ipp Implementation of LensDistortionConceptC
 * @author Evren Imre
 * @date 8 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LENS_DISTORTION_CONCEPT_IPP_0987545
#define LENS_DISTORTION_CONCEPT_IPP_0987545

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::CopyConstructible;
using boost::Assignable;
using boost::optional;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Lens distortion concept checker
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class LensDistortionConceptC
{
    ///@cond CONCEPT_CHECK

		BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
		BOOST_CONCEPT_ASSERT((Assignable<TestT>));

    public:

        BOOST_CONCEPT_USAGE(LensDistortionConceptC)
        {
            TestT tested1;

            auto distortionModel=TestT::modelCode; (void)distortionModel;

            Coordinate2DT centre;
            typename ValueTypeM<Coordinate2DT>::type kappa;
            TestT tested2(centre, kappa);

            optional<Coordinate2DT> out1=tested2.Apply(Coordinate2DT()); (void)out1;
            optional<Coordinate2DT> out2=tested2.Correct(Coordinate2DT()); (void)out2;
        }   //BOOST_CONCEPT_USAGE(LensDistortionConceptC)
    ///@endcond

};  //class LensDistortionConceptC

}   //GeometryN
}	//SeeSawN

#endif /* LENS_DISTORTION_CONCEPT_IPP_0987545 */
