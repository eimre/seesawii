/**
 * @file CoordinateTransformation.ipp Implementation of various coordinate transformations
 * @author Evren Imre
 * @date 24 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COORDINATE_TRANSFORMATION_IPP_6151325
#define COORDINATE_TRANSFORMATION_IPP_6151325

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/algorithm/transform.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <set>
#include <cstddef>
#include <cmath>
#include <type_traits>
#include <tuple>
#include <algorithm>
#include "../Wrappers/EigenMetafunction.h"
#include "../Elements/Coordinate.h"
#include "CoordinateStatistics.h"

#include <iostream>
namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using Eigen::Transform;
using Eigen::Array;
using Eigen::Matrix;
using std::vector;
using std::set;
using std::size_t;
using std::ceil;
using std::round;
using std::pow;
using std::abs;
using std::is_same;
using std::is_convertible;
using std::is_floating_point;
using std::tie;
using SeeSawN::ElementsN::CoordinateVectorT;
using SeeSawN::ElementsN::HCoordinateVectorT;
using SeeSawN::GeometryN::CoordinateStatisticsT;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Coordinate transformations
 * @tparam ValueT Type of a coordinate entry
 * @tparam DIM Dimensionality of a coordinate
 * @pre \c ValueT is a floating point type
 * @remarks Only for fixed-size structures (a limitation of Eigen.Geometry)
 * @ingroup Geometry
 * @nosubgrouping
 */
template<typename ValueT, unsigned int DIM>
class CoordinateTransformationsC
{
	static_assert(is_floating_point<ValueT>::value, "CoordinateTransformationsC : ValueT must be a floating point type");

    private:

		typedef CoordinateVectorT<ValueT, DIM> CoordinateT;	///< Coordinate type
		typedef HCoordinateVectorT<ValueT, DIM> HCoordinateT;	///< Coordinate type
        typedef Transform<ValueT, DIM, Eigen::Affine> AffineTransformT; ///< An affine transform
        typedef Transform<ValueT, DIM, Eigen::Projective> ProjectiveTransformT; ///< A projective transform

    public:

        typedef CoordinateT coordinate_type;	///< Coordinate type
        typedef HCoordinateT homogeneous_coordinate_type;	///< Homogeneous coordinate type
        typedef AffineTransformT affine_transform_type;	 ///< An affine transform
        typedef ProjectiveTransformT  projective_transform_type;	///< A projective transform

        typedef Array<ValueT, DIM, 1> ArrayD;  ///< A D-dimensional double vector, as array
        typedef Array<ValueT, DIM, 2> ArrayD2;  ///< A Dx2 array

        /** @name Algebraic transformations */ //@{
        template<class CoordinateRangeT> static AffineTransformT ComputeNormaliser(const CoordinateRangeT& coordinates, bool flagIsotropic=false);    ///< Computes the normalising transformation, which transforms the coordinate set to have 0 mean and unity standard deviation
        template<class CoordinateRangeT> static vector<HCoordinateT> ApplyProjectiveTransformation(const CoordinateRangeT& coordinates, const ProjectiveTransformT& mT);    ///< Applies a projective transformation to a range of coordinates
        template<class CoordinateRangeT> static vector<CoordinateT> ApplyAffineTransformation(const CoordinateRangeT& coordinates, const AffineTransformT& mT);    ///< Applies an affine transformation to a range of coordinates
        template<class CoordinateRangeT, class MatrixT> static vector< Matrix<ValueT, RowsM<MatrixT>::value, 1> > HomogeneousProject(const CoordinateRangeT& coordinates, const MatrixT& mP); ///< Homogeneous projection of coordinates to a space with a possibly different dimensionality

        template<class CoordinateRangeT> static vector<CoordinateT> NormaliseHomogeneous(const CoordinateRangeT& coordinates); ///< Normalises a range of homogeneous vectors

        static CoordinateT TransformCoordinate(const CoordinateT& coordinate, const ProjectiveTransformT& mT);	///< Applies a homogeneous transformation
        static CoordinateT TransformCoordinate(const CoordinateT& coordinate, const AffineTransformT& mT);	///< Applies an affine transformation
        //@}

        /** @name Quantisation */ //@{
        template<class CoordinateRangeT> static vector<vector<unsigned int> > Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent, bool flagOverlap);  ///<Assigns the coordinates to buckets
 //       template<class CoordinateRangeT> static vector<set<unsigned int> > Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent, bool flagOverlap);  ///<Assigns the coordinates to buckets
        template<class CoordinateRangeT> static vector<unsigned int> Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent);  ///<Assigns the coordinates to buckets- no overlap variant
        //@}
};  //class CoordinateTransformationsC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Computes the normalising transformation, which transforms the coordinate set to have 0 mean and unity standard deviation
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinates to be normalised
 * @param[in] flagIsotropic If \c true , the transformation performs isotropic scaling: standard deviation is not guaranteed to be unity in each dimension, however the average distance is still \c sqrt(DIM)
 * @return An affine normalising transformation
 * @pre \c CoordinateRangeT is a model of ForwardRangeConcept, holding elements of type CoordinateT
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
auto CoordinateTransformationsC<ValueT, DIM>::ComputeNormaliser(const CoordinateRangeT& coordinates, bool flagIsotropic)-> AffineTransformT
{
    //Precondition
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::ComputeNormaliser : CoordinateRangeT must hold elements of type coordinate_type");

    AffineTransformT mT;
    mT.setIdentity();

    if(boost::empty(coordinates) )
        return mT;

    ArrayD meanX;
    ArrayD stdX;
    tie(meanX, stdX)=*CoordinateStatisticsT<CoordinateT>::ComputeMeanAndStD(coordinates);

    ArrayD invScale;	//Inverse scale
    if(flagIsotropic)
    {
    	ValueT tmp=stdX.square().sum();
    	invScale.setConstant(sqrt(tmp/DIM));
    }	//if(flagIsotropic)
    else
    	invScale=stdX;

    //Set all 0 entries to 1. A 0 std happens only if all elements are identical. In that case, the translation part already maps them to 0
    invScale = (invScale!=0).select(invScale,1);

    //Compose the transformation
    mT.translate(-meanX.matrix());
    mT.prescale((invScale.inverse()).matrix());

    return mT;
}   //AffineTransformT Normalise(CoordinateRangeT& coordinates);

/**
 * @brief Applies a projective transformation to a range of coordinates
 * @tparam CoordinateRangeT A range of input coordinates
 * @param[in] coordinates Coordinates to be transformed, in non-homogeneous form
 * @param[in] mT Transformation
 * @return A vector of transformed coordinates, in homogeneous form
 * @pre \c CoordinateRangeT is a model of ForwardRangeConcept, holding elements of type CoordinateT
 * @pre Floating point types are compatible
 * @ingroup Geometry
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
auto CoordinateTransformationsC<ValueT, DIM>::ApplyProjectiveTransformation(const CoordinateRangeT& coordinates, const ProjectiveTransformT& mT) -> vector<HCoordinateT>
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::ApplyProjectiveTransformation : CoordinateRangeT must hold elements of type coordinate_type");

    vector<HCoordinateT> output;
    if(boost::empty(coordinates))
        return output;

    output.resize(boost::distance(coordinates));

    auto functor=[&](const CoordinateT& current){return mT*current.homogeneous();};
    boost::transform(coordinates, output.begin(), functor);

    return output;
}   //vector<CoordinateT> ApplyProjectiveTransformation(const CoordinateRangeT& coordinates, const ProjectiveTransformT& mT)

/**
 * @brief Applies an affine transformation to a range of coordinates
 * @tparam CoordinateRangeT A range of input coordinates
 * @param[in] coordinates Coordinates to be transformed
 * @param[in] mT Transformation
 * @return A vector of transformed coordinates
 * @pre \c CoordinateRangeT is a model of ForwardRangeConcept, holding elements of type CoordinateT
 * @ingroup Geometry
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
auto CoordinateTransformationsC<ValueT, DIM>::ApplyAffineTransformation(const CoordinateRangeT& coordinates, const AffineTransformT& mT) -> vector<CoordinateT>
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::ApplyAffineTransformation : CoordinateRangeT must hold elements of type coordinate_type");

    vector<CoordinateT> output;
    if(boost::empty(coordinates))
        return output;

    //Preconditions
    assert(mT.matrix().rows() == coordinates.begin()->size());

    output.reserve(boost::distance(coordinates));
    boost::for_each(coordinates, [&](const CoordinateT& current){output.emplace_back(mT*current);}  );

    return output;
}   //vector<CoordinateT> ApplyAffineTransformation(const CoordinateRangeT& coordinates, const AffineTransformT& mT)

/**
 * @brief Homogeneous projection of coordinates to a space with a possibly different dimensionality
 * @tparam CoordinateRangeT A range of coordinates
 * @tparam MatrixT A matrix
 * @param[in] coordinates Coordinates to be projected
 * @param[in] mP Projection operator
 * @return Projected coordinates
 * @pre \c CoordinateRangeT is a forward range of \c CoordinateT
 * @pre Floating point types are compatible
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT, class MatrixT>
auto CoordinateTransformationsC<ValueT, DIM>::HomogeneousProject(const CoordinateRangeT& coordinates, const MatrixT& mP) -> vector< Matrix<ValueT, RowsM<MatrixT>::value, 1> >
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::HomogeneousProject : CoordinateRangeT must hold elements of type coordinate_type");
    static_assert(is_convertible<ValueT, typename ValueTypeM<MatrixT>::type>::value, "CoordinateTransformations::HomogeneousProject : Floating point types must be compatible" );

    typedef CoordinateVectorT<ValueT, RowsM<MatrixT>::value> ReturnT;

    vector<ReturnT> output;
    if(boost::empty(coordinates))
        return output;

    output.reserve(boost::distance(coordinates));
    boost::for_each(coordinates, [&](const CoordinateT& current){output.emplace_back(mP*current.homogeneous());}  );

    return output;
}   //vector<CoordinateT> HomogeneousProject(const CoordinateRangeT& coordinates, const MatrixT& mP)

/**
 * @brief Normalises a range of homogeneous vectors
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Homogeneous coordinates to be normalised
 * @return Normalised coordinates
 * @pre \c CoordinateRangeT is a forward range of HCoordinateT
 * @pre \c coordinates does not have members which would have a NaN upon normalisation
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
auto CoordinateTransformationsC<ValueT, DIM>::NormaliseHomogeneous(const CoordinateRangeT& coordinates) -> vector<CoordinateT>
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same<typename range_value<CoordinateRangeT>::type, HCoordinateT>::value, "CoordinateTransformationsC::NormaliseHomogeneous : CoordinateRangeT must holds elements of type homogeneous_coordinate_type");

    vector<CoordinateT> output(boost::distance(coordinates));

    size_t c=0;
    for(const auto& current : coordinates)
    {
        output[c]=current.hnormalized();
        assert(!output[c].hasNaN());
        ++c;
    }   //for(const auto& current : coordinates)

    return output;
}   //vector<CoordinateT> NormaliseHomogeneous(const CoordinateRangeT& coordinates)

/**
 * @brief Assigns the coordinates to buckets
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinates to be bucketised
 * @param[in] bucketSize Bucket size for each dimension
 * @param[in] extent Extent of the bounding box that holds the coordinates. Each element indicates the minimum and maximum extent in the corresponding dimension
 * @param[in] flagOverlap If \c true , the buckets are 50% overlapping. This implies that a coordinate may belong to multiple buckets
 * @return A vector of bucket indices. Each element is a set holding the bucket indices for the corresponding coordinate
 * @pre \c CoordinateRangeT is a model of ForwardRangeConcept, holding elements of CoodinateT
 * @pre \c bucketSize has no elements <=0
 * @pre \c extent(:,1)>=extent(:,0)
 * @remarks A form of linear quantisation. If there is no overlap, the use of set<int> introduces an unnecessary overhead
 * @remarks Intended for use with relatively low dimensional vectors. The maximum number of indexable bins is limited by the return type
 *//*
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
vector< set<unsigned int> > CoordinateTransformationsC<ValueT, DIM>::Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent, bool flagOverlap)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::Bucketise : CoordinateRangeT must hold elements of type coordinate_type");
    assert(bucketSize.minCoeff()>0);
    assert( (extent.col(1)>=extent.col(0)).all());

    vector<set<unsigned int> > output;
    if(boost::empty(coordinates))
        return output;

    size_t nCoordinates=boost::distance(coordinates);
    output.resize(nCoordinates);

    //Helper array to convert the quantised coordinates to an index
    //nBuckets=[N1 N2 N3] -> indexHelper=[ N2*N1 N1 1]
    size_t sCoordinate=boost::const_begin(coordinates)->size();
    Array<int, DIM, 1> nBuckets;    //Number of buckets for each dimension. Necessary for dealing with the boundary cells
    for(unsigned int c=0; c<sCoordinate; ++c)
        nBuckets(c) = ceil( (extent(c,1)-extent(c,0))/bucketSize(c) )+1;    //Shadow bin, due to rounding. Imagine a line segment divided into two. Rounding quantises the points to 3 values

    Array<int, DIM, 1> indexHelper; indexHelper(sCoordinate-1)=1;
    int cp1=sCoordinate-1;
    for(int c=sCoordinate-2; c>=0; --c, --cp1)
        indexHelper(c)=indexHelper(cp1)*nBuckets(cp1);

    //Quantisation
    ArrayD buffer; // Temporary buffer
    Array<int, DIM, 1> quantised;   //Coordinates after quantisation
    vector<char> perturbations(sCoordinate); // Perturbations to obtain the nearest neighbours. +-1
    unsigned int nMemberships=pow(2, sCoordinate);  //Max number of memberships for a coordinate. Elements close to the bounding box borders may have less
    size_t c=0;
    for(const auto& current : coordinates)
    {
        //Closest bin
        buffer=(current.array() - extent.col(0)) / bucketSize;
        unsigned int binIndex=0;
        for(size_t c2=0; c2<sCoordinate; ++c2)
        {
            quantised(c2)=round(buffer(c2));
            binIndex+=quantised(c2)*indexHelper(c2);
        }   //for(size_t c2=0; c2<sCoordinate; ++c2)

        output[c].insert(binIndex);

        if(flagOverlap)
        {
            //Add the remaining nearest neighbours

            //Compute the perturbations to the coordinates of the closest bin
            for(size_t c2=0; c2<sCoordinate; ++c2)
            {
                perturbations[c2]= (buffer(c2)-quantised(c2) >0 ) ? 1 : -1;

                //Perturbations moving the point outside of the box are not allowed
                int test=quantised[c2]+perturbations[c2];
                if(test<0 || test>=nBuckets[c2])
                    perturbations[c2]=0;
            }   //for(size_t c2=0; c2<sCoordinate; ++c2)

            //Compute the index for each neighbour
            for(unsigned int c2=1; c2<nMemberships; ++c2)
            {
                unsigned int activePerturbations=c2;
                int update=0;
                for(int c3=sCoordinate-1; c3>=0; --c3, activePerturbations>>=1)
                    update += (activePerturbations & 1) ? indexHelper(c3)*perturbations[c3] : 0;    //Apply mask to see whether the current coordinate is to be perturbed or not

                output[c].insert( binIndex + update);
            }   //for(unsigned int c2=1; c2<nMemberships; ++c2)
        }   //if(flagOverlap)
        ++c;
    }   //for(const auto& current : coordinates)

    return output;
}   //vector< set<unsigned int> > Bucketise(const CoordinateRangeT& coordinates, const ArrayDd& bucketSize, const ArrayD2d& extent, bool flagOverlap)
*/

/**
 * @brief Assigns the coordinates to buckets
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinates to be bucketised
 * @param[in] bucketSize Bucket size for each dimension
 * @param[in] extent Extent of the bounding box that holds the coordinates. Each element indicates the minimum and maximum extent in the corresponding dimension
 * @param[in] flagOverlap If \c true , the buckets are 50% overlapping. This implies that a coordinate may belong to multiple buckets
 * @return A vector of bucket indices. Each element is a vector holding the bucket indices for the corresponding coordinate
 * @pre \c CoordinateRangeT is a model of ForwardRangeConcept, holding elements of CoodinateT
 * @pre \c bucketSize has no elements <=0
 * @pre \c extent(:,1)>=extent(:,0)
 * @remarks A form of linear quantisation. If there is no overlap, the use of set<int> introduces an unnecessary overhead
 * @remarks Intended for use with relatively low dimensional vectors. The maximum number of indexable bins is limited by the return type
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
vector< vector<unsigned int> > CoordinateTransformationsC<ValueT, DIM>::Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent, bool flagOverlap)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::Bucketise : CoordinateRangeT must hold elements of type coordinate_type");
    assert(bucketSize.minCoeff()>0);
    assert( (extent.col(1)>=extent.col(0)).all());

    vector<vector<unsigned int> > output;
    if(boost::empty(coordinates))
        return vector<vector<unsigned int> >();

    //Allocate memory for the output
    size_t nCoordinates=boost::distance(coordinates);
    size_t sCoordinate=boost::const_begin(coordinates)->size();
    unsigned int nMemberships=pow(2, sCoordinate);  //Max number of memberships for a coordinate. Elements close to the bounding box borders may have less
    output.resize(nCoordinates);
    for(auto& current : output)
    	current.reserve(nMemberships);

    //Helper array to convert the quantised coordinates to an index
    //nBuckets=[N1 N2 N3] -> indexHelper=[ N2*N1 N1 1]
    Array<int, DIM, 1> nBuckets;    //Number of buckets for each dimension. Necessary for dealing with the boundary cells
    for(unsigned int c=0; c<sCoordinate; ++c)
        nBuckets(c) = ceil( (extent(c,1)-extent(c,0))/bucketSize(c) )+1;    //Shadow bin, due to rounding. Imagine a line segment divided into two. Rounding quantises the points to 3 values

    Array<int, DIM, 1> indexHelper; indexHelper(sCoordinate-1)=1;
    int cp1=sCoordinate-1;
    for(int c=sCoordinate-2; c>=0; --c, --cp1)
        indexHelper(c)=indexHelper(cp1)*nBuckets(cp1);

    //Quantisation
    ArrayD buffer; // Temporary buffer
    Array<int, DIM, 1> quantised;   //Coordinates after quantisation
    vector<signed char> perturbations(sCoordinate); // Perturbations to obtain the nearest neighbours. +-1
    size_t c=0;
    for(const auto& current : coordinates)
    {
        //Closest bin
        buffer=(current.array() - extent.col(0)) / bucketSize;
        unsigned int binIndex=0;
        for(size_t c2=0; c2<sCoordinate; ++c2)
        {
            quantised(c2)=round(buffer(c2));
            binIndex+=quantised(c2)*indexHelper(c2);
        }   //for(size_t c2=0; c2<sCoordinate; ++c2)

        output[c].push_back(binIndex);

        if(flagOverlap)
        {
            //Add the remaining nearest neighbours

            //Compute the perturbations to the coordinates of the closest bin
			bool flagOut=false;	//true if any of the perturbations end up outside of the box
            for(size_t c2=0; c2<sCoordinate; ++c2)
            {
                perturbations[c2]= (buffer(c2)-quantised(c2) >0 ) ? 1 : -1;

                //Perturbations moving the point outside of the box are not allowed
                int test=quantised[c2]+perturbations[c2];
                if(test<0 || test>=nBuckets[c2])
                {
                    perturbations[c2]=0;
                    flagOut=true;
                }
            }   //for(size_t c2=0; c2<sCoordinate; ++c2)

            //Compute the index for each neighbour
            if(!flagOut)
            {
				for(unsigned int c2=1; c2<nMemberships; ++c2)
				{
					unsigned int activePerturbations=c2;
					int update=0;
					for(int c3=sCoordinate-1; c3>=0; --c3, activePerturbations>>=1)
						update += (activePerturbations & 1) ? indexHelper(c3)*perturbations[c3] : 0;    //Apply mask to see whether the current coordinate is to be perturbed or not

					output[c].push_back( binIndex + update);
				}   //for(unsigned int c2=1; c2<nMemberships; ++c2)
            }
            else	//Special case for if any members of perturbations is 0, where the same update can appear multiple times
            {
            	set<int> updates;
            	for(unsigned int c2=1; c2<nMemberships; ++c2)
				{
					unsigned int activePerturbations=c2;
					int update=0;
					for(int c3=sCoordinate-1; c3>=0; --c3, activePerturbations>>=1)
						update += (activePerturbations & 1) ? indexHelper(c3)*perturbations[c3] : 0;    //Apply mask to see whether the current coordinate is to be perturbed or not

					if(update!=0)
						updates.insert(update);
				}

            	for(const auto currentUpdate : updates)
            		output[c].push_back(binIndex + currentUpdate);
            }

        }   //if(flagOverlap)
        ++c;
    }   //for(const auto& current : coordinates)

    return output;
}   //vector< set<unsigned int> > Bucketise(const CoordinateRangeT& coordinates, const ArrayDd& bucketSize, const ArrayD2d& extent, bool flagOverlap)


/**
 * @brief Assigns the coordinates to buckets- no overlap variant
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinates to be bucketised
 * @param[in] bucketSize Bucket size for each dimension
 * @param[in] extent Extent of the bounding box that holds the coordinates. Each element indicates the minimum and maximum extent in the corresponding dimension
 * @return A vector of bucket indices
 * @pre \c CoordinateRangeT is a model of ForwardRangeConcept, holding elements of CoodinateT
 * @pre \c bucketSize has no elements <=0
 * @pre \c extent(:,1)>=extent(:,0)
 * @remarks A form of linear quantisation.
 * @remarks Intended for use with relatively low dimensional vectors. The maximum number of indexable bins is limited by the return type
 */
template<typename ValueT, unsigned int DIM>
template<class CoordinateRangeT>
vector<unsigned int> CoordinateTransformationsC<ValueT, DIM>::Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent)
{
	 //Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
	static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateTransformations::Bucketise : CoordinateRangeT must hold elements of type coordinate_type");
	assert(bucketSize.minCoeff()>0);
	assert( (extent.col(1)>=extent.col(0)).all());

	vector<unsigned int> output;
	if(boost::empty(coordinates))
		return output;

	size_t nCoordinates=boost::distance(coordinates);
	output.resize(nCoordinates);

	//Helper array to convert the quantised coordinates to an index

	size_t sCoordinate=boost::const_begin(coordinates)->size();
	Array<int, DIM, 1> nBuckets(sCoordinate);    //Number of buckets for each dimension. Necessary for dealing with the boundary cells
	for(unsigned int c=0; c<sCoordinate; ++c)
		nBuckets(c) = ceil( (extent(c,1)-extent(c,0))/bucketSize(c) )+1;    //Shadow bin, due to rounding. Imagine a line segment divided into two. Rounding quantises the points to 3 values

	Array<int, DIM, 1> indexHelper(sCoordinate); indexHelper(sCoordinate-1)=1;
	int cp1=sCoordinate-1;
	for(int c=sCoordinate-2; c>=0; --c, --cp1)
		indexHelper(c)=indexHelper(cp1)*nBuckets(cp1);

	//Quantisation
	ArrayD buffer(sCoordinate); // Temporary buffer
	Array<int, DIM, 1> quantised(sCoordinate);   //Coordinates after quantisation
	size_t c=0;
	for(const auto& current : coordinates)
	{
		//Closest bin
		buffer=(current.array() - extent.col(0)) / bucketSize;
		unsigned int binIndex=0;
		for(size_t c2=0; c2<sCoordinate; ++c2)
		{
			quantised(c2)=round(buffer(c2));
			binIndex+=quantised(c2)*indexHelper(c2);
		}   //for(size_t c2=0; c2<sCoordinate; ++c2)

		output[c]=binIndex;
		++c;
	}	//for(const auto& current : coordinates)

	return output;
}	//vector<unsigned int> Bucketise(const CoordinateRangeT& coordinates, const ArrayD& bucketSize, const ArrayD2& extent)

/**
 * @brief Applies a homogeneous transformation
 * @param[in] coordinate Point to be transformed
 * @param[in] mT Transformation
 * @return Normalised transformed point
 * @warning The transformed point is normalised, unlike ApplyProjectiveTransformation and HomogeneousProject
 */
template<typename ValueT, unsigned int DIM>
auto CoordinateTransformationsC<ValueT, DIM>::TransformCoordinate(const CoordinateT& coordinate, const ProjectiveTransformT& mT) -> CoordinateT
{
	return (mT*coordinate.homogeneous()).eval().hnormalized();
}	//CoordinateT TransformCoordinate(const CoordinateT& coordinate, const ProjectiveTransformT& mT)

/**
 * @brief Applies an affine transformation
 * @param[in] coordinate Point to be transformed
 * @param[in] mT Transformation
 * @return Normalised transformed point
 */
template<typename ValueT, unsigned int DIM>
auto CoordinateTransformationsC<ValueT, DIM>::TransformCoordinate(const CoordinateT& coordinate, const AffineTransformT& mT) -> CoordinateT
{
	return mT*coordinate;
}	//CoordinateT TransformCoordinate(const CoordinateT& coordinate, const AffineTransformT& mT)

}   //GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::set< unsigned int>;
#endif /* COORDINATE_TRANSFORMATION_IPP_6151325 */
