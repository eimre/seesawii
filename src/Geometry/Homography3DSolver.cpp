/**
 * @file Homography3DSolver.cpp Implementation of 5-point projective 3D homography estimation
 * @author Evren Imre
 * @date 24 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Homography3DSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** Homography3DSolverC **********/

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double Homography3DSolverC::Cost(unsigned int nCorrespondences)
{
	return DLTC<DLTProblemT>::Cost(max(nCorrespondences, sGenerator));
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Homography matrix
 * @pre Norm of \c minimalModel <=1
 */
auto Homography3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	//Preconditions
	assert(minimalModel.norm()<=1);

	RealT homogeneousScale=sqrt(1-minimalModel.squaredNorm());

	if(fabs(homogeneousScale)< 3*numeric_limits<RealT>::epsilon() )
		homogeneousScale=0;

	ModelVectorT vModel; vModel.head(BaseT::nDOF)=minimalModel; vModel[BaseT::nParameter-1]=homogeneousScale;

	return MakeModel(vModel);
}	//ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel)

/********** Homography3DMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented 3D homographies
 * @param[in] op1 Minuend Minuend
 * @param[in] op2 Subtrahend Subtrahend
 * @return Difference vector
 * @pre Norm of \c op1 <1
 * @pre Norm of \c op2 <=1
 * @remarks Difference: Projection to the plane tangent to the unit sphere at op2
 */
auto Homography3DMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) const -> result_type
{
	//Preconditions
	assert(op1.norm()<1);
	assert(op2.norm()<=2);

	typedef Homography3DSolverC::real_type RealT;
	typedef Matrix<RealT, 16, 1> ModelVectorT;
	ModelVectorT op1h; op1h.head(15)=op1; op1h[15]=1-op1.norm();
	ModelVectorT op2h; op2h.head(15)=op2; op2h[15]=1-op2.norm();

	RealT zero=3*numeric_limits<RealT>::epsilon();
	if(fabs(op1h[15])<zero) op1h[15]=0;
	if(fabs(op2h[15])<zero) op2h[15]=0;

	return *ProjectHomogeneousToTangentPlane<result_type>(op1h, op2h);	//Preconditions guarantee success
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

