/**
 * @file Homography3DSolver.ipp Implementation of 5-point projective 3D homography estimation
 * @author Evren Imre
 * @date 24 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY3D_SOLVER_IPP_9012368
#define HOMOGRAPHY3D_SOLVER_IPP_9012368

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/tuple/tuple.hpp>
#include <Eigen/Dense>
#include <type_traits>
#include <cstddef>
#include <vector>
#include <iterator>
#include <climits>
#include <cmath>
#include "GeometrySolverBase.h"
#include "DLT.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricError.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace GeometryN
{

//Forward declarations
struct Homography3DMinimalDifferenceC;

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::make_zip_iterator;
using Eigen::Matrix;
using std::is_same;
using std::size_t;
using std::vector;
using std::advance;
using std::numeric_limits;
using std::fabs;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::DLTC;
using SeeSawN::GeometryN::DLTProblemC;
using SeeSawN::MetricsN::SymmetricTransferErrorC;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::AlignSign;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::NumericN::ComputeHouseholderVector;
using SeeSawN::NumericN::ApplyHouseholderTransformationV;
using SeeSawN::NumericN::ProjectHomogeneousToTangentPlane;

//TODO What is the degeneracy condition?
/**
 * @brief 5-point 3D homography estimator
 * @remarks 5-point normalised DLT
 * @remarks Distance measure: 1-Absolute value of the dot-product between the unit vectors representing the homographies. Range=[0,1]
 * @remarks Minimal parameterisation: First 15 elements of the unit-norm homogeneous matrix. Covariance uses a different parameterisation!
 * @warning A valid problem has at least 15 constraints
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class Homography3DSolverC : public GeometrySolverBaseC<Homography3DSolverC, Homography3DT, SymmetricTransferErrorC<Homography3DT>, 3, 3, 5, true, 1, 16, 15>
{
	private:

		typedef GeometrySolverBaseC<Homography3DSolverC, Homography3DT, SymmetricTransferErrorC<Homography3DT>, 3, 3, 5, true, 1, 16, 15> BaseT;	///< Type of the base

		typedef Matrix<double, BaseT::nDOF, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 4x4 homography

		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::Homography3Tag, BaseT::sGenerator, 0, 4> DLTProblemT;	///< DLT problem for 3D homography estimation

		typedef MultivariateGaussianC<Homography3DMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		typedef Matrix<RealT, BaseT::nParameter, 1> ModelVectorT;	///< Type of the model in vector form

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the homography that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef Matrix<RealT, BaseT::nDOF, 1> minimal_model_type;	///< A minimally parameterised model.
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class HomographyRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of homographies
		//@}
};	//class Homography3DSolverC

/**
 * @brief Difference functor for minimally-represented 3D homographies
 * @remarks A model of adaptable binary function concept
 * @remarks Algorithm: Project the first operand to the plane tangent to the unit sphere at the second operand
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct Homography3DMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef Homography3DSolverC::minimal_model_type first_argument_type;
	typedef Homography3DSolverC::minimal_model_type second_argument_type;
	typedef Matrix<Homography3DSolverC::real_type, Homography3DSolverC::DoF(), 1> result_type;	///< Difference

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2) const;	///< Computes the difference between two minimally-represented 3D homographies
	//@}
};	//struct Homography3DMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre \c model has non-zero norm
 * @return Minimal form
 */
template<class DummyRangeT>
auto Homography3DSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	ModelVectorT v=MakeVector(model);
	v.normalize();

	return v.head(BaseT::DoF());
}	//auto Homography3DSolverC::MakeMinimal(const ModelT& model)

/**
 * @brief Computes the sample statistics for a set of homographies
 * @tparam HomographyRangeT A range of 3D homographies
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] homographies Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics. If the mean has 0 norm, invalid Gaussian
 * @pre \c HomographyRangeT is a forward range of elements of type \c Homography2DT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c homographies should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @remarks Algorithm
 * 	- Normalise the homographies to unit norm
 * 	- Compute the mean of the resulting vectors
 * 	- Compute the plane tangent to the unit sphere at the mean
 * 	- Project the samples to the tangent plane, and compute the covariance
 * The resulting representation is minimal. However it is different from the minimal representation provided by the class
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c homographies has 15 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class HomographyRangeT, class WeightRangeT, class DummyRangeT>
auto Homography3DSolverC::ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<HomographyRangeT>));
	static_assert(is_same<typename range_value<HomographyRangeT>::type, Homography3DT>::value, "Homography3DSolverC::ComputeSampleStatistics: HomographyRangeT must have elements of type Homography3DT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "Homography3DSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(homographies)==boost::distance(wMean));
	assert(boost::distance(homographies)==boost::distance(wCovariance));

	//Normalise the samples, and align the signs
	size_t nSample=boost::distance(homographies);
	vector<ModelVectorT> normalised; normalised.reserve(nSample);
	for(const auto& current : homographies)
	{
		normalised.push_back( MakeVector(current) );
		normalised.rbegin()->normalize();
	}	//for(const auto& current : homographies)

	vector<ModelVectorT> temp=AlignSign<RealT, BaseT::nDOF>(normalised);
	temp.swap(normalised);

	//Mean
	ModelVectorT mean=ModelVectorT::Zero();
	auto itWM=boost::const_begin(wMean);
	for(size_t c=0; c<nSample; ++c, advance(itWM,1))
		mean+= *itWM * (normalised[c]);

	//If mean cannot be mapped to the unit sphere, invalid Gaussian
	if(mean.norm()==0)
		return GaussianT();

	mean.normalize();
	ModelVectorT vHouseholder=ComputeHouseholderVector(mean, BaseT::nParameter-1);

	//Covariance
	typename GaussianT::covariance_type mCovariance; mCovariance.setZero();
	auto itC=boost::const_begin(wCovariance);
	for(size_t c=0; c<nSample; ++c, advance(itC,1))
	{
		Matrix<RealT, BaseT::nDOF, 1> projected=ApplyHouseholderTransformationV(vHouseholder, normalised[c] ).hnormalized();
		mCovariance+= *itC * (projected * projected.transpose());	//Mean is at the origin, so no subtraction
	}	//for(; itCov!=itCovE; advance(itCov,1))

	return GaussianT( MakeMinimal(Reshape<Homography3DT>(mean, 4, 4)), mCovariance);
}	//GaussianT ComputeSampleStatistics(const HomographyRangeT& homographies)

/**
 * @brief Computes the homography that best explains the correspondence set
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding the homography. An empty set if DLT fails, or \c correspondences has too few elements
 * @post The solution is unit norm
 */
template<class CorrespondenceRangeT>
auto Homography3DSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	return DLTC<DLTProblemT>::Run(correspondences);
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT

}	//GeometryN
}	//SeeSawN

#endif /* HOMOGRAPHY3D_SOLVER_IPP_9012368 */
