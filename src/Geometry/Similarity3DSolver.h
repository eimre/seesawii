/**
 * @file Similarity3DSolver.h Public interface for the solver for a 3D similarity transformation
 * @author Evren Imre
 * @date 26 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SIMILARITY3D_SOLVER_H_0732345
#define SIMILARITY3D_SOLVER_H_0732345

#include "Similarity3DSolver.ipp"

namespace SeeSawN
{
namespace GeometryN
{
class Similarity3DSolverC;	//< 3D similarity transformation estimator
struct Similarity3DMinimalDifferenceC;	//< Difference functor for minimally-represented 3D similarity transformations
}	//GeometryN
}	//SeeSawN

#endif /* SIMILARITY3D_SOLVER_H_0732345 */
