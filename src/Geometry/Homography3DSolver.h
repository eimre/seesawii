/**
 * @file Homography3DSolver.h Public interface for 5-point projective 3D homography estimation
 * @author Evren Imre
 * @date 24 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY3D_SOLVER_H_6013292
#define HOMOGRAPHY3D_SOLVER_H_6013292


#include "Homography3DSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{
class Homography3DSolverC;	//< 5-point 3D homography estimator
struct Homography3DMinimalDifferenceC;	//< Difference functor for minimally-represented 3D homographies
}	//GeometryN
}	//SeeSawN

#endif /* HOMOGRAPHY3D_SOLVER_H_6013292 */
