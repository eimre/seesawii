/**
 * @file LensDistortion.ipp Details of various lens distortion models
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LENS_DISTORTION_IPP_9743256
#define LENS_DISTORTION_IPP_9743256

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/optional.hpp>
#include <array>
#include <complex>
#include <cstddef>
#include <limits>
#include <vector>
#include <type_traits>
#include <algorithm>
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Numeric/Polynomial.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::math::pow;
using boost::optional;
using boost::ForwardRangeConcept;
using boost::range_value;
using boost::transform;
using boost::copy;
using std::array;
using std::vector;
using std::complex;
using std::size_t;
using std::numeric_limits;
using std::is_same;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::NumericN::QuadraticPolynomialRoots;
using SeeSawN::NumericN::CubicPolynomialRoots;

/** @ingroup Geometry */ //@{
enum class LensDistortionCodeT{FP1, OP1, DIV, NOD};	///< Lens distortion type codes: Full polynomial, odd polynomial, division, no distortion
//@}

/**
 * @brief No lens distortion
 * @ingroup Geometry
 * @nosubgrouping
 */
class LensDistortionNoDC
{
    private:

        typedef typename ValueTypeM<Coordinate2DT>::type RealT;  ///< A floating point type

    public:

        static constexpr LensDistortionCodeT modelCode=LensDistortionCodeT::NOD;	///< Distortion model

        /** @name Constructors */ //@{
        LensDistortionNoDC();    ///< Default constructor
        LensDistortionNoDC(const Coordinate2DT& ccentre, RealT kkappa);  ///< Constructor
        //@}

        /** @name Operations */ //@{
        optional<Coordinate2DT> Apply(const Coordinate2DT& undistorted) const;    ///< Applies lens distortion to a point
        optional<Coordinate2DT> Correct(const Coordinate2DT& distorted) const;  ///< Corrects a distorted point
        //@}
};  //class LensDistortionFP1C

/**
 * @brief First-order full-polynomial lens distortion model
 * @remarks The distortion model is \f$ \mathbf{x_d} = \mathbf{c} + (1+ \kappa_1 r) \mathbf{(x_u - c )}  \f$, where \b c is the centre of distortion, \f$ \mathbf {x_u} \f$ is the undistorted image point, and \f$ r \f$ is the Euclidean distance between \b c and \f$ \mathbf {x_u} \f$
 * @remarks "Review of Geometric Distortion Compensation for Fish-Eye Cameras," C. Hughes, M. Glavin, E. Jones, P. Denny, 16th IET Irish Signals and Systems Conference, June 2008, "Full polynomial model"
 * @remarks UniS camera type 1
 * @remarks Invertible operation: \c Apply fails if \c Correct cannot be performed successfully
 * @ingroup Geometry
 * @nosubgrouping
 */
class LensDistortionFP1C
{
    private:

        typedef typename ValueTypeM<Coordinate2DT>::type RealT;  ///< A floating point type

        /** @name Configuration */ //@{
        Coordinate2DT centre;   ///< Distortion centre
        RealT kappa;    ///< Distortion coefficient, 1/pixel
        bool flagValid; ///< \c true if the object is initialised
        //@}

        /**@name Implementation details */ //@{
        bool Verify(RealT radius) const;	///< Verifies whether both the distortion and the undistortion operations can be performed successfully
        //@}

    public:

        static constexpr LensDistortionCodeT modelCode=LensDistortionCodeT::FP1;	///< Distortion model

        /** @name Constructors */ //@{
        LensDistortionFP1C();    ///< Default constructor
        LensDistortionFP1C(const Coordinate2DT& ccentre, RealT kkappa);  ///< Constructor
        //@}

		/** @name Operations */ //@{
        optional<Coordinate2DT> Apply(const Coordinate2DT& undistorted) const;    ///< Applies lens distortion to a point
        optional<Coordinate2DT> Correct(const Coordinate2DT& distorted) const;  ///< Corrects a distorted point
        //@}
};  //class LensDistortionFP1C

/**
 * @brief First-order odd polynomial lens distortion model
 * @remarks The distortion model is \f$ \mathbf{x_d} = \mathbf{c} + (1+ \kappa_1 r^2) \mathbf{(x_u - c )}  \f$, where \b c is the center of distortion, \f$ \mathbf {x_u} \f$ is the undistorted image point, and \f$ r \f$ is the Euclidean distance between \b c and \f$ \mathbf {x_u} \f$
 * @remarks "Review of Geometric Distortion Compensation for Fish-Eye Cameras," C. Hughes, M. Glavin, E. Jones, P. Denny, 16th IET Irish Signals and Systems Conference, June 2008, "Full polynomial model"
 * @remarks UniS camera type 2
 * @ingroup Geometry
 * @nosubgrouping
 */
class LensDistortionOP1C
{
    private:

        typedef typename ValueTypeM<Coordinate2DT>::type RealT;  ///< A floating point type

        /** @name Configuration */ //@{
        Coordinate2DT centre;   ///< Distortion centre
        RealT kappa;    ///< Distortion coefficient, 1/pixel^2
        bool flagValid; ///< \c true if the object is initialised
        //@}

    public:

        static constexpr LensDistortionCodeT modelCode=LensDistortionCodeT::OP1;	///< Distortion model

        /** @name Constructors */ //@{
        LensDistortionOP1C();    ///< Default constructor
        LensDistortionOP1C(const Coordinate2DT& ccentre, RealT kkappa);  ///< Constructor
        //@}

		/** @name Operations */ //@{
        optional<Coordinate2DT> Apply(const Coordinate2DT& undistorted) const;    ///< Applies lens distortion to a point
        optional<Coordinate2DT> Correct(const Coordinate2DT& distorted) const;  ///< Corrects a distorted point
        //@}
};  //class LensDistortion)P1

/**
 * @brief Division lens distortion model
 * @remarks The distortion model is \f$ \mathbf{r_u} = \frac{r_d}{1+ \kappa r_d^2}  \f$, where \f$ r \f$ is the Euclidean distance between a point and the distortion center, and \f$ \kappa \f$ is the radial disotrion parameter
 * @remarks "Review of Geometric Distortion Compensation for Fish-Eye Cameras," C. Hughes, M. Glavin, E. Jones, P. Denny, 16th IET Irish Signals and Systems Conference, June 2008, "Full polynomial model"
 * @remarks Invertible operation: \c Correct fails if \c Apply cannot be performed successfully
 * @ingroup Geometry
 * @nosubgrouping
 */
class LensDistortionDivC
{
    private:

        typedef typename ValueTypeM<Coordinate2DT>::type RealT;  ///< A floating point type

        /** @name Configuration */ //@{
        Coordinate2DT centre;   ///< Distortion centre
        RealT kappa;    ///< Distortion coefficient, 1/pixel
        bool flagValid; ///< \c true if the object is initialised
        //@}

        /**@name Implementation details */ //@{
        bool Verify(RealT ru, RealT rd) const;	///< Verifies whether both the distortion and the undistortion operations can be performed successfully
        //@}

    public:

        static constexpr LensDistortionCodeT modelCode=LensDistortionCodeT::DIV;	///< Distortion model

        /** @name Constructors */ //@{
        LensDistortionDivC();    ///< Default constructor
        LensDistortionDivC(const Coordinate2DT& ccentre, RealT kkappa);  ///< Constructor
        //@}

		/** @name Operations */ //@{
        optional<Coordinate2DT> Apply(const Coordinate2DT& undistorted) const;    ///< Applies lens distortion to a point
        optional<Coordinate2DT> Correct(const Coordinate2DT& distorted) const;  ///< Corrects a distorted point
        //@}
};  //class LensDistortionDivC

/********** IMPLEMENTATION STARTS HERE **********/

/********** FREE FUNCTIONS **********/

/**
 * @brief Applies a specified lens distortion to a set of points
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] points Points to be distorted
 * @param[in] distortionType Distortion type code
 * @param[in] centre Distortion centre
 * @param[in] kappa Distortion coefficient
 * @return A vector of distorted points. Invalid element when the distortion fails
 * @pre \c CoordinateRangeT is a forward range of elements of type \c Coordinate2DT
 * @ingroup Geometry
 */
template<class CoordinateRangeT>
vector<optional<Coordinate2DT> > ApplyDistortion(const CoordinateRangeT& points, LensDistortionCodeT distortionType, const Coordinate2DT& centre, double kappa)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
	static_assert(is_same< typename range_value<CoordinateRangeT>::type, Coordinate2DT>::value, "ApplyDistortion : CoordinateRangeT must have elements of type Coordinate2DT");

	size_t nPoints=boost::distance(points);
	vector<optional<Coordinate2DT > > output(nPoints);

	if(distortionType==LensDistortionCodeT::NOD)
		copy(points, output.begin());

	if(distortionType==LensDistortionCodeT::FP1)
	{
		LensDistortionFP1C distorter(centre, kappa);
		auto action=[&](const Coordinate2DT& src){ return distorter.Apply(src);};
		transform(points, output.begin(), action);
	}	//if(distortionType==LensDistortionCodeT::FP1)

	if(distortionType==LensDistortionCodeT::OP1)
	{
		LensDistortionOP1C distorter(centre, kappa);
		auto action=[&](const Coordinate2DT& src){ return distorter.Apply(src);};
		transform(points, output.begin(), action);
	}	//if(distortionType==LensDistortionCodeT::FP1)

	if(distortionType==LensDistortionCodeT::DIV)
	{
		LensDistortionDivC distorter(centre, kappa);
		auto action=[&](const Coordinate2DT& src){ return distorter.Apply(src);};
		transform(points, output.begin(), action);
	}	//if(distortionType==LensDistortionCodeT::FP1)

	return output;
}	//vector<optional<Coordinate2DT> > ApplyDistortion(const CoordinateRangeT& points, LensDistortionCodeT& distortionType)

/**
 * @brief Applies a specified lens distortion correction to a set of points
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] points Points to be undistorted
 * @param[in] distortionType Distortion type code
 * @param[in] centre Distortion centre
 * @param[in] kappa Distortion coefficient
 * @return A vector of undistorted points. Invalid element when the distortion correction fails
 * @pre \c CoordinateRangeT is a forward range of elements of type \c Coordinate2DT
 * @ingroup Geometry
 */
template<class CoordinateRangeT>
vector<optional<Coordinate2DT> > ApplyDistortionCorrection(const CoordinateRangeT& points, LensDistortionCodeT distortionType, const Coordinate2DT& centre, double kappa)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
	static_assert(is_same< typename range_value<CoordinateRangeT>::type, Coordinate2DT>::value, "ApplyDistortionCorrection : CoordinateRangeT must have elements of type Coordinate2DT");

	size_t nPoints=boost::distance(points);
	vector<optional<Coordinate2DT > > output(nPoints);

	if(distortionType==LensDistortionCodeT::NOD)
		copy(points, output.begin());

	if(distortionType==LensDistortionCodeT::FP1)
	{
		LensDistortionFP1C undistorter(centre, kappa);
		auto action=[&](const Coordinate2DT& src){ return undistorter.Correct(src);};
		transform(points, output.begin(), action);
	}	//if(distortionType==LensDistortionCodeT::FP1)

	if(distortionType==LensDistortionCodeT::OP1)
	{
		LensDistortionOP1C undistorter(centre, kappa);
		auto action=[&](const Coordinate2DT& src){ return undistorter.Correct(src);};
		transform(points, output.begin(), action);
	}	//if(distortionType==LensDistortionCodeT::FP1)

	if(distortionType==LensDistortionCodeT::DIV)
	{
		LensDistortionDivC undistorter(centre, kappa);
		auto action=[&](const Coordinate2DT& src){ return undistorter.Correct(src);};
		transform(points, output.begin(), action);
	}	//if(distortionType==LensDistortionCodeT::FP1)

	return output;
}	//vector<optional<Coordinate2DT> > ApplyDistortion(const CoordinateRangeT& points, LensDistortionCodeT& distortionType)

}   //GeometryN
}	//SeeSawN

#endif /* LENS_DISTORTION_IPP_9743256 */
