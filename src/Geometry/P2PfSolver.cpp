/**
 * @file P2PfSolver.cpp Implementation of the 2-point orientation and focal length solver
 * @author Evren Imre
 * @date 9 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P2PfSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** P2PfSolverC **********/

/**
 * @brief Computational cost of the solver
 * @param[in] dummy Dummy variable
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double P2PfSolverC::Cost(unsigned int dummy)
{
	return P2PSolverC::Cost(sGenerator);	//P2PSolver only. The cost of the computation of f is negligible
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return A 3x4 camera matrix
 */
auto P2PfSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity();
	mK(0,0)=get<iFocalLength>(minimalModel); mK(1,1)=mK(0,0);

	ModelT output=ModelT::Zero();
	output.block(0,0,3,3)=mK*RotationVectorToQuaternion(get<iOrientation>(minimalModel)).matrix();
	return output;
}	//auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return \f$ 1- \frac{|<\mathbf{M_1, M_2}>|}{\sqrt{<\mathbf{M_1,M_1}><\mathbf{M_2,M_2}>}} \f$. [0,1]
 */
auto P2PfSolverC::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	return 1-fabs( (model1.cwiseProduct(model2)).sum() )/(model1.norm() * model2.norm());
}	//distance_type ComputeDistance(const ModelT& model1, const ModelT& model2)

/**
 * @brief Converts a model to a vector
 * @param[in] model Model
 * @return First 3x3 submatrix of \c model in vector form
 */
auto P2PfSolverC::MakeVector(const ModelT& model) -> VectorT
{
	return Reshape<VectorT>(model.block(0,0,3,3),9,1);
}	//VectorT MakeVector(const ModelT& model)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel Vector
 * @param[in] dummy Dummy parameter to meet the concept requirements
 * @return A 3x4 camera matrix
 */
auto P2PfSolverC::MakeModel(const VectorT& vModel, bool dummy) -> ModelT
{
	ModelT output=ModelT::Zero();
	output.block(0,0,3,3)=Reshape<RotationMatrix3DT>(vModel,3,3);
	return output;
}	//ModelT MakeModel(const VectorT& vModel)

/********** P2PfMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented similarity transformations
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector. [scale; Rotation vector(3) ]
 * @remarks Difference: scale difference (1); orientation (3)
 */
auto P2PfMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	result_type output;
	output[0]=get<P2PfSolverC::iFocalLength>(op1)-get<P2PfSolverC::iFocalLength>(op2);

	Rotation3DMinimalDifferenceC rotationDif;
	output.segment(1,3)=rotationDif(get<P2PfSolverC::iOrientation>(op1), get<P2PfSolverC::iOrientation>(op2));

	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type


}	//GeometryN
}	//SeeSawN

