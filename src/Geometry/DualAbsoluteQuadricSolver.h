/**
 * @file DualAbsoluteQuadricSolver.h Public interface for dual absolute quadric solver
 * @author Evren Imre
 * @date 4 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DUAL_ABSOLUTE_QUADRIC_SOLVER_H_6458012
#define DUAL_ABSOLUTE_QUADRIC_SOLVER_H_6458012

#include "DualAbsoluteQuadricSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{

class DualAbsoluteQuadricSolverC;	///< Estimates the dual absolute quadric from a set of cameras

/********** EXTERN TEMPLATES **********/
extern template list<Homography3DT> DualAbsoluteQuadricSolverC::operator()(const vector<CameraMatrixT>&) const;
}	//GeometryN
}	//SeeSawN

#endif /* DUAL_ABSOLUTE_QUADRIC_SOLVER_H_6458012 */
