/**
 * @file Homography2DSolver.ipp Implementation of Homgraphy2DSolverC
 * @author Evren Imre
 * @date 10 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY_2D_SOLVER_IPP_1290922
#define HOMOGRAPHY_2D_SOLVER_IPP_1290922

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/concepts.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <list>
#include <cmath>
#include <climits>
#include <vector>
#include <iterator>
#include <type_traits>
#include "../Elements/GeometricEntity.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Metrics/GeometricError.h"
#include "Rotation.h"
#include "GeometrySolverBase.h"
#include "DLT.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::math::pow;
using Eigen::Matrix3d;
using Eigen::Matrix;
using Eigen::VectorXd;
using Eigen::JacobiSVD;
using std::list;
using std::numeric_limits;
using std::make_tuple;
using std::tuple;
using std::get;
using std::tie;
using std::vector;
using std::advance;
using std::is_same;
using std::is_floating_point;
using std::sqrt;
using std::max;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::DLTC;
using SeeSawN::GeometryN::DLTProblemC;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::MetricsN::SymmetricTransferErrorC;

//Forward declarations
struct Homography2DMinimalDifferenceC;

/**
 * @brief 4-point 2D homography estimator
 * @remarks 4-point normalised DLT
 * @remarks Multiple View Geometry, R. Hartley, A. Zissermann, 2nd Ed, 2003, pp.91, Al 4.1
 * @remarks A generator is degenerate unless it has 3 non-collinear points. Untested, as the test is expensive.
 * @remarks Distance measure: 1-Absolute value of the dot-product between the unit vectors representing the homographies. Range=[0,1]
 * @remarks Minimal parameterisation:
 *	- \f$ \mathbf{H} = \mathbf{U S V^T} \f$. When \f$ \mathbf{H} \f$ is unit-norm, the sum of singular values is 1. Therefore, \f$ \mathbf{H} \f$ can be represented by two rotations (\f$ \mathbf{U, V}\f$ and two scalars (second and third singular values)
 * 	- Invariant to scale, but not to a sign change!
 * @warning A valid problem has at least 8 constraints
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class Homography2DSolverC : public GeometrySolverBaseC<Homography2DSolverC, Homography2DT, SymmetricTransferErrorC<Homography2DT>, 2, 2, 4, true, 1, 9, 8>
{
	private:

		typedef GeometrySolverBaseC<Homography2DSolverC, Homography2DT, SymmetricTransferErrorC<Homography2DT>, 2, 2, 4, true, 1, 9, 8> BaseT;	///< Type of the base

		typedef Matrix<double, BaseT::nDOF, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 3x3 homography

		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::Homography2Tag, BaseT::sGenerator, 0, 3> DLTProblemT;	///< DLT problem for 2D homography estimation

		typedef MultivariateGaussianC<Homography2DMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the homography that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<QuaternionT, QuaternionT, RealT, RealT> minimal_model_type;	///< A minimally parameterised model. [Rotation on the left; Rotation on the right; Second singular value; Third singular value]
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class HomographyRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of homographies
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iRotationL=0;	///< Index of the left rotation
		static constexpr unsigned int iRotationR=1;	///< Index of the right rotation
		static constexpr unsigned int iScalar2=2;	///< Index of the second singular value
		static constexpr unsigned int iScalar3=3;	///< Index of the third singular value
		//@}
};	//class Homography2DSolverC

/**
 * @brief Difference functor for minimally-represented 2D homographies
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct Homography2DMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef Homography2DSolverC::minimal_model_type first_argument_type;
	typedef Homography2DSolverC::minimal_model_type second_argument_type;
	typedef Matrix<Homography2DSolverC::real_type, Homography2DSolverC::DoF(), 1> result_type;	///< Difference in vector form: [Left rotation vector, right rotation vector, 2nd singular value, 3rd singular value]

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between two minimally-represented 2D homographies
	//@}
};	//struct Homography2DMinimalDifferenceC


/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre \c model has non-zero norm
 * @return Minimal form
 */
template<class DummyRangeT>
auto Homography2DSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	//Preconditions
	assert(model.norm()!=0);

	JacobiSVD<ModelT, Eigen::ColPivHouseholderQRPreconditioner> svd(model, Eigen::ComputeFullU | Eigen::ComputeFullV);

	//A rotation matrix has positive determinant
	int signL=1;
	if(svd.matrixU().determinant() < 0)
		signL=-1;

	int signR=1;
	if(svd.matrixV().determinant() < 0)
		signR=-1;

	auto sv=svd.singularValues().normalized();

	return make_tuple( QuaternionT(signL*svd.matrixU()), QuaternionT(signR*svd.matrixV()), signR*signL*sv[1], signR*signL*sv[2]);
}	//minimal_model_type MakeMinimal(const ModelT& model)

/**
 * @brief Computes the sample statistics for a set of homographies
 * @tparam HomographyRangeT A range of 2D homographies
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] homographies Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c HomographyRangeT is a forward range of elements of type \c Homography2DT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c homographies should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c homographies has 8 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class HomographyRangeT, class WeightRangeT, class DummyRangeT>
auto Homography2DSolverC::ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<HomographyRangeT>));
	static_assert(is_same<typename range_value<HomographyRangeT>::type, Homography2DT>::value, "Homography2DSolverC::ComputeSampleStatistics: HomographyRangeT must have elements of type Homography2DT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "Homography2DSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(homographies)==boost::distance(wMean));
	assert(boost::distance(homographies)==boost::distance(wCovariance));

	//Decompose the representation
	unsigned int nSample=boost::distance(homographies);
	vector<QuaternionT> qList1(nSample);
	vector<QuaternionT> qList2(nSample);
	vector<RealT> s2List(nSample);
	vector<RealT> s3List(nSample);
	size_t index=0;

	for(const auto& current : homographies)
	{

		tie(qList1[index], qList2[index], s2List[index], s3List[index])=MakeMinimal(current);

		//Resolve sign ambiguity: Rotations have positive determinant, and the scalars are positive
		s2List[index]=fabs(s2List[index]);
		s3List[index]=fabs(s3List[index]);
		++index;
	}	//for(const auto& current : homographies)

	//Mean

	minimal_model_type meanModel;

	//Scalars
	get<iScalar2>(meanModel)=boost::inner_product(s2List, wMean, (RealT)0);
	get<iScalar3>(meanModel)=boost::inner_product(s3List, wMean, (RealT)0);

	//Left rotation
	Matrix<RealT, 3, 3> mCovQ1;	//Dummy
	tie(get<iRotationL>(meanModel), mCovQ1)=ComputeQuaternionSampleStatistics(qList1, wMean, wCovariance);

	//Right rotation
	Matrix<RealT, 3, 3> mCovQ2;	//Dummy
	tie(get<iRotationR>(meanModel), mCovQ2)=ComputeQuaternionSampleStatistics(qList2, wMean, wCovariance);

	//Covariance
	Homography2DMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	auto itWC=boost::const_begin(wCovariance);
	for(size_t c=0; c<nSample; ++c)
	{
		typename Homography2DMinimalDifferenceC::result_type diff=subtractor(minimal_model_type(qList1[c], qList2[c], s2List[c], s3List[c]), meanModel);
		mCov+= *itWC * (diff * diff.transpose());

		advance(itWC,1);
	}	//for(size_t c=0; c<nSample; ++c)

	return GaussianT(meanModel, mCov);
}	//GaussianT ComputeSampleStatistics(const HomographyRangeT& homographies)

/**
 * @brief Computes the homography that best explains the correspondence set
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding the homography. An empty set if DLT fails, or \c correspondences has too few elements
 * @post The solution is unit norm
 */
template<class CorrespondenceRangeT>
auto Homography2DSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	return DLTC<DLTProblemT>::Run(correspondences);
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT

}	//GeometryN
}	//SeeSawN

#endif /* HOMOGRAPHY_2D_SOLVER_IPP_1290922 */
