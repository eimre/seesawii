/**
 * @file OneSidedFundamentalSolver.h Public interface for the 6-point one-sided fundamental matrix solver
 * @author Evren Imre
 * @date 13 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ONE_SIDED_FUNDAMENTAL_SOLVER_H_6983129
#define ONE_SIDED_FUNDAMENTAL_SOLVER_H_6983129

#include "OneSidedFundamentalSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{
class OneSidedFundamentalSolverC;	///< One-sided fundamental matrix estimation
struct OneSidedFundamentalMinimalDifferenceC;	///< Difference functor for minimally-represented one-sided fundamental matrices
}	//GeometryN
}	//SeeSawN

/********** EXTERNAL TEMPLATES **********/
extern template class std::complex<double>;
extern template class std::array<std::complex<double>,2>;
extern template class std::array<double,3>;
extern template class std::vector<double>;

#endif /* ONE_SIDED_FUNDAMENTAL_SOLVER_H_6983129 */
