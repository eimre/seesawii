/**
 * @file P4PSolver.h Public interface for the 4-point calibration solver
 * @author Evren Imre
 * @date 12 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P4P_SOLVER_H_8012692
#define P4P_SOLVER_H_8012692

#include "P4PSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{

class P4PSolverC;	///< 4-point pose and focal length solver
struct P4PMinimalDifferenceC;	///< Difference functor for 2 minimally represented solutions


}	//GeometryN
}	//SeeSawN

#endif /* P4PSOLVER_H_ */
