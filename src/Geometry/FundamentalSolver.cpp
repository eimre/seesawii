/**
 * @file FundamentalSolver.cpp Implementation and instantiations for 7- and 8-point fundamental matrix solvers
 * @author Evren Imre
 * @date 3 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "FundamentalSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** Fundamental7SolverC **********/

/**
 * @brief Given a basis, computes the corresponding fundamental matrices
 * @param[in] basis Fundamental matrices comprising the basis
 * @return Estimated fundamental matrices
 * @remarks Polynomial equation is from the corresponding function of OpenCV
 * @remarks Due to numerical errors, still it is necessary to use a rank-2 approximation
 */
auto Fundamental7SolverC::ModelFromBasis(const list<ModelT>& basis) -> list<ModelT>
{
	//F= lambda*(F1-F2) + F2
	//Use the determinant constraint to compute lambda-> A 3rd degree polynomial equation

	ModelT mF1=*basis.cbegin();
	ModelT mF2=*basis.crbegin();
	mF1-=mF2;

	array<double, 4> coefficients;

	double t0, t1, t2;   //Minors for the first row of mF1 and mF2;

	//Compute the minors for mF2
	t0 = mF2(1,1)*mF2(2,2) - mF2(1,2)*mF2(2,1);
	t1 = mF2(1,0)*mF2(2,2) - mF2(1,2)*mF2(2,0);
	t2 = mF2(1,0)*mF2(2,1) - mF2(1,1)*mF2(2,0);

	coefficients[3] = mF2(0,0)*t0 - mF2(0,1)*t1 + mF2(0,2)*t2;

	coefficients[2] = 	mF1(0,0)*t0 - mF1(0,1)*t1 + mF1(0,2)*t2 -
						mF1(1,0)*(mF2(0,1)*mF2(2,2) - mF2(0,2)*mF2(2,1)) +
						mF1(1,1)*(mF2(0,0)*mF2(2,2) - mF2(0,2)*mF2(2,0)) -
						mF1(1,2)*(mF2(0,0)*mF2(2,1) - mF2(0,1)*mF2(2,0)) +
						mF1(2,0)*(mF2(0,1)*mF2(1,2) - mF2(0,2)*mF2(1,1)) -
						mF1(2,1)*(mF2(0,0)*mF2(1,2) - mF2(0,2)*mF2(1,0)) +
						mF1(2,2)*(mF2(0,0)*mF2(1,1) - mF2(0,1)*mF2(1,0));

    //Compute the minors for mF1
    t0 = mF1(1,1)*mF1(2,2) - mF1(1,2)*mF1(2,1);
    t1 = mF1(1,0)*mF1(2,2) - mF1(1,2)*mF1(2,0);
    t2 = mF1(1,0)*mF1(2,1) - mF1(1,1)*mF1(2,0);

    coefficients[1] = 	mF2(0,0)*t0 - mF2(0,1)*t1 + mF2(0,2)*t2 -
						mF2(1,0)*(mF1(0,1)*mF1(2,2) - mF1(0,2)*mF1(2,1)) +
						mF2(1,1)*(mF1(0,0)*mF1(2,2) - mF1(0,2)*mF1(2,0)) -
						mF2(1,2)*(mF1(0,0)*mF1(2,1) - mF1(0,1)*mF1(2,0)) +
						mF2(2,0)*(mF1(0,1)*mF1(1,2) - mF1(0,2)*mF1(1,1)) -
						mF2(2,1)*(mF1(0,0)*mF1(1,2) - mF1(0,2)*mF1(1,0)) +
						mF2(2,2)*(mF1(0,0)*mF1(1,1) - mF1(0,1)*mF1(1,0));

    coefficients[0] = mF1(0,0)*t0 - mF1(0,1)*t1 + mF1(0,2)*t2;

    //If mF1 or mF2 represent a pure translation, many entries are 0, leading to a polynomial less than 3rd degree. Multiply by lambda to raise it to 3rd degree
    unsigned int nAttempt=0;
    while(coefficients[0]==0 && nAttempt<3)
    {
    	rotate(coefficients.begin(), coefficients.begin()+1, coefficients.end());	//Rotate left
    	++nAttempt;
    }	//while(coefficients[0]==0 && nAttempt<3)

    //Solve the equation
    array<complex<double>, 3> roots={ {complex<double>(0,0), complex<double>(0,1), complex<double>(0,1)} };	// Default: lambda=0

    if(coefficients[0]!=0)
    	roots=CubicPolynomialRoots(coefficients);

    //Identify the real roots
    set<RealT> lambda;
    RealT realZero= 3*numeric_limits<RealT>::epsilon();
    for(const auto current : roots)
    {
    	//Only real roots
    	if(fabs(current.imag()) > realZero)
    		continue;
    	else
    		lambda.insert(current.real());
    }	//for(const auto current : roots)

    //Output
    list<ModelT> output;
    for(const auto current : lambda)
    {
    	ModelT mF= current*mF1 + mF2;	//Guaranteed to be rank-2: det(lambda(F1-F2)+F2)=0

    	//Normalise and save
    	mF.normalize();
    	output.push_back(mF);
    }	//for(const auto current : roots)

    return output;
}	//list<ModelT> ModelFromBasis(const list<ModelT>& basis)

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Dummy
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 * @remarks Cost of the cubic solver is ignored
 */
double Fundamental7SolverC::Cost(unsigned int nCorrespondences)
{
	bool flagDouble=is_same<RealT, double>::value;

	double costDLT=DLTC<DLTProblemT>::Cost(sGenerator);
	double costLambda=ComplexityEstimatorC::MatrixAdd(3, 3, flagDouble) + (8.0/3)*ComplexityEstimatorC::Determinant(3, flagDouble);
	double costSolution=ComplexityEstimatorC::MatrixAdd(3, 3, flagDouble) + 2*ComplexityEstimatorC::MatrixScale(3, 3, flagDouble) +  ComplexityEstimatorC::FrobeniusNorm(3, 3, flagDouble);
	return costDLT + costLambda + 3*costSolution;
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Fundamental matrix
 * @pre The absolute value of the scalar component <1
 */
auto Fundamental7SolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	assert(fabs(get<iScalar>(minimalModel))<1);
	Matrix<RealT, 3, 1> vS; vS<< 0, get<iScalar>(minimalModel), 0;
	vS[0]=sqrt(1-pow<2>(vS[1]));

	if(vS[1]<0)
		vS[0]*=-1;	//Sign correction

	return (get<iRotationL>(minimalModel).toRotationMatrix() * (vS.asDiagonal() * get<iRotationR>(minimalModel).toRotationMatrix().transpose())).eval();
}	//ModelT MakeModel(const minimal_model_type& minimalModel)

/********** Fundamental8SolverC **********/
/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double Fundamental8SolverC::Cost(unsigned int nCorrespondences)
{
	return DLTC<DLTProblemT>::Cost(max(nCorrespondences, BaseT::sGenerator));
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Fundamental matrix
 */
auto Fundamental8SolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	return Fundamental7SolverC::MakeModelFromMinimal(minimalModel);
}	//auto MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/********** FundamentalMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented fundamental matrices
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector
 * @pre Scalar components of \c op1 and \c op2 have an absolute value <1
 */
auto FundamentalMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	//Preconditions
	assert(fabs(get<Fundamental7SolverC::iScalar>(op1))<1 );
	assert(fabs(get<Fundamental7SolverC::iScalar>(op2))<1);

	result_type output;

	QuaternionT qDif1=get<Fundamental7SolverC::iRotationL>(op2)*get<Fundamental7SolverC::iRotationL>(op1).conjugate();
	output.segment(0,3)=QuaternionToRotationVector(qDif1);

	QuaternionT qDif2=get<Fundamental7SolverC::iRotationR>(op2)*get<Fundamental7SolverC::iRotationR>(op1).conjugate();
	output.segment(3,3)=QuaternionToRotationVector(qDif2);

	output[6]= fabs( get<Fundamental7SolverC::iScalar>(op2)) - fabs(get<Fundamental7SolverC::iScalar>(op1)) ;
	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

