/**
 * @file DLT.ipp Implementation of DLTC
 * @author Evren Imre
 * @date 11 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DLT_IPP_5871234
#define DLT_IPP_5871234

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <list>
#include <vector>
#include <tuple>
#include <stdexcept>
#include <cstddef>
#include <type_traits>
#include <cmath>
#include "CoordinateTransformation.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Elements/Coordinate.h"
#include "../Numeric/Complexity.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace GeometryN
{

using Eigen::Matrix;
using Eigen::JacobiSVD;
using std::list;
using std::vector;
using std::tie;
using std::runtime_error;
using std::size_t;
using std::is_same;
using std::min;
using std::max;
using std::floor;
using SeeSawN::GeometryN::CoordinateTransformationsT;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::ColsM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::NumericN::ComplexityEstimatorC;
using SeeSawN::NumericN::MakeRankN;

namespace DetailN
{
	/**@brief 2D homography estimation
	 * @ingroup Tag*/
	struct Homography2Tag{};

	/**@brief 3D homography estimation
	 * @ingroup Tag*/
	struct Homography3Tag{};

	/**@brief 3D-2D homography estimation
	 * @ingroup Tag*/
	struct Homography32Tag{};

	/**@brief Epipolar geometry problems
	 * @ingroup Tag*/
	struct EpipolarTag{};
}	//DetailN

/**
 * @brief Normalised discrete linear transformation algorithm
 * @tparam DLTProblemT A DLT problem
 * @remarks Multiple View Geometry, R. Hartley, A. Zissermann, 2nd Ed, 2003, pp.91, Al 4.1
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class DLTProblemT>
class DLTC
{
	private:

		typedef typename DLTProblemT::solution_type SolutionT;	///< Solution type for DLT
		typedef typename DLTProblemT::minimal_coefficient_matrix_type MCoefficientT;	///< A coefficient matrix for the minimal case
		typedef typename DLTProblemT::coefficient_matrix_type GCoefficientT;	///< A coefficient matrix for the general case
		typedef typename DLTProblemT::category_tag CategoryTag;	///< A type indicating the category of the equation system
		static constexpr unsigned int sGenerator=DLTProblemT::generatorSize;	///< Size of the generator

		/** @name Implementation details */ //@{
		template<class MatrixT, class CorrespondenceRangeT> static list<SolutionT> Solve(const CorrespondenceRangeT& correspondences, CategoryTag* dummy, unsigned int minSolution=1);	///< Solves the DLT problem

		template<class MatrixT, class Coordinate1T, class Coordinate2T, class GenericTag> static MatrixT MakeCoefficientMatrix(const vector<Coordinate1T>& set1, const vector<Coordinate2T>& set2, GenericTag* dummy);	///< Generic implementation. Should not be called
		template<class MatrixT> static MatrixT MakeCoefficientMatrix(const vector<Coordinate2DT>& set1, const vector<Coordinate2DT>& set2, DetailN::Homography2Tag* dummy);	///< Makes a coefficient matrix for the 2D homography estimation problem
		template<class MatrixT> static MatrixT MakeCoefficientMatrix(const vector<Coordinate3DT>& set1, const vector<Coordinate3DT>& set2, DetailN::Homography3Tag* dummy);	///< Makes a coefficient matrix for the 3D homography estimation problem
		template<class MatrixT> static MatrixT MakeCoefficientMatrix(const vector<Coordinate3DT>& set1, const vector<Coordinate2DT>& set2, DetailN::Homography32Tag* dummy);	///< Makes a coefficient matrix for the 3D-2D homography estimation problem
		template<class MatrixT> static MatrixT MakeCoefficientMatrix(const vector<Coordinate2DT>& set1, const vector<Coordinate2DT>& set2, DetailN::EpipolarTag* dummy);	///< Makes a coefficient matrix for the epipolar geometry estimation problems

		template<class MatrixT> static MatrixT MakeDenormaliser2(const MatrixT& mT2);	///< Computes the denormaliser corresponding to the normaliser for the second set
		//@}

	public:

		typedef SolutionT solution_type;	///< Solution type for DLT
		template<class CorrespondenceRangeT> static list<SolutionT> Run(const CorrespondenceRangeT& correspondences, unsigned int minSolution=1);	///< Runs the algorithm

		static double Cost(unsigned int nCorrespondence=sGenerator);	///< Computational cost

};	//class DLTC

/**
 * @brief DLT problem template
 * @tparam SolutionT A DLT solution
 * @tparam MCoefficientT A coefficient matrix for the minimal case
 * @tparam GCoefficientT A coefficient matrix for the general case
 * @tparam CategoryTag A type indicating the problem category
 * @tparam GENERATOR_SIZE Cardinality of the minimal generator
 * @tparam MIN_RANK Minimum allowed rank for the geometric entity
 * @tparam MAX_RANK Maximum allowed rank for the geometric entity
 * @pre \c SolutionT is a fixed-size matrix
 * @pre \c MCoefficientT is a fixed-size matrix
 * @pre \c GCoefficentT has a fixed number of columns
 * @pre \c CategoryTag is a DLT problem category tag (unenforced)
 * @pre \c MIN_RANK<=MAX_RANK
 * @remarks No dedicated tests. Tested via geometry solvers
 * @ingroup Problem
 */
template<class SolutionT, class MCoefficientT, class GCoefficientT, class CategoryTag, unsigned int GENERATOR_SIZE, unsigned int MIN_RANK, unsigned int MAX_RANK>
struct DLTProblemC
{
	//Preconditions
	static_assert(RowsM<SolutionT>::value!=Eigen::Dynamic, "DLTProblemC : SolutionT must be a fixed-size matrix.");
	static_assert(ColsM<SolutionT>::value!=Eigen::Dynamic, "DLTProblemC : SolutionT must be a fixed-size matrix.");
	static_assert(RowsM<MCoefficientT>::value!=Eigen::Dynamic, "DLTProblemC : MCoefficientT must be a fixed-size matrix.");
	static_assert(ColsM<MCoefficientT>::value!=Eigen::Dynamic, "DLTProblemC : MCoefficientT must be a fixed-size matrix.");
	static_assert(ColsM<GCoefficientT>::value!=Eigen::Dynamic, "DLTProblemC : GCoefficientT must have a fixed number of columns.");
	static_assert(MIN_RANK<=MAX_RANK, "DLTProblemC : MIN_RANK cannot be greater than MAX_RANK.");

	typedef SolutionT solution_type;	///< A solution to DLT
	typedef MCoefficientT minimal_coefficient_matrix_type;	///< A coefficient matrix for the minimal case
	typedef GCoefficientT coefficient_matrix_type;	///< A coefficient matrix for the general case
	typedef CategoryTag category_tag;	///< Type of the problem
	static constexpr unsigned int generatorSize=GENERATOR_SIZE;	///< Size of the minimal generator
	static constexpr unsigned int minRank=MIN_RANK;	///< Minimum rank for a valid solution
	static constexpr unsigned int maxRank=MAX_RANK;	///< Maximum rank for a valid solution
};	//struct DLTProblemC

/********** DLTC **********/

/**
 * @brief Generic implementation. Should not be called
 * @param[in] set1 First coordinate set
 * @param[in] set2 Second coordinate set
 * @param dummy Dummy parameter for overload resolution
 * @throws runtime_error Always
 * @return An uninitialised matrix
 */
template<class DLTProblemT>
template<class MatrixT, class Coordinate1T, class Coordinate2T, class GenericTag>
auto DLTC<DLTProblemT>::MakeCoefficientMatrix(const vector<Coordinate1T>& set1, const vector<Coordinate2T>& set2, GenericTag* dummy) -> MatrixT
{
	throw(runtime_error("DLTC::MakeCoefficientMatrix : This is the generic implementation, and should not be called from the user code."));

	return MatrixT();
}	//MatrixT MakeCoefficientMatrix(const vector<Coordinate1T>& set1, const vector<Coordinate2T>& set2, CategoryTag* dummy)

/**
 * @brief Makes a coefficient matrix for the 2D homography estimation problem
 * @tparam A coefficient matrix
 * @param[in] set1 First coordinate set
 * @param[in] set2 Second coordinate set
 * @param[in] dummy Dummy parameter for overload resolution
 * @return The coefficient matrix for the correspondences
 * @pre \c MatrixT is either a 8x9 fixed-size matrix, or has 9 columns and is dynamic in rows.
 * @remarks Multiple View Geometry, R. Hartley, A. Zissermann, 2nd Ed, 2003, pp.91, Eq 4.3
 */
template<class DLTProblemT>
template<class MatrixT>
auto DLTC<DLTProblemT>::MakeCoefficientMatrix(const vector<Coordinate2DT>& set1, const vector<Coordinate2DT>& set2, DetailN::Homography2Tag* dummy) -> MatrixT
{
	//Preconditions
	static_assert(ColsM<MatrixT>::value==9, "DLTC::MakeCoefficientMatrix: MatrixT must have 9 columns.");
	static_assert(RowsM<MatrixT>::value==8 || RowsM<MatrixT>::value==Eigen::Dynamic, "DLTC::MakeCoefficientMatrix: MatrixT must either have 8 rows, or be dynamic in the rows.");

	size_t nEquations=set1.size()*2;

	MatrixT mA(nEquations,9);
	size_t iCorr=0;
	for(size_t c=0; c<nEquations; c+=2, ++iCorr)
	{
		size_t cp1=c+1;

		mA(c,0)=0; mA(cp1,0)=set1[iCorr](0);
		mA(c,1)=0; mA(cp1,1)=set1[iCorr](1);
		mA(c,2)=0; mA(cp1,2)=1;

		mA(c,3)=-mA(cp1,0); mA(cp1,3)=0;
		mA(c,4)=-mA(cp1,1); mA(cp1,4)=0;
		mA(c,5)=-1; mA(cp1,5)=0;

		mA(c,6)=mA(cp1,0)*set2[iCorr](1); mA(cp1,6)=mA(c,3)*set2[iCorr](0);
		mA(c,7)=mA(cp1,1)*set2[iCorr](1); mA(cp1,7)=mA(c,4)*set2[iCorr](0);
		mA(c,8)=set2[iCorr](1); 	  	  mA(cp1,8)=-set2[iCorr](0);
	}	//for(size_t c=0; c<=nEquations; c+=2)

	return mA;
}	//MatrixT MakeCoefficientMatrix(const vector<Coordinate1T>& set1, const vector<Coordinate2T>& set2, DetailN::Homography2Tag* dummy)

/**
 * @brief Makes a coefficient matrix for the 3D homography estimation problem
 * @tparam A coefficient matrix
 * @param[in] set1 First coordinate set
 * @param[in] set2 Second coordinate set
 * @param[in] dummy Dummy parameter for overload resolution
 * @return The coefficient matrix for the correspondences
 * @pre \c MatrixT is either a 15x16 fixed-size matrix, or has 16 columns and is dynamic in rows.
 * @remarks "Uncertain Projective Geometry," S. Heuel, Springer, pp.68 & pp.91
 */
template<class DLTProblemT>
template<class MatrixT>
auto DLTC<DLTProblemT>::MakeCoefficientMatrix(const vector<Coordinate3DT>& set1, const vector<Coordinate3DT>& set2, DetailN::Homography3Tag* dummy) -> MatrixT
{
	//Preconditions
	static_assert(ColsM<MatrixT>::value==16, "DLTC::MakeCoefficientMatrix: MatrixT must have 16 columns.");
	static_assert(RowsM<MatrixT>::value==15 || RowsM<MatrixT>::value==Eigen::Dynamic, "DLTC::MakeCoefficientMatrix: MatrixT must either have 15 rows, or be dynamic in the rows.");

	size_t nEquations=set1.size()*3;

	MatrixT mA(nEquations,16); mA.setZero();
	size_t iCorr=0;
	for(size_t c=0; c<nEquations; c+=3, ++iCorr)
	{
		typedef Matrix<typename ValueTypeM<Coordinate3DT>::type, 1, 3> Row3DT;
		Row3DT x=set1[iCorr].transpose();
		Row3DT y=set2[iCorr].transpose();

		size_t i=c;
		mA.row(i).segment(0,3)=x; mA(i,3)=1;
		mA.row(i).segment(12,3)=-y[0]*x; mA(i,15)=-y[0];
		++i;

		mA.row(i).segment(4,3)=x; mA(i,7)=1;
		mA.row(i).segment(12,3)=-y[1]*x; mA(i,15)=-y[1];
		++i;

		mA.row(i).segment(8,3)=x; mA(i,11)=1;
		mA.row(i).segment(12,3)=-y[2]*x; mA(i,15)=-y[2];
	}	//for(size_t c=0; c<=nEquations; c+=3)

	return mA;
}	//MatrixT MakeCoefficientMatrix(const vector<Coordinate3DT>& set1, const vector<Coordinate3DT>& set2, DetailN::Homography3Tag* dummy)

/**
 * @brief Makes a coefficient matrix for the 3D-2D homography estimation problem
 * @tparam A coefficient matrix
 * @param[in] set1 First coordinate set
 * @param[in] set2 Second coordinate set
 * @param[in] dummy Dummy parameter for overload resolution
 * @return The coefficient matrix for the correspondences
 * @pre \c MatrixT is either a 12x12 fixed-size matrix, or has 12 columns and is dynamic in rows.
 * @remarks  R. Hartley, A. Zisserman, "Multiple View Geometry in Computer Vision", 2nd Edition, 2003, pp. 179, Eq. 7.2
 */
template<class DLTProblemT>
template<class MatrixT>
auto DLTC<DLTProblemT>::MakeCoefficientMatrix(const vector<Coordinate3DT>& set1, const vector<Coordinate2DT>& set2, DetailN::Homography32Tag* dummy) -> MatrixT
{
	//Preconditions
	static_assert(ColsM<MatrixT>::value==12, "DLTC::MakeCoefficientMatrix: MatrixT must have 12 columns.");
	static_assert(RowsM<MatrixT>::value==12 || RowsM<MatrixT>::value==Eigen::Dynamic, "DLTC::MakeCoefficientMatrix: MatrixT must either have 12 rows, or be dynamic in the rows.");

	size_t nEquations=set1.size()*2;

	MatrixT mA(nEquations,12);

	size_t iCorr=0;
	for(size_t c=0; c<nEquations; c+=2, ++iCorr)
	{
		size_t cp1=c+1;

		mA(c,0)=0; mA(cp1,0)=set1[iCorr](0);
		mA(c,1)=0; mA(cp1,1)=set1[iCorr](1);
		mA(c,2)=0; mA(cp1,2)=set1[iCorr](2);
		mA(c,3)=0; mA(cp1,3)=1;

		mA(c,4)=-set1[iCorr](0); mA(cp1,4)=0;
		mA(c,5)=-set1[iCorr](1); mA(cp1,5)=0;
		mA(c,6)=-set1[iCorr](2); mA(cp1,6)=0;
		mA(c,7)=-1; mA(cp1,7)=0;

		mA(c,8)=set1[iCorr](0)*set2[iCorr](1); mA(cp1,8)=mA(c,4)*set2[iCorr](0);
		mA(c,9)=set1[iCorr](1)*set2[iCorr](1); mA(cp1,9)=mA(c,5)*set2[iCorr](0);
		mA(c,10)=set1[iCorr](2)*set2[iCorr](1); mA(cp1,10)=mA(c,6)*set2[iCorr](0);
		mA(c,11)=set2[iCorr](1); mA(cp1,11)=-set2[iCorr](0);
	}	//for(size_t c=0; c<=nEquations; c+=2)

	return mA;
}	//MatrixT MakeCoefficientMatrix(const vector<Coordinate3DT>& set1, const vector<Coordinate3DT>& set2, DetailN::Homography32Tag* dummy)

/**
 * @brief Makes a coefficient matrix for the epipolar geometry estimation problems
 * @tparam A coefficient matrix
 * @param[in] set1 First coordinate set
 * @param[in] set2 Second coordinate set
 * @param[in] dummy Dummy parameter for overload resolution
 * @return The coefficient matrix for the correspondences
 * @pre \c MatrixT has 9 columns. The number of rows may be 5, 7, 8 or dynamic.
 * @remarks R. Hartley, A. Zisserman, "Multiple View Geometry in Computer Vision", 2nd Edition, 2003, pp. 279, Eq. 11.3
 */
template<class DLTProblemT>
template<class MatrixT>
MatrixT DLTC<DLTProblemT>::MakeCoefficientMatrix(const vector<Coordinate2DT>& set1, const vector<Coordinate2DT>& set2, DetailN::EpipolarTag* dummy)
{
	//Preconditions
	static_assert(ColsM<MatrixT>::value==9, "DLTC::MakeCoefficientMatrix: MatrixT must have 9 columns.");
	static_assert(RowsM<MatrixT>::value==5 || RowsM<MatrixT>::value==6|| RowsM<MatrixT>::value==7 || RowsM<MatrixT>::value==8 || RowsM<MatrixT>::value==Eigen::Dynamic, "DLTC::MakeCoefficientMatrix: Invalid number of rows for MatrixT");

	size_t nEquations=set1.size();
	MatrixT mA(nEquations,9);
	for(size_t c=0; c<nEquations; ++c)
	{
		mA(c,0)= set2[c][0] * set1[c][0];
		mA(c,1)= set2[c][0] * set1[c][1];
		mA(c,2)= set2[c][0];

		mA(c,3)= set2[c][1] * set1[c][0];
		mA(c,4)= set2[c][1] * set1[c][1];
		mA(c,5)= set2[c][1];

		mA(c,6)= set1[c][0];
		mA(c,7)= set1[c][1];
		mA(c,8)= 1;
	}	//for(size_t c=0; c<nEquations; ++c)

	return mA;
}	//MatrixT MakeCoefficientMatrix(const vector<Coordinate2DT>& set1, const vector<Coordinate2DT>& set2, DetailN::EpipolarTag* dummy)

/**
 * @brief Computes the denormaliser corresponding to the normaliser for the second set
 * @tparam MatrixT A matrix
 * @param[in] mT2 Normalising transformation for the second set
 * @return Denormalising transformation for the second set
 * @pre \c MatrixT is an Eigen dense matrix (unenforced)
 * @remarks For constraints of type x2=Hx1, denormaliser is the inverse. For x2Fx1=0, transpose
 */
template<class DLTProblemT>
template<class MatrixT>
MatrixT DLTC<DLTProblemT>::MakeDenormaliser2(const MatrixT& mT2)
{
	if(is_same<typename DLTProblemT::category_tag, DetailN::EpipolarTag>::value)
		return mT2.transpose();
	else
		return mT2.inverse();
}	//MatrixT MakeDenormaliser2(const MatrixT& mT2)

/**
 * @brief Solves the DLT problem
 * @tparam MatrixT A coefficient matrix
 * @tparam CorrespondenceRangeT A range of coordinate correspondences
 * @param[in] correspondences Correspondences
 * @param[in] dummy Dummy parameter for overload resolution
 * @param[in] minSolution Minimum number of solutions the solver returns
 * @pre \c CorrespondenceRangeT is a bimap (unenforced)
 * @return A list of solutions
 */
template<class DLTProblemT>
template<class MatrixT, class CorrespondenceRangeT>
auto DLTC<DLTProblemT>::Solve(const CorrespondenceRangeT& correspondences, CategoryTag* dummy, unsigned int minSolution) -> list<SolutionT>
{
	list<SolutionT> output;

	typedef typename CorrespondenceRangeT::left_key_type Coordinate1T;
	typedef typename CorrespondenceRangeT::right_key_type Coordinate2T;

	//Detach the sets from the bimap
	vector<Coordinate1T> set1;
	vector<Coordinate2T> set2;
	tie(set1, set2)=BimapToVector<Coordinate1T, Coordinate2T>(correspondences);

	//Normalise
	typedef CoordinateTransformationsT<Coordinate1T> Transformer1T;
	typename Transformer1T::affine_transform_type normaliser1=Transformer1T::ComputeNormaliser(set1);
	vector<Coordinate1T> normalised1=Transformer1T::ApplyAffineTransformation(set1, normaliser1);

	typedef CoordinateTransformationsT<Coordinate2T> Transformer2T;
	typename Transformer2T::affine_transform_type normaliser2=Transformer2T::ComputeNormaliser(set2);
	vector<Coordinate2T> normalised2=Transformer2T::ApplyAffineTransformation(set2, normaliser2);

	//Construct the equation system
	MatrixT mA=MakeCoefficientMatrix<MatrixT>(normalised1, normalised2, dummy);
	assert(mA.cols()>1);

	//Solve
	JacobiSVD<MatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd(mA, Eigen::ComputeFullU | Eigen::ComputeFullV);	//Since we are working with homogeneous equations, QR and LU are not applicable

	//Extract the solutions and denormalise

	unsigned int nSolution=max(minSolution, (unsigned int)((mA.rows()>=mA.cols()) ? 1 : mA.cols()-mA.rows()) );
/*
 	//Removed, as it interferes with the fundamental matrix estimation in the case of pure translation and planar motion
	//Degeneracy check
	if(svd.singularValues()(mA.cols()-nSolution-1)<=zero)
		return output;
*/
	//Solution set is the smallest nSolution singular vectors
	typename Transformer1T::affine_transform_type::MatrixType mT1=normaliser1.matrix();
	typename Transformer2T::affine_transform_type::MatrixType mT2d=MakeDenormaliser2(normaliser2.matrix());
	size_t sV=svd.matrixV().cols();

	bool flagRankCheck= (DLTProblemT::minRank>0) || (DLTProblemT::maxRank < (unsigned int)min(RowsM<SolutionT>::value, ColsM<SolutionT>::value));	//If a solution can potentially fail the rank conditions, skip

	for(size_t c=1; c<=nSolution; ++c)
	{
		//Convert to SolutionT
		SolutionT mSol;
		mSol=Reshape<SolutionT>(svd.matrixV().col(sV-c), mSol.rows(), mSol.cols());	//mSol is fixed size

		SolutionT mSolD=mT2d*((mSol*mT1).eval());	//Denormalise

		//If the solution can potentially violate the rank condition
		if(flagRankCheck)
		{
			//Rank reduction
			unsigned int rank;
			tie(rank, mSolD)=MakeRankN<Eigen::FullPivHouseholderQRPreconditioner>(mSolD, DLTProblemT::maxRank);

			//Rank test
			if(rank<DLTProblemT::minRank)
				continue;
		}	//if(flagRankCheck)

		//Make unit norm
		mSolD.normalize();
		output.push_front(mSolD);	//So that the order of the singular vectors is preserved
	}	//for(size_t c=1; c<=nSolution; ++c)

	return output;
}	//list<SolutionT> Solve(const CorrespondenceRangeT& correspondences, CategoryTag* dummy)

/**
 * @brief Runs the algorithm
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence list
 * @param[in] minSolution Minimum number of solutions the solver returns
 * @return A list of solutions. Empty list if the algorithm fails
 * @post Existing solutions are always unit norm
 */
template<class DLTProblemT>
template<class CorrespondenceRangeT>
auto DLTC<DLTProblemT>::Run(const CorrespondenceRangeT& correspondences, unsigned int minSolution) -> list<SolutionT>
{
	size_t nCorrespondences=boost::distance(correspondences);
	CategoryTag* categoryTag=nullptr;

	//If not enough correspondences, quit
	if(nCorrespondences<sGenerator)
		return list<SolutionT>();

	//Minimal case uses fixed-size structures, so faster
	if(nCorrespondences==sGenerator)
		return Solve<MCoefficientT>(correspondences, categoryTag, minSolution);
	else
		return Solve<GCoefficientT>(correspondences, categoryTag, minSolution);
}	//list<SolutionT> Run(const CorrespondenceRangeT& correspondences)

/**
 * @brief Computational cost
 * @param[in] nCorrespondence Number of number of correspondences
 * @return Estimated cost
 * @pre \c nCorrespondence>=sGenerator
 */
template<class DLTProblemT>
double DLTC<DLTProblemT>::Cost(unsigned int nCorrespondence)
{
	assert(nCorrespondence>=sGenerator);

	unsigned int nUnknowns=ColsM<MCoefficientT>::value;
	bool flagDouble=is_same<typename ValueTypeM<MCoefficientT>::type, double>::value;

	unsigned int nConstraints= nCorrespondence * floor(RowsM<MCoefficientT>::value/sGenerator);	//# constraints = # correspondences * # constraints per correspondence
	unsigned int nSolution=(nConstraints>=nUnknowns) ? 1 : nUnknowns - nConstraints;

	unsigned int m=RowsM<SolutionT>::value;
	unsigned int n=ColsM<SolutionT>::value;

	//Normalisation
	double costNormaliser= ComplexityEstimatorC::VectorSampleVariance(m-1, nCorrespondence, flagDouble) +  ComplexityEstimatorC::VectorSampleVariance(n-1, nCorrespondence, flagDouble);
	double costNormalise= nCorrespondence * (ComplexityEstimatorC::MatrixVectorMultiplication(m, m, flagDouble) + ComplexityEstimatorC::MatrixVectorMultiplication(n, n, flagDouble));

	//SVD
	double costSVD=ComplexityEstimatorC::SVD(nConstraints, nUnknowns, flagDouble);

	//Rank adjustment
	bool flagRankCheck= (DLTProblemT::minRank>0) || (DLTProblemT::maxRank < (unsigned int)min(RowsM<SolutionT>::value, ColsM<SolutionT>::value));	//If a solution can potentially fail the rank conditions, skip
	double costRank= flagRankCheck ? (ComplexityEstimatorC::SVD(m, n, flagDouble) + ComplexityEstimatorC::MatrixMultiplication(m, m, n) + ComplexityEstimatorC::MatrixMultiplication(m, n, n) ):0;	//Estimated cost of MakeRankN

	//Denormalisation per solution
	double costDenormaliser=0;
	if(!is_same<typename DLTProblemT::category_tag, DetailN::EpipolarTag>::value)
		costDenormaliser=ComplexityEstimatorC::Inverse(m, flagDouble)+ComplexityEstimatorC::Inverse(n, flagDouble);

	double costDenormalise=ComplexityEstimatorC::MatrixMultiplication(m, m, n, flagDouble) + ComplexityEstimatorC::MatrixMultiplication(m, n, n, flagDouble) + ComplexityEstimatorC::FrobeniusNorm(m, n, flagDouble) + ComplexityEstimatorC::MatrixScale(m, n, flagDouble);

	return costNormaliser + costNormalise + costSVD + costDenormaliser + nSolution*(costRank+costDenormalise);
}	//double Cost(unsigned int nConstraints)

}	//GeometryN
}	//SeeSawN

#endif /* DLT_IPP_5871234 */
