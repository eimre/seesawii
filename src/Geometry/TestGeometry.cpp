/**
 * @file TestGeometry.cpp Unit tests for Geometry
 * @author Evren Imre
 * @date 24 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE GEOMETRY

#include <boost/test/unit_test.hpp>
#include <boost/bimap.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <list>
#include <set>
#include <cstddef>
#include <tuple>
#include <cmath>
#include "CoordinateTransformation.h"
#include "LensDistortion.h"
#include "Homography2DSolver.h"
#include "Homography3DSolver.h"
#include "FundamentalSolver.h"
#include "EssentialSolver.h"
#include "OneSidedFundamentalSolver.h"
#include "Rotation3DSolver.h"
#include "Similarity3DSolver.h"
#include "P6PSolver.h"
#include "P2PSolver.h"
#include "P2PfSolver.h"
#include "P3PSolver.h"
#include "P4PSolver.h"
#include "Rotation.h"
#include "Triangulation.h"
#include "MultiviewTriangulation.h"
#include "DualAbsoluteQuadricSolver.h"
#include "Projection32.h"
#include "SceneCoverageEstimation.h"
#include "RelativeRotationRegistration.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Numeric/LinearAlgebra.h"
#include "../DataGeneration/LatticeGenerator.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestGeometryN
{

using namespace SeeSawN::GeometryN;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using boost::optional;
using Eigen::Matrix3d;
using Eigen::VectorXd;
using std::vector;
using std::list;
using std::set;
using std::size_t;
using std::tie;
using std::isinf;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::HCoordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::ComposeSimilarity3D;
using SeeSawN::NumericN::MakeRankN;
using SeeSawN::DataGeneratorN::GenerateRectangularLattice;

BOOST_AUTO_TEST_SUITE(Coordinate_Transformations)

BOOST_AUTO_TEST_CASE(Coordinate_Transformations)
{
    vector<Coordinate2DT> coordinates(2);
    coordinates[0]<< 1, 2;
    coordinates[1]<< 3, 1;

    //ApplyProjectiveTransformation
    CoordinateTransformations2DT::projective_transform_type mT; mT.matrix()<<10, 1, -4, 1, -6, 3, 2, 0, 4;

    vector<HCoordinate2DT> vResult=CoordinateTransformations2DT::ApplyProjectiveTransformation(coordinates, mT);

    BOOST_CHECK_EQUAL(vResult.size(), (size_t)2);
    BOOST_CHECK(vResult[0]==HCoordinate2DT(8, -8, 6) );
    BOOST_CHECK(vResult[1]==HCoordinate2DT(27, 0, 10) );

    //ApplyAffineTransformation
    CoordinateTransformations2DT::affine_transform_type mA; mA.matrix()<<10, 1, -4, 1, -6, 3, 0, 0, 1;
    vector<Coordinate2DT> vResultA=CoordinateTransformations2DT::ApplyAffineTransformation(coordinates, mA);

    BOOST_CHECK_EQUAL(vResultA.size(), (size_t)2);
    BOOST_CHECK(vResultA[0]==Coordinate2DT(8, -8) );
    BOOST_CHECK(vResultA[1]==Coordinate2DT(27, 0) );

    //NormaliseCoordinates
    CoordinateTransformations2DT::affine_transform_type mN=CoordinateTransformations2DT::ComputeNormaliser(coordinates);
    CoordinateTransformations2DT::affine_transform_type mNr; mNr.matrix()<<1, 0, -2, 0, 2, -3, 0, 0, 1;

    BOOST_CHECK(mN.matrix()==mNr.matrix());

    CoordinateTransformations2DT::affine_transform_type mNIso=CoordinateTransformations2DT::ComputeNormaliser(coordinates, true);	//Isotropic
    CoordinateTransformations2DT::affine_transform_type mNIsor; mNIsor.matrix()<<1.26491, 0, -2.52982, 0, 1.26491, -1.89737, 0, 0, 1;
    BOOST_CHECK(mNIsor.matrix().isApprox(mNIso.matrix(), 1e-5));

    vector<Coordinate2DT> coordinates2(2);
    coordinates2[0]<< 1, 2;
    coordinates2[1]<< 1, 1;
    CoordinateTransformations2DT::affine_transform_type mNIso2=CoordinateTransformations2DT::ComputeNormaliser(coordinates2);
    CoordinateTransformations2DT::affine_transform_type mNIsor2; mNIsor2.matrix()<<1, 0, -1, 0, 2, -3, 0, 0, 1;
    BOOST_CHECK(mNIsor2.matrix().isApprox(mNIso2.matrix(), 1e-5));

    //Project

    vector<Coordinate3DT> coordinates3D(2);
    coordinates3D[0]<< 1, 1, 1;
    coordinates3D[1]<< 2, 1, 1;

    typedef typename ValueTypeM<Coordinate3DT>::type RealT;
    Matrix<RealT, 3, 4> mP; mP<<100, 0, 50, 50, 200, 25, 100, 100, 3, 2, 1, 1;
    vector<HCoordinate2DT> vResultG=CoordinateTransformations3DT::HomogeneousProject(coordinates3D, mP);

    BOOST_CHECK(vResultG[0]==HCoordinate2DT(200, 425, 7));
    BOOST_CHECK(vResultG[1]==HCoordinate2DT(300, 625, 10));

    //Normalise homogeneous vectors
    vector<HCoordinate2DT> hCoordinates(2);
    hCoordinates[0]<<200, 425, 7;
    hCoordinates[1]<<300, 625, 10;

    vector<Coordinate2DT> nCoordinates=CoordinateTransformations2DT::NormaliseHomogeneous(hCoordinates);
    BOOST_CHECK(nCoordinates[0]==Coordinate2DT(200/7.0, 425/7.0 ));
    BOOST_CHECK(nCoordinates[1]==Coordinate2DT(300/10.0, 625/10.0 ));

    //Single-point transformations

    Coordinate2DT vPt1=CoordinateTransformations2DT::TransformCoordinate(coordinates[0], mT);
    BOOST_CHECK(vResult[0].hnormalized()==vPt1);

    Coordinate2DT vAt1=CoordinateTransformations2DT::TransformCoordinate(coordinates[0], mA);
	BOOST_CHECK(vResultA[0]==vAt1);

}   //BOOST_AUTO_TEST_CASE(Coordinate_Transformations)

BOOST_AUTO_TEST_CASE(Quantisation)
{
    vector<Coordinate3DT> coordinates(4);
    coordinates[0]<< 1, 1, 1;
    coordinates[1]<< 2, 1, 1;
    coordinates[2]<< 1, 2, 1;
    coordinates[3]<< 1, 1, 1.5;

    CoordinateTransformations3DT::ArrayD binSize(1,0.5,0.2);
    CoordinateTransformations3DT::ArrayD2 extent(3,2); extent<<-0.4, 2.1, -0.71, 3.63, 0.01, 2.21;
    vector<vector<unsigned int>> buckets=CoordinateTransformations3DT::Bucketise(coordinates, binSize, extent, true);

    vector<unsigned int> b0{161, 160, 173, 172, 281, 280, 293, 292};
    BOOST_CHECK_EQUAL_COLLECTIONS(b0.begin(), b0.end(), buckets[0].begin(), buckets[0].end());

    vector<unsigned int> b2{185, 184, 197, 196, 305, 304, 317, 316};
    BOOST_CHECK_EQUAL_COLLECTIONS(b2.begin(), b2.end(), buckets[2].begin(), buckets[2].end());

    //No overlap case
    vector<vector<unsigned int>> bucketsNO=CoordinateTransformations3DT::Bucketise(coordinates, binSize, extent, false);

    BOOST_CHECK_EQUAL(bucketsNO[0].size(), (size_t)1);
    BOOST_CHECK_EQUAL(*bucketsNO[0].begin(), (unsigned int)161);

    BOOST_CHECK_EQUAL(bucketsNO[2].size(), (size_t)1);
    BOOST_CHECK_EQUAL(*bucketsNO[2].begin(), (unsigned int)185);

    //No overlap- separate function
    vector<unsigned int> bucketsNO2=CoordinateTransformations3DT::Bucketise(coordinates, binSize, extent);
    BOOST_CHECK_EQUAL(bucketsNO2[0], (unsigned int)161);
    BOOST_CHECK_EQUAL(bucketsNO2[2], (unsigned int)185);

}   //BOOST_AUTO_TEST_CASE(Quantisation)

BOOST_AUTO_TEST_CASE(Scene_Image_Projection)
{
	//No distortion

	QuaternionT q(0.25, 0.5, 0.75, 0.15); q.normalize();
	IntrinsicCalibrationMatrixT mK; mK.setIdentity();
	mK(0,0)=1000; mK(1,1)=1001; mK(0,1)=1e-5; mK(0,2)=960; mK(1,2)=540;
	CameraMatrixT mP=ComposeCameraMatrix(mK, Coordinate3DT(1, 1.2, 1.5), q);

	Coordinate3DT point3D(1, 1, 1);

	Projection32C p1;
	Projection32C p2(mP);

	Coordinate2DT point2D=p2(point3D);
	Coordinate2DT point2Dgt= (mP*point3D.homogeneous()).eval().hnormalized();

	BOOST_CHECK(point2D==point2Dgt);

	//Distortion

	LensDistortionNoDC noDistortion;
	Projection32dC<LensDistortionNoDC> p3(mP, noDistortion);
	optional<Coordinate2DT> point2D3=p3(point3D);

	BOOST_CHECK(point2D3);
	BOOST_CHECK(*point2D3==point2Dgt);

	LensDistortionFP1C fp1Distortion(Coordinate2DT(1,1), 1e-6);
	Projection32dC<LensDistortionFP1C> p4(mP, fp1Distortion);
	optional<Coordinate2DT> point2D4=p4(point3D);

	BOOST_CHECK(point2D4);
	BOOST_CHECK(*point2D4==*fp1Distortion.Apply(point2D));

	//Distorted projection via Projection32C

	optional<Coordinate2DT> point2D5=p2(point3D, fp1Distortion);
	BOOST_CHECK(point2D5);
	BOOST_CHECK( (*point2D5) == (*point2D4));

	LensDistortionC distortion6;
	distortion6.modelCode=LensDistortionCodeT::NOD;
	distortion6.centre=Coordinate2DT(1,1);
	distortion6.kappa=1;
	optional<Coordinate2DT> point2D6=p2(point3D, distortion6);
	BOOST_CHECK(point2D6);
	BOOST_CHECK( (*point2D6) == point2D);

	LensDistortionC distortion7;
	distortion7.modelCode=LensDistortionCodeT::FP1;
	distortion7.centre=Coordinate2DT(1,1);
	distortion7.kappa=1e-6;
	optional<Coordinate2DT> point2D7=p2(point3D, distortion7);
	BOOST_CHECK(point2D7);
	BOOST_CHECK( (*point2D7) == (*point2D4));
}	//BOOST_AUTO_TEST_CASE(Scene_Image_Projection)

BOOST_AUTO_TEST_SUITE_END() //Coordinate_Transformations

BOOST_AUTO_TEST_SUITE(Coordinate_Statistics)

BOOST_AUTO_TEST_CASE(Coordinate_Statistics)
{
    vector<Coordinate2DT> coordinates(4);
    coordinates[0]<< 1, 1;
    coordinates[1]<< 2, 4;
    coordinates[2]<< 1, 2;
    coordinates[3]<< 0, 0;

    CoordinateStatistics2DT::ArrayD meanX;
    CoordinateStatistics2DT::ArrayD stdX;
    tie(meanX, stdX)=*CoordinateStatistics2DT::ComputeMeanAndStD(coordinates);
    BOOST_CHECK(meanX.matrix()==Coordinate2DT(1, 1.75));
    BOOST_CHECK(stdX.isApprox(CoordinateStatistics2DT::ArrayD(0.707107, 1.47902), 1e-6 ) );

    BOOST_CHECK(!CoordinateStatistics2DT::ComputeMeanAndStD(vector<Coordinate2DT>()));

    CoordinateStatistics2DT::ArrayD2 boundingBox=*CoordinateStatistics2DT::ComputeExtent(coordinates);
    CoordinateStatistics2DT::ArrayD2 boundingBoxR; boundingBoxR<<0, 2, 0, 4;
    BOOST_CHECK(boundingBox.matrix()==boundingBoxR.matrix());

    BOOST_CHECK(!CoordinateStatistics2DT::ComputeExtent(vector<Coordinate2DT>()));

}   //BOOST_AUTO_TEST_CASE(Coordinate_Statistics)

BOOST_AUTO_TEST_SUITE_END() //Coordinate_Statistics

BOOST_AUTO_TEST_SUITE(Lens_Distortion)

BOOST_AUTO_TEST_CASE(No_distortion)
{
    Coordinate2DT centre(960, 540);
    typedef ValueTypeM<Coordinate2DT>::type RealT;
    RealT kappa=-6e-5;

    BOOST_CHECK(LensDistortionNoDC::modelCode==LensDistortionCodeT::NOD);

    LensDistortionNoDC distorter1;
	LensDistortionNoDC distorter2(centre, kappa);

	Coordinate2DT undistorted(600, 400);

	Coordinate2DT distorted=*distorter2.Apply(undistorted);
	Coordinate2DT corrected=*distorter2.Correct(distorted);

	BOOST_CHECK(distorter2.Correct(distorted));
	BOOST_CHECK(corrected==undistorted);
	BOOST_CHECK(distorted==undistorted);
}	//BOOST_AUTO_TEST_CASE(No_distortion)

BOOST_AUTO_TEST_CASE(Full_Polynomial1)
{
    Coordinate2DT centre(960, 540);
    typedef ValueTypeM<Coordinate2DT>::type RealT;
    RealT kappa=-6e-5;

    BOOST_CHECK(LensDistortionFP1C::modelCode==LensDistortionCodeT::FP1);

    LensDistortionFP1C distorter1;
    LensDistortionFP1C distorter2(centre, kappa);

    Coordinate2DT undistorted(600, 400);

    Coordinate2DT distorted=*distorter2.Apply(undistorted);
    Coordinate2DT corrected=*distorter2.Correct(distorted);

    BOOST_CHECK(distorter2.Correct(distorted));
    BOOST_CHECK(corrected.isApprox(undistorted, 1e-14));
    BOOST_CHECK(distorted.isApprox(Coordinate2DT(608.343, 403.245), 1e-3));
}   //BOOST_AUTO_TEST_CASE(Full_Polynomial1)

BOOST_AUTO_TEST_CASE(Odd_Polynomial1)
{
    Coordinate2DT centre(960, 540);
    typedef ValueTypeM<Coordinate2DT>::type RealT;
    RealT kappa=-1e-8;

    BOOST_CHECK(LensDistortionOP1C::modelCode==LensDistortionCodeT::OP1);

    LensDistortionOP1C distorter1;
    LensDistortionOP1C distorter2(centre, kappa);

    Coordinate2DT undistorted(600, 400);

    Coordinate2DT distorted=*distorter2.Apply(undistorted);
    Coordinate2DT corrected=*distorter2.Correct(distorted);

    BOOST_CHECK(distorter2.Correct(distorted));
    BOOST_CHECK(corrected.isApprox(undistorted, 1e-7));
    BOOST_CHECK(distorted.isApprox(Coordinate2DT(600.537, 400.209), 1e-3));
}   //BOOST_AUTO_TEST_CASE(Division_Polynomial1)


BOOST_AUTO_TEST_CASE(Division)
{
    Coordinate2DT centre(960, 540);
    typedef ValueTypeM<Coordinate2DT>::type RealT;
    RealT kappa=-1e-7;

    BOOST_CHECK(LensDistortionDivC::modelCode==LensDistortionCodeT::DIV);

    LensDistortionDivC distorter1;
    LensDistortionDivC distorter2(centre, kappa);

    Coordinate2DT undistorted(600, 400);

    Coordinate2DT distorted=*distorter2.Apply( undistorted);
    BOOST_CHECK(distorter2.Apply( undistorted));
    BOOST_CHECK(distorted.isApprox(Coordinate2DT(605.22, 402.03), 1e-3));

    Coordinate2DT corrected=*distorter2.Correct(distorted);

    BOOST_CHECK(distorter2.Correct(distorted));
    BOOST_CHECK(corrected.isApprox(undistorted, 1e-7));
}   //BOOST_AUTO_TEST_CASE(Division)

BOOST_AUTO_TEST_CASE(LENS_DISTORTION_FREE)
{
	Coordinate2DT centre(960, 540);
	typedef ValueTypeM<Coordinate2DT>::type RealT;
	RealT kappa=-6e-5;

	vector<Coordinate2DT> points{Coordinate2DT(600,400), Coordinate2DT(650, 350)};
	vector<optional<Coordinate2DT> > distorted=ApplyDistortion(points, LensDistortionCodeT::FP1, centre, kappa);

	LensDistortionFP1C distorter(centre, kappa);
	BOOST_CHECK(distorted[0]->isApprox(*distorter.Apply(points[0]), 1e-6));
	BOOST_CHECK(distorted[1]->isApprox(*distorter.Apply(points[1]), 1e-6));

	vector<optional<Coordinate2DT> > undistorted=ApplyDistortionCorrection(points, LensDistortionCodeT::FP1, centre, kappa);
	BOOST_CHECK(undistorted[0]->isApprox(*distorter.Correct(points[0]), 1e-6));
	BOOST_CHECK(undistorted[1]->isApprox(*distorter.Correct(points[1]), 1e-6));
}	//BOOST_AUTO_TEST_CASE(LENS_DISTORTION_FREE)

BOOST_AUTO_TEST_SUITE_END() //Lens_Distortion

BOOST_AUTO_TEST_SUITE(Geometry_Solvers)

BOOST_AUTO_TEST_CASE(GeometrySolverBase)
{
	Homography2DSolverC solver;

	BOOST_CHECK_EQUAL(solver.GeneratorSize(), 4);
	BOOST_CHECK_EQUAL(solver.IsNonminimal(), true);
	BOOST_CHECK_EQUAL(solver.MaxSolution(), 1);
	BOOST_CHECK_EQUAL(solver.DoF(), 8);
	BOOST_CHECK_EQUAL(solver.ValidateGenerator(CoordinateCorrespondenceList2DT()), true);

	typedef typename Homography2DSolverC::model_type ModelT;
	ModelT mH1; mH1<<2.5, 0, 0.5, 0, 3, 1, 0, 0, 1;
	mH1.normalize();
	
	ModelT mH2; mH2<<1.5, 0, -0.5, 0, -2, 0.5, 0, 0, 1;
	mH2.normalize();

	ModelT mH1d(mH1);

	typedef Homography2DSolverC::real_type RealT;
	
	//Similarity
	BOOST_CHECK_SMALL(Homography2DSolverC::ComputeDistance(mH1, mH1), (RealT)1e-6);
	BOOST_CHECK_SMALL(Homography2DSolverC::ComputeDistance(mH1, -mH1), (RealT)1e-6);
	BOOST_CHECK_CLOSE(Homography2DSolverC::ComputeDistance(mH2, mH1), (RealT)0.914132, (RealT)5e-2);

	//Vector conversions
	typedef Matrix<RealT, Eigen::Dynamic, 1> VectorT;
	VectorT vH1v=Homography2DSolverC::MakeVector(mH1);
	ModelT mH3=Homography2DSolverC::MakeModel(vH1v);
	BOOST_CHECK(mH3.isApprox(mH1, 1e-7));

	VectorT vH1dv=Homography2DSolverC::MakeVector(mH1d);
	ModelT mH3d=Homography2DSolverC::MakeModel(vH1dv, false);
	BOOST_CHECK(mH3d.isApprox(mH1d, 1e-7));

	//Model evaluation

	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;

	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(1,1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(1,-1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-1,0)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0,0), Coordinate2DT(0,0)));

	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(3,4)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(3,-2)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-2,1)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(0,0), Coordinate2DT(0.5,1)));

	Homography2DSolverC::error_type errorMetric;
	Homography2DSolverC::loss_type lossMap(sqrt(5.99), sqrt(5.99));
	double loss2=Homography2DSolverC::EvaluateModel(mH1, corr2, errorMetric, lossMap);
	BOOST_CHECK_SMALL(loss2, 1e-6);

	double loss1=Homography2DSolverC::EvaluateModel(mH1, corr1, errorMetric, lossMap);
	BOOST_CHECK_CLOSE(loss1, 13.1713, 1e-3);

	double error2=Homography2DSolverC::EvaluateModel(mH1, corr2, errorMetric);
	BOOST_CHECK_SMALL(error2, 1e-6);

	double error1=Homography2DSolverC::EvaluateModel(mH1, corr1, errorMetric);
	BOOST_CHECK_CLOSE(error1, 8.91507, 1e-3);
}	//BOOST_AUTO_TEST_CASE(GeometrySolverBase)

BOOST_AUTO_TEST_CASE(Homography2D)
{
	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef Homography2DSolverC::real_type RealT;

	//Identity

	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(1,1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(1,-1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-1,0)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0,0), Coordinate2DT(0,0)));

	typedef Homography2DSolverC::model_type ModelT;
	Homography2DSolverC solver;

	ModelT mH1=*(solver(corr1).begin());
	ModelT mI; mI.setIdentity(); mI.normalize();
	BOOST_CHECK(mI.isApprox(mH1, 1e-6));

	//Scale and shift
	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(3,4)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(3,-2)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-2,1)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(0,0), Coordinate2DT(0.5,1)));
	ModelT mH2=*(solver(corr2).begin());

	ModelT mH2r; mH2r<<2.5, 0, 0.5, 0, 3, 1, 0, 0, 1; mH2r.normalize();

	BOOST_CHECK(mH2r.isApprox(mH2, 1e-6));

	//Nonminimal
	corr2.push_back(BimapT::value_type(Coordinate2DT(2,1), Coordinate2DT(5.5, 4)));
	corr2.push_back(BimapT::value_type(Coordinate2DT(0,1), Coordinate2DT(0.5, 4)));
	ModelT mH3=*(solver(corr2).begin());
	BOOST_CHECK(mH2r.isApprox(mH3, 1e-6));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(1,1)));
	corr3.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(1,-1)));
	corr3.push_back(BimapT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-1,0)));
	BOOST_CHECK(solver(corr3).empty());

	//Degenerate
	//Degenerate sets are allowed
	//corr3.push_back(BimapT::value_type(Coordinate2DT(1,0), Coordinate2DT(1,0)));
	//BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(Homography2DSolverC::MakeModelFromMinimal(Homography2DSolverC::MakeMinimal(mH2r)).isApprox(mH2r, 1e-6));

	//Sample statistics

	ModelT mH6r; mH6r<<1.5, 0, -0.5, 0, -2, 0.5, 0, 0, 1;

	typedef Homography2DSolverC::minimal_model_type MinimalT;

	MinimalT min2=Homography2DSolverC::MakeMinimal(mH2r);
	MinimalT min6=Homography2DSolverC::MakeMinimal(mH6r);

	//Homography2DMinimalDifferenceC
	Homography2DMinimalDifferenceC subtractor;
	Homography2DMinimalDifferenceC::result_type min6m2=subtractor(min6, min2);

	Homography2DMinimalDifferenceC::result_type min6m2r; min6m2r<< -0.0494576,    2.77704,   0.323515,   0.632973,  -0.684802,  0.0034621,  0.0315213,  -0.100144;
	BOOST_CHECK(min6m2.isApprox(min6m2r,1e-3));

	//ComputeSampleStatistics

	vector<Homography2DT> sampleSet(8);
	sampleSet[0]<<1, 0, 2, 0, 1, 0, 0, 0, 3;
	sampleSet[1]<<2, 1, 2, 0, 1, 0, 6, 0, 3;
	sampleSet[2]<<1, 0, 2, 9, 1, 2, 1, 0, 3;
	sampleSet[3]<<1, 0, 3, 4, 1, 0, 0, 0, 3;
	sampleSet[4]<<1, 0, 2, 0, 1, 4, 0, 0, 3;
	sampleSet[5]<<1, 6, 2, 3, 1, 0, 2, 0, 3;
	sampleSet[6]<<1, 0, 2, 1, 1, 0, 8, 4, 3;
	sampleSet[7]<<1, 9, 2, 3, 1, 2, 0, 5, 3;

	Homography2DSolverC::uncertainty_type sampleStatistics=Homography2DSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(8, 1.0/8), vector<RealT>(8, 1.0/8));

	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min6), 1492.400618, 1e-4);
}	//BOOST_AUTO_TEST_CASE(Homography2D)

BOOST_AUTO_TEST_CASE(Fundamental)
{
	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef Fundamental7SolverC::real_type RealT;

	//Correspondences
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(2,1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(2,3), Coordinate2DT(2.5,3)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(0,-1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0.75,2.25), Coordinate2DT(1,2.25)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0.4,-1.2), Coordinate2DT(0.6,-1.2)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(11.0/6,-0.5), Coordinate2DT(2,-0.5)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,4.0/7), Coordinate2DT(8.0/7,4.0/7)));

	//Estimate

	typedef Fundamental7SolverC::model_type ModelT;
	Fundamental7SolverC solver;
	list<ModelT> mFList1=solver(corr1);

	BOOST_CHECK(!mFList1.empty());

	//Check conformance to the constraints
	for(const auto& current : mFList1)
	{
		typename Fundamental7SolverC::error_type errorMetric1;
		BOOST_CHECK_SMALL(solver.EvaluateModel(current, corr1, errorMetric1), 1e-6);
		BOOST_CHECK_SMALL(current.determinant(), 1e-6);
	}	//for(const auto& current : mFList1)

	//Nonminimal
	BimapT corr1nm(corr1);
	corr1nm.push_back(BimapT::value_type(Coordinate2DT(0.6, 1.3), Coordinate2DT(0.7, 1.3)));
	corr1nm.push_back(BimapT::value_type(Coordinate2DT(0.9, 1.3), Coordinate2DT(1, 1.3)));
	list<ModelT> mFList1nm=solver(corr1nm);

	//A nonminimal generator is not guaranteed to satisfy the epipolar constraint at every element
	//Test: look for the ground truth in the output
	bool flag1nm=false;
	EpipolarMatrixT mF1r; mF1r<<0, 0, 0, 0, 0, -0.707107, 0, 0.707107, 0;
	for(const auto& current : mFList1nm)
	{
		flag1nm|=Fundamental7SolverC::ComputeDistance(current, mF1r)<1e-4;
		BOOST_CHECK_SMALL(current.determinant(), 1e-6);
	}

	BOOST_CHECK(flag1nm);

	//Less-than-minimal
	BimapT corr2;
	BOOST_CHECK(solver(corr2).empty());

	//Minimal representation
	EpipolarMatrixT mF1; mF1<< -0.354799, -0.0724096, 0.43103, 0.104484, 0.0502294, -0.155242, 0.539492, 0.0469365, -0.593543;
	unsigned int rank1;
	tie(rank1, mF1)=MakeRankN<Eigen::ColPivHouseholderQRPreconditioner>(mF1,2);

	BOOST_CHECK(Fundamental7SolverC::MakeModelFromMinimal(Fundamental7SolverC::MakeMinimal(mF1)).isApprox(mF1, 1e-6));

	//Sample statistics

	//FundamentalMinimalDifference
	EpipolarMatrixT mF2; mF2<<0, 0, 0, 0, 0, -1, 0, 0, 1;

	typename Fundamental7SolverC::minimal_model_type min1=Fundamental7SolverC::MakeMinimal(mF1);
	typename Fundamental7SolverC::minimal_model_type min2=Fundamental7SolverC::MakeMinimal(mF2);
	FundamentalMinimalDifferenceC subtractor;
	FundamentalMinimalDifferenceC::result_type min2m1=subtractor(min2, min1);
	FundamentalMinimalDifferenceC::result_type min2m1r; min2m1r<< 1.79606,  0.495457,  0.838897, -0.134959, -0.720934,  0.609694, 0.0574391;
	BOOST_CHECK(min2m1r.isApprox(min2m1, 1e-3));

	//ComputeSampleStatistics
	vector<EpipolarMatrixT> sampleSet(7);
	sampleSet[0]<<1, 0, 2, 0, 1, 0, 0, 0, 3;
	sampleSet[1]<<2, 1, 2, 0, 1, 0, 6, 0, 3;
	sampleSet[2]<<1, 0, 2, 9, 1, 2, 1, 0, 3;
	sampleSet[3]<<1, 0, 3, 4, 1, 0, 0, 0, 3;
	sampleSet[4]<<1, 0, 2, 0, 1, 4, 0, 0, 3;
	sampleSet[5]<<1, 6, 2, 3, 1, 0, 2, 0, 3;
	sampleSet[6]<<1, 0, 2, 1, 1, 0, 8, 4, 3;

	for(auto& current : sampleSet)
	{
		unsigned int rank;
		tie(rank, current)=MakeRankN<Eigen::ColPivHouseholderQRPreconditioner>(current,2);
	}	//for(auto& current : sampleSet)

	Fundamental7SolverC::uncertainty_type sampleStatistics=Fundamental7SolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(7, 1.0/7), vector<RealT>(7, 1.0/7));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min2), 115.9390962888244, 1e-3);

	//Fundamental8

	BimapT corr3(corr1);
	corr3.push_back(BimapT::value_type(Coordinate2DT(0.6, 1.3), Coordinate2DT(0.7, 1.3)));

	Fundamental8SolverC solver8;
	list<ModelT> mFList3=solver8(corr3);

	BOOST_CHECK(!mFList3.empty());

	//Check conformance to the constraints
	BOOST_CHECK_SMALL(solver8.EvaluateModel(*mFList3.begin(), corr3, Fundamental8SolverC::error_type()), 1e-6);
	BOOST_CHECK_SMALL(mFList3.begin()->determinant(), 1e-6);

	//Nonminimal
	corr3.push_back(BimapT::value_type(Coordinate2DT(0.9, 1.3), Coordinate2DT(1, 1.3)));
	list<ModelT> mFList4=solver8(corr3);
	BOOST_CHECK_SMALL(solver8.EvaluateModel(*mFList4.begin(), corr3, Fundamental8SolverC::error_type()), 1e-6);
	BOOST_CHECK_SMALL(mFList4.begin()->determinant(), 1e-6);

	//Less-than-minimal
	BOOST_CHECK(solver8(corr2).empty());

}	//BOOST_AUTO_TEST_CASE(Fundamental)

BOOST_AUTO_TEST_CASE(Essential_Solver)
{
	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef EssentialSolverC::real_type RealT;

	//Correspondences
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(2,1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(2,3), Coordinate2DT(2.5,3)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(0,-1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0.75,2.25), Coordinate2DT(1,2.25)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0.4,-1.2), Coordinate2DT(0.6,-1.2)));

	//Estimate

	typedef EssentialSolverC::model_type ModelT;
	EssentialSolverC solver;
	list<ModelT> mEList1=solver(corr1);

	BOOST_CHECK(!mEList1.empty());

	//Check conformance to the constraints
	for(const auto& current : mEList1)
	{
		typename EssentialSolverC::error_type errorMetric1;
		BOOST_CHECK_SMALL(solver.EvaluateModel(current, corr1, errorMetric1), 1e-6);
		BOOST_CHECK_SMALL(current.determinant(), 1e-6);
		BOOST_CHECK(solver.VerifyTraceConstraint(current, 1e-6));
	}	//for(const auto& current : mEList1)

	//Nonminimal
	BimapT corr1nm(corr1);
	corr1nm.push_back(BimapT::value_type(Coordinate2DT(0.6, 1.3), Coordinate2DT(0.7, 1.3)));
	corr1nm.push_back(BimapT::value_type(Coordinate2DT(0.9, 1.3), Coordinate2DT(1, 1.3)));
	list<ModelT> mEList1nm=solver(corr1nm);

	//Check conformance to the constraints- epipolar constraint may not be satisfied for all correspondences in the nonminimal case
	for(const auto& current : mEList1)
	{
		BOOST_CHECK_SMALL(current.determinant(), 1e-6);
		BOOST_CHECK(solver.VerifyTraceConstraint(current, 1e-6));
	}	//for(const auto& current : mEList1)

	//Minimal representation

	BimapT corr1v;
	corr1v.push_back(BimapT::value_type(Coordinate2DT(11.0/6,-0.5), Coordinate2DT(2,-0.5)));
	corr1v.push_back(BimapT::value_type(Coordinate2DT(1,4.0/7), Coordinate2DT(8.0/7,4.0/7)));

	EpipolarMatrixT mE1; mE1<<-3.03691e-15, -5.42745e-16, 1.66324e-15, 2.98715e-15, -5.84059e-16, 0.707107, 3.67071e-15, -0.707107, -1.12391e-15;
	unsigned int rank1;
	tie(rank1, mE1)=MakeRankN<Eigen::ColPivHouseholderQRPreconditioner>(mE1,2);
	mE1.normalize();
	BOOST_CHECK_SMALL(EssentialSolverC::ComputeDistance(EssentialSolverC::MakeModelFromMinimal(EssentialSolverC::MakeMinimal(mE1, corr1v)), mE1), 1e-6);

	//Sample statistics

	//EssentialMinimalDifferenceC
	EpipolarMatrixT mE2; mE2<<0.246969, -0.507835, -0.334027, 0.358181, -0.0745203, -0.0938006, 0.551033, 0.330513, 0.11838;
	unsigned int rank2;
	tie(rank2, mE2)=MakeRankN<Eigen::ColPivHouseholderQRPreconditioner>(mE2,2);
	mE2.normalize();

	typename EssentialSolverC::minimal_model_type min1=EssentialSolverC::MakeMinimal(mE1, corr1v);
	typename EssentialSolverC::minimal_model_type min2=EssentialSolverC::MakeMinimal(mE2, corr1v);
	EssentialMinimalDifferenceC subtractor;
	EssentialMinimalDifferenceC::result_type min2m1=subtractor(min2, min1);
	EssentialMinimalDifferenceC::result_type min2m1r; min2m1r<<-0.505339, -0.459333, 0.0596583,   4.29695, -0.392377;
	BOOST_CHECK(min2m1r.isApprox(min2m1, 1e-3));

	//ComputeSampleStatistics
	vector<EpipolarMatrixT> sampleSet(6);
	sampleSet[0]<<-3.03691e-15, -5.42745e-16, 1.66324e-15, 2.98715e-15, -5.84059e-16, 0.707107, 3.67071e-15, -0.707107, -1.12391e-15;
	sampleSet[1]<< 0.246969, -0.507835, -0.334027, 0.358181, -0.0745203, -0.0938006, 0.551033, 0.330513, 0.11838;
	sampleSet[2]<< -0.440858, 0.198269, -0.0598617, 0.471172, -0.200021,  0.0344343, -0.15622, -0.203155, 0.658693;
	sampleSet[3]<<-0.251225, -0.166689, -0.123617, 0.551045, -0.167243, -0.398548, 0.334019, 0.389035, 0.374756;
	sampleSet[4]<<0.0991249, -0.233073, -0.100646, 0.133957, -0.0157385, 0.689454, 0.289701, -0.588817, -0.0393684;
	sampleSet[5]<<0.317383, -0.444315, -0.4466, 0.327121, -0.0948599, 0.380512, 0.345335, -0.134101, 0.323057;

	for(auto& current : sampleSet)
	{
		unsigned int rank;
		tie(rank, current)=MakeRankN<Eigen::ColPivHouseholderQRPreconditioner>(current,2);
		current.normalize();
	}	//for(auto& current : sampleSet)

	EssentialSolverC::uncertainty_type sampleStatistics=EssentialSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(6, 1.0/6), vector<RealT>(6, 1.0/6), corr1v);
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min2), 5.73773, 1e-3);
}	//BOOST_AUTO_TEST_CASE(Essential_Solver)

BOOST_AUTO_TEST_CASE(OS_Fundamental_Solver)
{
	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef OneSidedFundamentalSolverC::real_type RealT;

	//Correspondences
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,1), Coordinate2DT(2,1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(2,3), Coordinate2DT(2.5,3)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(1,-1), Coordinate2DT(0,-1)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0.75,2.25), Coordinate2DT(1,2.25)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(0.4,-1.2), Coordinate2DT(0.6,-1.2)));
	corr1.push_back(BimapT::value_type(Coordinate2DT(11.0/6,-0.5), Coordinate2DT(2,-0.5)));

	//Estimate

	typedef OneSidedFundamentalSolverC::model_type ModelT;
	OneSidedFundamentalSolverC solver;
	list<ModelT> mFList1=solver(corr1);

	BOOST_CHECK_EQUAL(mFList1.size(), 4);

	//Check conformance to the constraints
	vector<double> algebraicError1{2.41817, 2.30064, 1.41757, 3.44036};
	vector<double> focalLength1{0.28842, 1.88605, 0.88247, 1.02913};
	size_t index1=0;
	for(const auto& current : mFList1)
	{
		typename OneSidedFundamentalSolverC::error_type errorMetric1;
		BOOST_CHECK_CLOSE(solver.EvaluateModel(current, corr1, errorMetric1), algebraicError1[index1], 1e-3);
		BOOST_CHECK_SMALL(current.determinant(), 1e-6);

		optional<tuple<EpipolarMatrixT, double> > decomposed=OneSidedFundamentalSolverC::DecomposeOneSidedF(current);
		BOOST_CHECK(decomposed);

		EpipolarMatrixT mE;
		double f;
		std::tie(mE, f)=*decomposed;

		BOOST_CHECK(EssentialSolverC::VerifyTraceConstraint(mE, 1e-4));
		BOOST_CHECK_CLOSE(f, focalLength1[index1], 1e-3);

		++index1;
	}	//for(const auto& current : mFList1)

	//Nonminimal

	BimapT corr1nm(corr1);
	corr1nm.push_back(BimapT::value_type(Coordinate2DT(0.6, 1.3), Coordinate2DT(0.7, 1.3)));
	corr1nm.push_back(BimapT::value_type(Coordinate2DT(0.9, 1.3), Coordinate2DT(1, 1.3)));
	list<ModelT> mFList1nm=solver(corr1nm);
	BOOST_CHECK_EQUAL(mFList1nm.size(), 1);

	//Even the minimal case does not satisfy the epipolar constraint!
	for(const auto& current : mFList1nm)
	{
		optional<tuple<EpipolarMatrixT, double> > decomposed=OneSidedFundamentalSolverC::DecomposeOneSidedF(current);

		EpipolarMatrixT mE;
		double f;
		std::tie(mE, f)=*decomposed;

		BOOST_CHECK_SMALL(mE.determinant(), 1e-6);
		BOOST_CHECK(EssentialSolverC::VerifyTraceConstraint(mE, 1e-6));
		BOOST_CHECK_CLOSE(f, 0.972389, 1e-5);
	}	//for(const auto& current : mEList1)

	BOOST_CHECK(isinf(OneSidedFundamentalSolverC::Cost()));

	//Minimal representation
	EpipolarMatrixT mF1=*mFList1.begin();
	BOOST_CHECK_SMALL(OneSidedFundamentalSolverC::ComputeDistance(OneSidedFundamentalSolverC::MakeModelFromMinimal(OneSidedFundamentalSolverC::MakeMinimal(mF1, corr1)), mF1), 1e-6);

	//Sample statistics

	//OneSidedFundamentalMinimalDifferenceC
	EpipolarMatrixT mF2=*mFList1.rbegin();

	typename OneSidedFundamentalSolverC::minimal_model_type min1=OneSidedFundamentalSolverC::MakeMinimal(mF1, corr1);
	typename OneSidedFundamentalSolverC::minimal_model_type min2=OneSidedFundamentalSolverC::MakeMinimal(mF2, corr1);
	OneSidedFundamentalMinimalDifferenceC subtractor;
	OneSidedFundamentalMinimalDifferenceC::result_type min2m1=subtractor(min2, min1);
	OneSidedFundamentalMinimalDifferenceC::result_type min2m1r; min2m1r<<-0.163773,   1.29132,   1.18292,   2.82239, -0.334615,  0.740705;
	BOOST_CHECK(min2m1r.isApprox(min2m1, 5e-2));

	//ComputeSampleStatistics

	vector<EpipolarMatrixT> sampleSet; sampleSet.reserve(6);
	for(const auto& current : mFList1)
		if(OneSidedFundamentalSolverC::DecomposeOneSidedF(current))
			sampleSet.push_back(current);

	sampleSet.push_back(*mFList1nm.begin());

	EpipolarMatrixT mF1gt; mF1gt<<-3.03691e-15, -5.42745e-16, 1.66324e-15, 2.98715e-15, -5.84059e-16, 0.707107, 3.67071e-15, -0.707107, -1.12391e-15;
	sampleSet.push_back(mF1gt);
	size_t nSample=sampleSet.size();

	OneSidedFundamentalSolverC::uncertainty_type sampleStatistics=OneSidedFundamentalSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(nSample, 1.0/nSample), vector<RealT>(nSample, 1.0/nSample), corr1);
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min2), 6, 1e-3);

}	//BOOST_AUTO_TEST_CASE(OS_Fundamental_Solver)

BOOST_AUTO_TEST_CASE(Rotation3D)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based> BimapT;
	typedef Rotation3DSolverC::real_type RealT;

	//Identity
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,1,0), Coordinate3DT(1,1,0)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2)));

	typedef Rotation3DSolverC::model_type ModelT;
	Rotation3DSolverC solver;

	list<ModelT> lR1=solver(corr1);
	BOOST_CHECK(!lR1.empty());

	ModelT mR1=*lR1.begin();
	ModelT mI; mI.setIdentity();
	BOOST_CHECK(mI.isApprox(mR1, 1e-6));

	//Rotation
	ModelT mR2r; mR2r<<  -0.57637, 0.60179, 0.55286, 0, -0.15054, 0.58677, -0.79564, 0, -0.80320, -0.54181, -0.24760, 0, 0, 0, 0, 1;
	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,0), mR2r.block(0,0,3,3)*Coordinate3DT(1,1,0)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), mR2r.block(0,0,3,3)*Coordinate3DT(1,-1,2)));

	list<ModelT> lR2=solver(corr2);
	BOOST_CHECK(!lR2.empty());

	ModelT mR2=*lR2.begin();
	BOOST_CHECK(mR2.isApprox(mR2r, 1e-3));

	//Nonminimal
	corr2.push_back(BimapT::value_type(Coordinate3DT(-1,0,1), mR2r.block(0,0,3,3)*Coordinate3DT(-1,0,1)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(0,0,3), mR2r.block(0,0,3,3)*Coordinate3DT(0,0,3)));

	list<ModelT> lR3=solver(corr2);
	BOOST_CHECK(!lR3.empty());

	ModelT mR3=*lR3.begin();
	BOOST_CHECK(mR3.isApprox(mR2r, 1e-3));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,0), Coordinate3DT(1,1,0)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(Rotation3DSolverC::MakeModelFromMinimal(Rotation3DSolverC::MakeMinimal(mR2r)).isApprox(mR2r, 1e-3));

	//Sample statistics
	ModelT mR4r; mR4r<<-3.6297e-04, 5.9678e-01, 8.0241e-01, 0,-2.6934e-04, -8.0241e-01, 5.9678e-01, 0, 1-1.0215e-07, 4.9158e-07, 4.5199e-04, 0, 0, 0, 0, 1;
	typedef Rotation3DSolverC::minimal_model_type MinimalT;
	MinimalT min2=Rotation3DSolverC::MakeMinimal(mR2r);
	MinimalT min4=Rotation3DSolverC::MakeMinimal(mR4r);

	//Rotation3DMinimalDifferenceC
	Rotation3DMinimalDifferenceC subtractor;
	Rotation3DMinimalDifferenceC::result_type min4m2=subtractor(min4, min2);

	Rotation3DMinimalDifferenceC::result_type min4m2r; min4m2r<< -2.75955, 0.342619, 0.852944;
	BOOST_CHECK(min4m2.isApprox(min4m2r,1e-3));

	Rotation3DMinimalDifferenceC::result_type min2m2=subtractor(min2, min2);
	BOOST_CHECK_SMALL(min2m2.norm(), 1e-6);

	//ComputeSampleStatistics
	vector<Homography3DT> sampleSet(4);
	sampleSet[0]<<  -0.976192, -0.031164, -0.214660, 0, -0.145397, -0.640368, 0.754181, 0, -0.160964, 0.767436, 0.620591, 0, 0, 0, 0, 1;
	sampleSet[1]=mR2r;
	sampleSet[2]=mR4r;
	sampleSet[3]=mI;

	Rotation3DSolverC::uncertainty_type sampleStatistics=Rotation3DSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(4, 1.0/4), vector<RealT>(4, 1.0/4));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min4), 2.33163, 1e-4);

	//Overridden functions

	Homography3DT mR5=Rotation3DSolverC::MakeModel(Rotation3DSolverC::MakeVector(mR2r));
	BOOST_CHECK(mR5.isApprox(mR2r, 1e-6));

	BOOST_CHECK_SMALL(Rotation3DSolverC::ComputeDistance(mR2r, mR2r), 1e-6);
	BOOST_CHECK_CLOSE(Rotation3DSolverC::ComputeDistance(mR2r, mR4r), 0.462919, 1e-4);
}	//BOOST_AUTO_TEST_CASE(Rotation3D)

BOOST_AUTO_TEST_CASE(Similarity3D)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based> BimapT;
	typedef Homography3DSolverC::real_type RealT;

	//Identity

	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,1,0), Coordinate3DT(1,1,0)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(-1,0,1), Coordinate3DT(-1,0,1)));

	typedef Similarity3DSolverC::model_type ModelT;
	Similarity3DSolverC solver;

	list<ModelT> lH1=solver(corr1);
	BOOST_CHECK(!lH1.empty());

	ModelT mH1=*lH1.begin();
	ModelT mI; mI.setIdentity();
	BOOST_CHECK(mI.isApprox(mH1, 1e-3));

	//Transformation

	RotationMatrix3DT mR2r; mR2r<<  -0.57637, 0.60179, 0.55286, -0.15054, 0.58677, -0.79564, -0.80320, -0.54181, -0.24760;
	Homography3DT mH2r=ComposeSimilarity3D(2, Coordinate3DT(1, 2, 3), mR2r);
	CoordinateTransformations3DT::affine_transform_type mA; mA.matrix()=mH2r;

	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,0), CoordinateTransformations3DT::TransformCoordinate(Coordinate3DT(1,1,0), mA)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), CoordinateTransformations3DT::TransformCoordinate(Coordinate3DT(1,-1,2),mA)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(-1,0,1), CoordinateTransformations3DT::TransformCoordinate(Coordinate3DT(-1,0,1),mA)));

	list<ModelT> lH2=solver(corr2);
	BOOST_CHECK(!lH2.empty());
	BOOST_CHECK(lH2.begin()->isApprox(mH2r, 1e-3));

	//Non-minimal
	corr2.push_back(BimapT::value_type(Coordinate3DT(0,0,3), CoordinateTransformations3DT::TransformCoordinate(Coordinate3DT(0,0,3),mA)));
	list<ModelT> lH3=solver(corr2);
	BOOST_CHECK(!lH3.empty());
	BOOST_CHECK(lH3.begin()->isApprox(mH2r, 1e-3));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,0), Coordinate3DT(1,1,0)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(Similarity3DSolverC::MakeModelFromMinimal(Similarity3DSolverC::MakeMinimal(mH2r)).isApprox(mH2r, 1e-3));

	//Sample statistics
	ModelT mH4r=mH2r.inverse();
	typedef Similarity3DSolverC::minimal_model_type MinimalT;
	MinimalT min2=Similarity3DSolverC::MakeMinimal(mH2r);
	MinimalT min4=Similarity3DSolverC::MakeMinimal(mH4r);

	//Similarity3DMinimalDifferenceC
	Similarity3DMinimalDifferenceC subtractor;
	Similarity3DMinimalDifferenceC::result_type min4m2=subtractor(min4, min2);

	Similarity3DMinimalDifferenceC::result_type min4m2r; min4m2r<<  -1.50001, 0.291992, 1.55994, -0.865438, -5.57158, 0.72784, 2.25924;
	BOOST_CHECK(min4m2.isApprox(min4m2r,1e-3));

	Similarity3DMinimalDifferenceC::result_type min2m2=subtractor(min2, min2);
	BOOST_CHECK_SMALL(min2m2.norm(), 1e-6);

	//ComputeSampleStatistics
	vector<Homography3DT> sampleSet(9);
	sampleSet[0]=mH2r;
	sampleSet[1]=mH4r;
	sampleSet[2]=ComposeSimilarity3D(1.5, Coordinate3DT(0,2,1), QuaternionT(1,0,0,0).matrix());
	sampleSet[3]=ComposeSimilarity3D(0.5, Coordinate3DT(2,3,1), QuaternionT(0.5,0.5,0.5,0.5).matrix());
	sampleSet[4]=ComposeSimilarity3D(0.25, Coordinate3DT(1,2,4), QuaternionT(0.6,0,0.8,0).matrix());
	sampleSet[5]=ComposeSimilarity3D(2.5, Coordinate3DT(1,6,5), QuaternionT(0.6,0.8,0,0).matrix());
	sampleSet[6]=ComposeSimilarity3D(3, Coordinate3DT(0,7,3), QuaternionT(0.8,0,0,0.6).matrix());
	sampleSet[7]=ComposeSimilarity3D(1, Coordinate3DT(7,3,4), QuaternionT(0,1,0,0).matrix());
	sampleSet[8]=ComposeSimilarity3D(1, Coordinate3DT(7,3,4), QuaternionT(1,2,4,1).normalized().matrix());

	Similarity3DSolverC::uncertainty_type sampleStatistics=Similarity3DSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(9, 1.0/9), vector<RealT>(9, 1.0/9));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min4), 7.93290, 1e-4);

	//Overrides
	Homography3DT mH5=Similarity3DSolverC::MakeModel(Similarity3DSolverC::MakeVector(mH2r));
	BOOST_CHECK(mH5.isApprox(mH2r, 1e-6));

	BOOST_CHECK_SMALL(Similarity3DSolverC::ComputeDistance(mH2r, mH2r), 1e-6);
	BOOST_CHECK_CLOSE(Similarity3DSolverC::ComputeDistance(mH2r, mH4r), 0.4571396, 1e-4);
}	//BOOST_AUTO_TEST_CASE(Similarity3D)

BOOST_AUTO_TEST_CASE(Homography3D)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based> BimapT;
	typedef Homography3DSolverC::real_type RealT;

	//Identity

	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,1,0), Coordinate3DT(1,1,0)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(-1,0,1), Coordinate3DT(-1,0,1)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(0,0,3), Coordinate3DT(0,0,3)));
	corr1.push_back(BimapT::value_type(Coordinate3DT(3,1,3), Coordinate3DT(3,1,3)));

	typedef Homography3DSolverC::model_type ModelT;
	Homography3DSolverC solver;

	list<ModelT> lH1=solver(corr1);
	BOOST_CHECK(!lH1.empty());

	ModelT mH1=*lH1.begin();
	ModelT mI; mI.setIdentity(); mI.normalize();
	BOOST_CHECK(mI.isApprox(mH1, 1e-6));

	//Scale and shift
	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,0), Coordinate3DT(3,2,1)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(3,-2,3)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(-1,0,1), Coordinate3DT(-3,0,2)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(0,0,3), Coordinate3DT(0,0,4)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(3,1,3), Coordinate3DT(9,2,4)));

	ModelT mH2=*(solver(corr2).begin());
	ModelT mH2r=ModelT::Zero(); mH2r(0,0)=0.75; mH2r(1,1)=0.5; mH2r(2,2)=0.25; mH2r(2,3)=0.25; mH2r(3,3)=0.25;
	BOOST_CHECK(mH2r.isApprox(mH2, 1e-6));

	//Nonminimal
	corr2.push_back(BimapT::value_type(Coordinate3DT(10,4,9), Coordinate3DT(30,8,10)));
	corr2.push_back(BimapT::value_type(Coordinate3DT(7,5,12), Coordinate3DT(21,10,13)));
	ModelT mH3=*(solver(corr2).begin());
	BOOST_CHECK(mH2r.isApprox(mH3, 1e-6));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,1), Coordinate3DT(1,1,1)));
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,-1,1), Coordinate3DT(1,-1,1)));
	corr3.push_back(BimapT::value_type(Coordinate3DT(-1,0,1), Coordinate3DT(-1,0,1)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	ModelT mHmin=Homography3DSolverC::MakeModelFromMinimal(Homography3DSolverC::MakeMinimal(mH2r));
	BOOST_CHECK(mHmin.isApprox(mH2r, 1e-6));

	//Sample statistics
	ModelT mH4r=ModelT::Zero(); mH4r(0,0)=0.25; mH4r(1,1)=0.25; mH4r(2,2)=0.75; mH4r(2,3)=0.5; mH4r(3,3)=0.25;
	typedef Homography3DSolverC::minimal_model_type MinimalT;
	MinimalT min2=Homography3DSolverC::MakeMinimal(mH2r);
	MinimalT min4=Homography3DSolverC::MakeMinimal(mH4r);

	//Homography3DMinimalDifferenceC
	Homography3DMinimalDifferenceC subtractor;
	Homography3DMinimalDifferenceC::result_type min4m2=subtractor(min4, min2);

	Homography3DMinimalDifferenceC::result_type min4m2r; min4m2r<< 0.399564, -0, -0, -0, -0 , 0.137415, -0, -0, -0, -0, -0.898499, -0.511616, -0, -0, -0;
	BOOST_CHECK(min4m2.isApprox(min4m2r,1e-3));

	Homography3DMinimalDifferenceC::result_type min2m2=subtractor(min2, min2);
	BOOST_CHECK_SMALL(min2m2.norm(), 1e-6);

	//ComputeSampleStatistics
	vector<Homography3DT> sampleSet(17);
	sampleSet[0]<<9, 0, 3, 0, 0, 5, 0, 1, 0, 0, 2, 0, 7, 1, 0, 10;
	sampleSet[1]<<1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1;
	sampleSet[2]<<1, 0, 2, 0, 0, 1, 0, 4, 0, 0, 1, 0, 0, 0, 0, 1;
	sampleSet[3]<<1, 0, 0, 0, 3, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1;
	sampleSet[4]<<1, 0, 6, 0, 0, 1, 0, 0, 0, 11, 1, 0, 0, 0, 5, 1;
	sampleSet[5]<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 3;
	sampleSet[6]<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
	sampleSet[7]<<4, 0, 0, 0, 0, 1, 0, 0, 0, 8, 1, 0, 0, 0, 0, 6;
	sampleSet[8]<<12, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 0, 0, 4, 0, 1;
	sampleSet[9]<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 11;
	sampleSet[10]<<1, 0, 0, 0, 0, 1, 0, 0, 5, 0, 1, 0, 0, 0, 0, 1;
	sampleSet[11]<<9, 0, 0, 0, 0, 1, 0, 0, 7, 0, 12, 0, 0, 0, 5, 5;
	sampleSet[12]<<1, 6, 0, 0, 0, 1, 0, 0, 0, 0, 1, 3, 0, 0, 0, 2;
	sampleSet[13]<<1, 0, 8, 0, 0, 1, 0, 0, 0, 2, 1, 0, 1, 0, 0, 4;
	sampleSet[14]<<1, 0, 0, 13, 0, 1, 0, 0, 6, 0, 1, 0, 4, 0, 0, 1;
	sampleSet[15]<<1, 0, 0, 0, 7, 1, 0, 11, 0, 0, 1, 0, 0, 2, 0, 3;
	sampleSet[16]<<1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 6;

	Homography3DSolverC::uncertainty_type sampleStatistics=Homography3DSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(17, 1.0/17), vector<RealT>(17, 1.0/17));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min4), 504.62357, 1e-4);

	//MakeModel
	Homography3DT mH5=Homography3DSolverC::MakeModel(Homography3DSolverC::MakeVector(mH2r));
	BOOST_CHECK(mH5==mH2r);
}	//BOOST_AUTO_TEST_CASE(Homography3D)

BOOST_AUTO_TEST_CASE(P6P)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef Homography3DSolverC::real_type RealT;

	RotationMatrix3DT mR; mR<<  -0.57637, 0.60179, 0.55286, -0.15054, 0.58677, -0.79564, -0.80320, -0.54181, -0.24760;
	Coordinate3DT vC; vC<<1, 1.5, 0.25;
	IntrinsicCalibrationMatrixT mK; mK<<1500, 0, 960, 0, 1490, 540, 0, 0, 1;

	CameraMatrixT mP=ComposeCameraMatrix(mK, vC, mR);
	mP/=mP(2,3);

	//Minimal solver

	Projection32C projector(mP);

	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,1,0), projector(Coordinate3DT(1,1,0))));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), projector(Coordinate3DT(1,-1,2))));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,3,5), projector(Coordinate3DT(1,3,5))));
	corr1.push_back(BimapT::value_type(Coordinate3DT(0,6,3), projector(Coordinate3DT(0,6,3))));
	corr1.push_back(BimapT::value_type(Coordinate3DT(4,-1,7), projector(Coordinate3DT(4,-1,7))));
	corr1.push_back(BimapT::value_type(Coordinate3DT(3,6,2), projector(Coordinate3DT(3,6,2))));

	typedef P6PSolverC::model_type ModelT;
	P6PSolverC solver;

	list<ModelT> lP1=solver(corr1);
	BOOST_CHECK(lP1.size()==1);
	BOOST_CHECK(mP.isApprox( *lP1.begin()/(*lP1.begin())(2,3), 1e-6));

	//Nonminimal
	corr1.push_back(BimapT::value_type(Coordinate3DT(3,4,7), projector(Coordinate3DT(3,4,7))));
	list<ModelT> lP2=solver(corr1);
	BOOST_CHECK(lP2.size()==1);
	BOOST_CHECK(mP.isApprox( *lP2.begin()/(*lP2.begin())(2,3), 1e-6));

	//Less than minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,0), projector(Coordinate3DT(1,1,0))));
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), projector(Coordinate3DT(1,-1,2))));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	ModelT mPmin=P6PSolverC::MakeModelFromMinimal(P6PSolverC::MakeMinimal(mP));
	mPmin/=mPmin(2,3);

	BOOST_CHECK(mPmin.isApprox(mP, 1e-6));

	//CameraMinimalDifferenceC

	CameraMatrixT mP4=ComposeCameraMatrix(mK, vC+Coordinate3DT(1,0,3), mR);

	CameraMinimalDifferenceC subtractor;
	CameraMinimalDifferenceC::result_type min4m1=subtractor(solver.MakeMinimal(mP4), solver.MakeMinimal(mP));

	CameraMinimalDifferenceC::result_type min4m1r; min4m1r<< 0,0,0,0,0,0,0,0,1,0,3;
	BOOST_CHECK(min4m1.isApprox(min4m1r,1e-3));

	CameraMinimalDifferenceC::result_type min4m4=subtractor(solver.MakeMinimal(mP4), solver.MakeMinimal(mP4));
	BOOST_CHECK_SMALL(min4m4.norm(), 1e-6);

	//ComputeSampleStatistics
	vector<CameraMatrixT> samples(13);
	samples[0]<< 1807.46,  861.959,   395.75,  1150.25,  -631.04,  1661.08,  538.735,   1220.4, 0.286159, 0.107638, 0.952117,  1.01683;
	samples[1]<< 1795.03,   349.833,    128.22,  -5.41912,   1.05191,   1075.42,   1260.35,  -442.336,  0.551974, -0.359985,  0.752154, -0.425275;
	samples[2]<< 1811.95,   -1118.6,   -52.434,  -1136.55,   1231.31,   1510.96,  383.243,  -1644.89,  0.421041, -0.201469,  0.884384,  0.456248;
	samples[3]<< 1385.31,   -1209.23,   -169.115,   -422.379,    1188.12,    1019.59,   -600.208,    1511.65,   0.770629, 0.00864663,   0.637226,   0.377031;
	samples[4]<< -86.0848,  -651.983,   1251.33,   457.227,  -354.223,   771.739,   785.799,   248.833, -0.768671, -0.262616,  0.583248, 0.0126235;
	samples[5]<< 1652.87,  865.755, -686.546,  804.973, -451.985,  1741.28,  373.933, -1218.83, 0.664301, 0.339044, 0.666148, 0.471692;
	samples[6]<< 1475.04, -846.089,  811.901,  1800.86,  1269.35,  988.081, -606.119, -226.006, 0.475976, 0.433896, 0.764972,   0.2989;
	samples[7]<< 1490.5, -1461.51,  86.1336, -201.824,  1180.84,  764.593, -1336.84, -1187.14, 0.847789, 0.241246, 0.472286, 0.449193;
	samples[8]<< -122.336,  -724.229,   1800.32,  -868.964,   603.763,   1404.81,   896.612,  -1533.04, -0.728901, 0.308946,  0.610947, -0.720097;
	samples[9]<<  580.806,  -319.786,   1316.92,    449.35,   477.286,   1081.52,   438.455,   1113.53, -0.308388,   0.26805,  0.912713,   1.13768;
	samples[10]<< 1009.89,    1837.6,   19.3272,  -1275.09,  -1412.89,   1090.36,  -756.226,   881.278,  -0.25714,  0.671233,  0.695217, -0.150203;
	samples[11]<< 1439.4,   1391.93,   757.936,   56.5914,   -443.29,   1506.13,  -1213.96,  -32.3795, -0.340277,   0.73641,  0.584733, -0.888496;
	samples[12]<< 1140.89,  -1004.07,   578.758,    598.47,   1158.07,   817.709,   -22.576,   1623.96,  0.421149, 0.0586546,  0.905093,    1.0745;

	P6PSolverC::uncertainty_type sampleStatistics=P6PSolverC::ComputeSampleStatistics(samples, vector<RealT>(13, 1.0/13), vector<RealT>(13, 1.0/13));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(solver.MakeMinimal(samples[0])),12.145046, 1e-4);
}	//BOOST_AUTO_TEST_CASE(P6P)

BOOST_AUTO_TEST_CASE(P2P)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef P2PSolverC::real_type RealT;

	//Identity
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,1,5), Coordinate3DT(1,1,5).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2).hnormalized()));

	typedef P2PSolverC::model_type ModelT;
	P2PSolverC solver;

	list<ModelT> lR1=solver(corr1);
	BOOST_CHECK(!lR1.empty());

	ModelT mR1=*lR1.begin();
	ModelT mI; mI.setIdentity();
	BOOST_CHECK(mI.isApprox(mR1, 1e-6));

	//Rotation
	ModelT mR2r; mR2r<<  -0.57637, 0.60179, 0.55286, 0, -0.15054, 0.58677, -0.79564, 0, -0.80320, -0.54181, -0.24760,0;
	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,-10), (mR2r.block(0,0,3,3)*Coordinate3DT(1,1,-10)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,-3), (mR2r.block(0,0,3,3)*Coordinate3DT(1,-1,-3)).hnormalized()));

	list<ModelT> lR2=solver(corr2);
	BOOST_CHECK(!lR2.empty());

	ModelT mR2=*lR2.begin();
	BOOST_CHECK(mR2.isApprox(mR2r, 1e-3));

	//Nonminimal
	corr2.push_back(BimapT::value_type(Coordinate3DT(-1,0,-4), (mR2r.block(0,0,3,3)*Coordinate3DT(-1,0,-4)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(0,0,-3), (mR2r.block(0,0,3,3)*Coordinate3DT(0,0,-3)).hnormalized()));

	list<ModelT> lR3=solver(corr2);
	BOOST_CHECK(!lR3.empty());

	ModelT mR3=*lR3.begin();
	BOOST_CHECK(mR3.isApprox(mR2r, 1e-3));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,-10), Coordinate2DT(1,1)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(P2PSolverC::MakeModelFromMinimal(P2PSolverC::MakeMinimal(mR2r)).isApprox(mR2r, 1e-3));

	//Sample statistics
	ModelT mR4r; mR4r<<-3.6297e-04, 5.9678e-01, 8.0241e-01, 0,-2.6934e-04, -8.0241e-01, 5.9678e-01, 0, 1-1.0215e-07, 4.9158e-07, 4.5199e-04, 0;
	typedef P2PSolverC::minimal_model_type MinimalT;
	MinimalT min4=P2PSolverC::MakeMinimal(mR4r);

	//ComputeSampleStatistics
	vector<ModelT> sampleSet(4);
	sampleSet[0]<<  -0.976192, -0.031164, -0.214660, 0, -0.145397, -0.640368, 0.754181, 0, -0.160964, 0.767436, 0.620591, 0;
	sampleSet[1]=mR2r;
	sampleSet[2]=mR4r;
	sampleSet[3]=mI;

	P2PSolverC::uncertainty_type sampleStatistics=P2PSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(4, 1.0/4), vector<RealT>(4, 1.0/4));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min4), 2.33163, 1e-4);

	//Overridden functions

	ModelT mR5=P2PSolverC::MakeModel(P2PSolverC::MakeVector(mR2r));
	BOOST_CHECK(mR5.isApprox(mR2r, 1e-4));

	BOOST_CHECK_SMALL(P2PSolverC::ComputeDistance(mR2r, mR2r), 1e-6);
	BOOST_CHECK_CLOSE(P2PSolverC::ComputeDistance(mR2r, mR4r), 0.462919, 1e-4);
}	//BOOST_AUTO_TEST_CASE(Rotation3D)

BOOST_AUTO_TEST_CASE(P2Pf)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef P2PfSolverC::real_type RealT;

	//Identity
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,1,5), Coordinate3DT(1,1,5).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2).hnormalized()));

	typedef P2PfSolverC::model_type ModelT;
	P2PfSolverC solver;

	list<ModelT> lR1=solver(corr1);
	BOOST_CHECK(!lR1.empty());

	ModelT mR1=*lR1.begin();
	ModelT mI; mI.setIdentity();
	BOOST_CHECK(mI.isApprox(mR1, 1e-6));

	//P2Pf
	double f2=10;
	ModelT mKR2r; mKR2r<<  -f2*0.57637, f2*0.60179, f2*0.55286, 0, -f2*0.15054, f2*0.58677, -f2*0.79564, 0, -0.80320, -0.54181, -0.24760,0;
	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,-10), (mKR2r.block(0,0,3,3)*Coordinate3DT(1,1,-10)).hnormalized() ));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,-3), (mKR2r.block(0,0,3,3)*Coordinate3DT(1,-1,-3)).hnormalized() ));

	list<ModelT> lKR2=solver(corr2);
	BOOST_CHECK(!lKR2.empty());

	ModelT mKR2=*lKR2.begin();
	BOOST_CHECK(mKR2.isApprox(mKR2r, 1e-3));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,-10), Coordinate2DT(1,1)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(P2PfSolverC::MakeModelFromMinimal(P2PfSolverC::MakeMinimal(mKR2r)).isApprox(mKR2r, 1e-3));

	//Sample statistics
	double f4=5;
	ModelT mKR4r; mKR4r<<-f4*3.6297e-04, f4*5.9678e-01, f4*8.0241e-01, 0,-f4*2.6934e-04, -f4*8.0241e-01, f4*5.9678e-01, 0, 1-1.0215e-07, 4.9158e-07, 4.5199e-04, 0;
	typedef P2PfSolverC::minimal_model_type MinimalT;
	MinimalT min4=P2PfSolverC::MakeMinimal(mKR4r);

	//ComputeSampleStatistics
	vector<ModelT> sampleSet(5);
	sampleSet[0]<<  -0.976192, -0.031164, -0.214660, 0, -0.145397, -0.640368, 0.754181, 0, -0.160964, 0.767436, 0.620591, 0;
	sampleSet[1]=mKR2r;
	sampleSet[2]=mKR4r;
	sampleSet[3]=mI;
	sampleSet[4]=mKR2r.block(0,0,3,3)*mKR4r;

	P2PfSolverC::uncertainty_type sampleStatistics=P2PfSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(5, 1.0/5), vector<RealT>(5, 1.0/5));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min4), 1.431655, 1e-4);

	//Overridden functions

	ModelT mKR5=P2PfSolverC::MakeModel(P2PfSolverC::MakeVector(mKR2r));
	BOOST_CHECK(mKR5.isApprox(mKR2r, 1e-4));

	BOOST_CHECK_SMALL(P2PfSolverC::ComputeDistance(mKR2r, mKR2r), 1e-6);
	BOOST_CHECK_CLOSE(P2PfSolverC::ComputeDistance(mKR2r, mKR4r), 0.921622, 1e-4);
}	//BOOST_AUTO_TEST_CASE(P2Pf)

BOOST_AUTO_TEST_CASE(P3PSolver)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef P3PSolverC::real_type RealT;

	//Identity
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(4,1,5), Coordinate3DT(4,1,5).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(-1,-6,4), Coordinate3DT(-1,-6,4).hnormalized()));

	typedef P3PSolverC::model_type ModelT;
	P3PSolverC solver;

	list<ModelT> lP1=solver(corr1);
	BOOST_CHECK(!lP1.empty());

	ModelT mP1=*lP1.begin();
	ModelT mI=ModelT::Zero(); mI.block(0,0,3,3).setIdentity();
	BOOST_CHECK(mI.isApprox(mP1, 1e-6));

	//Transformation

	RotationMatrix3DT mR2; mR2<<  -0.57637, 0.60179, 0.55286, -0.15054, 0.58677, -0.79564, -0.80320, -0.54181, -0.24760;
	Coordinate3DT vC2(1,2,3);
	ModelT mP2r=ComposeCameraMatrix(vC2, mR2);

	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,0), (mR2*(Coordinate3DT(1,1,0)-vC2)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), (mR2*(Coordinate3DT(1,-1,2)-vC2)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(-1,0,5), (mR2*(Coordinate3DT(-1,0,5)-vC2)).hnormalized()));

	list<ModelT> lP2=solver(corr2);
	BOOST_CHECK(!lP2.empty());

	ModelT mP2=*lP2.rbegin();
	BOOST_CHECK(mP2.isApprox(mP2r, 1e-3));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,-10), Coordinate2DT(1,1)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(P3PSolverC::MakeModelFromMinimal(P3PSolverC::MakeMinimal(mP2r)).isApprox(mP2r, 1e-3));

	//ComputeSampleStatistics
	vector<CameraMatrixT> sampleSet(7);
	sampleSet[0]=mP2r;
	sampleSet[1]=mI;
	sampleSet[2]=ComposeCameraMatrix(Coordinate3DT(0,2,1), QuaternionT(1,0,0,0).matrix());
	sampleSet[3]=ComposeCameraMatrix(Coordinate3DT(2,3,1), QuaternionT(0.5,0.5,0.5,0.5).matrix());
	sampleSet[4]=ComposeCameraMatrix(Coordinate3DT(1,2,4), QuaternionT(0.6,0,0.8,0).matrix());
	sampleSet[5]=ComposeCameraMatrix(Coordinate3DT(1,6,5), QuaternionT(0.6,0.8,0,0).matrix());
	sampleSet[6]=ComposeCameraMatrix(Coordinate3DT(0,6,9), QuaternionT(1,2,4,1).normalized().matrix());

	P3PSolverC::uncertainty_type sampleStatistics=P3PSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(7, 1.0/7), vector<RealT>(7, 1.0/7));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);

	P3PSolverC::minimal_model_type min2=P3PSolverC::MakeMinimal(mP2r);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min2), 5.953102, 1e-4);

	//Overridden functions

	ModelT mP5=P3PSolverC::MakeModel(P3PSolverC::MakeVector(mP2r));
	BOOST_CHECK(mP5.isApprox(mP2r, 1e-4));

	BOOST_CHECK_SMALL(P3PSolverC::ComputeDistance(mP2r, mP2r), 1e-6);
	BOOST_CHECK_CLOSE(P3PSolverC::ComputeDistance(mP2r, sampleSet[6]), 0.443948, 1e-4);
}	//BOOST_AUTO_TEST_CASE(P3PSolverC)

BOOST_AUTO_TEST_CASE(P4P_Solver)
{
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	typedef P4PSolverC::real_type RealT;

	//Identity
	BimapT corr1;
	corr1.push_back(BimapT::value_type(Coordinate3DT(4,1,5), Coordinate3DT(4,1,5).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), Coordinate3DT(1,-1,2).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(-1,-6,4), Coordinate3DT(-1,-6,4).hnormalized()));
	corr1.push_back(BimapT::value_type(Coordinate3DT(2,-5,8), Coordinate3DT(2,-5,8).hnormalized()));

	typedef P4PSolverC::model_type ModelT;
	P4PSolverC solver;

	list<ModelT> lP1=solver(corr1);
	BOOST_CHECK(lP1.size()==2);

	ModelT mP1=*lP1.begin();
	ModelT mI=ModelT::Zero(); mI.block(0,0,3,3).setIdentity();
	BOOST_CHECK(mI.isApprox(mP1, 1e-6));

	//Transformation
	RotationMatrix3DT mR2; mR2<<  -0.57637, 0.60179, 0.55286, -0.15054, 0.58677, -0.79564, -0.80320, -0.54181, -0.24760;
	Coordinate3DT vC2(1,2,3);
	IntrinsicCalibrationMatrixT mK2=IntrinsicCalibrationMatrixT::Identity(); mK2(0,0)=2; mK2(1,1)=mK2(0,0);
	ModelT mP2r=ComposeCameraMatrix(mK2, vC2, mR2);

	BimapT corr2;
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,1,0), mK2(0,0)*(mR2*(Coordinate3DT(1,1,0)-vC2)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(1,-1,2), mK2(0,0)*(mR2*(Coordinate3DT(1,-1,2)-vC2)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(-1,0,5), mK2(0,0)*(mR2*(Coordinate3DT(-1,0,5)-vC2)).hnormalized()));
	corr2.push_back(BimapT::value_type(Coordinate3DT(2,3,4), mK2(0,0)*(mR2*(Coordinate3DT(2,3,4)-vC2)).hnormalized()));

	list<ModelT> lP2=solver(corr2);
	BOOST_CHECK(lP2.size()==2);

	ModelT mP2=*lP2.rbegin();
	BOOST_CHECK(mP2.isApprox(mP2r, 1e-3));

	//Less-than-minimal
	BimapT corr3;
	corr3.push_back(BimapT::value_type(Coordinate3DT(1,1,-10), Coordinate2DT(1,1)));
	BOOST_CHECK(solver(corr3).empty());

	//Minimal representation
	BOOST_CHECK(P4PSolverC::MakeModelFromMinimal(P4PSolverC::MakeMinimal(mP2r)).isApprox(mP2r, 1e-3));

	//ComputeSampleStatistics
	vector<CameraMatrixT> sampleSet(7);
	sampleSet[0]=mP2r;
	sampleSet[1]=mI;
	sampleSet[2]=ComposeCameraMatrix(Coordinate3DT(0,2,1), QuaternionT(1,0,0,0).matrix());
	sampleSet[3]=ComposeCameraMatrix(Coordinate3DT(2,3,1), QuaternionT(0.5,0.5,0.5,0.5).matrix());
	sampleSet[4]=ComposeCameraMatrix(Coordinate3DT(1,2,4), QuaternionT(0.6,0,0.8,0).matrix());
	sampleSet[5]=ComposeCameraMatrix(Coordinate3DT(1,6,5), QuaternionT(0.6,0.8,0,0).matrix());
	sampleSet[6]=ComposeCameraMatrix(Coordinate3DT(0,6,9), QuaternionT(1,2,4,1).normalized().matrix());

	P4PSolverC::uncertainty_type sampleStatistics=P4PSolverC::ComputeSampleStatistics(sampleSet, vector<RealT>(7, 1.0/7), vector<RealT>(7, 1.0/7));
	BOOST_CHECK_SMALL(*sampleStatistics.ComputeMahalonobisDistance(sampleStatistics.GetMean()), 1e-6);

	P4PSolverC::minimal_model_type min2=P4PSolverC::MakeMinimal(mP2r);
	BOOST_CHECK_CLOSE(*sampleStatistics.ComputeMahalonobisDistance(min2), 7, 1e-4);

	//Overridden functions

	ModelT mP5=P3PSolverC::MakeModel(P4PSolverC::MakeVector(mP2r));
	BOOST_CHECK(mP5.isApprox(mP2r, 1e-4));

	BOOST_CHECK_SMALL(P4PSolverC::ComputeDistance(mP2r, mP2r), 1e-6);
	BOOST_CHECK_CLOSE(P4PSolverC::ComputeDistance(mP2r, sampleSet[6]), 0.45908, 1e-4);

}	//BOOST_AUTO_TEST_CASE(P4P_Solver)

BOOST_AUTO_TEST_CASE(DAQ_Solver)
{
	//Data
	vector<QuaternionT> listq(4);
	listq[0]=QuaternionT(0.85154,0.0616971,-0.519809,-0.029535);
	listq[1]=QuaternionT(0.953864, 0.0283951, -0.298819, -0.00665323);
	listq[2]=QuaternionT(0.988633, 0.0457148, -0.143168, -0.00410147);
	listq[3]=QuaternionT(0.996071, 0.0144022, 0.0872254, -0.00511926 );

	vector<Coordinate3DT> listC(4);
	listC[0]=Coordinate3DT(-2.27321, -0.0101527, -2.9959);
	listC[1]=Coordinate3DT(-1.40423, -0.0169625, -4.2509);
	listC[2]=Coordinate3DT(-0.364868, -0.0446325, -5.00531);
	listC[3]=Coordinate3DT(1.63765, 0.172693, -5.39282);

	vector<CameraMatrixT> mPList(4);
	for(size_t c=0; c<4; ++c)
		mPList[c]=ComposeCameraMatrix(listC[c], listq[c].matrix());

	//Solver, metric
	DualAbsoluteQuadricSolverC solver1;
	list<Quadric3DT> listQ1=solver1(mPList);

	Quadric3DT mQ1r=Quadric3DT::Identity(); mQ1r(3,3)=0;
	mQ1r.normalize();

	BOOST_CHECK(listQ1.size()==1);
	BOOST_CHECK(mQ1r.isApprox(*listQ1.begin(), 1e-4));

	//TODO test with some real projective cameras

	//Error
	BOOST_CHECK_SMALL(DualAbsoluteQuadricSolverC::Evaluate(mQ1r, mPList[0]), 1e-8);
	BOOST_CHECK_CLOSE(DualAbsoluteQuadricSolverC::Evaluate(mQ1r, mPList[1]+mPList[2]), 0.00017190229743066433, 1e-3);

	BOOST_CHECK_EQUAL(DualAbsoluteQuadricSolverC::GeneratorSize(), 4);
}	//BOOST_AUTO_TEST_CASE(DAQ_Solver)

BOOST_AUTO_TEST_CASE(Relative_Rotation_Registration)
{
	vector<RotationMatrix3DT> mRList(4);
	mRList[0]=QuaternionT(0.85154,0.0616971,-0.519809,-0.029535).matrix();
	mRList[1]=QuaternionT(0.953864, 0.0283951, -0.298819, -0.00665323).matrix();
	mRList[2]=QuaternionT(0.988633, 0.0457148, -0.143168, -0.00410147).matrix();
	mRList[3]=QuaternionT(0.996071, 0.0144022, 0.0872254, -0.00511926 ).matrix();

	typedef RelativeRotationRegistrationC::observation_type ObservationT;
	constexpr size_t iIndex1=RelativeRotationRegistrationC::iIndex1;
	constexpr size_t iIndex2=RelativeRotationRegistrationC::iIndex2;
	constexpr size_t iRotation=RelativeRotationRegistrationC::iRotation;

	vector<ObservationT> observations; observations.reserve(6);
	for(size_t c1=0; c1<4; ++c1)
		for(size_t c2=c1+1; c2<4; ++c2)
			observations.emplace_back(c1, c2, mRList[c2]*mRList[c1].transpose(), c1+c2);

	//Basic operation, non-minimal

	vector<RotationMatrix3DT> result1=RelativeRotationRegistrationC::Run(observations, false);
	BOOST_CHECK_EQUAL(result1.size(), 4);
	for(const auto& current : observations)
		BOOST_CHECK(get<iRotation>(current).isApprox( result1[get<iIndex2>(current)] * result1[get<iIndex1>(current)].transpose(), 1e-6) );

	vector<RotationMatrix3DT> result2=RelativeRotationRegistrationC::Run(observations, true);
	BOOST_CHECK_EQUAL(result2.size(), 4);
	for(const auto& current : observations)
		BOOST_CHECK(get<iRotation>(current).isApprox( result2[get<iIndex2>(current)] * result2[get<iIndex1>(current)].transpose(), 1e-6) );

	//Minimal solver
	vector<ObservationT> incompleteObservations{observations[2], observations[4], observations[5]};
	vector<RotationMatrix3DT> result3=RelativeRotationRegistrationC::Run(incompleteObservations, false);
	BOOST_CHECK_EQUAL(result3.size(), 4);
	for(const auto& current : observations)
		BOOST_CHECK(get<iRotation>(current).isApprox( result3[get<iIndex2>(current)] * result3[get<iIndex1>(current)].transpose(), 1e-6) );

	//Failure
	vector<ObservationT> underconstrained{observations[0], observations[5]};
	BOOST_CHECK(RelativeRotationRegistrationC::Run(underconstrained, true).empty());

	//Validation

	BOOST_CHECK(RelativeRotationRegistrationC::ValidateObservations(observations));

	vector<ObservationT> invalidObservations1(2);
	invalidObservations1[0]=observations[0];
	invalidObservations1[1]=observations[5];
	BOOST_CHECK(!RelativeRotationRegistrationC::ValidateObservations(invalidObservations1));

	vector<ObservationT> invalidObservations2(observations);
	get<iIndex1>(invalidObservations2[2])=get<iIndex2>(invalidObservations2[2]);
	BOOST_CHECK(!RelativeRotationRegistrationC::ValidateObservations(invalidObservations2));

	//Deviation
	BOOST_CHECK_CLOSE(RelativeRotationRegistrationC::ComputeAngularDeviation(mRList[0], mRList[1]), RelativeRotationRegistrationC::ComputeAngularDeviation(QuaternionT(mRList[0]), QuaternionT(mRList[1])), 1e-4);
	BOOST_CHECK_EQUAL(RelativeRotationRegistrationC::ComputeAngularDeviation(mRList[0], mRList[0]), 0);

	QuaternionT q1(0.85154,0.0616971,-0.519809,-0.029535);
	QuaternionT q2(0.953864, 0.0283951, -0.298819, -0.00665323);
	QuaternionT qr=q1*q2.conjugate(); qr.normalize();
	BOOST_CHECK_EQUAL(RelativeRotationRegistrationC::ComputeAngularDeviation(q1, q2), 2*acos(fabs(qr.w())) );
}	//BOOST_AUTO_TEST_CASE(Relative_Rotation_Registration)

BOOST_AUTO_TEST_SUITE_END()	//Geometry_Solvers

BOOST_AUTO_TEST_SUITE(Rotation_Functions)

BOOST_AUTO_TEST_CASE(Rotation)
{
	vector<QuaternionT> quaternions(4);
	quaternions[0]=QuaternionT(0.0690316, -0.70293, 0.0708777, -0.704344);
	quaternions[1]=QuaternionT(-0.70293, 0.0690316, -0.0708777, -0.704344);
	quaternions[2]=QuaternionT(-0.70293, 0.0690316, 0.704344, 0.0708777);
	quaternions[3]=QuaternionT(-0.0690316, -0.70293, -0.0708777, -0.704344);

	typedef QuaternionT::Scalar RealT;

	//QuaternionToRotationVector
	RotationVectorT vRot=QuaternionToRotationVector(quaternions[0]);
	BOOST_CHECK(vRot.isApprox(RotationVectorT(-2.11624, 0.213384, -2.1205), 1e-6));

	//ComputeQuaternionSampleStatistics
	vector<double> wMean{0.1, 0.3, 0.3, 0.3};
	vector<double> wCovariance{0.3, 0.3, 0.3, 0.1};

	QuaternionT mean1;
	QuaternionT::Matrix3 covariance1;
	tie(mean1, covariance1)=ComputeQuaternionSampleStatistics(quaternions, wMean, wCovariance);

	QuaternionT mean1r(0.480586,0.402901,-0.0607091,0.776545);
	QuaternionT::Matrix3 covariance1r; covariance1r<<0.561586, 0.128495, -0.0861665, 0.128495, 0.417243, 0.825913, -0.0861665, 0.825913, 1.92381;


	BOOST_CHECK(mean1.isApprox(mean1r, 1e-6));
	BOOST_CHECK(covariance1.isApprox(covariance1r, 1e-6));

	//JacobianQuaternionToRotationMatrix
	Matrix<QuaternionT::Scalar, 9, 4> mJQ2Rr; mJQ2Rr<<0.138063,-1.40586,-0.141755,1.40869,1.40869,0.141755,-1.40586,-0.138063,0.141755,-1.40869,0.138063,-1.40586,-1.40869,0.141755,-1.40586,0.138063,0.138063,1.40586,0.141755,1.40869,1.40586,-0.138063,-1.40869,0.141755, -0.141755,-1.40869,-0.138063,-1.40586,-1.40586,0.138063,-1.40869,0.141755,0.138063,1.40586,-0.141755,-1.40869;
	Matrix<QuaternionT::Scalar, 9, 4> mJQ2R=JacobianQuaternionToRotationMatrix(quaternions[0]);
	BOOST_CHECK(mJQ2R.isApprox(mJQ2Rr, 1e-3));

	//RotationMatrixToRotationVector
	RotationVectorT vRot2=RotationMatrixToRotationVector(quaternions[0].matrix());
	BOOST_CHECK(vRot.isApprox(vRot2, 1e-6));

	//RotationVectorToAxisAngle

	RotationMatrix3DT mR3=RotationVectorToAxisAngle(vRot2).matrix();
	BOOST_CHECK(mR3.isApprox(quaternions[0].matrix(), 1e-6));
	RotationMatrix3DT mR4=RotationVectorToAxisAngle(RotationVectorT(0,0,0)).matrix();
	BOOST_CHECK(mR4==RotationMatrix3DT::Identity());

	//RotationVectorToQuaternion
	RotationMatrix3DT mR5=RotationVectorToQuaternion(vRot2).matrix();
	BOOST_CHECK(mR5.isApprox(quaternions[0].matrix(), 1e-6));

	//JacobianQuaternionConjugation
	Matrix<RealT,4,4> mJQConj=JacobianQuaternionConjugation();
	Matrix<RealT,4,4>  mJQConjr=-Matrix<RealT,4,4>::Identity(); mJQConjr(0,0)=1;
	BOOST_CHECK(mJQConj.isApprox(mJQConjr, 1e-12));

	//JacobianQuaternionMultiplication

	Matrix<RealT,4,8> mJq1q2=JacobianQuaternionMultiplication(quaternions[0], quaternions[1]);

	Matrix<RealT,8,4> mJq1q2rT;
	mJq1q2rT.row(0)<<-0.70293,  0.0690316, -0.0708777,  -0.704344;
	mJq1q2rT.row(1)<<-0.0690316, -0.70293,   0.704344, -0.0708777;
	mJq1q2rT.row(2)<<0.0708777,  -0.704344,   -0.70293, -0.0690316;
	mJq1q2rT.row(3)<<0.704344, 0.0708777, 0.0690316,  -0.70293;
	mJq1q2rT.row(4)<<0.0690316,  -0.70293, 0.0708777, -0.704344;
	mJq1q2rT.row(5)<<0.70293,  0.0690316,  -0.704344, -0.0708777;
	mJq1q2rT.row(6)<<-0.0708777,   0.704344,  0.0690316,  -0.70293;
	mJq1q2rT.row(7)<<0.704344, 0.0708777,   0.70293, 0.0690316;

	BOOST_CHECK(mJq1q2.isApprox(mJq1q2rT.transpose(), 1e-5));

	//JacobianAxisAngleToQuaternion
	Matrix<RealT,4,3> mJAAToQdAA=JacobianRotationVectorToQuaternion(vRot);

	Matrix<RealT,4,3>  mJAAToQdAAr;mJAAToQdAAr<< 0.351465, -0.0354388,   0.352172,
												   0.184386,  0.0149002,  -0.14807,
												   0.0149002,   0.330657,  0.0149302,
												   -0.14807,  0.0149302,   0.183791;
	BOOST_CHECK(mJAAToQdAA.isApprox(mJAAToQdAAr, 1e-4));


	Matrix<RealT,4,3> mJAAToQdAA2=JacobianRotationVectorToQuaternion(vRot2);
	Matrix<RealT,4,3>  mJAAToQdAAr2;
	mJAAToQdAAr2<< 0.351465, -0.0354388,   0.352172,
				   0.184386,  0.0149002,   -0.14807,
				   0.0149002,   0.330657,  0.0149302,
				  -0.14807,  0.0149302,   0.183791;
	BOOST_CHECK(mJAAToQdAA2.isApprox(mJAAToQdAAr2, 1e-4));

	//JacobianQuaternionToRotationVector

	Matrix<RealT,3,4> mJQToAAdQ=JacobianQuaternionToRotationVector(quaternions[1]);
	Matrix<RealT,3,4> mJQToAAdQr;
	mJQToAAdQr<< -0.0594889, -2.22503, 0, 0,
				 0.0610798, 0, -2.22503, 0,
				 0.606978, 0, 0, -2.22503;
	BOOST_CHECK(mJQToAAdQ.isApprox(mJQToAAdQr, 1e-3));
}	//BOOST_AUTO_TEST_CASE(Rotation)

BOOST_AUTO_TEST_SUITE_END()	//Rotation_Functions

BOOST_AUTO_TEST_SUITE(Triangulation)

BOOST_AUTO_TEST_CASE(Manifold_Projection)
{
	//Optimal

	//Epipole at infinity

	EpipolarMatrixT mF1; mF1<<0, 0, 0, 0, 0, -0.707107, 0, 0.707107, 0;
	Coordinate2DT x(1,1);
	Coordinate2DT y(2,1);

	Coordinate2DT x1;
	Coordinate2DT y1;
	tie(x1,y1)=OptimalProjectToEpipolarManifold(mF1,x,y);

	BOOST_CHECK(x.isApprox(x1, 1e-8));
	BOOST_CHECK(y.isApprox(y1, 1e-8));

	//Perturb
	Coordinate2DT xn(1.02,1.65);
	Coordinate2DT yn(2.55,1.12);

	Coordinate2DT x2;
	Coordinate2DT y2;
	tie(x2,y2)=OptimalProjectToEpipolarManifold(mF1,xn,yn);

	BOOST_CHECK(x2.isApprox(xn, 1e-8));
	BOOST_CHECK(y2.isApprox(Coordinate2DT(2.55, 1.65), 1e-8));

	//A more general fundamental matrix
	EpipolarMatrixT mF3; mF3<<0.941524, -0.108497, 0.274965, -0.144663, 0.023072, -0.021805,  0.013674, 0.066155, 0.220270;
	unsigned int dummy;
	tie(dummy, mF3)=MakeRankN<Eigen::ColPivHouseholderQRPreconditioner>(mF3, 2);

	Coordinate2DT xn3(2,1);
	Coordinate2DT yn3(1,3);

	Coordinate2DT x3;
	Coordinate2DT y3;
	tie(x3,y3)=OptimalProjectToEpipolarManifold(mF3,xn3,yn3);

	BOOST_CHECK_SMALL( (y3.homogeneous().transpose().dot( mF3 * x3.homogeneous())), 1e-8 );

	//Fast
	Coordinate2DT x4;
	Coordinate2DT y4;
	tie(x4,y4)=OptimalProjectToEpipolarManifold(mF3,xn3,yn3);

	BOOST_CHECK(x3.isApprox(x4, 1e-6));
	BOOST_CHECK(y3.isApprox(y4, 1e-6));
}	//BOOST_AUTO_TEST_CASE(Manifold_Projection)

BOOST_AUTO_TEST_CASE(Pairwise_Triangulation)
{
	IntrinsicCalibrationMatrixT mK; mK.setIdentity();
	mK(0,0)=1000; mK(1,1)=1001; mK(0,1)=1e-5; mK(0,2)=960; mK(1,2)=540;

	CameraMatrixT mP1; mP1<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0;
	mP1 = mK*mP1;

	QuaternionT q(0.25, 0.5, 0.75, 0.15); q.normalize();
	CameraMatrixT mP2=ComposeCameraMatrix(mK, Coordinate3DT(1, 1.2, 1.5), q);

	//One-shot

	Coordinate2DT x1(-41, 57);
	Coordinate2DT y1(-359,137);

	TriangulatorC triangulator;
	Coordinate3DT x3D1=triangulator.Triangulate(mP1, mP2, x1, y1, false);

	Coordinate3DT x3D1gt(0.692539, 0.333774, -0.691817);
	BOOST_CHECK(x3D1gt.isApprox(x3D1, 1e-6));

	//Fast triangulation
	Coordinate3DT x3D2=triangulator.Triangulate(mP1, mP2, x1, y1, true);
	Coordinate3DT x3D2gt(0.692479,  0.333755, -0.691797);
	BOOST_CHECK(x3D2gt.isApprox(x3D2, 1e-6));

	//Batch
	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;
	BimapT correspondences;
	correspondences.push_back(BimapT::value_type(Coordinate2DT(1960,1541), Coordinate2DT(-519,324)));
	correspondences.push_back(BimapT::value_type(Coordinate2DT(3960,1541), Coordinate2DT(-3053,1216)));

	vector<Coordinate3DT> listX3D=triangulator(mP1, mP2, correspondences, false);
	vector<Coordinate3DT> listX3Dgt{Coordinate3DT( 1, 1.00002, 1.00001), Coordinate3DT( 1.50002, 0.500045, 0.500008)};
	BOOST_CHECK_EQUAL(listX3D.size(), 2);
	BOOST_CHECK(listX3Dgt[0].isApprox(listX3D[0], 1e-3));
	BOOST_CHECK(listX3Dgt[1].isApprox(listX3D[1], 1e-3));

	//Fast
	Coordinate3DT x3D1f=triangulator.Triangulate(mP1, mP2, x1, y1);
	Coordinate3DT x3D1gtf(0.692479, 0.333755, -0.691797);
	BOOST_CHECK(x3D1gtf.isApprox(x3D1f, 1e-3));
}	//BOOST_AUTO_TEST_CASE(Optimal_Triangulation)

BOOST_AUTO_TEST_CASE(Multiview_Triangulation)
{
	//Not a unit test, just for compilation
	MultiviewTriangulationC triangulator; (void)triangulator;
}	//BOOST_AUTO_TEST_CASE(Multiview_Triangulation)

BOOST_AUTO_TEST_SUITE_END()	//Triangulation

BOOST_AUTO_TEST_SUITE(Scene_Coverage)

BOOST_AUTO_TEST_CASE(Scene_Coverage_Assessment)
{
	//Synthetic lattice
	Array<ValueTypeM<Coordinate3DT>::type, 3, 2 > extents; extents<<-4, 3, -4, 11, -8, 10;
	vector<Coordinate3DT> lattice=GenerateRectangularLattice(extents, 1);

	//Cameras

	vector<QuaternionT> listq(4);
	listq[0]=QuaternionT(0.605435, 0.343515, 0.343742, 0.630307);
	listq[1]=QuaternionT(0.543434, 0.395568, 0.426369, 0.605323);
	listq[2]=QuaternionT(0.448777, 0.548241, 0.488983, 0.508849);
	listq[3]=QuaternionT(0.371866, 0.558387, 0.568865, 0.475722);

	vector<Coordinate3DT> listC(4);
	listC[0]=Coordinate3DT(-0.0943611, -4.19398, -0.758271);
	listC[1]=Coordinate3DT(-0.102802, -4.53996, -0.241605);
	listC[2]=Coordinate3DT(-0.448148, -4.85523, 1.23215);
	listC[3]=Coordinate3DT(-0.541562, -5.03355, 2.83486);

	IntrinsicCalibrationMatrixT mK; mK<<1700, 0, 960, 0, 1700, 540, 0, 0, 1;
	vector<CameraC> cameraList(4);
	for(int c=0; c<4; ++c)
	{
		CameraMatrixT mP=ComposeCameraMatrix(mK, listC[c], listq[c]);

		FrameT frameBox(Coordinate2DT(0,0), Coordinate2DT(1919, 1079));

		LensDistortionC distortion;
		distortion.kappa=-((c+1)*1e-5);
		distortion.centre=Coordinate2DT(mK(0,2), mK(1,2));
		distortion.modelCode=LensDistortionCodeT::FP1;

		cameraList[c]=CameraC(mP, distortion, frameBox);

		mK(0,0)+=25; mK(1,1)+=25; mK(0,2)-=5; mK(1,2)+=5;
	}	//for(size_t c=0; c<4; ++c)

	vector<SceneCoverageEstimationC::unary_constraint_type> constraints; constraints.reserve(4);
	for(int c=0; c<4; ++c)
		constraints.emplace_back((c+1)*1e-5, FrameT(Coordinate2DT(300-c*10, 300-c*10), Coordinate2DT(1600+c*10, 1600+c*10)));

	//Tests

	//Parameter validity
	SceneCoverageEstimationParametersC parameters;
	parameters.radius=-1;
	BOOST_CHECK_THROW(SceneCoverageEstimationC(lattice, parameters), invalid_argument);
	parameters.radius=0.1;

	SceneCoverageEstimationC simulator(lattice, parameters);
	BOOST_CHECK(lattice==simulator.GetScene());

	typedef optional<tuple<unsigned int, unsigned int> > UIntPairT;

	//Invalid add
	UIntPairT addInvalid1=simulator.AddView(CameraC(), constraints[0]);
	BOOST_CHECK(!addInvalid1);
	BOOST_CHECK(simulator.GetCameras().empty());

	SceneCoverageEstimationC::unary_constraint_type invalidConstraint(-1, FrameT());
	BOOST_CHECK_THROW(simulator.AddView(cameraList[0], invalidConstraint), invalid_argument);

	//Simulated add
	UIntPairT addSim=simulator.AddView(cameraList[0], constraints[0], true);
	BOOST_CHECK(addSim);
	BOOST_CHECK(simulator.GetCameras().empty());
	BOOST_CHECK_EQUAL(get<0>(*addSim), 360);
	BOOST_CHECK_EQUAL(get<1>(*addSim), 360);

	//True add
	UIntPairT add0=simulator.AddView(cameraList[0], constraints[0]);
	BOOST_CHECK_EQUAL(simulator.GetCameras().size(),1);
	BOOST_CHECK(simulator.GetCameras().begin()->second.MakeCameraMatrix()->isApprox(*cameraList[0].MakeCameraMatrix(), 1e-6) );
	BOOST_CHECK_EQUAL(get<0>(*addSim), get<0>(*add0));
	BOOST_CHECK_EQUAL(get<1>(*addSim), get<1>(*add0));

	//Add another, and then remove
	UIntPairT add1=simulator.AddView(cameraList[1], constraints[1]);

	//Remove nonexistent
	UIntPairT removeInvalid=simulator.RemoveView(5);
	BOOST_CHECK(!removeInvalid);

	//Simulated remove
	UIntPairT removeSim=simulator.RemoveView(0, true);
	BOOST_CHECK(removeSim);
	BOOST_CHECK_EQUAL(simulator.GetCameras().size(),2);
	BOOST_CHECK_EQUAL(get<0>(*removeSim), 360);
	BOOST_CHECK_EQUAL(get<1>(*removeSim), 94);

	//True removal
	UIntPairT remove0=simulator.RemoveView(0);
	BOOST_CHECK_EQUAL(simulator.GetCameras().size(),1);
	BOOST_CHECK_EQUAL(get<0>(*removeSim), get<0>(*remove0));
	BOOST_CHECK_EQUAL(get<1>(*removeSim), get<1>(*remove0));

	//Clear
	simulator.Clear();
	BOOST_CHECK(simulator.GetCameras().empty());
	BOOST_CHECK(simulator.GetScene()==lattice);

	//Queries

	add0=simulator.AddView(cameraList[0], constraints[0]);
	add1=simulator.AddView(cameraList[1], constraints[1]);
	UIntPairT add2=simulator.AddView(cameraList[2], constraints[2]);

	//GetCoveredVertices

	vector<size_t> covered;
	vector<size_t> redundancy;
	tie(covered, redundancy)=simulator.GetCoveredVertices(0);
	BOOST_CHECK_EQUAL(covered.size(), lattice.size());

	tie(covered, redundancy)=simulator.GetCoveredVertices(2);
	BOOST_CHECK_EQUAL(covered.size(), redundancy.size());
	BOOST_CHECK_EQUAL(covered.size(), 322);

	tie(covered, redundancy)=simulator.GetCoveredVertices(4);
	BOOST_CHECK(covered.empty());
	BOOST_CHECK(redundancy.empty());

	//PredictMatchingPerformance

	unsigned int matchScore00=simulator.PredictMatchingPerformance(0,0,0.6,2);
	BOOST_CHECK_EQUAL(matchScore00, 360);

	unsigned int matchScore01=simulator.PredictMatchingPerformance(0,1,0.6,2);
	BOOST_CHECK_EQUAL(matchScore01, 144);

	unsigned int matchScore03=simulator.PredictMatchingPerformance(0,3,0.6,2);
	BOOST_CHECK_EQUAL(matchScore03, 0);

	//ComputeCoverageStatistics
	UIntPairT remove2=simulator.RemoveView(2, true);
	UIntPairT coverage2=simulator.ComputeCoverageStatistics(2);
	BOOST_CHECK_EQUAL(get<0>(*remove2), get<0>(*coverage2));
	BOOST_CHECK_EQUAL(get<1>(*remove2), get<1>(*coverage2));

	map<unsigned int, tuple<unsigned int, unsigned int> > contribution=simulator.ComputeCoverageStatistics();
	BOOST_CHECK(*simulator.ComputeCoverageStatistics(0) == contribution[0]);
	BOOST_CHECK(*simulator.ComputeCoverageStatistics(1) == contribution[1]);
	BOOST_CHECK(*simulator.ComputeCoverageStatistics(2) == contribution[2]);

	//RankViews
	map<unsigned int, size_t, greater<unsigned int> > ranks=simulator.RankViews(cameraList[3], constraints[3], 0.6, 2);
	BOOST_CHECK_EQUAL(ranks.begin()->first, 2);
	BOOST_CHECK_EQUAL(ranks.begin()->second, 216);

	//Gradient

	typedef SceneCoverageEstimationC::gradient_type GradientT;
	GradientT nablaCoverage;
	GradientT nablaUnique;
	tie(nablaCoverage, nablaUnique)=simulator.ComputeCoverageGradient(cameraList[3], constraints[3], 0.1, 0.05, 25);

	GradientT nablaCoverageGT; nablaCoverageGT<<-10, -15, 0, -630, -150, 150, -0.08;
	GradientT nablaUniqueGT; nablaUniqueGT<<25, -20, -30, -340, 440, 50, -0.04;

	BOOST_CHECK(nablaCoverage==nablaCoverageGT);
	BOOST_CHECK(nablaUnique==nablaUniqueGT);
}	//BOOST_AUTO_TEST_CASE(Scene_Coverage_Assessment)

BOOST_AUTO_TEST_SUITE_END()	//Scene_Coverage

}   //TestGeometryN
}   //UnitTestsN
}	//SeeSawN


