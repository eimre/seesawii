/**
 * @file Rotation3DSolver.cpp Implementation of the 2-point 3D rotation solver
 * @author Evren Imre
 * @date 25 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Rotation3DSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Extracts the rotation component from a model
 * @param[in] model Model
 * @return First 3x3 submatrix, scaled to have a Frobenius norm of sqrt(3)
 */
RotationMatrix3DT Rotation3DSolverC::ExtractRotation(const ModelT& model)
{
	RotationMatrix3DT mR=model.block(0,0,3,3);

	double scale=sqrt(3)/mR.norm();
	return scale*mR;
}	//RotationMatrix3DT ExtractRotation(const ModelT& model)

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double Rotation3DSolverC::Cost(unsigned int nCorrespondences)
{
	return ComplexityEstimatorC::EVD(4) + nCorrespondences*(ComplexityEstimatorC::MatrixAdd(3,3) + ComplexityEstimatorC::OuterProduct(3,3));
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return 4x4 Homography matrix
 */
auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	ModelT output=ModelT::Identity();
	output.block(0,0,3,3)=RotationVectorToAxisAngle(minimalModel).matrix();
	return output;
}	//auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return Angle between the two models, normalised to the range [0,1]
 */
auto Rotation3DSolverC::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	RotationMatrix3DT mR1=ExtractRotation(model1);
	RotationMatrix3DT mR2=ExtractRotation(model2);

	RealT angle=AxisAngleT(mR1*mR2.transpose()).angle();
	distance_type dist=angle/two_pi<RealT>();

	return (dist<0) ? dist+1 : dist;
}	//distance_type ComputeDistance(const ModelT& model1, const ModelT& model2)

/**
 * @brief Converts a model to a vector
 * @param[in] model Model
 * @return First 3x3 submatrix of \c model in vector form
 */
auto Rotation3DSolverC::MakeVector(const ModelT& model) -> VectorT
{
	return Reshape<VectorT>(ExtractRotation(model), 9, 1);
}	//VectorT MakeVector(const ModelT& model)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel Vector
 * @param[in] dummy Dummy parameter to meet the concept requirements
 * @return A 4x4 homography
 * @pre \c vModel is a 9-element vector representing a rotation matrix (unenforced)
 */
auto Rotation3DSolverC::MakeModel(const VectorT& vModel, bool dummy) -> ModelT
{
	//Preconditions
	assert(vModel.size()==9);

	ModelT output=ModelT::Identity();
	output.block(0,0,3,3)=Reshape<RotationMatrix3DT>(sqrt(3)*vModel.normalized(), 3, 3);
	return output;
}	//ModelT MakeModel(const VectorT& vModel)

/********** Rotation3DMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented 3D rotations
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector
 * @remarks Difference is the rotation vector that takes \c op2 to \c op1
 */
auto Rotation3DMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	QuaternionT q1( RotationVectorToAxisAngle(op1) );
	QuaternionT q2( RotationVectorToAxisAngle(op2) );

	return QuaternionToRotationVector(q1*q2.conjugate());
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

