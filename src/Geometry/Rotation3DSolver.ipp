/**
 * @file Rotation3DSolver.ipp Implementation of the 2-point 3D rotation solver
 * @author Evren Imre
 * @date 25 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROTATION3D_SOLVER_IPP_0063312
#define ROTATION3D_SOLVER_IPP_0063312

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/functions.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <vector>
#include <tuple>
#include <cstddef>
#include <cmath>
#include <type_traits>
#include <list>
#include "GeometrySolverBase.h"
#include "Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricError.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Numeric/Complexity.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"

namespace SeeSawN
{
namespace GeometryN
{

//Forward declaration
struct Rotation3DMinimalDifferenceC;

using boost::math::constants::two_pi;
using boost::range_value;
using boost::ForwardRangeConcept;
using Eigen::Matrix3d;
using Eigen::Matrix4d;
using Eigen::SelfAdjointEigenSolver;
using std::vector;
using std::tie;
using std::size_t;
using std::sqrt;
using std::is_same;
using std::list;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;
using SeeSawN::GeometryN::RotationVectorToAxisAngle;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::AxisAngleT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::MetricsN::SymmetricTransferErrorC;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::NumericN::ComplexityEstimatorC;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
//TESTME

/**
 * @brief 2-point 3D rotation estimator
 * @remarks Horn, B. K. P. , "Closed-Form Solution of Absolute Orientation Using Quaternions," Journal of the Optical Society of America A, vol. 4, pp. 629-642, April 1987
 * @remarks The input correspondences should be normalised to have the same mean and scale
 * @remarks In order to facilitate interoperability with the rest of the code, a 3D rotation is represented as a 4x4 homography
 * @warning Avoid non-isotropic scaling!
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class Rotation3DSolverC : public GeometrySolverBaseC<Rotation3DSolverC, Homography3DT, SymmetricTransferErrorC<Homography3DT>, 3, 3, 2, true, 1, 16, 3>
{
	private:

		typedef GeometrySolverBaseC<Rotation3DSolverC, Homography3DT, SymmetricTransferErrorC<Homography3DT>, 3, 3, 2, true, 1, 16, 3> BaseT;	///< Type of the base

		typedef typename BaseT::model_type ModelT;	///< 4x4 homography in which the rotation matrix is embedded
		typedef MultivariateGaussianC<Rotation3DMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		/** @name Implementation details */ ///@{
		static RotationMatrix3DT ExtractRotation(const ModelT& model);	///< Extracts the rotation component from a model
		///@}
	public:

		/** @name GeometrySolverConceptC interface */ //@{

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the rotation that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef RotationVectorT minimal_model_type;	///< A minimally parameterised model. Axis-angle vector
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class HomographyRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of rotations
		//@}

		/** @name Overrides */ //@{
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool dummy=true);	///< Converts a vector to a model
		//@}
};	//class Rotation3DSolverC

/**
 * @brief Difference functor for minimally-represented 3D rotations
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct Rotation3DMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef Rotation3DSolverC::minimal_model_type first_argument_type;
	typedef Rotation3DSolverC::minimal_model_type second_argument_type;
	typedef RotationVectorT result_type;	///< Difference in vector form

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between two minimally-represented 3D rotations
	//@}
};	//struct Rotation3DMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre First 3x3 submatrix of \c model represents a valid rotation (unenforced)
 * @return Minimal form
 */
template<class DummyRangeT>
auto Rotation3DSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	return RotationMatrixToRotationVector(ExtractRotation(model));
}	//auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of rotations
 * @tparam RotationRangeT A range of 3D rotations
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] rotations Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c RotationRangeT is a forward range of elements of type \c Homography3DT
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c rotations has 2 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class RotationRangeT, class WeightRangeT, class DummyRangeT>
auto Rotation3DSolverC::ComputeSampleStatistics(const RotationRangeT& rotations, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<RotationRangeT>));
	static_assert(is_same<ModelT, typename range_value<RotationRangeT>::type >::value, "Rotation3DSolverC::ComputeSampleStatistics : RotationRangeT must hold elements of type Homography3DT");

	//Rotation matrix to quaternion
	size_t nSamples=boost::distance(rotations);
	vector<QuaternionT> qList; qList.reserve(nSamples);
	for(const auto& current : rotations)
		qList.emplace_back(ExtractRotation(current));

	QuaternionT meanQ;
	typename GaussianT::covariance_type mCov;
	tie(meanQ, mCov)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	return GaussianT( QuaternionToRotationVector(meanQ), mCov);
}	//auto ComputeSampleStatistics(const RotationRangeT& rotations, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT

/**
 * @brief Computes the rotation that best explains the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding a 4x4 homography
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap of elements of type \c Coordinate3DT
 */
template<class CorrespondenceRangeT>
auto Rotation3DSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "Rotation3DSolverC::operator() : CorrespondenceRangeT must be bimap of elements of type Coordinate3DT");
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::right_key_type>::value, "Rotation3DSolverC::operator() : CorrespondenceRangeT must be bimap of elements of type Coordinate3DT");

	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	//4A: Construct M
	Matrix3d mM=Matrix3d::Zero();
	for(const auto& current : correspondences)
		mM += current.left * current.right.transpose();

	//Construct N. Only the lower-triangular part is referenced by the eigensolver. So, the upper-triangular part is not initialised
	Matrix4d mN;
    mN(0,0)=mM(0,0)+mM(1,1)+mM(2,2); mN(1,1)=mM(0,0)-mM(1,1)-mM(2,2); mN(2,2)=-mM(0,0)+mM(1,1)-mM(2,2); mN(3,3)=-mM(0,0)-mM(1,1)+mM(2,2);
    mN(1,0)=mM(1,2)-mM(2,1); mN(2,0)=mM(2,0)-mM(0,2); mN(3,0)=mM(0,1)-mM(1,0);
    mN(2,1)=mM(0,1)+mM(1,0); mN(3,1)=mM(2,0)+mM(0,2);
    mN(3,2)=mM(1,2)+mM(2,1);

    //Find the eigenvector corresponding to the maximum eigenvalue
    typedef SelfAdjointEigenSolver<Matrix4d> EVDSolverT;
    EVDSolverT evd(mN);

    if(evd.info()!=Eigen::Success)
    	return list<ModelT>();

    typename EVDSolverT::RealVectorType maxEv=evd.eigenvectors().col(3);
    maxEv.normalize();
    QuaternionT q(maxEv[0], maxEv[1], maxEv[2], maxEv[3]);

    list<ModelT> output;
    output.push_back(ModelT::Identity());
    output.begin()->block(0,0,3,3)=q.matrix();

	return output;
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT
}	//GeometryN
}	//SeeSawN

#endif /* ROTATION3D_SOLVER_IPP_0063312 */
