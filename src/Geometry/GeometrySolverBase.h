/**
 * @file GeometrySolverBase.h Public interface for GeometrySolverBaseC
 * @author Evren Imre
 * @date 10 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_SOLVER_BASE_H_8081221
#define GEOMETRY_SOLVER_BASE_H_8081221

#include "GeometrySolverBase.ipp"
namespace SeeSawN
{
namespace GeometryN
{

template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF> class GeometrySolverBaseC;	///< Base class for geometry solvers
}	//GeometryN
}	//SeeSawN

#endif /* GEOMETRY_SOLVER_BASE_H_8081221 */
