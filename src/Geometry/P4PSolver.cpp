/**
 * @file P4PSolver.cpp Implementation of the 4-point calibration solver
 * @author Evren Imre
 * @date 12 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P4PSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Estimates the focal length and the distances from the camera centre
 * @param edgeLengths Edge length between the pairs of world points
 * @param x1 First image point
 * @param x2 Second image point
 * @param x3 Third image point
 * @param x4 Fourth image point
 * @return A list of solutions
 */
auto P4PSolverC::RunP4PCore(const array<RealT,6>& edgeLengths, const Coordinate2DT& x1, const Coordinate2DT& x2, const Coordinate2DT& x3, const Coordinate2DT& x4) -> list<SolutionT>
{
	list<SolutionT> output;

	optional<ActionMatrixT> mA=BuildActionMatrix(edgeLengths, x1, x2, x3, x4);

	if(!mA)
		return output;

	typedef EigenSolver<ActionMatrixT> EVDT;
	EVDT evdEngine(*mA);

	if(evdEngine.info()!=Eigen::Success)
		return output;

	EVDT::EigenvectorsType mV=evdEngine.eigenvectors().topRows(5);

	RealT realZero=3*numeric_limits<RealT>::epsilon();
	SolutionT solution;
	for(size_t c=0; c<10; ++c)
	{
		if(abs(mV(0,c))==0)
			return output;

		mV.col(c)/=mV(0,c);

		//Focal length, real and positive?
		if( fabs(mV(4,c).imag()) >= realZero || mV(4,c).real()<=realZero )
			continue;

		for(size_t c2=0; c2<4; ++c2)
			solution[c2]=mV(4-c2, c).real();

		solution[0]=sqrt(solution[0]);

		output.push_back(solution);
	}	//for(size_t c=0; c<10; ++c)

	return output;
}	//list<SolutionT> RunP4PCore(const array<RealT,6>& edgeLengths, const Coordinate2DT& x1, const Coordinate2DT& x2, const Coordinate2DT& x3, const Coordinate2DT& x4)

/**
 * @brief Computational cost of the solver
 * @param[in] dummy Dummy variable
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double P4PSolverC::Cost(unsigned int dummy)
{
	//Distances
	double dist1=6*(ComplexityEstimatorC::VectorAdd(3) + ComplexityEstimatorC::L2Norm(3));

	//P4P core
	double luCost=ComplexityEstimatorC::MinimalLinearSolver(78, 78);	//Overestimate: LU is sparse
	double evdCost=ComplexityEstimatorC::EVD(10);
	double scale1=10*ComplexityEstimatorC::VectorScale(5);

	//Local coordinates
	double localCoord= dist1 + 4*ComplexityEstimatorC::VectorScale(3);

	//Soution
	double sol= localCoord + Similarity3DSolverC::Cost(4);

	return dist1 + (luCost+evdCost+scale1) + 10*sol;	//Overestimate, 10 solutions is very unlikely
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Camera matrix
 */
auto P4PSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity(); mK(0,0)=get<iFocalLength>(minimalModel); mK(1,1)=mK(0,0);

	return ComposeCameraMatrix(mK, get<iPosition>(minimalModel), RotationVectorToQuaternion(get<iOrientation>(minimalModel)).matrix());
}	//auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return \f$ 1- \frac{|<\mathbf{M_1, M_2}>|}{\sqrt{<\mathbf{M_1,M_1}><\mathbf{M_2,M_2}>}} \f$. [0,1]
 */
auto P4PSolverC::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	return P3PSolverC::ComputeDistance(model1, model2);
}	//auto ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type

/**
 * @brief Converts a model to a vector
 * @param[in] model Model
 * @return Camera matrix in vector form
 */
auto P4PSolverC::MakeVector(const ModelT& model) -> VectorT
{
	return P3PSolverC::MakeVector(model);
}	//VectorT MakeVector(const ModelT& model)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel Vector
 * @param[in] dummy Dummy parameter to satisfy the concept requirements
 * @return A camera matrix
 */
auto P4PSolverC::MakeModel(const VectorT& vModel, bool dummy) -> ModelT
{
	return P3PSolverC::MakeModel(vModel, dummy);
}	//ModelT MakeModel(const VectorT& vModel)

/**
 * @brief Builds the action matrix
 * @param edgeLengths Edge length between the pairs of world points
 * @param x1 First image point
 * @param x2 Second image point
 * @param x3 Third image point
 * @param x4 Fourth image point
 * @return Action matrix. Invalid if LU decomposition fails
 */
auto P4PSolverC::BuildActionMatrix(const array<RealT,6>& edgeLengths, const Coordinate2DT& x1, const Coordinate2DT& x2, const Coordinate2DT& x3, const Coordinate2DT& x4) -> optional<ActionMatrixT>
{
	optional<ActionMatrixT> mA;

	double glab=edgeLengths[0];
	double glac=edgeLengths[1];
	double glad=edgeLengths[2];
	double glbc=edgeLengths[3];
	double glbd=edgeLengths[4];
	double glcd=edgeLengths[5];

	double a1=x1[0]; double a12=pow<2>(a1);
	double a2=x1[1]; double a22=pow<2>(a2);
	double b1=x2[0];
	double b2=x2[1];
	double c1=x3[0]; double c12=pow<2>(c1);
	double c2=x3[1]; double c22=pow<2>(c2);
	double d1=x4[0]; double d12=pow<2>(d1);
	double d2=x4[1]; double d22=pow<2>(d2);

	typedef Triplet<double> TripletT;
	array<TripletT, 736> tripletList;
	size_t i=0;

	double q1= 1;
	tripletList[i]=TripletT(71, 0, q1); ++i;
	tripletList[i]=TripletT(73, 0, q1); ++i;
	tripletList[i]=TripletT(70, 1, q1); ++i;
	tripletList[i]=TripletT(75, 1, q1); ++i;
	tripletList[i]=TripletT(72, 2, q1); ++i;
	tripletList[i]=TripletT(77, 2, q1); ++i;
	tripletList[i]=TripletT(74, 3, q1); ++i;
	tripletList[i]=TripletT(76, 4, q1); ++i;
	tripletList[i]=TripletT(51, 6, q1); ++i;
	tripletList[i]=TripletT(58, 6, q1); ++i;
	tripletList[i]=TripletT(50, 7, q1); ++i;
	tripletList[i]=TripletT(62, 7, q1); ++i;
	tripletList[i]=TripletT(57, 8, q1); ++i;
	tripletList[i]=TripletT(69, 8, q1); ++i;
	tripletList[i]=TripletT(49, 9, q1); ++i;
	tripletList[i]=TripletT(56, 9, q1); ++i;
	tripletList[i]=TripletT(48, 10, q1); ++i;
	tripletList[i]=TripletT(55, 10, q1); ++i;
	tripletList[i]=TripletT(68, 10, q1); ++i;
	tripletList[i]=TripletT(61, 11, q1); ++i;
	tripletList[i]=TripletT(67, 12, q1); ++i;
	tripletList[i]=TripletT(47, 13, q1); ++i;
	tripletList[i]=TripletT(60, 13, q1); ++i;
	tripletList[i]=TripletT(54, 14, q1); ++i;
	tripletList[i]=TripletT(66, 14, q1); ++i;
	tripletList[i]=TripletT(46, 15, q1); ++i;
	tripletList[i]=TripletT(53, 15, q1); ++i;
	tripletList[i]=TripletT(59, 17, q1); ++i;
	tripletList[i]=TripletT(65, 18, q1); ++i;
	tripletList[i]=TripletT(45, 19, q1); ++i;
	tripletList[i]=TripletT(64, 19, q1); ++i;
	tripletList[i]=TripletT(52, 20, q1); ++i;
	tripletList[i]=TripletT(63, 20, q1); ++i;
	tripletList[i]=TripletT(22, 24, q1); ++i;
	tripletList[i]=TripletT(29, 25, q1); ++i;
	tripletList[i]=TripletT(21, 26, q1); ++i;
	tripletList[i]=TripletT(28, 26, q1); ++i;
	tripletList[i]=TripletT(20, 27, q1); ++i;
	tripletList[i]=TripletT(27, 27, q1); ++i;
	tripletList[i]=TripletT(35, 28, q1); ++i;
	tripletList[i]=TripletT(44, 29, q1); ++i;
	tripletList[i]=TripletT(19, 30, q1); ++i;
	tripletList[i]=TripletT(34, 30, q1); ++i;
	tripletList[i]=TripletT(43, 30, q1); ++i;
	tripletList[i]=TripletT(26, 31, q1); ++i;
	tripletList[i]=TripletT(42, 31, q1); ++i;
	tripletList[i]=TripletT(18, 32, q1); ++i;
	tripletList[i]=TripletT(25, 32, q1); ++i;
	tripletList[i]=TripletT(33, 32, q1); ++i;
	tripletList[i]=TripletT(41, 32, q1); ++i;
	tripletList[i]=TripletT(24, 33, q1); ++i;
	tripletList[i]=TripletT(40, 33, q1); ++i;
	tripletList[i]=TripletT(32, 35, q1); ++i;
	tripletList[i]=TripletT(39, 36, q1); ++i;
	tripletList[i]=TripletT(17, 37, q1); ++i;
	tripletList[i]=TripletT(31, 37, q1); ++i;
	tripletList[i]=TripletT(38, 37, q1); ++i;
	tripletList[i]=TripletT(23, 38, q1); ++i;
	tripletList[i]=TripletT(37, 38, q1); ++i;
	tripletList[i]=TripletT(30, 41, q1); ++i;
	tripletList[i]=TripletT(36, 42, q1); ++i;
	tripletList[i]=TripletT(6, 44, q1); ++i;
	tripletList[i]=TripletT(9, 45, q1); ++i;
	tripletList[i]=TripletT(5, 46, q1); ++i;
	tripletList[i]=TripletT(8, 46, q1); ++i;
	tripletList[i]=TripletT(12, 49, q1); ++i;
	tripletList[i]=TripletT(16, 50, q1); ++i;
	tripletList[i]=TripletT(4, 51, q1); ++i;
	tripletList[i]=TripletT(11, 51, q1); ++i;
	tripletList[i]=TripletT(15, 51, q1); ++i;
	tripletList[i]=TripletT(7, 52, q1); ++i;
	tripletList[i]=TripletT(14, 52, q1); ++i;
	tripletList[i]=TripletT(10, 55, q1); ++i;
	tripletList[i]=TripletT(13, 56, q1); ++i;
	tripletList[i]=TripletT(0, 64, q1); ++i;
	tripletList[i]=TripletT(1, 65, q1); ++i;
	tripletList[i]=TripletT(2, 67, q1); ++i;
	tripletList[i]=TripletT(3, 68, q1); ++i;

	double q2= 0.5/glad*glbc-0.5*glab/glad-0.5*glac/glad;
	tripletList[i]=TripletT(71, 4, q2); ++i;
	tripletList[i]=TripletT(70, 5, q2); ++i;
	tripletList[i]=TripletT(51, 12, q2); ++i;
	tripletList[i]=TripletT(50, 16, q2); ++i;
	tripletList[i]=TripletT(49, 18, q2); ++i;
	tripletList[i]=TripletT(48, 20, q2); ++i;
	tripletList[i]=TripletT(47, 21, q2); ++i;
	tripletList[i]=TripletT(46, 23, q2); ++i;
	tripletList[i]=TripletT(21, 36, q2); ++i;
	tripletList[i]=TripletT(20, 38, q2); ++i;
	tripletList[i]=TripletT(19, 40, q2); ++i;
	tripletList[i]=TripletT(18, 42, q2); ++i;
	tripletList[i]=TripletT(17, 43, q2); ++i;
	tripletList[i]=TripletT(6, 54, q2); ++i;
	tripletList[i]=TripletT(5, 56, q2); ++i;
	tripletList[i]=TripletT(4, 59, q2); ++i;
	tripletList[i]=TripletT(0, 71, q2); ++i;

	double q3= -1;
	tripletList[i]=TripletT(71, 7, q3); ++i;
	tripletList[i]=TripletT(75, 7, q3); ++i;
	tripletList[i]=TripletT(71, 8, q3); ++i;
	tripletList[i]=TripletT(77, 8, q3); ++i;
	tripletList[i]=TripletT(70, 11, q3); ++i;
	tripletList[i]=TripletT(74, 11, q3); ++i;
	tripletList[i]=TripletT(70, 12, q3); ++i;
	tripletList[i]=TripletT(76, 12, q3); ++i;
	tripletList[i]=TripletT(51, 24, q3); ++i;
	tripletList[i]=TripletT(62, 24, q3); ++i;
	tripletList[i]=TripletT(51, 25, q3); ++i;
	tripletList[i]=TripletT(69, 25, q3); ++i;
	tripletList[i]=TripletT(68, 27, q3); ++i;
	tripletList[i]=TripletT(50, 28, q3); ++i;
	tripletList[i]=TripletT(61, 28, q3); ++i;
	tripletList[i]=TripletT(50, 29, q3); ++i;
	tripletList[i]=TripletT(67, 29, q3); ++i;
	tripletList[i]=TripletT(49, 30, q3); ++i;
	tripletList[i]=TripletT(60, 30, q3); ++i;
	tripletList[i]=TripletT(49, 31, q3); ++i;
	tripletList[i]=TripletT(66, 31, q3); ++i;
	tripletList[i]=TripletT(48, 32, q3); ++i;
	tripletList[i]=TripletT(48, 33, q3); ++i;
	tripletList[i]=TripletT(47, 35, q3); ++i;
	tripletList[i]=TripletT(59, 35, q3); ++i;
	tripletList[i]=TripletT(47, 36, q3); ++i;
	tripletList[i]=TripletT(65, 36, q3); ++i;
	tripletList[i]=TripletT(46, 37, q3); ++i;
	tripletList[i]=TripletT(64, 37, q3); ++i;
	tripletList[i]=TripletT(46, 38, q3); ++i;
	tripletList[i]=TripletT(63, 38, q3); ++i;
	tripletList[i]=TripletT(45, 41, q3); ++i;
	tripletList[i]=TripletT(45, 42, q3); ++i;
	tripletList[i]=TripletT(21, 44, q3); ++i;
	tripletList[i]=TripletT(34, 44, q3); ++i;
	tripletList[i]=TripletT(43, 44, q3); ++i;
	tripletList[i]=TripletT(21, 45, q3); ++i;
	tripletList[i]=TripletT(42, 45, q3); ++i;
	tripletList[i]=TripletT(20, 46, q3); ++i;
	tripletList[i]=TripletT(33, 46, q3); ++i;
	tripletList[i]=TripletT(41, 46, q3); ++i;
	tripletList[i]=TripletT(20, 47, q3); ++i;
	tripletList[i]=TripletT(40, 47, q3); ++i;
	tripletList[i]=TripletT(19, 49, q3); ++i;
	tripletList[i]=TripletT(32, 49, q3); ++i;
	tripletList[i]=TripletT(19, 50, q3); ++i;
	tripletList[i]=TripletT(39, 50, q3); ++i;
	tripletList[i]=TripletT(18, 51, q3); ++i;
	tripletList[i]=TripletT(31, 51, q3); ++i;
	tripletList[i]=TripletT(38, 51, q3); ++i;
	tripletList[i]=TripletT(18, 52, q3); ++i;
	tripletList[i]=TripletT(37, 52, q3); ++i;
	tripletList[i]=TripletT(17, 55, q3); ++i;
	tripletList[i]=TripletT(30, 55, q3); ++i;
	tripletList[i]=TripletT(17, 56, q3); ++i;
	tripletList[i]=TripletT(36, 56, q3); ++i;
	tripletList[i]=TripletT(6, 62, q3); ++i;
	tripletList[i]=TripletT(12, 62, q3); ++i;
	tripletList[i]=TripletT(6, 63, q3); ++i;
	tripletList[i]=TripletT(16, 63, q3); ++i;
	tripletList[i]=TripletT(5, 64, q3); ++i;
	tripletList[i]=TripletT(11, 64, q3); ++i;
	tripletList[i]=TripletT(15, 64, q3); ++i;
	tripletList[i]=TripletT(5, 65, q3); ++i;
	tripletList[i]=TripletT(14, 65, q3); ++i;
	tripletList[i]=TripletT(4, 67, q3); ++i;
	tripletList[i]=TripletT(10, 67, q3); ++i;
	tripletList[i]=TripletT(4, 68, q3); ++i;
	tripletList[i]=TripletT(13, 68, q3); ++i;
	tripletList[i]=TripletT(0, 75, q3); ++i;
	tripletList[i]=TripletT(2, 75, q3); ++i;
	tripletList[i]=TripletT(0, 76, q3); ++i;
	tripletList[i]=TripletT(3, 76, q3); ++i;

	double q4= c2*b2+c1*b1;
	tripletList[i]=TripletT(71, 9, q4); ++i;
	tripletList[i]=TripletT(70, 13, q4); ++i;
	tripletList[i]=TripletT(51, 26, q4); ++i;
	tripletList[i]=TripletT(50, 30, q4); ++i;
	tripletList[i]=TripletT(49, 32, q4); ++i;
	tripletList[i]=TripletT(48, 34, q4); ++i;
	tripletList[i]=TripletT(47, 37, q4); ++i;
	tripletList[i]=TripletT(46, 39, q4); ++i;
	tripletList[i]=TripletT(22, 44, q4); ++i;
	tripletList[i]=TripletT(21, 46, q4); ++i;
	tripletList[i]=TripletT(20, 48, q4); ++i;
	tripletList[i]=TripletT(19, 51, q4); ++i;
	tripletList[i]=TripletT(18, 53, q4); ++i;
	tripletList[i]=TripletT(17, 57, q4); ++i;
	tripletList[i]=TripletT(6, 64, q4); ++i;
	tripletList[i]=TripletT(5, 66, q4); ++i;
	tripletList[i]=TripletT(4, 69, q4); ++i;
	tripletList[i]=TripletT(0, 77, q4); ++i;

	double q5= glac/glad-1/glad*glbc+glab/glad;
	tripletList[i]=TripletT(71, 12, q5); ++i;
	tripletList[i]=TripletT(70, 16, q5); ++i;
	tripletList[i]=TripletT(51, 29, q5); ++i;
	tripletList[i]=TripletT(49, 36, q5); ++i;
	tripletList[i]=TripletT(48, 38, q5); ++i;
	tripletList[i]=TripletT(47, 40, q5); ++i;
	tripletList[i]=TripletT(46, 42, q5); ++i;
	tripletList[i]=TripletT(45, 43, q5); ++i;
	tripletList[i]=TripletT(21, 50, q5); ++i;
	tripletList[i]=TripletT(20, 52, q5); ++i;
	tripletList[i]=TripletT(19, 54, q5); ++i;
	tripletList[i]=TripletT(18, 56, q5); ++i;
	tripletList[i]=TripletT(17, 59, q5); ++i;
	tripletList[i]=TripletT(5, 68, q5); ++i;
	tripletList[i]=TripletT(4, 71, q5); ++i;
	tripletList[i]=TripletT(0, 79, q5); ++i;

	double q6= 0.5/glad*glbc*d22-0.5*glab/glad*d22-0.5*glac/glad*d22-0.5*glac/glad*d12+0.5/glad*glbc*d12-0.5*glab/glad*d12;
	tripletList[i]=TripletT(71, 18, q6); ++i;
	tripletList[i]=TripletT(70, 21, q6); ++i;
	tripletList[i]=TripletT(51, 36, q6); ++i;
	tripletList[i]=TripletT(50, 40, q6); ++i;
	tripletList[i]=TripletT(49, 42, q6); ++i;
	tripletList[i]=TripletT(47, 43, q6); ++i;
	tripletList[i]=TripletT(22, 54, q6); ++i;
	tripletList[i]=TripletT(21, 56, q6); ++i;
	tripletList[i]=TripletT(20, 58, q6); ++i;
	tripletList[i]=TripletT(19, 59, q6); ++i;
	tripletList[i]=TripletT(18, 61, q6); ++i;
	tripletList[i]=TripletT(6, 71, q6); ++i;
	tripletList[i]=TripletT(5, 73, q6); ++i;
	tripletList[i]=TripletT(4, 74, q6); ++i;
	tripletList[i]=TripletT(0, 82, q6); ++i;

	double q7= 1-0.5*glac/glad-0.5*glab/glad+0.5/glad*glbc;
	tripletList[i]=TripletT(71, 29, q7); ++i;
	tripletList[i]=TripletT(49, 50, q7); ++i;
	tripletList[i]=TripletT(48, 52, q7); ++i;
	tripletList[i]=TripletT(47, 54, q7); ++i;
	tripletList[i]=TripletT(46, 56, q7); ++i;
	tripletList[i]=TripletT(45, 59, q7); ++i;
	tripletList[i]=TripletT(21, 63, q7); ++i;
	tripletList[i]=TripletT(20, 65, q7); ++i;
	tripletList[i]=TripletT(18, 68, q7); ++i;
	tripletList[i]=TripletT(17, 71, q7); ++i;
	tripletList[i]=TripletT(5, 76, q7); ++i;
	tripletList[i]=TripletT(4, 79, q7); ++i;
	tripletList[i]=TripletT(0, 83, q7); ++i;

	double q8= -b1*a1-a2*b2;
	tripletList[i]=TripletT(71, 30, q8); ++i;
	tripletList[i]=TripletT(70, 35, q8); ++i;
	tripletList[i]=TripletT(51, 44, q8); ++i;
	tripletList[i]=TripletT(50, 49, q8); ++i;
	tripletList[i]=TripletT(49, 51, q8); ++i;
	tripletList[i]=TripletT(48, 53, q8); ++i;
	tripletList[i]=TripletT(47, 55, q8); ++i;
	tripletList[i]=TripletT(46, 57, q8); ++i;
	tripletList[i]=TripletT(45, 60, q8); ++i;
	tripletList[i]=TripletT(22, 62, q8); ++i;
	tripletList[i]=TripletT(21, 64, q8); ++i;
	tripletList[i]=TripletT(20, 66, q8); ++i;
	tripletList[i]=TripletT(19, 67, q8); ++i;
	tripletList[i]=TripletT(18, 69, q8); ++i;
	tripletList[i]=TripletT(17, 72, q8); ++i;
	tripletList[i]=TripletT(6, 75, q8); ++i;
	tripletList[i]=TripletT(5, 77, q8); ++i;
	tripletList[i]=TripletT(4, 80, q8); ++i;
	tripletList[i]=TripletT(0, 84, q8); ++i;

	double q9= -c2*a2-c1*a1;
	tripletList[i]=TripletT(71, 31, q9); ++i;
	tripletList[i]=TripletT(70, 36, q9); ++i;
	tripletList[i]=TripletT(51, 45, q9); ++i;
	tripletList[i]=TripletT(50, 50, q9); ++i;
	tripletList[i]=TripletT(49, 52, q9); ++i;
	tripletList[i]=TripletT(47, 56, q9); ++i;
	tripletList[i]=TripletT(46, 58, q9); ++i;
	tripletList[i]=TripletT(45, 61, q9); ++i;
	tripletList[i]=TripletT(22, 63, q9); ++i;
	tripletList[i]=TripletT(21, 65, q9); ++i;
	tripletList[i]=TripletT(19, 68, q9); ++i;
	tripletList[i]=TripletT(18, 70, q9); ++i;
	tripletList[i]=TripletT(17, 73, q9); ++i;
	tripletList[i]=TripletT(6, 76, q9); ++i;
	tripletList[i]=TripletT(5, 78, q9); ++i;
	tripletList[i]=TripletT(4, 81, q9); ++i;
	tripletList[i]=TripletT(0, 85, q9); ++i;

	double q10= -a1/glad*glbc*d1+a1*glac/glad*d1+glac/glad*a2*d2+a1*glab/glad*d1-1/glad*glbc*a2*d2+glab/glad*a2*d2;
	tripletList[i]=TripletT(71, 36, q10); ++i;
	tripletList[i]=TripletT(70, 40, q10); ++i;
	tripletList[i]=TripletT(51, 50, q10); ++i;
	tripletList[i]=TripletT(50, 54, q10); ++i;
	tripletList[i]=TripletT(49, 56, q10); ++i;
	tripletList[i]=TripletT(48, 58, q10); ++i;
	tripletList[i]=TripletT(47, 59, q10); ++i;
	tripletList[i]=TripletT(46, 61, q10); ++i;
	tripletList[i]=TripletT(21, 68, q10); ++i;
	tripletList[i]=TripletT(20, 70, q10); ++i;
	tripletList[i]=TripletT(19, 71, q10); ++i;
	tripletList[i]=TripletT(18, 73, q10); ++i;
	tripletList[i]=TripletT(17, 74, q10); ++i;
	tripletList[i]=TripletT(6, 79, q10); ++i;
	tripletList[i]=TripletT(5, 81, q10); ++i;
	tripletList[i]=TripletT(4, 82, q10); ++i;
	tripletList[i]=TripletT(0, 86, q10); ++i;

	double q11= a22+a12-0.5*glac/glad*a22-0.5*a12*glac/glad+0.5/glad*glbc*a22-0.5*a12*glab/glad+0.5*a12/glad*glbc-0.5*glab/glad*a22;
	tripletList[i]=TripletT(71, 50, q11); ++i;
	tripletList[i]=TripletT(70, 54, q11); ++i;
	tripletList[i]=TripletT(51, 63, q11); ++i;
	tripletList[i]=TripletT(49, 68, q11); ++i;
	tripletList[i]=TripletT(48, 70, q11); ++i;
	tripletList[i]=TripletT(47, 71, q11); ++i;
	tripletList[i]=TripletT(46, 73, q11); ++i;
	tripletList[i]=TripletT(45, 74, q11); ++i;
	tripletList[i]=TripletT(21, 76, q11); ++i;
	tripletList[i]=TripletT(20, 78, q11); ++i;
	tripletList[i]=TripletT(19, 79, q11); ++i;
	tripletList[i]=TripletT(18, 81, q11); ++i;
	tripletList[i]=TripletT(17, 82, q11); ++i;
	tripletList[i]=TripletT(6, 83, q11); ++i;
	tripletList[i]=TripletT(5, 85, q11); ++i;
	tripletList[i]=TripletT(4, 86, q11); ++i;
	tripletList[i]=TripletT(0, 87, q11); ++i;

	double q12= -glac/glad;
	tripletList[i]=TripletT(73, 3, q12); ++i;
	tripletList[i]=TripletT(72, 5, q12); ++i;
	tripletList[i]=TripletT(58, 11, q12); ++i;
	tripletList[i]=TripletT(57, 16, q12); ++i;
	tripletList[i]=TripletT(56, 17, q12); ++i;
	tripletList[i]=TripletT(55, 19, q12); ++i;
	tripletList[i]=TripletT(54, 21, q12); ++i;
	tripletList[i]=TripletT(53, 22, q12); ++i;
	tripletList[i]=TripletT(28, 35, q12); ++i;
	tripletList[i]=TripletT(27, 37, q12); ++i;
	tripletList[i]=TripletT(26, 40, q12); ++i;
	tripletList[i]=TripletT(25, 41, q12); ++i;
	tripletList[i]=TripletT(24, 42, q12); ++i;
	tripletList[i]=TripletT(23, 43, q12); ++i;
	tripletList[i]=TripletT(9, 54, q12); ++i;
	tripletList[i]=TripletT(8, 55, q12); ++i;
	tripletList[i]=TripletT(7, 59, q12); ++i;
	tripletList[i]=TripletT(1, 71, q12); ++i;

	double q13= -2;
	tripletList[i]=TripletT(73, 7, q13); ++i;
	tripletList[i]=TripletT(72, 12, q13); ++i;
	tripletList[i]=TripletT(58, 24, q13); ++i;
	tripletList[i]=TripletT(57, 29, q13); ++i;
	tripletList[i]=TripletT(56, 30, q13); ++i;
	tripletList[i]=TripletT(55, 32, q13); ++i;
	tripletList[i]=TripletT(54, 36, q13); ++i;
	tripletList[i]=TripletT(53, 37, q13); ++i;
	tripletList[i]=TripletT(52, 42, q13); ++i;
	tripletList[i]=TripletT(28, 44, q13); ++i;
	tripletList[i]=TripletT(27, 46, q13); ++i;
	tripletList[i]=TripletT(26, 50, q13); ++i;
	tripletList[i]=TripletT(25, 51, q13); ++i;
	tripletList[i]=TripletT(24, 52, q13); ++i;
	tripletList[i]=TripletT(23, 56, q13); ++i;
	tripletList[i]=TripletT(9, 63, q13); ++i;
	tripletList[i]=TripletT(8, 64, q13); ++i;
	tripletList[i]=TripletT(7, 68, q13); ++i;
	tripletList[i]=TripletT(1, 76, q13); ++i;

	double q14= c12+c22;
	tripletList[i]=TripletT(73, 9, q14); ++i;
	tripletList[i]=TripletT(72, 14, q14); ++i;
	tripletList[i]=TripletT(58, 26, q14); ++i;
	tripletList[i]=TripletT(57, 31, q14); ++i;
	tripletList[i]=TripletT(56, 32, q14); ++i;
	tripletList[i]=TripletT(55, 34, q14); ++i;
	tripletList[i]=TripletT(54, 38, q14); ++i;
	tripletList[i]=TripletT(53, 39, q14); ++i;
	tripletList[i]=TripletT(29, 45, q14); ++i;
	tripletList[i]=TripletT(28, 46, q14); ++i;
	tripletList[i]=TripletT(27, 48, q14); ++i;
	tripletList[i]=TripletT(26, 52, q14); ++i;
	tripletList[i]=TripletT(25, 53, q14); ++i;
	tripletList[i]=TripletT(23, 58, q14); ++i;
	tripletList[i]=TripletT(9, 65, q14); ++i;
	tripletList[i]=TripletT(8, 66, q14); ++i;
	tripletList[i]=TripletT(7, 70, q14); ++i;
	tripletList[i]=TripletT(1, 78, q14); ++i;

	double q15= 2*glac/glad;
	tripletList[i]=TripletT(73, 11, q15); ++i;
	tripletList[i]=TripletT(72, 16, q15); ++i;
	tripletList[i]=TripletT(58, 28, q15); ++i;
	tripletList[i]=TripletT(56, 35, q15); ++i;
	tripletList[i]=TripletT(55, 37, q15); ++i;
	tripletList[i]=TripletT(54, 40, q15); ++i;
	tripletList[i]=TripletT(53, 41, q15); ++i;
	tripletList[i]=TripletT(52, 43, q15); ++i;
	tripletList[i]=TripletT(28, 49, q15); ++i;
	tripletList[i]=TripletT(27, 51, q15); ++i;
	tripletList[i]=TripletT(26, 54, q15); ++i;
	tripletList[i]=TripletT(25, 55, q15); ++i;
	tripletList[i]=TripletT(24, 56, q15); ++i;
	tripletList[i]=TripletT(23, 59, q15); ++i;
	tripletList[i]=TripletT(8, 67, q15); ++i;
	tripletList[i]=TripletT(7, 71, q15); ++i;
	tripletList[i]=TripletT(1, 79, q15); ++i;

	double q16= -glac/glad*d12-glac/glad*d22;
	tripletList[i]=TripletT(73, 17, q16); ++i;
	tripletList[i]=TripletT(72, 21, q16); ++i;
	tripletList[i]=TripletT(58, 35, q16); ++i;
	tripletList[i]=TripletT(57, 40, q16); ++i;
	tripletList[i]=TripletT(56, 41, q16); ++i;
	tripletList[i]=TripletT(54, 43, q16); ++i;
	tripletList[i]=TripletT(29, 54, q16); ++i;
	tripletList[i]=TripletT(28, 55, q16); ++i;
	tripletList[i]=TripletT(27, 57, q16); ++i;
	tripletList[i]=TripletT(26, 59, q16); ++i;
	tripletList[i]=TripletT(25, 60, q16); ++i;
	tripletList[i]=TripletT(24, 61, q16); ++i;
	tripletList[i]=TripletT(9, 71, q16); ++i;
	tripletList[i]=TripletT(8, 72, q16); ++i;
	tripletList[i]=TripletT(7, 74, q16); ++i;
	tripletList[i]=TripletT(1, 82, q16); ++i;

	double q17= -glac/glad+1;
	tripletList[i]=TripletT(73, 28, q17); ++i;
	tripletList[i]=TripletT(56, 49, q17); ++i;
	tripletList[i]=TripletT(55, 51, q17); ++i;
	tripletList[i]=TripletT(54, 54, q17); ++i;
	tripletList[i]=TripletT(53, 55, q17); ++i;
	tripletList[i]=TripletT(52, 59, q17); ++i;
	tripletList[i]=TripletT(28, 62, q17); ++i;
	tripletList[i]=TripletT(27, 64, q17); ++i;
	tripletList[i]=TripletT(25, 67, q17); ++i;
	tripletList[i]=TripletT(24, 68, q17); ++i;
	tripletList[i]=TripletT(23, 71, q17); ++i;
	tripletList[i]=TripletT(8, 75, q17); ++i;
	tripletList[i]=TripletT(7, 79, q17); ++i;
	tripletList[i]=TripletT(1, 83, q17); ++i;

	double q18= -2*c2*a2-2*c1*a1;
	tripletList[i]=TripletT(73, 30, q18); ++i;
	tripletList[i]=TripletT(72, 36, q18); ++i;
	tripletList[i]=TripletT(58, 44, q18); ++i;
	tripletList[i]=TripletT(57, 50, q18); ++i;
	tripletList[i]=TripletT(56, 51, q18); ++i;
	tripletList[i]=TripletT(55, 53, q18); ++i;
	tripletList[i]=TripletT(54, 56, q18); ++i;
	tripletList[i]=TripletT(53, 57, q18); ++i;
	tripletList[i]=TripletT(52, 61, q18); ++i;
	tripletList[i]=TripletT(29, 63, q18); ++i;
	tripletList[i]=TripletT(28, 64, q18); ++i;
	tripletList[i]=TripletT(27, 66, q18); ++i;
	tripletList[i]=TripletT(26, 68, q18); ++i;
	tripletList[i]=TripletT(25, 69, q18); ++i;
	tripletList[i]=TripletT(24, 70, q18); ++i;
	tripletList[i]=TripletT(23, 73, q18); ++i;
	tripletList[i]=TripletT(9, 76, q18); ++i;
	tripletList[i]=TripletT(8, 77, q18); ++i;
	tripletList[i]=TripletT(7, 81, q18); ++i;
	tripletList[i]=TripletT(1, 85, q18); ++i;

	double q19= 2*a1*glac/glad*d1+2*glac/glad*a2*d2;
	tripletList[i]=TripletT(73, 35, q19); ++i;
	tripletList[i]=TripletT(72, 40, q19); ++i;
	tripletList[i]=TripletT(58, 49, q19); ++i;
	tripletList[i]=TripletT(57, 54, q19); ++i;
	tripletList[i]=TripletT(56, 55, q19); ++i;
	tripletList[i]=TripletT(55, 57, q19); ++i;
	tripletList[i]=TripletT(54, 59, q19); ++i;
	tripletList[i]=TripletT(53, 60, q19); ++i;
	tripletList[i]=TripletT(28, 67, q19); ++i;
	tripletList[i]=TripletT(27, 69, q19); ++i;
	tripletList[i]=TripletT(26, 71, q19); ++i;
	tripletList[i]=TripletT(25, 72, q19); ++i;
	tripletList[i]=TripletT(24, 73, q19); ++i;
	tripletList[i]=TripletT(23, 74, q19); ++i;
	tripletList[i]=TripletT(9, 79, q19); ++i;
	tripletList[i]=TripletT(8, 80, q19); ++i;
	tripletList[i]=TripletT(7, 82, q19); ++i;
	tripletList[i]=TripletT(1, 86, q19); ++i;

	double q20= -glac/glad*a22+a22+a12-a12*glac/glad;
	tripletList[i]=TripletT(73, 49, q20); ++i;
	tripletList[i]=TripletT(72, 54, q20); ++i;
	tripletList[i]=TripletT(58, 62, q20); ++i;
	tripletList[i]=TripletT(56, 67, q20); ++i;
	tripletList[i]=TripletT(55, 69, q20); ++i;
	tripletList[i]=TripletT(54, 71, q20); ++i;
	tripletList[i]=TripletT(53, 72, q20); ++i;
	tripletList[i]=TripletT(52, 74, q20); ++i;
	tripletList[i]=TripletT(28, 75, q20); ++i;
	tripletList[i]=TripletT(27, 77, q20); ++i;
	tripletList[i]=TripletT(26, 79, q20); ++i;
	tripletList[i]=TripletT(25, 80, q20); ++i;
	tripletList[i]=TripletT(24, 81, q20); ++i;
	tripletList[i]=TripletT(23, 82, q20); ++i;
	tripletList[i]=TripletT(9, 83, q20); ++i;
	tripletList[i]=TripletT(8, 84, q20); ++i;
	tripletList[i]=TripletT(7, 86, q20); ++i;
	tripletList[i]=TripletT(1, 87, q20); ++i;

	double q21= 0.5/glad*glbd-0.5-0.5*glab/glad;
	tripletList[i]=TripletT(75, 4, q21); ++i;
	tripletList[i]=TripletT(74, 5, q21); ++i;
	tripletList[i]=TripletT(62, 12, q21); ++i;
	tripletList[i]=TripletT(61, 16, q21); ++i;
	tripletList[i]=TripletT(60, 18, q21); ++i;
	tripletList[i]=TripletT(59, 21, q21); ++i;
	tripletList[i]=TripletT(34, 36, q21); ++i;
	tripletList[i]=TripletT(33, 38, q21); ++i;
	tripletList[i]=TripletT(32, 40, q21); ++i;
	tripletList[i]=TripletT(31, 42, q21); ++i;
	tripletList[i]=TripletT(30, 43, q21); ++i;
	tripletList[i]=TripletT(12, 54, q21); ++i;
	tripletList[i]=TripletT(11, 56, q21); ++i;
	tripletList[i]=TripletT(10, 59, q21); ++i;
	tripletList[i]=TripletT(2, 71, q21); ++i;

	double q22= glab/glad-1/glad*glbd;
	tripletList[i]=TripletT(75, 12, q22); ++i;
	tripletList[i]=TripletT(74, 16, q22); ++i;
	tripletList[i]=TripletT(62, 29, q22); ++i;
	tripletList[i]=TripletT(60, 36, q22); ++i;
	tripletList[i]=TripletT(59, 40, q22); ++i;
	tripletList[i]=TripletT(34, 50, q22); ++i;
	tripletList[i]=TripletT(33, 52, q22); ++i;
	tripletList[i]=TripletT(32, 54, q22); ++i;
	tripletList[i]=TripletT(31, 56, q22); ++i;
	tripletList[i]=TripletT(30, 59, q22); ++i;
	tripletList[i]=TripletT(11, 68, q22); ++i;
	tripletList[i]=TripletT(10, 71, q22); ++i;
	tripletList[i]=TripletT(2, 79, q22); ++i;

	double q23= d2*b2+b1*d1;
	tripletList[i]=TripletT(75, 13, q23); ++i;
	tripletList[i]=TripletT(74, 17, q23); ++i;
	tripletList[i]=TripletT(62, 30, q23); ++i;
	tripletList[i]=TripletT(61, 35, q23); ++i;
	tripletList[i]=TripletT(60, 37, q23); ++i;
	tripletList[i]=TripletT(59, 41, q23); ++i;
	tripletList[i]=TripletT(35, 49, q23); ++i;
	tripletList[i]=TripletT(34, 51, q23); ++i;
	tripletList[i]=TripletT(33, 53, q23); ++i;
	tripletList[i]=TripletT(32, 55, q23); ++i;
	tripletList[i]=TripletT(31, 57, q23); ++i;
	tripletList[i]=TripletT(30, 60, q23); ++i;
	tripletList[i]=TripletT(12, 67, q23); ++i;
	tripletList[i]=TripletT(11, 69, q23); ++i;
	tripletList[i]=TripletT(10, 72, q23); ++i;
	tripletList[i]=TripletT(2, 80, q23); ++i;

	double q24= -0.5*glab/glad*d22-0.5*glab/glad*d12+0.5/glad*glbd*d22+0.5/glad*glbd*d12-0.5*d22-0.5*d12;
	tripletList[i]=TripletT(75, 18, q24); ++i;
	tripletList[i]=TripletT(74, 21, q24); ++i;
	tripletList[i]=TripletT(62, 36, q24); ++i;
	tripletList[i]=TripletT(61, 40, q24); ++i;
	tripletList[i]=TripletT(60, 42, q24); ++i;
	tripletList[i]=TripletT(59, 43, q24); ++i;
	tripletList[i]=TripletT(35, 54, q24); ++i;
	tripletList[i]=TripletT(34, 56, q24); ++i;
	tripletList[i]=TripletT(33, 58, q24); ++i;
	tripletList[i]=TripletT(32, 59, q24); ++i;
	tripletList[i]=TripletT(31, 61, q24); ++i;
	tripletList[i]=TripletT(12, 71, q24); ++i;
	tripletList[i]=TripletT(11, 73, q24); ++i;
	tripletList[i]=TripletT(10, 74, q24); ++i;
	tripletList[i]=TripletT(2, 82, q24); ++i;

	double q25= -0.5*glab/glad+0.5/glad*glbd+0.5;
	tripletList[i]=TripletT(75, 29, q25); ++i;
	tripletList[i]=TripletT(60, 50, q25); ++i;
	tripletList[i]=TripletT(59, 54, q25); ++i;
	tripletList[i]=TripletT(34, 63, q25); ++i;
	tripletList[i]=TripletT(33, 65, q25); ++i;
	tripletList[i]=TripletT(31, 68, q25); ++i;
	tripletList[i]=TripletT(30, 71, q25); ++i;
	tripletList[i]=TripletT(11, 76, q25); ++i;
	tripletList[i]=TripletT(10, 79, q25); ++i;
	tripletList[i]=TripletT(2, 83, q25); ++i;

	double q26= -a2*b2-b1*a1;
	tripletList[i]=TripletT(75, 30, q26); ++i;
	tripletList[i]=TripletT(74, 35, q26); ++i;
	tripletList[i]=TripletT(62, 44, q26); ++i;
	tripletList[i]=TripletT(61, 49, q26); ++i;
	tripletList[i]=TripletT(60, 51, q26); ++i;
	tripletList[i]=TripletT(59, 55, q26); ++i;
	tripletList[i]=TripletT(35, 62, q26); ++i;
	tripletList[i]=TripletT(34, 64, q26); ++i;
	tripletList[i]=TripletT(33, 66, q26); ++i;
	tripletList[i]=TripletT(32, 67, q26); ++i;
	tripletList[i]=TripletT(31, 69, q26); ++i;
	tripletList[i]=TripletT(30, 72, q26); ++i;
	tripletList[i]=TripletT(12, 75, q26); ++i;
	tripletList[i]=TripletT(11, 77, q26); ++i;
	tripletList[i]=TripletT(10, 80, q26); ++i;
	tripletList[i]=TripletT(2, 84, q26); ++i;

	double q27= -a1/glad*glbd*d1+a1*glab/glad*d1+glab/glad*a2*d2-1/glad*glbd*a2*d2;
	tripletList[i]=TripletT(75, 36, q27); ++i;
	tripletList[i]=TripletT(74, 40, q27); ++i;
	tripletList[i]=TripletT(62, 50, q27); ++i;
	tripletList[i]=TripletT(61, 54, q27); ++i;
	tripletList[i]=TripletT(60, 56, q27); ++i;
	tripletList[i]=TripletT(59, 59, q27); ++i;
	tripletList[i]=TripletT(34, 68, q27); ++i;
	tripletList[i]=TripletT(33, 70, q27); ++i;
	tripletList[i]=TripletT(32, 71, q27); ++i;
	tripletList[i]=TripletT(31, 73, q27); ++i;
	tripletList[i]=TripletT(30, 74, q27); ++i;
	tripletList[i]=TripletT(12, 79, q27); ++i;
	tripletList[i]=TripletT(11, 81, q27); ++i;
	tripletList[i]=TripletT(10, 82, q27); ++i;
	tripletList[i]=TripletT(2, 86, q27); ++i;

	double q28= 0.5/glad*glbd*a22+0.5*a12/glad*glbd-0.5*glab/glad*a22-0.5*a12*glab/glad+0.5*a12+0.5*a22;
	tripletList[i]=TripletT(75, 50, q28); ++i;
	tripletList[i]=TripletT(74, 54, q28); ++i;
	tripletList[i]=TripletT(62, 63, q28); ++i;
	tripletList[i]=TripletT(60, 68, q28); ++i;
	tripletList[i]=TripletT(59, 71, q28); ++i;
	tripletList[i]=TripletT(34, 76, q28); ++i;
	tripletList[i]=TripletT(33, 78, q28); ++i;
	tripletList[i]=TripletT(32, 79, q28); ++i;
	tripletList[i]=TripletT(31, 81, q28); ++i;
	tripletList[i]=TripletT(30, 82, q28); ++i;
	tripletList[i]=TripletT(12, 83, q28); ++i;
	tripletList[i]=TripletT(11, 85, q28); ++i;
	tripletList[i]=TripletT(10, 86, q28); ++i;
	tripletList[i]=TripletT(2, 87, q28); ++i;

	double q29= -0.5*glac/glad+0.5*glcd/glad-0.5;
	tripletList[i]=TripletT(77, 4, q29); ++i;
	tripletList[i]=TripletT(76, 5, q29); ++i;
	tripletList[i]=TripletT(69, 12, q29); ++i;
	tripletList[i]=TripletT(68, 15, q29); ++i;
	tripletList[i]=TripletT(67, 16, q29); ++i;
	tripletList[i]=TripletT(66, 18, q29); ++i;
	tripletList[i]=TripletT(65, 21, q29); ++i;
	tripletList[i]=TripletT(64, 22, q29); ++i;
	tripletList[i]=TripletT(63, 23, q29); ++i;
	tripletList[i]=TripletT(43, 35, q29); ++i;
	tripletList[i]=TripletT(42, 36, q29); ++i;
	tripletList[i]=TripletT(41, 37, q29); ++i;
	tripletList[i]=TripletT(40, 38, q29); ++i;
	tripletList[i]=TripletT(39, 40, q29); ++i;
	tripletList[i]=TripletT(38, 41, q29); ++i;
	tripletList[i]=TripletT(37, 42, q29); ++i;
	tripletList[i]=TripletT(36, 43, q29); ++i;
	tripletList[i]=TripletT(16, 54, q29); ++i;
	tripletList[i]=TripletT(15, 55, q29); ++i;
	tripletList[i]=TripletT(14, 56, q29); ++i;
	tripletList[i]=TripletT(13, 59, q29); ++i;
	tripletList[i]=TripletT(3, 71, q29); ++i;

	double q30= glac/glad-glcd/glad;
	tripletList[i]=TripletT(77, 12, q30); ++i;
	tripletList[i]=TripletT(76, 16, q30); ++i;
	tripletList[i]=TripletT(69, 29, q30); ++i;
	tripletList[i]=TripletT(68, 32, q30); ++i;
	tripletList[i]=TripletT(66, 36, q30); ++i;
	tripletList[i]=TripletT(65, 40, q30); ++i;
	tripletList[i]=TripletT(64, 41, q30); ++i;
	tripletList[i]=TripletT(63, 42, q30); ++i;
	tripletList[i]=TripletT(43, 49, q30); ++i;
	tripletList[i]=TripletT(42, 50, q30); ++i;
	tripletList[i]=TripletT(41, 51, q30); ++i;
	tripletList[i]=TripletT(40, 52, q30); ++i;
	tripletList[i]=TripletT(39, 54, q30); ++i;
	tripletList[i]=TripletT(38, 55, q30); ++i;
	tripletList[i]=TripletT(37, 56, q30); ++i;
	tripletList[i]=TripletT(36, 59, q30); ++i;
	tripletList[i]=TripletT(15, 67, q30); ++i;
	tripletList[i]=TripletT(14, 68, q30); ++i;
	tripletList[i]=TripletT(13, 71, q30); ++i;
	tripletList[i]=TripletT(3, 79, q30); ++i;

	double q31= c1*d1+c2*d2;
	tripletList[i]=TripletT(77, 14, q31); ++i;
	tripletList[i]=TripletT(76, 18, q31); ++i;
	tripletList[i]=TripletT(69, 31, q31); ++i;
	tripletList[i]=TripletT(68, 34, q31); ++i;
	tripletList[i]=TripletT(67, 36, q31); ++i;
	tripletList[i]=TripletT(66, 38, q31); ++i;
	tripletList[i]=TripletT(65, 42, q31); ++i;
	tripletList[i]=TripletT(44, 50, q31); ++i;
	tripletList[i]=TripletT(43, 51, q31); ++i;
	tripletList[i]=TripletT(42, 52, q31); ++i;
	tripletList[i]=TripletT(41, 53, q31); ++i;
	tripletList[i]=TripletT(39, 56, q31); ++i;
	tripletList[i]=TripletT(38, 57, q31); ++i;
	tripletList[i]=TripletT(37, 58, q31); ++i;
	tripletList[i]=TripletT(36, 61, q31); ++i;
	tripletList[i]=TripletT(16, 68, q31); ++i;
	tripletList[i]=TripletT(15, 69, q31); ++i;
	tripletList[i]=TripletT(14, 70, q31); ++i;
	tripletList[i]=TripletT(13, 73, q31); ++i;
	tripletList[i]=TripletT(3, 81, q31); ++i;

	double q32= 0.5*glcd/glad*d12-0.5*glac/glad*d22-0.5*glac/glad*d12-0.5*d12-0.5*d22+0.5*glcd/glad*d22;
	tripletList[i]=TripletT(77, 18, q32); ++i;
	tripletList[i]=TripletT(76, 21, q32); ++i;
	tripletList[i]=TripletT(69, 36, q32); ++i;
	tripletList[i]=TripletT(68, 39, q32); ++i;
	tripletList[i]=TripletT(67, 40, q32); ++i;
	tripletList[i]=TripletT(66, 42, q32); ++i;
	tripletList[i]=TripletT(65, 43, q32); ++i;
	tripletList[i]=TripletT(44, 54, q32); ++i;
	tripletList[i]=TripletT(43, 55, q32); ++i;
	tripletList[i]=TripletT(42, 56, q32); ++i;
	tripletList[i]=TripletT(41, 57, q32); ++i;
	tripletList[i]=TripletT(40, 58, q32); ++i;
	tripletList[i]=TripletT(39, 59, q32); ++i;
	tripletList[i]=TripletT(38, 60, q32); ++i;
	tripletList[i]=TripletT(37, 61, q32); ++i;
	tripletList[i]=TripletT(16, 71, q32); ++i;
	tripletList[i]=TripletT(15, 72, q32); ++i;
	tripletList[i]=TripletT(14, 73, q32); ++i;
	tripletList[i]=TripletT(13, 74, q32); ++i;
	tripletList[i]=TripletT(3, 82, q32); ++i;

	double q33= -0.5*glac/glad+0.5+0.5*glcd/glad;
	tripletList[i]=TripletT(77, 29, q33); ++i;
	tripletList[i]=TripletT(68, 46, q33); ++i;
	tripletList[i]=TripletT(66, 50, q33); ++i;
	tripletList[i]=TripletT(65, 54, q33); ++i;
	tripletList[i]=TripletT(64, 55, q33); ++i;
	tripletList[i]=TripletT(63, 56, q33); ++i;
	tripletList[i]=TripletT(43, 62, q33); ++i;
	tripletList[i]=TripletT(42, 63, q33); ++i;
	tripletList[i]=TripletT(41, 64, q33); ++i;
	tripletList[i]=TripletT(40, 65, q33); ++i;
	tripletList[i]=TripletT(38, 67, q33); ++i;
	tripletList[i]=TripletT(37, 68, q33); ++i;
	tripletList[i]=TripletT(36, 71, q33); ++i;
	tripletList[i]=TripletT(15, 75, q33); ++i;
	tripletList[i]=TripletT(14, 76, q33); ++i;
	tripletList[i]=TripletT(13, 79, q33); ++i;
	tripletList[i]=TripletT(3, 83, q33); ++i;


	double q34= -c1*a1-c2*a2;
	tripletList[i]=TripletT(77, 31, q34); ++i;
	tripletList[i]=TripletT(76, 36, q34); ++i;
	tripletList[i]=TripletT(69, 45, q34); ++i;
	tripletList[i]=TripletT(68, 48, q34); ++i;
	tripletList[i]=TripletT(67, 50, q34); ++i;
	tripletList[i]=TripletT(66, 52, q34); ++i;
	tripletList[i]=TripletT(65, 56, q34); ++i;
	tripletList[i]=TripletT(64, 57, q34); ++i;
	tripletList[i]=TripletT(63, 58, q34); ++i;
	tripletList[i]=TripletT(44, 63, q34); ++i;
	tripletList[i]=TripletT(43, 64, q34); ++i;
	tripletList[i]=TripletT(42, 65, q34); ++i;
	tripletList[i]=TripletT(41, 66, q34); ++i;
	tripletList[i]=TripletT(39, 68, q34); ++i;
	tripletList[i]=TripletT(38, 69, q34); ++i;
	tripletList[i]=TripletT(37, 70, q34); ++i;
	tripletList[i]=TripletT(36, 73, q34); ++i;
	tripletList[i]=TripletT(16, 76, q34); ++i;
	tripletList[i]=TripletT(15, 77, q34); ++i;
	tripletList[i]=TripletT(14, 78, q34); ++i;
	tripletList[i]=TripletT(13, 81, q34); ++i;
	tripletList[i]=TripletT(3, 85, q34); ++i;

	double q35= glac/glad*a2*d2+a1*glac/glad*d1-glcd/glad*a2*d2-glcd*a1/glad*d1;
	tripletList[i]=TripletT(77, 36, q35); ++i;
	tripletList[i]=TripletT(76, 40, q35); ++i;
	tripletList[i]=TripletT(69, 50, q35); ++i;
	tripletList[i]=TripletT(68, 53, q35); ++i;
	tripletList[i]=TripletT(67, 54, q35); ++i;
	tripletList[i]=TripletT(66, 56, q35); ++i;
	tripletList[i]=TripletT(65, 59, q35); ++i;
	tripletList[i]=TripletT(64, 60, q35); ++i;
	tripletList[i]=TripletT(63, 61, q35); ++i;
	tripletList[i]=TripletT(43, 67, q35); ++i;
	tripletList[i]=TripletT(42, 68, q35); ++i;
	tripletList[i]=TripletT(41, 69, q35); ++i;
	tripletList[i]=TripletT(40, 70, q35); ++i;
	tripletList[i]=TripletT(39, 71, q35); ++i;
	tripletList[i]=TripletT(38, 72, q35); ++i;
	tripletList[i]=TripletT(37, 73, q35); ++i;
	tripletList[i]=TripletT(36, 74, q35); ++i;
	tripletList[i]=TripletT(16, 79, q35); ++i;
	tripletList[i]=TripletT(15, 80, q35); ++i;
	tripletList[i]=TripletT(14, 81, q35); ++i;
	tripletList[i]=TripletT(13, 82, q35); ++i;
	tripletList[i]=TripletT(3, 86, q35); ++i;

	double q36= 0.5*a12+0.5*a22-0.5*glac/glad*a22+0.5*glcd*a12/glad-0.5*a12*glac/glad+0.5*glcd/glad*a22;
	tripletList[i]=TripletT(77, 50, q36); ++i;
	tripletList[i]=TripletT(76, 54, q36); ++i;
	tripletList[i]=TripletT(69, 63, q36); ++i;
	tripletList[i]=TripletT(68, 66, q36); ++i;
	tripletList[i]=TripletT(66, 68, q36); ++i;
	tripletList[i]=TripletT(65, 71, q36); ++i;
	tripletList[i]=TripletT(64, 72, q36); ++i;
	tripletList[i]=TripletT(63, 73, q36); ++i;
	tripletList[i]=TripletT(43, 75, q36); ++i;
	tripletList[i]=TripletT(42, 76, q36); ++i;
	tripletList[i]=TripletT(41, 77, q36); ++i;
	tripletList[i]=TripletT(40, 78, q36); ++i;
	tripletList[i]=TripletT(39, 79, q36); ++i;
	tripletList[i]=TripletT(38, 80, q36); ++i;
	tripletList[i]=TripletT(37, 81, q36); ++i;
	tripletList[i]=TripletT(36, 82, q36); ++i;
	tripletList[i]=TripletT(16, 83, q36); ++i;
	tripletList[i]=TripletT(15, 84, q36); ++i;
	tripletList[i]=TripletT(14, 85, q36); ++i;
	tripletList[i]=TripletT(13, 86, q36); ++i;
	tripletList[i]=TripletT(3, 87, q36);

	typedef SparseMatrix<double> SparseT;	//Sparse matrix type
	SparseT mM(78,88);
	mM.setFromTriplets(tripletList.begin(), tripletList.end());

	//Solve the equation system
	SparseLU<SparseT> luEngine;
	luEngine.compute(mM.leftCols(78));

	if(luEngine.info()!=Eigen::Success)
		return mA;

	Matrix<double, 78, 10> mB=mM.rightCols(10);
	Matrix<double, 78, 10> mR=luEngine.solve(mB);

	mA=ActionMatrixT::Zero();
	(*mA)(0,1)=1; (*mA)(1,5)=1; (*mA)(2,6)=1; (*mA)(3,7)=1; (*mA)(4,8)=1;
	for(size_t c=5; c<10; ++c)
		mA->row(c)=-mR.row(79-c).reverse();

	return mA;
}	//auto BuildActionMatrix(const array<RealT,6>& edgeLengths, const Coordinate2DT& x1, const Coordinate2DT& x2, const Coordinate2DT& x3, const Coordinate2DT& x4) -> ActionMatrixT

/********** P4PMinimalDifferenceC **********/
/**
 * @brief Computes the difference between to minimally-represented pose transformations
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector. [Focal length difference; Rotation vector(3); origin shift(3) ]
 * @remarks Difference: orientation and the origin of the transformation that takes \c op2 to \c op1 (6).
 */
auto P4PMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	typedef P3PSolverC::minimal_model_type MinimalPoseT;

	result_type output;
	output[0]=get<P4PSolverC::iFocalLength>(op1) - get<P4PSolverC::iFocalLength>(op2);

	PoseMinimalDifferenceC p3pDif;
	output.tail(6)=p3pDif(MinimalPoseT(get<P4PSolverC::iPosition>(op1), get<P4PSolverC::iOrientation>(op1)), MinimalPoseT(get<P4PSolverC::iPosition>(op2), get<P4PSolverC::iOrientation>(op2)) );

	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

