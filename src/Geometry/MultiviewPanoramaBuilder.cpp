/**
 * @file MultiviewPanoramaBuilder.cpp Implementation of \c PanoramaBuilderC
 * @author Evren Imre
 * @date 25 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "MultiviewPanoramaBuilder.h"

namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the input parameters
 * @param[in] correspondenceStack Correspondences
 * @param[in] nCameras Number of cameras
 * @param[in, out] parameters Parameters
 * @throws invalid_argument If any input parameters is invalid
 */
void MultiviewPanoramaBuilderC::ValidateInput(const CorrespondenceStackT& correspondenceStack, size_t nCameras, MultiviewPanoramaBuilderParametersC& parameters)
{
	// Are there any extra image pairs?
	priority_queue<unsigned int> indexList;
	for_each(correspondenceStack, [&](const CorrespondenceStackT::value_type& current){ indexList.push(get<0>(current.first));
																							 indexList.push(get<1>(current.first));} );
	if(indexList.top()>=nCameras)
		throw(invalid_argument(string("MultiviewPanoramaBuilderC::ValidateInput: No corresponding camera for the image ") + lexical_cast<string>(indexList.top()) ));

	if(parameters.maxAngularDeviation<0 || parameters.maxAngularDeviation >= 2*pi<double>())
		throw(invalid_argument(string("MultiviewPanoramaBuilderC::ValidateInput: maxAngularDeviation must be in [0,2*pi). Value: ") +lexical_cast<string>(parameters.maxAngularDeviation)) );

	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("MultiviewPanoramaBuilderC::ValidateInput: noiseVariance must be positive. Value: ") +lexical_cast<string>(parameters.noiseVariance)) );

	if(parameters.pReject<0 || parameters.pReject>1)
		throw(invalid_argument(string("MultiviewPanoramaBuilderC::ValidateInput: pReject must be in [0,1]. Value: ") +lexical_cast<string>(parameters.pReject)) );

	if(parameters.binSize<0)
		throw(invalid_argument(string("MultiviewPanoramaBuilderC::ValidateInput: binSize must be non-negative Value: ") + lexical_cast<string>(parameters.binSize)) );
}	//void ValidateInput(const CorrespondenceStackT& correspondenceStack, size_t nCameras, MultiviewPanoramaBuilderParametersC& parameters)

/**
 * @brief Processes the cameras
 * @param[in] cameraList Camera list
 * @return A tuple: Pairwise homographies; calibration parameters
 */
auto MultiviewPanoramaBuilderC::ProcessCameras(const vector<CameraMatrixT>& cameraList) -> tuple< map<IndexPairT, Homography2DT>, vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT>>>
{
	size_t nCameras=cameraList.size();
	vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT>> calibrationParameters; calibrationParameters.reserve(nCameras);
	vector<Matrix3d> mKRList; mKRList.reserve(nCameras);
	for(const auto& current : cameraList)
	{
		RotationMatrix3DT mR;
		IntrinsicCalibrationMatrixT mK;
		tie(mK, ignore, mR)=DecomposeCamera(current);

		calibrationParameters.emplace_back(mK, mR);
		mKRList.push_back(mK*mR);
	}	//	for(const auto& current : cameraList)

	map<IndexPairT, Homography2DT> homographyList;
	for(size_t c1=0; c1<nCameras; ++c1)
	{
		Matrix3d mKR1i= mKRList[c1].inverse();
		for(size_t c2=c1+1; c2<nCameras; ++c2)
			homographyList.emplace(IndexPairT(c1,c2), mKRList[c2]*mKR1i);
	}

	return make_tuple(homographyList, calibrationParameters);
}	//tuple< map<IndexPairT, Homography2DT>, vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT>  >  > ProcessCameras(const vector<CameraMatrixT>& cameraList)

/**
 * @brief Filters the image correspondences
 * @param[in] correspondenceStack Correspondences to be filtered
 * @param[in] coordinateStack Coordinates
 * @param[in] homographyList Homographies for the camera pairs
 * @param[in] parameters Parameters
 * @return Filtered correspondences
 */
auto MultiviewPanoramaBuilderC::FilterCorrespondences2D(const PairwiseViewT& correspondenceStack, const CoordinateStackT& coordinateStack, const map<IndexPairT, Homography2DT>& homographyList, const MultiviewPanoramaBuilderParametersC& parameters) -> PairwiseViewT
{
	double inlierTh=SymmetricTransferErrorConstraint2DT::error_type::ComputeOutlierThreshold(parameters.pReject, parameters.noiseVariance);

	PairwiseViewT output;

	//For each correspondence list
	for(const auto& currentList : correspondenceStack)
	{
		auto itH=homographyList.find(IndexPairT(get<0>(currentList.first), get<1>(currentList.first)));	//Guaranteed to exist
		SymmetricTransferErrorConstraint2DT validator(SymmetricTransferErrorConstraint2DT::error_type(itH->second), inlierTh);

		CorrespondenceFusionC::correspondence_container_type inliers;
		const CoordinateStackT::value_type& pList1=coordinateStack[get<0>(currentList.first)];
		const CoordinateStackT::value_type& pList2=coordinateStack[get<1>(currentList.first)];

		for(const auto& current : currentList.second )
			if(validator(pList1[current.left], pList2[current.right]))
				inliers.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(current.left, current.right, current.info) );

		if(!inliers.empty())
			output[currentList.first].swap(inliers);
	}	//for(const auto& current : correspondenceStack)

	return output;
}	//PairwiseViewT FilterCorrespondences2D(const PairwiseViewT& correspondenceStack, const MultimodelGeometryEstimationPipelineParametersC& parameters)

/**
 * @brief Estimates the 3D direction vectors from image measurements
 * @param[in] tracks Image feature tracks
 * @param[in] coordinateStack Image coordinates
 * @param[in] calibrationParameters Intrinsic and extrinsic calibration parameters
 * @param[in] parameters Parameters
 * @return 3D direction vectors
 */
auto MultiviewPanoramaBuilderC::EstimateDirections(const TrackListT& tracks, const CoordinateStackT& coordinateStack, const vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> >& calibrationParameters, const MultiviewPanoramaBuilderParametersC& parameters) -> vector<Coordinate3DT>
{
	//Invert the camera matrices
	size_t nCameras=calibrationParameters.size();
	vector<Matrix3d> backprojectors(nCameras);
	for(size_t c=0; c<nCameras; ++c)
		backprojectors[c] = (get<0>(calibrationParameters[c]) * get<1>(calibrationParameters[c])).inverse();

	size_t nTracks=tracks.size();
	vector<Coordinate3DT> output; output.reserve(nTracks);

	//For each track

	constexpr unsigned int iSourceId=CorrespondenceFusionC::iSourceId;
	constexpr unsigned int iElementId=CorrespondenceFusionC::iElementId;

	for(const auto& currentTrack : tracks)
	{
		//For each measurement, compute the median values
		multiset<double> azimuthList;
		multiset<double> inclinationList;
		list<Vector3d> backprojected;

		for(const auto& current : currentTrack)
		{
			size_t iCamera=get<iSourceId>(current);
			size_t iPoint=get<iElementId>(current);

			HCoordinate2DT warped=backprojectors[iCamera] * coordinateStack[iCamera][iPoint].homogeneous();
			Vector3d spherical=CartesianToSpherical(warped);
			azimuthList.insert(spherical[1]);
			inclinationList.insert(spherical[2]);
			backprojected.push_back(spherical);
		}	//for(const auto& current : currentTrack)

		size_t nMeasurements=currentTrack.size();
		size_t iMedian=floor(nMeasurements/2);
		double medianAzimuth= nMeasurements>2 ? *next(azimuthList.begin(), iMedian) : 0.5 * (*azimuthList.begin() + *azimuthList.rbegin());
		double medianInclination= nMeasurements>2 ? *next(inclinationList.begin(), iMedian) : 0.5*(*inclinationList.begin() + *inclinationList.rbegin());

		//Then, compute the mean from the inliers to the median
		double sinInclination=sin(medianInclination);
		double cosInclination=cos(medianInclination);
		double azimuthAcc=0;
		double inclinationAcc=0;
		size_t nInliers=0;
		for(const auto& current : backprojected)
		{
			//Great circle distance
			double angularDistance = acos (cos(current[2])*cosInclination + sin(current[2])*sinInclination * cos(current[1]-medianAzimuth));

			if(angularDistance > parameters.maxAngularDeviation)
				continue;

			azimuthAcc+=current[1];
			inclinationAcc+=current[2];
			++nInliers;
		}	//for(const auto& current : backprojected)

		//Estimated direction in spherical coordinates
		Vector3d sphericalEst(1, azimuthAcc/nInliers, inclinationAcc/nInliers);

		//Convert to cartesian coordinates
		output.push_back(SphericalToCartesian(sphericalEst));
	}	//for(const auto& current : tracks)

	return output;
}	//tuple<vector<Coordinate3DT>,  TrackListT> EstimateDirections(const PairwiseViewT& correspondences, const CoordinateStackT& coordinateStack, const vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> >& calibrationParameters)

/**
 * @brief Filters the estimated directions and returns the inlier track points
 * @param[in] directions Direction vectors to be filtered
 * @param[in] tracks Tracks to be filtered
 * @param[in] coordinateStack Coordinates
 * @param[in] calibrationParameters Camera parameters
 * @param[in] parameters Parameters
 * @return A tuple: inlier direction vectors; inlier track points
 */
auto MultiviewPanoramaBuilderC::FilterDirections(const vector<Coordinate3DT>& directions, const TrackListT& tracks, const CoordinateStackT& coordinateStack, const vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> >& calibrationParameters, const MultiviewPanoramaBuilderParametersC& parameters) -> tuple<vector<Coordinate3DT>, TrackListT>
{
	//Constraints

	double inlierTh=ReprojectionErrorConstraintT::error_type::ComputeOutlierThreshold(parameters.pReject, parameters.noiseVariance);

	size_t nCameras=calibrationParameters.size();
	Coordinate3DT origin(0,0,0);
	vector<ReprojectionErrorConstraintT> validators; validators.reserve(nCameras);
	for(size_t c=0; c<nCameras; ++c)
		validators.emplace_back(ReprojectionErrorConstraintT::error_type(ComposeCameraMatrix( get<0>(calibrationParameters[c]), origin, get<1>(calibrationParameters[c]))), inlierTh);

	//Filtering

	size_t nTracks=tracks.size();
	tuple<vector<Coordinate3DT>, TrackListT> output;
	get<0>(output).reserve(nTracks);
	get<1>(output).reserve(nTracks);

	constexpr unsigned int iCamera=CorrespondenceFusionC::iSourceId;
	constexpr unsigned int iPoint=CorrespondenceFusionC::iElementId;

	for(size_t c=0; c<nTracks; ++c)
	{
		//If too few measurements, skip
		if(tracks[c].size() < parameters.minObservationCount)
			continue;

		//Inlier check
		TrackT inliers;
		for(const auto& current : tracks[c])
			if( validators[get<iCamera>(current)](directions[c], coordinateStack[get<iCamera>(current)][get<iPoint>(current)] )  )
				inliers.insert(current);

		//If too few inliers, skip
		if(inliers.size() < parameters.minObservationCount)
			continue;

		get<0>(output).push_back(directions[c]);
		get<1>(output).push_back(inliers);
	}	//for(size_t c=0; c<nTracks; ++c)

	get<0>(output).shrink_to_fit();
	get<1>(output).shrink_to_fit();

	return output;
}	//tuple<vector<Coordinate3DT>, TrackListT> FilterDirections(const vector<Coordinate3DT>& directions, const PairwiseViewT& correspondences, const CoordinateStackT& coordinateStack, const vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> >& calibrationParameters, const MultiviewPanoramaBuilderParametersC& parameters)

/**
 * @brief Decimates a set of directions
 * @param[in] directions Directions
 * @param[in] tracks 2D tracks
 * @param[in] parameters Parameters
 * @return A tuple: Decimated directions and tracks
 * @pre \c directions is not empty
 */
auto MultiviewPanoramaBuilderC::Decimate(const vector<Coordinate3DT>& directions, const TrackListT& tracks, const MultiviewPanoramaBuilderParametersC& parameters) -> tuple<vector<Coordinate3DT>, TrackListT>
{
	//TODO This function should unwrap the unit sphere and work in 2D
	assert(!directions.empty());

	//Coordinate statistics for the quantiser
	CoordinateStatistics3DT::ArrayD2 extent=*CoordinateStatistics3DT::ComputeExtent(directions);

	//Quantise
	CoordinateStatistics3DT::ArrayD binSize(parameters.binSize, parameters.binSize, parameters.binSize);
	vector<unsigned int> quantised=CoordinateTransformations3DT::Bucketise(directions, binSize, extent);

	//From each bin, select the element with the best support
	size_t nElement=directions.size();
	map<unsigned int, pair<size_t, size_t> > representatives;
	for(size_t c=0; c<nElement; ++c)
	{
		size_t support=tracks[c].size();
		auto it=representatives.emplace(quantised[c], make_pair(c, support) );
		if(!it.second && it.first->second.second < support)
			representatives[quantised[c]]={c, support};
	}	//for(size_t c=0; c<nElement; ++c)

	//Survivors
	size_t nSurvivors=representatives.size();
	tuple<vector<Coordinate3DT>, TrackListT> output;
	get<0>(output).reserve(nSurvivors);
	get<1>(output).reserve(nSurvivors);

	for(const auto& current : representatives)
	{
		get<0>(output).push_back(directions[current.second.first]);
		get<1>(output).push_back(tracks[current.second.first]);
	}

	return output;
}	//tuple<vector<Coordinate3DT>, TrackListT> Decimate(const vector<Coordinate3DT>& directions, const TrackListT& tracks, const MultiviewPanoramaBuilderParametersC& parameters)

/**
 * @brief Runs the algorithm
 * @param[out] directionVectors A set of normalised direction vectors (or, equivalently, a 3D point cloud on a unit sphere)
 * @param[out] inliers Inlier image measurements, in the form of tracks. Each element is a set of track points for the corresponding 3D point
 * @param[in] correspondenceStack Image correspondences. [Pair id; correspondence list]. Pair id holds indices into \c coordinateStack
 * @param[in] coordinateStack Image feature coordinates. Each element is a list of coordinates in an image
 * @param[in] cameraList The camera matrices corresponding to the images. The camera centre component is ignore
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @pre For each element of \c coordinateStack , there is a corresponding element in \c cameraList
 */
MultiviewPanoramaBuilderDiagnosticsC MultiviewPanoramaBuilderC::Run(vector<Coordinate3DT>& directionVectors, TrackListT& inliers, const CorrespondenceStackT& correspondenceStack, const CoordinateStackT& coordinateStack, const vector<CameraMatrixT>& cameraList,  MultiviewPanoramaBuilderParametersC parameters)
{
	//Preconditions
	assert(coordinateStack.size()==cameraList.size());

	size_t nCameras=cameraList.size();
	ValidateInput(correspondenceStack, nCameras, parameters);

	MultiviewPanoramaBuilderDiagnosticsC diagnostics;
	diagnostics.flagSuccess=false;

	//Correspondence fusion
	PairwiseViewT observed;
	PairwiseViewT implied;
	std::tie(ignore, observed, implied)=CorrespondenceFusionC::Run(correspondenceStack, parameters.minObservationCount, true);

	//Fuse the observed and the implied correspondences
	PairwiseViewT spliced=CorrespondenceFusionC::SplicePairwiseViews(observed, implied);

	//Process the cameras
	vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> > cameraParameters;
	map<IndexPairT, Homography2DT> homographyList;
	tie(homographyList, cameraParameters)=ProcessCameras(cameraList);

	//Eliminate the outliers wrt symmetric transfer distance
	if(parameters.flagFiltering)
		spliced=FilterCorrespondences2D(spliced, coordinateStack, homographyList, parameters);

	//Estimate the point directions
	TrackListT tracks=CorrespondenceFusionC::ConvertToUnified(spliced);

	//Filter out the empty tracks. This invalidates the cluster ids in spliced
	TrackListT filteredTracks; filteredTracks.reserve(tracks.size());
	for(auto& current : tracks)
		if(!current.empty())
			filteredTracks.push_back(current);

	vector<Coordinate3DT> directions=EstimateDirections(filteredTracks, coordinateStack, cameraParameters, parameters);

	//Eliminate the outliers wrt reprojection error
	if(parameters.flagFiltering)
		tie(directions, tracks)=FilterDirections(directions, filteredTracks, coordinateStack, cameraParameters, parameters);

	//Decimation
	if(parameters.binSize>0 && !directions.empty())
		tie(directions, tracks)=Decimate(directions, tracks, parameters);

	directionVectors.swap(directions);
	inliers.swap(tracks);

	diagnostics.flagSuccess=true;
	diagnostics.nPoints=directionVectors.size();

	return diagnostics;
}	//MultiviewPanoramaBuilderDiagnosticsC Run(vector<Coordinate3DT>& result, const CorrespondenceStackT& correspondenceStack, const CoordinateStackT& coordinateStack, const vector<CameraMatrixT>& cameraList,  MultiviewPanoramaBuilderParametersC parameters)

}	//GeometryN
}	//SeeSawN
