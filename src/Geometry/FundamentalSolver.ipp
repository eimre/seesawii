/**
 * @file FundamentalSolver.ipp Implementation of 7- and 8-point fundamental matrix solvers
 * @author Evren Imre
 * @date 3 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FUNDAMENTAL_SOLVER_IPP_7102902
#define FUNDAMENTAL_SOLVER_IPP_7102902

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/range/numeric.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <list>
#include <array>
#include <complex>
#include <tuple>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <set>
#include <climits>
#include "GeometrySolverBase.h"
#include "DLT.h"
#include "Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/Polynomial.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Numeric/Complexity.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::math::pow;
using Eigen::Matrix;
using Eigen::JacobiSVD;
using std::list;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::array;
using std::complex;
using std::sqrt;
using std::max;
using std::fabs;
using std::is_same;
using std::rotate;
using std::set;
using std::numeric_limits;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::DLTC;
using SeeSawN::GeometryN::DLTProblemC;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::NumericN::CubicPolynomialRoots;
using SeeSawN::NumericN::MakeRankN;
using SeeSawN::NumericN::ComplexityEstimatorC;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;

//Forward declarations
struct FundamentalMinimalDifferenceC;

/**
 * @brief 7-point fundamental matrix estimator
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.281
 * @remarks Distance measure: 1-Absolute value of the dot-product between the unit vectors representing the fundamental matrices. Range=[0,1]
 * @remarks Degeneracies: 6 or more points related by a homography, e.g., planar structure or no translation. No degeneracy detection, as it is expensive. However, a degenerate configuration yields a valid (but arbitrary) fundamental matrix.
 * @remarks Minimal parameterisation:
 *	- \f$ \mathbf{F} = \mathbf{U S V^T} \f$. When \f$ \mathbf{F} \f$ is unit-norm, the sum of singular values is 1. Therefore, \f$ \mathbf{F} \f$ can be represented by two rotations (\f$ \mathbf{U, V}\f$ and one scalar (second singular value).
 *	The third singular value is 0
 * 	- Invariant to scale, but not to a sign change!
 * @remarks DoF:
 * 	- General motion=7
 *  - Pure translation=2
 *  - Planar motion=6
 * @remarks Nonminimal sets: DLT returns the singular vectors corresponding to the two smallest singular values
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class Fundamental7SolverC : public GeometrySolverBaseC<Fundamental7SolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 7, true, 3, 9, 7>
{
	private:

		typedef GeometrySolverBaseC<Fundamental7SolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 7, true, 3, 9, 7> BaseT;	///< Type of the base

		typedef Matrix<double, BaseT::nDOF, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 3x3 matrix
		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::EpipolarTag, BaseT::sGenerator, 0, 3> DLTProblemT;	///< DLT problem for 7-point fundamental matrix estimation
																																												//Basis solutions does not have to be rank-2
																																									//No rank condition on the basis vectors!
		typedef MultivariateGaussianC<FundamentalMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		/** @name Implementation details */ ///@{
		static list<ModelT> ModelFromBasis(const list<ModelT>& basis);	///< Given a basis, computes the corresponding fundamental matrices
		///@}

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		typedef typename BaseT::real_type distance_type;	///< Type of the distance between two models

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the model that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<QuaternionT, QuaternionT, RealT> minimal_model_type;	///< A minimally parameterised model. [Rotation on the left; Rotation on the right; Second singular value]
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class FundamentalRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of fundamental matrices
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iRotationL=0;	///< Index of the left rotation
		static constexpr unsigned int iRotationR=1;	///< Index of the right rotation
		static constexpr unsigned int iScalar=2;	///< Index of the second singular value
		//@}

};	//Fundamental7Solver

/**
 * @brief 8-point fundamental matrix estimator
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, Algorithm 11.2
 * @remarks Distance measure: 1-Absolute value of the dot-product between the unit vectors representing the fundamental matrices. Range=[0,1]
 * @remarks Degeneracies: 6 or more points related by a homography, e.g., planar structure or no translation. No degeneracy detection, as it is expensive. However, a degenerate configuration yields a valid (but arbitrary) fundamental matrix.
 * @remarks Minimal parameterisation:
 *	- \f$ \mathbf{F} = \mathbf{U S V^T} \f$. When \f$ \mathbf{F} \f$ is unit-norm, the sum of singular values is 1. Therefore, \f$ \mathbf{F} \f$ can be represented by two rotations (\f$ \mathbf{U, V}\f$ and one scalar (second singular value).
 *	The third singular value is 0
 * 	- Invariant to scale, but not to a sign change!
 * @remarks DoF:
 * 	- General motion=7
 *  - Pure translation=2
 *  - Planar motion=6
 * @remarks Only the functions that are different from \c Fundamental7SolverC are tested
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class Fundamental8SolverC : public GeometrySolverBaseC<Fundamental8SolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 8, true, 1, 9, 7>
{
	private:

		typedef GeometrySolverBaseC<Fundamental8SolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 8, true, 1, 9, 7> BaseT;	///< Type of the base

		typedef Matrix<double, 8, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 3x3 matrix
		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::EpipolarTag, BaseT::sGenerator, 2, 2> DLTProblemT;	///< DLT problem for 8-point fundamental matrix estimation

		typedef Fundamental7SolverC::uncertainty_type GaussianT;	///< Type of the Gaussian for the sample statistics

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		typedef BaseT::real_type distance_type;	///< Type of the distance between two models

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the model that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=BaseT::sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef Fundamental7SolverC::minimal_model_type minimal_model_type;	///< A minimally parameterised model. [Rotation on the left; Rotation on the right; Second singular value]
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class FundamentalRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of fundamental matrices
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iRotationL=Fundamental7SolverC::iRotationL;	///< Index of the left rotation
		static constexpr unsigned int iRotationR=Fundamental7SolverC::iRotationR;	///< Index of the right rotation
		static constexpr unsigned int iScalar=Fundamental7SolverC::iScalar;	///< Index of the second singular value
		//@}
};	//class Fundamental8Solver

/**
 * @brief Difference functor for minimally-represented fundamental matrices
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct FundamentalMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef Fundamental7SolverC::minimal_model_type first_argument_type;
	typedef Fundamental7SolverC::minimal_model_type second_argument_type;
	typedef Matrix<Fundamental7SolverC::real_type, Fundamental7SolverC::DoF(), 1> result_type;	///< Difference in vector form: [Left rotation vector, right rotation vector, 2nd singular value]

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between to minimally-represented 2D homographies
	//@}
};	//struct Homography2DMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/********** Fundamental7SolverC **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy argument for satisfying the concept requirements
 * @pre \c model has non-zero norm
 * @return Minimal form
 */
template <class DummyRangeT>
auto Fundamental7SolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	//Preconditions
	assert(model.norm()!=0);

	JacobiSVD<ModelT, Eigen::ColPivHouseholderQRPreconditioner> svd(model, Eigen::ComputeFullU | Eigen::ComputeFullV);

	//A rotation matrix has positive determinant
	int signL=1;
	if(svd.matrixU().determinant() < 0)
		signL=-1;

	int signR=1;
	if(svd.matrixV().determinant() < 0)
		signR=-1;

	auto sv=svd.singularValues().normalized();

	return make_tuple( QuaternionT(signL*svd.matrixU()), QuaternionT(signR*svd.matrixV()), signR*signL*sv[1]);
}	//minimal_model_type MakeMinimal(const ModelT& model)

/**
 * @brief Computes the sample statistics for a set of fundamental matrices
 * @tparam FundamentalRangeT A range of fundamental matrices
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] mFRange Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c FundamentalRangeT is a forward range of elements of type \c EpipolarMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c mFRange should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c mFRange has 7 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class FundamentalRangeT, class WeightRangeT, class DummyRangeT>
auto Fundamental7SolverC::ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FundamentalRangeT>));
	static_assert(is_same<typename range_value<FundamentalRangeT>::type, EpipolarMatrixT>::value, "Fundamental7SolverC::ComputeSampleStatistics: FundamentalRangeT must have elements of type EpipolarMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "Fundamental7SolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(mFRange)==boost::distance(wMean));
	assert(boost::distance(mFRange)==boost::distance(wCovariance));

	//Decompose the representation
	unsigned int nSample=boost::distance(mFRange);
	vector<QuaternionT> qList1(nSample);
	vector<QuaternionT> qList2(nSample);
	vector<RealT> sList(nSample);
	size_t index=0;
	for(const auto& current : mFRange)
	{
		tie(qList1[index], qList2[index], sList[index])=MakeMinimal(current);
		sList[index]=fabs(sList[index]);	//Sign convention
		++index;
	}	//for(const auto& current : homographies)

	//Mean

	minimal_model_type meanModel;

	//Scalars
	get<iScalar>(meanModel)=boost::inner_product(sList, wMean, (RealT)0);

	//Left rotation
	Matrix<RealT, 3, 3> mCovQ1;	//Dummy
	tie(get<iRotationL>(meanModel), mCovQ1)=ComputeQuaternionSampleStatistics(qList1, wMean, wCovariance);

	//Right rotation
	Matrix<RealT, 3, 3> mCovQ2;	//Dummy
	tie(get<iRotationR>(meanModel), mCovQ2)=ComputeQuaternionSampleStatistics(qList2, wMean, wCovariance);

	//Covariance
	FundamentalMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	auto itWC=boost::const_begin(wCovariance);
	for(size_t c=0; c<nSample; ++c)
	{
		typename FundamentalMinimalDifferenceC::result_type diff=subtractor(minimal_model_type(qList1[c], qList2[c], sList[c]), meanModel);

		mCov+= *itWC * (diff * diff.transpose());
		advance(itWC,1);
	}	//for(size_t c=0; c<nSample; ++c)

	return GaussianT(meanModel, mCov);
}	//auto ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance) -> GaussianT

/**
 * @brief Computes the model that best explains the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence set
 * @return A 1- or 3-element list, holding the solutions. An empty set if DLT fails, or \c correspondences has too few or too many elements
 * @post The solution is unit norm
 */
template<class CorrespondenceRangeT>
auto Fundamental7SolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	list<ModelT> basis=DLTC<DLTProblemT>::Run(correspondences, 2);

	if(basis.size()!=2)
		return list<ModelT>();

	return ModelFromBasis(basis);
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT

/********** Fundamental8SolverC **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy argument for satisfying the concept requirements
 * @pre \c model has non-zero norm
 * @return Minimal form
 */
template <class DummyRangeT>
auto Fundamental8SolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	return Fundamental7SolverC::MakeMinimal(model, dummy);
} //auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of fundamental matrices
 * @tparam FundamentalRangeT A range of fundamental matrices
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] mFRange Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c mFRange has 7 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class FundamentalRangeT, class WeightRangeT, class DummyRangeT>
auto Fundamental8SolverC::ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	return Fundamental7SolverC::ComputeSampleStatistics(mFRange, wMean, wCovariance, dummy);
}	//auto ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT

/**
 * @brief Computes the fundamental matrix that best explains the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding the fundamental matrix. An empty set if DLT fails, or \c correspondences has too few elements
 * @post The solution is unit norm
 */
template<class CorrespondenceRangeT>
auto Fundamental8SolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<BaseT::sGenerator)
		return list<ModelT>();

	return DLTC<DLTProblemT>::Run(correspondences);
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT


}	//GeometryN
}	//SeeSawN

/********** EXTERNAL TEMPLATES **********/
extern template class std::array<std::complex<double>, 3>;
extern template class std::array<double,4>;

#endif /* FUNDAMENTAL7_SOLVER_IPP_7102902 */
