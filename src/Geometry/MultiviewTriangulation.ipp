/**
 * @file MultiviewTriangulation.ipp Implementation details for MultiviewTriangulationC
 * @author Evren Imre
 * @date 10 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTIVIEW_TRIANGULATION_IPP_8031815
#define MULTIVIEW_TRIANGULATION_IPP_8031815

#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <vector>
#include <map>
#include <list>
#include <cstddef>
#include <tuple>
#include <iterator>
#include <functional>
#include <cmath>
#include <stdexcept>
#include <string>
#include <limits>
#include <utility>
#include "../Elements/Coordinate.h"
#include "../Elements/Camera.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTTriangulationProblem.h"
#include "../Metrics/CheiralityConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../StateEstimation/KalmanFilter.h"
#include "../StateEstimation/KFVectorMeasurementFusionProblem.h"
#include "../Geometry/CoordinateStatistics.h"
#include "Triangulation.h"
//TODO "Wide baseline matching from multi-view correspondences"
// 1. Sidedness filter
// 2. Conflicts: Instead of dropping a clique, try a resolution. Drop the offending edge, potentially drop the vertices (or the weakest vertex. sum the edge strengths) and try again
// 3. Set augmentation: For a triplet of images, intersect the epipolar lines to predict points. Or, presumably, reproject (but we will want to solve this in the multiview matcher module)

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using boost::bimaps::with_info;
using boost::lexical_cast;
using Eigen::Array;
using std::vector;
using std::map;
using std::list;
using std::size_t;
using std::tuple;
using std::get;
using std::make_tuple;
using std::tie;
using std::advance;
using std::greater;
using std::floor;
using std::next;
using std::string;
using std::invalid_argument;
using std::numeric_limits;
using std::make_pair;
using std::pair;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraFromFundamental;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationParametersC;
using SeeSawN::UncertaintyEstimationN::SUTTriangulationProblemC;
using SeeSawN::GeometryN::TriangulatorC;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::MetricsN::CheiralityConstraintC;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::StateEstimationN::KalmanFilterC;
using SeeSawN::StateEstimationN::KFVectorMeasurementFusionProblemC;
using SeeSawN::GeometryN::CoordinateStatistics3DT;

/**
 * @brief Parameters for \c MultiviewTriangulationC
 * @ingroup Parameters
 */
struct MultiviewTriangulationParametersC
{
	double inlierTh=2.45;	///< Inlier threshold for the reprojection error, in pixels. >0
	double noiseVariance=1;	///< Variance of the noise on the 2D coordinates.  >0
	bool flagFastTriangulation=true;	///< If \c true fast (as opposed to optimal) triangulation is performed

	double maxDistanceRank=0.99;	///< Maximum percentile point for an admissible scene point, when ranked wrt distance to the median of the point cloud. (0,1]
	double maxCovarianceTrace=numeric_limits<double>::infinity();	///< Maximum value of the trace of the covariance for a 3D point. >0

	bool flagCheirality=true;	///< If \c true cheirality constraints are enforced. Should be set to false if the camera matrices are not compatible with the cheirality test

	ScaledUnscentedTransformationParametersC sutParameters;	///< Scaled unscented transformation parameters

	double binSize=0;		///< Size of the edge of a quantisation bin, for decimation. 0 means no decimation. In m
};	//struct MultiviewTriangulationParametersC

/**
 * @brief Diagnostics for \c MultiviewTriangulationC
 * @ingroup Diagnostics
 */
struct MultiviewTriangulationDiagnosticsC
{
	bool flagSuccess;	///< True if the algorithm terminates successfully

	unsigned int nMeasurements;	///< Number of 3D measurements
	unsigned int nOriginal3D;	///< Number of 3D points, before filtering
	unsigned int nFiltered3D;	///< Number of 3D points, after filtering
	MultiviewTriangulationDiagnosticsC() : flagSuccess(false), nMeasurements(0), nOriginal3D(0), nFiltered3D(0)
	{}
};	//struct MultiviewTriangulationDiagnosticsC

/**
 * @brief Multiview triangulation algorithm
 * @remarks Usage notes
 * 	- The class is designed to work with \c MultisourceFeatureMatcherC
 * 	- If a source pair is not pointing to two different cameras, it is discarded
 * 	- The algorithm discards inconsistent correspondences. Correspondences from a k-NN matcher often includes inconsistencies, and may lead to loss of landmarks
 * @remarks Untested. Too complex for a unit test. Testing deferred to the application
 * @ingroup Algorithm
 * @nosubgrouping
 */
class MultiviewTriangulationC
{
	private:

		typedef CorrespondenceFusionC::correspondence_container_type CorrespondenceContainerT;	///< A correspondence container
		typedef CorrespondenceFusionC::pairwise_view_type<CorrespondenceContainerT> CorrespondenceStackT;	///< A stack of pairwise correspondences

		typedef SUTTriangulationProblemC::transformed_gaussian_type::covariance_type CovarianceMatrixT;	///< A covariance matrix

		/** @name Implementation details */ //@{
		static void ValidateParameters(const MultiviewTriangulationParametersC& parameters);	///< Validates the input parameters

		typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based, with_info<unsigned int> > CoordinateCorrespondenceContainerT;	///< A container for corresponding coordinates
		typedef tuple<size_t, size_t> SizePairT;	///< A pair of size_t variables
		typedef map<SizePairT, CoordinateCorrespondenceContainerT> CoordinateCorrespondenceStackT;	///< A collection of coordinate correspondences
		static CoordinateCorrespondenceStackT MakeCoordinateCorrespondenceStack(const CorrespondenceStackT& pairwiseCorrespondenceStack, const vector<vector<Coordinate2DT> >& projected);	///< Converts a stack of indices to a stack of correspondences

		static vector<Coordinate3DT> PairwiseTriangulation(const CoordinateCorrespondenceStackT::value_type& correspondences, const vector<CameraMatrixT>& cameras, const MultiviewTriangulationParametersC& parameters);	///< 2-view triangulation
		static tuple<vector<Coordinate3DT>, vector<size_t> > ApplyConstraints(const vector<Coordinate3DT>& unfiltered, const CoordinateCorrespondenceStackT::value_type& correspondences, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionConstraint, const vector<CheiralityConstraintC> cheiralityConstraint, const MultiviewTriangulationParametersC& parameters);	///< Applies the pairwise triangulation constraints

		typedef tuple<size_t, Coordinate2DT, size_t> ProjectionT;	///< A 2D projection
		static constexpr unsigned int iCamera=0;	///< Index of the camera id component
		static constexpr unsigned int iCoordinate2=1;	///< Index of the coordinate component
		static constexpr unsigned int iId=2;	///< Index of the point id

		typedef tuple<Coordinate3DT, ProjectionT, ProjectionT> TriangulationRecordT;	///< A triangulation record
		static constexpr unsigned int iCoordinate3=0;	///< Index of the 3D coordinate component
		static constexpr unsigned int iProjectionL=1;	///< Index of the projection on the first camera
		static constexpr unsigned int iProjectionR=2;	///< Index of the projection on the second camera

		typedef map<size_t, list<TriangulationRecordT> > MeasurementContainerT;	///< Holds the triangulation records
		static void ProcessPairwise(MeasurementContainerT& measurements, const vector<Coordinate3DT>& triangulated, const vector<size_t>& passed, const CoordinateCorrespondenceContainerT& coordinates, const CorrespondenceContainerT& indices, const SizePairT& cameraIds);	///< Processes a pairwise triangulation

		static map<SizePairT, Coordinate2DT> MakeProjectionList(const list<TriangulationRecordT>& records);	///< Extracts the unique projections from a list of triangulation records
		static tuple<int, double, set<SizePairT> > Evaluate3DPoint(const Coordinate3DT& p3D, const map<SizePairT, Coordinate2DT>& projections, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionErrorConstraint, const vector<CheiralityConstraintC>& cheiralityConstraint, const MultiviewTriangulationParametersC& parameters);	///< Evaluates a 3D point
		static void FilterRecords(list<TriangulationRecordT>& records, const set<SizePairT>& inliers);	///< Removes the outliers from the records
		static void RemoveOutliers(MeasurementContainerT& measurements, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionConstraint, const vector<CheiralityConstraintC>& cheiralityConstraint, const MultiviewTriangulationParametersC& parameters);	///< For each cluster, identifies the best measurement, and removes the outliers wrt it

		static optional<CovarianceMatrixT> ComputeCovariance(const Coordinate2DT& x1, const Coordinate2DT& x2, const CameraMatrixT& mP1, const CameraMatrixT& mP2, const MultiviewTriangulationParametersC& parameters);	///< Computes the covariance of a 3D point
		static tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT>, CorrespondenceStackT > FuseMeasurements(const MeasurementContainerT& measurements, const vector<CameraMatrixT>& cameras, const MultiviewTriangulationParametersC& parameters);	///< Fuses the measurements into a single 3D point cloud

		static vector<size_t> RankDistance(const vector<Coordinate3DT>& pointCloud);	///< Ranks the point cloud wrt distance to the median
		static tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT> > FilterStructure(CorrespondenceStackT& correspondences, const vector<Coordinate3DT>& pointCloud, const vector<CovarianceMatrixT>& covarianceList, const MultiviewTriangulationParametersC& parameters );	///< Filters the 3D point cloud wrt distance and trace constraints

		static vector<bool> Decimate(const vector<Coordinate3DT>& pointCloud, const vector<CovarianceMatrixT>& covariances, const MultiviewTriangulationParametersC& parameters);	///< Decimates a point cloud
		//@}

	public:

		typedef CovarianceMatrixT covariance_matrix_type;	///< Type of the covariance matrix

		static MultiviewTriangulationDiagnosticsC Run(vector<Coordinate3DT>& triangulated, vector<CovarianceMatrixT>& covariances, CorrespondenceStackT& inliers, const CorrespondenceStackT& pairwiseCorrespondenceStack, const vector<CameraMatrixT>& cameras, const vector<vector<Coordinate2DT> >& projected, const MultiviewTriangulationParametersC& parameters);	///< Runs the algorithm
};	//class MultiviewTriangulationC

}	//GeometryN
}	//SeeSawN

#endif /* MULTIVIEW_TRIANGULATION_IPP_8031815 */
