/**
 * @file Homography2DSolver.h Public interface for Homography2DSolverC
 * @author Evren Imre
 * @date 10 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY_2D_SOLVER_H_8092112
#define HOMOGRAPHY_2D_SOLVER_H_8092112

#include "Homography2DSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{

class Homography2DSolverC;	///< 4-point 2D homography estimator
struct Homography2DMinimalDifferenceC;	///< Difference functor for minimally-represented 2D homographies
}	//GeometryN
}	//SeeSawN

#endif /* HOMOGRAPHY_2D_SOLVER_H_8092112 */
