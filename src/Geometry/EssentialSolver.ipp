/**
 * @file EssentialSolver.ipp Implementation of the 5-point essential matrix solver
 * @author Evren Imre
 * @date 5 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ESSENTIAL_SOLVER_IPP_0892103
#define ESSENTIAL_SOLVER_IPP_0892103

#include <boost/math/special_functions/pow.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <Eigen/LU>
#include <Eigen/Eigenvalues>
#include <cstddef>
#include <limits>
#include <cmath>
#include <iterator>
#include <tuple>
#include <vector>
#include <type_traits>
#include <algorithm>
#include "GeometrySolverBase.h"
#include "DLT.h"
#include "Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/Complexity.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::math::pow;
using boost::math::constants::half_pi;
using boost::math::constants::two_pi;
using boost::math::constants::pi;
using boost::ForwardRangeConcept;
using Eigen::FullPivLU;
using Eigen::EigenSolver;
using std::size_t;
using std::numeric_limits;
using std::fabs;
using std::acos;
using std::atan2;
using std::sin;
using std::cos;
using std::advance;
using std::tie;
using std::get;
using std::vector;
using std::make_tuple;
using std::is_same;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::DLTC;
using SeeSawN::GeometryN::DLTProblemC;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::DecomposeEssential;
using SeeSawN::ElementsN::ComposeEssential;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::NumericN::ComplexityEstimatorC;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;

//Forward declarations
struct EssentialMinimalDifferenceC;

/**
 * @brief 5-point essential matrix estimator
 * @remarks H. Stewenius, C. Engels, D. Nister, "Recent Developements on Direct Relative Orientation, " ISPRS Journal of Photogrammetry and Remote Sensing, 60:284-294, June 2006
 * @remarks Distance measure: 1-Absolute value of the dot-product between the unit vectors representing the fundamental matrices. Range=[0,1]
 * @remarks Degeneracies: No translation. No degeneracy detection, as it is expensive. However, a degenerate configuration yields a valid (but arbitrary) essential matrix.
 * @remarks Minimal parameterisation: Rotation quaternion, and the azimuth and the elevation angles representing the translation direction
 * @warning Any 2D coordinates involved should be normalised by multiplication with the inverse of the appropriate intrinsic calibration matrix
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class EssentialSolverC : public GeometrySolverBaseC<EssentialSolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 5, true, 10, 9, 5>
{
	private:

		typedef GeometrySolverBaseC<EssentialSolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 5, true, 10, 9, 5> BaseT;	///< Type of the base

		typedef Matrix<double, BaseT::nDOF, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 3x3 matrix
		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::EpipolarTag, BaseT::sGenerator, 0, 3> DLTProblemT;	///< DLT problem for 5-point essential matrix estimation
																																									//No rank condition on the basis vectors!
		typedef MultivariateGaussianC<EssentialMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		/** @name Implementation details */ ///@{
		static list<ModelT> ModelFromBasis(const list<ModelT>& basis);	///< Given a basis, computes the corresponding essential matrices
		static Matrix<RealT, 10, 20> ComputeActionMatrix(const list<ModelT>& basis);	///< Computes the action matrix
		///@}

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		typedef typename BaseT::real_type distance_type;	///< Type of the distance between two models

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the model that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<QuaternionT, RealT, RealT> minimal_model_type;	///< A minimally parameterised model. [Rotation, azimuth, elevation]
		template<class CorrespondenceRangeT> static minimal_model_type MakeMinimal(const ModelT& model, const CorrespondenceRangeT& correspondences);	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class EssentialRangeT, class WeightRangeT, class CorrespondenceRangeT> static GaussianT ComputeSampleStatistics(const EssentialRangeT& mERange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const CorrespondenceRangeT& correspondences);	///< Computes the sample statistics for a set of essential matrices
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iRotation=0;	///< Index of the rotation
		static constexpr unsigned int iAzimuth=1;	///< Index of the azimuth
		static constexpr unsigned int iElevation=2;	///< Index of the elevation
		//@}

		static bool VerifyTraceConstraint(const EpipolarMatrixT& mE, RealT zero=3*numeric_limits<RealT>::epsilon());	///< Verifies whether a given matrix satisfies the trace constraint
};	//class EssentialSolverC

/**
 * @brief Difference functor for minimally-represented essential matrices
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct EssentialMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef EssentialSolverC::minimal_model_type first_argument_type;
	typedef EssentialSolverC::minimal_model_type second_argument_type;
	typedef Matrix<EssentialSolverC::real_type, EssentialSolverC::DoF(), 1> result_type;	///< Difference in vector form: [Rotation vector; azimuth; inclination]

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between to minimally-represented essential matrices
	//@}
};	//struct EssentialMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] model Model to be converted
 * @param[in] correspondences A small validation set to distinguish between the 4 possible decompositions of \c model
 * @pre \c model has non-zero norm
 * @pre \c model is a valid essential matrix (unenforced)
 * @pre Right members of \c correspondences are normalised (unenforced)
 * @return Minimal form
 * @remarks http://en.wikipedia.org/wiki/Spherical_coordinate_system
 */
template<class CorrespondenceRangeT>
auto EssentialSolverC::MakeMinimal(const ModelT& model, const CorrespondenceRangeT& correspondences) -> minimal_model_type
{
	RotationMatrix3DT mR;
	Coordinate3DT t;
	tie(t, mR)=*DecomposeEssential(model, correspondences, 10*numeric_limits<RealT>::epsilon());	//Precondition: model is a valid essential matrix, so DecomposeEssential cannot fail

	//Spherical coordinates corresponding to the baseline
	RealT elevation=acos(t[2])-half_pi<RealT>();
	RealT azimuth= (fabs(t[2])==1) ? 0 : atan2(t[1], t[0]);

	return make_tuple(QuaternionT(mR), azimuth, elevation);
}	//auto MakeMinimal(const ModelT& model) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of essential matrices
 * @tparam EssentialRangeT A range of essential matrices
 * @tparam WeightRangeT A range weight values
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] mERange Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] correspondences A small validation set to distinguish between the 4 possible decompositions of \c model
 * @return Gaussian for the sample statistics
 * @pre \c EssentialRangeT is a forward range of elements of type \c EpipolarMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c mERange should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @pre \c correspondences is appropriately normalised (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c mERange has 5 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class EssentialRangeT, class WeightRangeT, class CorrespondenceRangeT>
auto EssentialSolverC::ComputeSampleStatistics(const EssentialRangeT& mERange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const CorrespondenceRangeT& correspondences) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<EssentialRangeT>));
	static_assert(is_same<typename range_value<EssentialRangeT>::type, EpipolarMatrixT>::value, "EssentialSolverC::ComputeSampleStatistics: EssentialRangeT must have elements of type EpipolarMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "EssentialSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(mERange)==boost::distance(wMean));
	assert(boost::distance(mERange)==boost::distance(wCovariance));

	//Decompose
	unsigned int nSample=boost::distance(mERange);
	vector<QuaternionT> qList(nSample);
	vector<RealT> aList(nSample);	//Azimuth
	vector<RealT> eList(nSample);	//Elevation
	size_t index=0;
	for(const auto& current : mERange)
	{
		tie(qList[index], aList[index], eList[index])=MakeMinimal(current, correspondences);
		++index;
	}	//for(const auto& current : homographies)

	//Mean

	minimal_model_type meanModel;
	get<iAzimuth>(meanModel)=boost::inner_product(aList, wMean, (RealT)0);
	get<iElevation>(meanModel)=boost::inner_product(eList, wMean, (RealT)0);

	Matrix<RealT, 3, 3> mCovQ;	//Dummy
	tie(get<iRotation>(meanModel), mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	//Covariance
	EssentialMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	auto itWC=boost::const_begin(wCovariance);
	for(size_t c=0; c<nSample; ++c)
	{
		typename EssentialMinimalDifferenceC::result_type diff=subtractor(minimal_model_type(qList[c], aList[c], eList[c]), meanModel);
		mCov+= *itWC * (diff * diff.transpose());
		advance(itWC,1);
	}	//for(size_t c=0; c<nSample; ++c)

	return GaussianT(meanModel, mCov);
}	//auto ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance) -> GaussianT

/**
 * @brief Computes the model that best explains the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence set
 * @return A list of solutions with up to 10 elements. An empty set if DLT fails, or \c correspondences has too few or too many elements
 * @pre Point coordinates in \c correspondences are normalised
 * @post The solution is unit norm
 */
template<class CorrespondenceRangeT>
auto EssentialSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	list<ModelT> basis=DLTC<DLTProblemT>::Run(correspondences, 4);

	if(basis.size()!=4)
		return list<ModelT>();

	return ModelFromBasis(basis);
}	//auto operator()(const CorrespondenceRangeT& correspondences) -> ModelT

}	//GeometryN
}	//SeeSawN

#endif /* ESSENTIAL_SOLVER_IPP_0892103 */
