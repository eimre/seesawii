/**
 * @file CoordinateStatistics.cpp Instantiations for CoordinateStatisticsC
 * @author Evren Imre
 * @date 26 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CoordinateStatistics.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class CoordinateStatisticsC<typename ValueTypeM<Coordinate2DT>::type, 2>;
template optional<tuple<typename CoordinateStatistics2DT::ArrayD, typename CoordinateStatistics2DT::ArrayD>> CoordinateStatistics2DT::ComputeMeanAndStD(const vector<Coordinate2DT>&);
template optional<typename CoordinateStatistics2DT::ArrayD2> CoordinateStatistics2DT::ComputeExtent(const vector<Coordinate2DT>&);

template class CoordinateStatisticsC<typename ValueTypeM<Coordinate3DT>::type, 3>;
template optional<tuple<typename CoordinateStatistics3DT::ArrayD, typename CoordinateStatistics3DT::ArrayD>> CoordinateStatistics3DT::ComputeMeanAndStD(const vector<Coordinate3DT>&);
template optional<typename CoordinateStatistics3DT::ArrayD2> CoordinateStatistics3DT::ComputeExtent(const vector<Coordinate3DT>&);

}   //GeometryN
}	//SeeSawN


