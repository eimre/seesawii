/**
 * @file MultiviewTriangulation.cpp Implementation of MultiviewTriangulationC
 * @author Evren Imre
 * @date 10 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "MultiviewTriangulation.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief
 * @param[in] parameters
 * @throws invalid_argument If there is a parameter with an invalid value
 */
void MultiviewTriangulationC::ValidateParameters(const MultiviewTriangulationParametersC& parameters)
{
	if(parameters.inlierTh<=0)
		throw(invalid_argument(string("MultiviewTriangulationC::ValidateParameters : parameters.inlierTh must be positive. Value= ")+lexical_cast<string>(parameters.inlierTh)));

	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("MultiviewTriangulationC::ValidateParameters : parameters.noiseVariance must be positive. Value= ")+lexical_cast<string>(parameters.noiseVariance)));

	if(parameters.maxCovarianceTrace<=0)
		throw(invalid_argument(string("MultiviewTriangulationC::ValidateParameters : parameters.maxCovarianceTh must be positive. Value= ")+lexical_cast<string>(parameters.maxCovarianceTrace)));

	if(parameters.maxDistanceRank<=0 && parameters.maxDistanceRank>1)
		throw(invalid_argument(string("MultiviewTriangulationC::ValidateParameters : parameters.maxDistanceRank must be in (0,1]. Value= ")+lexical_cast<string>(parameters.maxDistanceRank)));

	if(parameters.binSize<0)
		throw(invalid_argument(string("MultivariateTriangulationC::ValidateParameters : parameters.binSize must be >=0. Value=") + lexical_cast<string>(parameters.binSize)) );
}	//void ValidateParameters(const MultiviewTriangulationParametersC& parameters)

/**
 * @brief Converts a stack of indices to a stack of correspondences
 * @param[in] pairwiseCorrespondenceStack A stack of corresponding indices for each image pair
 * @param[in] projected Image coordinates
 * @return A stack of corresponding coordinates for each image pair
 */
auto MultiviewTriangulationC::MakeCoordinateCorrespondenceStack(const CorrespondenceStackT& pairwiseCorrespondenceStack, const vector<vector<Coordinate2DT> >& projected) -> CoordinateCorrespondenceStackT
{
	CoordinateCorrespondenceStackT output;
	for(const auto& currentList : pairwiseCorrespondenceStack)
	{
		//Get the camera indices
		size_t i1;
		size_t i2;
		tie(i1, i2)=currentList.first;

		//Retrieve the actual coordinates from projected, and insert to the output
		CoordinateCorrespondenceContainerT& pContainer=output[currentList.first];
		for(const auto& current : currentList.second)
			pContainer.push_back(CoordinateCorrespondenceContainerT::value_type(projected[i1][current.left], projected[i2][current.right], current.info));
	}	//for(const auto& currentList : pairwiseCorrespondenceStack)

	return output;
}	//auto MakeCoordinateCorrespondenceStack(const CorrespondenceStackT& pairwiseCorrespondenceStack) -> CoordinateCorrespondenceStackT

/**
 * @brief 2-view triangulation
 * @param[in] correspondences Correspondences. [Camera indices; Corresponding indices]
 * @param[in] cameras Cameras
 * @param[in] parameters Parameters
 * @return Triangulated 3D coordinates
 */
vector<Coordinate3DT> MultiviewTriangulationC::PairwiseTriangulation(const CoordinateCorrespondenceStackT::value_type& correspondences, const vector<CameraMatrixT>& cameras, const MultiviewTriangulationParametersC& parameters)
{
	//Source Ids
	size_t i1;
	size_t i2;
	tie(i1, i2)=correspondences.first;

	if(i1==i2)
		return vector<Coordinate3DT>();

	//Triangulate
	TriangulatorC triangulator;
	return triangulator(cameras[i1], cameras[i2], correspondences.second, parameters.flagFastTriangulation);
}	//tuple<vector<Coordinate3DT>, vector<size_t> > PairwiseTriangulation(const CorrespondenceContainerT& correspondences, const ReprojectionErrorConstraintT& reprojectionConstraint)

/**
 * @brief Applies the pairwise triangulation constraints
 * @param[in] unfiltered 3D point list to be filtered
 * @param[in] correspondences 2D correspondences
 * @param[in] cameras Cameras
 * @param[in] reprojectionConstraint Reprojection constraint
 * @param[in] cheiralityConstraint Cheirality constraint vector
 * @param[in] parameters Parameters
 * @return A tuple: Filtered 3D points; Indices of the successful elements
 */
tuple<vector<Coordinate3DT>, vector<size_t> > MultiviewTriangulationC::ApplyConstraints(const vector<Coordinate3DT>& unfiltered, const CoordinateCorrespondenceStackT::value_type& correspondences, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionConstraint, const vector<CheiralityConstraintC> cheiralityConstraint, const MultiviewTriangulationParametersC& parameters)
{
	//Source Ids
	size_t i1;
	size_t i2;
	tie(i1, i2)=correspondences.first;

	tuple<vector<Coordinate3DT>, vector<size_t> > output;
	get<0>(output).reserve(unfiltered.size());
	get<1>(output).reserve(unfiltered.size());

	size_t index=0;
	for(const auto& current : correspondences.second)
	{
		//Cheirality constraint
		bool flagPass=parameters.flagCheirality || cheiralityConstraint[i1](unfiltered[index]);
		flagPass= flagPass &&  (parameters.flagCheirality || cheiralityConstraint[i2](unfiltered[index]));

		//Reprojection constraint
		flagPass= flagPass && reprojectionConstraint(cameras[i1], unfiltered[index], current.left);
		flagPass= flagPass && reprojectionConstraint(cameras[i2], unfiltered[index], current.right);

		if(flagPass)
		{
			get<0>(output).emplace_back(unfiltered[index]);
			get<1>(output).push_back(index);
		}	//if(flagPass)

		++index;
	}	//for(const auto& current : correspondences.second)

	get<0>(output).shrink_to_fit();
	get<1>(output).shrink_to_fit();

	return output;
}	//tuple<vector<Coordinate3DT>, vector<size_t> > ApplyConstraints(const vector<Coordinate3DT>& unfiltered, const CoordinateCorrespondenceStackT::value_type& correspondences, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionConstraint, const vector<CheiralityConstraintC> cheiralityConstraint, const MultiviewTriangulationParametersC& parameters);

/**
 * @brief Processes a pairwise triangulation
 * @param[out] measurements Measurement records
 * @param[in] triangulated Triangulated points
 * @param[in] passed Indices of the correspondences in \c triangulated
 * @param[in] coordinates Corresponding coordinates
 * @param[in] indices Corresponding indices
 * @param[in] cameraIds Camera indices
 */
void MultiviewTriangulationC::ProcessPairwise(MeasurementContainerT& measurements, const vector<Coordinate3DT>& triangulated, const vector<size_t>& passed, const CoordinateCorrespondenceContainerT& coordinates, const CorrespondenceContainerT& indices, const SizePairT& cameraIds)
{
	CoordinateCorrespondenceContainerT filteredCoordinates=FilterBimap(coordinates, passed);
	CorrespondenceContainerT filteredIndices=FilterBimap(indices, passed);

	auto itCoordinates=filteredCoordinates.begin();
	auto itIndices=filteredIndices.begin();

	for(const auto& current : triangulated)
	{
		ProjectionT projection1(get<0>(cameraIds), itCoordinates->left, itIndices->left);
		ProjectionT projection2(get<1>(cameraIds), itCoordinates->right, itIndices->right);

		list<TriangulationRecordT>& pList=measurements[itIndices->info];
		pList.emplace_back(current, projection1, projection2);

		advance(itCoordinates,1);
		advance(itIndices,1);
	}	//for(const auto& current : triangulated)
}	//void ProcessPairwise(MeasurementContainerT& measuerements, const vector<Coordinate3DT>& triangulated, const vector<size_t>& passed, const CoordinateCorrespondenceContainerT& coordinates, const CorrespondenceContainerT& indices)

/**
 * @brief Extracts the unique projections from a list of triangulation records
 * @param[in] records Triangulation records
 * @return A map of unique projections. [ [Camera id; Poind id]; 2D coordinates ]
 */
auto MultiviewTriangulationC::MakeProjectionList(const list<TriangulationRecordT>& records) -> map<SizePairT, Coordinate2DT>
{
	map<SizePairT, Coordinate2DT> output;

	for(const auto& currentRecord : records)
	{
		const ProjectionT& pProjectionL=get<iProjectionL>(currentRecord);
		output[SizePairT(get<iCamera>(pProjectionL), get<iId>(pProjectionL))]=get<iCoordinate2>(pProjectionL);

		const ProjectionT& pProjectionR=get<iProjectionR>(currentRecord);
		output[SizePairT(get<iCamera>(pProjectionR), get<iId>(pProjectionR))]=get<iCoordinate2>(pProjectionR);
	}	//for(const auto& currentRecord : currentMeasurement.second)

	return output;
}	//auto MakeProjectionList(const list<TriangulationRecordT>& records) -> map<SizePairT, Coordinate2DT>

/**
 * @brief Evaluates a 3D point
 * @param[in] p3D 3D point
 * @param[in] projections 2D projections
 * @param[in] cameras Cameras
 * @param[in] reprojectionErrorConstraint Reprojection error constraint
 * @param[in] cheiralityConstraint Cheirality constraint
 * @param[in] parameters Parameters
 * @return A tuple: # Cheirality successes, total error, inlier projections
 */
auto MultiviewTriangulationC::Evaluate3DPoint(const Coordinate3DT& p3D, const map<SizePairT, Coordinate2DT>& projections, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionErrorConstraint, const vector<CheiralityConstraintC>& cheiralityConstraint, const MultiviewTriangulationParametersC& parameters) -> tuple<int, double, set<SizePairT> >
{
	unsigned int nCheiralityPass=0;
	double error=0;
	set<SizePairT> inliers;

	for(const auto& currentProjection : projections)
	{
		size_t id=get<0>(currentProjection.first);
		bool flagCheirality= parameters.flagCheirality || cheiralityConstraint[id](p3D);

		if(!flagCheirality)
			continue;

		bool flagReprojection;
		double loss;
		tie(flagReprojection, loss)=reprojectionErrorConstraint.Enforce(cameras[id], p3D, currentProjection.second);

		++nCheiralityPass;
		error+=loss;

		if(flagReprojection)
			inliers.insert(currentProjection.first);

	}	//for(const auto& currentProjection : projections)

	return make_tuple(nCheiralityPass, error, inliers);
}	//auto Evaluate3DPoint(const Coordinate3DT& p3D, const map<SizePairT, Coordinate2DT>& projections, const ReprojectionErrorConstraintT& reprojectionErrorConstraint, const vector<CheiralityConstraintC>& cheiralityConstraint) -> tuple<int, double, set<SizePairT> >

/**
 * @brief Removes the outliers from the records
 * @param[in] records Records
 * @param[in] inliers Inlier identifiers
 */
void MultiviewTriangulationC::FilterRecords(list<TriangulationRecordT>& records, const set<SizePairT>& inliers)
{
	auto it=records.begin();
	auto itE=records.end();
	auto itInE=inliers.cend();
	for(; it!=itE; )
	{
		SizePairT iP1=SizePairT(get<iCamera>(get<iProjectionL>(*it)), get<iId>(get<iProjectionL>(*it)));
		SizePairT iP2=SizePairT(get<iCamera>(get<iProjectionR>(*it)), get<iId>(get<iProjectionR>(*it)));

		//A record is retained only if both of its projections are inliers
		if(inliers.find(iP1)!=itInE && inliers.find(iP2)!=itInE)
			advance(it,1);
		else
			it=records.erase(it);
	}	//for(; it!=itE; )
}	//void FilterRecords(list<TriangulationRecordT>& records, const set<SizePairT>& inliers)

/**
 * @brief For each cluster, identifies the best measurement, and removes the outliers wrt it
 * @param[in, out] measurements Measurements
 * @param[in] cameras Camera matrices
 * @param[in] reprojectionConstraint Reprojection error constraint
 * @param[in] cheiralityConstraint Cheirality constraints
 * @param[in] parameters Parameters
 */
void MultiviewTriangulationC::RemoveOutliers(MeasurementContainerT& measurements, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionConstraint, const vector<CheiralityConstraintC>& cheiralityConstraint, const MultiviewTriangulationParametersC& parameters)
{
	//For each cluster
	for(auto& currentMeasurement : measurements)
	{
		//Get the projections for the cluster
		map<SizePairT, Coordinate2DT> projections=MakeProjectionList(currentMeasurement.second);

		//Now, evaluate the 3D point in each record against the projections
		vector<set<SizePairT> > inliers(currentMeasurement.second.size());	//Inlier projections for each 3D point
		typedef tuple<int, double, int> ScoreT;	//Evaluation score type: [#Cheirality pass, -error, Id]
		ScoreT bestScore(-1,0,-1);
		size_t index=0;
		for(const auto& currentRecord : currentMeasurement.second)
		{
			unsigned int nCheiralityPass;
			double totalError;
			tie(nCheiralityPass, totalError, inliers[index])=Evaluate3DPoint(get<iCoordinate3>(currentRecord), projections, cameras, reprojectionConstraint, cheiralityConstraint, parameters);

			ScoreT score(nCheiralityPass, -totalError, -1);
			if(nCheiralityPass>0)
				get<2>(score)=index;

			if(score>bestScore)
				bestScore=score;

			++index;
		}	//for(const auto& currentRecord : currentMeasurement.second)

		//If no successful 3D points, delete all records
		if(get<2>(bestScore)==-1)
		{
			currentMeasurement.second.clear();
			continue;
		}	//if(get<2>(bestScore)==-1)

		//Filter the records
		FilterRecords(currentMeasurement.second, inliers[get<2>(bestScore)]);
	}	//for(auto& current : measurements)
}	//void RemoveOutliers(MeasurementContainerT& measurements, const vector<CameraMatrixT>& cameras, const ReprojectionErrorConstraintT& reprojectionConstraint)

/**
 * @brief Computes the covariance of a 3D point
 * @param[in] x1 Projection on the first image
 * @param[in] x2 Projection on the second image
 * @param[in] mP1 First camera matrix
 * @param[in] mP2 Second camera matrix
 * @param[in] parameters Parameters
 * @return Covariance. Invalid if SUT fails
 */
auto MultiviewTriangulationC::ComputeCovariance(const Coordinate2DT& x1, const Coordinate2DT& x2, const CameraMatrixT& mP1, const CameraMatrixT& mP2, const MultiviewTriangulationParametersC& parameters) -> optional<CovarianceMatrixT>
{
	SUTTriangulationProblemC sutProblem(x1, x2, parameters.noiseVariance, mP1, mP2, parameters.flagFastTriangulation);

	typedef ScaledUnscentedTransformationC<SUTTriangulationProblemC> SUTT;
	SUTT::result_type sutOutput;
	sutOutput=SUTT::Run(sutProblem, parameters.sutParameters);

	optional<CovarianceMatrixT> output;

	if(sutOutput)
		output=sutOutput->GetCovariance();

	return output;
}	//auto ComputeCovariance(const Coordinate2DT& x1, const Coordinate2DT& x2, const CameraMatrixT& mP1, const CameraMatrixT& mP2, const MultiviewTriangulationParametersC& parameters) -> optional<CovarianceMatrixT>

/**
 * @brief Fuses the measurements into a single 3D point cloud
 * @param[in] measurements Measurements
 * @param[in] cameras Cameras
 * @param[in] parameters Parameters
 * @return A tuple: 3D point cloud; covariance for each point; inlier correspondences
 */
auto MultiviewTriangulationC::FuseMeasurements(const MeasurementContainerT& measurements, const vector<CameraMatrixT>& cameras, const MultiviewTriangulationParametersC& parameters) ->tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT>,  CorrespondenceStackT>
{
	tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT>, CorrespondenceStackT > output;

	//Reserve, because a 3D point can be discarded for not having a valid correspondence
	get<0>(output).reserve(measurements.size());
	get<1>(output).reserve(measurements.size());

	typedef KFVectorMeasurementFusionProblemC<Coordinate3DT> KFProblemT;
	KFProblemT kfProblem;	//Has no state, so can be reused

	//For each cluster
	size_t cCluster=0;
	for(const auto& currentCluster : measurements)
	{
		//Add the measurements to the kalman filter
		KalmanFilterC<KFProblemT> kalmanFilter;
		kalmanFilter.Clear();	//To hush the compiler complaining about "maybe uninitialized"

		//Fuse the measurements
		for(const auto& currentMeasurement : currentCluster.second)
		{
			const ProjectionT& projectionL=get<iProjectionL>(currentMeasurement);
			const ProjectionT& projectionR=get<iProjectionR>(currentMeasurement);

			size_t id1=get<iCamera>(projectionL);
			size_t id2=get<iCamera>(projectionR);

			//Measurement covariance
			optional<CovarianceMatrixT> covariance=ComputeCovariance(get<iCoordinate2>(projectionL), get<iCoordinate2>(projectionR), cameras[id1], cameras[id2], parameters);
			if(!covariance)
				continue;

			//Fuse via the Kalman filter

			//Initialise the kalman filter
			if(!kalmanFilter.IsValid())
				kalmanFilter.SetState(KFProblemT::state_type(get<iCoordinate3>(currentMeasurement), *covariance), optional<CovarianceMatrixT>());	//No process noise
			else
				//No need for prediction, as this is just a weighted averaging problem
				//Also, cannot fail, due to the problem set-up
				kalmanFilter.Update(kfProblem, KFProblemT::measurement_type(get<iCoordinate3>(currentMeasurement), *covariance), kalmanFilter.GetState(), 0);	//Prediction is the state itself

			//Add to the inlier correspondences
			CorrespondenceContainerT& pContainer=get<2>(output)[SizePairT(id1, id2)];
			pContainer.push_back( CorrespondenceContainerT::value_type(get<iId>(projectionL), get<iId>(projectionR), cCluster) );
		}	//for(const auto& currentMeasurement : currentCluster)

		//Read the fused 3D point off the Kalman filter state
		if(kalmanFilter.IsValid())
		{
			get<0>(output).emplace_back(KFProblemT::GetStateMean(kalmanFilter.GetState()));
			get<1>(output).emplace_back(KFProblemT::GetStateCovariance(kalmanFilter.GetState()));
			++cCluster;
		}	//if(kalmanFilter.IsValid())
	}	//for(const auto& currentCluster : measurements)

	get<0>(output).shrink_to_fit();
	get<1>(output).shrink_to_fit();
	return output;
}	//auto FuseMeasurements(const MeasurementContainerT& measurements) ->tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT> >

/**
 * @brief Ranks the point cloud wrt distance to the median
 * @param[in] pointCloud Point cloud
 * @return Vector of ranks
 */
vector<size_t> MultiviewTriangulationC::RankDistance(const vector<Coordinate3DT>& pointCloud)
{
	//Median coordinate
	set<ValueTypeM<Coordinate3DT>::type > x,y,z;	//Coordinate sorters
	for(const auto& current : pointCloud)
	{
		x.insert(current[0]);
		y.insert(current[1]);
		z.insert(current[2]);
	}	//for(const auto& current : pointCloud)

	size_t nPoints=pointCloud.size();
	size_t medianIndex=floor(nPoints/2);

	Coordinate3DT medianCoordinate(*next(x.cbegin(), medianIndex), *next(y.cbegin(), medianIndex), *next(z.cbegin(), medianIndex) );

	//Rank wrt distance
	map<ValueTypeM<Coordinate3DT>::type, size_t> ranker;
	for(size_t c=0; c<nPoints; ++c)
		ranker.emplace( (pointCloud[c]-medianCoordinate).squaredNorm(),c);

	//Rank array
	vector<size_t> output(nPoints);
	size_t currentRank=0;
	for(const auto& current : ranker)
	{
		output[current.second]=currentRank;
		++currentRank;
	}	//for(const auto& current : ranker)

	return output;
}	//vector<size_t> RankDistance(const vector<Coordinate3DT>& pointCloud)

/**
 * @brief Decimates a point cloud
 * @param[in] pointCloud Point cloud
 * @param[in] covariances Point covariances
 * @param[in] parameters Parameters
 * @return An indicator array. False indicates a discarded point
 */
vector<bool> MultiviewTriangulationC::Decimate(const vector<Coordinate3DT>& pointCloud, const vector<CovarianceMatrixT>& covariances, const MultiviewTriangulationParametersC& parameters)
{
	//Coordinate statistics for the quantiser
	CoordinateStatistics3DT::ArrayD2 extent=*CoordinateStatistics3DT::ComputeExtent(pointCloud);

	//Quantise
	CoordinateStatistics3DT::ArrayD binSize(parameters.binSize, parameters.binSize, parameters.binSize);
	vector<unsigned int> quantised=CoordinateTransformations3DT::Bucketise(pointCloud, binSize, extent);

	//From each bin, select the element with the lowest total covariance
	size_t nElement=pointCloud.size();
	map<unsigned int, pair<size_t, double> > representatives;
	for(size_t c=0; c<nElement; ++c)
	{
		double totalUncertainty=covariances[c].trace();
		auto it=representatives.emplace(quantised[c], make_pair(c, totalUncertainty) );
		if(!it.second && it.first->second.second > totalUncertainty)
			representatives[quantised[c]]={c, totalUncertainty};
	}

	//Output
	vector<bool> output(nElement, false);
	for(const auto& current : representatives)
		output[current.second.first]=true;

	return output;
}	//vector<bool> Decimate(const vector<Coordinate3DT>& pointCloud, const vector<CovarianceMatrixT>& covariances, const MultiviewTriangulationParametersC& parameters)

/**
 * @brief Filters the 3D point cloud wrt distance and trace constraints
 * @param[in, out] correspondences Image correspondences. At the output, filtered
 * @param[in] pointCloud Point cloud
 * @param[in] covarianceList Covariances
 * @param[in] parameters Parameters
 * @return A tuple: Filtered point cloud, and the corresponding covariances
 */
auto MultiviewTriangulationC::FilterStructure(CorrespondenceStackT& correspondences, const vector<Coordinate3DT>& pointCloud, const vector<CovarianceMatrixT>& covarianceList, const MultiviewTriangulationParametersC& parameters ) -> tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT> >
{
	//Rank wrt distance to the median point
	vector<size_t> ranks=RankDistance(pointCloud);

	//Compute the traces
	size_t nPoints=pointCloud.size();
	vector<double> traces(nPoints);
	for(size_t c=0; c<nPoints; ++c)
		traces[c]=covarianceList[c].trace();

	//Filter the point cloud and the covariances
	size_t rankTh=floor(nPoints*parameters.maxDistanceRank);	//Any point with the distance rank above this value is discarded

	vector<Coordinate3DT> filteredPoints; filteredPoints.reserve(nPoints);
	vector<CovarianceMatrixT> filteredCovariances; filteredCovariances.reserve(nPoints);
	vector<int> indexList(nPoints, -1);	//New indices

	size_t nFiltered=0;
	vector<bool> flagFiltered(nPoints, false);
	for(size_t c=0; c<nPoints; ++c)
		if(ranks[c]<rankTh && traces[c]<=parameters.maxCovarianceTrace)
		{
			filteredPoints.push_back(pointCloud[c]);
			filteredCovariances.push_back(covarianceList[c]);

			flagFiltered[c]=true;

			indexList[c]=nFiltered;
			++nFiltered;
		}	//if(ranks[c]<rankTh && traces[c]<parameters.maxCovarianceTrace)

	//Decimation

	vector<Coordinate3DT> decimatedPoints;
	vector<CovarianceMatrixT> decimatedCovariances;
	vector<int> decimatedIndexList(nPoints, -1);	//New indices
	if(parameters.binSize>0)
	{
		vector<bool> flagDecimated=Decimate(filteredPoints, filteredCovariances, parameters);	//Indices /wrt filteredPoints

		decimatedPoints.reserve(nPoints);
		decimatedCovariances.reserve(nPoints);

		size_t nSurvivor=0;
		for(size_t c=0; c<nPoints; ++c)
		{
			flagFiltered[c] = flagFiltered[c] && flagDecimated[indexList[c]];		//indexList[c] may hold some negative values. But they will not be accessed, as the corresponding flagFilter[c] values would be false

			if(flagFiltered[c])
			{
				decimatedPoints.push_back(pointCloud[c]);
				decimatedCovariances.push_back(covarianceList[c]);

				decimatedIndexList[c]=nSurvivor;
				++nSurvivor;
			}	//if(flagFiltered)
		}	//for(size_t c=0; c<nPoints; ++c)
	}	//if(parameters.binSize>0)
	else
	{
		decimatedPoints.swap(filteredPoints);
		decimatedCovariances.swap(filteredCovariances);
		decimatedIndexList.swap(indexList);
	}


	//Now the correspondences
	for(auto& currentList : correspondences)
	{
		auto it=currentList.second.begin();
		auto itE=currentList.second.end();
		for(; it!=itE;)
		{
			if(flagFiltered[it->info])
			{
				it->info=decimatedIndexList[it->info];
				advance(it,1);
			}
			else
				it=currentList.second.erase(it);
		}	//for(; it!=itE;)
	}	//for(auto& currentList : correspondences)

	return make_tuple(move(decimatedPoints), move(decimatedCovariances));
}	//tuple<vector<Coordinate3DT>, vector<CovarianceMatrixT> > ApplyDistanceFilter(CorrespondenceStackT& correspondences, const vector<Coordinate3DT>& pointCloud, const vector<CovarianceMatrixT>& covarianceList, const MultiviewTriangulationParametersC& parameters )

/**
 * @brief Runs the algorithm
 * @param[out] triangulated 3D point cloud
 * @param[out] covariances Covariance matrix for each 3D point
 * @param[out] inliers Members of \c pairwiseCorrespondenceStack used in the construction of \c triangulated. A correspondence item is [Id in first image; Id in second image; Id in \c triangulated]
 * @param[in] pairwiseCorrespondenceStack Pairwise correspondences. Each element is a correspondence list. Each correspondence is a pair of indices into \c projected
 * @param[in] cameras Cameras
 * @param[in] projected Image coordinates of the points in \c pairwiseCorrespondenceStack
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @pre \c cameras and \c projected have the same number of elements
 */
MultiviewTriangulationDiagnosticsC MultiviewTriangulationC::Run(vector<Coordinate3DT>& triangulated, vector<CovarianceMatrixT>& covariances, CorrespondenceStackT& inliers, const CorrespondenceStackT& pairwiseCorrespondenceStack, const vector<CameraMatrixT>& cameras, const vector<vector<Coordinate2DT> >& projected, const MultiviewTriangulationParametersC& parameters)
{
	//Preconditions
	assert(cameras.size()==projected.size());

	ValidateParameters(parameters);

	MultiviewTriangulationDiagnosticsC diagnostics;

	//Initialisation
	CoordinateCorrespondenceStackT coordinateCorrespondenceStack=MakeCoordinateCorrespondenceStack(pairwiseCorrespondenceStack, projected);

	//Reprojection constraint
	CameraMatrixT dummyP;	//Dummy camera matrix
	ReprojectionErrorConstraintT reprojectionConstraint(TransferErrorH32DT(dummyP), parameters.inlierTh, 1);

	//Cheirality constraints
	vector<CheiralityConstraintC> cheiralityConstraint; cheiralityConstraint.reserve(cameras.size());
	for(const auto& current : cameras)
		cheiralityConstraint.emplace_back(current);

	//Pairwise triangulation
	MeasurementContainerT measurements;
	for(const auto& currentList : pairwiseCorrespondenceStack)
	{
		//Triangulate
		const CoordinateCorrespondenceStackT::value_type& currentCoordinateList=*coordinateCorrespondenceStack.find(currentList.first);
		vector<Coordinate3DT> pairwise3D=PairwiseTriangulation(currentCoordinateList, cameras, parameters);

		vector<size_t> filteredId;	//Indices of the correspondences that are represented in the output
		vector<Coordinate3DT> filtered3D;
		tie(filtered3D, filteredId)=ApplyConstraints(pairwise3D, currentCoordinateList, cameras, reprojectionConstraint, cheiralityConstraint, parameters);

		//Reorganise the correspondences and the 3D points into measurement records
		ProcessPairwise(measurements, filtered3D, filteredId, coordinateCorrespondenceStack[currentList.first], currentList.second, currentList.first);
	}	//for(const auto& currentList : pairwiseCorrespondenceStack)

	//Remove the outliers
	RemoveOutliers(measurements, cameras, reprojectionConstraint, cheiralityConstraint, parameters);

	//Fusion
	vector<Coordinate3DT> fused;
	vector<CovarianceMatrixT> fusedCovariances;
	std::tie(fused, fusedCovariances, inliers)=FuseMeasurements(measurements, cameras, parameters);
	std::tie(triangulated, covariances)=FilterStructure(inliers, fused, fusedCovariances, parameters);

	diagnostics.flagSuccess=true;
	diagnostics.nMeasurements=measurements.size();
	diagnostics.nOriginal3D=fused.size();
	diagnostics.nFiltered3D=triangulated.size();

	return diagnostics;
}	//MultiviewTriangulationDiagnosticsC static Run(vector<Coordinate3DT>& triangulated, CorrespondenceStackT& inliers, const CorrespondenceStackT& pairwiseCorrespondenceStack, const vector<CameraMatrixT>& cameras, const vector<vector<Coordinate2DT> >& projected, const MultiviewTriangulationParametersC& parameters)

}	//GeometryN
}	//SeeSawN

