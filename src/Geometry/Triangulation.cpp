/**
 * @file Triangulation.cpp Implementation of 2-view triangulation algorithm
 * @author Evren Imre
 * @date 5 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Triangulation.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Eigen triangulation
 * @param[in] mP1 First camera matrix
 * @param[in] mP2 Second camera matrix
 * @param[in] x1 Projection of the 3D point in the first image
 * @param[in] x2 Projection of the 3D point in the second image
 * @return Triangulated point
 * @remarks R. Hartley, A. Zisserman, "Multiple View Geometry in Computer Vision," 2nd Ed., 2003, pp.312
 */
Coordinate3DT TriangulatorC::EigenTriangulation(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const Coordinate2DT& x1, const Coordinate2DT& x2)
{
	Matrix<RealT, 4, 4> mA;	//Coefficient matrix
	mA.row(0)=x1[0]*mP1.row(2) - mP1.row(0);
	mA.row(1)=x1[1]*mP1.row(2) - mP1.row(1);
	mA.row(2)=x2[0]*mP2.row(2) - mP2.row(0);
	mA.row(3)=x2[1]*mP2.row(2) - mP2.row(1);

	JacobiSVD<Matrix<RealT, 4, 4>, Eigen::FullPivHouseholderQRPreconditioner> svd(mA, Eigen::ComputeFullU | Eigen::ComputeFullV);
	return svd.matrixV().col(3).hnormalized();
}	//Coordinate3DT EigenTriangulation(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const Coordinate2DT& x1, const Coordinate2DT& x2)

/**
 * @brief Optimal triangulation
 * @param[in] mP1 First camera matrix
 * @param[in] mP2 Second camera matrix
 * @param[in] mF Fundamental matrix for the camera pair
 * @param[in] x1 Projection in the first image
 * @param[in] x2 Projection in the second image
 * @param[in] flagFast If \c true , fast but suboptimal triangulation
 * @return Triangulated 3D point
 */
Coordinate3DT TriangulatorC::Triangulate(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2, bool flagFast)
{
	Coordinate2DT x1p;
	Coordinate2DT x2p;
	tie(x1p, x2p)= flagFast ? FastProjectToEpipolarManifold(mF, x1, x2) : OptimalProjectToEpipolarManifold(mF, x1, x2);

	return EigenTriangulation(mP1, mP2, x1p, x2p);
}	//optional<Coordinate3DT> Triangulate(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2, RealT inlierTh)

/**
 * @brief Triangulates a single corresponding pair
 * @param[in] mP1 First camera matrix
 * @param[in] mP2 Second camera matrix
 * @param[in] x1 Projection in the first image
 * @param[in] x2 Projection in the second image
 * @param[in] flagFast If \c true , fast but suboptimal triangulation
 * @return Triangulated point
 */
Coordinate3DT TriangulatorC::Triangulate(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const Coordinate2DT& x1, const Coordinate2DT& x2, bool flagFast)
{
	EpipolarMatrixT mF=CameraToFundamental(mP1, mP2);
	return Triangulate(mP1, mP2, mF, x1, x2, flagFast);
}	//optional<Coordinate3DT> Run(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const Coordinate2DT& x1, const Coordinate2DT& x2, bool flagCheirality)

/********** FREE FUNCTIONS **********/

/**
 * @brief Projects a 2D correspondence to the epipolar manifold defined by a fundamental matrix
 * @param[in] mF Fundamental matrix
 * @param[in] x1 First point
 * @param[in] x2 Second point
 * @return A tuple containing the projection of the pair to the epipolar manifold
 * @remarks R. Hartley, A. Zisserman, "Multiple View Geometry in Computer Vision," 2nd Ed., 2003, pp.312
 * @remarks Minimses the reporjection error
 */
tuple<Coordinate2DT, Coordinate2DT> OptimalProjectToEpipolarManifold(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2)
{
	//(i) Transformations that map x1 and x2 to the origins of their respective coordinate frames
	typedef Matrix<ValueTypeM<Coordinate2DT>::type, 3, 3> MatrixT;
	MatrixT mT1; mT1.setIdentity(); mT1(0,2)=-x1[0]; mT1(1,2)=-x1[1];
	MatrixT mT2; mT2.setIdentity(); mT2(0,2)=-x2[0]; mT2(1,2)=-x2[1];

	MatrixT mT1i=mT1.inverse();
	MatrixT mT2it=(mT2.inverse()).transpose();

	//(ii)
	EpipolarMatrixT mFii=mT2it * mF * mT1i;
	mFii/=mFii.norm();	//Normalize for numerical stability

	//(iii) Compute the epipoles
	JacobiSVD<EpipolarMatrixT,  Eigen::FullPivHouseholderQRPreconditioner> svd(mFii, Eigen::ComputeFullU | Eigen::ComputeFullV);
	HCoordinate2DT ve1=svd.matrixV().col(2); ve1/=hypot(ve1[0], ve1[1]);
	HCoordinate2DT ve2=svd.matrixU().col(2); ve2/=hypot(ve2[0], ve2[1]);

	//(iv) Rotation

	RotationMatrix3DT mR1; mR1.setZero();
	mR1(0,0)= ve1[0]; mR1(0,1)=ve1[1]; mR1(1,0)=-ve1[1]; mR1(1,1)=ve1[0]; mR1(2,2)=1;

	RotationMatrix3DT mR2; mR2.setZero();
	mR2(0,0)= ve2[0]; mR2(0,1)=ve2[1]; mR2(1,0)=-ve2[1]; mR2(1,1)=ve2[0]; mR2(2,2)=1;

	//(v) Transform so that the epipoles have the form (1,0,a)
	EpipolarMatrixT mFv= mR2 * mFii * mR1.transpose();

	//(vi)-(vii) Construct the polynomial equation
	double a=mFv(1,1);
	double b=mFv(1,2);
	double c=mFv(2,1);
	double d=mFv(2,2);
	double f1=ve1[2];
	double f2=ve2[2];

	double f12=pow<2>(f1);
	double f22=pow<2>(f2);

	double ad=a*d; double bc=b*c;
	double ac=a*c; double bd=b*d;
	double ab=a*b; double cd=c*d;

	double a2=pow<2>(a); double b2=pow<2>(b);
	double c2=pow<2>(c); double d2=pow<2>(d);

	double k1=ad+bc;
	double k2=ad-bc;
	double k3=a2+f22*c2;
	double k4=ab+f22*cd;
	double k5=b2+f22*d2;
	double k6=ac+f12*bd;
	double k14=k2*f12;

	array<double, 7> coefficients;
	coefficients[0]=k14*ac;
	coefficients[1]=pow<2>(k3)-k14*k1;
	coefficients[2]=4*k3*k4-k14*(k6+ac);
	coefficients[3]=2*(3*pow<2>(k4)+f22*pow<2>(k2)-k14*k1);
	coefficients[4]=4*k4*k5-k2*(k6+f12*bd);
	coefficients[5]=pow<2>(k5)-k2*k1;
	coefficients[6]=-bd*k2;

	//Solve the polynomial equations
	typedef complex<double> ComplexT;
	array<ComplexT, 6> roots;

	//If the epipoles are at infinity, the polynomial becomes t=0. To be handled as a special case
	if(f1!=0)
		roots=PolynomialRoots<6>(coefficients);

	//(viii) Find the root that minimises the cost

	//If epipole is not at infinity
	double tmin;
	if(! (f1==0 || f12==0) )	//numerical considerations
	{
		//Infinity is the 7th solution
		tmin=numeric_limits<double>::infinity();
		double bestCost=(double)1/f12 + c2/(a2+f22*c2);
		unsigned bestId=6;

		unsigned int index=0;

		for(auto current : roots)
		{
			if( fabs(current.imag()) < 3*numeric_limits<double>::epsilon())
			{
				double t2=pow<2>(current.real());

				double s1n=t2;
				double s1d=1+f22*t2;

				double m1= a*current.real()+b;
				double m2= c*current.real()+d;
				double s2n=pow<2>(m2);
				double s2d=pow<2>(m1)+f22*s2n;

				double term1 = s1n/s1d;
				double term2 = (m1==0 || m2==0) ? c2/(a2+f22*c2) : s2n/s2d;

				double cost=term1+term2;

				if(cost<bestCost)
				{
					bestCost=cost;
					bestId=index;
				}	//if(cost<bestCost)
			}	//if( fabs(current.imag()) < 3*numeric_limits<RealT>::epsilon())

			++index;
		}	//for(auto current : roots)

		if(bestId!=6)	//If t!=inf
			tmin=roots[bestId].real();
	}	//if(! (f1==0 || f12==0) )
	else
		tmin=0;	//Epipole at infinity

	//(ix) Compute the projections
	array<double, 3> l1;
	array<double, 3> l2;

	//If t=infinity
	if(tmin==numeric_limits<double>::infinity())
	{
		//Divide by tmin. The distances are independent of the scale factor- line is homogeneous
		l1[0]=f1; l1[1]=0; l1[2]=-1;
		l2[0]=-f2*c, l2[1]=a; l2[2]=c;
	}
	else
	{
		l1[0]=tmin*f1; l1[1]=1; l1[2]=-tmin;
		double m=c*tmin+d;
		l2[0]=-f2*m; l2[1]=a*tmin+b; l2[2]=m;
	}	//if(bestId==6)

	HCoordinate2DT x1pt(-l1[0]*l1[2], -l1[1]*l1[2], pow<2>(l1[0])+pow<2>(l1[1]) );
	HCoordinate2DT x2pt(-l2[0]*l2[2], -l2[1]*l2[2], pow<2>(l2[0])+pow<2>(l2[1]) );

	//(x) Transform back to the original reference frame
	Coordinate2DT x1p= (mT1i*mR1.transpose()*x1pt).hnormalized();
	Coordinate2DT x2p= (mT2it.transpose()*mR2.transpose()*x2pt).hnormalized();

	return make_tuple(x1p, x2p);
}	//tuple<Coordinate2DT, Coordinate2DT> ProjectToEpipolarManifold(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2)

/**
 * @brief Projects a 2D correspondence to the epipolar manifold defined by a fundamental matrix
 * @param[in] mF Fundamental matrix
 * @param[in] x1 First point
 * @param[in] x2 Second point
 * @return A tuple containing the projection of the pair to the epipolar manifold
 * @remarks P. Lindstrom, "Triangulation Made Easy," CVPR 2010
 * @remarks Suboptimal, but could be more accurate due to numerical issues
 * @remarks nIter1 variant, so that the backprojection lines intersect
 */
tuple<Coordinate2DT, Coordinate2DT> FastProjectToEpipolarManifold(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2)
{
	typedef ValueTypeM<EpipolarMatrixT>::type RealT;
	Matrix<RealT, 2, 3> mS; mS.setZero(); mS(0,0)=1; mS(1,1)=1;

	Matrix<RealT, 2, 3> mSE= mS*mF;
	Matrix<RealT, 2, 2> mSES= mSE*mS.transpose();

	Coordinate2DT n1 = mSE*x1.homogeneous();
	Coordinate2DT n2 = mS*mF.transpose()*x2.homogeneous();

	double a= n2.transpose()*mSES*n1;
	double b= 0.5*(n2.squaredNorm() + n1.squaredNorm());
	double c= x2.homogeneous().dot(mF*x1.homogeneous());
	double d= sqrt(pow<2>(b)-a*c);
	double lambda=c/(b+d);

	Coordinate2DT delta1=lambda*n1;
	Coordinate2DT delta2=lambda*n2;

	n1-=mSES*delta2;
	n2-=mSES.transpose()*delta1;

	delta1= (delta1.dot(n1)/n1.squaredNorm()) * n1;
	delta2= (delta1.dot(n2)/n2.squaredNorm()) * n2;

	tuple<Coordinate2DT, Coordinate2DT> output;
	get<0>(output)=x1- (mS.transpose()*delta1).segment(0,2);
	get<1>(output)=x2- (mS.transpose()*delta2).segment(0,2);

	return output;
}	//tuple<Coordinate2DT, Coordinate2DT> FastProjectToEpipolarManifold(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2)

}	//GeometryN
}	//SeeSawN

