/**
 * @file P3PSolver.cpp Implementation of the 3-point pose estimator
 * @author Evren Imre
 * @date 9 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P3PSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** P3PSolverC **********/

/**
 * @brief Computes the coordinates of the 3D points in the local reference frame
 * @param[in] edgeLengths Edge lengths of the triangle formed by the 3D points
 * @param[in] projectionRays Direction vectors indicating the projection rays for the 3D points
 * @return Up to 4 local coordinate sets, each corresponding to a solution
 */
list<array<Coordinate3DT,3 > > P3PSolverC::ComputeLocalCooordinates(const array<RealT,3>& edgeLengths, const array<Coordinate3DT,3>& projectionRays)
{
	list<array<Coordinate3DT,3 > > output;

	//Section 2

	RealT a=edgeLengths[0];
	RealT b=edgeLengths[1];
	RealT c=edgeLengths[2];

	if(a==0 || b==0 || c==0)
		return output;

	RealT cosa=projectionRays[1].dot(projectionRays[2]);
	RealT cosb=projectionRays[0].dot(projectionRays[2]);
	RealT cosc=projectionRays[0].dot(projectionRays[1]);

	//Section 3, Finsterwalder

	//Compute the lambda (Eq 12)

	RealT cosa2=pow<2>(cosa);
	RealT cosb2=pow<2>(cosb);
	RealT cosc2=pow<2>(cosc);
	RealT sina2=1-cosa2;
	RealT sinb2=1-cosb2;
	RealT sinc2=1-cosc2;

	RealT a2=pow<2>(a);
	RealT b2=pow<2>(b);
	RealT c2=pow<2>(c);

	//Coefficients of the polynomial equation
	RealT G=c2 *(c2*sinb2-b2*sinc2);
	RealT H=b2 *(b2-a2)*sinc2+c2*(c2+2*a2)*sinb2+2*b2*c2*(-1+cosa*cosb*cosc);
	RealT I=b2 *(b2-c2)*sina2+a2*(a2+2*c2)*sinb2+2*a2*b2*(-1+cosa*cosb*cosc);
	RealT J=a2*(a2*sinb2-b2*sina2);

	double tol2=1e4*numeric_limits<double>::epsilon();
	typedef complex<double> ComplexT;
	array<ComplexT,3> lambdaList=CubicPolynomialRoots({G,H,I,J}, tol2);	//High tolerance, as the root computations are prone to numerical errors

	//Any solution is ok. However, for stability, pick the one that maximises |C|
	//One of the roots will always lead to C=0 ! So unless there are 3 real roots, it is a singularity
	priority_queue< pair<double, double> > lambdaRanker;
	for(const auto& current : lambdaList)
	{
		if(current.imag()!=0)
			continue;

		double aC = fabs(b2-a2-current.real()*c2)/b2;

		if( aC < tol2)
			continue;

		lambdaRanker.emplace(aC, current.real());
	}	//for(const auto& current : lambdaList)

	//No lambda, no solution
	if(lambdaRanker.empty())
		return output;

	double lambda=lambdaRanker.top().second;

	//Compute u and v (Equation 11)

	double A=1+lambda;
	double B=-cosa;
	double C= (b2-a2-lambda*c2)/b2;
	double D=-lambda*cosc;
	double E=cosb*(a2+lambda*c2)/b2;
	double F=(-a2+lambda*(b2-c2))/b2;

	double B2=pow<2>(B);
	double E2=pow<2>(E);
	double AC=A*C;
	double CF=C*F;
	double BE=B*E;
	double CD=C*D;

	//The term in square root can be written as (pu+q)^2
	array<ComplexT,2> rootspq=QuadraticPolynomialRoots({ B2-AC,2*(BE-CD),E2-CF},tol2);

	if(rootspq[0].imag()!=0)	//Only check the first root: they are supposed to be identical
		return output;

	double p=sqrt(B2-AC);
	double q=-p*rootspq[0].real();

	//The equations relating u and v
	//If m~=0 Eq 8 is not satisfied, but still Eqs. 1-7 are ok. So, a valid solution is produced
	list<tuple<double, double> > listmn;
	listmn.emplace_back((-B+p)/C, (-E+q)/C);
	if(p!=0)	//Also implies q!=0
		listmn.emplace_back((-B-p)/C, (-E-q)/C);

	set<tuple<double, double> > listuv;	//Two m,n pairs, and for each, up to two solutions
	array<double, 3> coeffu;
	for(const auto& current : listmn)
	{
		//Substitute v=mu+n into Equation 6
		double m;
		double n;
		tie(m,n)=current;

		double a2ob2=a2/b2;
		double oneMa2ob2=1-a2ob2;

		coeffu[0]=1                    + pow<2>(m)*oneMa2ob2 - 2*m*cosa;
		coeffu[1]=2*m*n*oneMa2ob2     - 2*n*cosa             + 2*m*cosb*a2ob2;
		coeffu[2]=pow<2>(n)*oneMa2ob2 + 2*n*cosb*a2ob2       - a2ob2;
		if(fabs(coeffu[0]) < tol2)
		{
			double u=-coeffu[2]/coeffu[1];
			listuv.emplace(u, m*u+n);
		}
		else   //Else, it is degree 2
		{
			array<ComplexT,2> rootsu=QuadraticPolynomialRoots(coeffu, sqrt(tol2));

			if( fabs(rootsu[0].imag())>tol2 )
				continue;

			for_each(rootsu, [&](const ComplexT& item){ listuv.emplace(item.real(), m*item.real()+n);} );
		}	//if(fabs(coeffu[0]) < tol2)
	}	//for(const auto& current : listmn)

	//Now compute the coordinates of the scene points in the local frame
	array<Coordinate3DT,3> scene;
	for(const auto& current : listuv)
	{
		//Equation 5
		double u;
		double v;
		tie(u,v)=current;
		double s1=sqrt(a2/(pow<2>(u)+pow<2>(v)-2*u*v*cosa));

		scene[0]=s1*projectionRays[0];
		scene[1]=u*s1*projectionRays[1];
		scene[2]=v*s1*projectionRays[2];

		output.push_back(scene);
	}	//for(const auto& current : listuv)

	return output;
}	//optional<CoordinateCorrespondenceList3DT> ComputeLocalCooordinates(const array<RealT,3>& edgeLengths, const array<Coordinate3DT,3>& projectionRays)

/**
 * @brief Computational cost of the solver
 * @param[in] dummy Dummy variable
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double P3PSolverC::Cost(unsigned int dummy)
{
	//FIXME The cost is probably severely underestimated. However, op counting is not practical. It is better to express the cost in terms of another PnP solver. The fixed scale can be determined by direct measurements
	return Similarity3DSolverC::Cost(sGenerator);
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Camera matrix
 */
auto P3PSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	return ComposeCameraMatrix(get<iPosition>(minimalModel), RotationVectorToQuaternion(get<iOrientation>(minimalModel)).matrix());
}	//auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return \f$ 1- \frac{|<\mathbf{M_1, M_2}>|}{\sqrt{<\mathbf{M_1,M_1}><\mathbf{M_2,M_2}>}} \f$. [0,1]
 */
auto P3PSolverC::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	//Since the entities are not homogeneous, the dot-product trick does not work. Schwartz to the rescue!
	return 1-fabs((model1.cwiseProduct(model2)).sum())/(model1.norm()*model2.norm());
}	//auto ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type

/**
 * @brief Converts a model to a vector
 * @param[in] model Model
 * @return Camera matrix in vector form
 */
auto P3PSolverC::MakeVector(const ModelT& model) -> VectorT
{
	Similarity3DSolverC::model_type tmp=Similarity3DSolverC::model_type::Identity();
	tmp.block(0,0,3,4)=model;
	return Similarity3DSolverC::MakeVector(tmp);
}	//VectorT MakeVector(const ModelT& model)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel Vector
 * @param[in] dummy Dummy parameter to satisfy the concept requirements
 * @return A camera matrix
 */
auto P3PSolverC::MakeModel(const VectorT& vModel, bool dummy) -> ModelT
{
	return Similarity3DSolverC::MakeModel(vModel, dummy).block(0,0,3,4);
}	//ModelT MakeModel(const VectorT& vModel)

/********** PoseMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented pose transformations
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector. [Rotation vector(3); origin shift(3) ]
 * @remarks Difference: orientation and the origin of the transformation that takes \c op2 to \c op1 (6).
 */
auto PoseMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	result_type output;

	Rotation3DMinimalDifferenceC rotationDif;
	output.segment(0,3)=rotationDif(get<P3PSolverC::iOrientation>(op1), get<P3PSolverC::iOrientation>(op2));
	output.segment(3,3)=get<P3PSolverC::iPosition>(op1)-get<P3PSolverC::iPosition>(op2);
	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

