/**
 * @file P2PfSolver.h Public interface for the 2-point orientation and focal length solver
 * @author Evren Imre
 * @date 9 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2PF_SOLVER_H_8012343
#define P2PF_SOLVER_H_8012343

#include "P2PfSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{
class P2PfSolverC;	///< 2-point orientation and focal length solver
struct P2PfMinimalDifferenceC;	///< Difference functor for minimally-represented P2Pf solutions

}	//GeometryN
}	//SeeSawN

#endif /* P2PFSOLVER_H_ */
