/**
 * @file P6PSolver.h Public interface for the 6-point projective camera solver
 * @author Evren Imre
 * @date 4 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P6PSOLVER_H_7909003
#define P6PSOLVER_H_7909003

#include "P6PSolver.ipp"

namespace SeeSawN
{
namespace GeometryN
{
class P6PSolverC;	///< 6-point projective camera solver
struct CameraMinimalDifferenceC;	///< Difference functor for minimally-represented camera matrices

}	//GeometryN
}	//SeeSawN

#endif /* P6PSOLVER_H_7909003 */
