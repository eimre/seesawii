/**
 * @file EssentialSolver.h Public interface for the 5-point essential matrix solver
 * @author Evren Imre
 * @date 5 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ESSENTIAL_SOLVER_IPP_5092859
#define ESSENTIAL_SOLVER_IPP_5092859

#include "EssentialSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{
class EssentialSolverC;	///< 5-point essential matrix estimator
}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<double>;


#endif /* ESSENTIAL_SOLVER_IPP_5092859 */
