/**
 * @file LensDistortion.cpp Implementation of various lens distortion models
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "LensDistortion.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** LensDistortionNoDC **********/
/**
 * @brief Default constructor
 */
LensDistortionNoDC::LensDistortionNoDC()
{}

/**
 * @brief Constructor
 * @param[in] ccentre Distortion centre
 * @param[in] kkappa Distortion coefficient, 1/pixel
 */
LensDistortionNoDC::LensDistortionNoDC(const Coordinate2DT& ccentre, RealT kkappa)
{}

/**
 * @brief Applies lens distortion to a point
 * @param[in] undistorted Undistorted point
 * @return \c undistorted
 */
optional<Coordinate2DT> LensDistortionNoDC::Apply(const Coordinate2DT& undistorted) const
{
	return undistorted;
}	//bool Apply(Coordinate2DT& distorted, const Coordinate2DT& undistorted) const

/**
 * @brief Corrects a distorted point
 * @param[in] distorted Distorted point
 * @return \c distorted
 */
optional<Coordinate2DT> LensDistortionNoDC::Correct(const Coordinate2DT& distorted) const
{
	return distorted;
}	//bool Correct(Coordinate2DT& corrected, const Coordinate2DT& distorted) const

/********** LensDistortionFP1C **********/

/**
 * @brief Default constructor
 * @post An invalid object
 */
LensDistortionFP1C::LensDistortionFP1C() : kappa(0), flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] ccentre Distortion centre
 * @param[in] kkappa Distortion coefficient, 1/pixel
 * @post A valid object
 */
LensDistortionFP1C::LensDistortionFP1C(const Coordinate2DT& ccentre, RealT kkappa) : centre(ccentre), kappa(kkappa), flagValid(true)
{}

/**
 * @brief Verifies whether both the distortion and the undistortion operations can be performed successfully
 * @param[in] radius Distance of the distorted point to the distortion centre
 * @return \c false if \c Correct cannot be performed successfully
 * @remarks \c Correct fails if the discriminant of \f$ \kappa r q^2 + q - 1 =0\f$ is negative.
 */
bool LensDistortionFP1C::Verify(RealT radius) const
{
	return (kappa>=0) || (kappa*radius>=-0.25);
}	//bool Verify(RealT radius)

/**
 * @brief Applies lens distortion to a point
 * @param[in] undistorted Undistorted point
 * @return Distorted coordinates. Invalid if correction may fail
 * @pre \c flagValid=true
 */
optional<Coordinate2DT> LensDistortionFP1C::Apply(const Coordinate2DT& undistorted) const
{
    //Preconditions
    assert(flagValid);
    RealT radius=(undistorted-centre).norm();

    if(Verify(radius*(fma(kappa, radius, 1))))
    	return (centre + (1 + kappa*radius) * (undistorted-centre)).eval();
    else
    	return optional<Coordinate2DT>();
}   //Coordinate2DT Apply(const Coordinate2DT& undistorted)

/**
 * @brief Corrects a distorted point
 * @param[in] distorted Distorted point
 * @return Corrected coordinates. Invalid if the correction terms are complex
 * @pre \c flagValid=true
 */
optional<Coordinate2DT> LensDistortionFP1C::Correct(const Coordinate2DT& distorted) const
{
    //Preconditions
    assert(flagValid);
    RealT radius=(distorted-centre).norm();

    //If kappa or radius is zero, no further operation necessary (also, if radius*kappa=0, the equation for the correction term is no longer a quadratic polynomial)
    if(radius==0 || kappa==0)
        return distorted;

    //Solve for the correction terms

    //Is the discriminant positive?
    if(!Verify(radius))
    	return optional<Coordinate2DT>();

    array<complex<double>,2> roots=QuadraticPolynomialRoots({{kappa*radius, 1, -1}});

    //If kappa>0, single positive root
    RealT r=1;
    if(kappa>0)
         r = (roots[0].real()>0) ? roots[0].real() : roots[1].real();
    //Otherwise, two positive roots. Pick the one closest to 1
    else
    {
        RealT r1=roots[0].real();
        RealT r2=roots[1].real();
        r=( fabs(r1-1)<fabs(r2-1) ) ? r1 : r2;
    }   //if(kappa>0)

    return (centre + r*(distorted-centre)).eval();  //Apply the correction
}   //Coordinate2DT Correct(const Coordinate2DT& distorted)

/********** LensDistortionOP1C **********/

/**
 * @brief Default constructor
 * @post An invalid object
 */
LensDistortionOP1C::LensDistortionOP1C() : kappa(0), flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] ccentre Distortion centre
 * @param[in] kkappa Distortion coefficient, 1/pixel
 * @post A valid object
 */
LensDistortionOP1C::LensDistortionOP1C(const Coordinate2DT& ccentre, RealT kkappa) : centre(ccentre), kappa(kkappa), flagValid(true)
{}

/**
 * @brief Applies lens distortion to a point
 * @param[in] undistorted Undistorted point
 * @return Distorted point
 * @pre \c flagValid=true
 */
optional<Coordinate2DT> LensDistortionOP1C::Apply(const Coordinate2DT& undistorted) const
{
    //Preconditions
    assert(flagValid);
    RealT radius2=(undistorted-centre).squaredNorm();
    return (centre + (1 + kappa*radius2) * (undistorted-centre)).eval();
}   //Coordinate2DT Apply(const Coordinate2DT& undistorted)

/**
 * @brief Corrects a distorted point
 * @param[in] distorted Distorted point
 * @return Corrected point
 * @pre \c flagValid=true
 */
optional<Coordinate2DT> LensDistortionOP1C::Correct(const Coordinate2DT& distorted) const
{
    //Preconditions
    assert(flagValid);

    RealT radius=(distorted-centre).norm();

    //If kappa or radius is zero, no further operation necessary (also, if radius*kappa=0, the equation for the correction term is no longer a cubic polynomial)
     if(radius==0 || kappa==0)
    	 return distorted;

     //Solve for the correction terms
     array<complex<double>,3> roots=CubicPolynomialRoots({{kappa*pow<2>(radius), 0, 1, -1}});

     //Find the real root closest to 1
     double minDiff=numeric_limits<double>::infinity();
     double r=1;
     for(const auto& current : roots)
     {
         if(current.imag()!=0)
             continue;

         RealT currDif=fabs(current.real()-1);
         if(currDif<minDiff)
         {
             minDiff=currDif;
             r=current.real();
         }  //if(currDif<minDiff)
     }  //for(const auto& current : roots)

    return (centre+(distorted-centre)*r).eval();  //Apply the correction
}   //bool Correct(Coordinate2DT& corrected, const Coordinate2DT& distorted) const

/********** LensDistortionDivC **********/

/**
 * @brief Default constructor
 * @post An invalid object
 */
LensDistortionDivC::LensDistortionDivC() : kappa(0), flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] ccentre Distortion centre
 * @param[in] kkappa Distortion coefficient, 1/pixel
 * @post A valid object
 */
LensDistortionDivC::LensDistortionDivC(const Coordinate2DT& ccentre, RealT kkappa) : centre(ccentre), kappa(kkappa), flagValid(true)
{}

/**
 * @brief Verifies whether both the distortion and the undistortion operations can be performed successfully
 * @param[in] ru Distance of the undistorted point to the distortion centre
 * @param[in] rd Distance of the distorted point to the distortion centre
 * @return \c false if \c Apply cannot be completed successfully
 */
bool LensDistortionDivC::Verify(RealT ru, RealT rd) const
{
	return (kappa*pow<2>(ru)<=1) && (kappa*pow<2>(rd)>=-1);
}	//bool Verify(RealT radius) const

/**
 * @brief Applies lens distortion to a point
 * @param[in] undistorted Undistorted point
 * @return Distorted point. If \f$ \kappa r_u^2 > 1 \f$ , invalid.
 * @warning If \f$ \kappa r_u^2 > 0.25 \f$ , the result is approximate.
 * @pre \c flagValid=true
 */
optional<Coordinate2DT> LensDistortionDivC::Apply(const Coordinate2DT& undistorted) const
{
    //Preconditions
    assert(flagValid);

    if(kappa==0)
    	return undistorted;

    //Solve for rd
    RealT ru=(undistorted-centre).norm();
    array<complex<double>,2> roots=QuadraticPolynomialRoots({{kappa*ru, -1, ru}});

    //Real roots?
    RealT rd;
    bool flagExact=false;
    if(roots[0].imag()==0)
    {
        flagExact=true;

        //If kappa<0, only one positive root
        if(kappa<0)
            rd = (roots[0].real()>0) ? roots[0].real() : roots[1].real();
        else
            //Pick the smaller root (1-discriminant solution. 1+discriminant solution does not tend to ru as k->0)
            rd=(roots[0].real()<roots[1].real()) ? roots[0].real() : roots[1].real();
    }   //if(roots[0].imag()==0)

    //If no real roots, try an approximate solution
    if(!flagExact)
        rd=ru/(1-kappa*pow<2>(ru)); //First order Taylor approximation to ru=rd/(1+ kappa rd^2), expanded around 0, as a function of kappa

    if(rd<0)
        return optional<Coordinate2DT>();

    //Check whether Correct will succeed as well
    if(!Verify(ru, rd))
    	return optional<Coordinate2DT>();

    return (centre+(undistorted-centre)*(rd/ru)).eval();
}   //Coordinate2DT Apply(const Coordinate2DT& undistorted)

/**
 * @brief Corrects a distorted point
 * @param[in] distorted Distorted point
 * @return Corrected point. If \c kappa*rd2<-1 , invalid.
 * @pre \c flagValid=true
 */
optional<Coordinate2DT> LensDistortionDivC::Correct(const Coordinate2DT& distorted) const
{
    //Preconditions
    assert(flagValid);

    RealT rd2=(distorted-centre).squaredNorm();
    RealT rd=sqrt(rd2);
    RealT ru=rd/(1+kappa*rd2);

    if(ru<0)
        return optional<Coordinate2DT>();

    //Check whether Apply will succeed as well
    if(!Verify(ru, rd))
    	return optional<Coordinate2DT>();

    return (centre+(distorted-centre)*ru/rd).eval();
}   //bool Correct(Coordinate2DT& corrected, const Coordinate2DT& distorted) const

/********** EXPLICIT INSTANTIATIONS **********/
template vector<optional<Coordinate2DT> > ApplyDistortion(const vector<Coordinate2DT>&, LensDistortionCodeT, const Coordinate2DT&, double);
template vector<optional<Coordinate2DT> > ApplyDistortionCorrection(const vector<Coordinate2DT>&, LensDistortionCodeT, const Coordinate2DT&, double);

}   //GeometryN
}   //SeeSawN
