/**
 * @file P2PfSolver.ipp Implementation of the 2-point orientation and focal length solver
 * @author Evren Imre
 * @date 9 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2PF_SOLVER_IPP_6509812
#define P2PF_SOLVER_IPP_6509812

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/tuple/tuple.hpp>
#include <type_traits>
#include <list>
#include <tuple>
#include <array>
#include <complex>
#include <cmath>
#include <vector>
#include "GeometrySolverBase.h"
#include "P2PSolver.h"
#include "Rotation3DSolver.h"
#include "Rotation.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/Complexity.h"
#include "../Numeric/Polynomial.h"
#include "../Wrappers/BoostRange.h"
#include "../Wrappers/EigenExtensions.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::range::for_each;
using boost::ForwardRangeConcept;
using std::is_same;
using std::list;
using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;
using std::array;
using std::complex;
using std::max;
using std::sqrt;
using std::vector;
using SeeSawN::GeometryN::P2PSolverC;
using SeeSawN::GeometryN::Rotation3DMinimalDifferenceC;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::NumericN::ComplexityEstimatorC;
using SeeSawN::NumericN::QuadraticPolynomialRoots;
using SeeSawN::WrappersN::MakeBinaryZipRange;
using SeeSawN::WrappersN::Reshape;

//Forward declarations
struct P2PfMinimalDifferenceC;

/**
 * @brief 2-point orientation and focal length solver
 * @remarks E. Imre, J.-Y. Guillemaut, A. Hilton, "Calibration of Nodal and Free-Moving Cameras in Dynamic Scenes for Post-Production", 3DIMPVT 2011
 * @remarks The solver assumes that
 * 	- The camera centre is at the world origin. Therefore subtract the known camera centre from the scene points
 * 	- The principal point is the origin of the image plane, aspect ratio is 1 and skewness is 0. Therefore remove the effect of the known calibration parameters
 * 	- The scene points satisfy the cheirality constraints, i.e. in front of the camera. When this cannot be guaranteed, the solver will still return a valid rotation, however, it is unlikely to gather much support.
 * @remarks Distance metric: 1-|<u,v>|/sqrt(<u,u><v,v>). Range=[0,1]
 * @remarks Minimal parameterisation: Focal length and the rotation vector
 * @remarks For interoperability with the other calibration estimators, the scaled rotation matrix is embedded into a camera matrix
 * @warning For the normalisation of the coordinate magnitudes (i.e. following the removal of the effect of the known calibration parameters), avoid non-isotropic scaling!
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class P2PfSolverC : public GeometrySolverBaseC<P2PfSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 2, false, 2, 12, 4>
{
	private:

		typedef GeometrySolverBaseC<P2PfSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 2, false, 2, 12, 4> BaseT;	///< Type of the geometry solver base

		typedef typename BaseT::real_type RealT;	///< Floating point type
		typedef typename BaseT::model_type ModelT;	///<  CameraMatrixT which contains the rotation
		typedef MultivariateGaussianC<P2PfMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

	public:


		/** @name GeometrySolverConceptC interface */ //@{
		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the P2Pf solution that best explains the correspondence set

		static double Cost(unsigned int dummy=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<RealT, RotationVectorT>  minimal_model_type;	///< A minimally parameterised model. Focal length; Axis-angle vector
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class SampleRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of orientation and focal length estimates

		//@}

		/** @name Overrides */ //@{
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool dummy=true);	///< Converts a vector to a model
		//@}

		/** @name Minimally parameterised model */ //@{
		static constexpr unsigned int iFocalLength=0;	///< Index of the focal length component of a minimal representation
		static constexpr unsigned int iOrientation=1;	///< Index of the orientation component of a minimal representation
		//@}
};	//class P2PSolverC

/**
 * @brief Difference functor for minimally-represented P2Pf solutions
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct P2PfMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef P2PfSolverC::minimal_model_type first_argument_type;
	typedef P2PfSolverC::minimal_model_type second_argument_type;
	typedef Matrix<P2PfSolverC::real_type, P2PfSolverC::DoF(), 1> result_type;	///< Difference in vector form

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between two minimally-represented P2Pf solutions
	//@}
};	//struct P2PfMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre The first 3x3 submatrix can be decomposed into a diagonal and a rotation matrix (unenforced)
 * @return Minimal form
 */
template<class DummyRangeT>
auto P2PfSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	IntrinsicCalibrationMatrixT mK;
	RotationMatrix3DT mR;
	Coordinate3DT vC;
	tie(mK, vC, mR)=DecomposeCamera(model, false);

	return make_tuple( 0.5*(mK(0,0)+mK(1,1)), RotationMatrixToRotationVector(mR) );
}	//auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of orientation and focal length estimates
 * @tparam CameraRangeT A range of camera matrices
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] samples Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c CameraRangeT is a forward range of elements of type \c CameraMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c homographies should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c cameraList has 4 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class SampleRangeT, class WeightRangeT, class DummyRangeT>
auto P2PfSolverC::ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<SampleRangeT>));
	static_assert(is_same<ModelT, typename range_value<SampleRangeT>::type >::value, "P2PfSolverC::ComputeSampleStatistics : SampleRangeT must hold elements of type CameraMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "P2PfSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(samples)==boost::distance(wMean));
	assert(boost::distance(samples)==boost::distance(wCovariance));

	size_t nSamples=boost::distance(samples);

	//Decompose the models into focal length and orientation components
	vector<QuaternionT> qList; qList.reserve(nSamples);
	vector<minimal_model_type> minimalList; minimalList.reserve(nSamples);
	double meanf=0;
	for(const auto& current : MakeBinaryZipRange(samples, wMean))
	{
		minimalList.push_back(MakeMinimal(boost::get<0>(current)));
		meanf+= get<iFocalLength>(*minimalList.rbegin()) *boost::get<1>(current);
		qList.push_back(RotationVectorToQuaternion(get<iOrientation>(*minimalList.rbegin())));
	}	//for(const auto& current : MakeBinaryZipRange(cameraList, wMean))

	//Mean

	QuaternionT qMean;
	Matrix<RealT, 3, 3> mCovQ;
	tie(qMean, mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	minimal_model_type meanModel=make_tuple(meanf, QuaternionToRotationVector(qMean));

	//Covariance
	P2PfMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	for(const auto& current : MakeBinaryZipRange(minimalList, wCovariance))
	{
		typename P2PfMinimalDifferenceC::result_type vDif=subtractor(boost::get<0>(current), meanModel);
		mCov+=boost::get<1>(current) * (vDif * vDif.transpose());
	}	//for(const auto& current : MakeBinaryZipRange(samples, wCovariance))

	return GaussianT(meanModel, mCov);
}	//auto ComputeSampleStatistics(const RotationRangeT& rotations, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT


/**
 * @brief Computes the rotation that best explains the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondence set
 * @return Up to 2 camera matrices. Empty if the solver fails, or \c correspondences does not have exactly 2 elements
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap of elements of type \c Coordinate3DT and \c Coordinate2DT
 */
template<class CorrespondenceRangeT>
auto P2PfSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "P2PfSolverC::operator() : CorrespondenceRangeT::left_key_type must be Coordinate3DT");
	static_assert(is_same<Coordinate2DT, typename CorrespondenceRangeT::right_key_type>::value, "P2PfSolverC::operator() : CorrespondenceRangeT::right_key_type must be Coordinate2DT");

	list<ModelT> output;

	if(boost::distance(correspondences)!=sGenerator)
		return output;

	//Compute the focal length

	//Compute the cosine of the angle between the projection rays from the law of cosines
	RealT d102=correspondences.begin()->left.squaredNorm();
	RealT d202=correspondences.rbegin()->left.squaredNorm();
	RealT d122=(correspondences.begin()->left-correspondences.rbegin()->left).squaredNorm();
	RealT cosg= (d102 + d202 - d122)/(2*sqrt(d102*d202));

	//Compute cosg as the dot product of the projection rays, as a function of f
	RealT u2=correspondences.begin()->right.squaredNorm();
	RealT v2=correspondences.rbegin()->right.squaredNorm();
	RealT uv=correspondences.begin()->right.dot(correspondences.rbegin()->right);
	RealT cos2g=cosg*cosg;

	array<RealT,3> coeff{ {1-cos2g, 2*uv-cos2g*(u2+v2), uv*uv-cos2g*u2*v2} };
	array<complex<double>,2> roots=QuadraticPolynomialRoots(coeff);	//Quadratic in f^2

	//For each positive root, solve the P2P problem
	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity();
	for(const auto& current : roots )
	{
		//Real and positive
		if(current.imag()!=0 || current.real()<=0)
			continue;

		RealT f=sqrt(current.real());

		//Solve for the orientation
		CoordinateCorrespondenceList32DT normalised;
		for(const auto& current2 : correspondences)
			normalised.push_back(typename CoordinateCorrespondenceList32DT::value_type(current2.left, current2.right));

		normalised.begin()->right/=f;
		normalised.rbegin()->right/=f;

		P2PSolverC orientationSolver;
		list<CameraMatrixT> orientation=orientationSolver(normalised);

		if(orientation.empty())
			continue;

		mK(0,0)=f; mK(1,1)=f;
		output.push_back(mK* (*orientation.begin()));
	}	//for(const auto& current : roots )

	return output;
}	//auto operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>

}	//GeometryN
}	//SeeSawN

#endif /* P2PF_SOLVER_IPP_6509812 */
