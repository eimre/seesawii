/**
 * @file RelativeRotationRegistration.ipp Implementation details for absolute rotation estimation from relative rotations
 * @author Evren Imre
 * @date 28 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RELATIVE_ROTATION_REGISTRATION_IPP_4190921
#define RELATIVE_ROTATION_REGISTRATION_IPP_4190921

#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <vector>
#include <list>
#include <tuple>
#include <cstddef>
#include <unordered_set>
#include <utility>
#include <limits>
#include <iterator>
#include <cmath>
#include "../Elements/GeometricEntity.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/BoostGraph.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::for_each;
using Eigen::Matrix;
using Eigen::JacobiSVD;
using std::vector;
using std::tuple;
using std::tie;
using std::get;
using std::size_t;
using std::unordered_set;
using std::pair;
using std::make_pair;
using std::list;
using std::numeric_limits;
using std::advance;
using std::acos;
using std::fabs;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::FindConnectedComponents;
using SeeSawN::NumericN::ComputeClosestRotationMatrix;

/**
 * @brief Estimates a set of absolute rotations generating the observed relative rotations
 * @remarks The algorithm assumes that the relative rotations successfully constrain the absolute solution, i.e. form a connected graph, , and for weighted operation, non-zero weights
 * @remarks Ref 1: "Robust Rotation and Translation Estimation in Multiview Reconstruction," D. Martinec and T. Padjla, CVPR 2007
 * @remarks The relative rotations rotate the first camera to the reference frame of the second
 * @remarks The minimum number of constraints is one less than the number of cameras
 * @remarks The class bundles two solvers
 * - Minimal solver: When the number of constraints is one less than the number of cameras, just chaining the rotations yields a consistent solution
 * - LS solver (R1): When there are at least as many relative constraints as the number of cameras.
 * @remarks: Dealing with the sign ambiguity in the LS solution
 * 	- Problem: The LS solution can produce a set of rotations with negative determinants, i.e. "improper rotations"
 * 	- Solution: Compute the determinant of each solution candidate, and multiply by -1 if it is negative, before projection to the closest rotation matrix. This is expensive, but affordable, compared to the projection step
 * 	- In the absence of noise, it is sufficient to test the sign on a single solution candidate. However, with noise, this is risky
 * @ingroup Algorithm
 * @nosubgrouping
 */
class RelativeRotationRegistrationC
{
	private:

		typedef ValueTypeM<RotationMatrix3DT>::type RealT;	///< Floating point type
		typedef Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> MatrixT;	///< Floating point matrix type
		typedef Matrix<RealT, Eigen::Dynamic, 3> Matrix3T;	///< Nx3 floating point matrix type

		typedef tuple<unsigned int, unsigned int, RotationMatrix3DT, RealT> ObservationT;	///< Type describing an observation
		typedef tuple<unsigned int, unsigned int, QuaternionT, RealT> ObservationQT;	///< Type describing an observation, quaternion variant

		/** @name Implementation details */ //@{
		static MatrixT MakeCoefficientMatrix(const vector<ObservationT>& observations, size_t nCameras, bool flagWeighted);	///< Constructs the equation system
		static vector<RotationMatrix3DT> MakeSolution(const Matrix3T& solution);	///< Converts the solution vector to rotation matrices
		static vector<RotationMatrix3DT> SolveLS(const vector<ObservationT>& observations, size_t nCameras, bool flagWeighted);	///< LS solver
		static vector<RotationMatrix3DT> SolveMinimal(const vector<ObservationT>& observations);	///< Minimal solver
		//@}

	public:

		/** @name ObservationT definition*/ //@{
		typedef ObservationT observation_type;
		typedef ObservationQT quaternion_observation_type;
		static constexpr size_t iIndex1=0;	///< Index of the component holding the id of the first camera.
		static constexpr size_t iIndex2=1;	///< Index of the component holding the id of the second camera
		static constexpr size_t iRotation=2;	///< Index of the component for the rotation matrix
		static constexpr size_t iWeight=3;	///< Index of the component for the weight of individual observations
		//@}

		typedef RealT real_type;	///< Floating point type

		/** @name Operations */ //@{
		static vector<RotationMatrix3DT> Run(const vector<ObservationT>& observations, bool flagWeighted);	///< Runs the algorithm
		//@}

		/** @name Utility */ ///@{
		static bool ValidateObservations(const vector<ObservationT>& observations);	///< Checks whether an observation set satisfies the preconditions

		static RealT ComputeAngularDeviation(const RotationMatrix3DT& mR1, const RotationMatrix3DT& mR2);	///< Computes the angular deviation between two rotations
		static RealT ComputeAngularDeviation(const QuaternionT& q1, const QuaternionT& q2);	///< Computes the angular deviation between two rotations
		///@}
};	//class RelativeRotationRegistrationC

}	//GeometryN
}	//SeeSawN


#endif /* RELATIVE_ROTATION_REGISTRATION_IPP_4190921 */
