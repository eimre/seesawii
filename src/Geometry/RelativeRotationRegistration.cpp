/**
 * @file RelativeRotationRegistration.cpp Implementation of absolute rotation estimation from relative rotations
 * @author Evren Imre
 * @date 28 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RelativeRotationRegistration.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Constructs the equation system
 * @param[in] observations Observations
 * @param[in] nCameras Number of cameras
 * @param[in] flagWeighted If \c true , weighted LS
 * @return Coefficient matrix
 */
auto RelativeRotationRegistrationC::MakeCoefficientMatrix(const vector<ObservationT>& observations, size_t nCameras, bool flagWeighted) -> MatrixT
{
	//Coefficients
	size_t nObservations=observations.size();
	size_t nEquations=3*nObservations;

	MatrixT mA(nEquations, 3*nCameras); mA.setZero();
	RotationMatrix3DT mR;
	size_t iEquation=0;
	for(size_t c=0; c<nObservations; ++c, iEquation+=3)
	{
		size_t i1;
		size_t i2;
		RealT weight;
		tie(i1, i2, mR, weight)=observations[c];

		if(flagWeighted)
			mR*=weight;

		i1*=3;
		i2*=3;

		mA.block(iEquation,i1,3,3) = mR;
		mA.block(iEquation,i2,3,3).diagonal().setConstant( flagWeighted ? -weight : -1);
	}	//for(size_t c=0; c<nObservations; ++c)

	return mA;
}	//MatrixT MakeCoefficientMatrix(const vector<ObservationT>& observations, bool flagWeighted)

/**
 * @brief Converts the solution vector to rotation matrices
 * @param[in] solution Solution vector
 * @return A vector of rotation matrices
 */
vector<RotationMatrix3DT> RelativeRotationRegistrationC::MakeSolution(const Matrix3T& solution)
{
	Matrix<RealT,3,3> mW=solution.block(0, 0, 3, 3);

	size_t nCamera=solution.rows()/3;
	vector<RotationMatrix3DT> output; output.reserve(nCamera);
	size_t iSolution=0;
	for(size_t c=0; c<nCamera; ++c, iSolution+=3)
	{
		 mW=solution.block(iSolution, 0, 3, 3);

		 if(mW.determinant()<0)	//Projection to the closest rotation matrix involves a SVD. 3x3 determinant is relatively cheaper
			 mW*=-1;

		 output.push_back( ComputeClosestRotationMatrix(mW) );
	}	//for(size_t c=0; c<nCamera; ++c)

	return output;
}	//vector<RotationMatrix3DT> MakeSolution(const VectorT& solution)

/**
 * @brief LS solver
 * @param[in] observations Observations
 * @param[in] nCameras Number of cameras
 * @param[in] flagWeighted flagWeighted If \c false , the weight components are ignored. Otherwise, weighted LS solution
 * @return A vector holding a rotation matrix for each camera. Empty if the equation system is ill-conditioned
 */
vector<RotationMatrix3DT> RelativeRotationRegistrationC::SolveLS(const vector<ObservationT>& observations, size_t nCameras, bool flagWeighted)
{
	//Build the equation system
	MatrixT mA=MakeCoefficientMatrix(observations, nCameras, flagWeighted);

	//Solve the equation system
	//TODO This is a sparse system
	JacobiSVD<MatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd(mA, Eigen::ComputeFullU | Eigen::ComputeFullV);

	//If the null space has more than 3 basis vectors, no unique solution
	svd.setThreshold(3*numeric_limits<RealT>::epsilon());
	if(svd.rank()<mA.cols()-3)
		return vector<RotationMatrix3DT>();

	//Project to the space of rotation matrices
	Matrix3T solution=svd.matrixV().rightCols(3);
	return MakeSolution(solution);
}	//vector<RotationMatrix3DT> SolveLS(const vector<ObservationT>& observations, bool flagWeighted)

/**
 * @brief Minimal solver
 * @param[in] observations Observations
 * @return A vector holding a rotation matrix for each camera. Empty if the observations do not form a tree
 */
vector<RotationMatrix3DT> RelativeRotationRegistrationC::SolveMinimal(const vector<ObservationT>& observations)
{
	size_t nObservations=observations.size();
	size_t nCameras=nObservations+1;
	vector<bool> flagFixed(nCameras, false);

	list<tuple<unsigned int, unsigned int, size_t> > edges;
	for(size_t c=0; c<nObservations; ++c)
		edges.emplace_back( get<iIndex1>(observations[c]), get<iIndex2>(observations[c]), c);

	vector<RotationMatrix3DT> output(nCameras);

	flagFixed[0]=true;
	output[0]=RotationMatrix3DT::Identity();

	//Loop through the edges, and fix the rotations
	auto it=edges.begin();
	auto itE=edges.end();
	unsigned int i1, i2;
	size_t index;
	do
	{
		tie(i1, i2, index)=*it;

		//Skip for the moment
		if( !flagFixed[i1] && !flagFixed[i2])
			advance(it,1);
		else
		{
			if(flagFixed[i1])
			{
				output[i2] = get<iRotation>(observations[index]) * output[i1];
				flagFixed[i2]=true;
			}
			else
			{
				output[i1] = get<iRotation>(observations[index]).transpose() * output[i2];
				flagFixed[i1]=true;
			}	//if(flagFixed[i1])

			it=edges.erase(it);
			itE=edges.end();
		}	//if( !flagFixed[i1] && !flagFixed[i2])

		if(it==itE)
			it=edges.begin();
	}while(!edges.empty());

	return output;
}	//vector<RotationMatrix3DT> SolveMinimal(const vector<ObservationT>& minimalSolver)

/**
 * @brief Runs the algorithm
 * @param[in] observations Observations
 * @param[in] flagWeighted If \c false , the weight components are ignored. Otherwise, weighted LS solution. The minimal solver ignores the weights
 * @pre The observations form a connected graph (unenforced)
 * @return A vector holding the absolute orientation for each camera. Empty if the problem has too few constraints
 */
vector<RotationMatrix3DT> RelativeRotationRegistrationC::Run(const vector<ObservationT>& observations, bool flagWeighted)
{
	//Get the number of cameras
	unordered_set<unsigned int> indexList;
	for(const auto& current : observations)
	{
		//Invalid constraint
		if(get<iIndex1>(current) == get<iIndex2>(current) )
			return vector<RotationMatrix3DT>();

		indexList.insert(get<iIndex1>(current));
		indexList.insert(get<iIndex2>(current));
	}	//for(const auto& current : observations)

	size_t nCameras=indexList.size();

	//There must be at least two cameras and one observation
	if(nCameras<2 || (observations.size()<nCameras-1) )
		return vector<RotationMatrix3DT>();

	//Minimal solver
	if(observations.size()==(nCameras-1) )
		return SolveMinimal(observations);

	//LS solver
	return SolveLS(observations, nCameras, flagWeighted);
}	//vector<RotationMatrix3DT> Run(const vector<ObservationT>& observations)

/**
 * @brief Checks whether an observation set satisfies the preconditions
 * @param[in] observations Observations
 * @return \c false if the preconditions are not satisfied
 */
bool RelativeRotationRegistrationC::ValidateObservations(const vector<ObservationT>& observations)
{
	//Edge map
	typedef pair<unsigned int, unsigned int> EdgeT;
	vector<EdgeT> edgeList; edgeList.reserve(observations.size());
	for(const auto& current : observations)
	{
		//Relative rotation to itself
		if(get<iIndex1>(current)==get<iIndex2>(current) )
			return false;

		edgeList.emplace_back(get<iIndex1>(current), get<iIndex2>(current));
	}	//for(const auto& current : observations)

	//Connectendness
	vector<list<EdgeT> > components = FindConnectedComponents(edgeList, true);

	return (!components.empty()) && (components[0].size() == observations.size() );
}	//bool ValidateObservations(const vector<ObservationT>& observations)

/**
 * @brief Computes the angular deviation between two rotations
 * @param[in] mR1 First rotation
 * @param[in] mR2 Second rotation
 * @return Absolute value of the angle of the residual rotation
 */
auto RelativeRotationRegistrationC::ComputeAngularDeviation(const RotationMatrix3DT& mR1, const RotationMatrix3DT& mR2) -> RealT
{
	return 2*acos(QuaternionT(mR1*mR2.transpose()).normalized().w());
}	//RealT ComputeAngularDeviation(const RotationMatrix3DT& mR1, const RotationMatrix3DT& mR2)

/**
 * @brief Computes the angular deviation between two rotations
 * @param[in] q1 First rotation
 * @param[in] q2 Second rotation
 * @return Absolute value of the angle of the residual rotation
 */
auto RelativeRotationRegistrationC::ComputeAngularDeviation(const QuaternionT& q1, const QuaternionT& q2) -> RealT
{
	return 2*acos(fabs((q1*q2.conjugate()).normalized().w()));	//normalisation for numerical stability
}	//RealT ComputeAngularDeviation(const QuaternionT& q1, const QuaternionT& q2)
}	//GeometryN
}	//SeeSawN
