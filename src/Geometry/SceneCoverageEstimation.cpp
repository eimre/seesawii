/**
 * @file SceneCoverageEstimation.cpp Implementation of \c SceneCoverageEstimationC
 * @author Evren Imre
 * @date 7 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SceneCoverageEstimation.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Validates the input parameters
 * @param[in] parameters Parameters to be validated
 * @throws invalid_argument If any of the parameters has an invalid value
 */
void SceneCoverageEstimationC::ValidateParameters(const SceneCoverageEstimationParametersC& parameters) const
{
	if(parameters.radius<=0)
		throw(invalid_argument(string("SceneCoverageEstimationC::ValidateParameters : parameters.radius must be positive. Value=")+lexical_cast<string>(parameters.radius)));
}	//void ValidateParameters(const SceneCoverageEstimationParametersC& parameters) const

/**
 * @brief Validates a unary constraint
 * @param[in] constraint Constraint to be validated
 * @throws invalid_argument If the constraint is invalid
 */
void SceneCoverageEstimationC::ValidateUnaryConstraint(const UnaryConstraintT& constraint) const
{
	if(get<iArea>(constraint)<0)
		throw(invalid_argument(string("SceneCoverageEstimationC::ValidateUnaryConstraint : Area constraint must be >=0. Value=")+lexical_cast<string>(get<iArea>(constraint))));
}	//void ValidateUnaryConstraint(const UnaryConstraintT& constraint) const

/**
 * @brief Computes the area of the projection of a volume element
 * @param[in] centre Centre of the volume element
 * @param[in] projector Projection operator
 * @param[in] vx Direction of the x axis
 * @param[in] vy Direction of the y axis
 * @return Area of the ellipse on the image
 */
double SceneCoverageEstimationC::ComputeArea(const Coordinate3DT& centre, const Projection32C& projector, const Coordinate3DT& vx, const Coordinate3DT& vy) const
{
	double radius=parameters.radius;
	array<Coordinate3DT,4> p3D{ {centre+radius*vx, centre-radius*vx, centre+radius*vy, centre-radius*vy} };
	array<Coordinate2DT,4> p2D;
	transform(p3D, p2D.begin(), projector);
	return pi<double>() * (p2D[0]-p2D[1]).norm() * (p2D[2]-p2D[3]).norm()/4;
};	//double ComputeArea(const Coordinate3DT& centre, const Projection32C& projector, const Coordinate3DT& vx, const Coordinate3DT& vy)

/**
 * @brief Projects the scene onto a camera
 * @param[in] camera Camera
 * @param[in] constraint Coverage requirements
 * @return A vector holding the projection of the individual volume elements. Invalid if a vertex is not covered
 */
auto SceneCoverageEstimationC::ProjectScene(const CameraC& camera, const UnaryConstraintT& constraint) const -> ImageT
{
	ValidateUnaryConstraint(constraint);

	ImageT output;

	//Extract the camera matrix
	optional<CameraMatrixT> mP=camera.MakeCameraMatrix();
	optional<FrameT> frame=camera.FrameBox();

	//Not enough information
	if(!mP || !frame)
		return output;

	Projection32C projector(*mP);	//Projection operator
	Coordinate3DT vC=*camera.Extrinsics()->position;	//Camera centre

	//Lens distortion?
	bool flagDistortion=(bool)camera.Distortion();

	//Focus test?.
	double focusFieldMin=0;
	double focusFieldMax=numeric_limits<double>::infinity();
	bool focusTest=(parameters.focusPoint && camera.FNumber() && camera.PixelSize() && camera.Intrinsics() && camera.Intrinsics()->focalLength);
	if(focusTest)
		std::tie(focusFieldMin, focusFieldMax)=ComputeFocusField((vC-*parameters.focusPoint).norm(), *camera.Intrinsics()->focalLength, *camera.FNumber(), *camera.PixelSize());


	//Vectors parallel to the edges of the image plane
	RotationMatrix3DT mR=camera.Extrinsics()->orientation->matrix();
	Coordinate3DT vx=mR.row(1);
	Coordinate3DT vy=mR.row(2);

	//Project the scene onto the image plane
	size_t nPoints=scene.size();
	vector<Coordinate2DT> projected(nPoints);
	transform(scene, projected.begin(), projector);

	//Apply lens distoriton
	vector<optional<Coordinate2DT>> distorted;
	if(flagDistortion)
		distorted=ApplyDistortion(projected, camera.Distortion()->modelCode, camera.Distortion()->centre, camera.Distortion()->kappa);
	else
		distorted=ApplyDistortion(projected, LensDistortionCodeT::NOD, Coordinate2DT(0,0), 0);

	//Coverage

	CheiralityConstraintC cheirality(*mP);	//Cheirality constraint
	FrameT effectiveRoI=frame->intersection(get<iRoI>(constraint)) ;	//Effective frame, intersection of the image and the RoI
	double minArea=get<iArea>(constraint);
	double imageArea=frame->volume();
	output.resize(nPoints);

	for(size_t c=0; c<nPoints; ++c)
	{
		if(!distorted[c])	//If lens distortion failed, next point
			continue;

		//Tests
		bool flagCovered=cheirality(scene[c]);	//Cheirality
		flagCovered = flagCovered && effectiveRoI.contains(*distorted[c]);	//RoI

		//Area
		double relativeArea=0;

		if(flagCovered)
			relativeArea=ComputeArea(scene[c], projector, vx, vy)/imageArea;

		flagCovered = flagCovered && (relativeArea>=minArea);

		//In focus?
		if( flagCovered && focusTest)
		{
			double distance=(scene[c]-vC).norm();
			flagCovered = flagCovered && (distance>=focusFieldMin && distance<=focusFieldMax);
		}	//if( flagCovered && focusRange)

		if(flagCovered)
			output[c]=ProjectionT(relativeArea, (vC-scene[c]).normalized() );	//Relative area and projection ray
	}	//for(size_t c=0; c<nPoints; ++c)

	return output;
}	//vector<optional<double> > ProjectScene(const CameraC& camera, const UnaryConstraintT& constraint)

/**
 * @brief Predicts the appearance of the similarity of vertices covered by a pair of cameras
 * @param[in] image1 Projection of the scene onto the first camera. If a vertex is not covered, it is invalid
 * @param[in] image2 Projection of the scene onto the second camera. If a vertex is not covered, it is invalid
 * @return A vector. Each element holds the relative scale and the cosine viewpoint difference. If a vertex is not mutually covered, invalid
 * @pre \c image1 and \c image2 have as many elements as \c scene
 */
auto SceneCoverageEstimationC::PredictAppearanceSimilarity(const ImageT& image1, const ImageT& image2) const -> vector<optional<RealPairT> >
{
	//Preconditions
	assert(image1.size()==scene.size());
	assert(image2.size()==scene.size());

	size_t nPoints=scene.size();
	vector<optional<RealPairT> > output(nPoints);

	for(size_t c=0; c<nPoints; ++c)
		if(image1[c] && image2[c])	//If covered by both
		{
			double area1=get<iProjectedArea>(*image1[c]);
			double area2=get<iProjectedArea>(*image2[c]);

			output[c]=make_tuple( (area1>area2) ? area1/area2 : area2/area1, get<iProjectionRay>(*image1[c]).transpose()*get<iProjectionRay>(*image2[c]));
		}	//if(image1[c] && image2[c])

	return output;
}	//vector<optional<RealPairT> > PredictAppearanceSimilarity(const list<unsigned int>& vertexList) const

/**
 * @brief Default constructor
 * @post Invalid object
 */
SceneCoverageEstimationC::SceneCoverageEstimationC() : flagValid(false)
{}

/**
 * @brief Adds a new view
 * @param[in] camera Camera
 * @param[in] constraint Constraint
 * @param[in] flagSimulation If \c true, state is not updated
 * @return If the camera has missing parameters, invalid. Else, a tuple: [Vertices covered by the camera;  vertices uniquely covered by the camera]
 * @post State is modified, if the operation is successful and \c flagSimulation=true
 */
auto SceneCoverageEstimationC::AddView(const CameraC& camera, const UnaryConstraintT& constraint, bool flagSimulation) -> optional<UIntPairT>
{
	//Does the camera have sufficient information?
	optional<UIntPairT> output;
	bool flagValidCamera=camera.MakeCameraMatrix() && camera.FrameBox();

	if(!flagValidCamera)
		return output;

	//Project the scene onto the image
	vector<optional<ProjectionT> > image=ProjectScene(camera, constraint);

	//Statistics
	size_t nVertex=scene.size();
	unsigned int nCovered=0;	//Number of vertices covered by the camera
	unsigned int nUnique=0; //Number of vertices covered only by this camera
	for(size_t c=0; c<nVertex; ++c)
		if(image[c])
		{
			++nCovered;
			if(projections[c].empty()) ++nUnique;
		}	//if(image[c])

	output=UIntPairT(nCovered, nUnique);

	//If not a simulation, update the state
	if(flagSimulation)
		return output;

	unsigned int iCamera = views.empty() ? 0 : views.rbegin()->first+1;	//Get the camera index
	views.emplace(iCamera, ViewT(camera, constraint));	//Update the views

	//Update the projections
	for(size_t c=0; c<nVertex; ++c)
		if(image[c])
			projections[c].emplace(iCamera, *image[c]);

	images[iCamera]=image;	//Update the images

	return output;
}	//void AddView(const CameraC& camera, const UnaryConstraintT& constraint)

/**
 * @brief Removes a view from \c views
 * @param[in] iView Index of the view to be removed
 * @param[in] flagSimulation If \c true , state is not updated
 * @return Invalid, if the view is not in \c views. Else a tuple: [Vertices covered by the camera, vertices uniquely covered by the camera]
 * @post State is modified, if the operation is successful and \c flagSimulation=true
 */
auto SceneCoverageEstimationC::RemoveView(unsigned int iView, bool flagSimulation) -> optional<UIntPairT>
{
	optional<UIntPairT> output=ComputeCoverageStatistics(iView);

	if(!output)	//View not found
		return output;

	if(flagSimulation)	//No further action required
		return output;

	//Removal
	images.erase(iView);
	views.erase(iView);

	//Remove from the projections
	for(auto& current : projections)
		current.erase(iView);

	return output;
}	//optional<UIntPairT> RemoveView(unsigned int iView, bool flagSimulation)

/**
 * @brief Returns the cameras in \c views
 * @return A map indexing the cameras in \c views wrt their indices
 */
map<unsigned int, CameraC> SceneCoverageEstimationC::GetCameras() const
{
	map<unsigned int, CameraC> output;
	for(const auto& current : views)
		output.emplace(current.first, get<iCamera>(current.second));

	return output;
}	//vector<CameraC> GetCameras() const

/**
 * @brief Returns a constant reference to \c scene
 * @return A constant reference to \c scene
 */
const vector<Coordinate3DT>& SceneCoverageEstimationC:: GetScene() const
{
	return scene;
}	//const vector<Coordinate3DT>& GetScene() const

/**
 * @brief Returns a list of the vertices covered by the cameras
 * @param[in] minRedundancy Minimum number of cameras that should provide a valid coverage of a vertex
 * @return A tuple of vectors: Scene point ids, number of views covering the vertex
 */
tuple<vector<size_t>, vector<size_t> > SceneCoverageEstimationC::GetCoveredVertices(unsigned int minRedundancy) const
{
	tuple<vector<size_t>, vector<size_t> > output;

	size_t nVertex=scene.size();
	get<0>(output).reserve(nVertex);
	get<1>(output).reserve(nVertex);

	for(size_t c=0; c<nVertex; ++c)
	{
		size_t nProjection=projections[c].size();
		if(nProjection>=minRedundancy)
		{
			get<0>(output).push_back(c);
			get<1>(output).push_back(nProjection);
		}	//if(nProjection>=minRedundancy)
	}	//for(size_t c=0; c<nVertex; ++c)

	get<0>(output).shrink_to_fit();
	get<1>(output).shrink_to_fit();

	return output;
}	//tuple<vector<size_t>, vector<size_t> > GetCoverage(unsigned int minRedundancy)

/**
 * @brief Returns the number of vertices that can be potentially matched between two views
 * @param[in] iView1 Index of the first view
 * @param[in] iView2 Index of the second view
 * @param[in] maxAngle Maximum difference between the projection rays permitting a successful match. Radians. (0,pi]
 * @param[in] maxScale Maximum scale difference permitting a successful match
 * @return Number of matchable vertices. 0 unless both views exist in \c views
 * @remarks If any of the views is not in \c views , returns 0
 * @pre \c maxScale >0
 * @pre \c maxAngle in (0,pi]
 */
unsigned int SceneCoverageEstimationC::PredictMatchingPerformance(unsigned int iView1, unsigned int iView2, double maxAngle, double maxScale) const
{
	//Preconditions
	assert(maxScale>0);
	assert(maxAngle>0 && maxAngle<=pi<double>());

	auto itE=images.end();
	auto it1=images.find(iView1);
	auto it2=images.find(iView2);
	if(it1==itE || it2==itE)
		return 0;

	const ImageT& image1=it1->second;
	const ImageT& image2=it2->second;

	if(image1.empty() || image2.empty())
		return 0;

	//Filter
	double cosMaxAngle=cos(maxAngle);
	auto predicate=[&](const optional<RealPairT>& item){ return item && ( get<0>(*item)<maxScale && get<1>(*item)>cosMaxAngle);};
	return count_if(PredictAppearanceSimilarity(image1, image2), predicate);
}	//unsigned int EstimateMatchability(unsigned int view1, unsigned int view2, double maxAngle, double maxScale)

/**
 * @brief Ranks the views wrt/matching performance with the input
 * @param[in] camera Camera
 * @param[in] constraint Constraint
 * @param[in] maxAngle Maximum difference between the projection rays permitting a successful match. Radians. (0,pi]
 * @param[in] maxScale Maximum scale difference permitting a successful match
 * @return Views ranked wrt/number of matchable vertices
 * @remarks Best-view query
 */
map<unsigned int, size_t, greater<unsigned int> > SceneCoverageEstimationC::RankViews(const CameraC& camera, const UnaryConstraintT& constraint, double maxAngle, double maxScale) const
{
	map<unsigned int, size_t, greater<unsigned int> > ranker;

	//Projection for the camera
	ImageT imageC=ProjectScene(camera, constraint);

	//Matchability with the other views
	double cosMaxAngle=cos(maxAngle);
	auto predicate=[&](const optional<RealPairT>& item){ return item && ( get<0>(*item)<maxScale && get<1>(*item)>cosMaxAngle);};
	for(const auto& current : images)
		ranker.emplace(current.first, count_if(PredictAppearanceSimilarity(imageC, current.second), predicate));

	return ranker;
}	//map<unsigned int, size_t> RankViews(const CameraC& camera)

/**
 * @brief Coverage statistics for each view
 * @return A map: [view index; Vertices covered/uniquely covered by the camera]
 */
auto SceneCoverageEstimationC::ComputeCoverageStatistics() const -> map<unsigned int, UIntPairT>
{
	map<unsigned int, UIntPairT> output;

	//Unique coverage

	map<unsigned int, unsigned int> uniqueCoverage;

	//Initialise
	for(const auto& current : images)
		uniqueCoverage[current.first]=0;

	size_t nVertex=scene.size();
	for(size_t c=0; c<nVertex; ++c)
		if(projections[c].size()==1)
			uniqueCoverage[projections[c].begin()->first]++;

	//Output
	auto predicate=[](const optional<ProjectionT>& item){return item;};
	for(const auto& current : images)
	{
		unsigned int nCovered=count_if(current.second, predicate);
		output.emplace(current.first, UIntPairT(nCovered, uniqueCoverage[current.first]));
	}	//for(const auto& current : images)

	return output;
}	//map<unsigned int, UIntPairT> ComputeCoverageStatistics()

/**
 * @brief Coverage statistics for a view
 * @param[in] iView View index
 * @return A tuple:[Covered, uniquely covered]. Invalid if \c iView is not a valid view index
 */
auto SceneCoverageEstimationC::ComputeCoverageStatistics(unsigned int iView) const -> optional<UIntPairT>
{
	optional<UIntPairT> output;

	//Does it exist?
	auto itV=views.find(iView);
	if(itV==views.end())
		return output;

	//Statistics
	unsigned int nUnique=0;
	unsigned int nCovered=0;
	size_t index=0;
	for(const auto& current : images.find(iView)->second )
	{
		if(current)
		{
			++nCovered;

			if(projections[index].size()==1 && projections[index].find(iView)!=projections[index].end())	//Unique?
				++nUnique;
		}	//if(current)

		++index;
	}	//for(const auto& current : pImage)

	output=UIntPairT(nCovered, nUnique);
	return output;
}	//optional<UIntPairT> ComputeCoverageStatistics(unsigned int iView)

/**
 * @brief Clears the state
 * @post State is cleared, but configuration is intact
 */
void SceneCoverageEstimationC::Clear()
{
	projections.clear(); projections.resize(scene.size());
	views.clear();
	images.clear();
}	//void Clear()

/**
 * @brief Computes the coverage gradient wrt/ pose and focal length
 * @param[in] camera Camera
 * @param[in] constraint Constraint
 * @param[in] epsilonP Additive perturbation to the position, in m
 * @param[in] epsilonO Additive perturbation to the orientation, in rad
 * @param[in] epsilonF Additive perturbation to the focal length, in pixel
 * @return A tuple of gradient vectors: Local gradient of the total coverage, and that of the uniquely covered points
 * @remarks Layout: [Camera centre XYZ (3); Orientation XYZ(3); Focal length(1)]
 * @pre \c camera has sufficient information to make a camera matrix
 * @remarks The gradient is an indicator for the best action to increase the coverage
 */
auto SceneCoverageEstimationC::ComputeCoverageGradient(const CameraC& camera, const UnaryConstraintT& constraint, double epsilonP, double epsilonO, double epsilonF) -> tuple<gradient_type, gradient_type>
{
	//Preconditions
	assert(camera.MakeCameraMatrix());

	gradient_type nablaTotal;
	gradient_type nablaUnique;

	CameraC cameraP(camera);	//Positive perturbation
	CameraC cameraM(camera);	//Negative perturbation

	//Partial derivative function
	auto partialDerivative=[&](double epsilon){ RealPairT p=*AddView(cameraP, constraint, true); RealPairT m=*AddView(cameraM, constraint, true); return RealPairT( 0.5*(get<0>(p)-get<0>(m))/epsilon, 0.5*(get<1>(p)-get<1>(m))/epsilon ); };

	//Focal length
	*cameraP.Intrinsics()->focalLength += epsilonF;
	*cameraM.Intrinsics()->focalLength-=epsilonF;
	std::tie(nablaTotal[6], nablaUnique[6])=partialDerivative(epsilonF);

	*cameraP.Intrinsics()->focalLength=*camera.Intrinsics()->focalLength;
	*cameraM.Intrinsics()->focalLength=*camera.Intrinsics()->focalLength;

	//Camera centre
	for(size_t c=0; c<3; ++c)
	{
		(*cameraP.Extrinsics()->position)[c]+=epsilonP;
		(*cameraM.Extrinsics()->position)[c]-=epsilonP;
		std::tie(nablaTotal[c], nablaUnique[c])=partialDerivative(epsilonP);

		(*cameraP.Extrinsics()->position)[c]=(*camera.Extrinsics()->position)[c];
		(*cameraM.Extrinsics()->position)[c]=(*camera.Extrinsics()->position)[c];
	}	//for(size_t c=0; c<3; ++c)

	//Orientation
	typedef CameraC::real_type RealT;
	typedef AngleAxis<RealT> AngleAxisT;
	Matrix<RealT, 3, 1> axis; axis.setZero();
	for(size_t c=0; c<3; ++c)
	{
		axis[c]=1;

		*cameraP.Extrinsics()->orientation = QuaternionT(AngleAxisT(epsilonO, axis)) * (*cameraP.Extrinsics()->orientation);
		*cameraM.Extrinsics()->orientation = QuaternionT(AngleAxisT(-epsilonO, axis)) * (*cameraM.Extrinsics()->orientation);
		std::tie(nablaTotal[c+3], nablaUnique[c+3])=partialDerivative(epsilonO);

		*cameraP.Extrinsics()->orientation = *camera.Extrinsics()->orientation;
		*cameraM.Extrinsics()->orientation = *camera.Extrinsics()->orientation;

		axis[c]=0;
	}	//for(size_t c=0; c<3; ++c)

	return make_tuple(nablaTotal, nablaUnique);
}	//auto ComputeGradient(const CameraC& camera, const UnaryConstraintT& constraint, double epsilon=1e-4) const -> gradient_type

}	//GeometryN
}	//SeeSawN
