/**
 * @file Similarity3DSolver.ipp Implementation of the solver for a 3D similarity transformation
 * @author Evren Imre
 * @date 26 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SIMILARITY3D_SOLVER_IPP_5923654
#define SIMILARITY3D_SOLVER_IPP_5923654

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <Eigen/Dense>
#include <tuple>
#include <type_traits>
#include <vector>
#include <iterator>
#include <list>
#include <cmath>
#include "GeometrySolverBase.h"
#include "Rotation3DSolver.h"
#include "Rotation.h"
#include "CoordinateTransformation.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricError.h"
#include "../Wrappers/BoostBimap.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Numeric/Complexity.h"

namespace SeeSawN
{
namespace GeometryN
{

//Forward declarations
struct Similarity3DMinimalDifferenceC;

using boost::range_value;
using boost::make_zip_iterator;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using Eigen::Matrix;
using std::tie;
using std::make_tuple;
using std::tuple;
using std::is_floating_point;
using std::vector;
using std::advance;
using std::list;
using std::max;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::GeometryN::Rotation3DMinimalDifferenceC;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;
using SeeSawN::GeometryN::RotationVectorToAxisAngle;
using SeeSawN::GeometryN::CoordinateTransformations3DT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::ElementsN::ComposeSimilarity3D;
using SeeSawN::MetricsN::SymmetricTransferErrorC;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::VectorToBimap;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::NumericN::ComplexityEstimatorC;

/**
 * @brief 3-point 3D similarity transformation estimator
 * @remarks Horn, B. K. P. , "Closed-Form Solution of Absolute Orientation Using Quaternions," Journal of the Optical Society of America A, vol. 4, pp. 629-642, April 1987
 * @remarks For the minimal representation, the transformation is encoded as \f$ s\mathbf{R(X-C)} \f$, as opposed to the conventional \f$ s\mathbf{RX+t}\f$, to facilitate code reuse
 * @remarks Distance metric: 1-|<u,v>|/sqrt(<u,u><v,v>). Range=[0,1]
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class Similarity3DSolverC : public GeometrySolverBaseC<Similarity3DSolverC, Homography3DT, SymmetricTransferErrorC<Homography3DT>, 3, 3, 3, true, 1, 16, 7>
{
	private:

		typedef GeometrySolverBaseC<Similarity3DSolverC, Homography3DT, SymmetricTransferErrorC<Homography3DT>, 3, 3, 3, true, 1, 16, 7> BaseT;	///< Type of the base

		typedef typename BaseT::model_type ModelT;	///< 4x4 homography, in which a similarity transformation is embedded
		typedef MultivariateGaussianC<Similarity3DMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

	public:

		/** @name GeometrySolverConceptC interface */ //@{

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the similarity transformation that best explains the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<RealT, Coordinate3DT, RotationVectorT> minimal_model_type;	///< A minimally parameterised model. [scale; origin of the new coordinate frame; orientation of the new coordinate frame]
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class HomographyRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of rotations
		//@}

		/** @name Overrides */ //@{
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool dummy=true);	///< Converts a vector to a model
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iScale=0;	///< Index of the scale
		static constexpr unsigned int iCenter=1;	///< Index of the origin of the new coordinate frame (not translation!)
		static constexpr unsigned int iOrientation=2;	///< Index of the orientation of the new coordinate frame
		//@}
};	//class Similarity3DSolverC

/**
 * @brief Difference functor for minimally-represented 3D similarity transformations
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct Similarity3DMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef Similarity3DSolverC::minimal_model_type first_argument_type;
	typedef Similarity3DSolverC::minimal_model_type second_argument_type;
	typedef Matrix<Similarity3DSolverC::real_type, Similarity3DSolverC::DoF(), 1> result_type;	///< Difference in vector form

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between two minimally-represented similarity transformations
	//@}

};	//struct Similarity3DMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre \c model is a similarity transformation (unenforced)
 * @return Minimal form
 */
template<class DummyRangeT>
auto Similarity3DSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	RealT scale;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(scale, vC, mR)=DecomposeSimilarity3D(model);

	return make_tuple(scale, vC, RotationMatrixToRotationVector(mR));
}	//auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of transformations
 * @tparam HomographyRangeT A range of 3D similarity transformations
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] homographies Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c HomographyRangeT is a forward range of elements of type \c Homography3DT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c homographies should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c homographies has 3 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class HomographyRangeT, class WeightRangeT, class DummyRangeT>
auto Similarity3DSolverC::ComputeSampleStatistics(const HomographyRangeT& homographies, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<HomographyRangeT>));
	static_assert(is_same<ModelT, typename range_value<HomographyRangeT>::type >::value, "Similarity3DSolverC::ComputeSampleStatistics : HomographyRangeT must hold elements of type Homography3DT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "Similarity3DSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(homographies)==boost::distance(wMean));
	assert(boost::distance(homographies)==boost::distance(wCovariance));

	size_t nSamples=boost::distance(homographies);

	//Mean: Decompose the homography into scale, origin and quaternion components
	RealT meanScale=0;
	Coordinate3DT meanOrigin=Coordinate3DT::Zero();
	vector<QuaternionT> qList; qList.reserve(nSamples);
	vector<minimal_model_type> minimalList; minimalList.reserve(nSamples);

	auto itH=boost::const_begin(homographies);
	auto itWM=boost::const_begin(wMean);
	for(size_t c=0; c<nSamples; ++c, advance(itH,1), advance(itWM,1))
	{
		minimal_model_type minimalRep=MakeMinimal(*itH);

		meanScale += *itWM * get<iScale>(minimalRep);
		meanOrigin += *itWM * get<iCenter>(minimalRep);
		qList.emplace_back( RotationVectorToQuaternion(get<iOrientation>(minimalRep)) );

		minimalList.push_back(minimalRep);
	}

	//Mean quaternion
	QuaternionT qMean;
	Matrix<RealT, 3, 3> mCovQ;
	tie(qMean, mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	minimal_model_type mean(meanScale, meanOrigin, QuaternionToRotationVector(qMean));

	//Covariance
	Similarity3DMinimalDifferenceC subtractor;
	auto itC=boost::const_begin(minimalList);
	auto itWC=boost::const_begin(wCovariance);
	typename GaussianT::covariance_type mCov; mCov.setZero();
	for(size_t c=0; c<nSamples; ++c, advance(itWC,1), advance(itC,1))
	{
		typename Similarity3DMinimalDifferenceC::result_type dif=subtractor(*itC, mean);
		mCov += *itWC * (dif*dif.transpose());
	}	//for(size_t c=0; c<nSamples; ++c)

	return GaussianT(mean, mCov);
}	//auto ComputeSampleStatistics(const RotationRangeT& rotations, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT

/**
 * @brief Computes the similarity transformation that best explains the correspondence set
 * @param[in] correspondences Correspondence set
 * @return A 1-element list, holding a 4x4 homography
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap of elements of type \c Coordinate3DT
 */
template<class CorrespondenceRangeT>
auto Similarity3DSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "Similarity3DSolverC::operator() : CorrespondenceRangeT must be bimap of elements of type Coordinate3DT");
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::right_key_type>::value, "Similarity3DSolverC::operator() : CorrespondenceRangeT must be bimap of elements of type Coordinate3DT");

	//Normalise the coordinates
	vector<Coordinate3DT> left;
	vector<Coordinate3DT> right;
	tie(left, right)=BimapToVector<Coordinate3DT, Coordinate3DT>(correspondences);

	typedef typename CoordinateTransformations3DT::affine_transform_type NormaliserT;
	NormaliserT normaliserL=CoordinateTransformations3DT::ComputeNormaliser(left, true);
	vector<Coordinate3DT> leftNormalised=CoordinateTransformations3DT::ApplyAffineTransformation(left, normaliserL);

	NormaliserT normaliserR=CoordinateTransformations3DT::ComputeNormaliser(right, true);
	vector<Coordinate3DT> rightNormalised=CoordinateTransformations3DT::ApplyAffineTransformation(right, normaliserR);

	//Rotation
	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based> BimapT;
	Rotation3DSolverC rotationSolver;
	list<Homography3DT> rotationList=rotationSolver(VectorToBimap<BimapT>(leftNormalised, rightNormalised));

	if(rotationList.empty())
		return list<ModelT>();

	RotationMatrix3DT mR=rotationList.begin()->block(0,0,3,3);

	//2D,E: Scale
	RealT scale=normaliserL.matrix()(0,0)/normaliserR.matrix()(0,0);	//(1/sl)/(1/sr)=sr/sl

	//2B,C: Translation
	Coordinate3DT meanL=-normaliserL.matrix().col(3).segment(0,3)/normaliserL.matrix()(0,0);
	Coordinate3DT meanR=-normaliserR.matrix().col(3).segment(0,3)/normaliserR.matrix()(0,0);
	Coordinate3DT t=meanR-scale*mR*meanL;

	Homography3DT mH; mH.row(3)<<0,0,0,1;
	mH.block(0,0,3,3)=scale*mR;
	mH.block(0,3,3,1)=t;

	list<ModelT> output; output.push_back(mH);
	return output;
}	//auto operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
}	//GeometryN
}	//SeeSawN

#endif /* SIMILARITY3D_SOLVER_IPP_5923654 */
