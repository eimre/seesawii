/**
 * @file Rotation.cpp Implementation and instantiations for various rotation-related functions
 * @author Evren Imre
 * @date 9 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Rotation.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Converts a quaternion to a rotation vector
 * @param[in] q Quaternion to be converted
 * @return Axis-angle vector
 * @remarks Eigen offers an axis-angle conversion, but accessors for the axis-angle components does not seem to be a part of the public interface
 * @remarks Convention: In the implementation, if the scalar element of \c q is not positive, the quaternion vector is negated
 * @ingroup Utility
 */
RotationVectorT QuaternionToRotationVector(QuaternionT q)
{
	if(q.w()<0)
	{
		q.w()=-q.w();
		q.vec()=-q.vec();
	}	//if(q.w()<0)

	AxisAngleT aa(q);

	typedef ValueTypeM<RotationVectorT>::type RealT;
	RealT angle=aa.angle();

	//Map the angle to the interval +-pi
	if(fabs(angle)>pi<RealT>())
		angle=remainder(angle, two_pi<RealT>());

	return aa.axis()*angle;
}	//QuaternionT::Vector3 QuaternionToRotationVector(const QuaternionT& q)

/**
 * @brief Converts a rotation matrix to an axis-angle vector
 * @param[in] mR Rotation matrix
 * @return Axis-angle vector
 * @pre \c mR is a valid rotation matrix (unenforced)
 * @ingroup Utility
 */
RotationVectorT RotationMatrixToRotationVector(RotationMatrix3DT mR)
{
	AxisAngleT aa(mR);

	typedef ValueTypeM<RotationVectorT>::type RealT;
	RealT angle=aa.angle();

	//Map the angle to the interval +-pi
	if(fabs(angle)>pi<RealT>())
		angle=remainder(angle, two_pi<RealT>());

	return aa.axis()*angle;
}	//RotationVectorT RotationMatrixToRotationVector(RotationMatrix3DT mR)

/**
 * @brief Converts a rotation vector to an axis-angle object
 * @param[in] v Rotation vector
 * @return Axis-angle object
 * @ingroup Utility
 */
AxisAngleT RotationVectorToAxisAngle(const RotationVectorT& v)
{
	ValueTypeM<RotationVectorT>::type normV=v.norm();
	return (normV>=3*numeric_limits<ValueTypeM<RotationVectorT>::type>::epsilon()) ? AxisAngleT(normV, v/normV):AxisAngleT(0, RotationVectorT(1,0,0)) ;
}	//AxisAngleT RotationVectorToAxisAngle(const RotationVectorT& v)

/**
 * @brief Converts a rotation vector to a quaternion
 * @param[in] v Rotation vector
 * @return Quaternion corresponding to \c v
 * @ingroup Utility
 */
QuaternionT RotationVectorToQuaternion(const RotationVectorT& v)
{
	return QuaternionT(RotationVectorToAxisAngle(v));
}	//QuaternionT RotationVectorToQuaternion(const RotationVectorT& v)

/**
 * @brief Jacobian for the quaternion to rotation matrix conversion
 * @param[in] q Quaternion
 * @return Jacobian
 * @ingroup Jacobian
 */
Matrix<QuaternionT::Scalar, 9, 4> JacobianQuaternionToRotationMatrix(const QuaternionT& q)
{
	Matrix<QuaternionT::Scalar, 9, 4> mJ;
	mJ(0,0)= q.w(); mJ(0,1)= q.x(); mJ(0,2)=-q.y(); mJ(0,3)=-q.z();
	mJ(1,0)=-q.z(); mJ(1,1)= q.y(); mJ(1,2)= q.x(); mJ(1,3)=-q.w();
	mJ(2,0)= q.y(); mJ(2,1)= q.z(); mJ(2,2)= q.w(); mJ(2,3)= q.x();
	mJ(3,0)= q.z(); mJ(3,1)= q.y(); mJ(3,2)= q.x(); mJ(3,3)= q.w();
	mJ(4,0)= q.w(); mJ(4,1)=-q.x(); mJ(4,2)= q.y(); mJ(4,3)=-q.z();
	mJ(5,0)=-q.x(); mJ(5,1)=-q.w(); mJ(5,2)= q.z(); mJ(5,3)= q.y();
	mJ(6,0)=-q.y(); mJ(6,1)= q.z(); mJ(6,2)=-q.w(); mJ(6,3)= q.x();
	mJ(7,0)= q.x(); mJ(7,1)= q.w(); mJ(7,2)= q.z(); mJ(7,3)= q.y();
	mJ(8,0)= q.w(); mJ(8,1)=-q.x(); mJ(8,2)=-q.y(); mJ(8,3)= q.z();

	return 2*mJ;
}	//Matrix<QuaternionT::Scalar, 4, 9> JacobianQuaternionToRotationMatrix(const QuaternionT& q)

/**
 * @brief Computes the Jacobian of the rotation vector->quaternion conversion
 * @param[in] vRotation Rotation vector
 * @return Jacobian matrix
 * @ingroup Jacobian
 */
Matrix<QuaternionT::Scalar, 4, 3> JacobianRotationVectorToQuaternion(const RotationVectorT& vRotation)
{
	Matrix<QuaternionT::Scalar, 4, 3> output;

	double angle=vRotation.norm();
	double halfAngle=angle/2;

	//If the angle is 0, the output is a zero matrix
	if(halfAngle==0)
		return Matrix<QuaternionT::Scalar, 4, 3>::Zero();

	double sinHA=sin(halfAngle);
	double cosHA=cos(halfAngle);

	RotationVectorT axis=vRotation/angle;

	output.row(0)=-axis*sinHA/2;

	Matrix3d dAxisdNorm=JacobianNormalise<Matrix3d>(vRotation);
	Matrix3d temp=axis * axis.transpose();

	output.bottomRows(3)=(cosHA/2)*temp + sinHA*dAxisdNorm;

	return output;
}	//Matrix<QuaternionT::Scalar, 4, 3> JacobianAxisAngleToQuaternion(const RotationVectorT& axisAngle)

/**
 * @brief Jacobian for the quaternion to rotation vector conversion
 * @param[in] q Quaternion
 * @return Jacobian for the conversion
 * @remarks Infinity at q.w()=1
 */
Matrix<QuaternionT::Scalar,3,4> JacobianQuaternionToRotationVector(const QuaternionT& q)
{
	typedef QuaternionT::Scalar RealT;
	Matrix<RealT,3,4> output; output.setZero();

	if(q.w()==1)
	{
		output.col(0).setConstant(numeric_limits<QuaternionT::Scalar>::infinity());
		output(0,1)=output(0,0); output(1,2)=output(0,0); output(2,3)=output(0,0);
		return output;
	}

	double angle=2*acos(q.w());

	//Map the angle to the interval +-pi
	if(fabs(angle)>pi<RealT>())
		angle=remainder(angle, two_pi<RealT>());

	double denum=sqrt(1-pow<2>(q.w()));
	double scale=angle/denum;
	double dScaledqw =(1/pow<2>(denum)) * ( angle*q.w()/denum - 2 );

	output.col(0) = dScaledqw * q.vec();
	output(0,1)=scale; output(1,2)=scale; output(2,3)=scale;

	//angle (after mapping between +-pi) is an odd function of qw. So, the sign change does not affect the Jacobian

	return output;
}	//Matrix<QuaternionT::Scalar,3,4> JacobianQuaternionToRotationVector(const RotationVectorT& vRotation)

/**
 * @brief Jacobian of the quaternion conjugation operation
 * @return Jacobian matrix
 */
Matrix<QuaternionT::Scalar, 4, 4> JacobianQuaternionConjugation()
{
	Matrix<QuaternionT::Scalar, 4, 4> output=Matrix<QuaternionT::Scalar, 4, 4>::Identity();
	output(1,1)=-1; output(2,2)=-1; output(3,3)=-1;
	return output;
}	//Matrix<QuaterntionT::Scalar, 4, 4> JacobianQuaternionConjugation(const QuaternionT& q)

/**
 * @brief Jacobian of the quaternion multiplication operation
 * @param[in] q1 Multiplier
 * @param[in] q2 Multiplicand
 * @return Jacobian matrix
 */
Matrix<QuaternionT::Scalar, 4, 8> JacobianQuaternionMultiplication(const QuaternionT& q1, const QuaternionT& q2)
{
	Matrix<QuaternionT::Scalar, 4, 8> output;

	output.row(0).segment(0,4)=QuaternionToVector(q2.conjugate());
	output.row(0).segment(4,4)=QuaternionToVector(q1.conjugate());

	output(1,0)= q2.x(); output(1,1)= q2.w(); output(1,2)= q2.z(); output(1,3)=-q2.y();
	output(1,4)= q1.x(); output(1,5)= q1.w(); output(1,6)=-q1.z(); output(1,7)= q1.y();

	output(2,0)= q2.y(); output(2,1)=-q2.z(); output(2,2)= q2.w(); output(2,3)= q2.x();
	output(2,4)= q1.y(); output(2,5)= q1.z(); output(2,6)= q1.w(); output(2,7)=-q1.x();

	output(3,0)= q2.z(); output(3,1)= q2.y(); output(3,2)=-q2.x(); output(3,3)= q2.w();
	output(3,4)= q1.z(); output(3,5)=-q1.y(); output(3,6)= q1.x(); output(3,7)= q1.w();

	return output;
}	//Matrix<QuaternionT::Scalar, 4, 4> JacobianQuaternionMultiplication(const QuaternionT& q1, const QuaternionT& q2)

/********** EXPLICIT INSTANTIATIONS **********/
template tuple<QuaternionT, Matrix3d> ComputeQuaternionSampleStatistics(const vector<QuaternionT>&, const vector<double>&, const vector<double>&);

}	//GeometryN
}	//SeeSawN

