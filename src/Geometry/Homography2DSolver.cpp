/**
 * @file Homography2DSolver.cpp Implementation of Homography2DSolverC
 * @author Evren Imre
 * @date 10 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Homography2DSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/********** Homography2DSolverC **********/

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double Homography2DSolverC::Cost(unsigned int nCorrespondences)
{
	return DLTC<DLTProblemT>::Cost(max(nCorrespondences, sGenerator));
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return Homography matrix
 * @pre The absolute values of the scalar components are <1
 * @pre Sum of the squares of the scalar components are <1
 */
auto Homography2DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	assert(fabs(get<iScalar2>(minimalModel)<1));
	assert(fabs(get<iScalar3>(minimalModel)<1));
	assert(pow<2>(get<iScalar2>(minimalModel)) + pow<2>(get<iScalar3>(minimalModel)) < 1);

	Matrix<RealT, 3, 1> vS; vS<< 0, get<iScalar2>(minimalModel), get<iScalar3>(minimalModel);
	vS[0]=sqrt(1-pow<2>(vS[1])-pow<2>(vS[2]));

	if(vS[1]<0)
		vS[0]*=-1;	//sign correction

	return (get<iRotationL>(minimalModel).toRotationMatrix() * (vS.asDiagonal() * get<iRotationR>(minimalModel).toRotationMatrix().transpose())).eval();
}	//ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel)

/********** Homography2DMinimalDifferenceC **********/

/**
 * @brief Computes the difference between to minimally-represented 2D homographies
 * @param[in] op1 Minuend
 * @param[in] op2 Subtrahend
 * @return Difference vector
 * @pre For both operands, the sum of squares of the scalars <1
 * @remarks Composition with inverse homography is unstable: Even when \c op1=op2 , numerical errors may give rise to non-identity rotation
 */
auto Homography2DMinimalDifferenceC::operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	//Preconditions
	assert( pow<2>(get<Homography2DSolverC::iScalar2>(op1)) + pow<2>(get<Homography2DSolverC::iScalar3>(op1)) < 1);
	assert( pow<2>(get<Homography2DSolverC::iScalar2>(op2)) + pow<2>(get<Homography2DSolverC::iScalar3>(op2)) < 1);

	result_type output;

	QuaternionT qDif1=get<Homography2DSolverC::iRotationL>(op2)*get<Homography2DSolverC::iRotationL>(op1).conjugate();
	output.segment(0,3)=QuaternionToRotationVector(qDif1);

	QuaternionT qDif2=get<Homography2DSolverC::iRotationR>(op2)*get<Homography2DSolverC::iRotationR>(op1).conjugate();
	output.segment(3,3)=QuaternionToRotationVector(qDif2);

	output[6]=fabs( get<Homography2DSolverC::iScalar2>(op2)) - fabs(get<Homography2DSolverC::iScalar2>(op1));
	output[7]=fabs( get<Homography2DSolverC::iScalar3>(op2)) - fabs(get<Homography2DSolverC::iScalar3>(op1));

	return output;
}	//auto operator()(const first_argument_type& op1, const second_argument_type& op2) -> result_type

}	//GeometryN
}	//SeeSawN

