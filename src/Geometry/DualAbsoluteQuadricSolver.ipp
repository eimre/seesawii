/**
 * @file DualAbsoluteQuadricSolver.ipp Implementation of dual absolute quadric solver
 * @author Evren Imre
 * @date 4 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DUAL_ABSOLUTE_QUADRIC_SOLVER_IPP_5284022
#define DUAL_ABSOLUTE_QUADRIC_SOLVER_IPP_5284022

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <list>
#include <vector>
#include <cmath>
#include <limits>
#include <type_traits>
#include <cstddef>
#include <tuple>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::math::pow;
using boost::optional;
using Eigen::MatrixXd;
using Eigen::Matrix4d;
using Eigen::Matrix;
using Eigen::JacobiSVD;
using std::list;
using std::vector;
using std::exp;
using std::min;
using std::fabs;
using std::numeric_limits;
using std::is_same;
using std::size_t;
using std::tie;
using std::ignore;
using SeeSawN::ElementsN::MapToCanonical;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::IntrinsicsFromDAQ;
using SeeSawN::ElementsN::Quadric3DT;
using SeeSawN::NumericN::MakeRankN;

//TODO Would it be better to drop the focal length constraints altogether, now that the minimum is set to 4 cameras?
/**
 * @brief Estimates the dual absolute quadric from a set of cameras
 * @remarks "Robust Linear Auto-Calibration of a Moving Camera from Image Sequences," T. Thormahlen, H. Broszio, P. Mikulastik, ACCV06
 * @remarks Usage notes
 *  - For each camera, the algorithm initially assumes
 * 		- Unity aspect ratio
 * 		- 0 skew
 * 		- Principal point at (0,0)
 * 		- Unity focal length
 *  The matrices should be normalised appropriately, to reflect these assumptions. In the original algorithm, the result is biased towards the initial estimate.
 * 	However, the "variable-weight" solver prevents a bias for the focal length.
 *	- The theoretical minimum generator is 3. However, when the focal length equations are assigned low weights, the equation system becomes ill-conditioned. That is why, the minimum generator size is set to 4.
 *	- Constraints are enforced via least-squares, i.e., are soft. Therefore deviations should be expected.
 *	- If the intrinsics are known to be identical across the camera set, this should be enforced separately.
 *	- Cost is very high wrt/evaluation. In WaldSAC, relative cost can effectively be set to infinity
 *	- Decomposition of DAQ yields a rectifying homography to upgrade a projective camera to metric
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class DualAbsoluteQuadricSolverC
{
	private:

		/** @name Configuration */ //@{
		static constexpr unsigned int sGenerator=4;	///< Minimum number of elements
		static constexpr double maxCost=numeric_limits<double>::infinity();	///< Cost associated with a failed mK estimate
		unsigned int nVWIterations;	///< Number of iterations for the variable-weight solver
		//@}

		/** @name Implementation details */ //@{
		typedef Matrix<double, 1, 10> EquationT;	///< A single constraint
		static EquationT MakeEquation(const CameraMatrixT& mP, size_t i1, size_t i2);	///< Makes an equation

		typedef Matrix<double, Eigen::Dynamic, 10> SystemMatrixT;	///< Type of the system matrix
		static SystemMatrixT MakeEquationSystem(const vector<CameraMatrixT>& mPnList);	///< Builds the equation system

		typedef Matrix<double, 10, 1> VectorQT;	///< Vector representing an absolute dual quadric
		static Quadric3DT MakeQ(const VectorQT& q);	///< Converts the solution vector to an absolute dual quadric
		//@}

	public:

		typedef Quadric3DT model_type;	///< A geometric model

		DualAbsoluteQuadricSolverC(unsigned int nnVWIterations=50);	///< Constructor

		template <class CameraMatrixRangeT> list<Quadric3DT> operator()(const CameraMatrixRangeT& mPList) const;	///< Estimates the dual absolute quadric for a set of cameras
		static constexpr unsigned int GeneratorSize(); ///< Returns the size of the minimal generator

		static double Evaluate(const Quadric3DT& mQ, const CameraMatrixT& mP);	///< Evaluates the cost for an intrinsic calibration matrix
};	//class LinearAutocalibrationSolverC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Returns the size of the minimal generator
 * @return \c sGenerator
 */
constexpr unsigned int DualAbsoluteQuadricSolverC::GeneratorSize()
{
	return sGenerator;
}	//unsigned int GeneratorSize()

/**
 * @brief Estimates the rectifying homography for a set of cameras
 * @param[in] mPList List of normalised projective camera matrices
 * @return Rectifying homography
 * @pre \c mPList has at least 3 elements
 * @pre \c CameraMatrixRangeT is a single pass range holding elements of type \c CameraMatrixT
 * @post The output is unit-norm and positive semi-definite
 */
template <class CameraMatrixRangeT>
list<Quadric3DT> DualAbsoluteQuadricSolverC::operator()(const CameraMatrixRangeT& mPList) const
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<CameraMatrixRangeT>));
	static_assert(is_same<CameraMatrixT, typename range_value<CameraMatrixRangeT>::type>::value, "DualAbsoluteQuadricSolverC::operator() : CameraMatrixRangeT must hold elements of type CameraMatrixT");

	list<Quadric3DT> output;
	if(boost::distance(mPList)<sGenerator)
		return output;

	//Transform the coordinate frame so that the first camera is canonical

	Homography3DT mHc;
	tie(mHc, ignore)=MapToCanonical(*boost::const_begin(mPList), false);

	vector<CameraMatrixT> mPnList; mPnList.reserve(boost::distance(mPList));
	for(const auto& current : mPList)
		mPnList.emplace_back(current*mHc);

	//Build the equation system
	SystemMatrixT mA=MakeEquationSystem(mPnList);
	size_t nEq=mA.rows();

	//Variable-weight solver

	JacobiSVD<SystemMatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd;

	double beta=0.1;	//Focal length weight
	double e03=exp(0.3);	//Eq 23
	optional<Quadric3DT> bestQ;
	double minError=numeric_limits<double>::infinity();
	for(size_t c=0; c<nVWIterations; ++c, beta*=e03)
	{
		//Update the weight of the focal length terms
		//Copying is not necessary. However, incrementally updating the focal length constraints may introduce numerical errors
		MatrixXd mAc(mA);
		for(size_t c2=0; c2<nEq; c2+=6)
			mAc.block(c2+4, 0, 2, 10)*=1.0/beta;

		//Solve
		svd.compute(mAc, Eigen::ComputeFullU | Eigen::ComputeFullV);
		Quadric3DT mQ=MakeQ(svd.matrixV().col(9));

		//Evaluate
		double error=0;
		for(const auto& current : mPnList)
			error+=Evaluate(mQ, current);

		if(error<minError)
		{
			minError=error;
			bestQ=mQ;
		}	//if(error<minError)
	}	//for(size_t c=0; c<nIteration; ++c)

	if(bestQ)
	{
		Quadric3DT denormalised= mHc * (*bestQ) * mHc.transpose();	//Denormalise
		denormalised.normalize();
		output.push_back(denormalised);
	}

	return output;
}	//Homography3DT operator()(const CameraMatrixRangeT& mPList)

}	//GeometryN
}	//SeeSawN

#endif /* DUAL_ABSOLUTE_QUADRIC_SOLVER_IPP_5284022 */
