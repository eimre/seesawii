/**
 * @file OneSidedFundamentalSolver.ipp Implementation of the 6-point one-sided fundamental matrix solver
 * @author Evren Imre
 * @date 13 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ONE_SIDED_FUNDAMENTAL_SOLVER_IPP_0598313
#define ONE_SIDED_FUNDAMENTAL_SOLVER_IPP_0598313

#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/concept_check.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/range/functions.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <tuple>
#include <list>
#include <iterator>
#include <vector>
#include <cstddef>
#include <climits>
#include <cmath>
#include <complex>
#include <array>
#include <type_traits>
#include "GeometrySolverBase.h"
#include "EssentialSolver.h"
#include "DLT.h"
#include "CoordinateTransformation.h"
#include "Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Metrics/GeometricError.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Numeric/Polynomial.h"

namespace SeeSawN
{
namespace GeometryN
{

//Forward declarations
struct OneSidedFundamentalMinimalDifferenceC;

using boost::math::pow;
using boost::math::constants::half_pi;
using boost::optional;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using Eigen::Matrix;
//using Eigen::GeneralizedEigenSolver;
using Eigen::EigenSolver;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::get;
using std::list;
using std::vector;
using std::advance;
using std::size_t;
using std::numeric_limits;
using std::fabs;
using std::abs;
using std::complex;
using std::sqrt;
using std::array;
using std::is_same;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::EssentialSolverC;
using SeeSawN::GeometryN::EssentialMinimalDifferenceC;
using SeeSawN::GeometryN::DLTProblemC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::DecomposeEssential;
using SeeSawN::ElementsN::ComposeEssential;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::NumericN::MakeRankN;
using SeeSawN::NumericN::QuadraticPolynomialRoots;

/**
 * @brief One-sided fundamental matrix estimation
 * @remarks The first image is fully calibrated. For the second image, only the focal length is unknown. The algorithm estimates f2 and FK1
 * @remarks Ref 1: "Polynomial Eigenvalue Solutions to the 5-pt and 6-pt Relative Pose Problems, ", Z. Kukelova, M. Bujnak and T. Pajdla, BMVC 2008
 * @remarks Ref 2: "3D reconstruction from image collections with a single known focal length, " M. Bujnak, Z. Kukelova, T. Padjla, ICCV 2009
 * @remarks Ref 3: "One-sided Fundamental Matrix Estimation, ", J. H. Brito, C. Zach, K. Koser, M. J. Ferreira, M. Pollefeys, BMVC 2012
 * @remarks: Algorithm:
 * 	- Compute the one-sided F and the focal length for the second camera via Ref 1/2. The resulting F satisfies the epipolar constraint, but not the trace and the determinant constraints
 * 	- Compute the closest valid E to the (f,F) pair
 * 	- Recompute F. This F satisfies the trace and the determinant constraints, but violates the epipolar constraint.
 * 	- If necessary, the result can be decomposed into an E and f, via Ref 3.
 * @remarks Alternative strategy (inspired by R3): Estimate F via a non-minimal normalised set with 7/8-pt algorithm, then decompose
 * @remarks As an initial guess for f, Ref 3 recommends \f$ \frac{width/2}{\tan{FoV/2} } \f$, with a FoV estimate of 50 degrees.
 * @warning The point correspondences must be normalised, so that the known calibration parameters are removed
 * @ingroup GeometrySolvers
 * @nosubgrouping
 */
class OneSidedFundamentalSolverC : public GeometrySolverBaseC<OneSidedFundamentalSolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 6, true, 10, 9, 6>
{
	private:

		typedef GeometrySolverBaseC<OneSidedFundamentalSolverC, EpipolarMatrixT, EpipolarSampsonErrorC, 2, 2, 6, true, 10, 9, 6> BaseT;	///< Type of the base

		typedef Matrix<double, BaseT::nDOF, BaseT::nParameter> MinimalCoefficientMatrixT;	///< Type of the coefficient matrix for the minimal case
		typedef Matrix<double, Eigen::Dynamic, BaseT::nParameter> CoefficientMatrixT;	///< A generic coefficient matrix
		typedef typename BaseT::model_type ModelT;	///< 3x3 matrix
		typedef DLTProblemC<ModelT, MinimalCoefficientMatrixT, CoefficientMatrixT, SeeSawN::GeometryN::DetailN::EpipolarTag, BaseT::sGenerator, 0, 3> DLTProblemT;	///< DLT problem for 6-point one-sided fundamental matrix estimation
																																												//Basis solutions does not have to be rank-2

		typedef MultivariateGaussianC<OneSidedFundamentalMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		typedef Matrix<RealT, 3, 1> Vector3T;	///< A 3x1 floating point vector

		/** @name Implementation details */ //@{
		typedef Matrix<double, 10, 10> Matrix10T;	///< A 10x10 double matrix
		static tuple<Matrix10T, Matrix10T> ComputeCoefficients(const list<EpipolarMatrixT>& basis);	///< Computes the coefficient of the equation system

		typedef tuple<ModelT, double> SolutionT;	///< Solution type of the minimal solver. [F; focal length]
		static list<SolutionT> ModelFromBasis(const list<ModelT>& basis);	///< Computes the one-sided fundamental matrices and focal lengths from a basis
		template<class CorrespondenceRangeT> static list<ModelT> Project(const list<SolutionT>& solutions, const CorrespondenceRangeT& validators);	///< Projects the solutions to valid fundamental matrices
		//@}

	public:

		/** @name GeometrySolverConceptC interface */ //@{
		typedef typename BaseT::real_type distance_type;	///< Type of the distance between two models

		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the models that best explain the correspondence set

		static double Cost(unsigned int nCorrespondences=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<QuaternionT, RealT, RealT, RealT> minimal_model_type;	///< A minimally parameterised model. [Rotation, azimuth, elevation, focal length]
		template<class CorrespondenceRangeT> static minimal_model_type MakeMinimal(const ModelT& model, const CorrespondenceRangeT& correspondences);	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class ModelRangeT, class WeightRangeT, class CorrespondenceRangeT> static GaussianT ComputeSampleStatistics(const ModelRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const CorrespondenceRangeT& correspondences);	///< Computes the sample statistics for a set of one-sided fundamental matrices
		//@}

		/** @name Minimally-parameterised model */ //@{
		static constexpr unsigned int iRotation=0;	///< Index of the rotation
		static constexpr unsigned int iAzimuth=1;	///< Index of the azimuth
		static constexpr unsigned int iElevation=2;	///< Index of the elevation
		static constexpr unsigned int iFocalLength=3;	///< Index of the focal length
		//@}

		static optional<tuple<EpipolarMatrixT, double> > DecomposeOneSidedF(const ModelT& mFd, bool flagOverrideTrace=false);	///< Decomposes a normalised one-sided fundamental matrix into an essential matrix and a focal length

	private:

		friend OneSidedFundamentalMinimalDifferenceC;

		/** @name Implementation details */ //@{
		typedef EssentialSolverC::minimal_model_type EssentialMinimalT;	///< Minimal model type for an essential matrix
		static EssentialMinimalT ExtractMinimalEssential(const minimal_model_type& minimalModel);	///< Extracts the essential matrix component from a minimal model
		//@}

};	//class OneSidedFundamentalSolverC

/**
 * @brief Difference functor for minimally-represented one-sided fundamental matrices
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct OneSidedFundamentalMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef OneSidedFundamentalSolverC::minimal_model_type first_argument_type;
	typedef OneSidedFundamentalSolverC::minimal_model_type second_argument_type;
	typedef Matrix<OneSidedFundamentalSolverC::real_type, OneSidedFundamentalSolverC::DoF(), 1> result_type;	///< Difference in vector form: [Rotation vector; azimuth; inclination; focal length]

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between to minimally-represented one-sided fundamental matrices
	//@}
};	//struct OneSidedFundamentalMinimalDifferenceC


/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Projects the solutions to valid fundamental matrices
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] solutions List of matrix;focal length pairs.
 * @param[in] validators Validation set. Only the first 3 elements are used
 * @return A list of fundamental matrices
 * @pre \c CorrespondenceRangeT supports the bimap interface (unenforced)
 * @pre \c CorrespondenceRangeT is a bimap of elements of type \c Coordinate2DT
 * @pre \c validators has at least 3 elements
 */
template<class CorrespondenceRangeT>
auto OneSidedFundamentalSolverC::Project(const list<SolutionT>& solutions, const CorrespondenceRangeT& validators) -> list<ModelT>
{
	//Preconditions
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, Coordinate2DT>::value, "OneSidedSolverC::Project: CorrespondenceRangeT must hold elements of type Coordinate2DT");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, Coordinate2DT>::value, "OneSidedSolverC::Project: CorrespondenceRangeT must hold elements of type Coordinate2DT");
	assert(boost::distance(validators)>=3);

	typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;	//Validator set
	auto itVB=boost::begin(validators);
	typename CoordinateTransformations2DT::affine_transform_type mA;	//Normalising transformation for coordinates in the second image

	list<ModelT> output;
	for(const auto& current : solutions)
	{
		//Make E
		EpipolarMatrixT mE=Vector3T(get<1>(current), get<1>(current), 1).asDiagonal()*get<0>(current);

		//Make validation set
		mA.matrix()=Vector3T(1, 1, get<1>(current)).asDiagonal();	//Inverse of the intrinsic calibration matrix
		BimapT normalisedValidator;
		auto it=itVB;
		for(size_t c=0; c<3; ++c, advance(it,1))
			normalisedValidator.push_back( typename BimapT::value_type(it->left, CoordinateTransformations2DT::TransformCoordinate(it->right, mA)));

		//Project E to the closest valid essential matrix, and decompose
		optional<tuple<Coordinate3DT, RotationMatrix3DT>> relativeCalibration=DecomposeEssential(mE, normalisedValidator);

		if(!relativeCalibration)
			continue;

		//Recompose E, and convert to one-sided F
		EpipolarMatrixT mFv=mA.matrix()*ComposeEssential(get<0>(*relativeCalibration), get<1>(*relativeCalibration));
		mFv.normalize();

		//Do not let anything through that cannot be decomposed
		if(!DecomposeOneSidedF(mFv))
			continue;

		output.push_back(mFv);
	}	//for(const auto& current : models)

	return output;
}	//list<ModelT> Project(const list<tuple<ModelT, double> >& models)

/**
 * Computes the models that best explain the correspondence set
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] correspondences Correspondences
 * @return A list of models. Empty if the solver fails
 * @pre Points in \c correspondences are normalised as follows (unenforced)
 * 	- Points in the first image are multiplied with the inverse of the known intrinsic calibration matrix
 * 	- Points in the second image are multiplied with the inverse of the partially known calibration matrix. The only unknown element, f, can be set to 1. However, a reasonable initial value improves the stability of the calculations
 * 	@remarks "3D reconstruction from image collections with a single known focal length, " M. Bujnak, z. Kukelova, T. Padjla, ICCV 2009
 */
template<class CorrespondenceRangeT>
auto OneSidedFundamentalSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	if(boost::distance(correspondences)<sGenerator)
		return list<ModelT>();

	list<ModelT> basis=DLTC<DLTProblemT>::Run(correspondences, 3);

	if(basis.size()!=3)
		return list<ModelT>();

	list<SolutionT> solutions=ModelFromBasis(basis);
	return Project(solutions, correspondences);
}	//list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const

/**
 * @brief Converts a model to the minimal form
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] model Model to be converted
 * @param[in] correspondences A small validation set to distinguish between the 4 possible decompositions of \c model
 * @pre \c model has non-zero norm
 * @pre \c model is a valid one-sided fundamental matrix (unenforced)
 * @pre \c model is normalised, i.e., the known intrinsic matrix is of the form \c diag(f,f,1) (unenforced)
 * @pre Right members of \c correspondences are normalised (unenforced)
 * @return Minimal form
 * @remarks http://en.wikipedia.org/wiki/Spherical_coordinate_system
 */
template<class CorrespondenceRangeT>
auto OneSidedFundamentalSolverC::MakeMinimal(const ModelT& model, const CorrespondenceRangeT& correspondences) -> minimal_model_type
{
	//Decompose F to get f and mE
	double f;
	EpipolarMatrixT mE;
	tie(mE, f)=*DecomposeOneSidedF(model, true);	//Trace constraint is overriden, so cannot fail for a vaild model

	//Decompose the essential matrix
	typename EssentialSolverC::minimal_model_type minimalEssential=EssentialSolverC::MakeMinimal(mE, correspondences);

	//Compose the output
	minimal_model_type output;
	get<iRotation>(output)=get<iRotation>(minimalEssential);
	get<iAzimuth>(output)=get<iAzimuth>(minimalEssential);
	get<iElevation>(output)=get<iElevation>(minimalEssential);
	get<iFocalLength>(output)=f;

	return output;
}	//auto MakeMinimal(const ModelT& model) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of one-sided fundamental matrices
 * @tparam ModelRangeT A range of one-sided fundamental matrices
 * @tparam WeightRangeT A range weight values
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] mFRange Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] correspondences A small validation set to distinguish between the 4 possible decompositions of \c model
 * @return Gaussian for the sample statistics
 * @pre \c ModelRangeT is a forward range of elements of type \c EpipolarMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c mFRange should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @pre \c correspondences is appropriately normalised (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c mFRange has 6 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class ModelRangeT, class WeightRangeT, class CorrespondenceRangeT>
auto OneSidedFundamentalSolverC::ComputeSampleStatistics(const ModelRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const CorrespondenceRangeT& correspondences) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<ModelRangeT>));
	static_assert(is_same<typename range_value<ModelRangeT>::type, EpipolarMatrixT>::value, "OneSidedFundamentalSolverC::ComputeSampleStatistics: ModelRangeT must have elements of type EpipolarMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "EssentialSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(mFRange)==boost::distance(wMean));
	assert(boost::distance(mFRange)==boost::distance(wCovariance));

	//Decompose
	unsigned int nSample=boost::distance(mFRange);
	vector<QuaternionT> qList(nSample);
	vector<RealT> aList(nSample);	//Azimuth
	vector<RealT> eList(nSample);	//Elevation
	vector<RealT> fList(nSample);	//Focal length
	size_t index=0;
	for(const auto& current : mFRange)
	{
		tie(qList[index], aList[index], eList[index], fList[index])=MakeMinimal(current, correspondences);
		++index;
	}	//for(const auto& current : mFRange)

	//Mean
	minimal_model_type meanModel;
	get<iAzimuth>(meanModel)=boost::inner_product(aList, wMean, (RealT)0);
	get<iElevation>(meanModel)=boost::inner_product(eList, wMean, (RealT)0);
	get<iFocalLength>(meanModel)=boost::inner_product(fList, wMean, (RealT)0);

	Matrix<RealT, 3, 3> mCovQ;	//Dummy
	tie(get<iRotation>(meanModel), mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	//Covariance
	OneSidedFundamentalMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	auto itWC=boost::const_begin(wCovariance);
	for(size_t c=0; c<nSample; ++c)
	{
		typename OneSidedFundamentalMinimalDifferenceC::result_type diff=subtractor(minimal_model_type(qList[c], aList[c], eList[c], fList[c]), meanModel);
		mCov+= *itWC * (diff * diff.transpose());
		advance(itWC,1);
	}	//for(size_t c=0; c<nSample; ++c)

	return GaussianT(meanModel, mCov);
}	//auto ComputeSampleStatistics(const FundamentalRangeT& mFRange, const WeightRangeT& wMean, const WeightRangeT& wCovariance) -> GaussianT

}	//GeometryN
}	//SeeSawN

//class EssentialSolverC : public GeometrySolverBaseC<EssentialSolverC, EpipolarMatrixT, EpipolarSampsonErrorC, Coordinate2DT, Coordinate2DT, 5, true, 10, 9, 5>
#endif /* ONE_SIDED_FUNDAMENTAL_SOLVER_IPP_0598313 */
