/**
 * @file MultiviewPanoramaBuilder.ipp Implementation details for \c PanoramaBuilderC
 * @author Evren Imre
 * @date 25 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef MULTIVIEW_PANORAMA_BUILDER_IPP_6877129
#define MULTIVIEW_PANORAMA_BUILDER_IPP_6877129

#include <boost/range/algorithm.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <vector>
#include <map>
#include <queue>
#include <tuple>
#include <string>
#include <stdexcept>
#include <cstddef>
#include <utility>
#include <list>
#include <cmath>
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Elements/Correspondence.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::for_each;
using boost::lexical_cast;
using Eigen::Matrix3d;
using Eigen::Vector3d;
using std::vector;
using std::priority_queue;
using std::get;
using std::tie;
using std::ignore;
using std::invalid_argument;
using std::string;
using std::size_t;
using std::pair;
using std::make_pair;
using std::map;
using std::multiset;
using std::list;
using std::sin;
using std::cos;
using std::floor;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::HCoordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::MetricsN::SymmetricTransferErrorConstraint2DT;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::NumericN::CartesianToSpherical;
using SeeSawN::NumericN::SphericalToCartesian;
using SeeSawN::GeometryN::CoordinateStatistics3DT;
using SeeSawN::GeometryN::CoordinateTransformations3DT;

/**
 * @brief Parameters for \c MultiviewPanoramaBuilderC
 * @ingroup Parameters
 */
struct MultiviewPanoramaBuilderParametersC
{
	bool flagFiltering;	///< If \c true , the measurements are filtered wrt symmetric transfer error, reprojection error and cardinality
	unsigned int minObservationCount;	///< Minimum number of observations for a scene point
	double noiseVariance;	///< Noise variance on the image coordinates >0
	double pReject;	///< Probability of rejecting an inlier. [0,1]

	double maxAngularDeviation;	///< Maximum angular deviation for an inlier point, in radians. [0,2pi)

	double binSize;	///< Size of the edge of a quantisation bin on the unit sphere, for decimation. 0 means no decimation. In m

	MultiviewPanoramaBuilderParametersC() : flagFiltering(true), minObservationCount(2), noiseVariance(1), pReject(0.05), maxAngularDeviation(0.01), binSize(0.01)
	{}

};	//struct MultiviewPanoramaBuilderParametersC

/**
 * @brief Parameters for \c MultiviewPanoramaBuilderC
 * @ingroup Diagnostics
 */
struct MultiviewPanoramaBuilderDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation is terminated successfully

	size_t nPoints;	///< Number of points in the panorama

	MultiviewPanoramaBuilderDiagnosticsC() : flagSuccess(false), nPoints(0)
	{}
};	//struct MultiviewPanoramaBuilderDiagnosticsC

/**
 * @brief Multiview panorama builder
 * @remarks Usage notes
 * - All cameras are assumed to be at the origin, and any camera centre components are ignored
 * - The algorithm discards inconsistent correspondences. Correspondences from a k-NN matcher often includes inconsistencies, and may lead to loss of landmarks
 * @ingroup Algorithm
 * @nosubgrouping
 */
class MultiviewPanoramaBuilderC
{
	public:
		typedef vector<vector<Coordinate2DT>> coordinate_stack_type;	///< Type for a stack of feature coordinates
		typedef CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT> correspondence_stack_type;	///< Type for a stack of pairwise feature correspondences

	private:

		typedef coordinate_stack_type CoordinateStackT;
		typedef correspondence_stack_type CorrespondenceStackT;
		typedef CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> PairwiseViewT;	///< Type for the container holding the image correspondences

		typedef CorrespondenceFusionC::cluster_member_type TrackPointT;
		typedef CorrespondenceFusionC::cluster_type TrackT;
		typedef CorrespondenceFusionC::unified_view_type TrackListT;

		typedef pair<size_t, size_t> IndexPairT;	///< Type for a pair of indices

		/** @name Implementation details */ //@{
		static void ValidateInput(const CorrespondenceStackT& correspondenceStack, size_t nCameras, MultiviewPanoramaBuilderParametersC& parameters);	///< Validates the input parameters

		static tuple< map<IndexPairT, Homography2DT>, vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT>  >  > ProcessCameras(const vector<CameraMatrixT>& cameraList);	///< Processes the cameras
		static PairwiseViewT FilterCorrespondences2D(const PairwiseViewT& correspondenceStack, const CoordinateStackT& coordinateStack, const map<IndexPairT, Homography2DT>& homographyList, const MultiviewPanoramaBuilderParametersC& parameters);	///< Filters the image correspondences

		static vector<Coordinate3DT> EstimateDirections(const TrackListT& tracks, const CoordinateStackT& coordinateStack, const vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> >& calibrationParameters, const MultiviewPanoramaBuilderParametersC& parameters);	///< Estimates the 3D direction vectors from image measurements
		static tuple<vector<Coordinate3DT>, TrackListT> FilterDirections(const vector<Coordinate3DT>& directions, const TrackListT& tracks, const CoordinateStackT& coordinateStack, const vector< tuple<IntrinsicCalibrationMatrixT, RotationMatrix3DT> >& calibrationParameters, const MultiviewPanoramaBuilderParametersC& parameters);	///< Filters the estimated directions and returns the inlier track points
		static tuple<vector<Coordinate3DT>, TrackListT> Decimate(const vector<Coordinate3DT>& directions, const TrackListT& tracks, const MultiviewPanoramaBuilderParametersC& parameters);	///< Decimates a set of directions
		//@}

	public:

		static MultiviewPanoramaBuilderDiagnosticsC Run(vector<Coordinate3DT>& directionVectors, TrackListT& inliers, const CorrespondenceStackT& correspondenceStack, const CoordinateStackT& coordinateStack, const vector<CameraMatrixT>& cameraList,  MultiviewPanoramaBuilderParametersC parameters);	///< Runs the algorithm

};	//class MultiviewPanoramaBuilderC

}	//GeometryN
}	//SeeSawN




#endif /* MULTIVIEW_PANORAMA_BUILDER_IPP_6877129 */
