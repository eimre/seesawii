/**
 * @file P2PSolver.cpp Implementation of the 2-point camera orientation solver
 * @author Evren Imre
 * @date 7 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P2PSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Embeds a camera matrix into a 4x4 identity homography
 * @param[in] model Model
 * @return A 4x4 homography corresponding to the model
 */
auto P2PSolverC::MakeRotation(const ModelT& model) -> Rotation3DSolverC::model_type
{
	Rotation3DSolverC::model_type mH=Rotation3DSolverC::model_type::Identity();
	mH.block(0,0,3,4)=model;
	return mH;
}	//Rotation3DSolverC::model_type MakeRotation(const ModelT& model)

/**
 * @brief Computational cost of the solver
 * @param[in] nCorrespondences Number of correspondences
 * @return Computational cost of the solver
 * @remarks No unit tests, as the costs are volatile
 */
double P2PSolverC::Cost(unsigned int nCorrespondences)
{
	//Rotation solver + norm and scaling for each correspondence
	return Rotation3DSolverC::Cost(nCorrespondences) + nCorrespondences * (ComplexityEstimatorC::L2Norm(3) + ComplexityEstimatorC::VectorScale(3));
}	//double Cost(unsigned int nCorrespondences)

/**
 * @brief Minimal form to matrix conversion
 * @param[in] minimalModel A model in minimal form
 * @return A 3x4 camera matrix
 */
auto P2PSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT
{
	return Rotation3DSolverC::MakeModelFromMinimal(minimalModel).block(0,0,3,4);
}	//auto Rotation3DSolverC::MakeModelFromMinimal(const minimal_model_type& minimalModel) -> ModelT

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return Angle between the two models, normalised to the range [0,1]
 */
auto P2PSolverC::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	return Rotation3DSolverC::ComputeDistance(MakeRotation(model1), MakeRotation(model2));
}	//distance_type ComputeDistance(const ModelT& model1, const ModelT& model2)

/**
 * @brief Converts a model to a vector
 * @param[in] model Model
 * @return First 3x3 submatrix of \c model in vector form
 */
auto P2PSolverC::MakeVector(const ModelT& model) -> VectorT
{
	return Rotation3DSolverC::MakeVector(MakeRotation(model));
}	//VectorT MakeVector(const ModelT& model)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel Vector
 * @param[in] dummy Dummy parameter to meet the concept requirements
 * @return A 3x4 camera matrix
 */
auto P2PSolverC::MakeModel(const VectorT& vModel, bool dummy) -> ModelT
{
	return Rotation3DSolverC::MakeModel(vModel, dummy).block(0,0,3,4);
}	//ModelT MakeModel(const VectorT& vModel)
}	//GeometryN
}	//SeeSawN

