/**
 * @file SceneCoverageEstimation.ipp Implementation of \c SceneCoverageEstimationC
 * @author Evren Imre
 * @date 7 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_COVERAGE_ESTIMATION_IPP_5832083
#define SCENE_COVERAGE_ESTIMATION_IPP_5832083

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <array>
#include <map>
#include <set>
#include <list>
#include <tuple>
#include <cstddef>
#include <type_traits>
#include <iterator>
#include <cmath>
#include <stdexcept>
#include <string>
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Geometry/Projection32.h"
#include "../Geometry/LensDistortion.h"
#include "../Metrics/CheiralityConstraint.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::math::constants::pi;
using boost::range_value;
using boost::optional;
using boost::transform;
using boost::count_if;
using boost::set_intersection;
using boost::lexical_cast;
using Eigen::Matrix;
using Eigen::AngleAxis;
using std::vector;
using std::array;
using std::tuple;
using std::get;
using std::map;
using std::size_t;
using std::is_same;
using std::set;
using std::list;
using std::back_inserter;
using std::max;
using std::min;
using std::cos;
using std::greater;
using std::string;
using std::invalid_argument;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::ComputeFocusField;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::GeometryN::ApplyDistortion;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::MetricsN::CheiralityConstraintC;

/**
 * @brief Parameters for \c SceneCoverageEstimationC
 * @ingroup Parameters
 */
struct SceneCoverageEstimationParametersC
{
	double radius;	///< Radius of the sphere representing a volume element, in m. >0
	optional<Coordinate3DT> focusPoint;	///< Focus point for the setup. If valid, the in-focus constraint is enforced

	SceneCoverageEstimationParametersC() : radius(0.1)
	{}
};	//SceneCoverageEstimationParametersC

/**
 * @brief Coverage and related statistics for a set of cameras
 * @remarks Usage notes
 * 	- Scene: Could be a real sparse scene model, or just a lattice covering the capture volume
 * 	- Queries
 * 		- Scene coverage
 * 		- Contribution of a single camera (for removing redundant cameras)
 * 		- Matchability: Success predictor for a sparse/dense feature matching task on a pair of cameras
 * 		- Best view: Witness cameras which offer the most similar (matchable) image to an image taken by a principal camera
 *	- Tests:
 *		- Scene: Minimum redundancy
 *		- View: RoI (framing) and resolution
 *		- Pair: Relative scale and viewpoint difference
 *		- Focus: Whether a point is in focus. Performed only for cameras with a valid fNumber and pixel size
 * @ingroup Algorithm
 * @nosubgrouping
 */
class SceneCoverageEstimationC
{
	public:

		/** @name Unary constraint type */ ///@{
		typedef tuple< double, FrameT> unary_constraint_type;	///< A constraint for a single camera
		static constexpr unsigned int iArea=0;	///< Index of the relative area component. >=0
		static constexpr unsigned int iRoI=1;
		///@}

	private:

		typedef unary_constraint_type UnaryConstraintT;	///< A constraint for a single camera
		typedef tuple<double, double> RealPairT;	///< A pair of real numbers
		typedef tuple<unsigned int, unsigned int> UIntPairT;	///< A pair of unsigned interegers


		/** @name ViewT */ //@{
		typedef tuple<CameraC, UnaryConstraintT> ViewT;	///< A view
		static constexpr unsigned int iCamera=0;	///< Index of the camera component
		static constexpr unsigned int iUConstraint=1;	///< Index of the constraint component
		//@}

		/** @name ProjectionT */ //@{
		typedef tuple<double, Coordinate3DT> ProjectionT;	///< 2D projection of a volume element
		static constexpr unsigned int iProjectedArea=0; ///< Index of the area component: Area relative to the image
		static constexpr unsigned int iProjectionRay=1;	///< Index of the projection ray component
		//@}

		typedef vector<optional<ProjectionT> > ImageT;	///< An image, as a vector of projection of the volume elements

		/** @name Configuration */ ///@{
		bool flagValid;	///< \c true if the algorithm is properly initialised
		vector<Coordinate3DT> scene;	///< Scene, as a point cloud
		SceneCoverageEstimationParametersC parameters;	///< Parameters
		///@}

		/** @name State */ ///@{

		vector<map<unsigned int, ProjectionT> > projections;	///< Projections, organised wrt/scene. Each element is a map: [camera id; projection]
		map<unsigned int, ImageT > images;	///< Projections, organised wrt/cameras. [view id; projections]. Vertices not covered are invalid
		map<unsigned int, ViewT> views;	///< Views
		///@}

		/** @name Implementation details */ ///@{
		void ValidateParameters(const SceneCoverageEstimationParametersC& parameters) const;	///< Validates the input parameters
		void ValidateUnaryConstraint(const UnaryConstraintT& constraint) const;	///< Validates a unary constraint

		double ComputeArea(const Coordinate3DT& centre, const Projection32C& projector, const Coordinate3DT& vx, const Coordinate3DT& vy) const;	///< Computes the area of the projection of a volume element

		ImageT ProjectScene(const CameraC& camera, const UnaryConstraintT& constraint) const;	///< Projects the scene onto a camera
		vector<optional<RealPairT> > PredictAppearanceSimilarity(const ImageT& image1, const ImageT& image2) const;	///< Predicts the appearance of the similarity of vertices covered by a pair of cameras
		///@}

	public:


		/** @name Constructors */ ///@{
		SceneCoverageEstimationC();	///< Default constructor
		template <class CoordinateRangeT> SceneCoverageEstimationC(const CoordinateRangeT& sscene, SceneCoverageEstimationParametersC pparameters);
		///@}

		/** @name View and scene management */ ///@{
		optional<UIntPairT> AddView(const CameraC& camera, const UnaryConstraintT& constraint, bool flagSimulation=false);	///< Adds a new view
		optional<UIntPairT> RemoveView(unsigned int iView, bool flagSimulation=false);	///< Removes a view from \c views
		///@}

		/** @name Queries */ ///@{
		tuple<vector<size_t>, vector<size_t> > GetCoveredVertices(unsigned int minRedundancy) const;	///< Returns a list of the vertices covered by the cameras

		unsigned int PredictMatchingPerformance(unsigned int iView1, unsigned int iView2, double maxAngle, double maxScale) const;	///< Returns the number of vertices that can be potentially matched between two views
		map<unsigned int, size_t, greater<unsigned int> > RankViews(const CameraC& camera, const UnaryConstraintT& constraint, double maxAngle, double maxScale) const;	///< Ranks the views wrt/matching performance with the input

		map<unsigned int, UIntPairT> ComputeCoverageStatistics() const;	///< Coverage statistics for each view
		optional<UIntPairT> ComputeCoverageStatistics(unsigned int iView) const;	///< Coverage statistics for a view

		typedef Matrix<ValueTypeM<CameraMatrixT>::type, 7, 1> gradient_type;	///< Type of the gradient vector
		tuple<gradient_type, gradient_type> ComputeCoverageGradient(const CameraC& camera, const UnaryConstraintT& constraint, double epsilonP=0.1, double epsilonO=0.05, double epsilonF=25);	///< Computes the coverage gradient wrt/ pose and focal length
		///@}

		/** @name Accessors */ ///@{
		map<unsigned int, CameraC> GetCameras() const;	///< Returns the cameras in \c views
		const vector<Coordinate3DT>& GetScene() const;	///< Returns a constant reference to \c scene
		///@}

		/**@name Utilities */ ///@{
		void Clear();	///< Clears the state
		///@}
};	//class SceneCoverageEstimationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @param[in] sscene Scene
 * @param[in] pparameters Parameters
 */
template <class CoordinateRangeT>
SceneCoverageEstimationC::SceneCoverageEstimationC(const CoordinateRangeT& sscene, SceneCoverageEstimationParametersC pparameters)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
	static_assert(is_same< typename range_value<CoordinateRangeT>::type, Coordinate3DT>::value, "SceneCoverageEstimationC::SceneCoverageEstimationC : CoordinateRangeT must have elements of type Coordinate3DT");

	ValidateParameters(pparameters);
	parameters=pparameters;

	flagValid=true;

	size_t nPoints=boost::distance(sscene);
	scene.resize(nPoints);
	copy(sscene, scene.begin());

	projections.resize(nPoints);
}	//SceneCoverageEstimationC(const CoordinateRangeT& sscene, SceneCoverageEstimationParametersC parameters)


}	//GeometryN
}	//SeeSawN

#endif /* SCENECOVERAGEESTIMATION_IPP_ */
