/**
 * @file MultiviewTriangulation.h Public interface for MultiviewTriangulationC
 * @author Evren Imre
 * @date 10 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTIVIEW_TRIANGULATION_H_0932803
#define MULTIVIEW_TRIANGULATION_H_0932803

#include "MultiviewTriangulation.ipp"
namespace SeeSawN
{
namespace GeometryN
{

struct MultiviewTriangulationParametersC;	///< Parameters for \c MultiviewTriangulationC
struct MultiviewTriangulationDiagnosticsC;	///< Diagnostics for \c MultiviewTriangulationC
class MultiviewTriangulationC;	///< Multiview triangulation algorithm
}	//GeometryN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template class std::map<double, std::size_t>;
extern template double boost::lexical_cast<double, std::string>(const std::string&);


#endif /* MULTIVIEW_TRIANGULATION_H_0932803 */
