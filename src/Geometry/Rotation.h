/**
 * @file Rotation.h Public interface for various rotation-related functions
 * @author Evren Imre
 * @date 9 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROTATION_H_8791232
#define ROTATION_H_8791232

#include "Rotation.ipp"
#include <vector>
namespace SeeSawN
{
namespace GeometryN
{

RotationVectorT QuaternionToRotationVector(QuaternionT q);	///< Converts a quaternion to an axis-angle vector
RotationVectorT RotationMatrixToRotationVector(RotationMatrix3DT mR);	///< Converts a rotation matrix to an axis-angle vector
AxisAngleT RotationVectorToAxisAngle(const RotationVectorT& v);	///< Converts a rotation vector to an axis-angle object
QuaternionT RotationVectorToQuaternion(const RotationVectorT& v);	///< Converts a rotation vector to a quaternion

template<class QuaternionRangeT, class WeightRangeT> tuple<QuaternionT, typename QuaternionT::Matrix3> ComputeQuaternionSampleStatistics(const QuaternionRangeT& quaternions, const WeightRangeT& wMean, const WeightRangeT& wCovariance);	///< Computes the sample statistics for a set of quaternions

Matrix<QuaternionT::Scalar, 9, 4> JacobianQuaternionToRotationMatrix(const QuaternionT& q);	///< Jacobian for the quaternion to rotation matrix conversion
Matrix<QuaternionT::Scalar,3,4> JacobianQuaternionToRotationVector(const QuaternionT& q);	///< Jacobian for the quaternion to rotation vector conversion
Matrix<QuaternionT::Scalar, 4, 3> JacobianRotationVectorToQuaternion(const RotationVectorT& vRotation);    ///< Computes the Jacobian of the rotation vector->quaternion conversion

Matrix<QuaternionT::Scalar, 4, 4> JacobianQuaternionConjugation();	///< Jacobian of the quaternion conjugation operation
Matrix<QuaternionT::Scalar, 4, 8> JacobianQuaternionMultiplication(const QuaternionT& q1, const QuaternionT& q2);	///< Jacobian of the quaternion multiplication operation

/********** EXTERN TEMPLATES **********/
using std::vector;
using Eigen::Matrix3d;
extern template tuple<QuaternionT, Matrix3d> ComputeQuaternionSampleStatistics(const vector<QuaternionT>&, const vector<double>&, const vector<double>&);

}	//GeometryN
}	//SeeSawN

#endif /* ROTATION_H_8791232 */
