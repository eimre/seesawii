/**
 * @file Projection32.cpp Implementation of \c Projection32C
 * @author Evren Imre
 * @date 8 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Projection32.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
Projection32C::Projection32C() : flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] mmP Camera matrix
 * @post Valid object
 */
Projection32C::Projection32C(const CameraMatrixT& mmP) : mP(mmP), flagValid(true)
{}

/**
 * @brief Projects a scene point onto the image plane of the camera
 * @param[in] p3D Scene point
 * @return Projection of \c p3D on the image plane. Might have infinite coordinates
 * @pre \c flagValid=true
 */
Coordinate2DT Projection32C::operator()(const Coordinate3DT& p3D) const
{
	assert(flagValid);
	return (mP*p3D.homogeneous()).eval().hnormalized();
}	//Coordinate2DT operator()(const Coordinate3DT& p3D) const

/**
 * @brief Projects a scene onto the image plane of a camera with a lens distortion.
 * @param[in] p3D Scene point
 * @param[in] distortion Lens distortion
 * @throws invalid_argument if the distortion model cannot be handled
 * @return Projection of \c p3D on the image plane. Might have infinite coordinates. Invalid if lens distortion fails
 */
optional<Coordinate2DT> Projection32C::operator()(const Coordinate3DT& p3D, const LensDistortionC& distortion) const
{
	if(distortion.modelCode==LensDistortionCodeT::DIV)
		return this->operator()(p3D, LensDistortionDivC(distortion.centre, distortion.kappa));

	if(distortion.modelCode==LensDistortionCodeT::FP1)
		return this->operator()(p3D, LensDistortionFP1C(distortion.centre, distortion.kappa));

	if(distortion.modelCode==LensDistortionCodeT::OP1)
		return this->operator()(p3D, LensDistortionOP1C(distortion.centre, distortion.kappa));

	if(distortion.modelCode==LensDistortionCodeT::NOD)
		return this->operator()(p3D, LensDistortionNoDC(distortion.centre, distortion.kappa));

	throw(invalid_argument("Projection32C::operator() : Invalid model code. The function can handle only DIV, FP1, OP1 and NOD models "));

	return this->operator()(p3D);
}	//optional<Coordinate2DT> Projection32C::operator()(const Coordinate3DT& p3D, const LensDistortionC& distortion) const

}	//GeometryN
}	//SeeSawN

