/**
 * @file Triangulation.ipp Implementation of 2-view triangulation algorithm
 * @author Evren Imre
 * @date 5 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef TRIANGULATION_IPP_0981322
#define TRIANGULATION_IPP_0981322

#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/range/concepts.hpp>
#include <boost/concept_check.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <vector>
#include <array>
#include <tuple>
#include <cstddef>
#include <cmath>
#include <climits>
#include <complex>
#include <type_traits>
#include "../Elements/Coordinate.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Numeric/Polynomial.h"
#include "CoordinateTransformation.h"

#include <iostream>
namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::SinglePassRangeConcept;
using boost::math::pow;
using Eigen::Matrix;
using Eigen::JacobiSVD;
using std::vector;
using std::array;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::size_t;
using std::hypot;
using std::complex;
using std::numeric_limits;
using std::fabs;
using std::sqrt;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::HCoordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::NumericN::PolynomialRoots;

/**
 * @brief 2-view triangulation algorithms
 * @remarks R. Hartley, A. Zisserman, "Multiple View Geometry in Computer Vision," 2nd Ed., 2003, pp.312
 * @remarks P. Lindstrom, "Triangulation Made Easy," CVPR 2010
 * @remarks Algorithm:
 * - Project the 2D points to the epipolar manifold, where the epipolar constraint is exactly satisfied
 * 	- Optimal: Minimises the reprojection error
 * 	- Fast: Suboptimal, but works well in practice. Actually, it works better than optimal in numerically problematic cases
 * - Triangulate via eigentriangulation
 * @ingroup Algorithm
 * @nosubgrouping
 */
class TriangulatorC
{
	private:

		typedef ValueTypeM<Coordinate2DT>::type RealT;	///< Floating point type

		static Coordinate3DT EigenTriangulation(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const Coordinate2DT& x1, const Coordinate2DT& x2);	///< Eigen triangulation
		static Coordinate3DT Triangulate(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2, bool flagFast);	///< Optimal triangulation

	public:

		static Coordinate3DT Triangulate(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const Coordinate2DT& x1, const Coordinate2DT& x2, bool flagFast=true);	///< Triangulates a single corresponding pair
		template<class CorrespondenceRangeT> vector<Coordinate3DT> operator()(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const CorrespondenceRangeT& correspondences, bool flagFast=true) const;	///< Triangulates a list of 2D correspondences
};	//class OptimalTriangulationC

/********** FREE FUNCTIONS **********/
tuple<Coordinate2DT, Coordinate2DT> OptimalProjectToEpipolarManifold(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2);	///< Projects a 2D correspondence to the epipolar manifold defined by a fundamental matrix
tuple<Coordinate2DT, Coordinate2DT> FastProjectToEpipolarManifold(const EpipolarMatrixT& mF, const Coordinate2DT& x1, const Coordinate2DT& x2);	///< Projects a 2D correspondence to the epipolar manifold defined by a fundamental matrix

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Triangulates a list of 2D correspondences
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] mP1 First camera matrix
 * @param[in] mP2 Second camera matrix
 * @param[in] correspondences Image correspondences
 * @param[in] flagFast If \c true , fast but suboptimal triangulation
 * @return Triangulated points
 * @pre \c CorrespondenceRangeT is a single pass range
 * @pre \c CorrespondenceRangeT supports the interface of a bimap holding elements of type \c Coordinate2DT (unenforced)
 */
template<class CorrespondenceRangeT>
vector<Coordinate3DT> TriangulatorC::operator()(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const CorrespondenceRangeT& correspondences, bool flagFast) const
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, Coordinate2DT>::value, "OptimalTriangulationC::operator() : CorrespondenceRangeT::left_key_type must be of type Coordinate2DT");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, Coordinate2DT>::value, "OptimalTriangulationC::operator() : CorrespondenceRangeT::left_key_type must be of type Coordinate2DT");

	EpipolarMatrixT mF=CameraToFundamental(mP1, mP2);

	size_t nCorrespondences=correspondences.size();
	vector<Coordinate3DT> triangulated; triangulated.reserve(nCorrespondences);
	size_t index=0;
	for(const auto& current : correspondences)
	{
		triangulated.emplace_back(Triangulate(mP1, mP2, mF, current.left, current.right, flagFast));
		++index;
	}	//for(const auto& current : correspondences)

	return triangulated;
}	//tuple< vector<Coordinate3DT>, vector<size_t> > Run(const CameraMatrixT& mP1, const CameraMatrixT& mP2, const CorrespondenceRangeT& correspondences, bool flagCheirality)

}	//GeometryN
}	//SeeSawN

#endif /* TRIANGULATION_IPP_0981322 */
