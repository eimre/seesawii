/**
 * @file Projection32.h Public interface for \c Projection32C
 * @author Evren Imre
 * @date 8 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PROJECTION32_H_6598243
#define PROJECTION32_H_6598243

#include "Projection32.ipp"

namespace SeeSawN
{
namespace GeometryN
{

class Projection32C;	///< 3D-2D projection operator
template<class LensDistortionT> class Projection32dC;	///< Combined 3D-2D projection and lens distortion operator
}	//GeometryN
}	//SeeSawN

#endif /* PROJECTION32_H_6598243 */
