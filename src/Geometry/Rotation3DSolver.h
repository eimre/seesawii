/**
 * @file Rotation3DSolver.h Public interface for 2-point 3D rotation estimation
 * @author Evren Imre
 * @date 25 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROTATION3D_SOLVER_H_5691232
#define ROTATION3D_SOLVER_H_5691232

#include "Rotation3DSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{

class Rotation3DSolverC;	///< 2-point 3D rotation estimator
struct Rotation3DMinimalDifferenceC;	///< Difference functor for minimally-represented 3D rotations
}	//GeometryN
}	//SeeSawN

#endif /* ROTATION3DSOLVER_H_ */
