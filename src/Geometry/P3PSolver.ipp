/**
 * @file P3PSolver.ipp Implementation of the 3-point pose estimator
 * @author Evren Imre
 * @date 9 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P3P_SOLVER_IPP_2096124
#define P3P_SOLVER_IPP_2096124

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/tuple/tuple.hpp>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <tuple>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <climits>
#include <iterator>
#include <complex>
#include <vector>
#include <type_traits>
#include <utility>
#include "GeometrySolverBase.h"
#include "Similarity3DSolver.h"
#include "Rotation3DSolver.h"
#include "Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Camera.h"
#include "../Metrics/GeometricError.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Numeric/Polynomial.h"
#include "../Numeric/Complexity.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Wrappers/BoostRange.h"

namespace SeeSawN
{
namespace GeometryN
{

//Forward declarations
struct PoseMinimalDifferenceC;

using boost::math::pow;
using boost::range::for_each;
using boost::ForwardRangeConcept;
using Eigen::umeyama;
using Eigen::Matrix;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;
using std::list;
using std::numeric_limits;
using std::next;
using std::complex;
using std::map;
using std::vector;
using std::priority_queue;
using std::pair;
using std::is_floating_point;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::Similarity3DSolverC;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::NumericN::CubicPolynomialRoots;
using SeeSawN::NumericN::QuadraticPolynomialRoots;
using SeeSawN::NumericN::ComplexityEstimatorC;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::WrappersN::MakeBinaryZipRange;

/**
 * @brief 3-point pose solver
 * @remarks Haralick R. M ,. Lee C. , Ottenberg K. Noelle M., "Analysis and Solutions of the Three-Point Perspective Pose Estimation Problem, " 1991 IEEE Computer Society Conference on Computer Vision and Pattern Recognition, pp.592-598
 * @remarks The solver assumes that
 * 	- The focal length is 1 and the principal point is the origin of the image plane. Therefore, normalise the image coordinates with the inverse of the intrinsic calibration matrix
 * 	- The scene points satisfy the cheirality constraints, i.e. in front of the camera. When this cannot be guaranteed, the solver will still return a valid rotation, however, it is unlikely to gather much support.
 * Also, there are a number of singularities without an obvious geometric meaning.
 * @remarks Distance measure: 1-|<u,v>|/(<u,u> <v,v>)
 * @remarks Minimal parameterisation:  rotation vector; camera centre
 * @warning For the normalisation of the coordinate magnitudes (i.e. following the normalisation with the intrinsics), avoid non-isotropic scaling!
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
class P3PSolverC : public GeometrySolverBaseC<P3PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 3, false, 4, 12, 6>
{
	private:

		typedef GeometrySolverBaseC<P3PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 3, false, 4, 12, 6> BaseT;	///< Type of the geometry solver base

		typedef typename BaseT::model_type ModelT;	///<  CameraMatrixT which contains the rotation
		typedef MultivariateGaussianC<PoseMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		/** @name Implementation details */ //@{
		static list<array<Coordinate3DT,3> > ComputeLocalCooordinates(const array<RealT,3>& edgeLengths, const array<Coordinate3DT,3>& projectionRays);	///< Computes the coordinates of the 3D points in the local reference frame
		//@}

	public:


		/** @name GeometrySolverConceptC interface */ //@{
		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the pose that best explains the correspondence set

		static double Cost(unsigned int dummy=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<Coordinate3DT, RotationVectorT> minimal_model_type;	///< A minimally parameterised model. [Coordinate vector; Axis-angle vector]
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class SampleRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of pose estimates

		//@}

		/** @name Overrides */ //@{
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool dummy=true);	///< Converts a vector to a model
		//@}

		/**@name Minimal representation */ //@{
		static constexpr unsigned int iPosition=0;	///< Index of the position component
		static constexpr unsigned int iOrientation=1;	///< Index of the orientation component
		//@}
};	//class P3PSolverC

/**
 * @brief Difference functor for minimally-represented P2Pf solutions
 * @remarks A model of adaptable binary function concept
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
struct PoseMinimalDifferenceC
{
	/** @name Adaptable binary function interface */ //@{
	typedef P3PSolverC::minimal_model_type first_argument_type;
	typedef P3PSolverC::minimal_model_type second_argument_type;
	typedef Matrix<P3PSolverC::real_type, P3PSolverC::DoF(), 1> result_type;	///< Difference in vector form

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between two minimally-represented P3P solutions
	//@}
};	//struct PoseMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre \c model is a camera matrix (unenforced)
 * @return Minimal form
 */
template<class DummyRangeT>
auto P3PSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	IntrinsicCalibrationMatrixT mK;
	tie(mK, vC, mR)=DecomposeCamera(model, true);

	return make_tuple(vC, RotationMatrixToRotationVector(mR));
}	//auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of pose estimates
 * @tparam SampleRangeT A range of camera matrices
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] samples Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c SampleRangeT is a forward range of elements of type \c CameraMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c samples should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c samples has 6 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class SampleRangeT, class WeightRangeT, class DummyRangeT>
auto P3PSolverC::ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<SampleRangeT>));
	static_assert(is_same<ModelT, typename range_value<SampleRangeT>::type >::value, "P3PSolverC::ComputeSampleStatistics : SampleRangeT must hold elements of type CameraMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "P3PSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(samples)==boost::distance(wMean));
	assert(boost::distance(samples)==boost::distance(wCovariance));

	size_t nSamples=boost::distance(samples);

	//Decompose the model
	vector<QuaternionT> qList; qList.reserve(nSamples);
	Coordinate3DT meanPosition=Coordinate3DT::Zero();
	vector<minimal_model_type> minimalList; minimalList.reserve(nSamples);
	for(const auto& current : MakeBinaryZipRange(samples, wMean))
	{
		minimalList.push_back(MakeMinimal(boost::get<0>(current)));

		qList.push_back(RotationVectorToQuaternion(get<iOrientation>(*minimalList.rbegin())));
		meanPosition+= boost::get<1>(current)*(get<iPosition>(*minimalList.rbegin()));
	}	//for(const auto& current : samples)

	//Mean
	QuaternionT qMean;
	Matrix<RealT, 3, 3> mCovQ;
	tie(qMean, mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	minimal_model_type mean=make_tuple(meanPosition, QuaternionToRotationVector(qMean));

	//Covariance
	PoseMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	for(const auto& current : MakeBinaryZipRange(minimalList, wCovariance))
	{
		typename PoseMinimalDifferenceC::result_type vDif=subtractor(boost::get<0>(current), mean);
		mCov+=boost::get<1>(current) * (vDif * vDif.transpose());
	}	//for(const auto& current : MakeBinaryZipRange(samples, wCovariance))

	return GaussianT(mean, mCov);
}	//GaussianT ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy)

/**
 * @brief Computes the pose that best explains the correspondence set
 * @param[in] correspondences Correspondence set
 * @return A list of up to 4 camera matrices
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap, where the left element is of type \c Coordinate3DT , and the right, \c Coordinate2DT
 */
template<class CorrespondenceRangeT>
auto P3PSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	//Precondition
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "P3PSolverC::operator() : CorrespondenceRangeT::left_key_type must be Coordinate3DT");
	static_assert(is_same<Coordinate2DT, typename CorrespondenceRangeT::right_key_type>::value, "P3PSolverC::operator() : CorrespondenceRangeT::right_key_type must be Coordinate2DT");

	list<ModelT> output;
	if(boost::distance(correspondences)<sGenerator)
		return output;

	//Compute the edge lengths
	array<RealT,3> edgeLengths;
	edgeLengths[0]=(next(correspondences.begin(),1)->left - correspondences.rbegin()->left).norm();
	edgeLengths[1]=(correspondences.begin()->left - correspondences.rbegin()->left).norm();
	edgeLengths[2]=(correspondences.begin()->left - next(correspondences.begin(),1)->left).norm();

	//Coincident points
	if(edgeLengths[0]*edgeLengths[1]*edgeLengths[2] <= numeric_limits<typename BaseT::real_type>::epsilon())
		return output;

	//Compute the direction vectors
	array<Coordinate3DT,3> projectionRays;
	projectionRays[0]=correspondences.begin()->right.homogeneous().normalized();
	projectionRays[1]=next(correspondences.begin(),1)->right.homogeneous().normalized();
	projectionRays[2]=correspondences.rbegin()->right.homogeneous().normalized();

	list<array<Coordinate3DT,3> > localCoordinates=ComputeLocalCooordinates(edgeLengths, projectionRays);

	//Compute a pose for each coordinate set
	Matrix<RealT, 3, 3> world;
	size_t iw=0;
	for(const auto& current : correspondences)
	{
		world.col(iw)=current.left;
		++iw;
	}

	Matrix<RealT, 3, 3> local;
	for(const auto& current : localCoordinates)
	{
		//Correspondences between the world and the camera reference frame
		size_t il=0;
		for(const auto& currentPt : current)
		{
			local.col(il)=currentPt;
			++il;
		}	//for(const auto& currentPt : current)

		Homography3DT mH=umeyama(world, local, true);
		RealT scale = mH.block(0,0,3,3).norm()/sqrt(3);
		RotationMatrix3DT orientation = (mH.block(0,0,3,3))/scale;
		Coordinate3DT position = -(orientation.transpose()) * (mH.col(3).segment(0,3)/scale);

		output.push_back(ComposeCameraMatrix(position, orientation));	//Ignore the scale, which may deviate from 1 due to noise
	}	//for(const auto& current : localCoordinates)

	return output;
}	//auto operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
}	//GeometryN
}	//SeeSawN

#endif /* P3P_SOLVER_IPP_2096124 */
