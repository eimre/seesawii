/**
 * @file MultiviewPanoramaBuilder.h Public interface for \c PanoramaBuilderC
 * @author Evren Imre
 * @date 25 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef MULTIVIEW_PANORAMA_BUILDER_H_7879022
#define MULTIVIEW_PANORAMA_BUILDER_H_7879022

#include "MultiviewPanoramaBuilder.ipp"
namespace SeeSawN
{
namespace GeometryN
{

struct MultiviewPanoramaBuilderParametersC;	///< Parameters for \c MultiviewPanoramaBuilderC
struct MultiviewPanoramaBuilderDiagnosticsC;	///< Parameters for \c MultiviewPanoramaBuilderC
class MultiviewPanoramaBuilderC;	///< Multiview panorama builder

}	//GeometryN
}	//SeeSawN




#endif /* MULTIVIEW_PANORAMA_BUILDER_H_7879022 */
