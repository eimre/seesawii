/**
 * @file FundamentalSolver.h Public interface for 7- and 8-point fundamental matrix solvers
 * @author Evren Imre
 * @date 3 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FUNDAMENTAL_SOLVER_H_3980265
#define FUNDAMENTAL_SOLVER_H_3980265

#include "FundamentalSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{
class Fundamental7SolverC;	///< 7-point fundamental matrix estimator
class Fundamental8SolverC;	///< 8-point fundamental matrix estimator
struct FundamentalMinimalDifferenceC;	///< Difference functor for minimally-represented fundamental matrices

}	//GeometryN
}	//SeeSawN

#endif /* FUNDAMENTAL7_SOLVER_H_3980265 */
