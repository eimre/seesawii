/**
 * @file P3PSolver.h Public interface the 3-point pose estimator
 * @author Evren Imre
 * @date 9 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P3P_SOLVER_H_9027912
#define P3P_SOLVER_H_9027912

#include "P3PSolver.ipp"
namespace SeeSawN
{
namespace GeometryN
{

class P3PSolverC;	///< 3-point pose solver
struct PoseMinimalDifferenceC;	///< Difference functor for 2 minimally represented poses

}	//GeometryN
}	//SeeSawN

#endif /* P3P_SOLVER_H_9027912 */
