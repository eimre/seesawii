/**
 * @file CoordinateStatistics.ipp Implementation of CoordinateStatisticsC
 * @author Evren Imre
 * @date 26 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COORDINATE_STATISTICS_IPP_3216535
#define COORDINATE_STATISTICS_IPP_3216535

#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <tuple>
#include <limits>
#include <type_traits>
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::optional;
using Eigen::Array;
using std::tuple;
using std::get;
using std::make_tuple;
using std::numeric_limits;
using std::is_same;
using SeeSawN::ElementsN::CoordinateVectorT;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Statistics of coordinate sets
 * @tparam ValueT Type of a coordinate entry
 * @tparam DIM Dimensionality of a coordinate. \c Eigen::Dynamic for dynamic vectors
 * @pre \c ValueT is a floating point type
 * @ingroup Geometry
 * @nosubgrouping
 */
template<typename ValueT, int DIM>
class CoordinateStatisticsC
{
    private:
		typedef CoordinateVectorT<ValueT, DIM> CoordinateT;	///< Type of the coordinate

    public:

        /** @name Operations */ //@{
        typedef Array<ValueT, DIM, 1> ArrayD;  ///< A Dx1 array
        typedef Array<ValueT, DIM, 2> ArrayD2;  ///< A Dx2 array
        typedef CoordinateT coordinate_type;	///< Coordinate type

        template<class CoordinateRangeT> static optional<tuple<ArrayD, ArrayD> > ComputeMeanAndStD(const CoordinateRangeT& coordinates);  ///< Computes the per-dimension mean and std
        template<class CoordinateRangeT> static optional<ArrayD2> ComputeExtent(const CoordinateRangeT& coordinates);   ///< Computes the per-dimension spatial extent
        //@}
};  //class CoordinateStatisticsC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Computes the per-dimension mean and std
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinate set
 * @return A tuple of arrays: [Mean, StD]. Invalid if \c coordinates is empty
 * @pre \c CoordinateRangeT is a forward range of \c CoordinateT
 */
template<typename ValueT, int DIM>
template<class CoordinateRangeT>
auto CoordinateStatisticsC<ValueT, DIM>::ComputeMeanAndStD(const CoordinateRangeT& coordinates) -> optional< tuple<ArrayD, ArrayD> >
{
    //Precondition
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateStatisticsC::ComputeMeanAndStD : CoordinateRangeT must hold elements of type coordinate_type");

    if(boost::empty(coordinates))
        return optional<tuple<ArrayD, ArrayD> >();

    size_t sCoordinate=boost::begin(coordinates)->size();
    CoordinateT x(sCoordinate); x.setZero();
    CoordinateT x2(sCoordinate); x2.setZero();
    for(const auto& current : coordinates)
    {
        x+=current;
        x2+= (current.array().square()).matrix();
    }   //for(const auto& current : coordinates)

    size_t nElements=boost::distance(coordinates);

    ArrayD mean= x/nElements;
    ArrayD std= (x2.array()/nElements - mean.square()).sqrt();	//Biased, but more robust. If you ever switch to the unbiased form, check the assertions (nonempty input) in the callers across the code

    return make_tuple(mean,std);
}   //tuple<ArrayDd, ArrayDd> ComputeMeanAndStD(const CoordinateRangeT& coordinates)

/**
 * @brief Computes the per-dimension spatial extent
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinate set
 * @return A Dx2 array, where each row indicate the minimum and maximum coordinates in the corresponding dimension. Invalid if \c coordinates is empty
 * @pre \c CoordinateRangeT is a forward range of \c CoordinateT
 */
template<typename ValueT, int DIM>
template<class CoordinateRangeT>
auto CoordinateStatisticsC<ValueT, DIM>::ComputeExtent(const CoordinateRangeT& coordinates) -> optional<ArrayD2>
{
    //Precondition
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_same< typename range_value<CoordinateRangeT>::type, CoordinateT >::value, "CoordinateStatisticsC::ComputeExtent : CoordinateRangeT must hold elements of type coordinate_type");

    if(boost::empty(coordinates))
        return optional<ArrayD2>();

    //Initialise
    size_t sCoordinate=boost::begin(coordinates)->size();
    ArrayD2 output;
    output.resize(sCoordinate,2);
    output.col(0).setConstant( numeric_limits<double>::infinity());
    output.col(1)=-output.col(0);

    for(const auto& current : coordinates)
    {
        output.col(0) = (output.col(0)<=current.array() ).select(output.col(0), current.array());
        output.col(1) = (output.col(1)>=current.array() ).select(output.col(1), current.array());
    }   //for(const auto& current : coordinates)

    return output;
}   //ArrayD2d ComputeExtent(const CoordinateRangeT& coordinates)

}   //GeometryN
}	//SeeSawN

#endif /* COORDINATE_STATISTICS_IPP_3216535 */
