/**
 * @file P4PSolver.ipp Implementation of the 4-point calibration solver
 * @author Evren Imre
 * @date 12 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P4P_SOLVER_IPP_1502721
#define P4P_SOLVER_IPP_1502721

#include <boost/optional.hpp>
#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <type_traits>
#include <tuple>
#include <list>
#include <array>
#include <iterator>
#include <limits>
#include <complex>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Metrics/GeometricError.h"
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Wrappers/BoostRange.h"
#include "../Numeric/Complexity.h"
#include "GeometrySolverBase.h"
#include "Similarity3DSolver.h"
#include "P3PSolver.h"
#include "Rotation.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using boost::ForwardRangeConcept;
using boost::math::pow;
using Eigen::SparseMatrix;
using Eigen::SparseLU;
using Eigen::Matrix;
using Eigen::EigenSolver;
using Eigen::Triplet;
using Eigen::umeyama;
using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;
using std::is_same;
using std::list;
using std::array;
using std::advance;
using std::next;
using std::numeric_limits;
using std::sqrt;
using std::abs;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::WrappersN::MakeBinaryZipRange;
using SeeSawN::GeometryN::GeometrySolverBaseC;
using SeeSawN::GeometryN::Similarity3DSolverC;
using SeeSawN::GeometryN::P3PSolverC;
using SeeSawN::GeometryN::PoseMinimalDifferenceC;
using SeeSawN::GeometryN::QuaternionT;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::NumericN::ComplexityEstimatorC;

struct P4PMinimalDifferenceC;	//Forward declaration

/**
 * @brief 4-point pose and focal length solver
 * @remarks Bujnak, M. Kukelova, Z., Padjla, T., "A General Solution to the P4P Problem for Camera with Unknown Focal Length, " CVPR 2008
 * @remarks The solver assumes
 * 	- 0 skew, unity aspect ratio, and a principal point at the origin of the image coordinate system. Therefore, normalise the image coordinates with the inverse of a pseudo-intrinsic calibration matrix, with f=1
 * 	- that the points satisfy the cheirality constraint
 * @remarks Distance measure: 1-|<u,v>|/(<u,u> <v,v>)
 * @remarks Minimal parameterisation: Focal length; rotation vector; camera centre
 * @warning For the normalisation of the coordinate magnitudes (i.e. following the normalisation with the pseudo-intrinsics), avoid non-isotropic scaling!
 * @ingroup GeometrySolver
 */
class P4PSolverC : public GeometrySolverBaseC<P4PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 4, false, 10, 12, 7>
{
	private:

		typedef GeometrySolverBaseC<P4PSolverC, CameraMatrixT, TransferErrorH32DT, 3, 2, 4, false, 10, 12, 7> BaseT;	///< Type of the geometry solver base

		typedef typename BaseT::model_type ModelT;	///<  CameraMatrixT which contains the rotation
		typedef MultivariateGaussianC<P4PMinimalDifferenceC, BaseT::nDOF, RealT> GaussianT;	///< Type of the Gaussian for the sample statistics

		typedef BaseT::real_type RealT;	///< Floating point type
		typedef array<RealT,4> SolutionT;	///< A solution to the polynomial equation system

		/** @name Implementation details */ //@{

		typedef Matrix<double,10,10> ActionMatrixT;	///< An action matrix
		static optional<ActionMatrixT> BuildActionMatrix(const array<RealT,6>& edgeLengths, const Coordinate2DT& x1, const Coordinate2DT& x2, const Coordinate2DT& x3, const Coordinate2DT& x4);		///< Builds the action matrix
		static list<SolutionT> RunP4PCore(const array<RealT,6>& edgeLengths, const Coordinate2DT& x1, const Coordinate2DT& x2, const Coordinate2DT& x3, const Coordinate2DT& x4);	///< Estimates the focal length and the distances from the camera centre
		template<class CorrespondenceRangeT> static array<Coordinate3DT,4> ComputeLocalCoordinates(const SolutionT& solution, const CorrespondenceRangeT& correspondences, const array<RealT,6>& edgeLengths);	///< Computes the local coordinates corresponding to a solution
		//@}


	public:

		/** @name GeometrySolverConceptC interface */ //@{
		template<class CorrespondenceRangeT> list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const;	///< Computes the poses and focal lengths that best explain the correspondence set

		static double Cost(unsigned int dummy=sGenerator);	///< Computational cost of the solver

		//Minimal models
		typedef tuple<RealT, Coordinate3DT, RotationVectorT> minimal_model_type;	///< A minimally parameterised model. [Focal length; Coordinate vector; Axis-angle vector]
		template<class DummyRangeT=int> static minimal_model_type MakeMinimal(const ModelT& model, const DummyRangeT& dummy=DummyRangeT());	///< Converts a model to the minimal form
		static ModelT MakeModelFromMinimal(const minimal_model_type& minimalModel);	///< Minimal form to matrix conversion

		//Sample statistics
		typedef GaussianT uncertainty_type;	///< Type of the uncertainty of the estimate
		template<class SampleRangeT, class WeightRangeT, class DummyRangeT=int> static GaussianT ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy=DummyRangeT());	///< Computes the sample statistics for a set of pose and focal length estimates

		//@}

		/** @name Overrides */ //@{
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool dummy=true);	///< Converts a vector to a model
		//@}

		/**@name Minimal representation */ //@{
		static constexpr unsigned int iFocalLength=0;	///< Index of the focal length component
		static constexpr unsigned int iPosition=1;	///< Index of the position component
		static constexpr unsigned int iOrientation=2;	///< Index of the orientation component
		//@}
};	//class P4PSolverC

/**
 * @brief Difference functor for 2 minimally represented solutions
 * @ingroup Algorithm
 */
struct P4PMinimalDifferenceC
{
	typedef P4PSolverC::minimal_model_type first_argument_type;
	typedef P4PSolverC::minimal_model_type second_argument_type;
	typedef Matrix<P4PSolverC::real_type, P4PSolverC::DoF(), 1> result_type;	///< Difference in vector form

	result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between two minimally-represented P4P solutions
};	//struct P4PMinimalDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Computes the local coordinates corresponding to a solution
 * @param[in] solution Solution vector
 * @param[in] correspondences Correspondences
 * @param[in] edgeLengths E
 * @return 3D coordinates in the camera reference frame
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap, where the left element is of type \c Coordinate3DT , and the right, \c Coordinate2DT
 * @pre \c correspondences holds 4 elements
 */
template<class CorrespondenceRangeT>
array<Coordinate3DT,4> P4PSolverC::ComputeLocalCoordinates(const SolutionT& solution, const CorrespondenceRangeT& correspondences, const array<RealT,6>& edgeLengths)
{
	//Precondition
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "P4PSolverC::operator() : CorrespondenceRangeT::left_key_type must be Coordinate3DT");
	static_assert(is_same<Coordinate2DT, typename CorrespondenceRangeT::right_key_type>::value, "P4PSolverC::operator() : CorrespondenceRangeT::right_key_type must be Coordinate2DT");
	assert(boost::distance(correspondences)==4);

	auto it = boost::const_begin(correspondences);
	auto itE= boost::const_end(correspondences);

	array<Coordinate3DT,4> output;
	output[0]=Coordinate3DT(it->right[0], it->right[1], solution[0]);
	advance(it,1);

	for(size_t c=1; c<4; ++c, advance(it,1))
		output[c]=solution[c]*Coordinate3DT(it->right[0], it->right[1], solution[0]);

	//Recover the overall scale
	size_t index=0;
	RealT scaleAcc=0;
	for(auto it1=boost::const_begin(correspondences); it1!=itE; advance(it1,1))
		for(auto it2=next(it1,1); it2!=itE; advance(it2,1))
		{
			scaleAcc += sqrt(edgeLengths[index])/(it1->left - it2->left).norm();
			++index;
		}	//for(auto it2=next(it1,1); it2!=itE; advance(it2,1))

	double scale=scaleAcc/6;
	for(size_t c=0; c<4; ++c)
		output[c]*=scale;

	return output;
}	//array<Coordinate3DT,4> ComputeLocalCoordinates(const SolutionT& solution, const CorrespondenceRangeT& correspondences)

/**
 * @brief Converts a model to the minimal form
 * @param[in] model Model to be converted
 * @param[in] dummy Dummy parameter for satisfying the concept requirements
 * @pre \c model is a camera matrix (unenforced)
 * @return Minimal form
 */
template<class DummyRangeT>
auto P4PSolverC::MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type
{
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	IntrinsicCalibrationMatrixT mK;
	tie(mK, vC, mR)=DecomposeCamera(model, false);

	return minimal_model_type(mK(0,0), vC, RotationMatrixToRotationVector(mR));
}	//auto MakeMinimal(const ModelT& model, const DummyRangeT& dummy) -> minimal_model_type

/**
 * @brief Computes the sample statistics for a set of pose and focal length estimates
 * @tparam SampleRangeT A range of camera matrices
 * @tparam WeightRangeT A range weight values
 * @tparam DummyRangeT A dummy range
 * @param[in] samples Sample set
 * @param[in] wMean Sample weights for the computation of mean
 * @param[in] wCovariance Sample weights for the computation of the covariance
 * @param[in] dummy A dummy parameter for concept compliance
 * @return Gaussian for the sample statistics
 * @pre \c SampleRangeT is a forward range of elements of type \c CameraMatrixT
 * @pre \c WeightRangeT is a forward range of floating point values
 * @pre \c samples should have the same number of elements as \c wMean and \c wCovariance
 * @pre The sum of elements of \c wMean should be 1 (unenforced)
 * @pre The sum of elements of \c wCovariance should be 1 (unenforced)
 * @warning For SUT, \c wCovariance does not add up to 1
 * @warning Unless \c samples has 7 distinct elements, the resulting covariance matrix is rank-deficient
 */
template<class SampleRangeT, class WeightRangeT, class DummyRangeT>
auto P4PSolverC::ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<SampleRangeT>));
	static_assert(is_same<ModelT, typename range_value<SampleRangeT>::type >::value, "P4PSolverC::ComputeSampleStatistics : SampleRangeT must hold elements of type CameraMatrixT");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<WeightRangeT>));
	static_assert(is_floating_point<typename range_value<WeightRangeT>::type>::value, "P4PSolverC::ComputeSampleStatistics: WeightRangeT must be a range of floating point values." );

	assert(boost::distance(samples)==boost::distance(wMean));
	assert(boost::distance(samples)==boost::distance(wCovariance));

	size_t nSamples=boost::distance(samples);

	//Decompose the model
	vector<QuaternionT> qList; qList.reserve(nSamples);
	Coordinate3DT meanPosition=Coordinate3DT::Zero();
	RealT meanFocal=0;
	vector<minimal_model_type> minimalList; minimalList.reserve(nSamples);
	for(const auto& current : MakeBinaryZipRange(samples, wMean))
	{
		minimalList.push_back(MakeMinimal(boost::get<0>(current)));
		auto it=minimalList.rbegin();

		qList.push_back(RotationVectorToQuaternion(get<iOrientation>(*it)));
		meanPosition+= boost::get<1>(current)*(get<iPosition>(*it));
		meanFocal+=boost::get<1>(current)*get<iFocalLength>(*it);
	}	//for(const auto& current : samples)

	//Mean
	QuaternionT qMean;
	Matrix<RealT, 3, 3> mCovQ;
	tie(qMean, mCovQ)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	minimal_model_type mean=make_tuple(meanFocal, meanPosition, QuaternionToRotationVector(qMean));

	//Covariance
	P4PMinimalDifferenceC subtractor;
	typename GaussianT::covariance_type mCov; mCov.setZero();
	for(const auto& current : MakeBinaryZipRange(minimalList, wCovariance))
	{
		typename P4PMinimalDifferenceC::result_type vDif=subtractor(boost::get<0>(current), mean);
		mCov+=boost::get<1>(current) * (vDif * vDif.transpose());
	}	//for(const auto& current : MakeBinaryZipRange(samples, wCovariance))

	return GaussianT(mean, mCov);
}	//auto ComputeSampleStatistics(const SampleRangeT& samples, const WeightRangeT& wMean, const WeightRangeT& wCovariance, const DummyRangeT& dummy) -> GaussianT

/**
 * @brief Computes the poses and focal lengths that best explain the correspondence set
 * @param[in] correspondences Normalised correspondence set
 * @return A list of up to 10 camera matrices
 * @pre \c CorrespondenceRangeT is a forward range
 * @pre \c CorrespondenceRangeT is a bimap, where the left element is of type \c Coordinate3DT , and the right, \c Coordinate2DT
 */
template<class CorrespondenceRangeT>
auto P4PSolverC::operator()(const CorrespondenceRangeT& correspondences) const -> list<ModelT>
{
	//Precondition
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));
	static_assert(is_same<Coordinate3DT, typename CorrespondenceRangeT::left_key_type>::value, "P4PSolverC::operator() : CorrespondenceRangeT::left_key_type must be Coordinate3DT");
	static_assert(is_same<Coordinate2DT, typename CorrespondenceRangeT::right_key_type>::value, "P4PSolverC::operator() : CorrespondenceRangeT::right_key_type must be Coordinate2DT");

	list<ModelT> output;
	if(boost::distance(correspondences)!=sGenerator)
		return output;

	//Compute the inter-point distances
	array<RealT,6> edgeLengths;
	RealT degeneracyTest=1;
	unsigned int index=0;
	auto itE=boost::const_end(correspondences);
	for(auto it1=boost::const_begin(correspondences); it1!=itE; advance(it1,1))
		for(auto it2=next(it1,1); it2!=itE; advance(it2,1))
		{
			edgeLengths[index]=(it1->left - it2->left).squaredNorm();
			degeneracyTest*=edgeLengths[index];
			++index;
		}	//for(auto it2=next(it,1); it2!=itE; advance(it2,1))

	if(degeneracyTest<=numeric_limits<RealT>::epsilon())
		return output;

	//P4P core solver
	auto it2D=boost::const_begin(correspondences);
	list<SolutionT> solutions=RunP4PCore(edgeLengths, it2D->right, next(it2D,1)->right, next(it2D,2)->right, next(it2D,3)->right);

	//Compute the calibration estimates
	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity();
	Coordinate3DT position;
	RotationMatrix3DT orientation;

	Matrix<RealT, 3, 4> world;
	Matrix<RealT, 3, 4> local;

	for(const auto& current : solutions)
	{
		//Scene points in the camera reference frame
		array<Coordinate3DT,4> local3D=ComputeLocalCoordinates(current, correspondences, edgeLengths);

		auto it=correspondences.begin();
		for(size_t i=0; i<4; ++i, advance(it,1))
		{
			local.col(i)=local3D[i];
			world.col(i)=it->left;
		}	//for(size_t i=0; i<4; ++i, advance(it,1))

		Homography3DT mH=umeyama(world, local, true);
		RealT scale = mH.block(0,0,3,3).norm()/sqrt(3);
		RotationMatrix3DT orientation = (mH.block(0,0,3,3))/scale;
		Coordinate3DT position = -(orientation.transpose()) * (mH.col(3).segment(0,3)/scale);

/*		//Solve for the similarity transformation between the world and the camera reference frames
		CoordinateCorrespondenceList3DT tmp;
		for(const auto& current2 : MakeBinaryZipRange(correspondences, local3D))
			tmp.push_back(typename CoordinateCorrespondenceList3DT::value_type( boost::get<0>(current2).left, boost::get<1>(current2)));

		Similarity3DSolverC similaritySolver;
		list<typename Similarity3DSolverC::model_type> hList=similaritySolver(tmp);

		if(hList.empty())
			continue;

		//Compose the camera matrix

		RealT scale;
		tie(scale, position, orientation)=DecomposeSimilarity3D(*hList.begin());	//Ignore the scale*/
		mK(0,0)=current[0]; mK(1,1)=current[0];

		output.push_back(ComposeCameraMatrix(mK, position, orientation));
	}	//for(const auto& current : solutions)

	return output;
}	//list<ModelT> operator()(const CorrespondenceRangeT& correspondences) const

}	//GeometryN
}	//SeeSawN

#endif /* P4PSOLVER_IPP_ */
