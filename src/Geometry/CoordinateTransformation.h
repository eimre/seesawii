/**
 * @file CoordinateTransformation.h Public interface for CoordinateTransformationsC
 * @author Evren Imre
 * @date 24 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COORDINATE_TRANSFORMATION_H_3213256
#define COORDINATE_TRANSFORMATION_H_3213256

#include "CoordinateTransformation.ipp"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace GeometryN
{

template<typename ValueT, unsigned int DIM> class CoordinateTransformationsC;   ///< Coordinate transformations

/** @ingroup Elements */ //@{
template<class CoordinateT> using CoordinateTransformationsT=CoordinateTransformationsC<typename ValueTypeM<CoordinateT>::type, RowsM<CoordinateT>::value>;	///< Coordinate transformations from a coordinate
//@}


/********** EXTERN TEMPLATES **********/

using SeeSawN::ElementsN::Coordinate2DT;
typedef CoordinateTransformationsC<typename ValueTypeM<Coordinate2DT>::type, 2> CoordinateTransformations2DT;
extern template class CoordinateTransformationsC<typename ValueTypeM<Coordinate2DT>::type, 2>;

using SeeSawN::ElementsN::Coordinate3DT;
typedef CoordinateTransformationsC<typename ValueTypeM<Coordinate3DT>::type, 3> CoordinateTransformations3DT;
extern template class CoordinateTransformationsC<typename ValueTypeM<Coordinate3DT>::type, 3>;

extern template typename CoordinateTransformations2DT::AffineTransformT CoordinateTransformations2DT::ComputeNormaliser(const vector<Coordinate2DT>&, bool);
extern template typename CoordinateTransformations3DT::AffineTransformT CoordinateTransformations3DT::ComputeNormaliser(const vector<Coordinate3DT>&, bool);

using SeeSawN::ElementsN::HCoordinate2DT;
extern template vector<HCoordinate2DT> CoordinateTransformations2DT::ApplyProjectiveTransformation(const vector<Coordinate2DT>&, const typename CoordinateTransformations2DT::ProjectiveTransformT&);
extern template vector<Coordinate2DT> CoordinateTransformations2DT::ApplyAffineTransformation(const vector<Coordinate2DT>&, const typename CoordinateTransformations2DT::AffineTransformT&);
extern template vector<Coordinate2DT> CoordinateTransformations2DT::NormaliseHomogeneous(const vector<HCoordinate2DT>&);

extern template vector<vector<unsigned int> > CoordinateTransformations2DT::Bucketise(const vector<Coordinate2DT>&, const typename CoordinateTransformations2DT::ArrayD&, const typename CoordinateTransformations2DT::ArrayD2&, bool);
extern template vector<vector<unsigned int> > CoordinateTransformations3DT::Bucketise(const vector<Coordinate3DT>&, const typename CoordinateTransformations3DT::ArrayD&, const typename CoordinateTransformations3DT::ArrayD2&, bool);

}   //GeometryN
}   //SeeSawN

#endif /* COORDINATE_TRANSFORMATION_H_3213256 */
