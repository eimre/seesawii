/**
 * @file P2PSolver.h Public interface for the 2-point camera orientation solver
 * @author Evren Imre
 * @date 7 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2P_SOLVER_H_7093123
#define P2P_SOLVER_H_7093123

#include "P2PSolver.ipp"

namespace SeeSawN
{
namespace GeometryN
{
class P2PSolverC;	///< 2-point orientation solver
}	//GeometryN
}	//SeeSawN

#endif /* P2PSOLVER_H_ */
