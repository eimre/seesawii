/**
 * @file LensDistortion.h Public interface for various lens distortion models
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LENS_DISTORTION_H_2136569
#define LENS_DISTORTION_H_2136569

#include "LensDistortion.ipp"
namespace SeeSawN
{
namespace GeometryN
{

class LensDistortionNoDC;	///< No lens distortion
class LensDistortionFP1C;    ///< First-order full-polynomial lens distortion model
class LensDistortionOP1C;    ///< First-order odd-polynomial lens distortion model
class LensDistortionDivC;   ///< Division lens distortion model

template<class CoordinateRangeT> vector<optional<Coordinate2DT> > ApplyDistortion(const CoordinateRangeT& points, LensDistortionCodeT distortionType, const Coordinate2DT& centre, double kappa);	///< Applies a specified lens distortion to a set of points
template<class CoordinateRangeT> vector<optional<Coordinate2DT> > ApplyDistortionCorrection(const CoordinateRangeT& points, LensDistortionCodeT distortionType, const Coordinate2DT& centre, double kappa);	///< Applies a specified lens distortion correction to a set of points


/********** EXTERN TEMPLATES **********/
extern template vector<optional<Coordinate2DT> > ApplyDistortion(const vector<Coordinate2DT>&, LensDistortionCodeT, const Coordinate2DT&, double);
extern template vector<optional<Coordinate2DT> > ApplyDistortionCorrection(const vector<Coordinate2DT>&, LensDistortionCodeT, const Coordinate2DT&, double);

}   //GeometryN
}	//SeeSawN


/********** EXTERN TEMPLATES **********/
extern template class std::array<double,2>;
#endif /* LENS_DISTORTION_H_2136569 */
