/**
 * @file DualAbsoluteQuadricSolver.cpp Implementation of dual absolute quadric solver
 * @author Evren Imre
 * @date 4 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "DualAbsoluteQuadricSolver.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Constructor
 * @param[in] nnVWIterations Number of variable-weight iterations. 50 is suggested in the reference
 * @pre \c nnVWIterations>0
 */
DualAbsoluteQuadricSolverC::DualAbsoluteQuadricSolverC(unsigned int nnVWIterations) : nVWIterations(nnVWIterations)
{
	assert(nnVWIterations>0);
}	//DualAbsoluteQuadricSolverC(unsigned int nnVWIterations)

/**
 * @brief Makes an equation
 * @param mP Camera matrix
 * @param i1 Row index for the left side
 * @param i2 Row index for the right side
 * @return An equation
 */
auto DualAbsoluteQuadricSolverC::MakeEquation(const CameraMatrixT& mP, size_t i1, size_t i2) -> EquationT
{
	Matrix4d mCoeff=mP.row(i1).transpose()*mP.row(i2);

	EquationT output;
	output(0) = mCoeff(0,0);
	output(1) = mCoeff(0,1) + mCoeff(1,0);
	output(2) = mCoeff(0,2) + mCoeff(2,0);
	output(3) = mCoeff(0,3) + mCoeff(3,0);
	output(4) = mCoeff(1,1);
	output(5) = mCoeff(1,2) + mCoeff(2,1);
	output(6) = mCoeff(1,3) + mCoeff(3,1);
	output(7) = mCoeff(2,2);
	output(8) = mCoeff(2,3) + mCoeff(3,2);
	output(9) = mCoeff(3,3);

	return output;
}	//Matrix<double,1,10> MakeEquation(const CameraMatrixT& mP, size_t i1, size_t i2, double w)

/**
 * @brief Builds the equation system
 * @param[in] mPnList Camera matrices
 * @return System matrix
 */
auto DualAbsoluteQuadricSolverC::MakeEquationSystem(const vector<CameraMatrixT>& mPnList) -> SystemMatrixT
{
	size_t nCameras=mPnList.size();
	SystemMatrixT mA((nCameras-1)*6, 10);

	EquationT v00;
	EquationT v11;
	EquationT v22;

	size_t index=0;
	for(size_t c=1; c<nCameras; ++c, index+=6)
	{
		const CameraMatrixT& mP=mPnList[c];
		mA.row(index)  =100*MakeEquation(mP, 0, 1);	//Skewness. Eq 14
		mA.row(index+1)=10 *MakeEquation(mP, 0, 2);	//Principal point. Eq 15
		mA.row(index+2)=10 *MakeEquation(mP, 1, 2);	//Principal point.Eq 16

		v00=MakeEquation(mP, 0, 0);
		v11=MakeEquation(mP, 1, 1);
		v22=MakeEquation(mP, 2, 2);

		mA.row(index+3)=5 *(v00-v11);	//Aspect ratio. Eq 17
		mA.row(index+4)=(v00-v22);	//Focal length. Eq 18
		mA.row(index+5)=(v11-v22);	//Focal length. Eq 19
	}	//for(size_t c=0; c<nCameras; ++c)

	return mA;
}	//MatrixXd MakeEquationSystem(const vector<CameraMatrixT>& mPnList)

/**
 * @brief Converts the solution vector to an absolute dual quadric
 * @param[in] q Absolute quadric estimated via DLT
 * @return Absolute quadric
 */
Quadric3DT DualAbsoluteQuadricSolverC::MakeQ(const VectorQT& q)
{
	//q -> Q
	Matrix4d mQ;
	mQ(0,0)=q[0]; mQ(0,1)=mQ(1,0)=q[1]; mQ(0,2)=mQ(2,0)=q[2]; mQ(0,3)=mQ(3,0)=q[3];
	mQ(1,1)=q[4]; mQ(1,2)=mQ(2,1)=q[5]; mQ(1,3)=mQ(3,1)=q[6];
	mQ(2,2)=q[7]; mQ(2,3)=mQ(3,2)=q[8];
	mQ(3,3)=q[9];

	//Make rank 3 and renormalise
	Matrix4d mQ3;
	unsigned int rankQ;
	tie(rankQ, mQ3)=MakeRankN<Eigen::FullPivHouseholderQRPreconditioner>(mQ, 3);
	assert(rankQ==3);

	//Symmetrise
	mQ3 =(mQ3 + mQ3.transpose())*0.5;

	//For a positive definite matrix, trace is positive
	if(mQ3.trace()<0)
		mQ3*=-1;

	return mQ3;
}	//Matrix4d MakeQ(const VectorQT& q)

/**
 * @brief Evaluates the cost for an intrinsic calibration matrix
 * @param[in] mQ Intrinsic calibration matrix
 * @param[in] mP Camera matrix
 * @return Error value
 */
double DualAbsoluteQuadricSolverC::Evaluate(const Quadric3DT& mQ, const CameraMatrixT& mP)
{
	optional<IntrinsicCalibrationMatrixT> mK=IntrinsicsFromDAQ(mQ, mP);

	//Eq 24. If mP or mQ is broken, invalid
	return mK ? (pow<2>((*mK)(0,1)) + pow<2>((*mK)(0,2)) + pow<2>((*mK)(1,2)) + pow<2>((*mK)(1,1)/(*mK)(0,0)-1) )/pow<2>((*mK)(0,0)) : numeric_limits<double>::infinity();
}	//double Evaluate(const Matrix4d& mQ, const CameraMatrixT& mP)

/********** EXPLICIT INSTANTIATIONS **********/
template list<Homography3DT> DualAbsoluteQuadricSolverC::operator()(const vector<CameraMatrixT>&) const;
}	//GeometryN
}	//SeeSawN

