/**
 * @file GeometrySolverConcept.ipp Implementation of GeometrySolverConceptC
 * @author Evren Imre
 * @date 15 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_SOLVER_CONCEPT_IPP_5712232
#define GEOMETRY_SOLVER_CONCEPT_IPP_5712232

#include <boost/concept_check.hpp>
#include <Eigen/Dense>
#include <set>
#include <cstddef>
#include <list>
#include <vector>
#include <type_traits>
#include "../Elements/Correspondence.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::CopyConstructible;
using boost::Assignable;
using Eigen::Matrix;
using std::set;
using std::list;
using std::vector;
using std::size_t;
using std::is_floating_point;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ColsM;

/**
 * @brief Enforces the geometry solver concept
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class GeometrySolverConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

	private:

	public:

		BOOST_CONCEPT_USAGE(GeometrySolverConceptC)
		{
			typedef typename TestT::coordinate_type1 Coordinate1T;
			static_assert(RowsM<Coordinate1T>::value!=Eigen::Dynamic, "GeometrySolverConceptC : Coordinate1T must be a fixed size vector.");
			static_assert(ColsM<Coordinate1T>::value==1, "GeometrySolverConceptC : Coordinate1T must be a fixed size vector.");

			typedef typename TestT::coordinate_type2 Coordinate2T;
			static_assert(RowsM<Coordinate2T>::value!=Eigen::Dynamic, "GeometrySolverConceptC : Coordinate2T must be a fixed size vector.");
			static_assert(ColsM<Coordinate2T>::value==1, "GeometrySolverConceptC : Coordinate2T must be a fixed size vector.");

			typedef typename TestT::model_type ModelT;

			typedef typename TestT::real_type RealT;
			static_assert(is_floating_point<RealT>::value, "GeometrySolverConceptC : RealT must be a floating point type.");

			RealT r; (void)r;

			TestT tested;
			constexpr unsigned int sGenerator=TestT::GeneratorSize(); (void)sGenerator;
			constexpr unsigned int maxSolution=TestT::MaxSolution();	(void)maxSolution;
			constexpr bool flagNonminimal=TestT::IsNonminimal(); (void)flagNonminimal;
			constexpr unsigned int nDoF=TestT::DoF(); (void)nDoF;

			set<unsigned int> generator;
			bool flagValidGenerator=tested.ValidateGenerator(generator); (void)flagValidGenerator;

			typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CorrespondenceContainerT;
			CorrespondenceContainerT correspondences;
			list<ModelT> models=tested(correspondences);

			typename TestT::distance_type modelDistance=tested.ComputeDistance(*models.begin(), *models.rbegin()); (void)modelDistance;

			Matrix<typename ValueTypeM<ModelT>::type, Eigen::Dynamic, 1> vModel=tested.MakeVector(*models.begin());
			ModelT newModel=tested.MakeModel(vModel); (void)newModel;

			bool flagN;
			ModelT newModeln=tested.MakeModel(vModel, flagN); (void)newModeln;

			typedef typename TestT::minimal_model_type MinimalT;
			MinimalT minimal=tested.MakeMinimal(*models.begin(), correspondences);

			ModelT newModel2=tested.MakeModelFromMinimal(minimal); (void)newModel2;

			typedef typename TestT::uncertainty_type UncertaintyT;
			UncertaintyT uncertainty=tested.ComputeSampleStatistics(models, vector<RealT>(), vector<RealT>(), correspondences); (void)uncertainty;

			typedef typename TestT::error_type ErrorT;
			ErrorT error;

			typedef typename TestT::constraint_type ConstraintT;
			ConstraintT constraint; (void)constraint;

			typedef typename TestT::loss_type LossT;
			LossT lossMap;

			double loss=TestT::EvaluateModel(ModelT(), correspondences, error, lossMap); (void)loss;

			double cost=tested.Cost(); (void)cost;
		}	//BOOST_CONCEPT_USAGE(GeometrySolverConceptC)

	///@endcond
};	//class GeometrySolverConceptC

}	//GeometryN
}	//SeeSawN

#endif /* GEOMETRY_SOLVER_CONCEPT_IPP_5712232 */
