/**
 * @file GeometrySolverBase.ipp Implementation of GeometrySolverBaseC
 * @author Evren Imre
 * @date 10 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_SOLVER_BASE_IPP_0802312
#define GEOMETRY_SOLVER_BASE_IPP_0802312

#include <Eigen/Dense>
#include <type_traits>
#include <cmath>
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/RobustLossFunction.h"
#include "../Metrics/GeometricErrorConcept.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace GeometryN
{

using Eigen::Matrix;
using std::is_same;
using std::fabs;
using SeeSawN::MetricsN::GeometricConstraintC;
using SeeSawN::MetricsN::PseudoHuberLossC;
using SeeSawN::MetricsN::GeometricErrorConceptC;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::ElementsN::CoordinateVectorT;

/**
 * @brief Base class for geometry solvers
 * @tparam DerivedT Derived type
 * @tparam ModelT Type of the model computed by the solver
 * @tparam ErrorT Geometric error associated with the solver
 * @tparam DIM1 Dimensionality of the first domain
 * @tparam DIM2 Dimesionality of the second domain
 * @tparam GENERATOR_SIZE Minimum number of correspondences required to instantiate a model
 * @tparam FLAG_NONMINIMAL \c true if the solver can handle nonminimal generator sets
 * @tparam NSOLUTION Maximum number of solution a generator can return
 * @tparam NPARAMETER Number of parameters of the model
 * @tparam NDOF Dimensionality of the model
 * @pre \c ErrorT is a model of \c GeometricErrorConceptC
 * @remarks Common services for geometry solvers
 * @ingroup GeometrySolver
 * @nosubgrouping
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
class GeometrySolverBaseC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometricErrorConceptC<ErrorT>));
	///@endcond

	protected:

		/** @name Configuration */ //@{
		static constexpr unsigned int sGenerator=GENERATOR_SIZE;	///< Cardinality of the minimal generator set
		static constexpr bool flagNonminimal=FLAG_NONMINIMAL;	///< \c true if the solver can handle nonminimal generator sets as well
		static constexpr unsigned int nSolution=NSOLUTION;	///< Maximum number of solutions per generator
		static constexpr unsigned int nParameter=NPARAMETER; ///< Number of parameters of the model
		static constexpr unsigned int nDOF=NDOF;	///< Dimensionality of the model. If nDOF<nParameter, redundant parameterisation
		//@}

		typedef typename ErrorT::result_type RealT;	///< Floating point type
		typedef Matrix<RealT,Eigen::Dynamic,1> VectorT;

	public:

		/** @name GeometrySolverConceptC interface */ //@{
		typedef CoordinateVectorT<RealT, DIM1> coordinate_type1;	///< Type of the first coordinate
		typedef CoordinateVectorT<RealT, DIM2> coordinate_type2;	///< Type of the second coordinate
		typedef ModelT model_type;	///< Type of the estimated geometric model
		typedef RealT real_type;	///< Floating point type

		typedef ErrorT error_type;	///< Type of the geometric error for evaluating a model
		typedef GeometricConstraintC<ErrorT> constraint_type;	///< Type of the geometric constraint for evaluating a model
		typedef PseudoHuberLossC<typename ErrorT::result_type> loss_type;	///< Robust error function type

		static constexpr unsigned int GeneratorSize();	///< Returns the generator size
		static constexpr bool IsNonminimal();	///< Checks whether the solver can handle nonminimal generators
		static constexpr unsigned int MaxSolution();	///< Returns the maximum number of solutions
		static constexpr unsigned int DoF();	///< Returns the number of degrees-of-freedom for the model

		template<class CorrespondenceRangeT> static bool ValidateGenerator(const CorrespondenceRangeT& generator);	///< Default validator, always \c true
		template<class CorrespondenceRangeT> static typename loss_type::result_type EvaluateModel(const ModelT& model, const CorrespondenceRangeT& validators, const ErrorT& errorMetric, const loss_type& lossFunction);	///< Evaluates the model
		template<class CorrespondenceRangeT> static typename ErrorT::result_type EvaluateModel(const ModelT& model, const CorrespondenceRangeT& validators, const ErrorT& errorMetric);	///< Evaluates the model

		//Model manipulation
		typedef RealT distance_type;	///< Type of the distance between two models
		static distance_type ComputeDistance(const ModelT& model1, const ModelT& model2);	///< Computes the distance between two models
		static VectorT MakeVector(const ModelT& model);	///< Converts a model to a vector
		static ModelT MakeModel(const VectorT& vModel, bool flagNormalise=true);	///< Converts a vector to a model

		//@}
};	//class GeometrySolverBaseC

/**
 * @brief Returns the generator size
 * @return Returns \c sGenerator
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
constexpr unsigned int GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::GeneratorSize()
{
	return sGenerator;
}	//unsigned int GeneratorSize()

/**
 * @brief Checks whether the solver can handle nonminimal generators
 * @return Returns \c flagNonminimal
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
constexpr bool GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::IsNonminimal()
{
	return flagNonminimal;
}	//unsigned int GeneratorSize()

/**
 * @brief Returns the maximum number of solutions
 * @return Returns \c nSolution
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
constexpr unsigned int GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::MaxSolution()
{
	return nSolution;
}	//unsigned int GeneratorSize()

/**
 * @brief Returns the number of degrees-of-freedom for the model
 * @return \c nDoF
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
constexpr unsigned int GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::DoF()
{
	return nDOF;
}	//unsigned int DoF()

/**
 * @brief Default validator, always \c true
 * @tparam CorrespondenceRangeT A correspondence range
 * @param[in] generator Generator
 * @return \c true
 * @remarks Unless the derived solver has a dedicated test, automatic success
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
template<class CorrespondenceRangeT>
bool GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::ValidateGenerator(const CorrespondenceRangeT& generator)
{
	return true;
}	//bool ValidateGenerator(const CorrespondenceRangeT& generator)

/**
 * @brief Evaluates the model
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] model Model to be evaluated
 * @param[in] validators Validation set
 * @param[in] errorMetric Error metric
 * @param[in] lossFunction Error-to-loss converter
 * @pre \c CorrespondenceRangeT is a bimap (unenforced)
 * @pre \c CorrespondenceRangeT holds the appropriate coordinate types
 * @return Total loss
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
template<class CorrespondenceRangeT>
auto GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::EvaluateModel(const ModelT& model, const CorrespondenceRangeT& validators, const ErrorT& errorMetric, const loss_type& lossFunction) -> typename loss_type::result_type
{
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, coordinate_type1>::value, "GeometrySolverBase::EvaluateModel : Wrong left member type.");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, coordinate_type2>::value, "GeometrySolverBase::EvaluateModel : Wrong right member type.");

	typename loss_type::result_type loss=0;
	for(const auto& current : validators)
		loss+=lossFunction(errorMetric(model, current.left, current.right));

	return loss;
}	//ErrorT::result_type EvaluateModel(const ModelT& model, const CorrespondenceRangeT& validators, const ErrorT& errorMetric, const loss_type& lossFunction)

/**
 * @brief Evaluates the model
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] model Model to be evaluated
 * @param[in] validators Validation set
 * @param[in] errorMetric Error metric
 * @pre \c CorrespondenceRangeT is a bimap (unenforced)
 * @pre \c CorrespondenceRangeT holds the appropriate coordinate types
 * @return Total loss
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
template<class CorrespondenceRangeT>
auto GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::EvaluateModel(const ModelT& model, const CorrespondenceRangeT& validators, const ErrorT& errorMetric) -> typename ErrorT::result_type
{
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, coordinate_type1>::value, "GeometrySolverBase::EvaluateModel : Wrong left member type.");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, coordinate_type2>::value, "GeometrySolverBase::EvaluateModel : Wrong right member type.");

	typename ErrorT::result_type error=0;
	for(const auto& current : validators)
		error+=errorMetric(model, current.left, current.right);

	return error;
}	//auto EvaluateModel(const ModelT& model, const CorrespondenceRangeT& validators, const ErrorT& errorMetric) -> typename ErrorT::result_type

/**
 * @brief Computes the distance between two models
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return 1-Absolute value of the dot-product between the unit vectors representing the models. [0,1]
 * @pre \c model1 and \c model2 have unit norm
 * @remarks abs, because model and -model are projectively identical
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
auto GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::ComputeDistance(const ModelT& model1, const ModelT& model2) -> distance_type
{
	//Preconditions
	assert( fabs(model1.norm()-1.0)<=numeric_limits<double>::epsilon());
	assert( fabs(model2.norm()-1.0)<=numeric_limits<double>::epsilon());

	return 1-fabs((model1.cwiseProduct(model2)).sum());
}	//distance_type ComputeSimilarity(const ModelT& model1, const ModelT& model2)

/**
 * @brief Converts a model to a vector
 * @param[in] model A model
 * @return Vector corresponding to the model
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
auto GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::MakeVector(const ModelT& model) -> VectorT
{
	return Reshape<VectorT>(model, nParameter, 1);
}	//VectorXd MakeVector(const ModelT& mH)

/**
 * @brief Converts a vector to a model
 * @param[in] vModel A vector
 * @param[in] flagNormalise If \c true , the model is normalised
 * @return Model corresponding to the vector
 * @remarks Default assumption: a matrix corresponding to a projective model
 */
template<class DerivedT, class ModelT, class ErrorT, unsigned int DIM1, unsigned int DIM2, unsigned int GENERATOR_SIZE, bool FLAG_NONMINIMAL, unsigned int NSOLUTION, unsigned int NPARAMETER, unsigned int NDOF>
auto GeometrySolverBaseC<DerivedT, ModelT, ErrorT, DIM1, DIM2, GENERATOR_SIZE, FLAG_NONMINIMAL, NSOLUTION, NPARAMETER, NDOF>::MakeModel(const VectorT& vModel, bool flagNormalise) -> ModelT
{
	return Reshape<ModelT>((flagNormalise ? vModel.normalized() : vModel), DIM2+1, DIM1+1);
}	//ModelT MakeModel(const VectorXd& vH)
}	//GeometryN
}	//SeeSawN

#endif /* GEOMETRY_SOLVER_BASE_IPP_0802312 */
