/**
 * @file Projection32.ipp Implementation of \c Projection32C
 * @author Evren Imre
 * @date 8 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PROJECTION32_IPP_2969831
#define PROJECTION32_IPP_2969831

#include <stdexcept>
#include <boost/optional.hpp>
#include <boost/concept_check.hpp>
#include "LensDistortionConcept.h"
#include "LensDistortion.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using std::invalid_argument;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::GeometryN::LensDistortionConceptC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;

/**
 * @brief 3D-2D projection operator
 * @ingroup Geometry
 * @nosubgrouping
 */
class Projection32C
{
	private:

		/** @name Configuration */ ///@{
		CameraMatrixT mP;	///< Camera matrix
		bool flagValid;	///< \c true if the object is valid
		///@}

	public:

		Projection32C();	///< Default constructor
		Projection32C(const CameraMatrixT& mmP);	///< Constructor

		Coordinate2DT operator()(const Coordinate3DT& p3D) const;	///< Projects a scene point onto the image plane of the camera

		template<class LensDistortionT> optional<Coordinate2DT> operator()(const Coordinate3DT& p3D, const LensDistortionT& distortion) const;	///< Projects a scene onto the image plane of a camera with a lens distortion.
		optional<Coordinate2DT> operator()(const Coordinate3DT& p3D, const LensDistortionC& distortion) const;	///< Projects a scene onto the image plane of a camera with a lens distortion.
};	//class Projection32C

/**
 * @brief Combined 3D-2D projection and lens distortion operator
 * @tparam LensDistortionT Lens distortion type
 * @pre \c LensDistortionT satisfies the lens distortion concept
 * @ingroup Geometry
 * @nosubgrouping
 */
template<class LensDistortionT>
class Projection32dC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((LensDistortionConceptC<LensDistortionT>));
	///@endcond

	private:

		/** @name Configuration */ //@{
		Projection32C projector;	///< 3D-2D projection operator
		LensDistortionT distortion;	///< Distortion operator
		bool flagValid;	///< \c true if the object is valid
		//@}

	public:

		Projection32dC();	///< Default constructor
		Projection32dC(const CameraMatrixT& mmP, const LensDistortionT& ddistortion);	///< Constructor

		optional<Coordinate2DT> operator()(const Coordinate3DT& p3D) const;	///< Projects a scene point onto the image plane of the camera
};	//class Projection32dC

/********** IMPLEMENTATION STARTS HERE **********/

/********** Projection32C **********/

/**
 * @brief Projects a scene onto the image plane of a camera with a lens distortion.
 * @tparam LensDistortionT Lens distortion type
 * @param[in] p3D Scene point
 * @param[in] distortion Lens distortion
 * @pre \c LensDistortionT satisfies the \c LensDistortionConceptC
 * @return Projection of \c p3D on the image plane. Might have infinite coordinates. Invalid if lens distortion fails
 */
template<class LensDistortionT>
optional<Coordinate2DT> Projection32C::operator()(const Coordinate3DT& p3D, const LensDistortionT& distortion) const
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((LensDistortionConceptC<LensDistortionT>));
	return distortion.Apply(this->operator()(p3D));
}	//optional<Coordinate2DT> Projection32C::operator()(const Coordinate3DT& p3D, const LensDistortionT& distortion) const


/********** Projection32dC **********/

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class LensDistortionT>
Projection32dC<LensDistortionT>::Projection32dC() : flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] mmP Camera matrix
 * @param[in] ddistortion Lens distortion
 * @post Valid object
 */
template<class LensDistortionT>
Projection32dC<LensDistortionT>::Projection32dC(const CameraMatrixT& mmP, const LensDistortionT& ddistortion) : projector(mmP), distortion(ddistortion), flagValid(true)
{}

/**
 * @brief Projects a scene point onto the image plane of the camera
 * @param[in] p3D Scene point
 * @return Projection of \c p3D on the image plane. Might have infinite coordinates. Invalid if lens distortion fails
 * @pre \c flagValid=true
 */
template<class LensDistortionT>
optional<Coordinate2DT> Projection32dC<LensDistortionT>::operator()(const Coordinate3DT& p3D) const
{
	assert(flagValid);
	return projector(p3D, distortion);
}	//Coordinate2DT Projection32dC<LensDistortionT>::operator()(const Coordinate3DT& p3D) const

}	//GeometryN
}	//SeeSawN

#endif /* PROJECTION32_IPP_2969831 */
