/**
 * @file PDLHomographyKRKDecompositionProblem.ipp Implementation details for the KRK decomposition problem
 * @author Evren Imre
 * @date 15 Jun 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef PDL_HOMOGRAPHY_KRK_DECOMPOSITION_PROBLEM_IPP_2770912
#define PDL_HOMOGRAPHY_KRK_DECOMPOSITION_PROBLEM_IPP_2770912

#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <type_traits>
#include <tuple>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Correspondence.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Metrics/GeometricError.h"
#include "../Geometry/Rotation.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using boost::math::pow;
using Eigen::MatrixXd;
using Eigen::Matrix3d;
using Eigen::Matrix;
using Eigen::VectorXd;
using std::true_type;
using std::false_type;
using std::tie;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::DecomposeK2RK1i;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::QuaternionToVector;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::MetricsN::SymmetricTransferErrorH2DT;
using SeeSawN::GeometryN::JacobianQuaternionToRotationMatrix;

/**
 * @brief Problem for optimal KRK decomposition of a homography
 * @remarks Decomposition of a homography into a two diagonal matrices and a rotation matrix. This is necessary for extracting the relative rotation and focal length parameters from a homography,
 * i.e. for a decomposition of type \f$ \mathbf{K_2 R K_1^{-1} } \f$
 * @remarks Problem: Find a decomposition that minimises the symmetric transfer error
 * @remarks The image points should be normalised so that the intrinsic calibration matrix is diagonal- i.e. the effects of the aspect ratio, skewness and the principal point should be removed
 * @warning A valid problem has 6 constraints
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLHomographyKRKDecompositionProblemC
{
	private:

		/** @name Configuration */ //@{
		static constexpr unsigned int nDoF=6;	///< Number of degrees of freedom of the model
		bool flagValid;	///< True if the object is initialised
		Homography2DT initialEstimate;	///< Initial estimate
		CoordinateCorrespondenceList2DT observations;	///< Constraints
		optional<MatrixXd> mW	;	///< Weight matrix
		//@}

		typedef ValueTypeM<Homography2DT>::type RealT;	///< Floating point type


	public:

		/** @name PowellDogLegProblemConcept interface */ //@{
		typedef Homography2DT result_type;
		typedef Homography2DT model_type;
		typedef CoordinateCorrespondenceList2DT observation_set_type;

		typedef false_type has_normal_solver;	///< Uses the facilities of the optimisation engine for the normal equations
		typedef false_type has_covariance_estimator;	///< Uses the facilities of the optimisation engine for the covariance
		typedef true_type has_nontrivial_update;	///< Has a dedicated update function

		PDLHomographyKRKDecompositionProblemC();	///< Default constructor

		void Configure(const Homography2DT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& mmW);	///< Configures the problem

		VectorXd InitialEstimate() const;	///< Returns the initial estimate vector
		const optional<MatrixXd>& ObservationWeights() const;	///< Returns a constant reference to \c mW

		VectorXd ComputeError(const VectorXd& vP) const;	///< Computes the error vector
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const;	///< Computes the Jacobian at the current solution

		static VectorXd Update(const VectorXd& vP, const VectorXd& vU);	///< Updates the current solution
		static RealT ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU);	///< Computes the model-space distance after an update
		static Homography2DT MakeResult(const VectorXd& vP);	///< Converts a parameter vector to a model

		bool IsValid() const;	///< Returns \c flagValid

		static void InitialiseIteration();	///< Dummy
		static void FinaliseIteration();	///< Dummy
		//@}

		PDLHomographyKRKDecompositionProblemC(const Homography2DT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& mmW);	///< Constructor

};	//class PDLHomographyKRKDecompositionProblemC

}	//OptimisationN
}	//SeeSawN



#endif /* PDL_HOMOGRAPHY_KRK_DECOMPOSITION_PROBLEM_IPP_2770912 */
