/**
 * @file PDLP2PProblem.cpp Implementation of the nonlinear optimisation problem for 2-point orientation estimation
 * @author Evren Imre
 * @date 7 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLP2PProblem.h"

namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLP2PProblemC::PDLP2PProblemC()
{}

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 * @pre \c vP is unit norm
 */
auto PDLP2PProblemC::MakeResult(const VectorXd& vP) const -> ModelT
{
	assert(BaseT::flagValid);
	assert(fabs(vP.norm()-1)<3*numeric_limits<RealT>::epsilon());

	ModelT output=ModelT::Zero();
	output.block(0,0,3,3)=QuaternionT(vP[0], vP[1], vP[2], vP[3]).matrix();
	return output;
}	//auto MakeResult(const VectorXd& vP, bool dummy) const -> ModelT

/**
 * @brief Returns the initial estimate in vector form
 * @return Rotation quaternion corresponding to the initial estimate, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLP2PProblemC::InitialEstimate() const
{
	assert(BaseT::flagValid);
	RotationMatrix3DT mR=initialEstimate.block(0,0,3,3);
	QuaternionT q(mR);
	return (q.w()<0 ? -1 : 1)*QuaternionToVector<double>(q);
}	//VectorXd PDLRotation3DEstimationProblemC::InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLP2PProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeError(MakeResult(vP));
}	//VectorXd ComputeError(const VectorXd& vP)

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLP2PProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vNew=(vP+vU).normalized();
	return (vNew[0]<0 ? -1 : 1) * vNew;
}	//VectorXd PDLRotation3DEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 */
double PDLP2PProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP), MakeResult(vPU));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLP2PProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//Jacobian wrt camera matrix
	optional<MatrixXd> mJP=BaseT::ComputeJacobian(MakeResult(vP), vP.size());

	optional<MatrixXd> output;
	if(!mJP)
		return output;

	//Camera matrix wrt rotation and focal length

	QuaternionT q(vP[0], vP[1], vP[2], vP[3]);
	Matrix<double, 9, 4> dRdq=JacobianQuaternionToRotationMatrix(q);

	vector<size_t> indexList{0,1,2,4,5,6,8,9,10};
	Matrix<double, 12, 9> dPdR=JacobiandAda<Matrix<double, 12, 9> >(12, indexList);
	Matrix<double, 12, 4> dPdq=dPdR*dRdq;

	output=(*mJP)*dPdq;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP)
}	//OptimisationN
}	//SeeSawN

