/**
 * @file PDLP4PProblem.h Public interface for the PDL problem for pose and focal length estimation
 * @author Evren Imre
 * @date 19 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_P4P_PROBLEM_H_0812817
#define PDL_P4P_PROBLEM_H_0812817

#include "PDLP4PProblem.ipp"
namespace SeeSawN
{
namespace OptimisationN
{

class PDLP4PProblemC;	///< Problem for Powell's dog leg optimisation algorithm, for pose and focal length estimation

}	//OptimisationN
}	//SeeSawN

#endif /* PDL_P4P_PROBLEM_H_0812817 */
