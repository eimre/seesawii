/**
 * @file PDLFrameAlignmentProblem.cpp Implementation of the Powell's dog leg problem for frame alignment
 * @author Evren Imre
 * @date 22 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLFrameAlignmentProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Shifts a coordinate
 * @param[in] x Point coordinates
 * @param[in] forward Forward translation vector
 * @param[in] backward Backward translation vector
 * @param magnitude Magnitude of shift
 * @return Shifted coordinates
 */
Coordinate2DT PDLFrameAlignmentProblemC::Shift(const Coordinate2DT& x, const Coordinate2DT& forward, const Coordinate2DT& backward, double magnitude) const
{
	return x+fabs(magnitude)*( magnitude>=0 ? forward : backward ) ;
}	//Coordinate2DT Shift(const Coordinate2DT& x, const Coordinate2DT& direction, double scale)

/**
 * @brief Default constructor
 * @post The object is invalid
 */
PDLFrameAlignmentProblemC::PDLFrameAlignmentProblemC() : flagValid(false)
{}

/**
 * @brief Returns the initial estimate in vector form
 * @return A vector corresponding to the initial estimate
 * @pre \c flagValid=true
 */
VectorXd PDLFrameAlignmentProblemC::InitialEstimate() const
{
	//Preconditions
	assert(flagValid);

	VectorXd output(2); output<<initialEstimate[iShift1], initialEstimate[iShift2];
	return output;
}	//VectorXd InitialEstimate()

/**
 * @brief Returns the observation weight matrix
 * @return A constant reference to \c observationWeights
 */
const optional<MatrixXd>& PDLFrameAlignmentProblemC::ObservationWeights() const
{
	return observationWeights;
}	//const optional<MatrixXd>& ObservationWeights()

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool PDLFrameAlignmentProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Dummy function to satisfy the concept requirements
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] oobservations Observation list
 * @param[in] oobservationWeights Observation weight matrix. Invalid if not specified
 */
void PDLFrameAlignmentProblemC::Configure(const ModelT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& oobservationWeights)
{}

/**
 * @brief Dummy function to satisfy the concept requirements
 */
void PDLFrameAlignmentProblemC::InitialiseIteration()
{}

/**
 * @brief Dummy function to satisfy the concept requirements
 */
void PDLFrameAlignmentProblemC::FinaliseIteration()
{}

/**
 * @brief Computes the error vector
 * @param[in] vP Current solution
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLFrameAlignmentProblemC::ComputeError(const VectorXd& vP)  const
{
	//Preconditions
	assert(flagValid);

	//Shift and evaluate
	size_t nObservations=observations.size();
	VectorXd output(nObservations);
	size_t c=0;
	for(const auto& current : observations)
	{
		output[c]=errorMetric( Shift(current.left, forwardTranslation1[c], backwardTranslation1[c], vP[0]), Shift(current.right, forwardTranslation2[c], backwardTranslation2[c], vP[1]) );
		++c;
	}	//for(const auto& current : observations)

	return output;
}	//VectorXd ComputeError(const VectorXd& vP)

/**
 * @brief Computes the Jacobian
 * @param[in] vP Current solution
 * @return Jacobian matrix. Invalid if the Jacobian computation fails
 * @pre \c flagValid=true
 */
optional<MatrixXd> PDLFrameAlignmentProblemC::ComputeJacobian(const VectorXd& vP)  const
{
	//Preconditions
	assert(flagValid);

	optional<MatrixXd> mJ=MatrixXd(observations.size(), vP.size()); mJ->setZero();
	Matrix<double,4,2> dxdp; dxdp.setZero();
	size_t c=0;
	size_t cValid=0;
	for(const auto& current : observations)
	{
		//Jacobian wrt coordinates at the shifted observations
		optional<RowVectorXd> mJx=errorMetric.ComputeJacobiandx1x2( Shift(current.left, forwardTranslation1[c], backwardTranslation1[c], vP[0]), Shift(current.right, forwardTranslation2[c], backwardTranslation2[c], vP[1]));

		if(mJx)
		{
			//Chain rule: coordinate to shift magnitude

			if(vP[0]!=0)
				dxdp.col(0).segment(0,2) = vP[0]>0 ?  forwardTranslation1[c] : (-backwardTranslation1[c]).eval();
			else	//Not defined at 0. Resort to numerical approximation
				dxdp.col(0).segment(0,2) = 0.5*(forwardTranslation1[c]-backwardTranslation1[c]);

			if(vP[1]!=0)
				dxdp.col(1).segment(2,2) = vP[1]>=0 ?  forwardTranslation2[c] : (-backwardTranslation2[c]).eval();
			else	//Not defined at 0. Resort to numerical approximation
				dxdp.col(1).segment(2,2) = 0.5*(forwardTranslation2[c]-backwardTranslation2[c]);

			mJ->row(c)=(*mJx)*dxdp;

			cValid += ((mJ->row(c)).norm()!=0) ? 1:0;	//A 0-row does not contribute to the rank
		}	//if(mJx)

		++c;
	}	//for(const auto& current : observations)

	//Rank test
	bool flagValid= (cValid>=2) && mJ->col(0).norm()>0 && mJ->col(1).norm()>0;

	return flagValid ? mJ : optional<MatrixXd>();
}	//optional<VectorXd> ComputeJacobian(const VectorXd& vP)

/**
 * @brief Computes the distance before and after the application of an update
 * @param[in] vP Current solution
 * @param[in] vU Update term
 * @return Euclidean distance
 * @pre \c flagValid=true
 */
double PDLFrameAlignmentProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU)  const
{
	//Preconditions
	assert(flagValid);

	return vU.norm();
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Makes a result from a parameter vector
 * @param[in] vP Solution vector
 * @return Model structure
 * @pre \c flagValid=true
 */
auto PDLFrameAlignmentProblemC::MakeResult(const VectorXd& vP)  const ->ModelT
{
	//Preconditions
	assert(flagValid);

	ModelT output{ {vP[0], vP[1]} };
	return output;
}	//ModelT MakeResult(const VectorXd& vP)

/**
 * @brief Constructor
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] oobservations Observation list
 * @param[in] oobservationWeights Observation weights. Invalid, if no weighting is applied
 * @param[in] mF Relative calibration between the images
 * @param[in] fforwardTranslation1 Forward translation vectors for the first image
 * @param[in] bbackwardTranslation1 Backward translation vectors for the first image
 * @param[in] fforwardTranslation2 Forward translation vectors for the second image
 * @param[in] bbackwardTranslation2 Backward translation vectors for the second image
 * @pre \c oobservations and the translation vectors have the same number of elements
 * @pre If \c oobservationsWeights is valid, it has the same number of elements as \c oobservations
 * @post The object is valid
 */
PDLFrameAlignmentProblemC::PDLFrameAlignmentProblemC(const ModelT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& oobservationWeights, const EpipolarMatrixT& mF, const vector<Coordinate2DT>& fforwardTranslation1, const vector<Coordinate2DT>& bbackwardTranslation1, const vector<Coordinate2DT> fforwardTranslation2, const vector<Coordinate2DT> bbackwardTranslation2) : flagValid(true)
{
	//Verify the array lengths
	assert(oobservations.size()==fforwardTranslation1.size());
	assert(fforwardTranslation1.size()==fforwardTranslation2.size());
	assert(fforwardTranslation1.size()==bbackwardTranslation1.size());
	assert(fforwardTranslation1.size()==bbackwardTranslation2.size());

	initialEstimate=iinitialEstimate;
	observations=oobservations;

	if(oobservationWeights)
		observationWeights=*oobservationWeights;

	forwardTranslation1=fforwardTranslation1;
	forwardTranslation2=fforwardTranslation2;
	backwardTranslation1=bbackwardTranslation1;
	backwardTranslation2=bbackwardTranslation2;

	errorMetric=EpipolarSampsonErrorC(mF);

	flagValid=true;
}	//PDLFrameAlignmentProblemC(const ModelT& initialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& oobservationWeights, const EpipolarMatrixT& mF, const vector<Coordinate2DT>& fforwardTranslation1, const vector<Coordinate2DT>& bbackgroundTranslation1, const vector<Coordinate2DT> fforwardTranslation2, const vector<Coordinate2DT> bbackgroundTranslation2)

}	//OptimisationN
}	//SeeSawN
