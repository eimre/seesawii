/**
 * @file PDLSimilarity3DEstimationProblem.cpp Implementation of \c PDLSimilarity3DEstimationProblemC
 * @author Evren Imre
 * @date 30 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLSimilarity3DEstimationProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLSimilarity3DEstimationProblemC::PDLSimilarity3DEstimationProblemC()
{}

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 * @pre \c vP is unit norm
 */
auto PDLSimilarity3DEstimationProblemC::MakeResult(const VectorXd& vP) const -> ModelT
{
	//Preconditions
	assert(BaseT::flagValid);
	return ComposeSimilarity3D(vP[0], Coordinate3DT(vP[1], vP[2], vP[3]), QuaternionT(vP[4], vP[5], vP[6], vP[7]).matrix()  );
}	//auto MakeResult(const VectorXd& vP, bool dummy) const -> ModelT

/**
 * @brief Returns the initial estimate in vector form
 * @return Rotation quaternion corresponding to the initial estimate, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLSimilarity3DEstimationProblemC::InitialEstimate() const
{
	assert(BaseT::flagValid);

	double scale;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(scale, vC, mR)=DecomposeSimilarity3D(initialEstimate);

	VectorXd output(8);
	output[0]=scale;
	output.segment(1,3)=vC;

	QuaternionT q(mR);
	output.tail(4)= (q.w()<0 ? -1 : 1) * QuaternionToVector<double>(q);
	return output;
}	//VectorXd PDLRotation3DEstimationProblemC::InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLSimilarity3DEstimationProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeError(MakeResult(vP));
}	//VectorXd ComputeError(const VectorXd& vP)

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLSimilarity3DEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);

	VectorXd vNew=vP+vU;

	VectorXd vq=vNew.tail(4);
	vq.normalize();
	vNew.tail(4) = (vq[0]<0 ? -1 : 1)*vq;

	if(vNew[0]==0)
		vNew[0]=vP[0];	//0-scale breaks the algorithm

	return vNew;
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 * @pre \c vP is unit norm
 */
double PDLSimilarity3DEstimationProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP), MakeResult(vPU));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLSimilarity3DEstimationProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//Jacobian wrt homography
	ModelT mH=MakeResult(vP);
	optional<MatrixXd> mJH=BaseT::ComputeJacobian(mH, vP.size());

	optional<MatrixXd> output;
	if(!mJH)
		return output;

	//Jacobian: dHds,C,q

	RealT s;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(s, vC, mR)=DecomposeSimilarity3D(mH);

	Matrix<double,16,13> dHdsCR=JacobiandHdsCR(mH);

	Matrix<double,16,8> dHdsCq;
	dHdsCq.leftCols(4)=dHdsCR.leftCols(4);

	QuaternionT q(mR);
	Matrix<double,9,4> dRdq=JacobianQuaternionToRotationMatrix(q);
	dHdsCq.block(0,4,16,4)=dHdsCR.block(0,4,16,9)*dRdq;

	output=(*mJH)*dHdsCq;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP)

}	//OptimisationN
}	//SeeSawN

