/**
 * @file PDLFundamentalMatrixEstimationProblem.cpp Implementation of PDLFundamentalMatrixEstimationProblemC
 * @author Evren Imre
 * @date 7 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLFundamentalMatrixEstimationProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLFundamentalMatrixEstimationProblemC::PDLFundamentalMatrixEstimationProblemC()
{}

/**
 * @brief Returns the initial estimate in vector form
 * @return Camera matrix corresponding to the initial estimate, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLFundamentalMatrixEstimationProblemC::InitialEstimate() const
{
	//Preconditions
	assert(BaseT::flagValid);
	CameraMatrixT mP=CameraFromFundamental(BaseT::initialEstimate);	//Compute the corresponding camera matrix

	return Reshape<VectorXd>(mP, 12, 1);
}	//VectorXd InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLFundamentalMatrixEstimationProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	CameraMatrixT mP=Reshape<CameraMatrixT>(vP, 3, 4);
	EpipolarMatrixT mF=CameraToFundamental(mP, false);	//Switch to unnormalised fundamental matrix
	return BaseT::ComputeError(mF);
}	//VectorXd ComputeError(const VectorXd& vP) const

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLFundamentalMatrixEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	return vP+vU;	//Unnormalised camera matrix
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 */
auto PDLFundamentalMatrixEstimationProblemC::MakeResult(const VectorXd& vP) const -> ModelT
{
	//Preconditions
	assert(BaseT::flagValid);
	CameraMatrixT mP=Reshape<CameraMatrixT>(vP, 3, 4);
	return CameraToFundamental(mP, true);
}	//result_type MakeResult(const VectorXd& vP)

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 */
double PDLFundamentalMatrixEstimationProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP), MakeResult(vPU));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLFundamentalMatrixEstimationProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//Jacobian wrt F
	CameraMatrixT mP=Reshape<CameraMatrixT>(vP, 3, 4);
	optional<MatrixXd> mJF=BaseT::ComputeJacobian(CameraToFundamental(mP, false), vP.size());

	optional<MatrixXd> output;
	if(!mJF)
		return output;

	//Camera-to-fundamental matrix conversion, dF/dP
	Matrix<RealT, 9, 12> dFdP1=JacobianCameraToFundamental(mP, false);	//Unnormalised

	//Columns need to be reordered: the last column of mP is represented in the last 3 columns of dFdP. This is not compatible with the ordering of vP
	Matrix<RealT, 9, 12> dFdP(dFdP1);
	dFdP.col(3)=dFdP1.col(9);
	dFdP.block(0,4,9,3)=dFdP1.block(0,3,9,3); dFdP.col(7)=dFdP1.col(10);
	dFdP.block(0,8,9,3)=dFdP1.block(0,6,9,3);

	// dedP = dedF dFdP
	output= (*mJF)*dFdP;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const

}	//OptimisationN
}	//SeeSawN

