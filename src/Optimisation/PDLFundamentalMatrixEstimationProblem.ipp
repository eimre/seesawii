/**
 * @file PDLFundamentalMatrixEstimationProblem.ipp Implementation of PDLFundamentalMatrixEstimationProblemC
 * @author Evren Imre
 * @date 7 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_IPP_7039051
#define PDL_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_IPP_7039051

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <type_traits>
#include <cstddef>
#include "PDLGeometryEstimationProblemBase.h"
#include "../Geometry/FundamentalSolver.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Wrappers/EigenExtensions.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::Matrix;
using std::true_type;
using std::false_type;
using std::size_t;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::GeometryN::Fundamental7SolverC;
using SeeSawN::ElementsN::CameraFromFundamental;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::ElementsN::JacobianCameraToFundamental;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::WrappersN::Reshape;

/**
 * @brief Problem for Powell's dog leg algorithm, for the estimation of a fundamental matrix
 * @remarks The problem minimises the Sampson's error (or rather, its square-root)
 * @remarks Internally, the fundamental matrix is represented as the second camera in a canonical camera pair (i.e., the one not located at the origin). This redundant parameterisation guarantees that the optimisation is performed over valid fundamental matrices.
 * @remarks The optimisation is performed over unnormalised entities, to prevent unnecessary coupling between parameters
 * @warning A valid problem has at least 12 constraints
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLFundamentalMatrixEstimationProblemC : public PDLGeometryEstimationProblemBaseC<PDLFundamentalMatrixEstimationProblemC, Fundamental7SolverC >
{
	private:

		typedef PDLGeometryEstimationProblemBaseC<PDLFundamentalMatrixEstimationProblemC, Fundamental7SolverC> BaseT;	///< Type of the base

		typedef typename BaseT::real_type RealT;
		typedef typename BaseT::solver_type GeometrySolverT;
		typedef typename BaseT::model_type ModelT;
		typedef typename BaseT::error_type GeometricErrorT;

	public:

		/**@name Constructors */ //@{
		PDLFundamentalMatrixEstimationProblemC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLFundamentalMatrixEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix=optional<MatrixXd>());	///< Initial estimate
		//@}

		/** @name PowellDogLegProblemConceptC */ //@{
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP);	///< Computes the Jacobian
		//@}

		/** @name Overrides */ //@{
		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		VectorXd ComputeError(const VectorXd& vP);	///< Computes the error vector for a model
		VectorXd Update(const VectorXd& vP, const VectorXd& vU) const;	///< Updates the current solution
		ModelT MakeResult(const VectorXd& vP) const;	///< Converts a vector to a model
		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const;	///< Computes the distance in the model space as a result of the update \c vU
		//@}
};	//class PDLFundamentalMatrixEstimationProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @param[in] oobservationWeightMatrix Observation weights
 * @post A valid object, if \c ccorrespondences has at least 12 elements
 * @remarks Initial estimate is normalised
 */
template<class CorrespondenceRangeT>
PDLFundamentalMatrixEstimationProblemC::PDLFundamentalMatrixEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)
{
	if(boost::distance(ccorrespondences)<12)
		flagValid=false;
}	//PDLHomographyXDEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)

}	//OptimisationN
}	//SeeSawN

#endif /* PDL_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_IPP_7039051 */
