/**
 * @file PowellDogLeg.h Public interface for PowellDogLegC
 * @author Evren Imre
 * @date 25 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POWELL_DOG_LEG_H_6823223
#define POWELL_DOG_LEG_H_6823223

#include "PowellDogLeg.ipp"
namespace SeeSawN
{
namespace OptimisationN
{
struct PowellDogLegParametersC;	///< Parameters for PowellDogLegC
struct PowellDogLegDiagnosticsC;	///< Diagnostics for PowellDogLegC
template <class ProblemT> class PowellDogLegC;	///< Powell's dog leg algortihm for nonlinear least-squares minimisation
}	//OptimisationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
#endif /* POWELL_DOG_LEG_H_6823223 */
