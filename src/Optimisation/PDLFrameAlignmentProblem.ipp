/**
 * @file PDLFrameAlignmentProblem.ipp Implementation of the Powell's dog leg problem for frame alignment
 * @author Evren Imre
 * @date 22 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_FRAME_ALIGNMENT_PROBLEM_IPP_1608482
#define PDL_FRAME_ALIGNMENT_PROBLEM_IPP_1608482

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <cstddef>
#include <array>
#include <vector>
#include <cmath>
#include <type_traits>
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricError.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using Eigen::MatrixXd;
using Eigen::Matrix;
using Eigen::VectorXd;
using Eigen::RowVectorXd;
using std::array;
using std::size_t;
using std::false_type;
using std::vector;
using std::fabs;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;

/**
 * @brief PDL problem for aligning two frames
 * @remarks Given a set of correspondences and the associated translation vectors, the problems aims to find the shift parameters that minimise the error.
 * @warning Origin for the translation vectors is the associated observation
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLFrameAlignmentProblemC
{
	private:

		typedef array<double,2> ModelT;	///< A pair of shift parameters

		/** @name Configuration */ //@{

		bool flagValid;	///< \c true if the object is valid

		ModelT initialEstimate;	///< Initial estimate
		CoordinateCorrespondenceList2DT observations;	///< Observation list
		optional<MatrixXd> observationWeights;	///< Observation weights

		EpipolarSampsonErrorC errorMetric;	///< Sampson error evaluator
		vector<Coordinate2DT> forwardTranslation1;	///< Forward translation vectors for the first image
		vector<Coordinate2DT> backwardTranslation1;	///< Backward translation vectors for the first image
		vector<Coordinate2DT> forwardTranslation2;	///< Forward translation vectors for the second image
		vector<Coordinate2DT> backwardTranslation2;	///< Backward translation vectors for the second image
		//@}

	public:
		/** @name Implementation details */ //@{
		Coordinate2DT Shift(const Coordinate2DT& x, const Coordinate2DT& forward, const Coordinate2DT& backward, double magnitude) const;	///< Shifts a coordinate
		//@}


		/** @name \c PowellDogLegProblemConceptC interface */ //@{

		typedef CoordinateCorrespondenceList2DT observation_set_type;	///< Type for the observations
		typedef ModelT model_type;	///< Type for the estimated parameters
		typedef ModelT result_type;	///< Type for the result

		typedef false_type has_nontrivial_update;	///< No dedicated update function
		typedef false_type has_normal_solver;	///< No dedicated normal solver
		typedef false_type has_covariance_estimator;	///< No dedicated covariance estimator

		PDLFrameAlignmentProblemC();	///< Default constructor

		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		const optional<MatrixXd>& ObservationWeights() const;	///< A constant reference to \c observationWeights
		bool IsValid() const;	///< Returns \c flagValid

		static void Configure(const ModelT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& oobservationWeights);	///< Dummy function to satisfy the concept requirements
		static void InitialiseIteration();	///< Dummy function to satisfy the concept requirements
		static void FinaliseIteration();	///< Dummy function to satisfy the concept requirements

		VectorXd ComputeError(const VectorXd& vP)  const;	///< Computes the error vector
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP)  const;	///< Computes the Jacobian

		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU)  const;	///< Computes the distance before and after the application of an update
		ModelT MakeResult(const VectorXd& vP)  const;	///< Makes a result from a parameter vector
		//@}

		/**@name model_type definition*/ //@{
		static constexpr size_t iShift1=0;	///< Index of the shift component for the first image
		static constexpr size_t iShift2=1;	///< Index of the shift component for the second image
		//@}

		/**@name Constructor */
		PDLFrameAlignmentProblemC(const ModelT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& oobservationWeights, const EpipolarMatrixT& mF, const vector<Coordinate2DT>& fforwardTranslation1, const vector<Coordinate2DT>& bbackwardTranslation1, const vector<Coordinate2DT> fforwardTranslation2, const vector<Coordinate2DT> bbackwardTranslation2);	///< Constructor
};	//class PDLFrameAlignmentProblemC


}	//OptimisationN
}	//SeeSawN

#endif /* PDLFRAMEALIGNMENTPROBLEM_IPP_ */
