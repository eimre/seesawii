/**
 * @file PDLPoseGraphProblem.cpp Implementation of the pose graph optimisation problem
 * @author Evren Imre
 * @date 22 Jan 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLPoseGraphProblem.h"

namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post The object is invalid
 */
PDLPoseGraphProblemC::PDLPoseGraphProblemC() : flagValid(false)
{}

/**
 * @brief Initialises an iteration
 * @remarks Dummy function to satisfy the concept requirements
 */
void PDLPoseGraphProblemC::InitialiseIteration()
{}

/**
 * @brief Finalises an iteration
 * @remarks Dummy function to satisfy the concept requirements
 */
void PDLPoseGraphProblemC::FinaliseIteration()
{}

/**
 * @brief Returns the initial estimate
 * @pre The object is initialised
 * @return Initial estimate in vector form
 */
VectorXd PDLPoseGraphProblemC::InitialEstimate() const
{
	//Preconditions
	assert(flagValid);

	size_t nParameters=initialEstimate.size() * sPose;
	VectorXd output(nParameters);

	size_t index=0;
	for(const auto& current : initialEstimate)
	{
		output.segment(index, sPosition)=current.second.position;
		output.segment(index+sPosition, sOrientation)=QuaternionToVector(current.second.orientation);
		index+=sPose;
	}	//for(const auto& current : initialEstimate)

	return output;
}	//VectorXd InitialEstimate()

/**
 * @brief Returns the observation weights. Always invalid
 * @return Returns an invalid object
 */
optional<MatrixXd> PDLPoseGraphProblemC::ObservationWeights()
{
	return optional<MatrixXd>();
}	//optional<MatrixXd> ObservationWeights()

/**
 * @brief Converts a parameter vector to a result object
 * @param[in] vP Parameter vector
 * @return Estimated model
 * @pre The object is valid
 */
auto PDLPoseGraphProblemC::MakeResult(const VectorXd& vP) const -> ModelT
{
	//Preconditions
	assert(flagValid);

	ModelT output;

	size_t sP=vP.size();
	auto it=initialEstimate.cbegin();
	for(size_t c=0; c<sP; c+=sPose, advance(it,1))
	{
		Viewpoint3DC currentViewpoint;
		currentViewpoint.position=vP.segment(c, sPosition);

		size_t index=c+sPosition;
		currentViewpoint.orientation=QuaternionT(vP[index], vP[index+1], vP[index+2], vP[index+3]);

		output.emplace(it->first, currentViewpoint);
	}	//for(size_t c=0; c<sP; c+=sPose)

	return output;
}	//auto MakeResult(const VectorXd& vP) -> ModelT

/**
 * @brief Updates a parameter vector
 * @param[in] vP Current solution
 * @param[in] vU Update
 * @return Updated solution
 */
VectorXd PDLPoseGraphProblemC::Update(const VectorXd& vP, const VectorXd& vU)
{
	VectorXd vNew=vP+vU;

	//Normalise the orientation quaternions
	size_t sP=vP.size();
	for(size_t c=sPosition; c<sP; c+=sPose)
	{
		Vector4d vq=vNew.segment(c, sOrientation);
		vq.normalize();

		if(vq[0]<0)
			vq*=-1;

		vNew.segment(c, sOrientation)=vq;
	}	//for(size_t c=sPosition; c<sP; c+=sPose)

	return vNew;
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Computes the distance in the model space as a result of the update
 * @param[in] vP Current parameter vector
 * @param[in] vU Updated term
 * @return Distance in model space
 */
double PDLPoseGraphProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU)
{
	VectorXd vPNew=Update(vP, vU);
	return (vPNew-vP).norm()/max(vPNew.norm(), vP.norm());	//FIXME Unlike rotation difference, translation difference is unbounded. So, this is biased towards translations.
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Computes the error for the current estimate
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre The object is valid
 */
VectorXd PDLPoseGraphProblemC::ComputeError(const VectorXd& vP) const
{
	//Preconditions
	assert(flagValid);

	size_t nObservations=observations.size();
	VectorXd output(nObservations);

	ModelT solution=MakeResult(vP);	//Current solution

	typedef Matrix<double, 6, 1> ErrorVectorT;

	size_t index=0;
	for(const auto& current : observations)
	{
		Viewpoint3DC estimated=ComputeRelativePose(solution[current.first.first], solution[current.first.second]);
		Viewpoint3DC difference=ComputeRelativePose(get<iViewpoint>(current.second), estimated);

		ErrorVectorT error; error.head(sPosition)=difference.position;
		error.tail(3)=QuaternionToRotationVector(difference.orientation);

		output[index]=error.transpose() * (get<iCovariance>(current.second) * error);
		++index;
	}	//for(const auto& current : observations)

	return output;
}	//VectorXd ComputeError(const VectorXd& vP)

/**
 * @brief Computes the Jacobian at the current estimate
 * @param[in] vP Parameter vector
 * @return Jacobian at \c vP
 * @pre \c flagValid=true
 */
optional<MatrixXd> PDLPoseGraphProblemC::ComputeJacobian(const VectorXd& vP) const
{
	//Preconditions
	assert(flagValid);

	if(observations.size() < (size_t)vP.size())
		return optional<MatrixXd>();

	optional<MatrixXd> output=MatrixXd(observations.size(), vP.size());
	output->setZero();

	ModelT solution=MakeResult(vP);	//Current solution

	size_t index=0;
	size_t cSuccess=0;
	for(const auto& current : observations)
	{
		Viewpoint3DC estimated=ComputeRelativePose(solution[current.first.first], solution[current.first.second]);
		Viewpoint3DC difference=ComputeRelativePose(get<iViewpoint>(current.second), estimated);

		if(difference.orientation.w()==1)	//Infinite derivative for QuaternionToRotationVector
		{
			++index;
			continue;
		}

		//Infinite derivative for JacobianRelativePose
		if(get<iViewpoint>(current.second).orientation.w()==1 || solution[current.first.first].orientation.w()==1)
		{
			++index;
			continue;
		}

		Matrix<double,6,1> vDiff;
		vDiff.segment(0,3)=difference.position;
		vDiff.segment(3,3)=QuaternionToRotationVector(difference.orientation);

		Matrix<double,1,6> dedDiff=JacobiandxtAxdx<Matrix<double,1,6>>(get<iCovariance>(current.second), vDiff);
		Matrix<double,3,4> dQ2AA=JacobianQuaternionToRotationVector(difference.orientation);

		Matrix<double,1,7> dedDiffQ;
		dedDiffQ.head(sPosition)=dedDiff.head(3);
		dedDiffQ.tail(sOrientation)=dedDiff.tail(3)*dQ2AA;

		Matrix<double,7,7> dDifferencedEstimate=JacobianRelativePose(get<iViewpoint>(current.second), estimated).rightCols(7);
		Matrix<double,7,14> dEstimatedViewpoints=JacobianRelativePose(solution[current.first.first], solution[current.first.second]);

		Matrix<double,1,14> mJ= dedDiffQ * dDifferencedEstimate * dEstimatedViewpoints;
		output->row(index).segment(current.first.first*sPose, sPose)=mJ.segment(0,sPose);
		output->row(index).segment(current.first.second*sPose, sPose)=mJ.segment(sPose,sPose);

		++index;
		++cSuccess;
	}	//for(const auto& current : observations)

	if(cSuccess < (size_t)vP.size() )
		return optional<MatrixXd>();

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const

/**
 * @brief \c true if the problem object is valid
 * @return Returns \c flagValid
 */
bool PDLPoseGraphProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid()
}	//OptimisationN
}	//SeeSawN

