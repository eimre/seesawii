/**
 * @file PDLGeometryEstimationProblemBase.ipp Implementation of PDLGeometryEstimationProblemBaseC
 * @author Evren Imre
 * @date 24 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_GEOMETRY_ESTIMATION_PROBLEM_BASE_IPP_0934124
#define PDL_GEOMETRY_ESTIMATION_PROBLEM_BASE_IPP_0934124

#include <boost/bimap.hpp>
#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <cmath>
#include <type_traits>
#include <stdexcept>
#include "../Elements/Correspondence.h"
#include "../Geometry/GeometrySolverConcept.h"
#include "../Metrics/GeometricErrorConcept.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using boost::ForwardRangeConcept;
using boost::for_each;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using std::max;
using std::is_same;
using std::true_type;
using std::false_type;
using std::logic_error;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::MetricsN::GeometricErrorConceptC;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Base problem class for Powell's dog leg geometry estimation problems
 * @tparam DerivedT Type derived from this base
 * @tparam GeometrySolverT A geometry solver
 * @pre \c GeometrySolverT is a model of \c GeometrySolverConceptC
 * @pre \c GeometrySolverT::error_type is a model of \c GeometricErrorConceptC
 * @pre \c GeometrySolverT::model_type is an Eigen dense object (unenforced)
 * @remarks Tested through PDLHomographyXDEstimationProblem.h
 * @remarks The minimisation is performed over unnormalised models. Normalisation takes place only at the initialisation and the output. This prevents unnecessary coupling between parameters.
 * @ingroup Problem
 * @nosubgrouping
 */
template<class DerivedT, class GeometrySolverT>
class PDLGeometryEstimationProblemBaseC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<GeometrySolverT>));
	BOOST_CONCEPT_ASSERT((GeometricErrorConceptC<typename GeometrySolverT::error_type>));
	//@endcond

	private:

		typedef typename GeometrySolverT::real_type RealT;	///< Floating point type for the geometry solver
		typedef typename GeometrySolverT::model_type ModelT;	///< Type of the entity being optimised
		typedef typename GeometrySolverT::coordinate_type1 Coordinate1T;	///< Type of the first coordinate in a constraint
		typedef typename GeometrySolverT::coordinate_type2 Coordinate2T;	///< Type of the second coordinate in a constraint
		typedef bimap<list_of<Coordinate1T>, list_of<Coordinate2T>, left_based> CorrespondenceContainerT;	///< A container for constraints

		typedef typename GeometrySolverT::error_type GeometricErrorT;	///< Type of the geometric error

	protected:

		/** @name Configuration */ //@{
		ModelT initialEstimate;	///< Initial estimate
		CorrespondenceContainerT correspondences;	///< Coordinate correspondences
		GeometrySolverT solver;	///< Geometry solver
		GeometricErrorT errorMetric;	///< Error metric
		bool flagValid;	///< \c true if the object is initialised properly
		//@}

		/** @name Implementation details */ //@{
		VectorXd ComputeError(const ModelT& model);	///< Computes the error vector for a model
		optional<MatrixXd> ComputeJacobian(const ModelT& model, unsigned int nParamters);	///< Computes the Jacobian of the error function with respect to a model
		//@}

	public:

		typedef RealT real_type;	///< Floating point type
		typedef ModelT model_type;	///< Type of the geometric model
		typedef GeometrySolverT solver_type;	///< Type of the solver
		typedef GeometricErrorT error_type;	///< Type of the error

		/** @name Constructors */ //@{
		PDLGeometryEstimationProblemBaseC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLGeometryEstimationProblemBaseC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric);	///< Initial estimate
		//@}

		/** @name PowellDogLegProblemConceptC interface */ //@{
		typedef typename GeometrySolverT::model_type result_type;	///< Type of the result
		typedef false_type has_normal_solver;	///< Uses the facilities of the optimisation engine for the normal equations
		typedef false_type has_covariance_estimator;	///< Uses the facilities of the optimisation engine for the covariance
		typedef true_type has_nontrivial_update;	///< Has a dedicated update function

		typedef CorrespondenceContainerT observation_set_type;	///< A set of observations
		template<class CorrespondenceRangeT> void Configure(const model_type& iinitialEstimate, const CorrespondenceRangeT& observations, const optional<MatrixXd>& observationWeightMatrix);	///< Configures the optimisation problem

		bool IsValid() const;	///< Returns \c flagValid
		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		VectorXd ComputeError(const VectorXd& vP);	///< Computes the error vector for a model
		VectorXd Update(const VectorXd& vP, const VectorXd& vU) const;	///< Updates the current solution
		result_type MakeResult(const VectorXd& vP) const;	///< Converts a vector to a model
		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const;	///< Computes the distance in the model space as a result of the update \c vU

		static void InitialiseIteration();	///< Default implementation
		static void FinaliseIteration(); ///< Default implementation
		static optional<MatrixXd> ObservationWeights();	///< Default implementation

		VectorXd SolveNormalEquations(const MatrixXd& mA, const VectorXd& vG) const;	///< Default implementation
		optional<MatrixXd> ComputeCovariance(const VectorXd& vP) const;	///< Default implementation
		//@}
};	//class PDLGeometryEstimationProblemBaseC

/**
 * @brief Computes the error vector for a model
 * @param[in] model Model
 * @return Error vector
 * @pre The object is valid
 * @remarks No dedicated unit tests. A utility function to pack shared code
 */
template<class DerivedT, class GeometrySolverT>
VectorXd PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::ComputeError(const ModelT& model)
{
	//Preconditions
	assert(flagValid);

	errorMetric.SetProjector(model);

	VectorXd vError(correspondences.size());
	size_t c=0;
	for_each(correspondences, [&](const typename CorrespondenceContainerT::value_type& current){ vError[c]=errorMetric(current.left, current.right); ++c; });

	return vError;
}	//VectorXd ComputeError(const ModelT& model, const VectorXd& vP)

/**
 * @brief Computes the Jacobian of the error function with respect to a model
 * @param[in] model Model
 * @param[in] nParameters Number of parameters for the internal representation
 * @return Jacobian matrix. Invalid if the Jacobian computation fails, or there are too few constraints
 * @pre The object is valid
 * @remarks No dedicated unit tests. A utility function to pack shared code
 */
template<class DerivedT, class GeometrySolverT>
optional<MatrixXd> PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::ComputeJacobian(const ModelT& model, unsigned int nParameters)
{
	//Preconditions
	assert(flagValid);

	optional<MatrixXd> output;
	if(correspondences.size() < nParameters)
		return output;

	errorMetric.SetProjector(model);

	output=MatrixXd::Zero(correspondences.size(), model.size());

	typedef typename GeometricErrorT::jacobian_type JacobianT;
	size_t c=0;
	size_t cSuccess=0;
	for(const auto& current : correspondences)
	{
		optional<JacobianT> vJ(errorMetric.ComputeJacobiandP(current.left, current.right));
		if(vJ)
		{
			output->row(c)=vJ->template cast<double>();
			++cSuccess;
		}	//if(vJ)

		++c;
	}	//for(const auto& current : correspondences)

	//If the number of non-zero rows is less than number of parameters, (J^T)J is rank deficient
	if(cSuccess<nParameters)
		return optional<MatrixXd>();

	return output;
}	//optional<MatrixXd> ComputeJacobian(const ModelT& model)

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class DerivedT, class GeometrySolverT>
PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::PDLGeometryEstimationProblemBaseC()
{
	flagValid=false;
}	//PDLGeometryEstimationProblemBaseC()

/**
 * @brief Initial estimate
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @pre A bimap of appropriate correspondence types (unenforced)
 * @post A valid object, unless \c ccorrespondences is empty
 */
template<class DerivedT, class GeometrySolverT>
template<class CorrespondenceRangeT>
PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::PDLGeometryEstimationProblemBaseC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric) : solver(ssolver), errorMetric(eerrorMetric), flagValid(false)
{
	Configure(iinitialEstimate, ccorrespondences, optional<MatrixXd>());
}	//PDLGeometryEstimationProblemBaseC(const ModelT iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric)

/**
 * @brief Configures the optimisation problem
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] observations Observations
 * @param[in] observationWeightMatrix Observation weight matrix. Ignored in this default implementation
 * @pre A bimap of appropriate correspondence types (unenforced)
 * @post \c initialEstimate and \c correspondences are modified
 * @post \c flagValid is set, if \c observations do not have less elements than the DoF of the model
 */
template<class DerivedT, class GeometrySolverT>
template<class CorrespondenceRangeT>
void PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::Configure(const model_type& iinitialEstimate, const CorrespondenceRangeT& observations, const optional<MatrixXd>& observationWeightMatrix)
{
	//Preconditions
	static_assert(is_same<Coordinate1T, typename CorrespondenceRangeT::left_key_type>::value, "PDLGeometryEstimationProblemBaseC::Configure : Coordinate type mismatch. See GeometrySolverT.");
	static_assert(is_same<Coordinate2T, typename CorrespondenceRangeT::right_key_type>::value, "PDLGeometryEstimationProblemBaseC::Configure : Coordinate type mismatch. See GeometrySolverT.");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));

	flagValid=boost::distance(observations)>=GeometrySolverT::DoF();

	initialEstimate=iinitialEstimate;
	correspondences.clear();
	for(const auto& current : observations)
		correspondences.push_back(typename CorrespondenceContainerT::value_type(current.left, current.right));
}	//void Configure(const model_type& iinitialEstimate, const observation_set_type& observations)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class DerivedT, class GeometrySolverT>
bool PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Returns the initial estimate in vector form
 * @return A vector corresponding to the initial estimate
 * @pre \c flagValid=true
 */
template<class DerivedT, class GeometrySolverT>
VectorXd PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::InitialEstimate() const
{
	//Preconditions
	assert(flagValid);
	return solver.MakeVector(initialEstimate).template cast<double>();
}	//VectorXd InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 * @post \c errorMetric is modified
 */
template<class DerivedT, class GeometrySolverT>
VectorXd PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(flagValid);

	ModelT mP=solver.MakeModel(vP.cast<RealT>(), false);	//Unnormalised, to prevent unnecessary coupling between parameters
	return ComputeError(mP);
}	//VectorXd ComputeError(const VectorXd& vP) const

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
template<class DerivedT, class GeometrySolverT>
VectorXd PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(flagValid);

	ModelT mNew=solver.MakeModel( (vP+vU).cast<RealT>(), false);	//Convert to model. Unnormalised, to prevent unnecessary coupling between parameters
	return solver.MakeVector(mNew).template cast<double>();	// Back to vector
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 */
template<class DerivedT, class GeometrySolverT>
auto PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::MakeResult(const VectorXd& vP) const -> result_type
{
	//Preconditions
	assert(flagValid);

	return solver.MakeModel(vP.cast<RealT>(), true);
}	//result_type MakeResult(const VectorXd& vP)

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 */
template<class DerivedT, class GeometrySolverT>
double PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(flagValid);

	//Vector for the updated model
	ModelT mPU=solver.MakeModel( (vP+vU).cast<RealT>() , false);
	VectorXd vPU=solver.MakeVector(mPU).template cast<double>();

	return (vP-vPU).norm()/max(vP.norm(), vPU.norm());
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Default implementation
 */
template<class DerivedT, class GeometrySolverT>
void PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::InitialiseIteration()
{

}	//void InitialiseIteration() const

/**
 * @brief Default implementation
 */
template<class DerivedT, class GeometrySolverT>
void PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::FinaliseIteration()
{

}	//void FinaliseIteration() const

/**
 * @brief Default implementation
 * @return An invalid optional objects
 */
template<class DerivedT, class GeometrySolverT>
optional<MatrixXd> PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::ObservationWeights()
{
	return optional<MatrixXd>();
}	//optional<MatrixXd> ObservationWeights() const

/**
 * @brief Default implementation
 * @param[in] mA Coefficient matrix
 * @param[in] vG Rhs vector
 * @return Empty vector
 * @throws logic_error Always
 * @remarks Only for satisfying the PowellDogLegProblemConceptC requirements. Should not be called
 */
template<class DerivedT, class GeometrySolverT>
VectorXd PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::SolveNormalEquations(const MatrixXd& mA, const VectorXd& vG) const
{
	throw(logic_error("PDLGeometryEstimationProblemBaseC::SolveNormalEquations : This function must be overriden by the derived class."));
	return VectorXd();
}	//VectorXd SolveNormalEquations(const MatrixXd& mA, const VectorXd& vG) const

/**
 * @brief Default implementation
 * @param[in] vP Parameter vector
 * @return Invalid object
 * @throws logic_error Always
 * @remarks Only for satisfying the PowellDogLegProblemConceptC requirements. Should not be called
 */
template<class DerivedT, class GeometrySolverT>
optional<MatrixXd> PDLGeometryEstimationProblemBaseC<DerivedT, GeometrySolverT>::ComputeCovariance(const VectorXd& vP) const
{
	throw(logic_error("PDLGeometryEstimationProblemBaseC::ComputeCovariance : This function must be overriden by the derived class."));
	return optional<MatrixXd>();
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const
}	//OptimisationN
}	//SeeSawN

#endif /* PDL_GEOMETRY_ESTIMATION_PROBLEM_BASE_IPP_0934124 */
