/**
 * @file PowellDogLeg.ipp Implementation of PowellDogLegC
 * @author Evren Imre
 * @date 25 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POWELL_DOG_LEG_IPP_6823223
#define POWELL_DOG_LEG_IPP_6823223

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <tuple>
#include <array>
#include <complex>
#include <list>
#include <climits>
#include "PowellDogLegProblemConcept.h"
#include "../Numeric/Polynomial.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::lexical_cast;
using boost::optional;
using boost::math::pow;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::invalid_argument;
using std::string;
using std::true_type;
using std::false_type;
using std::tuple;
using std::tie;
using std::get;
using std::array;
using std::complex;
using std::list;
using std::numeric_limits;
using SeeSawN::OptimisationN::PowellDogLegProblemConceptC;
using SeeSawN::NumericN::QuadraticPolynomialRoots;
using SeeSawN::NumericN::ComputePseudoInverse;

/**
 * @brief Parameters for PowellDogLegC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct PowellDogLegParametersC
{
	/** @name Convergence */ //@{
	unsigned int maxIteration;	///< Maximum number of iterations
	double epsilon1;	///< Gradient convergence criterion, as the maximum absolute value of the RHS of the normal equations
	double epsilon2;	///< Solution convergence criterion, as the relative distance between two solutions
	//@}

	/** @name Trust region management */ //@{
	double delta0;	///< Initial radius of the trust region
	double lambdaShrink;	///< Multiplicative shrinking factor for the trust region for a failed update
	double lambdaGrowth;	///< Multiplicative growth factor for the trust region, for a successful update
	//@}

	bool flagCovariance;	///< If \c true , the covariance is calculated as well

	/** brief Default constructor */
	PowellDogLegParametersC() : maxIteration(100), epsilon1(1e-12), epsilon2(1e-12), delta0(1), lambdaShrink(0.8), lambdaGrowth(1.25), flagCovariance(false)
	{}
};	//struct PowellDogLegParametersC

/**
 * @brief Diagnostics for PowellDogLegC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct PowellDogLegDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminated successfully
	unsigned int nIterations;	///< Number of iteration at the termination
	double error;	///< Error at the termination

	double finalEpsilon1;	///< \f$ \epsilon_1 \f$ at termination
	double finalEpsilon2;	///< \f$ \epsilon_2 \f$ at termination
	double finalDelta;	///< \c delta at termination
	list<double> errorHistory;	///< Error history

	/** @brief Default constructor */
	PowellDogLegDiagnosticsC() : flagSuccess(false), nIterations(0), error(-1), finalEpsilon1(-1), finalEpsilon2(-1), finalDelta(-1)
	{}
};	//struct PowellDogLegDiagnosticsC

/**
 * @brief Powell's dog leg algorithm for nonlinear least squares minimisation
 * @tparam ProblemT A problem
 * @pre \c ProblemT is a model of PowellDogLegProblemConceptC
 * @remarks R1: "Is Levenberg-Marquardt the Most Efficient Optimization Algorithm for Implementing Bundle Adjstment?, " M.I.A. Lourakis, A. Argyros, ICCV 2005
 * @remarks R2: "A Brief Description of the Levenberg-Marquardt Algorithm as Implemented by levmar, " M.I.A. Lourakis
 * @remarks R3: "Multiple View Geometry in Computer Vision," R. Hartley, A. Zisserman, 2nd Ed, 2003
 * @remarks Differences with R1
 * - \c epsilon2 termination criterion is the threshold for the relative model distance. This works better in non-Euclidean manifolds
 * - The observations are implicit, i.e., an observation is of the form \f$ h(\mathbf{x,p})=0 \f$. R1 employs explicit observations with the form \f$ \mathbf{x}=f(\mathbf{p}) \f$
 * - In the implict observation model, the sign of the error is flipped:
 * 		- Implict: \f$ h(\mathbf{x,p+\delta_p}) = h(\mathbf{x,p}) + \mathbf{J_h \delta_p}\f$
 * 		- Explicit: \f$ \mathbf{x}-f(\mathbf{p})-\mathbf{J_f \delta_p} \f$
 * 		Therefore \f$ \mathbf{g} = -\mathbf{J^t \epsilon_p}\f$
 * - The current solution can be updated by applying the dog-leg step in the model domain (has_nontrivial_update=true_type in the problem). This is useful when dealing with rotations and models with unit-norm constraint, where a simple addition yields an invalid model.
 * @warning The algorithm minimises the total square error on the individual constraints. Care should be taken with error metrics such as Sampson error, which are often defined in the "squared" form.
 * @warning If the procedure refuses to move away from the initial solution, a likely culprit is the sign of the Jacobian.
 * @warning The estimated covariance only pertains to the basin in which the solution resides. Also, it is may not work when there are redundant parameters
 * @ingroup Algorithm
 * @nosubgrouping
 */
template <class ProblemT>
class PowellDogLegC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((PowellDogLegProblemConceptC<ProblemT>));
	//@endcond

	private:
		typedef typename ProblemT::result_type ResultT;

		/** @name Implementation details */ //@{
		static void ValidateParameters(const PowellDogLegParametersC& parameters);	///<  Validates the parameters

		static VectorXd SolveNormalEquations(const ProblemT& problem, const MatrixXd& mA, const VectorXd& vG, true_type dummy);	///< Delegates the normal equations to the problem
		static VectorXd SolveNormalEquations(const ProblemT& problem, const MatrixXd& mA, const VectorXd& vG, false_type dummy);	///< Solves the normal equations

		static VectorXd Update(const ProblemT& problem, const VectorXd& vP, const VectorXd& vU, true_type dummy);	///< Delegates the model update operation to the problem
		static VectorXd Update(const ProblemT& problem, const VectorXd& vP, const VectorXd& vU, false_type dummy);	///< Updates the model

		static optional<MatrixXd> ComputeCovariance(const ProblemT& problem, const MatrixXd& mA, true_type dummy);	///< Delegates the computation of the covariance to the problem
		static optional<MatrixXd> ComputeCovariance(const ProblemT& problem, const MatrixXd& mA, false_type dummy);	///< Computes the covariance

		static bool ComputeGradientVectors(VectorXd& vSD, VectorXd& vGN, MatrixXd& mA, VectorXd& vG, const VectorXd& vP, const VectorXd& vE, ProblemT& problem, const PowellDogLegParametersC& parameters);	///< Computes the Gauss-Newton and the steepest descent vectors

		static double ComputeBeta(const VectorXd& vSD, const VectorXd& vDif, double delta);	///< Computes the mixing parameter
		static VectorXd ComputeDogLegVector(const VectorXd& vSD, const VectorXd& vGN, double lengthSD, double lengthGN, double delta);	///< Computes the dog-leg vector
		//@}

	public:

		typedef ResultT result_type;	///< Type of the result
		typedef optional<MatrixXd> covariance_type;	///< Type of the covariance
		static PowellDogLegDiagnosticsC Run(ResultT& solution, optional<MatrixXd>& covariance, ProblemT& problem, const PowellDogLegParametersC& parameters);	///< Runs the algorithm

};	//class PowellDogLegC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @param[in] parameters Parameters to be validated
 * @throws invalid_argument if any of the parameters is invalid
 */
template<class ProblemT>
void PowellDogLegC<ProblemT>::ValidateParameters(const PowellDogLegParametersC& parameters)
{
	if(parameters.epsilon1<=0)
		throw(invalid_argument(string("PowellDogLegC::ValidateParameters : epsilon1 must be a positive value. Value: ")+lexical_cast<string>(parameters.epsilon1)));

	if(parameters.epsilon2<=0)
		throw(invalid_argument(string("PowellDogLegC::ValidateParameters : epsilon2 must be a positive value. Value: ")+lexical_cast<string>(parameters.epsilon2)));

	if(parameters.delta0<=0)
		throw(invalid_argument(string("PowellDogLegC::ValidateParameters : delta0 must be a positive value. Value: ")+lexical_cast<string>(parameters.delta0)));

	if(parameters.lambdaShrink<=0 || parameters.lambdaShrink>=1)
		throw(invalid_argument(string("PowellDogLegC::ValidateParameters : lambdaShrink must be in (0,1). Value: ")+lexical_cast<string>(parameters.lambdaShrink)));

	if(parameters.lambdaGrowth<=1)
		throw(invalid_argument(string("PowellDogLegC::ValidateParameters : lambdaGrowth must be in >1. Value: ")+lexical_cast<string>(parameters.lambdaGrowth)));
}	//void ValidateParameters(const PowellDogLegParametersC& parameters)

/**
 * @brief Delegates the normal equations to the problem
 * @param[in] problem Problem
 * @param[in] mA Coefficient matrix
 * @param[in] vG Right-hand side
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Solution to the normal equations
 */
template<class ProblemT>
VectorXd SolveNormalEquations(const ProblemT& problem, const MatrixXd& mA, const VectorXd& vG, true_type dummy)
{
	return problem.SolveNormalEquations(mA, vG);
}	//VectorXd SolveNormalEquations(const MatrixXd& mA, const VectorXd& vG, true_type dummy)

/**
 * @brief Solves the normal equations
 * @param[in] problem Problem. Dummy
 * @param[in] mA Coefficient matrix
 * @param[in] vG Right-hand side
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Solution to the normal equations
 */
template<class ProblemT>
VectorXd PowellDogLegC<ProblemT>::SolveNormalEquations(const ProblemT& problem, const MatrixXd& mA, const VectorXd& vG, false_type dummy)
{
	return mA.colPivHouseholderQr().solve(vG);
}	//VectorXd SolveNormalEquations(const MatrixXd& mA, const VectorXd& vG, false_type dummy)

/**
 * @brief Delegates the model update operation to the problem
 * @param[in] problem Problem
 * @param[in] vP Current parameter vector
 * @param[in] vU Update term
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Updated parameter vector
 * @remarks Nontrivial update
 */
template<class ProblemT>
VectorXd PowellDogLegC<ProblemT>::Update(const ProblemT& problem, const VectorXd& vP, const VectorXd& vU, true_type dummy)
{
	return problem.Update(vP, vU);
}	//VectorXd Update(ProblemT& problem, const VectorXd& vP, const VectorXd& vU, true_type dummy)

/**
 * @brief Updates the model
 * @param[in] problem Problem
 * @param[in] vP Current parameter vector
 * @param[in] vU Update term
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Updated parameter vector
 * @remarks Trivial update: just addition
 */
template<class ProblemT>
VectorXd PowellDogLegC<ProblemT>::Update(const ProblemT& problem, const VectorXd& vP, const VectorXd& vU, false_type dummy)
{
	return vP+vU;
}	//VectorXd Update(ProblemT& problem, const VectorXd& vP, const VectorXd& vU, false_type dummy)

/**
 * @brief Delegates the computation of the covariance to the problem
 * @param[in] problem Problem
 * @param[in] mA Coefficient matrix for the normal equations
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Covariance matrix, or an invalid value if the inversion failed
 */
template<class ProblemT>
optional<MatrixXd> PowellDogLegC<ProblemT>::ComputeCovariance(const ProblemT& problem, const MatrixXd& mA, true_type dummy)
{
	return problem.ComputeCovariance(mA);
}	//optional<MatrixXd> ComputeCovariance(ProblemT& problem, const MatrixXd& mA, true_type dummy)

/**
 * @brief Computes the covariance
 * @param[in] problem Problem. Dummy
 * @param[in] mA Coefficient matrix for the normal equations
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Covariance matrix, or an invalid value if the inversion failed
 * @remarks Since \c -vG is negated, the covariance expression should be negated as well
 */
template<class ProblemT>
optional<MatrixXd> PowellDogLegC<ProblemT>::ComputeCovariance(const ProblemT& problem, const MatrixXd& mA, false_type dummy)
{
	return optional<MatrixXd>(ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mA, 10*numeric_limits<double>::epsilon()));	//R3, Eq. 5.12
}	//optional<MatrixXd> ComputeCovariance(ProblemT& problem, const MatrixXd& mA, false_type dummy)

/**
 * @brief Computes the Gauss-Newton and the steepest descent vectors
 * @param[out] vSD Steepest descent vector
 * @param[out] vGN Gauss-Newton vector
 * @param[out] mA Coefficient matrix for the normal equations
 * @param[out] vG RHS vector for the normal equations
 * @param[in] vP Current solution vector
 * @param[in] vE Error vector
 * @param[in] problem Problem
 * @param[in] parameters Parameters
 * @return \c false if the Jacobian computation fails, or the algorithm terminates
 */
template<class ProblemT>
bool PowellDogLegC<ProblemT>::ComputeGradientVectors(VectorXd& vSD, VectorXd& vGN, MatrixXd& mA, VectorXd& vG, const VectorXd& vP, const VectorXd& vE, ProblemT& problem, const PowellDogLegParametersC& parameters)
{
	optional<MatrixXd> mJ=problem.ComputeJacobian(vP);

	//If the Jacobian computation fails, break
	if(!mJ)
		return false;

	//Set up the normal equations
	MatrixXd mJt=mJ->transpose();
	if(problem.ObservationWeights())	//Weighted measurements, e.g. inverse covariance. R2, Eq4
		mJt*= (*problem.ObservationWeights());

	mA=mJt*(*mJ);
	vG=-mJt*vE;	//See the remark about the observations

	//Check termination, epsilon1
	if(vG.array().abs().maxCoeff()<=parameters.epsilon1)
		return false;

	//Gauss-Newton direction
	vGN=SolveNormalEquations(problem, mA, vG, typename ProblemT::has_normal_solver());

	//If the normal equation solver fails
	if(isnan(vGN.norm()))
	    return false;

	//Steepest descent direction
	VectorXd vJG=(*mJ)*vG;
	vSD= (vG.squaredNorm()/vJG.squaredNorm()) * vG;

	return true;
}	//tuple<VectorXd, VectorXd> ComputeGradientVectors(VectorXd& vSD, VectorXd& vGN, MatrixXd& mA, MatrixXd& vG, const VectorXd& solution, const VectorXd& vE, ProblemT& problem, const PowellDogLegParametersC& parameters)

/**
 * @brief Computes the mixing parameter
 * @param[in] vSD Steepest descent vector
 * @param[in] vDif Difference between the GN and SD vectors
 * @param[in] delta Radius of the trust region
 * @return The mixing parameter
 */
template<class ProblemT>
double PowellDogLegC<ProblemT>::ComputeBeta(const VectorXd& vSD, const VectorXd& vDif, double delta)
{
	array<double,3> coefficients{{vDif.squaredNorm(), 2*vDif.dot(vSD), vSD.squaredNorm()-pow<2>(delta)}};

	//3rd coefficient is guaranteed to be negative- check the flow.
	//1. There is a postive root
	//2. Since the first coefficient is positive, the discriminant is guaranteed to be positive

	array<complex<double>, 2> roots=QuadraticPolynomialRoots(coefficients);
	return roots[0].real()>0 ? roots[0].real() : roots[1].real();
}	//double ComputeBeta(const VectorXd& vSD, const VectorXd& vDif, double beta)

/**
 * @brief Computes the dog-leg vector
 * @param[in] vSD Steepest-descent vector
 * @param[in] vGN Gauss-Newton vector
 * @param[in] lengthSD Norm of vSD
 * @param[in] lengthGN Norm of vGN
 * @param[in] delta Radius of the trust region
 * @return Dog-leg vector
 */
template<class ProblemT>
VectorXd PowellDogLegC<ProblemT>::ComputeDogLegVector(const VectorXd& vSD, const VectorXd& vGN, double lengthSD, double lengthGN, double delta)
{
	//If the SD update goes beyond the trust region, move along vSD
	if(lengthSD>=delta)
		return (delta/lengthSD)*vSD;	//Clip at the boundary of the trust region

	//If the Gauss-Newton update is within the trust region, move along vGN
	if(lengthGN<=delta)
		return vGN;

	//If VGN=vSD, no need to compute beta: when vSD=vGN, beta computation fails
	if(vGN.isApprox(vSD, 10*numeric_limits<double>::epsilon()))
		return vSD;

	//vDL as a linear combination of vGN and vSD
	VectorXd vDif=vGN-vSD;
	double beta=ComputeBeta(vSD, vDif, delta);
	assert(beta>0 && beta<1);

	return vSD + beta*vDif;
}	//VectorXd ComputeDogLegVector(const VectorXd& vSD, const VectorXd& vGN, double lengthSD, double lengthGN, double delta)

/**
 * @brief Runs the algorithm
 * @param[out] solution Output
 * @param[out] covariance Covariance
 * @param[in,out] problem Optimisation problem
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @throws invalid_argument If the problem is not initialised properly
 * @remarks The default implementation computes the covariance via pseudo-inverse. So, \c covariance is not guaranteed to be symmetric or invertible
 */
template<class ProblemT>
PowellDogLegDiagnosticsC PowellDogLegC<ProblemT>::Run(ResultT& solution, optional<MatrixXd>& covariance, ProblemT& problem, const PowellDogLegParametersC& parameters)
{
	ValidateParameters(parameters);

	if(!problem.IsValid())
		throw(invalid_argument("PowellDogLegC::Run : Invalid problem"));

	PowellDogLegDiagnosticsC diagnostics;

	//Implementation follows R1 Figure 3
	unsigned int cIteration=0;	//Iteration counter
	double delta=parameters.delta0;	//Radius of the confidence region

	VectorXd vP=problem.InitialEstimate();	//Current solution, in vector form
	VectorXd vE=problem.ComputeError(vP);	//Current error vector

	double bestError=vE.norm();
	diagnostics.errorHistory.push_back(bestError);
	bool flagSolutionFound=false;

	MatrixXd mA;	//Coefficient matrix for the normal equations

	do
	{
		problem.InitialiseIteration();	//Notifies the problem that a new iteration has begun

		//Compute the new gradient directions
		VectorXd vGN;	//Gauss-Newton direction
		VectorXd vSD;	//Steepest descent direcion
		VectorXd vG;	//RHS vector for the normal equations
		bool flagGradient=ComputeGradientVectors(vSD, vGN, mA, vG, vP, vE, problem, parameters);

		if(!flagGradient)
			break;

		//TODO This block can be parallelised: Try multiple steps at once
		//Try to move in the dog-leg direction until the error is reduced, or termination
		double lengthSD=vSD.norm();
		double lengthGN=vGN.norm();
		bool flagTerminate=false;
		while(true)
		{
			//Compute the dog-leg vector
			VectorXd vDL=ComputeDogLegVector(vSD, vGN, lengthSD, lengthGN, delta);

			//Proceed along the dog-leg vector, and evaluate the error
			VectorXd vPNew=Update(problem, vP, vDL, typename ProblemT::has_nontrivial_update());	//The update operation may involve more than a simple addition

			//If no meaningful update, terminate
			double epsilon2=problem.ComputeRelativeModelDistance(vP, vDL);
			if(epsilon2<=parameters.epsilon2)
			{
				flagTerminate=true;
				break;
			}	//if(problem.ComputeModelDistance(solution, vPNew)<=parameters.epsilon2)

			vE=problem.ComputeError(vPNew);
			double newError=vE.norm();

			//Update the trust region
			double deltaL=2*vG.dot(vDL) - vDL.dot( (mA*vDL).eval() );	//R1 Eq 5

			if(deltaL<=0)	//This should not happen in normal operation. But may occur in practice due to numerical issues
			{
				flagTerminate=true;
				break;
			}

			double rho=(bestError-newError)/deltaL;	//Error reduction, relative to the predicted reduction

			//If no/poor error reduction, try smaller moves. Else, get bolder
			delta *= (rho<=0.25) ? parameters.lambdaShrink : ( (rho>=0.75) ? parameters.lambdaGrowth : 1 );
/*
			//Termination on low delta makes sense, as it directly affects the step size. On the other hand, negligible steps should not cause a non-negligible move in the model space. So, currently, only the epsilon2 check
			//Negligible steps, break
			if(delta<numeric_limits<double>::epsilon())
			{
				flagTerminate=true;
				break;
			}
			*/

			//If there is improvement, accept the update
			if(newError<bestError)
			{
				//Update the solution
				vP=Update(problem, vP, vDL, typename ProblemT::has_nontrivial_update());	//The update operation may involve more than a simple addition
				bestError=newError;

				flagSolutionFound=true;

				//Diagnostics
				diagnostics.errorHistory.push_back(bestError);
				diagnostics.finalEpsilon1=vG.array().abs().maxCoeff();
				diagnostics.finalEpsilon2=epsilon2;

				break;
			}	//if(newError<error)
		}	//while(true)

		++cIteration;

		//Check termination
		if(flagTerminate || cIteration>=parameters.maxIteration)
			break;

		problem.FinaliseIteration();	//Notifies the problem that the current iteration is over
	}while(true);

	problem.FinaliseIteration();	//Finalise the last iteration

	//Solution
	solution=problem.MakeResult(vP);

	//Compute the covariance
	if(flagSolutionFound && parameters.flagCovariance)
		covariance=ComputeCovariance(problem, mA, typename ProblemT::has_covariance_estimator());

	//Diagnostics
	diagnostics.error=bestError;
	diagnostics.flagSuccess=flagSolutionFound;
	diagnostics.nIterations=cIteration;
	diagnostics.finalDelta=delta;

	return diagnostics;
}	//PowellDogLegDiagnosticsC Run(ResultT& output, ProblemT& problem, const PowellDogLegParametersC& parameters)

}	//OptimisationN
}	//SeeSawN

#endif /* POWELL_DOG_LEG_IPP_6823223 */
