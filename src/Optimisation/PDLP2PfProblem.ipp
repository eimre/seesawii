/**
 * @file PDLP2PfProblem.ipp Implementation of the problem for Powell's dog leg algorithm for orientation and focal length estimation
 * @author Evren Imre
 * @date 11 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_P2PF_PROBLEM_IPP_1098423
#define PDL_P2PF_PROBLEM_IPP_1098423

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <climits>
#include <vector>
#include <cstddef>
#include <tuple>
#include "PDLGeometryEstimationProblemBase.h"
#include "../Geometry/P2PfSolver.h"
#include "../Geometry/Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Numeric/LinearAlgebraJacobian.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using Eigen::VectorXd;
using Eigen::Matrix;
using std::vector;
using std::numeric_limits;
using std::size_t;
using std::tie;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::GeometryN::P2PfSolverC;
using SeeSawN::GeometryN::JacobianQuaternionToRotationMatrix;
using SeeSawN::GeometryN::QuaternionToVector;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::JacobiandPdKCR;
using SeeSawN::ElementsN::CameraMatrixT;

/**
 * @brief Problem for Powell's dog leg optimisation algorithm, for orientation and focal length estimation
 * @remarks The problem minimises the reprojection error
 * @remarks An orientation is internally parameterised by a quaternion
 * @warning A valid problem has at least 5 constraints
 * @warning The image points should be normalised by the known intrinsics (i.e. all other than the focal length)
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLP2PfProblemC : public PDLGeometryEstimationProblemBaseC<PDLP2PfProblemC, P2PfSolverC>
{
	private:

		typedef PDLGeometryEstimationProblemBaseC<PDLP2PfProblemC, P2PfSolverC> BaseT;	///< Type of the base

		typedef typename BaseT::real_type RealT;
		typedef typename BaseT::solver_type GeometrySolverT;
		typedef typename BaseT::model_type ModelT;
		typedef typename BaseT::error_type GeometricErrorT;

	public:

		/**@name Constructors */ //@{
		PDLP2PfProblemC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLP2PfProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix=optional<MatrixXd>());	///< Constructor
		//@}

		/** @name PowellDogLegProblemConceptC */ //@{
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP);	///< Computes the Jacobian
		//@}

		/** @name Overrides */ //@{
		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		VectorXd ComputeError(const VectorXd& vP);	///< Computes the error vector for a model
		VectorXd Update(const VectorXd& vP, const VectorXd& vU) const;	///< Updates the current solution
		ModelT MakeResult(const VectorXd& vP) const;	///< Converts a vector to a model
		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const;	///< Computes the distance in the model space as a result of the update \c vU
		//@}
};	//class PDLP2PfProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @param[in] oobservationWeightMatrix Observation weights
 * @post A valid object, of \c CorrespondenceRangeT has 5 elements
 */
template<class CorrespondenceRangeT>
PDLP2PfProblemC::PDLP2PfProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)
{
	if(boost::distance(ccorrespondences)<5)
		flagValid=false;
}	//PDLRotation3DEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix)


}	//OptimisationN
}	//SeeSawN

#endif /* PDL_P2PF_PROBLEM_IPP_1098423 */
