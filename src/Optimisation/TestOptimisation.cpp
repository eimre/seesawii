/*
 * TestOptimisation.cpp Unit tests for the optimisation module
 * @author Evren Imre
 * @date 26 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE OPTIMISATION

#include <boost/test/unit_test.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <Eigen/Dense>
#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <iterator>
#include "PDLGeometryEstimationProblemBase.h"
#include "PDLHomographyXDEstimationProblem.h"
#include "PDLFundamentalMatrixEstimationProblem.h"
#include "PDLEssentialMatrixEstimationProblem.h"
#include "PDLOneSidedFundamentalMatrixEstimationProblem.h"
#include "PDLRotation3DEstimationProblem.h"
#include "PDLSimilarity3DEstimationProblem.h"
#include "PDLP2PProblem.h"
#include "PDLP2PfProblem.h"
#include "PDLP3PProblem.h"
#include "PDLP4PProblem.h"
#include "PDLFrameAlignmentProblem.h"
#include "PDLPoseGraphProblem.h"
#include "PDLHomographyKRKDecompositionProblem.h"
#include "PowellDogLeg.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Viewpoint3D.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Geometry/Homography2DSolver.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestOptimisationN
{

using namespace SeeSawN::OptimisationN;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using Eigen::VectorXd;
using Eigen::RowVectorXd;
using Eigen::MatrixXd;
using std::logic_error;
using std::invalid_argument;
using std::max;
using std::copy;
using std::back_inserter;
using std::next;
using SeeSawN::ElementsN::Viewpoint3DC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::ComposeSimilarity3D;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::GeometryN::Homography2DSolverC;

BOOST_AUTO_TEST_SUITE(PDL_Geometry_Problems)

BOOST_AUTO_TEST_CASE(PDL_HomographyXD_Problem)
{
	//Model: 2x+3, y+5
	CoordinateCorrespondenceList2DT observationSet;
	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;

	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(5, 6), MatchStrengthC(0.1, 0.1)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.3)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.35)));

	//Noisy inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));

	//PDLGeometryEstimationProblemBaseC

	//Default constructor
	typedef PDLHomographyXDEstimationProblemC<Homography2DSolverC> PDLHomography2DProblemT;

	PDLHomography2DProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLHomography2DProblemT::model_type mH; mH<<2, 0, 3, 0, 1, 5, 0, 0, 1;
	PDLHomography2DProblemT::solver_type solver;
	PDLHomography2DProblemT::error_type errorMetric;
	PDLHomography2DProblemT problem2(mH, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	CoordinateCorrespondenceList2DT emptySet;
	PDLHomography2DProblemT problem3(mH, emptySet, solver, errorMetric);
	BOOST_CHECK(!problem3.IsValid());

	problem3.Configure(mH, observationSet, optional<MatrixXd>());
	BOOST_CHECK(problem3.IsValid());
	BOOST_CHECK(problem3.InitialEstimate().isApprox(problem2.InitialEstimate(), 1e-15));

	problem3.Configure(mH, emptySet, optional<MatrixXd>());
	BOOST_CHECK(!problem3.IsValid());

	//Operations
	VectorXd vH=Reshape<VectorXd>(mH, 9, 1); vH.normalize();
	BOOST_CHECK(vH.isApprox(problem2.InitialEstimate(),1e-6));

	VectorXd vE=problem2.ComputeError(vH);
	VectorXd vEr(observationSet.size());
	size_t cObs=0;
	errorMetric.SetProjector(mH);
	for(const auto& current : observationSet)
	{
		vEr(cObs)=errorMetric(current.left, current.right);
		++cObs;
	}	//for(const auto& current, observationSet);
	BOOST_CHECK(vE.isApprox(vEr, 1e-5));

	VectorXd vU(9); vU<<0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09;
	VectorXd vUP=problem2.Update(vH, vU);
	BOOST_CHECK(vUP.isApprox(vH+vU, 1e-6));

	typedef PDLHomography2DProblemT::model_type ModelT;
	ModelT mH2=problem2.MakeResult(vH);
	ModelT mHn(mH); mHn.normalize();
	BOOST_CHECK(mHn.isApprox(mH2,1e-6));

	double dist=problem2.ComputeRelativeModelDistance(vH, vU);
	BOOST_CHECK_CLOSE(dist, 0.153954, 1e-3);

	//Default implementations
	BOOST_CHECK(!problem2.ObservationWeights());
	BOOST_CHECK_THROW(problem2.ComputeCovariance(MatrixXd()), logic_error);
	BOOST_CHECK_THROW(problem2.SolveNormalEquations(MatrixXd(), VectorXd()), logic_error);

	//PDLHomographyXDEstimationProblemC

	optional<MatrixXd> mJ=problem2.ComputeJacobian(vH);
	BOOST_CHECK(mJ);

	errorMetric.SetProjector(mHn);
	optional<PDLHomography2DProblemT::error_type::jacobian_type> vJ=errorMetric.ComputeJacobiandP(observationSet.rbegin()->left, observationSet.rbegin()->right);
	BOOST_CHECK(vJ->cast<double>().isApprox( mJ->bottomRows(1), 1e-6));

	//Rank deficiency
	CoordinateCorrespondenceList2DT observationSet4;
	copy(observationSet.begin(), next(observationSet.begin(),8), back_inserter(observationSet4));
	PDLHomography2DProblemT problem4(mH, observationSet4, solver, errorMetric);
	optional<MatrixXd> mJinvalid=problem4.ComputeJacobian(vH);
	BOOST_CHECK(!mJinvalid);
}	//BOOST_AUTO_TEST_CASE(PDL_HomographyXD_Problem)

BOOST_AUTO_TEST_CASE(PDL_Fundamental_Matrix_Problem)
{
	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;
	CoordinateCorrespondenceList2DT observationSet;
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(2,1), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(2,3), Coordinate2DT(2.5,3), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,-1), Coordinate2DT(0,-1), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.75,2.25), Coordinate2DT(1,2.25), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.4,-1.2), Coordinate2DT(0.6,-1.2), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(11.0/6,-0.5), Coordinate2DT(2,-0.5), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,4.0/7), Coordinate2DT(8.0/7,4.0/7), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));

	//PDLFundamentalMatrixEstimationProblemC

	//Default constructor
	typedef PDLFundamentalMatrixEstimationProblemC PDLProblemT;

	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mF; mF<<3.74882e-08,2.85887e-06,-0.000385163,3.70286e-06,-1.46293e-08,0.0136915,-0.000730566,-0.0201989,0.999702;
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mF, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mF, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(12); vU.setZero(); vU[9]=0.25;
	VectorXd vUP=problem2.Update(vP, vU);
	BOOST_CHECK(vUP.isApprox( vP+vU, 1e-6));

	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), 0.0571909, 1e-4);

	PDLProblemT::model_type mFInvalid; mFInvalid.setZero();
	PDLProblemT problem3(mFInvalid, observationSet, solver, errorMetric);
	problem3.Configure(mF, observationSet, optional<MatrixXd>());
	BOOST_CHECK( vP==problem3.InitialEstimate());

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow12(12); vJRow12<<14.0384, 44.9965, 1.02348, 0.411444, -802.577, -2854.08, -57.7778, -23.1861, 1737.3, 40986.5, 34.2656, 8.6482;
	BOOST_CHECK(mJ->row(11).isApprox(vJRow12, 1e-3));

	//Rank deficiency
	CoordinateCorrespondenceList2DT observationSet4;
	observationSet4.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(2,1), MatchStrengthC(0,0)));
	problem3.Configure(mF, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem3.ComputeJacobian(vP));
}	//BOOST_AUTO_TEST_CASE(PDL_Fundamental_Matrix_Problem)

BOOST_AUTO_TEST_CASE(PDL_Essential_Matrix_Problem)
{
	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;
	CoordinateCorrespondenceList2DT observationSet;
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(2,1), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(2,3), Coordinate2DT(2.5,3), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,-1), Coordinate2DT(0,-1), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.75,2.25), Coordinate2DT(1,2.25), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.4,-1.2), Coordinate2DT(0.6,-1.2), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(11.0/6,-0.5), Coordinate2DT(2,-0.5), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,4.0/7), Coordinate2DT(8.0/7,4.0/7), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));

	//Default constructor
	typedef PDLEssentialMatrixEstimationProblemC PDLProblemT;

	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mE; mE<<0.246969, -0.507835, -0.334027, 0.358181, -0.0745203, -0.0938006, 0.551033, 0.330513, 0.11838;
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mE, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mE, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(7); vU.setZero(); vU[1]=0.05; vU[5]=0.1;
	VectorXd vUP=problem2.Update(vP, vU);
	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mE, problem2.MakeResult(vUP)), 1e-2);
/*
	PDLProblemT::model_type mEInvalid; mEInvalid.setZero();
	PDLProblemT problem3(mEInvalid, observationSet, solver, errorMetric);
	problem3.Configure(mE, observationSet, optional<MatrixXd>());
	BOOST_CHECK( vP==problem3.InitialEstimate());
*/
	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow12(7); vJRow12<<1.26984, 21.9383, -25.3184, 31.7831, 0.397842, -0.281162, -0.221256;
	BOOST_CHECK(mJ->row(11).isApprox(vJRow12, 1e-3));

	//Rank deficiency
	CoordinateCorrespondenceList2DT observationSet4;
	observationSet4.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(2,1), MatchStrengthC(0,0)));
	problem2.Configure(mE, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));

}	//BOOST_AUTO_TEST_CASE(PDL_Essential_Matrix_Problem)

BOOST_AUTO_TEST_CASE(PDL_OS_Fundamental_Matrix_Problem)
{
	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;
	CoordinateCorrespondenceList2DT observationSet;
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(2,1), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(2,3), Coordinate2DT(2.5,3), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,-1), Coordinate2DT(0,-1), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.75,2.25), Coordinate2DT(1,2.25), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.4,-1.2), Coordinate2DT(0.6,-1.2), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(11.0/6,-0.5), Coordinate2DT(2,-0.5), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,4.0/7), Coordinate2DT(8.0/7,4.0/7), MatchStrengthC(0,0)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));

	//Default constructor
	typedef PDLOneSidedFundamentalMatrixEstimationProblemC PDLProblemT;

	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mF; mF<<0.246969, -0.507835, -0.334027, 0.358181, -0.0745203, -0.0938006, 0.551033, 0.330513, 0.11838;
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mF, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mF, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(8); vU.setZero(); vU[1]=0.05; vU[5]=0.1;
	VectorXd vUP=problem2.Update(vP, vU);
	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mF, problem2.MakeResult(vUP)), 1e-2);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow12(8); vJRow12<<1.26984, 21.9383, -25.3184, 31.7831, 0.397843, -0.281163, -0.221257, -0.586605;
	BOOST_CHECK(mJ->row(11).isApprox(vJRow12, 5e-3));

	//Rank deficiency
	CoordinateCorrespondenceList2DT observationSet4;
	observationSet4.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(2,1), MatchStrengthC(0,0)));
	problem2.Configure(mF, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));
}	//PDL_OS_Fundamental_Matrix_Problem

BOOST_AUTO_TEST_CASE(PDL_Rotation_3D_Problem)
{
	//Data
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;

	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based> BimapT;
	BimapT observationSet;
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,1,2), mR*Coordinate3DT(0.11,1.05,1.99) ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,3,2), mR*Coordinate3DT(1.96,2.99,2.01) ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,6,-2), mR*Coordinate3DT(4.1,6.05,-1.99) ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-3,-3), mR*Coordinate3DT(-0.1,-2.85,3.02) ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(5,-1,4), mR*Coordinate3DT(5.06,-1.05,4.06) ) );

	typedef PDLRotation3DEstimationProblemC PDLProblemT;

	//Default constructor
	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mH; mH.setIdentity(); mH.block(0,0,3,3)=mR;
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mH, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mH, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(4); vU.setZero(); vU[1]=0.05; vU[3]=0.1;
	VectorXd vUP=problem2.Update(vP, vU);
	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mH, problem2.MakeResult(vUP)), 1e-2);

	VectorXd vE=problem2.ComputeError(vP);
	BOOST_CHECK_EQUAL(vE.size(), observationSet.size());
	BOOST_CHECK_CLOSE(vE[0], errorMetric(mH, observationSet.begin()->left, observationSet.begin()->right), 1e-3);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow5(4); vJRow5<<5.51399, -2.6681, 10.3256, 14.0214;
	BOOST_CHECK(mJ->row(4).isApprox(vJRow5, 5e-3));

	//Rank deficiency
	BimapT observationSet4;
	observationSet4.push_back(BimapT::value_type(Coordinate3DT(0,1,2), mR*Coordinate3DT(0.11,1.05,1.99) ) );
	problem2.Configure(mH, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));
}	//BOOST_AUTO_TEST_CASE(PDL_Rotation_3D_Problem)

BOOST_AUTO_TEST_CASE(PDL_Similarity_3D_Problem)
{
	//Data
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;
	Coordinate3DT vC(1, 1.2, 1.5);

	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based> BimapT;
	BimapT observationSet;
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,1,2), 2*mR*Coordinate3DT(0.11,1.05,1.99)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,3,2), 2*mR*Coordinate3DT(1.96,2.99,2.01)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,6,-2), 2*mR*Coordinate3DT(4.1,6.05,-1.99)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-3,-3), 2*mR*Coordinate3DT(-0.1,-2.85,3.02)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(5,-1,4), 2*mR*Coordinate3DT(5.06,-1.05,4.06)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(6,-1,4), 2*mR*Coordinate3DT(5.91,-1.01,4.02)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(1,0,-2), 2*mR*Coordinate3DT(1.12,-0.05,-2.03)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-1,-1), 2*mR*Coordinate3DT(-0.03,-0.97,-1.02)+vC ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(-3,3,1), 2*mR*Coordinate3DT(-2.96,3.01,0.93)+vC ) );

	typedef PDLSimilarity3DEstimationProblemC PDLProblemT;

	//Default constructor
	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mH=ComposeSimilarity3D(2, vC, mR);
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mH, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mH, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(8); vU.setZero(); vU[1]=0.05; vU[3]=0.1;
	VectorXd vUP=problem2.Update(vP, vU);
	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mH, problem2.MakeResult(vUP)), 1e-2);

	VectorXd vE=problem2.ComputeError(vP);
	BOOST_CHECK_EQUAL(vE.size(), observationSet.size());
	BOOST_CHECK_CLOSE(vE[0], errorMetric(mH, observationSet.begin()->left, observationSet.begin()->right), 1e-3);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow9(8); vJRow9<<-0.593388, 0.289467, 1.16628, 1.88573, -3.82928, 18.1972, -6.60751, 2.4438;
	BOOST_CHECK(mJ->row(8).isApprox(vJRow9, 1e-2));

	//Rank deficiency
	BimapT observationSet4;
	observationSet4.push_back(BimapT::value_type(Coordinate3DT(0,1,2), 2*mR*Coordinate3DT(0.11,1.05,1.99)+vC ) );
	problem2.Configure(mH, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));
}	//BOOST_AUTO_TEST_CASE(PDL_Similarity_3D_Problem)

BOOST_AUTO_TEST_CASE(PDL_P2P_Problem)
{
	//Data
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;

	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	BimapT observationSet;
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*Coordinate3DT(0.11,1.05,1.99)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,3,2), (mR*Coordinate3DT(1.96,2.99,2.01)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,6,-2), (mR*Coordinate3DT(4.1,6.05,-1.99)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-3,-3), (mR*Coordinate3DT(-0.1,-2.85,3.02)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(5,-1,4), (mR*Coordinate3DT(5.06,-1.05,4.06)).hnormalized() ) );

	typedef PDLP2PProblemC PDLProblemT;

	//Default constructor
	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mP; mP.setZero(); mP.block(0,0,3,3)=mR;
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mP, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mP, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(4); vU.setZero(); vU[1]=0.05; vU[3]=0.1;
	VectorXd vUP=problem2.Update(vP, vU);
	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mP, problem2.MakeResult(vUP)), 1e-2);

	VectorXd vE=problem2.ComputeError(vP);
	BOOST_CHECK_EQUAL(vE.size(), observationSet.size());
	BOOST_CHECK_CLOSE(vE[0], errorMetric(mP, observationSet.begin()->left, observationSet.begin()->right), 1e-3);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow5(4); vJRow5<<34.8843, -77.3933,  44.3995, -28.6193;
	BOOST_CHECK(mJ->row(4).isApprox(vJRow5, 5e-3));

	//Rank deficiency
	BimapT observationSet4;
	observationSet4.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*Coordinate3DT(0.11,1.05,1.99) ).hnormalized()) );
	problem2.Configure(mP, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));
}	//BOOST_AUTO_TEST_CASE(PDL_P2P_Problem)

BOOST_AUTO_TEST_CASE(PDL_P2Pf_Problem)
{
	//Data
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;
	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity();
	mK(0,0)=5; mK(1,1)=5;

	MatrixXd mKR=mK*mR;

	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	BimapT observationSet;
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mKR*Coordinate3DT(0.11,1.05,1.99)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,3,2), (mKR*Coordinate3DT(1.96,2.99,2.01)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,6,-2), (mKR*Coordinate3DT(4.1,6.05,-1.99)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-3,-3), (mKR*Coordinate3DT(-0.1,-2.85,3.02)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(5,-1,4), (mKR*Coordinate3DT(5.06,-1.05,4.06)).hnormalized() ) );

	typedef PDLP2PfProblemC PDLProblemT;

	//Default constructor
	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mP; mP.setZero(); mP.block(0,0,3,3)=mKR;
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mP, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides

	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mP, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(5); vU.setZero(); vU[0]=1; vU[2]=0.05; vU[4]=0.1;
	VectorXd vUP=problem2.Update(vP, vU);

	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mP, problem2.MakeResult(vUP)), 1e-2);

	VectorXd vE=problem2.ComputeError(vP);
	BOOST_CHECK_EQUAL(vE.size(), observationSet.size());
	BOOST_CHECK_CLOSE(vE[0], errorMetric(mP, observationSet.begin()->left, observationSet.begin()->right), 2e-3);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow5(5); vJRow5<<-6.94899 , 174.42, -386.967,  221.997, -143.096;
	BOOST_CHECK(mJ->row(4).isApprox(vJRow5, 5e-3));

	//Rank deficiency
	BimapT observationSet4;
	observationSet4.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*Coordinate3DT(0.11,1.05,1.99) ).hnormalized()) );
	problem2.Configure(mP, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));
}	//BOOST_AUTO_TEST_CASE(PDL_P2Pf_Problem)

BOOST_AUTO_TEST_CASE(PDL_P3P_Problem)
{
	//Data
	Coordinate3DT vC(2,1,3);
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;

	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	BimapT observationSet;
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*(Coordinate3DT(0.11,1.05,1.99)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,3,2), (mR*(Coordinate3DT(1.96,2.99,2.01)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,6,-2), (mR*(Coordinate3DT(4.1,6.05,-1.99)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-3,-3), (mR*(Coordinate3DT(-0.1,-2.85,3.02)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,0,5), (mR*(Coordinate3DT(2.1,0.07,5.03)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(6,3,1), (mR*(Coordinate3DT(6.01,2.97,0.96)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(5,-1,4), (mR*(Coordinate3DT(5.06,-1.05,4.06)-vC)).hnormalized() ) );

	typedef PDLP3PProblemC PDLProblemT;

	//Default constructor
	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mP=ComposeCameraMatrix(vC, mR);
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mP, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides
	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mP, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(7); vU.setZero(); vU[0]=1; vU[2]=0.05; vU[4]=0.1; vU[6]=-0.2;
	VectorXd vUP=problem2.Update(vP, vU);

	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mP, problem2.MakeResult(vUP)), 1e-2);

	VectorXd vE=problem2.ComputeError(vP);
	BOOST_CHECK_EQUAL(vE.size(), observationSet.size());
	BOOST_CHECK_CLOSE(vE[0], errorMetric(mP, observationSet.begin()->left, observationSet.begin()->right), 5e-3);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow7(7); vJRow7<<0.0996562, 0.913949, 1.52893, -2.32612, 10.8128, -6.24721, 4.10908;
	BOOST_CHECK(mJ->row(6).isApprox(vJRow7, 5e-3));

	//Rank deficiency
	BimapT observationSet4;
	observationSet4.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*(Coordinate3DT(0.11,1.05,1.99)-vC)).hnormalized()) );
	problem2.Configure(mP, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));

}	//BOOST_AUTO_TEST_CASE(PDL_P3P_Problem)

BOOST_AUTO_TEST_CASE(PDL_P4P_Problem)
{
	//Data
	Coordinate3DT vC(2,1,3);
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;

	typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based> BimapT;
	BimapT observationSet;
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*(Coordinate3DT(0.11,1.05,1.99)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,3,2), (mR*(Coordinate3DT(1.96,2.99,2.01)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,6,-2), (mR*(Coordinate3DT(4.1,6.05,-1.99)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(0,-3,-3), (mR*(Coordinate3DT(-0.1,-2.85,3.02)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(2,0,5), (mR*(Coordinate3DT(2.1,0.07,5.03)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(6,3,1), (mR*(Coordinate3DT(6.01,2.97,0.96)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(5,-1,4), (mR*(Coordinate3DT(5.06,-1.05,4.06)-vC)).hnormalized() ) );
	observationSet.push_back(BimapT::value_type(Coordinate3DT(4,-2,7), (mR*(Coordinate3DT(3.91,-2.01,7.03)-vC)).hnormalized() ) );

	typedef PDLP4PProblemC PDLProblemT;

	//Default constructor
	PDLProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor
	PDLProblemT::model_type mP=ComposeCameraMatrix(vC, mR);
	PDLProblemT::solver_type solver;
	PDLProblemT::error_type errorMetric;
	PDLProblemT problem2(mP, observationSet, solver, errorMetric, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Overrides
	VectorXd vP=problem2.InitialEstimate();
	BOOST_CHECK_SMALL(solver.ComputeDistance(mP, problem2.MakeResult(vP)), 1e-4);

	VectorXd vU(8); vU.setZero(); vU[0]=1; vU[1]=1; vU[3]=0.05; vU[5]=0.1; vU[7]=-0.2;
	VectorXd vUP=problem2.Update(vP, vU);

	BOOST_CHECK_CLOSE(problem2.ComputeRelativeModelDistance(vP, vU), solver.ComputeDistance(mP, problem2.MakeResult(vUP)), 1e-2);

	VectorXd vE=problem2.ComputeError(vP);
	BOOST_CHECK_EQUAL(vE.size(), observationSet.size());
	BOOST_CHECK_CLOSE(vE[0], errorMetric(mP, observationSet.begin()->left, observationSet.begin()->right), 5e-3);

	//Jacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(vP);
	BOOST_CHECK(mJ);

	RowVectorXd vJRow7(8); vJRow7<<-2.28316, 0.0996562, 0.913949, 1.52893, -2.32612, 10.8128, -6.24721, 4.10908;
	BOOST_CHECK(mJ->row(6).isApprox(vJRow7, 5e-3));

	//Rank deficiency
	BimapT observationSet4;
	observationSet4.push_back(BimapT::value_type(Coordinate3DT(0,1,2), (mR*(Coordinate3DT(0.11,1.05,1.99)-vC)).hnormalized()) );
	problem2.Configure(mP, observationSet4, optional<MatrixXd>());
	BOOST_CHECK(!problem2.ComputeJacobian(vP));

}	//PDL_P4P_Problem

BOOST_AUTO_TEST_CASE(PDL_KRK)
{

	IntrinsicCalibrationMatrixT mK1=IntrinsicCalibrationMatrixT::Identity(); mK1(0,0)=2200; mK1(1,1)=2200;
	IntrinsicCalibrationMatrixT mK2=IntrinsicCalibrationMatrixT::Identity(); mK2(0,0)=1700; mK2(1,1)=1700;
	RotationMatrix3DT mR; mR<<-0.976192, -0.031164, -0.214660, -0.145397, -0.640368, 0.75418, -0.160964, 0.767436, 0.620591;

	Homography2DT mH = mK2 * mR * mK1.inverse();
	//mH+= 0.000001*mH.transpose();

	CoordinateCorrespondenceList2DT observations;
	observations.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(250, 115), Coordinate2DT(-865.91, 1863.45), MatchStrengthC(1,0) ) );
	observations.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(1023, 534), Coordinate2DT(-1570.26,  1233.48), MatchStrengthC(1,0) ) );
	observations.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(714, 389), Coordinate2DT(-1296.62,  1433.71), MatchStrengthC(1,0) ) );
	observations.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(1120, 1203), Coordinate2DT(-1292.65,  585.406), MatchStrengthC(1,0) ) );
	observations.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(300, 1830), Coordinate2DT(-513.569,   277.17), MatchStrengthC(1,0) ) );
	observations.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(500, 945), Coordinate2DT(-837.124,  829.979), MatchStrengthC(1,0) ) );

	optional<MatrixXd> mW;

	//Constructors

	PDLHomographyKRKDecompositionProblemC problem1;
	BOOST_CHECK(!problem1.IsValid());

	problem1.Configure(mH, observations, mW);
	BOOST_CHECK(problem1.IsValid());

	PDLHomographyKRKDecompositionProblemC problem2(mH, observations, mW);
	BOOST_CHECK(problem2.IsValid());

	problem2.Configure(mH, CoordinateCorrespondenceList2DT(), mW);
	BOOST_CHECK(!problem2.IsValid());

	problem2.Configure(Homography2DT::Zero(), observations, mW);
	BOOST_CHECK(!problem2.IsValid());

	//Operation

	//Accessors
	VectorXd vInitial=problem1.InitialEstimate();

	VectorXd vInitialT(6);
	RotationMatrix3DT mRInitial;
	tie(vInitialT[0],vInitialT[1],mRInitial)=*DecomposeK2RK1i(mH);
	vInitialT.tail(4)=QuaternionToVector(QuaternionT(mRInitial));
	vInitialT.tail(4)*= vInitialT[2]<0 ? -1 : 1;

	BOOST_CHECK(vInitial.isApprox(vInitialT, 1e-8));

	BOOST_CHECK(!problem1.ObservationWeights());

	//MakeResult
	Homography2DT mHRecomposed=problem1.MakeResult(vInitial);
	IntrinsicCalibrationMatrixT mKInitial1=IntrinsicCalibrationMatrixT::Identity(); mKInitial1(0,0)=vInitial[0]; mKInitial1(1,1)=vInitial[0];
	IntrinsicCalibrationMatrixT mKInitial2=IntrinsicCalibrationMatrixT::Identity(); mKInitial2(0,0)=vInitial[1]; mKInitial2(1,1)=vInitial[1];
	BOOST_CHECK(mHRecomposed.isApprox( mKInitial2*mRInitial*mKInitial1.inverse(), 1e-8 ));

	//Update
	VectorXd vU(6); vU<<100, 80, 0.01, 0.0025, 0.0025, 0.0012;
	VectorXd vUpdated=problem1.Update(vInitial, vU);

	VectorXd vUpdatedT(6);
	vUpdatedT[0]=vInitialT[0]+vU[0];
	vUpdatedT[1]=vInitialT[1]+vU[1];

	QuaternionT qUpdated;
	qUpdated.w()=vInitialT[2]+vU[2];
	qUpdated.vec()=vInitialT.tail(3)+vU.tail(3);
	qUpdated.normalize();
	vUpdatedT.tail(4)=QuaternionToVector(qUpdated);
	vUpdatedT.tail(4) *= vUpdatedT[2]<0 ? -1 : 1;

	BOOST_CHECK(vUpdatedT.isApprox(vUpdated, 1e-8));

	//ComputeModelDistance

	Homography2DT mHUpdated=problem1.MakeResult(vUpdated);
	BOOST_CHECK_CLOSE( (mHUpdated.normalized()-mHRecomposed.normalized()).norm(), problem1.ComputeRelativeModelDistance(vInitial, vU), 1e-2 );

	VectorXd vZero(6); vZero.setZero();
	BOOST_CHECK_SMALL(problem1.ComputeRelativeModelDistance(vInitial, vZero), 1e-8 );

	//Error
	VectorXd errorVector=problem1.ComputeError(vInitial);

	VectorXd errorVectorT(6);
	SymmetricTransferErrorH2DT errorMetric(mHRecomposed);
	size_t index=0;
	for(const auto& current : observations)
	{
		errorVectorT[index]=errorMetric(current.left, current.right);
		++index;
	}	//for(const auto& current : observations)

	BOOST_CHECK(errorVector==errorVectorT);

	//Jacobian
	optional<MatrixXd> mJ=problem1.ComputeJacobian(vInitial);
	BOOST_CHECK(mJ);

	MatrixXd mJr(6,6);
	mJr<<     0.173686,    0.830566,       -2807,    -2711.69,    -6447.64,     2616.99,
			  0.0989294,    -1.37833,     2633.83,    -6396.12,     5614.09,    -3288.12,
			 -0.0119974,    -1.32653,     1961.08,    -5117.35,     6355.45,    -3511.95,
			  -0.412689,     1.03044,    -1075.18,     7872.35,    -322.038,      1026.9,
			  -0.123966,   -0.585856,     2055.82,    -6387.61,     1125.01,    -1197.45,
			-0.00586173,   -0.836812,     712.774,    -6251.47,     2166.77,    -1718.73;


	BOOST_CHECK(mJ->isApprox(mJr, 1e-2));
}	//BOOST_AUTO_TEST_CASE(PDL_KRK)

BOOST_AUTO_TEST_SUITE_END()	//PDL_Geometry_Problems

BOOST_AUTO_TEST_SUITE(PDL_SfM)

BOOST_AUTO_TEST_CASE(PDL_Pose_Graph)
{
	//Problem
	vector<QuaternionT> quaternions(3);
	quaternions[0]=QuaternionT(1,0,0,0);
	quaternions[1]=QuaternionT(0.5, 0.5, 0.25, 0.25); quaternions[1].normalize();
	quaternions[2]=QuaternionT(0.25, 0.75, 0.5, 0.25); quaternions[2].normalize();

	vector<Coordinate3DT> coordinates{Coordinate3DT(0,0,0), Coordinate3DT(1,2,-1), Coordinate3DT(3,-1,1)};

	PDLPoseGraphProblemC::result_type initialEstimate;
	for(size_t c=0; c<3; ++c)
		initialEstimate.emplace(c, Viewpoint3DC(coordinates[c], quaternions[c]));

	typedef pair<unsigned int, unsigned int> IndexPairT;
	PDLPoseGraphProblemC::observation_set_type observations;
	for(size_t c1=0; c1<3; ++c1)
		for(size_t c2=0; c2<3; ++c2)
		{
			if(c1==c2)
				continue;

			Viewpoint3DC relativePose=ComputeRelativePose(initialEstimate[c1], initialEstimate[c2]);
			relativePose.position+=0.1*initialEstimate[c2].position;

			QuaternionT rq(1, 0.01*initialEstimate[c2].position[1], 0.01*initialEstimate[c2].position[1], 0.01 );
			rq.normalize();
			relativePose.orientation*=rq;

			Matrix<double,6,6> mCov; mCov.setIdentity(); mCov.block(0,0,3,3)*=0.04; mCov.block(3,3,3,3)*=0.00001;

			PDLPoseGraphProblemC::pose_type pose(relativePose, mCov );
			observations.emplace(IndexPairT(c1,c2), pose);
		}	//for(size_t c2=0; c2<3; ++c2)

	//Constructors
	PDLPoseGraphProblemC defaultProblem;
	BOOST_CHECK(!defaultProblem.IsValid());

	PDLPoseGraphProblemC problem1(initialEstimate, observations, optional<MatrixXd>());
	BOOST_CHECK(problem1.IsValid());

	PDLPoseGraphProblemC problem2;

	initialEstimate[3]=initialEstimate[1];
	BOOST_CHECK_THROW(problem2.Configure(initialEstimate, observations, optional<MatrixXd>()), invalid_argument);
	initialEstimate.erase(3);

	observations.emplace(IndexPairT(3,1), PDLPoseGraphProblemC::pose_type());
	BOOST_CHECK_THROW(problem2.Configure(initialEstimate, observations, optional<MatrixXd>()), invalid_argument);
	observations.erase(IndexPairT(3,1));

	problem2.Configure(initialEstimate, observations, optional<MatrixXd>());
	BOOST_CHECK(problem2.IsValid());

	//Accessors

	VectorXd initialEstimateV=problem2.InitialEstimate();
	BOOST_CHECK_EQUAL(initialEstimateV.size(), 21);
	BOOST_CHECK(coordinates[0]==initialEstimateV.segment(0,3));
	BOOST_CHECK(quaternions[0].isApprox(QuaternionT(initialEstimateV[3],initialEstimateV[4],initialEstimateV[5],initialEstimateV[6]), 1e-12));
	BOOST_CHECK(coordinates[1]==initialEstimateV.segment(7,3));
	BOOST_CHECK(quaternions[1].isApprox(QuaternionT(initialEstimateV[10],initialEstimateV[11],initialEstimateV[12],initialEstimateV[13]), 1e-12));
	BOOST_CHECK(coordinates[2]==initialEstimateV.segment(14,3));
	BOOST_CHECK(quaternions[2].isApprox(QuaternionT(initialEstimateV[17],initialEstimateV[18],initialEstimateV[19],initialEstimateV[20]), 1e-12));

	BOOST_CHECK(!problem2.ObservationWeights());

	//Operations

	//MakeResult
	PDLPoseGraphProblemC::result_type model=problem2.MakeResult(initialEstimateV);
	BOOST_CHECK(model[0].position==initialEstimate[0].position);
	BOOST_CHECK(model[0].orientation.isApprox(initialEstimate[0].orientation,1e-12));
	BOOST_CHECK(model[1].position==initialEstimate[1].position);
	BOOST_CHECK(model[1].orientation.isApprox(initialEstimate[1].orientation,1e-12));
	BOOST_CHECK(model[2].position==initialEstimate[2].position);
	BOOST_CHECK(model[2].orientation.isApprox(initialEstimate[2].orientation,1e-12));

	//ComputeError
	VectorXd error=problem2.ComputeError(initialEstimateV);

	Viewpoint3DC trueObservation12=ComputeRelativePose(initialEstimate[1], initialEstimate[2]);
	Viewpoint3DC observationError=ComputeRelativePose(get<PDLPoseGraphProblemC::iViewpoint>(observations.find(IndexPairT(1,2))->second), trueObservation12);
	VectorXd observationErrorV(6); observationErrorV.head(3)=observationError.position; observationErrorV.tail(3)=QuaternionToRotationVector(observationError.orientation);
	double errorGT=observationErrorV.transpose() * get<PDLPoseGraphProblemC::iCovariance>(observations.find(IndexPairT(1,2))->second).inverse() * observationErrorV;

	BOOST_CHECK_EQUAL(errorGT, error[3]);

	//ComputeJacobian
	optional<MatrixXd> mJ=problem2.ComputeJacobian(initialEstimateV);

	MatrixXd mJr(6,21);
	mJr<<	         -11,            5,            2,       485.77,     -22393.3,     -7997.6,     -3199.04,           11,           -5,           -2,     -17358.3,      12933.6,      10288.8,      115.667,            0,            0,            0,            0,            0,            0,            0,
    -1,          -15,           -7,      170.974,      9065.76,      -2666.4,     10132.3,            0,            0,            0,            0,            0,            0,            0,            1,           15,            7,     8334.09,     -8090.37,      6186.33,      4187.82,
     0,            0,            0,      53.3305, -8.06037e-12, -7.98088e-12,      7999.73,            0,            0,            0,      2563.47,     -2496.01,      5076.34,     -5042.61,            0,            0,            0,            0,            0,            0,            0,
     0,            0,            0,            0,            0,            0,            0,           -1,          -15,           -7,     -7986.49,      9889.13,     -5173.71,      1909.08,            1,           15,            7,      8365.07,     -8160.09,      6170.84,      4211.06,
     0,            0,            0,      53.3305,  1.06639e-11, -2.91831e-11,      7999.73,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,      2079.29,     -4089.73,      6224.11,     -2051.75,
     0,            0,            0,            0,            0,            0,           0,           11,           -5,           -2,     -17386.7,      12892.5,      10235.1,      80.8815,          -11,            5,            2,      22427.1,     -4992.65,     -5118.08,      4668.36;

	//Update
	VectorXd vU=initialEstimateV;
	vU[4]=0.1; vU[11]=0.1; vU[18]=0.1;

	VectorXd vNew=problem2.Update(initialEstimateV, vU);
	PDLPoseGraphProblemC::result_type updated=problem2.MakeResult(vNew);
	BOOST_CHECK(updated[0].position==2*initialEstimate[0].position);
	BOOST_CHECK(updated[0].orientation.isApprox(QuaternionT(2*quaternions[0].w(), quaternions[0].x()+0.1, 2*quaternions[0].y(), 2*quaternions[0].z()).normalized(), 1e-6) );
	BOOST_CHECK(updated[1].position==2*initialEstimate[1].position);
	BOOST_CHECK(updated[1].orientation.isApprox(QuaternionT(2*quaternions[1].w(), quaternions[1].x()+0.1, 2*quaternions[1].y(), 2*quaternions[1].z()).normalized(), 1e-6) );
	BOOST_CHECK(updated[2].position==2*initialEstimate[2].position);
	BOOST_CHECK(updated[2].orientation.isApprox(QuaternionT(2*quaternions[2].w(), quaternions[2].x()+0.1, 2*quaternions[2].y(), 2*quaternions[2].z()).normalized(), 1e-6) );

	//ComputeRelativeDistance
	double relativeDistance=problem2.ComputeRelativeModelDistance(initialEstimateV, vU);
	double relativeDistanceT=(vNew-initialEstimateV).norm()/max(vNew.norm(), initialEstimateV.norm());
	BOOST_CHECK_EQUAL(relativeDistance, relativeDistanceT);

}	//BOOST_AUTO_TEST_CASE(PDL_Pose_Graph)

BOOST_AUTO_TEST_SUITE_END()	//PDL_SfM


BOOST_AUTO_TEST_SUITE(PDL_Synchronisation)

BOOST_AUTO_TEST_CASE(PDL_Frame_Alignment)
{
	//Default constructor
	PDLFrameAlignmentProblemC problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor

	EpipolarMatrixT mF; mF<<0.941524, -0.108497, 0.274965, -0.144663, 0.023072, -0.021805,  0.013674, 0.066155, 0.220270;

	CoordinateCorrespondenceList2DT correspondenceList;
	correspondenceList.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(2,1), Coordinate2DT(3,-1)));
	correspondenceList.push_back( CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(0,3), Coordinate2DT(-2,4)));

	vector<Coordinate2DT> forward1{Coordinate2DT(1,1), Coordinate2DT(2,0.6) };
	vector<Coordinate2DT> backward1{Coordinate2DT(-1,-1), Coordinate2DT(-2,-0.6) };
	vector<Coordinate2DT> forward2{Coordinate2DT(2,3), Coordinate2DT(1,2) };
	vector<Coordinate2DT> backward2{Coordinate2DT(4,1), Coordinate2DT(1,4) };

	PDLFrameAlignmentProblemC::model_type initialEstimate{{0,0}};
	PDLFrameAlignmentProblemC problem2(initialEstimate, correspondenceList, optional<MatrixXd>(), mF, forward1, backward1, forward2, backward2);

	BOOST_CHECK(problem2.IsValid());

	//Operations

	BOOST_CHECK(problem2.InitialEstimate()[0]==0);
	BOOST_CHECK(problem2.InitialEstimate()[1]==0);

	VectorXd vP0(2); vP0[0]=-0.1; vP0[1]=0.2;
	PDLFrameAlignmentProblemC::model_type mP0=problem2.MakeResult(vP0);
	BOOST_CHECK(mP0[0]==vP0[0]);
	BOOST_CHECK(mP0[1]==vP0[1]);

	VectorXd vP1(2); vP1.setZero();
	VectorXd vU2(2); vU2<<0.1, -0.2;
	BOOST_CHECK_EQUAL(problem2.ComputeRelativeModelDistance(vP1, vU2), vU2.norm());

	//Jacobian

	optional<MatrixXd> vJ1=problem2.ComputeJacobian(vP1);
	MatrixXd mJ1r(2,2); mJ1r<< 0.498024, -0.183814, -1.8803, -0.00221278;


	VectorXd vP2(vU2);
	optional<MatrixXd> mJ2=problem2.ComputeJacobian(vP2);
	MatrixXd mJ2r(2,2); mJ2r<< 0.579576, -0.436162, -1.8849, -0.105189;

	//Error
	VectorXd vE1=problem2.ComputeError(vP1);

	EpipolarSampsonErrorC error(mF);
	BOOST_CHECK_CLOSE(vE1[0], error(correspondenceList.begin()->left, correspondenceList.begin()->right), 1e-8);
	BOOST_CHECK_CLOSE(vE1[1], error(correspondenceList.rbegin()->left, correspondenceList.rbegin()->right), 1e-8);

	VectorXd vE2r(2); vE2r<<2.01412, 0.118141;
	VectorXd vE2=problem2.ComputeError(vP2);
	BOOST_CHECK(vE2r.isApprox(vE2, 1e-4));
}	//BOOST_AUTO_TEST_CASE(PDL_Frame_Alignment)

BOOST_AUTO_TEST_SUITE_END()	//PDL_Synchronisation

BOOST_AUTO_TEST_SUITE(Powells_Dog_Leg)

BOOST_AUTO_TEST_CASE(Powells_Dog_Leg_Operation)
{
	//Model: 2x+3, y+5
	CoordinateCorrespondenceList2DT observationSet;
	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;

	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(5, 6), MatchStrengthC(0.1, 0.1)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.3)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.35)));

	//Noisy inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));

	//Problem
	typedef PDLHomographyXDEstimationProblemC<Homography2DSolverC> PDLHomography2DProblemT;
	PDLHomography2DProblemT::model_type mH; mH<<2.1, 0.05, 2.9, 0.01, 0.97, 4.85, 0.001, -0.002, 1.0003;
	PDLHomography2DProblemT::solver_type solver;
	PDLHomography2DProblemT::error_type errorMetric;
	PDLHomography2DProblemT problem1(mH, observationSet, solver, errorMetric);

	//Operation
	typedef PowellDogLegC<PDLHomography2DProblemT> PDLHomography2DEngineT;
	PowellDogLegParametersC parameters1;
	PDLHomography2DProblemT::result_type mHres;
	optional<MatrixXd> mCov;
	PowellDogLegDiagnosticsC diagnostics1=PDLHomography2DEngineT::Run(mHres, mCov, problem1, parameters1);

	BOOST_CHECK(diagnostics1.flagSuccess);
	BOOST_CHECK(diagnostics1.nIterations<=parameters1.maxIteration);
	BOOST_CHECK_CLOSE(diagnostics1.error, 2.18401, 2e-3);

	PDLHomography2DProblemT::model_type mHr; mHr<< 	   0.348892, 0.00285204, 0.433454, 0.00188728, 0.176177, 0.793493, 4.83717e-06, 9.42192e-05, 0.172368;
	BOOST_CHECK(mHres.isApprox(mHr, 1e-5));

	BOOST_CHECK(!mCov);

/*	parameters1.flagCovariance=true;
	PowellDogLegDiagnosticsC diagnostics2=PDLHomography2DEngineT::Run(mHres, mCov, problem1, parameters1);
	BOOST_CHECK(mCov);
	BOOST_CHECK_CLOSE(mCov->trace(), 1068516777104.2974, 1e-3); //TODO This value does not seem to be stable. Changes with every update to Eigen. Probably ill-conditioned
*/
	//Errors
	PDLHomography2DProblemT problem3;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem3, parameters1), invalid_argument);

	PowellDogLegParametersC brokenParameters;

	brokenParameters.epsilon1=-1;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem1, brokenParameters), invalid_argument);
	brokenParameters.epsilon1=1;

	brokenParameters.epsilon2=-1;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem1, brokenParameters), invalid_argument);
	brokenParameters.epsilon2=1;

	brokenParameters.delta0=-1;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem1, brokenParameters), invalid_argument);
	brokenParameters.delta0=1;

	brokenParameters.lambdaGrowth=-1;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem1, brokenParameters), invalid_argument);
	brokenParameters.lambdaGrowth=2;

	brokenParameters.lambdaShrink=-1;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem1, brokenParameters), invalid_argument);
	brokenParameters.lambdaShrink=2;
	BOOST_CHECK_THROW(PDLHomography2DEngineT::Run(mHres, mCov, problem1, brokenParameters), invalid_argument);
	brokenParameters.lambdaShrink=0.5;

}	//Powells_Dog_Leg

BOOST_AUTO_TEST_SUITE_END()	//Powells_Dog_Leg
}	//TestOptimisationN
}	//UnitTestsN
}	//SeeSawN

