/**
 * @file PDLHomographyKRKDecompositionProblem.cpp Implementation of the KRK decomposition problem
 * @author Evren Imre
 * @date 15 Jun 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLHomographyKRKDecompositionProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLHomographyKRKDecompositionProblemC::PDLHomographyKRKDecompositionProblemC() : flagValid(false)
{}

/**
 * @brief Configures the problem
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] oobservations Constraints
 * @param[in] mmW Weight matrix
 * @post \c flagValid is true if there are at least 6 constraints, and \c mW is either invalid, or is a square matrix with as many rows as the number of constraints
 */
void PDLHomographyKRKDecompositionProblemC::Configure(const Homography2DT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& mmW)
{
	size_t nObservations=oobservations.size();
	flagValid= (nObservations>=nDoF);
	flagValid = flagValid && (!mmW || (mmW && mmW->rows()==(int)nObservations && mmW->cols()==(int)nObservations));
	flagValid = flagValid && !!DecomposeK2RK1i(iinitialEstimate);

	if(!flagValid)
		return;

	//Decompose the initial estimate, and recompose as an exactly KRK-decomposable matrix
	RealT f1;
	RealT f2;
	RotationMatrix3DT mR;
	tie(f1, f2, mR)=*DecomposeK2RK1i(iinitialEstimate);

	Matrix3d mK1=Matrix3d::Identity(); mK1(0,0)=f1; mK1(1,1)=f1;
	Matrix3d mK2=Matrix3d::Identity(); mK2(0,0)=f2; mK2(1,1)=f2;

	initialEstimate= mK2*mR*mK1.inverse();
	observations=oobservations;

	if(mmW)
		mW=mmW;
}	//void Configure(const Homography2DT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& mmW)

/**
 * @brief Returns the initial estimate vector
 * @return Converts the initial estimate to a vector
 * @pre The object is valid
 */
VectorXd PDLHomographyKRKDecompositionProblemC::InitialEstimate() const
{
	//Preconditions
	assert(flagValid);

	RealT f1;
	RealT f2;
	RotationMatrix3DT mR;
	tie(f1, f2, mR)=*DecomposeK2RK1i(initialEstimate);	//The decomposition is verified in Configure
	QuaternionT q(mR);

	if(q.w()<0)
	{
		q.w()*=-1;
		q.vec()=-q.vec();
	}

	VectorXd output(6);
	output[0]=f1;
	output[1]=f2;
	output.tail(4)=QuaternionToVector(q);

	return output;
}	//VectorXd InitialEstimate() const

/**
 * @brief Returns a constant reference to \c mW
 * @return A constant reference to \c mW
 * @pre The object is valid
 */
const optional<MatrixXd>& PDLHomographyKRKDecompositionProblemC::ObservationWeights() const
{
	//Preconditions
	assert(flagValid);

	return mW;
}	//const optional<MatrixXd>& ObservationWeights() const

/**
 * @brief Computes the error vector
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre The object is valid
 */
VectorXd PDLHomographyKRKDecompositionProblemC::ComputeError(const VectorXd& vP) const
{
	//Preconditions
	assert(flagValid);

	SymmetricTransferErrorH2DT error(MakeResult(vP));

	VectorXd output(observations.size());
	size_t c=0;
	for(const auto& current : observations)
	{
		output[c]=error(current.left, current.right);
		++c;
	}	//for(const auto& current : observations)

	return output;
}	//VectorXd ComputeError(const VectorXd& vP) const

/**
 * @brief Computes the Jacobian at the current solution
 * @param[in] vP Current solution
 * @return Jacobian
 * @pre The object is valid
 */
optional<MatrixXd> PDLHomographyKRKDecompositionProblemC::ComputeJacobian(const VectorXd& vP) const
{

	//Preconditions
	assert(flagValid);

	RealT f1=vP[0];
	RealT f2=vP[1];
	RealT fRatio=f2/f1;

	QuaternionT q;
	q.w()= vP[2]<0 ? -vP[2] : vP[2];
	q.vec()= ((vP[2]<0) ? -1 : 1) * vP.tail(3);	//WARNING: q.vec()= vP[2]<0 ? -vp.tail(3) : vp.tail(3) returns an incorrect output!

	RotationMatrix3DT mR=q.matrix();

	typedef Matrix<RealT,9,1> Col9T;
	Col9T vR=Reshape< Col9T >(mR, 9, 1);

	//dHdf1
	Col9T dHdf1=vR;
	dHdf1.head(6)*=(-fRatio/f1);
	dHdf1.tail(3)/=-pow<2>(f1);
	dHdf1[2]=0; dHdf1[5]=0; dHdf1[8]=0;

	//dHdf2
	Col9T dHdf2=vR;
	dHdf2.tail(3).setZero();
	dHdf2.head(2)/=f1;
	dHdf2.segment(3,2)/=f1;

	//dHdR
	Matrix<RealT,9,9> dHdR=Matrix<RealT,9,9>::Identity();
	dHdR.diagonal()<< fRatio, fRatio, f2, fRatio, fRatio, f2, 1/f1, 1/f1, 1;

	//dRdq
	Matrix<RealT,9,4> dRdq=JacobianQuaternionToRotationMatrix(q);

	//dHdq
	Matrix<RealT,9,4> dHdq=dHdR * dRdq;

	optional<MatrixXd> output;
	if(observations.size() < nDoF)
		return output;

	SymmetricTransferErrorH2DT errorMetric(MakeResult(vP));

	output=MatrixXd(observations.size(), 6);
	output->setZero();

	size_t c=0;
	size_t cSuccess=0;
	for(const auto& current : observations)
	{
		optional<SymmetricTransferErrorH2DT::jacobian_type> dedH=errorMetric.ComputeJacobiandP(current.left, current.right);

		if(dedH)
		{
			MatrixXd tmpF1=((*dedH) * dHdf1);
			output->row(c)[0] =tmpF1(0,0);

			MatrixXd tmpF2=((*dedH) * dHdf2);
			output->row(c)[1] = tmpF2(0,0);

			output->row(c).tail(4) = (*dedH) * dHdq;
			++cSuccess;

		}	//if(dedH)

		++c;
	}	//for(const auto& current : correspondences)

	//If the number of non-zero rows is less than number of parameters, (J^T)J is rank deficient
	if(cSuccess<nDoF)
		return output=optional<MatrixXd>();

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 */
VectorXd PDLHomographyKRKDecompositionProblemC::Update(const VectorXd& vP, const VectorXd& vU)
{
	VectorXd vNew=vP+vU;
	vNew.tail(4) = vNew.tail(4).normalized();
	vNew.tail(4) *= vNew[2]<0 ? -1 : 1;

	//Non-positive f is not permissible
	if(vNew[0]<=0)
		vNew[0]=vP[0];

	if(vNew[1]<=0)
		vNew[1]=vP[1];

	return vNew;
}	//VectorXd PDLRotation3DEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the model-space distance after an update
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Frobenious norm between the normalised matrices for the current and the updated solution
 */
auto PDLHomographyKRKDecompositionProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) -> RealT
{
	Homography2DT mHP=MakeResult(vP).normalized();
	Homography2DT mHPU=MakeResult(vP+vU).normalized();

	return (mHP-mHPU).norm();
}	//RealT ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Converts a parameter vector to a model
 * @param[in] vP Parameter vector
 * @return Homography
 */
Homography2DT PDLHomographyKRKDecompositionProblemC::MakeResult(const VectorXd& vP)
{
	Matrix3d mK1i=Matrix3d::Identity();
	mK1i(0,0)=1/vP[0]; mK1i(1,1)=mK1i(0,0);

	Matrix3d mK2=Matrix3d::Identity();
	mK2(0,0)=vP[1]; mK2(1,1)=mK2(0,0);

	QuaternionT q;
	q.w()=vP[2];
	q.vec()=vP.tail(3);
	RotationMatrix3DT mR=q.matrix();

	return mK2*mR*mK1i;
}	//Homography2DT MakeResult(const VectorXd& vP)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool PDLHomographyKRKDecompositionProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid() const

/**
 * @brief Dummy
 */
void PDLHomographyKRKDecompositionProblemC::InitialiseIteration()
{}

/**
 * @brief Dummy
 */
void PDLHomographyKRKDecompositionProblemC::FinaliseIteration()
{}

/**
 * @brief Constructor
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] oobservations Constraints
 * @param[in] mmW Weight matrix
 * @post The object is valid if Configure succeeds
 */
PDLHomographyKRKDecompositionProblemC::PDLHomographyKRKDecompositionProblemC(const Homography2DT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& mmW)
{
	Configure(iinitialEstimate, oobservations, mmW);
}	//PDLHomographyKRKDecompositionProblemC(const Homography2DT& iinitialEstimate, const CoordinateCorrespondenceList2DT& oobservations, const optional<MatrixXd>& mmW)


}	//OptimisationN
}	//SeeSawN

