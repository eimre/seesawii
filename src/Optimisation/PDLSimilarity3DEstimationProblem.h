/**
 * @file PDLSimilarity3DEstimationProblem.h Public interface for \c PDLSimilarity3DEstimationProblemC
 * @author Evren Imre
 * @date 30 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_SIMILARITY_3D_ESTIMATION_PROBLEM_H_1578792
#define PDL_SIMILARITY_3D_ESTIMATION_PROBLEM_H_1578792

#include "PDLSimilarity3DEstimationProblem.ipp"
namespace SeeSawN
{
namespace OptimisationN
{

class PDLSimilarity3DEstimationProblemC;	///<Problem for Powell's dog leg algorithm, for the estimation of 3D similarity transformations
}	//OptimisationN
}	//SeeSawN

#endif /* PDL_SIMILARITY_3D_ESTIMATION_PROBLEM_H_1578792 */
