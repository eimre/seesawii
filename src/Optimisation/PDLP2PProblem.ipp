/**
 * @file PDLP2PProblem.ipp Implementation of the nonlinear optimisation problem for 2-point orientation estimation
 * @author Evren Imre
 * @date 7 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_P2P_PROBLEM_IPP_5609120
#define PDL_P2P_PROBLEM_IPP_5609120

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <climits>
#include <vector>
#include <cstddef>
#include "PDLGeometryEstimationProblemBase.h"
#include "../Geometry/P2PSolver.h"
#include "../Geometry/Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Numeric/LinearAlgebraJacobian.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using Eigen::VectorXd;
using Eigen::Matrix;
using std::vector;
using std::numeric_limits;
using std::size_t;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::GeometryN::P2PSolverC;
using SeeSawN::GeometryN::JacobianQuaternionToRotationMatrix;
using SeeSawN::GeometryN::QuaternionToVector;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::NumericN::JacobiandAda;

/**
 * @brief Problem for Powell's dog leg optimisation algorithm, for 2-point orientation estimation
 * @remarks The problem minimises the reprojection error
 * @remarks An orientation is internally parameterised by a quaternion
 * @warning A valid problem has at least 4 constraints
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLP2PProblemC : public PDLGeometryEstimationProblemBaseC<PDLP2PProblemC, P2PSolverC>
{
	private:

		typedef PDLGeometryEstimationProblemBaseC<PDLP2PProblemC, P2PSolverC> BaseT;	///< Type of the base

		typedef typename BaseT::real_type RealT;
		typedef typename BaseT::solver_type GeometrySolverT;
		typedef typename BaseT::model_type ModelT;
		typedef typename BaseT::error_type GeometricErrorT;

	public:

		/**@name Constructors */ //@{
		PDLP2PProblemC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLP2PProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix=optional<MatrixXd>());	///< Constructor
		//@}

		/** @name PowellDogLegProblemConceptC */ //@{
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP);	///< Computes the Jacobian
		//@}

		/** @name Overrides */ //@{
		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		VectorXd ComputeError(const VectorXd& vP);	///< Computes the error vector for a model
		VectorXd Update(const VectorXd& vP, const VectorXd& vU) const;	///< Updates the current solution
		ModelT MakeResult(const VectorXd& vP) const;	///< Converts a vector to a model
		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const;	///< Computes the distance in the model space as a result of the update \c vU
		//@}
};	//class PDLP2PProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @param[in] oobservationWeightMatrix Observation weights
 * @post A valid object, of \c CorrespondenceRangeT has 4 elements
 */
template<class CorrespondenceRangeT>
PDLP2PProblemC::PDLP2PProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)
{
	if(boost::distance(ccorrespondences)<4)
		flagValid=false;
}	//PDLRotation3DEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix)

}	//OptimisationN
}	//SeeSawN

#endif /* PDLP2PPROBLEM_IPP_ */
