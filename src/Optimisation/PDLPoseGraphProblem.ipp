/**
 * @file PDLPoseGraphProblem.ipp Implementation of the pose graph optimisation problem
 * @author Evren Imre
 * @date 22 Jan 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_POSE_GRAPH_PROBLEM_IPP_4189012
#define PDL_POSE_GRAPH_PROBLEM_IPP_4189012

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <tuple>
#include <utility>
#include <vector>
#include <type_traits>
#include <map>
#include <set>
#include <cstddef>
#include <stdexcept>
#include <iterator>
#include <cmath>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Viewpoint3D.h"
#include "../Geometry/Rotation.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::for_each;
using boost::set_difference;
using boost::range_value;
using boost::PairAssociativeContainer;
using boost::optional;
using Eigen::Matrix;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::Vector4d;
using std::tuple;
using std::get;
using std::make_tuple;
using std::pair;
using std::vector;
using std::true_type;
using std::false_type;
using std::map;
using std::multimap;
using std::is_same;
using std::set;
using std::invalid_argument;
using std::size_t;
using std::advance;
using std::inserter;
using std::max;
using SeeSawN::ElementsN::Viewpoint3DC;
using SeeSawN::ElementsN::ComputeRelativePose;
using SeeSawN::ElementsN::JacobianRelativePose;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::GeometryN::QuaternionToVector;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::JacobianQuaternionToRotationVector;
using SeeSawN::NumericN::JacobiandxtAxdx;

/**
 * @brief Problem for Powell's dog leg algorithm, for the estimation of absolute pose from relative pose measurements
 * @remarks The problem minimises the Mahalanobis distance the measured and the predicted relative pose parameters
 * @warning A valid problem should have at least one relative pose constraint for each viewpoint
 * @warning The covariance matrices for the observations should be invertible
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLPoseGraphProblemC
{
	private:

		typedef pair<unsigned int, unsigned int> IndexPairT;	///< A pair of indices
		typedef Matrix<double, 6, 6> CovarianceT;	///< Covariance matrix type for a pose constraint
		typedef tuple<Viewpoint3DC, CovarianceT> PoseT;	///< A pose

		typedef map<unsigned int, Viewpoint3DC> ModelT;	///< Type of the initial estimate
		typedef multimap<IndexPairT, PoseT> ObservationSetT;	///< Type of the constraint set

		/** @name State */ //@{
		bool flagValid;	///< \c true if the object is initialised
		ModelT initialEstimate;	///< Initial estimate
		ObservationSetT observations;	///< Constraints
		//@}

		static constexpr size_t sPosition=3;	///< Number of parameters encoding a position
		static constexpr size_t sOrientation=4;	///< Number of parameters encoding an orientation
		static constexpr size_t sPose=sPosition+sOrientation;	///< Number of parametrs encoding a pose

	public:

		/** @name Observation definition*///@{
		typedef PoseT pose_type;
		static constexpr unsigned int iViewpoint=0;	///< Viewpoint component of a relative pose constraint
		static constexpr unsigned int iCovariance=1;	///< Covariance component of a relative pose constraint
		//@}

		/** @name PowellDogLegProblemC interface */ //@{
		typedef map<unsigned int, Viewpoint3DC> result_type ;	///< Type of the result
		typedef ObservationSetT observation_set_type;	///< Type of the constraint set
		typedef ModelT model_type;	///< Type of the initial estimate

		typedef false_type has_normal_solver;	///< Uses the facilities of the optimisation engine for the normal equations
		typedef false_type has_covariance_estimator;	///< Uses the facilities of the optimisation engine for the covariance
		typedef true_type has_nontrivial_update;	///< Has a dedicated update function

		PDLPoseGraphProblemC();	///< Default constructor
		template<class InitialEstimateT, class ObservationContainerT> void Configure(const InitialEstimateT& iinitialEstimate, const ObservationContainerT& oobservations, const optional<MatrixXd>& dummyWeights);	///< Configures a problem

		VectorXd InitialEstimate() const;	///< Returns the initial estimate
		static optional<MatrixXd> ObservationWeights();	///< Returns the observation weights. Always invalid

		static VectorXd Update(const VectorXd& vP, const VectorXd& vU);	///< Updates a parameter vector
		VectorXd ComputeError(const VectorXd& vP) const;	///< Computes the error for the current estimate
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const;	///< Computes the Jacobian at the current estimate
		static double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU);	///< Computes the distance in the model space as a result of the update
		ModelT MakeResult(const VectorXd& vP) const;	///< Converts a parameter vector to a result object

		bool IsValid() const;	///< \c true if the problem object is valid

		static void InitialiseIteration();	///< Initialises an iteration
		static void FinaliseIteration();	///< Finalises an iteration

		//@}

		template<class InitialEstimateT, class ObservationContainerT> PDLPoseGraphProblemC(const InitialEstimateT& iinitialEstimate, const ObservationContainerT& oobservations, const optional<MatrixXd>& dummyWeights);	///< Constructor
};	//PDLPoseGraphProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Configures a problem
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] oobservations Observations
 * @param[in] dummyWeights Dummy parameter
 * @pre \c InitialEstimateT is a pair associative container holding elements of type \c {unsigned int;Viewpoint3DC}
 * @pre \c ObservationContainerT is a pair associative container holding elements of type \c {IndexPairT; PoseT}
 * @pre For each member of \c iinitialEstimate , there is at least one element in \c oobservations
 * @pre All indices specified in \c oobservations correspond to an element of \c iinitialEstimate
 * @throw invalid_argument If the observations do not constrain all viewpoints
 * @throw invalid_argument If some observations refer to nonexisting viewpoints
 * @post The state is modified, and the object is valid
 */
template<class InitialEstimateT, class ObservationContainerT>
void PDLPoseGraphProblemC::Configure(const InitialEstimateT& iinitialEstimate, const ObservationContainerT& oobservations, const optional<MatrixXd>& dummyWeights)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((PairAssociativeContainer<InitialEstimateT>));
	BOOST_CONCEPT_ASSERT((PairAssociativeContainer<ObservationContainerT>));
	static_assert(is_same<typename range_value<InitialEstimateT>::type, typename ModelT::value_type>::value, "PDLPoseGraphProblemC::Configure InitialEstimateT must hold elements of type model_type::value_type ");
	static_assert(is_same<typename range_value<ObservationContainerT>::type, typename ObservationSetT::value_type>::value, "PDLPoseGraphProblemC::Configure: ObservationContainerT must hold elements of type observation_set_type::value_type ");

	set<unsigned int> viewpointIndices;
	for_each(iinitialEstimate, [&](const pair<unsigned int, Viewpoint3DC>& value){viewpointIndices.insert(value.first);});

	set<unsigned int> observationIndices;
	for_each(oobservations, [&](const pair<IndexPairT, PoseT>& value){observationIndices.insert(value.first.first); observationIndices.insert(value.first.second); });

	set<unsigned int> diff1;
	set_difference(viewpointIndices, observationIndices, inserter(diff1, diff1.begin()));

	if(!diff1.empty() )
		throw(invalid_argument("PDLPoseGraphProblemC::Configure : Unconstrained viewpoints in iinitialEstimate" ) );

	set<unsigned int> diff2;
	set_difference(observationIndices, viewpointIndices, inserter(diff2, diff2.begin()));

	if(!diff2.empty() )
		throw(invalid_argument("PDLPoseGraphProblemC::Configure : Some observations do not have a corresponding viewpoint" ) );

	initialEstimate.clear();
	for_each(iinitialEstimate, [&](const pair<unsigned int, Viewpoint3DC>& value){initialEstimate.emplace(value.first, value.second);} );

	observations.clear();
	for_each(oobservations, [&](const pair<IndexPairT, PoseT>& value){observations.emplace(value.first, PoseT(get<iViewpoint>(value.second), get<iCovariance>(value.second).inverse() ) );});

	flagValid=true;
}	//template<class InitialEstimateT, class ObservationRangeT> void Configure(const InitialEstimateT& iinitialEstimate, const ObservationRangeT& oobservations, const optional<MatrixXd>& dummyWeights)

/**
 * @brief Constructor
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] oobservations Observations
 * @param[in] dummyWeights Dummy parameter
 * @post The state is modified, and the object is valid
 */
template<class InitialEstimateT, class ObservationContainerT>
PDLPoseGraphProblemC::PDLPoseGraphProblemC(const InitialEstimateT& iinitialEstimate, const ObservationContainerT& oobservations, const optional<MatrixXd>& dummyWeights) : flagValid(false)
{
	Configure(iinitialEstimate, oobservations, dummyWeights);
}	//PDLPoseGraphProblemC(const InitialEstimateT& iinitialEstimate, const ObservationContainerT& oobservations, const optional<MatrixXd>& dummyWeights)

}	//OptimisationN
}	//SeeSawN

#endif /* PDLPOSEGRAPHPROBLEM_IPP_ */
