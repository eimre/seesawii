/**
 * @file PDLOneSidedFundamentalMatrixEstimationProblem.ipp Implementation of Powell's dog leg optimisation problem for one-sided fundamental matrix estimation
 * @author Evren Imre
 * @date 21 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_ONE_SIDED_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_IPP_7090213
#define PDL_ONE_SIDED_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_IPP_7090213

#include <boost/optional.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <iterator>
#include <tuple>
#include "PDLGeometryEstimationProblemBase.h"
#include "../Geometry/OneSidedFundamentalSolver.h"
#include "../Geometry/Rotation.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Numeric/LinearAlgebraJacobian.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using boost::const_begin;
using Eigen::Vector3d;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::Matrix;
using std::advance;
using std::tie;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::GeometryN::OneSidedFundamentalSolverC;
using SeeSawN::GeometryN::QuaternionToVector;
using SeeSawN::GeometryN::JacobianQuaternionToRotationMatrix;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::DecomposeEssential;
using SeeSawN::ElementsN::ComposeEssential;
using SeeSawN::ElementsN::TranslationToPosition;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::JacobianNormalisedCameraToEssential;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::NumericN::JacobiandABdB;

/**
 * @brief Problem for Powell's dog leg algorithm, for the estimation of a one-sided fundamental matrix
 * @remarks The problem minimises the Sampson's error (or rather, its square-root)
 * @remarks Internally, the fundamental matrix is represented as the focal length, the orientation quaternion and the camera center of the second camera, in a normalised canonical camera pair.
 * @warning A valid problem has at least 8 constraints
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLOneSidedFundamentalMatrixEstimationProblemC : public PDLGeometryEstimationProblemBaseC<PDLOneSidedFundamentalMatrixEstimationProblemC, OneSidedFundamentalSolverC>
{
	private:

		typedef PDLGeometryEstimationProblemBaseC<PDLOneSidedFundamentalMatrixEstimationProblemC, OneSidedFundamentalSolverC> BaseT;	///< Type of the base

		typedef typename BaseT::real_type RealT;
		typedef typename BaseT::solver_type GeometrySolverT;
		typedef typename BaseT::model_type ModelT;
		typedef typename BaseT::error_type GeometricErrorT;

		/** @name Configuration */ //@{
		typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> BimapT;
		BimapT configurationValidators;	///< A set of 2D correspondences to identify which of the 4 possible decompositions of an essential matrix is valid
		void SetConfigurationValidators();	///< Sets the configuration validators

		EpipolarMatrixT MakeEssential(const VectorXd& vP) const;	///< Computes the essential matrix corresponding to the parameter vector
		//@}

	public:

		/**@name Constructors */ //@{
		PDLOneSidedFundamentalMatrixEstimationProblemC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLOneSidedFundamentalMatrixEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix=optional<MatrixXd>());	///< Initial estimate
		//@}

		/** @name PowellDogLegProblemConceptC */ //@{
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP);	///< Computes the Jacobian
		//@}

		/** @name Overrides */ //@{
		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		VectorXd ComputeError(const VectorXd& vP);	///< Computes the error vector for a model
		VectorXd Update(const VectorXd& vP, const VectorXd& vU) const;	///< Updates the current solution
		ModelT MakeResult(const VectorXd& vP, bool flagNormalise=true) const;	///< Converts a vector to a model
		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const;	///< Computes the distance in the model space as a result of the update \c vU
		//@}

};	//class PDLOneSidedFundamentalMatrixEstimationProblemC

/**
 * @brief Constructor
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @param[in] oobservationWeightMatrix Observation weights
 * @post A valid object, if \c ccorrespondences has at least 8 elements
 * @remarks Initial estimate is normalised
 */
template<class CorrespondenceRangeT>
PDLOneSidedFundamentalMatrixEstimationProblemC::PDLOneSidedFundamentalMatrixEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)
{
	if(boost::distance(ccorrespondences)<8)
	{
		flagValid=false;
		return;
	}	//if(boost::distance(ccorrespondences)<8)

	SetConfigurationValidators();
}	//PDLHomographyXDEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)

}	//OptimisationN
}	//SeeSawN

#endif /* PDL_ONE_SIDED_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_IPP_7090213 */
