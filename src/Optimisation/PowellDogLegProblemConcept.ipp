/**
 * @file PowellDogLegProblemConcept.ipp Implementation of PowellDogLegProblemConceptC
 * @author Evren Imre
 * @date 25 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POWELL_DOG_LEG_PROBLEM_CONCEPT_IPP_3549912
#define POWELL_DOG_LEG_PROBLEM_CONCEPT_IPP_3549912

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include "../Elements/Correspondence.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::Assignable;
using boost::CopyConstructible;
using boost::optional;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;

/**
 * @brief Powell's dog leg algorithm problem concept checker
 * @ingroup Concept
 */
template<class TestT>
class PowellDogLegProblemConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));

	public:
		BOOST_CONCEPT_USAGE(PowellDogLegProblemConceptC)
		{
			typedef typename TestT::result_type ResultT;

			TestT tested;

			typename TestT::observation_set_type observations;
			typename TestT::model_type initialEstimate;
			optional<MatrixXd> mWnew;
			tested.Configure(initialEstimate, observations, mWnew);

			bool flagValid=tested.IsValid(); (void)flagValid;
			VectorXd sI=tested.InitialEstimate(); (void)sI;

			tested.InitialiseIteration();
			tested.FinaliseIteration();

			VectorXd vP;
			VectorXd vE=tested.ComputeError(vP); (void)vE;
			optional<MatrixXd> mJ=tested.ComputeJacobian(vP); (void)mJ;
			optional<MatrixXd> mW=tested.ObservationWeights(); (void)mW;

			VectorXd vDL;
			double d=tested.ComputeRelativeModelDistance(vP, vDL); (void)d;
			ResultT res=tested.MakeResult(vP); (void)res;

			typename TestT::has_nontrivial_update flagUpdate; (void)flagUpdate;
			//VectorXd sU=tested.Update(vP, vDL); (void)sU;

			typename TestT::has_normal_solver flagSolver; (void)flagSolver;
			//VectorXd vGN=tested.SolveNormalEquations(MatrixXd(), VectorXd());	//If has_normal_solver is false_type, never called

			typename TestT::has_covariance_estimator flagCovariance; (void)flagCovariance;	//If has_covariance_estimator is false_type, never called
			//optional<MatrixXd> mC=tested.ComputeCovariance(MatrixXd());
		}	//BOOST_CONCEPT_USAGE(PowellDogLegProblemConceptC)

	///@endcond
};	//class PowellDogLegProblemConceptC

}	//OptimisationN
}	//SeeSawN

#endif /* POWELL_DOG_LEG_PROBLEM_CONCEPT_IPP_3549912 */
