/**
 * @file PDLRotation3DEstimationProblem.ipp Implementation of \c PDLRotation3DEstimationProblemC
 * @author Evren Imre
 * @date 30 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_ROTATION_3D_ESTIMATION_PROBLEM_IPP_5789983
#define PDL_ROTATION_3D_ESTIMATION_PROBLEM_IPP_5789983

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <cstddef>
#include <vector>
#include <climits>
#include "PDLGeometryEstimationProblemBase.h"
#include "../Geometry/Rotation3DSolver.h"
#include "../Geometry/Rotation.h"
#include "../Elements/GeometricEntity.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Numeric/LinearAlgebraJacobian.h"

namespace SeeSawN
{
namespace OptimisationN
{
using boost::optional;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::Matrix;
using std::size_t;
using std::vector;
using std::numeric_limits;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::GeometryN::JacobianQuaternionToRotationMatrix;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::WrappersN::QuaternionToVector;
using SeeSawN::NumericN::JacobiandAda;

/**
 * @brief Problem for Powell's dog leg algorithm, for the estimation of 3D rotations
 * @remarks The problem minimises the symmetric transfer error
 * @remarks A rotation is internally parameterised by a quaternion
 * @warning A valid problem has at least 4 constraints
 * @ingroup Problem
 * @nosubgrouping
 */
class PDLRotation3DEstimationProblemC : public PDLGeometryEstimationProblemBaseC<PDLRotation3DEstimationProblemC, Rotation3DSolverC>
{
	private:

		typedef PDLGeometryEstimationProblemBaseC<PDLRotation3DEstimationProblemC, Rotation3DSolverC> BaseT;	///< Type of the base

		typedef typename BaseT::real_type RealT;
		typedef typename BaseT::solver_type GeometrySolverT;
		typedef typename BaseT::model_type ModelT;
		typedef typename BaseT::error_type GeometricErrorT;

	public:

		/**@name Constructors */ //@{
		PDLRotation3DEstimationProblemC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLRotation3DEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix=optional<MatrixXd>());	///< Constructor
		//@}

		/** @name PowellDogLegProblemConceptC */ //@{
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP);	///< Computes the Jacobian
		//@}

		/** @name Overrides */ //@{
		VectorXd InitialEstimate() const;	///< Returns the initial estimate in vector form
		VectorXd ComputeError(const VectorXd& vP);	///< Computes the error vector for a model
		VectorXd Update(const VectorXd& vP, const VectorXd& vU) const;	///< Updates the current solution
		ModelT MakeResult(const VectorXd& vP) const;	///< Converts a vector to a model
		double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const;	///< Computes the distance in the model space as a result of the update \c vU
		//@}
};	//class PDLRotation3DEstimationProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @param[in] oobservationWeightMatrix Observation weights
 * @post A valid object, of \c CorrespondenceRangeT has 4 elements
 */
template<class CorrespondenceRangeT>
PDLRotation3DEstimationProblemC::PDLRotation3DEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)
{
	if(boost::distance(ccorrespondences)<4)
		flagValid=false;
}	//PDLRotation3DEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix)

}	//OptimisationN
}	//SeeSawN

#endif /* PDL_ROTATION_3D_ESTIMATION_PROBLEM_IPP_5789983 */
