/**
 * @file PDLOneSidedFundamentalMatrixEstimationProblem.cpp Implementation of Powell's dog leg optimisation problem for one-sided fundamental matrix estimation
 * @author Evren Imre
 * @date 21 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLOneSidedFundamentalMatrixEstimationProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLOneSidedFundamentalMatrixEstimationProblemC::PDLOneSidedFundamentalMatrixEstimationProblemC()
{}

/**
 * @brief Sets the configuration validators
*/
void PDLOneSidedFundamentalMatrixEstimationProblemC::SetConfigurationValidators()
{
	configurationValidators.clear();
	auto it=boost::const_begin(BaseT::correspondences);
	for(size_t c=0; c<6; ++c, advance(it,1))
		configurationValidators.push_back(typename BimapT::value_type(it->left, it->right));
}	//void SetConfigurationValidators(const CorrespondenceRangeT& src)

/**
 * @brief Computes the essential matrix corresponding to the parameter vector
 * @param[in] vP Parameter vector
 * @return Essential matrix
 */
EpipolarMatrixT PDLOneSidedFundamentalMatrixEstimationProblemC::MakeEssential(const VectorXd& vP) const
{
	QuaternionT q(vP[0], vP[1], vP[2], vP[3]);
	RotationMatrix3DT mR=q.toRotationMatrix();

	Coordinate3DT vt=-mR*vP.segment(4,3);
	EpipolarMatrixT mE=ComposeEssential(vt, mR, false);

	return mE;
}	//EpipolarMatrixT MakeEssential(const VectorXd& vP)

/**
 * @brief Returns the initial estimate in vector form
 * @return Camera matrix corresponding to the initial estimate, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLOneSidedFundamentalMatrixEstimationProblemC::InitialEstimate() const
{
	//Preconditions
	assert(BaseT::flagValid);

	//Decompose into essential and focal
	double f;
	EpipolarMatrixT mE;
	tie(mE, f)=*OneSidedFundamentalSolverC::DecomposeOneSidedF(BaseT::initialEstimate, true);

	//Decompose the essential matrix
	RotationMatrix3DT mR;
	Coordinate3DT vt;
	tie(vt, mR)=*DecomposeEssential(mE, configurationValidators);	//Valid, since mE is a valid essential matrix

	//Compose the internal parameterisation
	VectorXd output(8);

	QuaternionT q(mR);
	output.head(4)= (q.w()>0 ? 1:-1)*QuaternionToVector(q);	//First element is positive
	output.segment(4,3)=TranslationToPosition(vt, mR);
	output(7)=f;

	return output;
}	//VectorXd InitialEstimate() const

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @param[in] flagNormalise If \c true , the result is normalised
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 * @pre \c vP(0:3) is unit norm
 */
auto PDLOneSidedFundamentalMatrixEstimationProblemC::MakeResult(const VectorXd& vP, bool flagNormalise) const -> ModelT
{
	//Preconditions
	assert(BaseT::flagValid);
	assert(vP.head(4).norm()==1);

	EpipolarMatrixT mF=Vector3d(1, 1, vP[7]).asDiagonal() * MakeEssential(vP);

	if(flagNormalise)
		mF.normalize();

	return mF;
}	//result_type MakeResult(const VectorXd& vP)

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLOneSidedFundamentalMatrixEstimationProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeError(MakeResult(vP, false));	//Switch to OS-fundamental matrix
}	//VectorXd ComputeError(const VectorXd& vP) const

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLOneSidedFundamentalMatrixEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);

	//Normalised quaternion with the first element positive
	VectorXd vq=vP.head(4)+vU.head(4);
	vq.normalize();

	if(vq[0]<0)
		vq=-vq;

	VectorXd vNew(vP.size());
	vNew.segment(0,4)=vq;
	vNew.segment(4,3)=vP.segment(4,3)+vU.segment(4,3);	//Camera centre update
	vNew(7)=vP(7)+vU(7);

	return vNew;
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 * @pre \c vP is unit norm
 */
double PDLOneSidedFundamentalMatrixEstimationProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP, true), MakeResult(vPU, true));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLOneSidedFundamentalMatrixEstimationProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//F and E
	EpipolarMatrixT mE=MakeEssential(vP);
	double f=vP[7];
	IntrinsicCalibrationMatrixT mKi=Vector3d(1, 1, f).asDiagonal();
	EpipolarMatrixT mF=mKi*mE;

	//Jacobian wrt error
	optional<MatrixXd> mJF=BaseT::ComputeJacobian(mF, vP.size());

	optional<MatrixXd> output;

	if(!mJF)
		return output;

	//Jacobian wrt internal representation

	typedef Matrix<double, 9, 9> Matrix9T;
	Matrix9T dFdE=JacobiandABdB<Matrix9T>(mKi, mE); dFdE.setIdentity();

	typedef Matrix<double, 9, 1> Vector9T;
	Vector9T dFdf=Vector9T::Zero(); dFdf.tail(3)=mE.row(2).transpose();

	//dE/dq,C : dE/dR,C and then dR/dq

	//dE/dR,C
	Coordinate3DT vC=vP.segment(4,3);
	QuaternionT q(vP[0], vP[1], vP[2], vP[3]);
	RotationMatrix3DT mR=q.matrix();
	CameraMatrixT mCam=ComposeCameraMatrix(vC, mR);
	Matrix<double, 9, 12> dEdRC=JacobianNormalisedCameraToEssential(mCam, false);

	Matrix<double, 9, 4> dRdq=JacobianQuaternionToRotationMatrix(q);

	Matrix<double, 9, 8> dFdqCf;
	dFdqCf.block(0,4,9,3)=dFdE*dEdRC.block(0,9,9,3);	//dC
	dFdqCf.block(0,0,9,4)=dFdE*(dEdRC.block(0,0,9,9)*dRdq);	//dq
	dFdqCf.col(7)=dFdf;	//df

	output=(*mJF)*dFdqCf;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const

}	//OptimisationN
}	//SeeSawN

