/**
 * @file PDLP2PfProblem.cpp Implementation of the problem for Powell's dog leg algorithm for orientation and focal length estimation
 * @author Evren Imre
 * @date 11 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLP2PfProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLP2PfProblemC::PDLP2PfProblemC()
{}

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 * @pre \c vP.segment(1,4) is unit norm
 * @pre \c vP[0]>0
 */
auto PDLP2PfProblemC::MakeResult(const VectorXd& vP) const -> ModelT
{
	assert(BaseT::flagValid);
	assert(fabs(vP.segment(1,4).norm()-1)<3*numeric_limits<RealT>::epsilon());
	assert(vP[0]>0);

	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity();
	mK(0,0)=vP[0]; mK(1,1)=vP[0];

	ModelT output=ModelT::Zero();
	output.block(0,0,3,3)= mK * QuaternionT(vP[1], vP[2], vP[3], vP[4]).matrix();
	return output;
}	//auto MakeResult(const VectorXd& vP, bool dummy) const -> ModelT

/**
 * @brief Returns the initial estimate in vector form
 * @return Focal length and the rotation quaternion corresponding to the initial estimate, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLP2PfProblemC::InitialEstimate() const
{
	//Preconditions
	assert(BaseT::flagValid);

	IntrinsicCalibrationMatrixT mK;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(mK, vC, mR)=DecomposeCamera(initialEstimate, false);

	VectorXd output(5);
	output[0]=0.5*(mK(0,0)+mK(1,1));

	QuaternionT q(mR);
	output.segment(1,4)=QuaternionToVector(q);
	output.segment(1,4)*=(q.w()<0 ? -1 : 1);
	return output;
}	//VectorXd PDLRotation3DEstimationProblemC::InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLP2PfProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeError(MakeResult(vP));
}	//VectorXd ComputeError(const VectorXd& vP)

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLP2PfProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);

	VectorXd output(5);

	RealT fnew=vP[0]+vU[0];
	output[0]= (fnew>0) ? fnew : vP[0];	//Reject an update that results in a negative focal length

	VectorXd vRot=(vP.segment(1,4)+vU.segment(1,4)).normalized();
	output.segment(1,4)= (vRot[0]<0 ? -1:1) * vRot;

	return output;
}	//VectorXd PDLRotation3DEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 */
double PDLP2PfProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP), MakeResult(vPU));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLP2PfProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//Jacobian wrt camera matrix
	CameraMatrixT mP=MakeResult(vP);
	optional<MatrixXd> mJP=BaseT::ComputeJacobian(mP, vP.size());

	optional<MatrixXd> output;
	if(!mJP)
		return output;

	//Camera matrix wrt orientation and focal length

	QuaternionT q(vP[1], vP[2], vP[3], vP[4]);
	Matrix<double, 9, 4> dRdq=JacobianQuaternionToRotationMatrix(q);

	Matrix<double, 12,17> dPdKCR=JacobiandPdKCR(mP);

	Matrix<double,12,5> dPdfq; dPdfq.setZero();
	dPdfq.col(0)=dPdKCR.col(0);	//Focal length
	dPdfq.rightCols(4)= dPdKCR.rightCols(9) * dRdq; //Orientation

	output=(*mJP)*dPdfq;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP)
}	//OptimisationN
}	//SeeSawN

