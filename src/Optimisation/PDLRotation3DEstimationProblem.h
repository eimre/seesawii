/**
 * @file PDLRotation3DEstimationProblem.h Public interface for \c PDLRotation3DEstimationProblemC
 * @author Evren Imre
 * @date 30 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_ROTATION_3DESTIMATION_PROBLEM_H_6329043
#define PDL_ROTATION_3DESTIMATION_PROBLEM_H_6329043

#include "PDLRotation3DEstimationProblem.ipp"
namespace SeeSawN
{
namespace OptimisationN
{

class PDLRotation3DEstimationProblemC;	///<Problem for Powell's dog leg algorithm, for the estimation of 3D rotations
}	//OptimisationN
}	//SeeSawN

#endif /* PDL_ROTATION_3DESTIMATION_PROBLEM_H_6329043 */
