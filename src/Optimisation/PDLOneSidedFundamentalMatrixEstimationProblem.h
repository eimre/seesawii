/**
 * @file PDLOneSidedFundamentalMatrixEstimationProblem.h Public interface for Powell's dog leg optimisation problem for one-sided fundamental matrix estimation
 * @author Evren Imre
 * @date 21 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_ONE_SIDED_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_H_0591312
#define PDL_ONE_SIDED_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_H_0591312

#include "PDLOneSidedFundamentalMatrixEstimationProblem.ipp"
namespace SeeSawN
{
namespace OptimisationN
{
class PDLOneSidedFundamentalMatrixEstimationProblemC;	///< Problem for Powell's dog leg algorithm, for the estimation of a one-sided fundamental matrix
}	//OptimisationN
}	//SeeSawN

#endif /* PDL_ONE_SIDED_FUNDAMENTAL_MATRIX_ESTIMATION_PROBLEM_H_0591312 */
