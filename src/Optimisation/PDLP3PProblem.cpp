/**
 * @file PDLP3PProblem.cpp Implementation of the problem for Powell's dog leg algorithm for pose estimation
 * @author Evren Imre
 * @date 11 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLP3PProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLP3PProblemC::PDLP3PProblemC()
{}

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 * @pre \c vP.segment(3,4) is unit norm
 */
auto PDLP3PProblemC::MakeResult(const VectorXd& vP) const -> ModelT
{
	assert(BaseT::flagValid);
	assert(fabs(vP.segment(3,4).norm()-1)<3*numeric_limits<RealT>::epsilon());
	return ComposeCameraMatrix(vP.head(3), QuaternionT(vP[3], vP[4], vP[5], vP[6]).matrix() );
}	//auto MakeResult(const VectorXd& vP, bool dummy) const -> ModelT

/**
 * @brief Returns the initial estimate in vector form
 * @return Position and orientation quaternion, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLP3PProblemC::InitialEstimate() const
{
	//Preconditions
	assert(BaseT::flagValid);

	IntrinsicCalibrationMatrixT mK;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(mK, vC, mR)=DecomposeCamera(initialEstimate, true);

	VectorXd output(7);
	output.segment(0,3)=vC;

	QuaternionT q(mR);
	output.segment(3,4)=QuaternionToVector(q);
	output.segment(3,4)*=(q.w()<0 ? -1 : 1);
	return output;
}	//VectorXd InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLP3PProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeError(MakeResult(vP));
}	//VectorXd ComputeError(const VectorXd& vP)

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLP3PProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);

	VectorXd output=vP+vU;
	output.segment(3,4) = output.segment(3,4).normalized();

	if(output[3]<0)
		output.segment(3,4)*=-1;

	return output;
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 */
double PDLP3PProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);
	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP), MakeResult(vPU));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLP3PProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//Jacobian wrt camera matrix
	CameraMatrixT mP=MakeResult(vP);
	optional<MatrixXd> mJP=BaseT::ComputeJacobian(mP, vP.size());

	optional<MatrixXd> output;
	if(!mJP)
		return output;

	//Camera matrix wrt orientation and pose

	QuaternionT q(vP[3], vP[4], vP[5], vP[6]);
	Matrix<double, 9, 4> dRdq=JacobianQuaternionToRotationMatrix(q);

	Matrix<double,12,12> dPdCR=JacobiandPdCR(mP);

	Matrix<double,12,7> dPdCq; dPdCq.setZero();
	dPdCq.leftCols(3)=dPdCR.leftCols(3);
	dPdCq.rightCols(4)= dPdCR.rightCols(9)*dRdq;

	output=(*mJP)*dPdCq;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP)
}	//OptimisationN
}	//SeeSawN

