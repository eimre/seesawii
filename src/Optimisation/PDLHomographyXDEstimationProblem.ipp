/**
 * @file PDLHomographyXDEstimationProblem.ipp Implementation of PDLHomographyXDEStimationProblemC
 * @author Evren Imre
 * @date 24 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PDL_HOMOGRAPHYXD_ESTIMATION_PROBLEM_IPP_7921312
#define PDL_HOMOGRAPHYXD_ESTIMATION_PROBLEM_IPP_7921312

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <cstddef>
#include "PDLGeometryEstimationProblemBase.h"
#include "../Geometry/GeometrySolverConcept.h"

namespace SeeSawN
{
namespace OptimisationN
{

using boost::optional;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using std::size_t;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::GeometryN::GeometrySolverConceptC;

/**
 * @brief Problem for Powell's dog leg algorithm, for the estimation of NxN homographies
 * @tparam HomographySolverT A homography solver
 * @pre \c HomographySolverT is a geometric solver for NxN homographies (unenforced)
 * @remarks The problem minimises the symmetric transfer error
 * @remarks Minimisation is performed over unnormalised models, but the result is normalised.
 * @ingroup Problem
 * @nosubgrouping
 */
template<class HomographySolverT>
class PDLHomographyXDEstimationProblemC : public PDLGeometryEstimationProblemBaseC<PDLHomographyXDEstimationProblemC<HomographySolverT>, HomographySolverT >
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<HomographySolverT>));
	//@endcond
	private:

		typedef PDLGeometryEstimationProblemBaseC<PDLHomographyXDEstimationProblemC<HomographySolverT>, HomographySolverT> BaseT;	///< Type of the base

		typedef typename BaseT::real_type RealT;
		typedef typename BaseT::solver_type GeometrySolverT;
		typedef typename BaseT::model_type ModelT;
		typedef typename BaseT::error_type GeometricErrorT;

	public:

		/**@name Constructors */ //@{
		PDLHomographyXDEstimationProblemC();	///< Default constructor
		template<class CorrespondenceRangeT> PDLHomographyXDEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix=optional<MatrixXd>());	///< Constructor
		//@}

		/** @name PowellDogLegProblemConceptC */ //@{
		optional<MatrixXd> ComputeJacobian(const VectorXd& vP);	///< Computes the Jacobian
		//@}

		/** @name Overrides */ //@{
		template<class CorrespondenceRangeT> void Configure(const ModelT& iinitialEstimate, const CorrespondenceRangeT& observations, const optional<MatrixXd>&  observationWeightMatrix=optional<MatrixXd>());	///< Configures the optimisation problem
		//@}

};	//class PDLHomographyXDEstimationProblemC

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class HomographySolverT>
PDLHomographyXDEstimationProblemC<HomographySolverT>::PDLHomographyXDEstimationProblemC()
{}

/**
 * @brief Constructor
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] ccorrespondences Correspondences
 * @param[in] ssolver Geometry solver
 * @param[in] eerrorMetric Error metric
 * @param[in] oobservationWeightMatrix Observation weights
 * @post A valid object, if \c ccorrespondences has at least 8 elements
 * @remarks Initial estimate is normalised
 */
template<class HomographySolverT>
template<class CorrespondenceRangeT>
PDLHomographyXDEstimationProblemC<HomographySolverT>::PDLHomographyXDEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric, const optional<MatrixXd>& oobservationWeightMatrix) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)
{
	BaseT::initialEstimate.normalize();
}	//PDLHomographyXDEstimationProblemC(const ModelT& iinitialEstimate, const CorrespondenceRangeT& ccorrespondences, const GeometrySolverT& ssolver, const GeometricErrorT& eerrorMetric) : BaseT(iinitialEstimate, ccorrespondences, ssolver, eerrorMetric)

/**
 * @brief Configures the optimisation problem
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] iinitialEstimate Initial estimate
 * @param[in] observations Observations
 * @param[in] observationWeightMatrix Observation weight matrix. Ignored in this default implementation
 * @pre A bimap of appropriate correspondence types (unenforced)
 * @post \c initialEstimate and \c correspondences are modified
 * @post \c flagValid is set, if \c observations has at least 8 elements
 */
template<class HomographySolverT>
template<class CorrespondenceRangeT>
void PDLHomographyXDEstimationProblemC<HomographySolverT>::Configure(const ModelT& iinitialEstimate, const CorrespondenceRangeT& observations, const optional<MatrixXd>& observationWeightMatrix)
{
	RealT nH=iinitialEstimate.norm();
	BaseT::Configure(iinitialEstimate/nH, observations, observationWeightMatrix);
}	//void Configure(const model_type& iinitialEstimate, const CorrespondenceRangeT& observations, const optional<MatrixXd>& observationWeightMatrix)

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
template<class HomographySolverT>
optional<MatrixXd> PDLHomographyXDEstimationProblemC<HomographySolverT>::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeJacobian(BaseT::solver.MakeModel(vP, false), vP.size());
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const

}	//OptimisationN
}	//SeeSawN

#endif /* PDL_HOMOGRAPHYXD_ESTIMATION_PROBLEM_IPP_7921312 */
