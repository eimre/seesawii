/**
 * @file PDLEssentialMatrixEstimationProblem.cpp Implementation of PDLEssentialMatrixEstimationProblemC
 * @author Evren Imre
 * @date 8 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PDLEssentialMatrixEstimationProblem.h"
namespace SeeSawN
{
namespace OptimisationN
{

/**
 * @brief Default constructor
 * @post Invalid object
 */
PDLEssentialMatrixEstimationProblemC::PDLEssentialMatrixEstimationProblemC()
{}

/**
 * @brief Sets the configuration validators
*/
void PDLEssentialMatrixEstimationProblemC::SetConfigurationValidators()
{
	configurationValidators.clear();
	auto it=boost::const_begin(BaseT::correspondences);
	for(size_t c=0; c<5; ++c, advance(it,1))
		configurationValidators.push_back(typename BimapT::value_type(it->left, it->right));
}	//void SetConfigurationValidators(const CorrespondenceRangeT& src)

/**
 * @brief Returns the initial estimate in vector form
 * @return Camera matrix corresponding to the initial estimate, in vector form
 * @pre \c flagValid=true
 */
VectorXd PDLEssentialMatrixEstimationProblemC::InitialEstimate() const
{
	//Preconditions
	assert(BaseT::flagValid);

	//Decompose the initial estimate
	RotationMatrix3DT mR;
	Coordinate3DT vt;
	tie(vt, mR)=*DecomposeEssential(BaseT::initialEstimate, configurationValidators);	//Valid, since mE is a valid essential matrix

	//Compose the internal parameterisation
	VectorXd output(7);

	QuaternionT q(mR);
	output.segment(0,4)= (q.w()>0 ? 1:-1)*QuaternionToVector(q);	//First element is positive
	output.segment(4,3)=TranslationToPosition(vt, mR);

	return output;
}	//VectorXd InitialEstimate() const

/**
 * @brief Computes the error vector for a model
 * @param[in] vP Parameter vector
 * @return Error vector
 * @pre \c flagValid=true
 */
VectorXd PDLEssentialMatrixEstimationProblemC::ComputeError(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);
	return BaseT::ComputeError(MakeResult(vP, false));
}	//VectorXd ComputeError(const VectorXd& vP) const

/**
 * @brief Updates the current solution
 * @param[in] vP Parameter vector
 * @param[in] vU Update
 * @return Updated parameter vector
 * @pre \c flagValid=true
 */
VectorXd PDLEssentialMatrixEstimationProblemC::Update(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);

	//Normalised quaternion with the first element positive
	VectorXd vq=vP.segment(0,4)+vU.segment(0,4);
	vq.normalize();

	if(vq[0]<0)
		vq=-vq;

	VectorXd vNew(vP.size());
	vNew.segment(0,4)=vq;
	vNew.segment(4,3)=vP.segment(4,3)+vU.segment(4,3);	//Camera centre update

	return vNew;
}	//VectorXd Update(const VectorXd& vP, const VectorXd& vU)

/**
 * @brief Converts a vector to a model
 * @param[in] vP Parameter vector
 * @param[in] flagNormalise If \c true , the result is normalised
 * @return Model corresponding to vP
 * @pre \c flagValid=true
 * @pre \c vP(0:3) is unit norm
 */
auto PDLEssentialMatrixEstimationProblemC::MakeResult(const VectorXd& vP, bool flagNormalise) const -> ModelT
{
	//Preconditions
	assert(BaseT::flagValid);
	assert(vP.segment(0,4).norm()==1);

	QuaternionT q(vP[0], vP[1], vP[2], vP[3]);
	RotationMatrix3DT mR=q.toRotationMatrix();

	Coordinate3DT vt=-mR*vP.segment(4,3);
	return ComposeEssential(vt, mR, flagNormalise);
}	//result_type MakeResult(const VectorXd& vP)

/**
 * @brief Computes the distance in the model space as a result of the update \c vU
 * @param[in] vP Parameter vector
 * @param[in] vU Update vector
 * @return Relative distance between the models corresponding to \c vP and \c vP+vU
 * @pre \c flagValid=true
 * @pre \c vP is unit norm
 */
double PDLEssentialMatrixEstimationProblemC::ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const
{
	//Preconditions
	assert(BaseT::flagValid);

	VectorXd vPU=Update(vP, vU);
	return GeometrySolverT::ComputeDistance(MakeResult(vP, true), MakeResult(vPU, true));
}	//double ComputeRelativeModelDistance(const VectorXd& vP, const VectorXd& vU) const

/**
 * @brief Computes the Jacobian
 * @param[in] vP Parameter vector
 * @return Jacobian matrix, if successful. Else, invalid
 * @pre The object is valid
 */
optional<MatrixXd> PDLEssentialMatrixEstimationProblemC::ComputeJacobian(const VectorXd& vP)
{
	//Preconditions
	assert(BaseT::flagValid);

	//Compose the essential matrix
	Coordinate3DT vC=vP.segment(4,3);
	QuaternionT q(vP[0], vP[1], vP[2], vP[3]);
	RotationMatrix3DT mR=q.matrix();

	optional<MatrixXd> mJE=BaseT::ComputeJacobian(ComposeEssential(-mR*vC, mR, false), vP.size());

	optional<MatrixXd> output;
	if(!mJE)
		return output;

	//dE/dR,C

	CameraMatrixT mCam=ComposeCameraMatrix(vC, mR);
	Matrix<RealT, 9, 12> dEdRC=JacobianNormalisedCameraToEssential(mCam, false);

	Matrix<RealT, 9, 4> dRdq=JacobianQuaternionToRotationMatrix(q);

	Matrix<RealT, 9, 7> dEdqC;
	dEdqC.block(0,4,9,3)=dEdRC.block(0,9,9,3);
	dEdqC.block(0,0,9,4)=dEdRC.block(0,0,9,9)*dRdq;

	output=(*mJE)*dEdqC;

	return output;
}	//optional<MatrixXd> ComputeJacobian(const VectorXd& vP) const
}	//OptimisationN
}	//SeeSawN

