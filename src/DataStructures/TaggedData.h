/**
 * @file TaggedData.h Public interface of TaggedDataC
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef TAGGED_DATA_H_78234456
#define TAGGED_DATA_H_78234456

#include "TaggedData.ipp"

namespace SeeSawN
{
namespace DataStructuresN
{
template<class TagT, class DataT> class TaggedDataC;    ///< A structure for a piece of data with a tag

/********** EXTERN TEMPLATES **********/
extern template class TaggedDataC<unsigned int, double>;
extern template class TaggedDataC<unsigned int, float>;
}   //DataStructuresN
}	//SeeSawN

#endif /* TAGGED_DATA_H_78234456 */
