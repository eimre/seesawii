/**
 * @file TestDataStructures.cpp Unit tests for DataStructures
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/


#define BOOST_TEST_MODULE DATA_STRUCTURES

#include <boost/test/unit_test.hpp>
#include "TaggedData.h"
#include "OrderedBuffer.h"
#include "TopNBuffer.h"
#include <set>
#include <vector>
#include <functional>

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestDataStructuresN
{

using namespace SeeSawN::DataStructuresN;
using std::vector;
using std::greater;

BOOST_AUTO_TEST_SUITE(Data_Structures)

BOOST_AUTO_TEST_CASE(Tagged_Data)
{
    typedef TaggedDataC<unsigned int, double> TaggedT;

    TaggedT data1;
    TaggedT data2(0, 4);

    BOOST_CHECK_EQUAL(data2.Tag(), (unsigned int)0 );
    BOOST_CHECK_EQUAL(data2.Data(), 4);

    TaggedT data3(0,4);
    BOOST_CHECK(data2==data3);

    data3.Tag()=1;
    BOOST_CHECK(data2!=data3);
    BOOST_CHECK_EQUAL(data3.Tag(), (unsigned int)1 );

    data3.Tag()=0;
    data3.Data()=5;
    BOOST_CHECK(data2!=data3);
    BOOST_CHECK_EQUAL(data3.Data(), 5);

    data3.Tag()=1;
    BOOST_CHECK(data2!=data3);
}   //BOOST_AUTO_TEST_CASE(Tagged_Data)


BOOST_AUTO_TEST_CASE(Ordered_Buffer)
{
	typedef multiset<double, greater<double> > ContainerT;
	OrderedBufferC<ContainerT> buffer1;

	BOOST_CHECK(!buffer1.Insert(3));

	buffer1.Resize(3);
	BOOST_CHECK(buffer1.Insert(4));
	BOOST_CHECK(buffer1.Insert(3));
	BOOST_CHECK(buffer1.Insert(2));
	BOOST_CHECK(!buffer1.Insert(1));

	OrderedBufferC<ContainerT> buffer2(2);
	BOOST_CHECK(buffer2.Insert(3));
	BOOST_CHECK(buffer2.Insert(2));
	BOOST_CHECK(!buffer2.Insert(1));

	buffer1.Resize(1);
	BOOST_CHECK_EQUAL(*(buffer1.Container().begin()),4);
}	//BOOST_AUTO_TEST_CASE(Ordered_Buffer)

BOOST_AUTO_TEST_CASE(TopN_Buffer)
{
	typedef TopNBufferC<vector<int>, greater<int> >  BufferT;
	BufferT buffer1(3);

	BOOST_CHECK(buffer1.Insert(5));
	BOOST_CHECK(buffer1.Insert(3));
	BOOST_CHECK(buffer1.Insert(2));
	BOOST_CHECK(!buffer1.Insert(1));
	BOOST_CHECK(!buffer1.Insert(-1));
	BOOST_CHECK(buffer1.Insert(6));

	vector<int> container=buffer1.Container();
	vector<int> containerR{3,5,6};
	BOOST_CHECK_EQUAL_COLLECTIONS(container.begin(), container.end(), containerR.begin(), containerR.end());

	vector<int> sorted=buffer1.Sort();
	vector<int> sortedR{6,5,3};
	BOOST_CHECK_EQUAL_COLLECTIONS(sorted.begin(), sorted.end(), sortedR.begin(), sortedR.end());

}	//BOOST_AUTO_TEST_CASE(TopN_Buffer)

BOOST_AUTO_TEST_SUITE_END() //Data_Structures

}   //TestDataStructuresN
}   //UnitTestsN
}	//SeeSawN


