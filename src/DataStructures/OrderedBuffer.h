/**
 * @file OrderedBuffer.h Public interface of OrderedBufferC
 * @author Evren Imre
 * @date 21 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ORDERED_BUFFER_H_6987621
#define ORDERED_BUFFER_H_6987621

#include "OrderedBuffer.ipp"
#include <set>
#include <map>
#include <functional>
#include <cstddef>

namespace SeeSawN
{
namespace DataStructuresN
{

template<class ContainerT> class OrderedBufferC;	///< A sorted buffer that only keeps the top-N elements

/********** EXTERN TEMPLATES **********/
using std::multiset;
extern template class OrderedBufferC<multiset<double> >;
extern template class OrderedBufferC<multiset<float> >;

using std::multimap;
using std::greater;
using std::size_t;
extern template class OrderedBufferC<multimap<double, size_t, greater<double> > >;
extern template class OrderedBufferC<multimap<float, size_t, greater<float>  > >;
}	//DataStructuresN
}	//SeeSawN

#endif /* ORDERED_BUFFER_H_6987621 */
