/**
 * @file TopNBuffer.h
 * @author Evren Imre
 * @date 20 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef TOP_N_BUFFER_H_7120932
#define TOP_N_BUFFER_H_7120932

#include "TopNBuffer.ipp"
namespace SeeSawN
{
namespace DataStructuresN
{
	template<class ContainerT, class ComparatorT> class TopNBufferC;	///< An unordered buffer that only keeps the top-N elements
}	//DataStructuresN
}	//SeeSawN



#endif /* TOP_N_BUFFER_H_7120932 */
