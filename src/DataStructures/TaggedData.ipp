/**
 * @file TaggedData.ipp Implementation of TaggedDataC
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef TAGGED_DATA_IPP_8234132
#define TAGGED_DATA_IPP_8234132

#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace DataStructuresN
{

using boost::EqualityComparableConcept;

/**
 * @brief A structure for a piece of data with a tag
 * @tparam TagT A tag type
 * @tparam DataT A data type
 * @pre \c TagT and \c DataT are equality comparable
 * @ingroup Utility
 * @nosubgrouping
 */
template<class TagT, class DataT>
class TaggedDataC
{
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((EqualityComparableConcept<TagT>));
    BOOST_CONCEPT_ASSERT((EqualityComparableConcept<DataT>));
    //@endcond

    private:

        /** @name Data */ //@{
        TagT tag;    ///< Tag
        DataT data; ///< Data
        //@}

    public:

        /** @name Constructors */ //@{
        TaggedDataC();  ///< Default constructor
        TaggedDataC(const TagT& ttag, const DataT& ddata);  ///< Constructor
        //@}

        /** @name Accessors */ //@{
        TagT& Tag();    ///< Accessor for \c tag
        const TagT& Tag() const;    ///< Accessor for \c tag

        DataT& Data();    ///< Accessor for \c data
        const DataT& Data() const;    ///< Accessor for \c data
        //@}

        /** @name Operations */ //@{
        bool operator==(const TaggedDataC& other) const;    ///< == operator
        bool operator!=(const TaggedDataC& other) const;    ///< != operator
        //@}

        /** @name Types */ //@{
        typedef DataT data_type;    ///< Data type
        typedef TagT tag_type;  ///< Tag
        //@}

};  //class TaggedDataC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post The object is not valid
 */
template<class TagT, class DataT>
TaggedDataC<TagT, DataT>::TaggedDataC()
{}

/**
 * @brief Constructor
 * @param[in] ttag Tag
 * @param[in] ddata Data
 */
template<class TagT, class DataT>
TaggedDataC<TagT, DataT>::TaggedDataC(const TagT& ttag, const DataT& ddata) : tag(ttag), data(ddata)
{}

/**
 * @brief Accessor for \c tag
 * @return \c tag
 */
template<class TagT, class DataT>
auto TaggedDataC<TagT, DataT>::Tag()-> TagT&
{
    return tag;
}   //auto Tag()-> TagT&

/**
 * @brief Accessor for \c tag
 * @return \c tag
 */
template<class TagT, class DataT>
auto TaggedDataC<TagT, DataT>::Tag() const -> const TagT&
{
    return tag;
}   //auto Tag() const -> const TagT&

/**
 * @brief Accessor for \c data
 * @return \c data
 */
template<class TagT, class DataT>
auto TaggedDataC<TagT, DataT>::Data()  ->  DataT&
{
    return data;
}   //auto Data()-> DataT&

/**
 * @brief Accessor for \c data
 * @return \c data
 */
template<class TagT, class DataT>
auto TaggedDataC<TagT, DataT>::Data() const -> const DataT&
{
    return data;
}   //auto Data() const -> const DataT&

/**
 * @brief == operator
 * @param[in] other Item to be compared with
 * @return \c true if both \c tag and \c data are equal
 */
template<class TagT, class DataT>
bool TaggedDataC<TagT, DataT>::operator==(const TaggedDataC& other) const
{
    return tag==other.tag && data==other.data;
}   //bool operator==(const TaggedDataC& other) const

/**
 * @brief != operator
 * @param[in] other Item to be compared with
 * @return \c false if both \c tag and \c data are equal
 */
template<class TagT, class DataT>
bool TaggedDataC<TagT, DataT>::operator!=(const TaggedDataC& other) const
{
    return !(*this==other);
}   //bool operator==(const TaggedDataC& other) const


}   //DataStructuresN
}	//SeeSawN

#endif /* TAGGED_DATA_IPP_8234132 */
