/**
 * @file OrderedBuffer.ipp Implementation of OrderedBufferC
 * @author Evren Imre
 * @date 21 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ORDERED_BUFFER_IPP_0892132
#define ORDERED_BUFFER_IPP_0892132

#include <boost/concept_check.hpp>
#include <cstddef>
#include <iterator>

namespace SeeSawN
{
namespace DataStructuresN
{

using boost::AssociativeContainerConcept;
using std::size_t;
using std::next;
using std::prev;

/**
 * @brief A sorted buffer that only keeps the top-N elements
 * @tparam ContainerT An associative container
 * @pre \c ContainerT is an associative container
 * @ingroup Utility
 * @nosubgrouping
 */
template<class ContainerT>
class OrderedBufferC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((AssociativeContainerConcept<ContainerT>));
	//@endcond

	private:

		/** @name State */ //@{
		ContainerT container;	///< Underlying container
		typename ContainerT::iterator bottomElement;	///< An iterator to the bottom element
		//@}

		/** @name Configuration */ //@{
		size_t capacity;	///< Maximum capacity of the buffer
		//@}

	public:

		/** @name Types */ //@{
		typedef ContainerT container_type;	///< Type of the underlying container
		//@}

		/** @name Constructors */ //@{
		explicit OrderedBufferC(size_t ccapacity=0);	///< Constructor
		//@}

		/** @name Accessors */ //@{
		const ContainerT& Container() const;	///< Returns a constant reference to the underlying container
		//@}

		/** @name Operations */ //@{
		void Resize(size_t newCapacity);	///< Adjusts the capacity
		bool Insert(const typename ContainerT::value_type& value);	///< Inserts a value to the container
		//@}
};	//class OrderedBufferC

/**
 * @brief Constructor
 * @param[in] ccapacity Capacity of the buffer
 */
template<class ContainerT>
OrderedBufferC<ContainerT>::OrderedBufferC(size_t ccapacity) : bottomElement(container.begin()), capacity(ccapacity)
{}

/**
 * @brief Adjusts the capacity
 * @param[in] newCapacity New capacity of the buffer
 * @remarks Deletes the excess elements
 */
template<class ContainerT>
void OrderedBufferC<ContainerT>::Resize(size_t newCapacity)
{
	//If the new capacity is smaller, resize the buffer
	if(newCapacity<capacity)
	{
		if(newCapacity==0)
			container.clear();
		else
			container.erase(next(container.begin(), newCapacity), container.end());

		bottomElement=prev(container.end(),1);
	}

	capacity=newCapacity;
}	//void Resize(size_t newCapacity)

/**
 * @brief Inserts a value to the container
 * @param[in] value Value to be inserted
 * @return \c false if the insertion fails
 */
template<class ContainerT>
bool OrderedBufferC<ContainerT>::Insert(const typename ContainerT::value_type& value)
{
	//If there is no room, insertion may fail
	if(container.size()==capacity)
	{
		//Insertion fails
		if( (capacity==0) || container.value_comp()(*bottomElement, value))
			return false;
		else	//Insertion succeeds, remove the last element
			container.erase(bottomElement);
	}	//if(container.size()>=capacity)

	container.insert(value);
	bottomElement=prev(container.end(),1);

	return true;
}	//bool Insert(typename ContainerT::value_type& value)

/**
 * @brief Returns a constant reference to the underlying container
 * @return A constant reference to the underlying container
 */
template<class ContainerT>
const ContainerT& OrderedBufferC<ContainerT>::Container() const
{
	return container;
}	//const ContainerT Container()

}	//DataStructuresN
}	//SeeSawN

#endif /* ORDERED_BUFFER_IPP_0892132 */
