/**
 * @file TopNBuffer.ipp
 * @author Evren Imre
 * @date 20 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef TOP_N_BUFFER_IPP_8091092
#define TOP_N_BUFFER_IPP_8091092

#include <boost/concept_check.hpp>
#include <cstddef>
#include <algorithm>

#include <iostream>

namespace SeeSawN
{

namespace DataStructuresN
{

using boost::RandomAccessContainer;
using boost::BackInsertionSequence;
using std::size_t;
using std::push_heap;
using std::pop_heap;
using std::sort_heap;

/**
 * @brief An unordered buffer that only keeps the top-N elements
 * @tparam ContainerT Underlying container for the buffer
 * @tparam ComparatorT Compares two values to identify the bottom element in the buffer
 * @pre \c ContainerT is a random access container
 * @pre \c ContainerT is a back insertion sequence
 * @warning The underlying implementation utilises a heap
 * @remarks Lazy heap: No heapification until the container size hits \c capacity
 * @ingroup Utility
 * @nosubgrouping
 */
template<class ContainerT, class ComparatorT>
class TopNBufferC
{

	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((RandomAccessContainer<ContainerT>));
	BOOST_CONCEPT_ASSERT((BackInsertionSequence<ContainerT>));
	//@endcond

	private:

		/** @name State  */ //@{
		ContainerT container;	///< Container
		//@}

		/** @name Configuration */ //@{
		size_t capacity;	///< Maximum capacity of the buffer
		bool flagHeap;	///< \c true if \c container is a heap
		ComparatorT comparator;	///< Comparison function
		//@}

	public:

		/** @name Types */ //@{
		typedef ContainerT container_type;	///< Type of the underlying container
		//@}

		/** @name Constructors */ //@{
		TopNBufferC(size_t ccapacity);	///< Constructor
		//@}

		/** @name Accessors */ //@{
		const ContainerT& Container() const;	///< Returns a constant reference to the underlying container
		//@}

		/** @name Operations */ //@{
		bool Insert(const typename ContainerT::value_type& value);	///< Inserts a value to the container
		ContainerT Sort() const;	///< Returns a sorted copy of \c container
		//@}

};	//class TopNBuffer

/********** IMPLEMENTATION STARTS HERE **********/
/**
 * @brief Constructor
 * @param[in] ccapacity Capacity of the buffer
 */
template<class ContainerT, class ComparatorT>
TopNBufferC<ContainerT, ComparatorT>::TopNBufferC(size_t ccapacity) : capacity(ccapacity), flagHeap(false)
{
	container.reserve(capacity);
}	//TopNBufferC(size_t ccapacity)

/**
 * @brief Inserts a value to the container
 * @param[in] value Value to be inserted
 * @return \c false if the insertion fails
 */
template<class ContainerT, class ComparatorT>
bool TopNBufferC<ContainerT, ComparatorT>::Insert(const typename ContainerT::value_type& value)
{
	//If the capacity is full, insertion can fail
	if(container.size() == capacity)
	{
		if(!flagHeap)
		{
			make_heap(container.begin(), container.end(), comparator);
			flagHeap=true;
		}

		//0 capacity or no insertion, quit
		if(capacity==0 || comparator(container.front(), value))
			return false;

		//Replace the top element
		pop_heap(container.begin(), container.end(), comparator);	//Moves the front element to back
		container.back()=value;	//Discard by overwriting
		push_heap(container.begin(), container.end(), comparator);	//Push the new element into the heap
	}	//if(container.size() == capacity)
	else
		container.push_back(value);	//If there is room, ordinary insertion

	return true;
}	//bool Insert(const typename ContainerT::value_type& value)

/**
 * @brief Returns a sorted copy of \c container
 * @return A sorted copy of \c container
 */
template<class ContainerT, class ComparatorT>
ContainerT TopNBufferC<ContainerT, ComparatorT>::Sort() const
{
	ContainerT output(container);
	sort_heap(output.begin(), output.end(), comparator);

	return output;
}	//ContainerT Sort() const

/**
 * @brief Returns a constant reference to the underlying container
 * @return A constant reference to the underlying container
 */
template<class ContainerT, class ComparatorT>
const ContainerT& TopNBufferC<ContainerT, ComparatorT>::Container() const
{
	return container;
}	//const ContainerT Container()
}	//DataStructuresN

}	//SeeSawN



#endif /* TOP_N_BUFFER_IPP_8091092 */
