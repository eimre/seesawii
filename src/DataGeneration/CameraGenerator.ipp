/**
 * @file CameraGenerator.ipp Implementation details for camera generators
 * @author Evren Imre
 * @date 4 Aug 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CAMERA_GENERATOR_IPP_6109232
#define CAMERA_GENERATOR_IPP_6109232

#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <array>
#include <random>
#include <tuple>
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Geometry/Rotation.h"
#include "../Geometry/LensDistortion.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace DataGeneratorN
{

using boost::math::constants::pi;
using Eigen::Matrix;
using std::array;
using std::uniform_real_distribution;
using std::tuple;
using std::get;
using std::make_tuple;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::CoordinateVectorT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Generates a random orientation vector
 * @tparam RNGT Random number generator
 * @param[in,out] rng Random number generator
 * @param[in] lowerBound Lower bound for the orientation: rotation around x,y and z axes, in radians
 * @param[in] upperBound Upper bound for the orientation: rotation around x,y and z axes, in radians
 * @pre \c RNGT is a C++ random number generator (unenforced)
 * @pre The entries of \c lowerBound are <= those of \c upperBound
 * @return A random quaternion
 * @remarks Internally, the function generates a unit-norm direction vector
 * @ingroup DataGeneration
 */
template<class RNGT>
QuaternionT GenerateRandomOrientation(RNGT& rng, const array<typename QuaternionT::Scalar,3>& lowerBound, const array<typename QuaternionT::Scalar,3>& upperBound)
{
	//Preconditions
	assert(lowerBound[0]<=upperBound[0]);
	assert(lowerBound[1]<=upperBound[1]);
	assert(lowerBound[2]<=upperBound[2]);

	typedef typename QuaternionT::Scalar RealT;

	RotationVectorT rotation;
	for(size_t c=0; c<3; ++c)
		rotation[c]=uniform_real_distribution<RealT>(lowerBound[c], upperBound[c])(rng);

	rotation.normalize();

	return RotationVectorToQuaternion(rotation);
}	//QuaternionT GenerateRandomOrientation(RNGT& rng, const array<typename QuaternionT::Scalar,3>& lowerBound, const array<typename QuaternionT::Scalar,3>& upperBound)

/**
 * @brief Generates a random set of intrinsic parameters
 * @tparam RNGT Random number generator
 * @param[in,out] rng Random number generator
 * @param[in] lowerBound Lower bound for the intrinsic calibration parameters
 * @param[in] upperBound Upper bound for the intrinsic calibration parameters
 * @pre \c RNGT is a C++ random number generator (unenforced)
 * @pre The entries of \c lowerBound are <= those of \c upperBound
 * @pre \c aspectRatio bounds are positive
 * @pre If valid \c focalLength bounds are positive
 * @return An intrinsic parameter object
 * @remarks If a member of \c lowerBound or \c upperBound is invalid, the corresponding entry in the output remains invalid
 * @ingroup DataGeneration
 */
template<class RNGT>
IntrinsicC GenerateRandomIntrinsics(RNGT& rng, const IntrinsicC& lowerBound, const IntrinsicC& upperBound)
{
	//Preconditions
	assert(lowerBound.aspectRatio<=upperBound.aspectRatio);
	assert(lowerBound.skewness<=upperBound.skewness);
	assert(lowerBound.aspectRatio>0);
	assert(!lowerBound.focalLength || lowerBound.focalLength>0);
	assert( !(lowerBound.focalLength && upperBound.focalLength) || (*lowerBound.focalLength <= *upperBound.focalLength) );
	assert( !(lowerBound.principalPoint && upperBound.principalPoint) ||  ( (lowerBound.principalPoint->array() <= upperBound.principalPoint->array())).all() );

	typedef typename IntrinsicC::real_type RealT;
	typedef uniform_real_distribution<RealT> UrnT;

	IntrinsicC output;
	output.aspectRatio=UrnT(lowerBound.aspectRatio, upperBound.aspectRatio)(rng);
	output.skewness=UrnT(lowerBound.skewness, upperBound.skewness)(rng);

	if(lowerBound.focalLength && upperBound.focalLength)
		output.focalLength=UrnT(*lowerBound.focalLength, *upperBound.focalLength)(rng);

	if(lowerBound.principalPoint && upperBound.principalPoint)
		output.principalPoint=Coordinate2DT( UrnT( (*lowerBound.principalPoint)[0], (*upperBound.principalPoint)[0] )(rng), UrnT( (*lowerBound.principalPoint)[1], (*upperBound.principalPoint)[1] )(rng) );

	return output;
}	//IntrinsicC GenerateRandomIntrinsics(RNGT& rng, const IntrinsicC& lowerBound, const IntrinsicC& upperBound)

/**
 * @brief Generates a random lens distortion
 * @tparam RNGT Random number generator
 * @param[in,out] rng Random number generator
 * @param lowerBound Lower bound for the distortion parameters
 * @param upperBound Upper bound for the distortion parameters
 * @pre \c RNGT is a C++ random number generator (unenforced)
 * @pre The entries of \c lowerBound are <= those of \c upperBound
 * @pre \c lowerBound and \c upperBound has the same distortion types
 * @return A lens distortion object
 * @remarks If the lens distortion type is NOD, all parameters are set to 0
 * @ingroup DataGeneration
 */
template<class RNGT>
LensDistortionC GenerateRandomDistortion(RNGT& rng, const LensDistortionC& lowerBound, const LensDistortionC& upperBound)
{
	//Preconditions
	assert(lowerBound.kappa <= upperBound.kappa);
	assert((lowerBound.centre.array() <= upperBound.centre.array()).all());
	assert(lowerBound.modelCode == upperBound.modelCode);

	typedef typename IntrinsicC::real_type RealT;
	typedef uniform_real_distribution<RealT> UrnT;

	LensDistortionC output;
	output.modelCode=lowerBound.modelCode;

	bool flagNOD= output.modelCode==LensDistortionCodeT::NOD;	//No distortion
	output.kappa= flagNOD ? 0: UrnT(lowerBound.kappa, upperBound.kappa)(rng);
	output.centre[0]=flagNOD ? 0 : UrnT(lowerBound.centre[0], upperBound.centre[0])(rng);
	output.centre[1]=flagNOD ? 0 : UrnT(lowerBound.centre[1], upperBound.centre[1])(rng);

	return output;
}	//LensDistortionC GenerateRandomDistortion(RNGT& rng, const LensDistortionC& lowerBound, const LensDistortionC& upperBound)

/**
 * @brief Generates a random camera relative to a fixation point
 * @tparam RNGT Random number generator
 * @param[in,out] rng Random number generator
 * @param[in] fixationPoint Fixation point
 * @param[in] frameBox Frame box
 * @param[in] lowerBound Lower bound for the random values. [Distance to the fixation point; orientation; intrinsics; lens distortion]
 * @param[in] upperBound Upper bound for the random values. [Distance to the fixation point; orientation; intrinsics; lens distortion]
 * @pre \c RNGT is a C++ random number generator (unenforced)
 * @pre \c 0<get<0>(lowerBound)<=get<0>(upperBound) (distance)
 * @return A random camera object
 * @remarks If any component is invalid, the corresponding element in the output will be invalid as well
 * @ingroup DataGeneration
 */
template<class RNGT>
CameraC GenerateRandomCamera(RNGT& rng, const Coordinate3DT& fixationPoint, const FrameT& frameBox, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& lowerBound, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& upperBound)
{
	//Preconditions
	assert(get<0>(lowerBound)<=get<0>(upperBound));

	typedef typename CameraC::real_type RealT;
	typedef uniform_real_distribution<RealT>  UrnT;

	//Generate the orientation, intrinsics and distortion
	QuaternionT q=GenerateRandomOrientation(rng, get<1>(lowerBound), get<1>(upperBound));
	IntrinsicC intrinsics=GenerateRandomIntrinsics(rng, get<2>(lowerBound), get<2>(upperBound));
	LensDistortionC distortion=GenerateRandomDistortion(rng, get<3>(lowerBound), get<3>(upperBound));

	//Generate the camera centre
	RealT r=UrnT(get<0>(lowerBound), get<0>(upperBound))(rng);	//Distance to the fixation point
	RotationVectorT principalAxis=q.matrix().row(2);	//Principal axis direction
	Coordinate3DT vC= fixationPoint-r*principalAxis;

	ExtrinsicC extrinsics;
	extrinsics.orientation=q;
	extrinsics.position=vC;

	return CameraC(extrinsics, intrinsics, distortion, frameBox);
}	//CameraC GenerateRandomCamera(RNGT& rng, const CoordinateVectorT<typename CameraC::real_type,3>& lowerCentre, const CoordinateVectorT<typename CameraC::real_type,3>& upperCentre, const array<typename CameraC::real_type, 3>& lowerOrientation, const array<typename CameraC::real_type,3>& upperOrientation, const IntrinsicC& lowerIntrinsic, const IntrinsicC& upperIntrinsic, const LensDistortionC& lowerDistortion, const LensDistortionC& upperDistortion);

/**
 * @brief Generates a camera pair fixated on a point
 * @tparam RNGT Random number generator
 * @param[in,out] rng Random number generator
 * @param[in] fixationPoint Fixation point
 * @param[in] frameBox Frame box
 * @param[in] lowerBound1 Lower bound for the random values for the first camera. [Distance to the fixation point; orientation; intrinsics; lens distortion]. The distance component is used for the second camera as well
 * @param[in] upperBound1 Upper bound for the random values for the first camera. [Distance to the fixation point; orientation; intrinsics; lens distortion]. The distance component is used for the second camera as well
 * @param[in] minBaseline Lower bound for the baseline angle between the first and the second camera. Radians.
 * @param[in] maxBaseline Upper bound for the baseline angle between the first and the second camera. Radians.
 * @pre \c RNGT is a C++ random number generator (unenforced)
 * @pre \c 0<get<0>(lowerBound1)<=get<0>(upperBound1) (distance)
 * @pre \c 0<minBaseline<maxBaseline
 * @return A pair of random cameras
 * @remarks The function generates the second camera by randomly offsetting the first camera by a rotation around the fixation point.
 * @ingroup DataGeneration
 */
template<class RNGT>
tuple<CameraC, CameraC> GenerateRandomCameraPair(RNGT& rng, const Coordinate3DT& fixationPoint, const FrameT& frameBox, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& lowerBound1, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& upperBound1, double minBaseline, double maxBaseline)
{
	//Preconditions
	assert(minBaseline<=maxBaseline);
	assert(minBaseline>0);
	assert(get<0>(lowerBound1)<=get<0>(upperBound1));
	assert(get<0>(lowerBound1)>0);


	//Generate the cameras
	CameraC camera1=GenerateRandomCamera(rng, fixationPoint, frameBox, lowerBound1, upperBound1);
	CameraC camera2=GenerateRandomCamera(rng, fixationPoint, frameBox, lowerBound1, upperBound1);

	//Replace the extrinsics for the second camera
	typedef typename CameraC::real_type RealT;
	typedef uniform_real_distribution<RealT>  UrnT;

	RealT r=UrnT(get<0>(lowerBound1), get<0>(upperBound1))(rng);	//Distance to the fixation point
	RealT baseline=UrnT(minBaseline, maxBaseline)(rng);	//Baseline angle

	//Compute the new orientation
	//Apply a random rotation about the normal vector of the x-axis plane of the first camera
	RotationVectorT vNx=camera1.MakeCameraMatrix()->row(0).segment(0,3).normalized();	//Normal of the x-axis plane. Camera matrix is guaranteed to be valid by construction
	QuaternionT q= RotationVectorToQuaternion(baseline*vNx);	//Rotate around the normal
	QuaternionT q2=q*(*camera1.Extrinsics()->orientation);	//Apply the rotation
	RotationMatrix3DT mR2=q2.matrix();

	Coordinate3DT vC2=fixationPoint-r*mR2.row(2).transpose();	//Camera centre: Move along the principal axis vector to the camera centre

	camera2.SetExtrinsics(vC2, mR2);

	return make_tuple(camera1, camera2);
}	//tuple<CameraC, CameraC> GenerateRandomCameraPair(RNGT& rng, const Coordinate3DT& fixationPoint, const FrameT& frameBox, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& lowerBound1, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& upperBound1, double minBaseline, double maxBaseline)

}	//DataGeneratorN
}	//SeeSawN

#endif /* CAMERAGENERATOR_IPP_ */
