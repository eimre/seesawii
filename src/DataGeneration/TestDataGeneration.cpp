/**
 * @file TestDataGeneration.cpp Unit tests for DataGenerataion
 * @author Evren Imre
 * @date 10 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE DATA_STRUCTURES

#include <boost/test/unit_test.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <array>
#include <cstddef>
#include <random>
#include <tuple>
#include <cmath>
#include "LatticeGenerator.h"
#include "CameraGenerator.h"
#include "GeometryProblemGenerator.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Geometry/Rotation.h"
#include "../Geometry/LensDistortion.h"
#include "../Metrics/CheiralityConstraint.h"

namespace SeeSawN
{
namespace TestDataGenerationN
{

using namespace SeeSawN::DataGeneratorN;

using boost::math::constants::pi;
using boost::math::constants::half_pi;
using boost::optional;
using Eigen::Array;
using Eigen::AlignedBox;
using std::vector;
using std::array;
using std::size_t;
using std::mt19937_64;
using std::make_tuple;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::MetricsN::CheiralityConstraintC;

BOOST_AUTO_TEST_SUITE(Point_Cloud_Generator)

BOOST_AUTO_TEST_CASE(Rectangular_Lattice_Generator3)
{

	Array<double, 3, 2> extents; extents<<-1, 1, 0, 2, -3, 3;
	vector<Coordinate3DT> lattice=GenerateRectangularLattice(extents, 2);

	BOOST_CHECK_EQUAL(lattice.size(), 192);

	Coordinate3DT point0(-1,0,-3);
	Coordinate3DT point23(1,2,3);

	BOOST_CHECK(point0.isApprox(lattice[0], 1e-6) );
	BOOST_CHECK(point23.isApprox(lattice[191], 1e-6) );
}	//BOOST_AUTO_TEST_CASE(Rectangular_Lattice_Generator3)

BOOST_AUTO_TEST_CASE(Random_Rectangular_Lattice_Generator)
{
	typedef ValueTypeM<Coordinate3DT>::type RealT;
	vector<Coordinate3DT> lattice=GenerateRandomRectangularLattice<RealT,3>(Coordinate3DT(0,-1, -2), Coordinate3DT(0,1,2), 1);
	BOOST_CHECK_EQUAL(lattice.size(),8);

	AlignedBox<RealT, 3> box(Coordinate3DT(0,-1,-2), Coordinate3DT(0,1,2));
	for(const auto& current : lattice)
		BOOST_CHECK(box.contains(current));

	//Empty set
	BOOST_CHECK( (GenerateRandomRectangularLattice<RealT,3>(Coordinate3DT(0,-1, -2), Coordinate3DT(0,1,2), 0).empty()) );
	BOOST_CHECK( (GenerateRandomRectangularLattice<RealT,3>(Coordinate3DT(0,-1, -2), Coordinate3DT(0,1,2), -1).empty()) );

}	//BOOST_AUTO_TEST_CASE(Random_Rectangular_Lattice_Generator)

BOOST_AUTO_TEST_SUITE_END()	//Point_Cloud_Generator

BOOST_AUTO_TEST_SUITE(Camera_Generator)

BOOST_AUTO_TEST_CASE(Component_Generator)
{
	typedef ValueTypeM<Coordinate3DT>::type RealT;

	mt19937_64 rng;

	//GenerateRandomOrientation

	array<RealT,3> lowerBoundO1{-half_pi<RealT>(), -0.25*pi<RealT>(), -0.125*pi<RealT>()};
	array<RealT,3> upperBoundO1{half_pi<RealT>(), 0.25*pi<RealT>(), 0.125*pi<RealT>()};
	RotationVectorT rv1= QuaternionToRotationVector(GenerateRandomOrientation(rng, lowerBoundO1, upperBoundO1));

	for(size_t c=0; c<3; ++c)
		BOOST_CHECK(rv1[c] <= upperBoundO1[c] && rv1[c]>=lowerBoundO1[c]);

	//GenerateRandomIntrinsics

	IntrinsicC lowerBoundI2;
	lowerBoundI2.aspectRatio=0.97;
	lowerBoundI2.focalLength=1000;
	lowerBoundI2.skewness=1e-5;
	lowerBoundI2.principalPoint=Coordinate2DT(950, 530);

	IntrinsicC upperBoundI2;
	upperBoundI2.aspectRatio=1.03;
	upperBoundI2.focalLength=1500;
	upperBoundI2.skewness=5e-5;
	upperBoundI2.principalPoint=Coordinate2DT(970, 550);

	IntrinsicC rIntrinsic2=GenerateRandomIntrinsics(rng, lowerBoundI2, upperBoundI2);

	BOOST_CHECK(rIntrinsic2.aspectRatio>=lowerBoundI2.aspectRatio && rIntrinsic2.aspectRatio<=upperBoundI2.aspectRatio);
	BOOST_CHECK(rIntrinsic2.skewness>=lowerBoundI2.skewness && rIntrinsic2.skewness<=upperBoundI2.skewness);
	BOOST_CHECK(rIntrinsic2.focalLength);
	BOOST_CHECK(*rIntrinsic2.focalLength>= *lowerBoundI2.focalLength && *rIntrinsic2.focalLength<= *upperBoundI2.focalLength);
	BOOST_CHECK(rIntrinsic2.principalPoint);
	BOOST_CHECK( (rIntrinsic2.principalPoint->array() >= lowerBoundI2.principalPoint->array()).all() && (rIntrinsic2.principalPoint->array() <= upperBoundI2.principalPoint->array()).all());

	//Invalid components

	IntrinsicC lowerBoundI3(lowerBoundI2);
	lowerBoundI3.focalLength=optional<RealT>();

	IntrinsicC upperBoundI3(upperBoundI2);
	upperBoundI3.focalLength=optional<RealT>();
	upperBoundI3.principalPoint=optional<Coordinate2DT>();

	IntrinsicC rIntrinsic3=GenerateRandomIntrinsics(rng, lowerBoundI3, upperBoundI3);

	BOOST_CHECK(!rIntrinsic3.focalLength);
	BOOST_CHECK(!rIntrinsic3.principalPoint);

	//GenerateRandomLensDistortion

	LensDistortionC lowerBoundLD4;
	lowerBoundLD4.modelCode=LensDistortionCodeT::DIV;
	lowerBoundLD4.kappa=1e-8;
	lowerBoundLD4.centre=Coordinate2DT(950,530);

	LensDistortionC upperBoundLD4;
	upperBoundLD4.modelCode=LensDistortionCodeT::DIV;
	upperBoundLD4.kappa=5e-8;
	upperBoundLD4.centre=Coordinate2DT(970,550);

	LensDistortionC rDistortion4=GenerateRandomDistortion(rng, lowerBoundLD4, upperBoundLD4);
	BOOST_CHECK(rDistortion4. modelCode==lowerBoundLD4.modelCode);
	BOOST_CHECK(rDistortion4.kappa>=lowerBoundLD4.kappa && rDistortion4.kappa<=upperBoundLD4.kappa);
	BOOST_CHECK( (rDistortion4.centre.array() >= lowerBoundLD4.centre.array()).all() && (rDistortion4.centre.array() <= upperBoundLD4.centre.array()).all());
}	//BOOST_AUTO_TEST_CASE(Component_Generator)

BOOST_AUTO_TEST_CASE(Random_Camera_Generator)
{
	typedef ValueTypeM<Coordinate3DT>::type RealT;

	//GenerateRandomCamera

	FrameT frameBox(Coordinate2DT(0,0), Coordinate2DT(1919,1079));
	Coordinate3DT fixationPoint(2,5,1);

	mt19937_64 rng;

	array<RealT,3> lowerBoundO{-half_pi<RealT>(), -0.25*pi<RealT>(), -0.125*pi<RealT>()};
	array<RealT,3> upperBoundO{half_pi<RealT>(), 0.25*pi<RealT>(), 0.125*pi<RealT>()};

	IntrinsicC lowerBoundI;
	lowerBoundI.aspectRatio=0.97;
	lowerBoundI.focalLength=1000;
	lowerBoundI.skewness=1e-5;
	lowerBoundI.principalPoint=Coordinate2DT(950, 530);

	IntrinsicC upperBoundI;
	upperBoundI.aspectRatio=1.03;
	upperBoundI.focalLength=1500;
	upperBoundI.skewness=5e-5;
	upperBoundI.principalPoint=Coordinate2DT(970, 550);

	LensDistortionC lowerBoundLD;
	lowerBoundLD.modelCode=LensDistortionCodeT::DIV;
	lowerBoundLD.kappa=1e-8;
	lowerBoundLD.centre=Coordinate2DT(950,530);

	LensDistortionC upperBoundLD;
	upperBoundLD.modelCode=LensDistortionCodeT::DIV;
	upperBoundLD.kappa=5e-8;
	upperBoundLD.centre=Coordinate2DT(970,550);

	RealT minDistance=1;
	RealT maxDistance=5;

	CameraC camera=GenerateRandomCamera(rng, fixationPoint, frameBox, make_tuple(minDistance, lowerBoundO, lowerBoundI, lowerBoundLD), make_tuple(maxDistance, upperBoundO, upperBoundI, upperBoundLD) );

	CheiralityConstraintC cheirality(*camera.MakeCameraMatrix());
	BOOST_CHECK(cheirality(fixationPoint));
	BOOST_CHECK(cheirality.Distance(fixationPoint)<=maxDistance);
	BOOST_CHECK(cheirality.Distance(fixationPoint)>=minDistance);

	//GenerateRandomCameraPair
	CameraC camera2;
	CameraC camera3;
	tie(camera2, camera3)=GenerateRandomCameraPair(rng, fixationPoint, frameBox, make_tuple(minDistance, lowerBoundO, lowerBoundI, lowerBoundLD), make_tuple(maxDistance, upperBoundO, upperBoundI, upperBoundLD), 0, pi<double>()/4 );

	CheiralityConstraintC cheirality2(*camera2.MakeCameraMatrix());
	BOOST_CHECK(cheirality2(fixationPoint));
	BOOST_CHECK(cheirality2.Distance(fixationPoint)<=maxDistance);
	BOOST_CHECK(cheirality2.Distance(fixationPoint)>=minDistance);

	CheiralityConstraintC cheirality3(*camera3.MakeCameraMatrix());
	BOOST_CHECK(cheirality3(fixationPoint));
	BOOST_CHECK(cheirality3.Distance(fixationPoint)<=maxDistance);
	BOOST_CHECK(cheirality3.Distance(fixationPoint)>=minDistance);

	double baseline2=acos(camera2.Extrinsics()->orientation->matrix().row(2).dot(camera3.Extrinsics()->orientation->matrix().row(2)));
	BOOST_CHECK(baseline2>=0 && baseline2<=pi<double>()/4);
}	//BOOST_AUTO_TEST_CASE(Random_Camera_Generator)

BOOST_AUTO_TEST_SUITE_END()	//Camera_Generator

BOOST_AUTO_TEST_SUITE(Problem_Generator)

BOOST_AUTO_TEST_CASE(Geometry_Problem_Generator)
{
	typedef ValueTypeM<Coordinate3DT>::type RealT;

	FrameT frameBox(Coordinate2DT(0,0), Coordinate2DT(1919,1079));
	Coordinate3DT fixationPoint(2,5,1);

	mt19937_64 rng;

	array<RealT,3> lowerBoundO{-half_pi<RealT>(), -0.25*pi<RealT>(), -0.125*pi<RealT>()};
	array<RealT,3> upperBoundO{half_pi<RealT>(), 0.25*pi<RealT>(), 0.125*pi<RealT>()};

	IntrinsicC lowerBoundI;
	lowerBoundI.aspectRatio=0.97;
	lowerBoundI.focalLength=1000;
	lowerBoundI.skewness=1e-5;
	lowerBoundI.principalPoint=Coordinate2DT(950, 530);

	IntrinsicC upperBoundI;
	upperBoundI.aspectRatio=1.03;
	upperBoundI.focalLength=1500;
	upperBoundI.skewness=5e-5;
	upperBoundI.principalPoint=Coordinate2DT(970, 550);

	LensDistortionC lowerBoundLD;
	lowerBoundLD.modelCode=LensDistortionCodeT::DIV;
	lowerBoundLD.kappa=1e-8;
	lowerBoundLD.centre=Coordinate2DT(950,530);

	LensDistortionC upperBoundLD;
	upperBoundLD.modelCode=LensDistortionCodeT::DIV;
	upperBoundLD.kappa=5e-8;
	upperBoundLD.centre=Coordinate2DT(970,550);

	RealT minDistance=1;
	RealT maxDistance=5;

	RandomGeometryProblemGeneratorC::camera_bound_type lowerBound=make_tuple(minDistance, lowerBoundO, lowerBoundI, lowerBoundLD);
	RandomGeometryProblemGeneratorC::camera_bound_type upperBound=make_tuple(maxDistance, upperBoundO, upperBoundI, upperBoundLD);

	VolumeT referenceVolume( 0.5*fixationPoint, 2*fixationPoint);
	vector<RandomGeometryProblemGeneratorC::structure_parameter_type > structureData={ {1,1,2}, {0.4, 0.6, 2}, {0.1,0.5,3} };

	CameraC camera1a;
	CameraC camera1b;
	vector<CoordinateCorrespondenceList2DT> structures1;
	std::tie(structures1, camera1a, camera1b)=RandomGeometryProblemGeneratorC::GenerateProblem22(rng, frameBox, referenceVolume, structureData, lowerBound, upperBound, 0, pi<double>()/4, 0.8, true);
	std::tie(structures1, camera1a, camera1b)=RandomGeometryProblemGeneratorC::GenerateProblem22(rng, frameBox, referenceVolume, structureData, lowerBound, upperBound, 0, pi<double>()/4, 0.8, false);
}	//BOOST_AUTO_TEST_CASE(Geometry_Problem_Generator)

BOOST_AUTO_TEST_SUITE_END()	//Problem_Generator

}	//TestDataGenerationN
}	//SeeSawN

