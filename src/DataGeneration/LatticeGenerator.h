/**
 * @file LatticeGenerator.h Public interface for various lattice generators
 * @author Evren Imre
 * @date 9 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LATTICE_GENERATOR_H_0588921
#define LATTICE_GENERATOR_H_0588921

#include "LatticeGenerator.ipp"
namespace SeeSawN
{
namespace DataGeneratorN
{

vector<Coordinate3DT> GenerateRectangularLattice(const Array<ValueTypeM<Coordinate3DT>::type, 3, 2>& extents, double density);	///< Generates a regular rectangular 3D lattice

template<typename ValueT, unsigned int DIM> vector< CoordinateVectorT<ValueT, DIM> > GenerateRandomRectangularLattice(const CoordinateVectorT<ValueT, DIM>& minCorner, const CoordinateVectorT<ValueT, DIM>& maxCorner, double density);	///< Generates a random rectangular lattice

}	//DataGeneratorN
}	//SeeSawN

#endif /* LATTICE_GENERATOR_H_0588921 */
