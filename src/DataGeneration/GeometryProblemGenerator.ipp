/**
 * @file GeometryProblemGenerator.ipp Implementation of \c RandomGeometryProblemGeneratorC
 * @author Evren Imre
 * @date 6 Aug 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRY_PROBLEM_GENERATOR_IPP_5902142
#define GEOMETRY_PROBLEM_GENERATOR_IPP_5902142

#include <boost/math/constants/constants.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <array>
#include <tuple>
#include <random>
#include <iterator>
#include "LatticeGenerator.h"
#include "CameraGenerator.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Camera.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/Projection32.h"
#include "../Metrics/CheiralityConstraint.h"

namespace SeeSawN
{
namespace DataGeneratorN
{

using boost::math::constants::pi;
using boost::range::transform;
using boost::range::transform;
using boost::optional;
using Eigen::Matrix;
using Eigen::Hyperplane;
using std::vector;
using std::array;
using std::back_inserter;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::uniform_real_distribution;
using SeeSawN::DataGeneratorN::GenerateRandomCameraPair;
using SeeSawN::DataGeneratorN::GenerateRandomRectangularLattice;
using SeeSawN::DataGeneratorN::GenerateRandomOrientation;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::VolumeT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::GeometryN::CoordinateTransformations3DT;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::MetricsN::CheiralityConstraintC;

/**
 * @brief Generates random problems for geometry estimators
 * @remarks No dedicated unit tests beyond compilation
 * @ingroup DataGeneration
 * @nosubgrouping
 */
class RandomGeometryProblemGeneratorC
{
	private:

		typedef CameraC::real_type RealT;	///< Floating point type
		typedef Matrix<RealT,3,1> Vector3T;	///< 3D vector

		typedef array<RealT, 3> StructureT;	///< Defines a 3D point cloud. [Lower bound of the random scale; upper bound of the random scale; Density]
		typedef tuple<RealT, array<RealT,3>, IntrinsicC, LensDistortionC> CameraBoundT;	///< Upper or lower bound type for random camera generation

		/** @name Implementation details */ //@{
		template<class RNGT> static vector<Coordinate3DT> ApplyRandom3DTransformation(RNGT& rng, const vector<Coordinate3DT>& source, const VolumeT& captureVolume, const CameraC& camera1, const CameraC& camera2);	///< Applies a random 3D transformation to a point cloud
		static vector<Coordinate3DT> MakePlane(const vector<Coordinate3DT>& source, const Vector3T& normal, const Coordinate3DT& point);	///< Project a general 3D point cloud onto a plane within the capture volume, and parallel to the image plane of the camera
		//@}

	public:

		/** @name StructureT */ //@{
		typedef StructureT structure_parameter_type;	///< Structure parameters
		static constexpr unsigned int iMinScale=0;	///< Index of the minimum scale component
		static constexpr unsigned int iMaxScale=1;	///< Index of the maximum scale component
		static constexpr unsigned int iDensity=2;	///< Index of the density component
		//@}

		/** @name CameraBoundT */ //@{
		typedef CameraBoundT camera_bound_type;		///< Lower and upper bounds for the camera parameters
		static constexpr unsigned int iDistance=0;	///< Index of the distance component
		static constexpr unsigned int iOrientation=1;	///< Index of the orientation component
		static constexpr unsigned int iIntrinsics=2;	///< Index of the intrinsics component
		static constexpr unsigned int iLensDistortion=3;	///< Index of the lens distortion component
		//@}

		template<class RNGT> static tuple<vector<CoordinateCorrespondenceList2DT>, CameraC, CameraC> GenerateProblem22(RNGT& rng, const FrameT& frameBox, const VolumeT& captureVolume, const vector<StructureT>& structureList, const CameraBoundT& lowerBound, const CameraBoundT& upperBound, double minBaseline, double maxBaseline,  double fixationRadius=0.8, bool flagPlanar=false);		///< Generates a problem for solvers involving a camera pair and 2D correspondences

};	//class RandomGeometryProblemGeneratorC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Applies a random 3D transformation to a point cloud
 * @tparam RNGT Random number generator type
 * @param[in,out] rng Random number generator
 * @param[in] source Point cloud to be transformed
 * @param[in] captureVolume Capture volume
 * @param[in] camera1 First camera
 * @param[in] camera2 Second camera
 * @pre \c RNGT is a c++ pseudo-random number generation engine (unenforced)
 * @return Transformed point cloud
 * @remarks The function translates the point cloud to a random location within \c captureVolume, and then applies a random rotation. The new origin is visible to \c camera
 */
template<class RNGT>
vector<Coordinate3DT> RandomGeometryProblemGeneratorC::ApplyRandom3DTransformation(RNGT& rng, const vector<Coordinate3DT>& source, const VolumeT& captureVolume, const CameraC& camera1, const CameraC& camera2)
{
	//Find a new origin within the viewing frustums of both cameras
	//Since the cameras are pointed towards a point within the capture volume, this will eventually be successful
	Projection32C projector1(*camera1.MakeCameraMatrix());
	FrameT imageFrame1=*camera1.FrameBox();

	Projection32C projector2(*camera2.MakeCameraMatrix());
	FrameT imageFrame2=*camera2.FrameBox();

	Coordinate3DT newOrigin;
	do
	{
		newOrigin=captureVolume.sample();
	}while(false);//imageFrame1.contains(projector1(newOrigin, *camera1.Distortion())) && imageFrame2.contains(projector2(newOrigin, *camera2.Distortion())));
	projector1(newOrigin, *camera1.Distortion());

	Coordinate3DT deltaT=newOrigin-captureVolume.center();		//Translation

	//Rotation
	array<RealT,3> minOrientation{0,0,0};
	array<RealT,3> maxOrientation{2*pi<double>(), 2*pi<double>(), 2*pi<double>()};
	QuaternionT deltaQ=GenerateRandomOrientation(rng, minOrientation, maxOrientation);	//Random orientation

	//Construct the transformation
	typedef typename CoordinateTransformations3DT::affine_transform_type TransformT;
	TransformT mT=TransformT::Identity();	//Transformation
	mT.translate(deltaT); mT.rotate(deltaQ);

	return CoordinateTransformations3DT::ApplyAffineTransformation(source, mT);	//Apply
}	//vector<Coordinate3DT> ApplyRandom3DTransformation(RNGT& rng, const vector<Coordinate3DT>& source, const VolumeT& referenceVolume)

/**
 * @brief Project a general 3D point cloud onto a plane within the capture volume, and parallel to the image plane of the camera
 * @param[in] source Point cloud to be transformed
 * @param[in] normal Plane normal
 * @param[in] point A point on the plane
 * @return Transformed point cloud
 */
vector<Coordinate3DT> RandomGeometryProblemGeneratorC::MakePlane(const vector<Coordinate3DT>& source, const Vector3T& normal, const Coordinate3DT& point)
{
	Hyperplane<RealT, 3> plane(normal, point);
	vector<Coordinate3DT> destination; destination.reserve(source.size());
	transform(source, back_inserter(destination), [&](const Coordinate3DT& current){return plane.projection(current);} );

	return destination;
}	//vector<Coordinate3DT> MakePlane(const vector<Coordinate3DT>& source, const Vector3T& normal, const Coordinate3DT& point)

/**
 * @brief Generates a problem for solvers involving a camera pair and 2D correspondences
 * @tparam RNGT Random number generator type
 * @param[in,out] rng Random number generator
 * @param[in] frameBox 2D box defining the image.
 * @param[in] captureVolume Capture volume. However, random displacements may cause some structures to be outside of the volume.
 * @param[in] structureList Structure parameters. For each structure, the capture volume is randomly scaled. Also, each structure may have a different point density
 * @param[in] lowerBound Lower bound for the camera parameters
 * @param[in] upperBound Upper bound for the camera parameters
 * @param[in] minBaseline Minimum baseline angle between the cameras  (radians)
 * @param[in] maxBaseline Maximum baseline angle between the cameras (radians)
 * @param[in] flagPlanar If \c true the generated structures are 2D planes.
 * @param[in] fixationRadius Maximum distance of the fixation point from the centre of the capture volume, in terms of the half-diagonal. (0,1]
 * @pre \c RNGT is a c++ pseudo-random number generation engine (unenforced)
 * @pre \c fixationRadius is in (0,1]
 * @pre All entries in \c structureList are positive
 * @return A 3-tuple: 2D correspondences for each structure, and a pair of cameras
 * @remarks The function can generate structures satisfying different geometric constraints, i.e., can generate a multi-structure problem
 * @remarks The number of points per unit volume is \c density^3
 * @remarks Points that fail the cheirality constraint, or do not fall within the frame are discarded
 */
template<class RNGT>
auto RandomGeometryProblemGeneratorC::GenerateProblem22(RNGT& rng, const FrameT& frameBox, const VolumeT& captureVolume, const vector<StructureT>& structureList, const CameraBoundT& lowerBound, const CameraBoundT& upperBound, double minBaseline, double maxBaseline, double fixationRadius, bool flagPlanar) -> tuple<vector<CoordinateCorrespondenceList2DT>, CameraC, CameraC>
{
	//Preconditions
	assert(fixationRadius>0 && fixationRadius<=1);
#ifndef NDEBUG
	for(const auto& current : structureList)
	{
		assert(current[iMinScale]>0);
		assert(current[iMaxScale]>0);
		assert(current[iDensity]>0);
	}
#endif

	typedef uniform_real_distribution<RealT> UrnT;

	Coordinate3DT centre=captureVolume.center();
	Vector3T diagonalVector=captureVolume.diagonal();
	RealT sizeBox=diagonalVector.norm();
	diagonalVector.normalize();

	//Generate the cameras

	//Fixation point for the cameras. Reject samples from the boundary, to improve visibility
	Coordinate3DT fixationPoint;
	RealT maxDistance=fixationRadius*0.5*sizeBox;
	do
	{
		fixationPoint=captureVolume.sample();
	}while((fixationPoint-centre).norm()>maxDistance);

	CameraC camera1;
	CameraC camera2;
	tie(camera1, camera2)=GenerateRandomCameraPair(rng, fixationPoint, frameBox, lowerBound, upperBound, minBaseline, maxBaseline);

	//Generate the structure
	//All point clouds following the first one are transformed randomly, to simulate independent motion between the frames
	size_t nStructure=structureList.size();
	vector<CoordinateCorrespondenceList2DT> correspondences(nStructure);
	for(size_t c=0; c<nStructure; ++c)
	{
		//Resize the volume
		RealT newSize=UrnT(structureList[c][iMinScale], structureList[c][iMaxScale])(rng) * sizeBox;	//Length of the diagonal of the volume
		Coordinate3DT bottomCorner= centre - 0.5*newSize*diagonalVector;
		Coordinate3DT topCorner= centre + 0.5*newSize*diagonalVector;

		vector<Coordinate3DT> pointCloud1=GenerateRandomRectangularLattice<RealT,3>(bottomCorner, topCorner, structureList[c][iDensity]);

		//Flatten to a plane parallel to the image plane of the first camera
		if(flagPlanar)
			pointCloud1=MakePlane(pointCloud1, camera1.MakeRotationMatrix()->row(2), captureVolume.sample());

		//If not the first structure, apply a random transformation.
		vector<Coordinate3DT> pointCloud2;
		if(c!=0)
			pointCloud2=ApplyRandom3DTransformation(rng, pointCloud1, captureVolume, camera1, camera2);

		//Project

		size_t nPoints=pointCloud1.size();
		CameraMatrixT mP1=*camera1.MakeCameraMatrix();
		Projection32C projector1(mP1);
		vector<optional<Coordinate2DT> > projected1(nPoints);
		transform(pointCloud1, projected1.begin(), [&](const Coordinate3DT& current){return projector1(current, *camera1.Distortion());}  );

		CameraMatrixT mP2=*camera2.MakeCameraMatrix();
		Projection32C projector2(mP2);
		vector<optional<Coordinate2DT> > projected2(nPoints);
		transform( c==0 ? pointCloud1 : pointCloud2, projected2.begin(), [&](const Coordinate3DT& current){return projector2(current, *camera2.Distortion());}  );

		//Form the correspondence list
		CheiralityConstraintC cheirality1(mP1);
		CheiralityConstraintC cheirality2(mP2);
		CoordinateCorrespondenceList2DT correspondenceList;
		for(size_t c2=0; c2<nPoints; ++c2)
		{
			bool flagSuccess=projected1[c2] && projected2[c2];	//Are the points successfully projected?
			flagSuccess = flagSuccess && frameBox.contains(*projected1[c2]) && frameBox.contains(*projected2[c2]);	//Are they within the image?
			flagSuccess = flagSuccess && cheirality1(pointCloud1[c2]) && cheirality2( c==0 ? pointCloud1[c2] : pointCloud2[c2]);	//Are they in front of the camera?

			if(flagSuccess)
				correspondenceList.push_back(typename CoordinateCorrespondenceList2DT::value_type(*projected1[c2], *projected2[c2], MatchStrengthC(1,0)) );
		}	//for(size_t c2=0; c2<nPoints; ++c2)

		correspondences[c].swap(correspondenceList);
	}	//for(size_t c=0; c<nStructure; ++c)

	return make_tuple(correspondences, camera1, camera2);
}	//tuple<CoordinateCorrespondenceList2DT, CameraC, CameraC> GenerateProblem22(RNGT& rng, const FrameT& frameBox, const VolumeT& referenceVolume, const vector<StructureT>& structureList, const CameraBoundT& lowerBound, const CameraBoundT& upperBound, double minBaseline, double maxBaseline)


}	//DataGeneratorN
}	//SeeSawN

#endif /* GEOMETRY_PROBLEM_GENERATOR_IPP_5902142 */
