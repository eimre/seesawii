/**
 * @file LatticeGenerator.ipp Implementation details for various lattice generators
 * @author Evren Imre
 * @date 9 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LATTICE_GENERATOR_IPP_6980932
#define LATTICE_GENERATOR_IPP_6980932

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <array>
#include <cmath>
#include <cstddef>
#include <type_traits>
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace DataGeneratorN
{

using Eigen::Array;
using Eigen::AlignedBox;
using std::vector;
using std::array;
using std::fabs;
using std::min;
using std::max;
using std::ceil;
using std::size_t;
using std::is_arithmetic;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateVectorT;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Generates a random rectangular lattice
 * @tparam ValueT Coordinate value type
 * @tparam DIM Coordinate dimensionality
 * @param[in] minCorner Minimum coordinate values
 * @param[in] maxCorner Maximum coordinate values
 * @param density Average number of points per unit distance
 * @return Lattice as a vector
 * @pre \c ValueT is an arithmetic type
 * @pre \c maxCorner>=minCorner
 * @ingroup DataGeneration
 */
template<typename ValueT, unsigned int DIM>
vector< CoordinateVectorT<ValueT, DIM> > GenerateRandomRectangularLattice(const CoordinateVectorT<ValueT, DIM>& minCorner, const CoordinateVectorT<ValueT, DIM>& maxCorner, double density)
{
	//Preconditions
	static_assert(is_arithmetic<ValueT>::value, "GenerateRandomRectangularLattice: ValueT must be an arithmetic type");
	assert( ((maxCorner-minCorner)>=0).all() );

	typedef CoordinateVectorT<ValueT,DIM> CoordinateT;
	typedef AlignedBox<ValueT, DIM> BoxT;

	if(density<=0) return vector<CoordinateT>();

	BoxT box(minCorner, maxCorner);
	Array<ValueT, DIM, 1> densityVector=(maxCorner-minCorner) * density;
	double nPoints=1;
	for(size_t c=0; c<DIM; ++c)
		nPoints*=max(densityVector[c],(ValueT)1);

	size_t cPoints=ceil(nPoints);
	vector<CoordinateT> output(cPoints);
	for(size_t c=0; c<cPoints; ++c)
		output[c]=box.sample();

	return output;
}	//vector< CoordinateVectorT<ValueT, DIM> > GenerateRandomRectangularLattice(const CoordinateVectorT<ValueT, DIM>& minCorner, const CoordinateVectorT<ValueT, DIM>& maxCorner, double density)

}	//DataGeneratorN
}	//SeeSawN

#endif /* LATTICE_GENERATOR_IPP_6980932 */
