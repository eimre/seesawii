/**
 * @file CameraGenerator.cpp Instantiations for camera generators
 * @author Evren Imre
 * @date 4 Aug 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CameraGenerator.h"

namespace SeeSawN
{
namespace DataGeneratorN
{

/********** EXPLICIT INSTANTIATIONS **********/
template QuaternionT GenerateRandomOrientation(mt19937_64&, const array<typename QuaternionT::Scalar,3>&, const array<typename QuaternionT::Scalar,3>&);
template IntrinsicC GenerateRandomIntrinsics(mt19937_64&, const IntrinsicC&, const IntrinsicC&);
template LensDistortionC GenerateRandomDistortion(mt19937_64&, const LensDistortionC&, const LensDistortionC&);
template CameraC GenerateRandomCamera(mt19937_64&, const Coordinate3DT&, const FrameT&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&);
template tuple<CameraC, CameraC> GenerateRandomCameraPair(mt19937_64&, const Coordinate3DT&, const FrameT&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&, double, double);

}	//DataGeneratorN
}	//SeeSawN

