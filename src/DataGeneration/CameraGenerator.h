/**
 * @file CameraGenerator.h Public interface for camera generators
 * @author Evren Imre
 * @date 4 Aug 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CAMERA_GENERATOR_H_7012380
#define CAMERA_GENERATOR_H_7012380

#include "CameraGenerator.ipp"

namespace SeeSawN
{
namespace DataGeneratorN
{

template<class RNGT> QuaternionT GenerateRandomOrientation(RNGT& rng, const array<typename QuaternionT::Scalar,3>& lowerBound, const array<typename QuaternionT::Scalar,3>& upperBound);	///< Generates a random orientation vector
template<class RNGT> IntrinsicC GenerateRandomIntrinsics(RNGT& rng, const IntrinsicC& lowerBound, const IntrinsicC& upperBound);	///< Generates a random set of intrinsic parameters
template<class RNGT> LensDistortionC GenerateRandomDistortion(RNGT& rng, const LensDistortionC& lowerBound, const LensDistortionC& upperBound);	///< Generates a random lens distortion
template<class RNGT> CameraC GenerateRandomCamera(RNGT& rng, const Coordinate3DT& fixationPoint, const FrameT& frameBox, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& lowerBound, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& upperBound);	///< Generates a random camera relative to a fixation point
template<class RNGT> tuple<CameraC, CameraC> GenerateRandomCameraPair(RNGT& rng, const Coordinate3DT& fixationPoint, const FrameT& frameBox, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& lowerBound1, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& upperBound1, double minBaseline, double maxBaseline);	///< Generates a camera pair fixated on a point


/********** EXTERN TEMPLATES **********/
using std::mt19937_64;
extern template QuaternionT GenerateRandomOrientation(mt19937_64&, const array<typename QuaternionT::Scalar,3>&, const array<typename QuaternionT::Scalar,3>&);
extern template IntrinsicC GenerateRandomIntrinsics(mt19937_64&, const IntrinsicC&, const IntrinsicC&);
extern template LensDistortionC GenerateRandomDistortion(mt19937_64&, const LensDistortionC&, const LensDistortionC&);
extern template CameraC GenerateRandomCamera(mt19937_64&, const Coordinate3DT&, const FrameT&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&);
extern template tuple<CameraC, CameraC> GenerateRandomCameraPair(mt19937_64&, const Coordinate3DT&, const FrameT&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>&, const tuple<typename CameraC::real_type, array<typename CameraC::real_type,3>, IntrinsicC, LensDistortionC>& , double, double);
}	//DataGeneratorN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::array<double, 3>;
extern template class std::array<float, 3>;


#endif /* CAMERA_GENERATOR_H_7012380 */
