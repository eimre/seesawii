/**
 * @file LatticeGenerator.cpp Implementation of various lattice generators
 * @author Evren Imre
 * @date 9 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "LatticeGenerator.h"
namespace SeeSawN
{
namespace DataGeneratorN
{

/**
 * @brief Generates a regular rectangular 3D lattice
 * @param[in] extents Extent of the lattice in X, Y and Z axes
 * @param[in] density Density of points per unit length
 * @return A vector containing the points on the lattice
 * @pre \c density>0
 * @remarks The number of points per unit volume is \c density^3
 */
vector<Coordinate3DT> GenerateRectangularLattice(const Array<ValueTypeM<Coordinate3DT>::type, 3, 2>& extents, double density)
{
	//Preconditions
	assert(density>0);

	typedef ValueTypeM<Coordinate3DT>::type RealT;
	typedef Array<RealT, Eigen::Dynamic, 1> RealArrayT;

	array<RealArrayT, 3> coordinates;	//Coordinate arrays
	for(size_t c=0; c<3; ++c)
	{
		double low=min(extents(c,0), extents(c,1));
		double high=max(extents(c,0), extents(c,1));
		unsigned int nPoints=ceil((high-low)*density);
		coordinates[c].setLinSpaced(nPoints, low, high);
	}	//for(size_t c=0; c<3; ++c)

	//Generate the 3D points

	size_t nX=coordinates[0].size();
	size_t nY=coordinates[1].size();
	size_t nZ=coordinates[2].size();

	size_t nPoints=nX*nY*nZ;
	vector<Coordinate3DT> output(nPoints);

	size_t index=0;
	for(size_t cz=0; cz<nZ; ++cz)
		for(size_t cy=0; cy<nY; ++cy)
			for(size_t cx=0; cx<nX; ++cx, ++index)
				output[index]=Coordinate3DT(coordinates[0][cx], coordinates[1][cy], coordinates[2][cz]);

	return output;
}	//vector<Coordinate3DT> GenerateRectangularLattice(const Array<ValueTypeM<Coordinate3DT>::type, 3, 2>& extents, double density)

}	//DataGeneratorN
}	//SeeSawN

