/**
 * @file RANSAC.h Public interface for RANSAC
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_H_0901232
#define RANSAC_H_0901232

#include "RANSAC.ipp"

namespace SeeSawN
{
namespace RANSACN
{

struct RANSACParametersC;	///< Parameters for RANSACC
struct RANSACDiagnosticsC;	///< Diagnostics of RANSAC
template<class ProblemT, class RNGT> class RANSACC;	///< RANSAC

}	//RANSACN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template class std::vector<std::size_t>;
extern template class std::vector<double>;
extern template class std::set<unsigned int>;
extern template class std::set<double>;
extern template class std::list<unsigned int>;
extern template class std::multimap<double, unsigned int, std::greater<double> > ;
extern template class std::map<unsigned int, unsigned int>;
extern template class boost::optional<double>;
#endif /* RANSAC_H_0901232 */
