/**
 * @file RANSACGeometryEstimationProblem.ipp Implementation of RANSACGeometryEstimationProblemC
 * @author Evren Imre
 * @date 15 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_GEOMETRY_ESTIMATION_PROBLEM_IPP_6923231
#define RANSAC_GEOMETRY_ESTIMATION_PROBLEM_IPP_6923231

#include <boost/concept_check.hpp>
#include <boost/bimap.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <set>
#include <list>
#include <cstddef>
#include <tuple>
#include "../Elements/Correspondence.h"
#include "../Metrics/GeometricConstraintConcept.h"
#include "../Metrics/GeometricErrorConcept.h"
#include "../Geometry/GeometrySolverConcept.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace RANSACN
{

using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using boost::AdaptableUnaryFunctionConcept;
using boost::optional;
using Eigen::Array;
using std::vector;
using std::set;
using std::list;
using std::size_t;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::MetricsN::GeometricConstraintConceptC;
using SeeSawN::MetricsN::GeometricErrorConceptC;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::GeometryN::CoordinateStatisticsT;
using SeeSawN::GeometryN::CoordinateTransformationsT;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::VectorToBimap;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;

/**
 * @brief RANSAC problem for geometry estimation
 * @tparam MinimalGeometrySolverT A minimal geometry solver
 * @tparam LOGeometrySolverT A geometry solver for the local optimisation step
 * @pre \c MinimalGeometrySolverT::constraint_type is a model of GeometricConstraintConceptC
 * @pre \c MinimalGeometrySolverT::loss_type is a model of AdaptableUnaryFunctionConcept
 * @pre \c MinimalGeometrySolverT is a model of GeometrySolverConceptC
 * @pre \c LOGeometrySolverT is a model of GeometrySolverConceptC
 * @ingroup Problem
 * @nosubgrouping
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT=MinimalGeometrySolverT>
class RANSACGeometryEstimationProblemC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<MinimalGeometrySolverT>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<LOGeometrySolverT>));
	BOOST_CONCEPT_ASSERT((GeometricConstraintConceptC<typename MinimalGeometrySolverT::constraint_type>));
	BOOST_CONCEPT_ASSERT((GeometricErrorConceptC<typename MinimalGeometrySolverT::constraint_type::error_type>));
	BOOST_CONCEPT_ASSERT((AdaptableUnaryFunctionConcept<typename MinimalGeometrySolverT::loss_type, typename MinimalGeometrySolverT::loss_type::result_type, typename MinimalGeometrySolverT::constraint_type::distance_type>));
	///@endcond

	private:

		typedef set<unsigned int> GeneratorT;

		/**@name Configuration */ //@{
		typedef typename MinimalGeometrySolverT::coordinate_type1 Coordinate1T;	///< Type of the first member of a corresponding pair
		typedef typename MinimalGeometrySolverT::coordinate_type2 Coordinate2T;	///< Type of the second member of a corresponding pair
		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CorrespondenceContainerT;	///< A container of correspondences
		typedef typename CorrespondenceContainerT::value_type CorrespondenceT;	///< A correspondence

		//Data
		vector<Coordinate1T> observationSetL;	///< Observation set, first domain (left)
		vector<Coordinate2T> observationSetR;	///< Observation set, second domain (right)
		vector<MatchStrengthC> observationStrength;	///< Observation set, match strength

		vector<Coordinate1T> validationSetL;	///< Validation set, first domain (left)
		vector<Coordinate2T> validationSetR;	///< Validation set, second domain (right)
		vector<MatchStrengthC> validationStrength;	///< Validation set, match strength

		typedef tuple<unsigned int, unsigned int> BinT;	///< A data bin, quantised coordinates for a correspondences
		vector<BinT> observationBins;	///< Bins for the observation set

		typedef typename CoordinateTransformationsT<Coordinate1T>::ArrayD DimensionLT;
		typedef typename CoordinateTransformationsT<Coordinate2T>::ArrayD DimensionRT;
		DimensionLT binDimensions1;	///< Bin dimensions, first domain
		DimensionRT binDimensions2;	///< Bin dimensions, second domain

		MinimalGeometrySolverT minimalSolver;	///< Minimal geometry solver
		LOGeometrySolverT loSolver;	///< LO geometry solver

		typedef typename MinimalGeometrySolverT::constraint_type GeometricConstraintT;
		GeometricConstraintT evaluator;	///< Evaluates the distance of an observation to the model, and identifies the outliers

		typedef typename MinimalGeometrySolverT::loss_type MapT;
		MapT lossMap;	///< Converts an error to a loss

		typedef typename MinimalGeometrySolverT::distance_type ModelDistanceT;	///< Type of the distance between two models
		ModelDistanceT modelSimilarityTh;	///< If the distance between two models is below this value, they are considered similar

		unsigned int loGeneratorSize;	///< Size of the generator for the local optimisation step

		double cost;	///< Cost of running the geometry solver over a generator, relative to evaluating a correspondence
		//@}

		/**@name State */ //@{
		bool flagValid;	///< \c true if the problem is properly initialised
		//@}

		/** @name Implementation details */ //@{
		bimap<list_of<Coordinate1T> , list_of<Coordinate2T>, left_based> MakeSolverInput(const GeneratorT& generator) const;	///< Converts a generator to an input for the solver
		void InitialiseData(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, bool flagObservationOnly);	///< Initialises the data members
		//@}

	public:

		/**@name RANSACProblemConceptC interface */ //@{

		typedef typename MinimalGeometrySolverT::model_type model_type;	///< A model
		typedef MinimalGeometrySolverT minimal_solver_type;	///< Type of the minimal solver
		typedef LOGeometrySolverT lo_solver_type;	///< Type of the LO solver

		RANSACGeometryEstimationProblemC();	///< Default constructor

		bool IsValid() const;	///< Returns \c flagValid

		//Solver properties
		double Cost() const;	///< Returns \c cost
		static constexpr unsigned int GeneratorSize();	///< Returns the size of the minimal generator set

		//Data properties
		size_t GetObservationSetSize() const;	///< Returns the size of the observation set
		size_t GetValidationSetSize() const;	///< Returns the size of the validation set

		//Generator
		unsigned int ComputeDiversity(const GeneratorT& generator) const;	///< Computes the diversity of the generator
		bool ValidateGenerator(const GeneratorT& generator) const;	///< Validates the generator
		list<model_type> GenerateModel(const GeneratorT& generator) const;	///< Generates hypotheses from the generator

		list<model_type> GenerateModelLO(const GeneratorT& generator) const;	///< Generates hypotheses from the generator- LO variant
		unsigned int LOGeneratorSize() const;	///< Returns the size of the minimum generator set- LO variant

		bool EvaluateObservation(const model_type& model, unsigned int index) const;	///< Evaluates an observation against a model
		tuple<bool, double> EvaluateValidator(const model_type& model, unsigned int index) const;	///< Evaluates an validator against a model

		//Accessors
		typedef CorrespondenceContainerT data_container_type;	///< A container for the observation and the validation sets
		data_container_type GetInlierObservations(const model_type& model) const;	///< Returns the observations that are inliers wrt/a model
		data_container_type GetObservations() const;	///< Returns the observations
		void SetObservations(const CorrespondenceContainerT& src);	///< Overwrites the current observations
		data_container_type GetValidators() const;	///< Returns the validators
		void SetValidators(const CorrespondenceContainerT& src);	///< Sets the validation set
		const optional<CorrespondenceT> GetObservation(unsigned int index) const;	///< Returns the observation corresponding to an index
		optional<unsigned int> GetObservationIndex(const CorrespondenceT& observation) const;	///< Returns the index of an observation

		bool IsSimilar(const model_type& model1, const model_type& model2) const;	///< Returns \c true if the models are algebraically similar
		//@}

		/** @name RANSACGeometryEstimationProblemConceptC interface */ //@{

		//Accessors

		typedef GeometricConstraintT constraint_type;	///< A constraint for the evaluation of a correspondence
		const GeometricConstraintT& GetConstraint() const;	///< Returns the geometric constraint for the evaluation of the validators

		typedef MapT loss_type;	///< Type of the function for error-to-loss conversion
		const MapT& GetLossMap() const;	///< Returns the loss function

		const MinimalGeometrySolverT& GetMinimalSolver() const;	///< Returns a constant reference to the minimal solver
		const LOGeometrySolverT& GetLOSolver() const;	///< Return a constant reference to the LO solver

		bool Configure(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, const GeometricConstraintT&  eevaluator, const MinimalGeometrySolverT& mminimalSolver, const LOGeometrySolverT& lloSolver, const DimensionLT& binSize1, const DimensionRT& binSize2);	///< Configures the problem

		typedef DimensionLT dimension_type1;	///< Dimension type for the bins in the first domain
		typedef DimensionRT dimension_type2;	///< Dimension type for the bins in the second domain
		//@}

		/** @name Constructors */ //@{
		RANSACGeometryEstimationProblemC(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, const GeometricConstraintT&  eevaluator, const MapT& llossMap, const MinimalGeometrySolverT& mminimalSolver, const LOGeometrySolverT& lloSolver, const ModelDistanceT& mmodelSimilarityTh, unsigned int lloGeneratorSize, const DimensionLT& binSize1, const DimensionRT& binSize2);	///< Constructor
		//@}
};	//class RANSACGeometryEstimationProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Initialises the data members
 * @param[in] observationSet Observation set
 * @param[in] validationSet Validation set
 * @param[in] flagObservationOnly If \c true, validation set components are not initialised
 * @remarks Constructs the internal representations of the data
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
void RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::InitialiseData(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, bool flagObservationOnly)
{
	//Validation set
	if(!flagObservationOnly)
		SetValidators(validationSet);

	observationBins.clear();
	if(!boost::empty(observationSet))
	{
		//Observation set
		tie(observationSetL, observationSetR, observationStrength)=BimapToVector<Coordinate1T, Coordinate2T, MatchStrengthC>(observationSet);	//Decompose into separate lists

		//Bucketise
		typename CoordinateStatisticsT<Coordinate1T>::ArrayD2 extentsL=*CoordinateStatisticsT<Coordinate1T>::ComputeExtent(observationSetL);	//Cannot be empty
		vector<unsigned int> binsL=CoordinateTransformationsT<Coordinate1T>::Bucketise(observationSetL, binDimensions1, extentsL);

		typename CoordinateStatisticsT<Coordinate2T>::ArrayD2 extentsR=*CoordinateStatisticsT<Coordinate2T>::ComputeExtent(observationSetR);	//Cannot be empty
		vector<unsigned int> binsR=CoordinateTransformationsT<Coordinate2T>::Bucketise(observationSetR, binDimensions2, extentsR);

		size_t nElements=binsL.size();
		observationBins.clear();
		observationBins.reserve(nElements);
		for(size_t c=0; c<nElements; ++c)
			observationBins.emplace_back(binsL[c], binsR[c]);
	}	//if(boost::empty(observationSet))
}	//void InitialiseData(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet)

/**
 * @brief Converts a generator to an input for the solver
 * @param[in] generator Indices of the generator set
 * @return A bimap corresponding to the generator set
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::MakeSolverInput(const GeneratorT& generator) const -> bimap<list_of<Coordinate1T> , list_of<Coordinate2T>, left_based>
{
	typedef bimap<list_of<Coordinate1T> , list_of<Coordinate2T>, left_based> InputT;
	typedef typename InputT::value_type ValueT;

	InputT generatorSet;
	for(auto current : generator)
		generatorSet.push_back(ValueT(observationSetL[current], observationSetR[current]));

	return generatorSet;
}	//bimap<list_of<Coordinate1T> , list_of<Coordinate2T>, left_based> MakeSolverInput(const GeneratorT& generator)

/**
 * @brief Configures the problem
 * @param[in] observationSet Observation set
 * @param[in] validationSet Validation set
 * @param[in] eevaluator Observation evaluator
 * @param[in] mminimalSolver Geometry solver for a minimal generator
 * @param[in] lloSolver Geometry solver for a nonminimal generator
 * @param[in] binSize1 Bin dimensions for the first domain
 * @param[in] binSize2 Bin dimensions for the second domain
 * @return \c true if the configuration is successful
 * @remarks Write access to the parts of the problem which may be modified during the guided matching iterations
 * @remarks \c flagValid is NOT modified, i.e., calling Configure after the default constructor does not result in a valid problem
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
bool RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::Configure(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, const GeometricConstraintT&  eevaluator, const MinimalGeometrySolverT& mminimalSolver, const LOGeometrySolverT& lloSolver, const DimensionLT& binSize1, const DimensionRT& binSize2)
{
	binDimensions1=binSize1;
	binDimensions2=binSize2;
	InitialiseData(observationSet, validationSet, false);

	evaluator=eevaluator;
	minimalSolver=mminimalSolver;
	loSolver=lloSolver;
	return true;
}	//bool Configure(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, const GeometricConstraintT&  eevaluator, const MapT& eerrorToFitnessMap, const MinimalGeometrySolverT& mminimalSolver, const LOGeometrySolverT& lloSolver)

/**
 * @brief Constructor
 * @param[in] observationSet Observation set
 * @param[in] validationSet Validation set
 * @param[in] eevaluator Observation evaluator
 * @param[in] llossMap Error-to-loss conversion map
 * @param[in] mminimalSolver Geometry solver for a minimal generator
 * @param[in] lloSolver Geometry solver for a nonminimal generator
 * @param[in] mmodelSimilarityTh If the distance between two models is below this value, they are considered similar. Meaningful range is different for each solver! Disable when substantially different models may still have a small algebraic distance (e.g. fundamental matrices)
 * @param[in] lloGeneratorSize Generator size for the local optimisation solver
 * @param[in] binSize1 Bin dimensions for the first domain
 * @param[in] binSize2 Bin dimensions for the second domain
 * @post A valid problem
 * @remarks If the solver cannot handle non-minimal sets, \c loGeneratorSize is set to the minimal generator size
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::RANSACGeometryEstimationProblemC(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, const GeometricConstraintT&  eevaluator, const MapT& llossMap, const MinimalGeometrySolverT& mminimalSolver, const LOGeometrySolverT& lloSolver, const ModelDistanceT& mmodelSimilarityTh, unsigned int lloGeneratorSize, const DimensionLT& binSize1, const DimensionRT& binSize2) : lossMap(llossMap), modelSimilarityTh(mmodelSimilarityTh)
{
	loGeneratorSize = LOGeometrySolverT::IsNonminimal() ? lloGeneratorSize : LOGeometrySolverT::GeneratorSize();

	cost=mminimalSolver.Cost()/eevaluator.GetErrorMetric().Cost();
	flagValid=Configure(observationSet, validationSet, eevaluator, mminimalSolver, lloSolver, binSize1, binSize2);
}	//RANSACGeometryEstimationProblemC(const CorrespondenceContainerT& observationSet, const CorrespondenceContainerT& validationSet, const GeometricConstraintT&  eevaluator, const MapT& eerrorToFitnessMap, const MinimalGeometrySolverT& mminimalSolver, const LOGeometrySolverT& lloSolver)

/**
 * @brief Default constructor
 * @post An invalid problem
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::RANSACGeometryEstimationProblemC() : loGeneratorSize(0), cost(0), flagValid(false)
{
}	//RANSACGeometryEstimationProblemC()

/**
 * @brief Returns \c flagValid
 * @return \c true if the problem is valid
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
bool RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Returns \c cost
 * @return \c cost
 * @pre \c flagValid=true
 * @remarks No unit test, as costs are volatile
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
double RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::Cost() const
{
	//Preconditions
	assert(flagValid);

	return cost;
}	//double Cost()

/**
 * @brief Returns the size of the minimum generator set
 * @return Size of the minimum generator set
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
constexpr unsigned int RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GeneratorSize()
{
	return MinimalGeometrySolverT::GeneratorSize();
}	//unsigned int GeneratorSize()

/**
 * @brief Returns the size of the observation set
 * @return The number of elements in \c observationSet
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
size_t RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetObservationSetSize() const
{
	//Preconditions
	assert(flagValid);
	return observationSetL.size();
}	//size_t GetObservationSetSize()

/**
 * @brief Returns the size of the validation set
 * @return The number of elements in \c ValidationSet
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
size_t RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetValidationSetSize() const
{
	//Preconditions
	assert(flagValid);
	return validationSetL.size();
}	//size_t GetValidationSetSize()

/**
 * @brief Computes the diversity of the generator
 * @param[in] generator Generator set
 * @return Number of bins represented in the generator set
 * @remarks Each observation belongs to a bin. Diversity is the number of unique bins represented in the generator
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
unsigned int RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::ComputeDiversity(const GeneratorT& generator) const
{
	//Preconditions
	assert(flagValid);

	set<BinT> bins;
	for(auto current : generator)
		bins.insert(observationBins[current]);

	return bins.size();
}	//unsigned int ComputeDiversity(const GeneratorT& generator)

/**
 * @brief Validates the generator
 * @param[in] generator Generator set
 * @return \c true if the generator can spawn a valid model
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
bool RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::ValidateGenerator(const GeneratorT& generator) const
{
	//Preconditions
	assert(flagValid);
	return minimalSolver.ValidateGenerator(MakeSolverInput(generator));
}	//bool ValidateGenerator(const GeneratorT& generator)

/**
 * @brief Generates hypotheses from the generator
 * @param[in] generator Generator set
 * @return A list of hypotheses
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GenerateModel(const GeneratorT& generator) const -> list<model_type>
{
	//Preconditions
	assert(flagValid);
	return minimalSolver(MakeSolverInput(generator));
}	//list<model_type> GenerateModel(const GeneratorT& generator)

/**
 * @brief Evaluates an validator against a model
 * @param[in] model Model
 * @param[in] index Index of the observation
 * @return A tuple: [Inlier flag; fitness score]
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
tuple<bool, double> RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::EvaluateValidator(const model_type& model, unsigned int index) const
{
	//Preconditions
	assert(flagValid);

	bool flagInlier;
	typename GeometricConstraintT::distance_type error;
	tie(flagInlier, error)=evaluator.Enforce(model, validationSetL[index], validationSetR[index]);

	return make_tuple(flagInlier, lossMap(error));
}	//tuple<bool, double> EvaluateObservation(const model_type& model)

/**
 * @brief Evaluates an observation against a model
 * @param[in] model Model
 * @param[in] index Index of the observation
 * @return \c true if the observation is an inlier to the model
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
bool RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::EvaluateObservation(const model_type& model, unsigned int index) const
{
	//Preconditions
	assert(flagValid);
	return evaluator(model, observationSetL[index], observationSetR[index]);
}	//tuple<bool, double> EvaluateObservation(const model_type& model)

/**
 * @brief Returns the observations that are inliers wrt/a model
 * @param[in] model Model for which the inliers are sought
 * @return List of inliers
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetInlierObservations(const model_type& model) const -> data_container_type
{
	data_container_type output;
	size_t nObservation=observationSetL.size();
	for(size_t c=0; c<nObservation; ++c)
		if(EvaluateObservation(model, c))
			output.push_back( typename data_container_type::value_type(observationSetL[c], observationSetR[c], observationStrength[c]));

	return output;
}	//data_container_type GetInlierObservations(const model_type& model) const

/**
 * @brief Overwrites the current observations
 * @param[in] src Source data
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
void RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::SetObservations(const CorrespondenceContainerT& src)
{
	//Preconditions
	assert(flagValid);
	InitialiseData(src, CorrespondenceContainerT(), true);
}	//void SetObservations(const CorrespondenceContainerT& src)

/**
 * @brief Sets the validation set
 * @param[in] src Source data
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
void RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::SetValidators(const CorrespondenceContainerT& src)
{
	tie(validationSetL, validationSetR, validationStrength)=BimapToVector<Coordinate1T, Coordinate2T, MatchStrengthC>(src);
}	//void SetValidators(const CorrespondenceContainerT& src)

/**
 * @brief Returns the observation corresponding to an index
 * @param[in] index Index of the observation
 * @return Observation corresponding to the index. Invalid if index out of range
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetObservation(unsigned int index) const -> const optional<CorrespondenceT>
{
	assert(flagValid);
	return index<observationSetL.size() ? CorrespondenceT(observationSetL[index], observationSetR[index], observationStrength[index]) : optional<CorrespondenceT>();
}	//const CorrespondenceT& GetObservation(unsigned int index) const

/**
 * @brief Returns the index of an observation
 * @param[in] observation Observation
 * @return Index of the observation. Invalid if not found
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
optional<unsigned int> RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetObservationIndex(const CorrespondenceT& observation) const
{
	//Preconditions
	assert(flagValid);

	optional<unsigned int> index;

	size_t nObservations=GetObservationSetSize();
	for(size_t c=0; c<nObservations; ++c)
		if(observationSetL[c]==observation.left && observationSetR[c]==observation.right && observationStrength[c].Ambiguity()==observation.info.Ambiguity() && observationStrength[c].Similarity()==observation.info.Similarity())
		{
			index=c;
			break;
		}	//if(observationSetL[c]==observation.left && observationSetR[c]==observation.right && observationStrength[c]==observation.info)

	return index;
}	//unsigned int GetObservationIndex(const CorrespondenceT& observation) const

/**
 * @brief Generates hypotheses from the generator
 * @param[in] generator Generator set
 * @return A list of hypotheses
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GenerateModelLO(const GeneratorT& generator) const -> list<model_type>
{
	//Preconditions
	assert(flagValid);
	return loSolver(MakeSolverInput(generator));
}	//list<model_type> GenerateModelLO(const GeneratorT& generator)

/**
 * @brief Returns \c true if the models are algebraically similar
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return \c false if the models are not similar
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
bool RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::IsSimilar(const model_type& model1, const model_type& model2) const
{
	//Preconditions
	assert(flagValid);
	return minimalSolver.ComputeDistance(model1, model2 ) <= modelSimilarityTh;
}	//bool IsSimilar(const model_type& model1, const model_type& model2)

/**
 * @brief Returns the size of the minimum generator set- LO variant
 * @return \c loGeneratorSize
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
unsigned int RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::LOGeneratorSize() const
{
	//Preconditions
	assert(flagValid);
	return loGeneratorSize;
}	//unsigned int LOGeneratorSize()

/**
 * @brief Returns the observations
 * @return Observations as a correspondence list
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetObservations() const -> CorrespondenceContainerT
{
	return VectorToBimap<CorrespondenceContainerT>(observationSetL, observationSetR, observationStrength);
}	//CorrespondenceContainerT GetObservations()

/**
 * @brief Returns the validators
 * @return Validators as a correspondence list
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetValidators() const -> CorrespondenceContainerT
{
	return VectorToBimap<CorrespondenceContainerT>(validationSetL, validationSetR, validationStrength);
}	//CorrespondenceContainerT GetObservations()

/**
 * @brief Returns the geometric constraint for the evaluation of the validators
 * @return A constant reference to \c evaluator
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetConstraint() const -> const GeometricConstraintT&
{
	assert(flagValid);
	return evaluator;
}	//GeometricConstraintT GetConstraint()

/**
 * @brief Returns the loss function
 * @return A constant reference to \c lossMap
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetLossMap() const -> const MapT&
{
	assert(flagValid);
	return lossMap;
}	//MapT GetFitnessMap()

/**
 * @brief Returns a constant reference to the minimal solver
 * @return A constant reference to \c minimalSolver
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetMinimalSolver() const -> const MinimalGeometrySolverT&
{
	assert(flagValid);
	return minimalSolver;
}	//const MinimalGeometrySolverT& GetMinimalSolver()

/**
 * @brief Return a constant reference to the LO solver
 * @return A constant reference to \c loSolver
 * @pre \c flagValid=true
 */
template<class MinimalGeometrySolverT, class LOGeometrySolverT>
auto RANSACGeometryEstimationProblemC<MinimalGeometrySolverT, LOGeometrySolverT>::GetLOSolver() const -> const LOGeometrySolverT&
{
	assert(flagValid);
	return loSolver;
}	//auto GetLOSolver() const -> const LOGeometrySolverT&

/********** FREE FUNCTIONS **********/

/**
 * @brief Computes the bin dimensions
 * @tparam CoordinateT Coordinate type
 * @tparam CoordinateRangeT A range of coordinates
 * @param coordinates Coordinates
 * @param binDensity Bin density
 * @return Bin size. Invalid if std estimation fails
 * @pre \c CoordinateT is an Eigen row vector (unenforced)
 * @pre \c binDensity>0
 * @remarks Computes the standard deviation of the coordinates, and sets the bin size, so that each standard deviation has \c binDensity bins. A utility function for RANSAC geometry estimation problems.
 * @ingroup Utility
 */
template<class CoordinateT, class CoordinateRangeT>
auto ComputeBinSize(const CoordinateRangeT& coordinates, typename ValueTypeM<CoordinateT>::type binDensity) -> optional<typename CoordinateStatisticsT<CoordinateT>::ArrayD>
{
	//Preconditions
	assert(binDensity>0);

	typedef typename CoordinateStatisticsT<CoordinateT>::ArrayD ArrayT;
	optional<tuple<ArrayT, ArrayT> > statistics=CoordinateStatisticsT<CoordinateT>::ComputeMeanAndStD(coordinates);

	optional<ArrayT> output;

	if(statistics)
		output=get<1>(*statistics)/binDensity;

	return output;
}	//Array<double, DIM, 1> ComputeBinSize(const CoordinateRangeT& coordinates, double binDensity)



}	//RANSACN
}	//SeeSawN

#endif /* RANSAC_GEOMETRY_ESTIMATION_PROBLEM_IPP_6923231 */
