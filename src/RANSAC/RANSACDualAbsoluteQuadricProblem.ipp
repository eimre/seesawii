/**
 * @file RANSACDualAbsoluteQuadricProblem.ipp Implementation of \c RANSACDualAbsoluteQuadricProblemC
 * @author Evren Imre
 * @date 4 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_DUAL_ABSOLUTE_QUADRIC_PROBLEM_IPP_9072131
#define RANSAC_DUAL_ABSOLUTE_QUADRIC_PROBLEM_IPP_9072131

#include <boost/optional.hpp>
#include <vector>
#include <set>
#include <list>
#include <tuple>
#include <cstddef>
#include <map>
#include <cmath>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Geometry/DualAbsoluteQuadricSolver.h"

namespace SeeSawN
{
namespace RANSACN
{

using boost::optional;
using std::vector;
using std::set;
using std::list;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::size_t;
using std::multimap;
using std::min;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::WrappersN::EigenLexicalCompareC;
using SeeSawN::GeometryN::DualAbsoluteQuadricSolverC;

/**
 * @brief RANSAC problem for dual absolute quadric estimation
 * @ingroup Problem
 * @nosubgrouping
 */
class RANSACDualAbsoluteQuadricProblemC
{
	private:

		typedef set<unsigned int> GeneratorT;	///< A generator
		typedef vector<CameraMatrixT> DataContainerT;	///< A data container

		/** @name Configuration */ //@{
		static constexpr double cost=1e16;	///< Cost of running the solver once, relative to the evaluation. Effectively infinity

		bool flagValid;	///< True if the object is initialised correctly

		DataContainerT observationSet;	///< Observations
		DataContainerT validationSet;	///< Validators

		multimap<CameraMatrixT, unsigned int, EigenLexicalCompareC> observationMap;	///< Observation-to-index map

		unsigned int sGenerator;	///< Size of the generator for the minimal solver
		DualAbsoluteQuadricSolverC solver;	///< Minimal solver

		unsigned int sLOGenerator;	///< Size of the generator for the LO solver
		DualAbsoluteQuadricSolverC loSolver;	///< LO solver

		double inlierTh;	///< Inlier threshold
		double modelSimilarityTh;	///< Model similarity threshold
		//@}

		/**@name Implementation details */ //@{
		vector<CameraMatrixT> MakeGenerator(const GeneratorT& generator) const;	///< Makes a generator from the indices
		//@}

	public:

		/** @name Constructors */ //@{
		RANSACDualAbsoluteQuadricProblemC();	///< Default constructor
		RANSACDualAbsoluteQuadricProblemC(const DataContainerT& oobservationSet, const DataContainerT& vvalidationSet, const DualAbsoluteQuadricSolverC& ssolver, const DualAbsoluteQuadricSolverC& lloSolver, unsigned int ssGenerator, unsigned int ssLOGenerator, double iinlierTh, double mmodelSimilarityTh);	///< Constructor

		//@}
		/** @name RANSACProblemConceptC interface */ //@{
		typedef DualAbsoluteQuadricSolverC::model_type model_type;	///< Type of the estimated model
		typedef DualAbsoluteQuadricSolverC minimal_solver_type;	///< Type of the minimal solver
		typedef DualAbsoluteQuadricSolverC lo_solver_type;	///< Type of the LO solver
		typedef DataContainerT data_container_type;	///< A data container

		bool IsValid() const;	///< Returns \c flagValid
		double Cost() const;	///< Returns \c cost

		//Data
		size_t GetObservationSetSize() const;	///< Returns the number of elements in the observation set
		size_t GetValidationSetSize() const;	///< Returns the number of elements in the validation set
		DataContainerT GetInlierObservations(const model_type& model) const;	///< Returns the observations that are inliers wrt/a model
		void SetObservations(const DataContainerT& src);	///< Sets the observations
		void SetValidators(const DataContainerT& src);	///< Sets the validators
		const DataContainerT& GetObservations() const;	///< Returns a constant reference to \c observationSet
		optional<unsigned int> GetObservationIndex(const CameraMatrixT& observation) const;	///< Returns the index of an observation

		//Generator

		unsigned int ComputeDiversity(const GeneratorT& generator) const;	///< Computes the diversity of a generator
		bool ValidateGenerator(const GeneratorT& generator) const;	///< Validates a generator

		//Solvers
		list<model_type> GenerateModel(const GeneratorT& generator) const;	///< Generates a hypothesis
		unsigned int GeneratorSize() const;	///< Returns the size of the minimum generator set
		const minimal_solver_type& GetMinimalSolver() const;	///< Returns a constant reference to \c solver

		list<model_type> GenerateModelLO(const GeneratorT& generator) const; ///< Generates a hypothesis from the generator- LO variant
		unsigned int LOGeneratorSize() const;	///< Returns the size of the minimum generator set- LO variant
		const lo_solver_type& GetLOSolver() const;	///< Returns a constant reference to \c loSolver

		//Evaluation
		bool EvaluateObservation(const model_type& model, unsigned int index) const;	///< Evaluates an observation against a model
		tuple<bool, double> EvaluateValidator(const model_type& model, unsigned int index) const;	///< Evaluates an validator against a model
		bool IsSimilar(const model_type& model1, const model_type& model2) const;	///< Verifies whether two hypotheses are similar
		//@}
};	//class RANSACLinearAutocalibrationProblemC

}	//RANSACN
}	//SeeSawN

#endif /* RANSAC_DUAL_ABSOLUTE_QUADRIC_PROBLEM_IPP_9072131 */
