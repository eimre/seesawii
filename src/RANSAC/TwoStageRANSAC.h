/**
 * @file TwoStageRANSAC.h Public interface for TwoStageRANSACC
 * @author Evren Imre
 * @date 27 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef TWO_STAGE_RANSAC_H_7323254
#define TWO_STAGE_RANSAC_H_7323254

#include "TwoStageRANSAC.ipp"

namespace SeeSawN
{
namespace RANSACN
{

struct TwoStageRANSACParametersC;	///< Parameters for TwoStageRANSACC
struct TwoStageRANSACDiagnosticsC;	///< Diagnostics for TwoStageRANSACC
template<class Problem1T, class Problem2T, class RNGT> class TwoStageRANSACC;	///< Two-stage RANSAC algorithm

namespace DetailN
{
	template<class RANSAC1T, class RANSAC2T> void EvaluateStage1Model(typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type & problemS2);	///< Evaluates the model from the first stage against the constraint of the second stage
	template<class RANSAC1T, class RANSAC2T, typename enable_if<is_same<typename RANSAC1T::problem_type, typename RANSAC2T::problem_type>::value, int>::type=0 > optional<unsigned int> InsertStage1Result(typename RANSAC2T::result_type& resultS2, typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type& problem2);	///< Attempts to replace the worst result in stage-2 by the stage-1 result
	template<class RANSAC1T, class RANSAC2T, typename enable_if<!is_same<typename RANSAC1T::problem_type, typename RANSAC2T::problem_type>::value, int>::type=0 > optional<unsigned int> InsertStage1Result(typename RANSAC2T::result_type& resultS2, typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type& problem2);	///< Dummy function for overload hiding
}	//namespace DetailN

}	//RANSACN
}	//SeeSawN

#endif /* TWOSTAGERANSAC_H_ */
