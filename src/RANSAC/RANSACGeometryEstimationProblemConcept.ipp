/**
 * @file RANSACGeometryEstimationProblemConcept.ipp Implementation of RANSACGeometryEstimationProblemConceptC
 * @author Evren Imre
 * @date 15 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_GEOMETRY_ESTIMATION_PROBLEM_CONCEPT_IPP_0984124
#define RANSAC_GEOMETRY_ESTIMATION_PROBLEM_CONCEPT_IPP_0984124

#include <boost/concept_check.hpp>
#include "RANSACProblemConcept.h"

namespace SeeSawN
{
namespace RANSACN
{

using SeeSawN::RANSACN::RANSACProblemConceptC;

/**
 * @brief Concept for RANSAC geometry estimation problem
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class RANSACGeometryEstimationProblemConceptC
{
	public:

		///@cond CONCEPT_CHECK
		BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<TestT>));

		BOOST_CONCEPT_USAGE(RANSACGeometryEstimationProblemConceptC)
		{
			TestT tested;

			typename TestT::constraint_type constraint=tested.GetConstraint(); (void)constraint;
			typename TestT::loss_type errorMap=tested.GetLossMap(); (void)errorMap;

			typename TestT::dimension_type1 binSize1;
			typename TestT::dimension_type2 binSize2;

			bool flagConfigure=tested.Configure(typename TestT::data_container_type(), typename TestT::data_container_type(), constraint, typename TestT::minimal_solver_type(), typename TestT::lo_solver_type(), binSize1, binSize2); (void)flagConfigure;
		}	//BOOST_CONCEPT_USAGE(RANSACGeometryEstimationProblemConceptC)
		///@endcond
};	//class RANSACGeometryEstimationProblemConceptC

}	//RANSACN
}	//SeeSawN

#endif /* RANSAC_GEOMETRY_ESTIMATION_PROBLEM_CONCEPT_IPP_0984124 */
