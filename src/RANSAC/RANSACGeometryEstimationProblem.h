/**
 * @file RANSACGeometryEstimationProblem.h Public interface for RANSACGeometryEstimationProblemC
 * @author Evren Imre
 * @date 15 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_GEOMETRY_ESTIMATION_PROBLEM_H_1419002
#define RANSAC_GEOMETRY_ESTIMATION_PROBLEM_H_1419002

#include "RANSACGeometryEstimationProblem.ipp"

namespace SeeSawN
{
namespace RANSACN
{

template<class MinimalGeometrySolverT, class LOGeometrySolverT> class RANSACGeometryEstimationProblemC;	///< RANSAC problem for geometry estimation

template<class CoordinateT, class CoordinateRangeT> optional<typename CoordinateStatisticsT<CoordinateT>::ArrayD> ComputeBinSize(const CoordinateRangeT& coordinates, typename ValueTypeM<CoordinateT>::type binDensity);	///< Computes the bin dimensions

}	//RANSACN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::tuple<unsigned int, unsigned int>;
extern template class std::vector<unsigned int>;
#endif /* RANSAC_GEOMETRY_ESTIMATION_PROBLEM_H_1419002 */
