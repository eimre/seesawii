/**
 * @file SequentialRANSAC.h Public interface for the sequential RANSAC algorithm
 * @author Evren Imre
 * @date 15 May 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SEQUENTIAL_RANSAC_H_1698921
#define SEQUENTIAL_RANSAC_H_1698921

#include "SequentialRANSAC.ipp"

namespace SeeSawN
{
namespace RANSACN
{

struct SequentialRANSACParametersC;	///< Parameter object for SequentialRANSACC
struct SequentialRANSACDiagnosticsC;	///< Diagnostics object for SequentialRANSACC
template<class ProblemT, class RNGT> class SequentialRANSACC;	///< Sequential RANSAC

}	//RANSACN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template std::string boost::lexical_cast<std::string, double>(const double&);

#endif /* SEQUENTIAL_RANSAC_H_1698921 */
