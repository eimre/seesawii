/**
 * @file RANSACDualAbsoluteQuadricProblem.cpp Implementation of \c RANSACDualAbsoluteQuadricProblemC
 * @author Evren Imre
 * @date 4 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RANSACDualAbsoluteQuadricProblem.h"
namespace SeeSawN
{
namespace RANSACN
{

/**
 * @brief Makes a generator from the indices
 * @param[in] generator Generator indices
 * @return Generator elements
 * @pre \c flagValid=true
 */
vector<CameraMatrixT> RANSACDualAbsoluteQuadricProblemC::MakeGenerator(const GeneratorT& generator) const
{
	//Preconditions
	assert(flagValid);

	size_t sElement=generator.size();
	vector<CameraMatrixT> output; output.reserve(sElement);
	for(auto current : generator)
		output.push_back(observationSet[current]);

	return output;
}	//vector<CameraMatrixT> MakeGenerator(const GeneratorT& generator) const

/**
 * @brief Default constructor
 * @post Invalid object
 */
RANSACDualAbsoluteQuadricProblemC::RANSACDualAbsoluteQuadricProblemC() : flagValid(false), sGenerator(0), sLOGenerator(0), inlierTh(0), modelSimilarityTh(0)
{}

/**
 * @brief Constructor
 * @param[in] oobservationSet Observation set
 * @param[in] vvalidationSet Validation set
 * @param[in] ssolver Minimal solver
 * @param[in] lloSolver LO solver
 * @param[in] ssGenerator Generator size for the minimal solver
 * @param[in] ssLOGenerator Generator size for the LO solver
 * @param[in] iinlierTh Inlier threshold. >0
 * @param[in] mmodelSimilarityTh Model similarity threshold. (0,1]
 * @pre \c iinlierTh >0
 * @pre \c mmodelSimilarityTh is within (0,1]
 * @pre \c oobservationSet does not have less elements than \c ssGenerator and \c ssLOGenerator
 * @pre \c ssGenerator and \c ssLOGenerator are nor less than the minimal generator
 * @pre \c vvalidationSet is not empty
 * @post Valid object
 */
RANSACDualAbsoluteQuadricProblemC::RANSACDualAbsoluteQuadricProblemC(const DataContainerT& oobservationSet, const DataContainerT& vvalidationSet, const DualAbsoluteQuadricSolverC& ssolver, const DualAbsoluteQuadricSolverC& lloSolver, unsigned int ssGenerator, unsigned int ssLOGenerator, double iinlierTh, double mmodelSimilarityTh) : flagValid(true), validationSet(vvalidationSet), sGenerator(ssGenerator), solver(ssolver),  sLOGenerator(ssLOGenerator), loSolver(lloSolver), inlierTh(iinlierTh), modelSimilarityTh(mmodelSimilarityTh)
{
	//Preconditions
	assert(iinlierTh>0);
	assert(mmodelSimilarityTh>0 || mmodelSimilarityTh<=1);
	assert(oobservationSet.size()>=ssGenerator);
	assert(oobservationSet.size()>=ssLOGenerator);
	assert(!vvalidationSet.empty());
	assert(ssGenerator>=DualAbsoluteQuadricSolverC::GeneratorSize());
	assert(ssLOGenerator>=DualAbsoluteQuadricSolverC::GeneratorSize());

	SetObservations(oobservationSet);
}	//RANSACLinearAutocalibrationProblemC(const DataContainerT& oobservationSet, const DataContainerT& vvalidationSet, const LinearAutocalibrationSolverC& ssolver, const LinearAutocalibrationSolverC& lloSolver, unsigned int ssGenerator, unsigned int ssLOGenerator, double iinlierTh, double mmodelSimilarityTh)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool RANSACDualAbsoluteQuadricProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Returns \c cost
 * @return \c cost
 */
double RANSACDualAbsoluteQuadricProblemC::Cost() const
{
	return cost;
}	//double Cost()

/**
 * @brief Returns the number of elements in the observation set
 * @return Size of the observation set
 * @pre \c flagValid=true
 */
size_t RANSACDualAbsoluteQuadricProblemC::GetObservationSetSize() const
{
	assert(flagValid);
	return observationSet.size();
}	//size_t GetObservationSetSize()

/**
 * @brief Returns the number of elements in the validation set
 * @return Size of the validation set
 * @pre \c flagValid=true
 */
size_t RANSACDualAbsoluteQuadricProblemC::GetValidationSetSize() const
{
	assert(flagValid);
	return validationSet.size();
}	//size_t GetValidationSetSize()

/**
 * @brief Computes the diversity of a generator
 * @param generator Generator
 * @return \c sGenerator
 * @pre \c flagValid=true
 */
unsigned int RANSACDualAbsoluteQuadricProblemC::ComputeDiversity(const set<unsigned int>& generator) const
{
	assert(flagValid);
	return sGenerator;
}	//unsigned int ComputeDiversity(const set<unsigned int>& generator)

/**
 * @brief Validates a generator
 * @param generator Generator
 * @return \c true
 */
bool RANSACDualAbsoluteQuadricProblemC::ValidateGenerator(const set<unsigned int>& generator) const
{
	return true;
}	//bool ValidateGenerator(const set<unsigned int>& generator)

/**
 * @brief Generates a hypothesis
 * @param[in] generator Generator
 * @return A 1-element list of homographies
 * @pre \c flagValid=true
 */
auto RANSACDualAbsoluteQuadricProblemC::GenerateModel(const GeneratorT& generator) const -> list<model_type>
{
	assert(flagValid);
	return solver(MakeGenerator(generator));
}	//list<model_type> GenerateModel(const GeneratorT& generator)

/**
 * @brief Returns the size of the minimum generator set
 * @return \c sLOGenerator
 * @pre \c flagValid=true
 */
unsigned int RANSACDualAbsoluteQuadricProblemC::GeneratorSize() const
{
	assert(flagValid);
	return sGenerator;
}	//unsigned int LOGeneratorSize() const

/**
 * @brief Returns a constant reference to \c solver
 * @return A constant reference to \c solver
 * @pre \c flagValid=true
 */
auto RANSACDualAbsoluteQuadricProblemC::GetMinimalSolver() const -> const minimal_solver_type&
{
	assert(flagValid);
	return solver;
}	//const minimal_solver_type& GetMinimalSolver() const

/**
 * @brief Generates a hypothesis from the generator- LO variant
 * @param[in] generator
 * @return A 1-element list of homographies
 * @pre \c flagValid=true
 */
auto RANSACDualAbsoluteQuadricProblemC::GenerateModelLO(const GeneratorT& generator) const -> list<model_type>
{
	assert(flagValid);
	return loSolver(MakeGenerator(generator));
}	//auto GenerateModelLO(const GeneratorT& generator) const -> list<model_type>

/**
 * @brief Returns the size of the minimum generator set- LO variant
 * @return \c sLOGenerator
 * @pre \c flagValid=true
 */
unsigned int RANSACDualAbsoluteQuadricProblemC::LOGeneratorSize() const
{
	assert(flagValid);
	return sLOGenerator;
}	//unsigned int LOGeneratorSize() const

/**
 * @brief Returns a constant reference to \c solver
 * @return A constant reference to \c solver
 * @pre \c flagValid=true
 */
auto RANSACDualAbsoluteQuadricProblemC::GetLOSolver() const -> const lo_solver_type&
{
	assert(flagValid);
	return loSolver;
}	//const minimal_solver_type& GetMinimalSolver() const

/**
 * @brief Evaluates an observation against a model
 * @param[in] model Hypothesis
 * @param[in] index Index of the observation
 * @return \c true if the observation is an inlier
 * @pre \c flagValid=true
 */
bool RANSACDualAbsoluteQuadricProblemC::EvaluateObservation(const model_type& model, unsigned int index) const
{
	assert(flagValid);
	return solver.Evaluate(model, observationSet[index])<=inlierTh;
} //bool :EvaluateObservation(const model_type& model, unsigned int index) const

/**
 * @brief Evaluates an validator against a model
 * @param[in] model Hypothesis
 * @param[in] index Index of the validator
 * @return A tuple: Inlier flag, loss
 * @pre \c flagValid=true
 */
tuple<bool, double> RANSACDualAbsoluteQuadricProblemC::EvaluateValidator(const model_type& model, unsigned int index) const
{
	assert(flagValid);
	double error=solver.Evaluate(model, validationSet[index]);

	return make_tuple(error<inlierTh, min(error, inlierTh));
}	//tuple<bool, double> EvaluateValidator(const model_type& model, unsigned int index) const

/**
 * @brief Returns the observations that are inliers wrt/a model
 * @param[in] model Model
 * @return Inlier observations
 * @pre \c flagValid=true
 */
auto RANSACDualAbsoluteQuadricProblemC::GetInlierObservations(const model_type& model) const -> DataContainerT
{
	assert(flagValid);

	unsigned int sObservation=GetObservationSetSize();
	DataContainerT output; output.reserve(sObservation);

	for(size_t c=0; c<sObservation; ++c)
		if(EvaluateObservation(model, c))
			output.push_back(observationSet[c]);

	output.shrink_to_fit();
	return output;
}	//DataContainerT GetInlierObservations(const model_type& model) const

/**
 * @brief Sets the observations
 * @param[in] src New observations
 * @post \c observationSet is modified
 * @pre \c flagValid=true
 * @pre \c src does not have less elements than \c sGenerator and \c sLOGenerator
 */
void RANSACDualAbsoluteQuadricProblemC::SetObservations(const DataContainerT& src)
{
	//Preconditions
	assert(flagValid);
	assert(src.size()>=sGenerator);
	assert(src.size()>=sLOGenerator);

	observationSet=src;

	size_t nObservations=observationSet.size();
	for(size_t c=0; c<nObservations; ++c)
		observationMap.emplace(observationSet[c],c);
}	//void SetObservations(const DataContainerT& src)

/**
 * @brief Returns a constant reference to \c observationSet
 * @return A constant reference to \c observationSet
 * @pre \c flagValid=true
 */
auto RANSACDualAbsoluteQuadricProblemC::GetObservations() const -> const DataContainerT&
{
	assert(flagValid);
	return observationSet;
}	//const DataContainerT& GetObservations() const

/**
 * @brief Sets the validators
 * @param[in] src New validators
 * @post \c validationSet is modified
 * @pre \c flagValid=true
 * @pre \c src is not empty
 */
void RANSACDualAbsoluteQuadricProblemC::SetValidators(const DataContainerT& src)
{
	//Preconditions
	assert(flagValid);
	assert(!src.empty());

	validationSet=src;
}	//void SetValidators(const DataContainerT& src)

/**
 * @brief Verifies whether two hypotheses are similar
 * @param[in] model1 First model
 * @param[in] model2 Second model
 * @return \c true if the models are similar
 * @pre \c flagValid=true
 * @pre \c model1 and \c model2 are unit norm
 */
bool RANSACDualAbsoluteQuadricProblemC::IsSimilar(const model_type& model1, const model_type& model2) const
{
	assert(flagValid);
	assert(model1.norm()==1);
	assert(model2.norm()==1);
	return 1-fabs(model1.cwiseProduct(model2).sum())<modelSimilarityTh;
}	//bool IsSimilar(const model_type& model1, const model_type& model2) const

/**
 * @brief Returns the index of an observation
 * @param[in] observation Observation
 * @return Index in \c observationSet. Invalid if not found
 * @pre \c flagValid=true
 */
optional<unsigned int> RANSACDualAbsoluteQuadricProblemC::GetObservationIndex(const CameraMatrixT& observation) const
{
	assert(flagValid);
	optional<unsigned int> output;
	auto it=observationMap.find(observation);

	if(it!=observationMap.end())
		output=it->second;

	return output;
}	//optional<unsigned int> GetObservationIndex(const CameraMatrixT& observation) const


}	//RANSACN
}	//SeeSawN

