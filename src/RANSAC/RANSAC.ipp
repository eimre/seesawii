/**
 * @file RANSAC.ipp Implementation of RANSAC
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_IPP_7097012
#define RANSAC_IPP_7097012

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <Eigen/Dense>
#include <stdexcept>
#include <vector>
#include <cstddef>
#include <string>
#include <climits>
#include <cmath>
#include <tuple>
#include <list>
#include <set>
#include <map>
#include <algorithm>
#include <utility>
#include <iterator>
#include <numeric>
#include <functional>
#include <random>
#include <utility>
#include <omp.h>
#include "RANSACProblemConcept.h"
#include "../Numeric/Combinatorics.h"
#include "../Elements/Correspondence.h"
#include "../Wrappers/BoostDynamicBitset.h"

namespace SeeSawN
{
namespace RANSACN
{

using boost::optional;
using boost::lexical_cast;
using boost::iterator_range;
using boost::make_iterator_range;
using boost::range::transform;
using boost::accumulate;
using boost::dynamic_bitset;
using Eigen::Array;
using Eigen::ArrayXd;
using std::vector;
using std::size_t;
using std::invalid_argument;
using std::string;
using std::numeric_limits;
using std::log;
using std::ceil;
using std::fabs;
using std::exp;
using std::pow;
using std::fma;
using std::min;
using std::max;
using std::floor;
using std::ceil;
using std::tuple;
using std::get;
using std::make_tuple;
using std::tie;
using std::list;
using std::set;
using std::map;
using std::multimap;
using std::back_inserter;
using std::iota;
using std::set_intersection;
using std::set_difference;
using std::min_element;
using std::distance;
using std::next;
using std::greater;
using std::make_tuple;
using std::mt19937_64;
using std::pair;
using std::make_pair;
using SeeSawN::RANSACN::RANSACProblemConceptC;
using SeeSawN::NumericN::GenerateRandomCombination;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::WrappersN::MakeBitset;

//TODO A seperate header for the statistics of RANSAC
//TODO A separate documentation for usage notes and parameter selection
//TODO Mathematical model of MO-RANSAC, specifically, the number of iterations
//TODO Should SPRT automatically succeed in the first pass? Does this make the initial epsilon and delta estimates redundant?
//TODO Update the documentation with the paper. Especially the description of the class
//DOCUMENTME A low eligibility ratio keeps the threshold high, and therefore helps the weaker models in MO, when there is a large disparity between the supports
//DOCUMENTME MO works fine ~1:1. Beyond 2:1 is risky. That is why the balancing of the sample set is important
//DOCUMENTME MORANSAC
/**
 * @brief Parameters for RANSACC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct RANSACParametersC
{
	/** @name Operation */ //@{
	unsigned int nThreads;	///< Number of threads, for multithreaded operation

	//LO RANSAC
	unsigned int nLOIterations;	///< Number of inner RANSAC iterations
	double minObservationSupport;	///< Minimum ratio of the number of observation set inliers for a hypothesis to the generator. >=1 . Prevents the execution of LO-RANSAC for poor hypotheses.
	double maxErrorRatio;	///< If the ratio between the error for a hypothesis to that of the best (or associated) solution is below this value, LO is triggered. >=1. A value slightly above 1 runs LO on promising, but inferior solutions

	//PROSAC
	bool flagPROSAC;	///< If false, PROSAC is disabled
	double growthRate;	///< Growth rate, as a percentage of the dataset. (0,1]

	//SPRT
	bool flagWaldSAC;	///< If false, SPRT is disabled by setting the decision threshold to infinity
	double epsilon;	///< Initial estimate for p(eligible inlier|good model). (0,1)
	double delta;	///< Initial estimate for p(eligible inlier|bad model). (0,1) and \c delta<epsilon
	double maxDeltaTolerance;	///< If the relative difference for the current estimate of p(eligible inlier|bad model) and the one used in the design of SPRT is above this value, a new test is designed. (0,1]
	//@}

	/**@name Multiobjective operation */ //@{
	double minModelSimilarity;	///< Given the inlier sets A and B associated with two models, if \f$ \frac{ |A \cap B| }{ min(|A|,|B|) } \f$ is above this value, the models are considered similar. [0,1]
	double minCoverage;	///< Given the inlier set A, and the validation set V, if \f$ \frac{|A|}{|V|}\f$ is below this value, the model cannot become a separate objective. Active if there is more than one element in the current solution set. [0,1]
	unsigned int maxSolution;	///< Maximum number of solutions

	double parsimonyFactor;	///< Given the inlier sets \f${A_i}\f$ to the objectives \f$ O \f$, a new objective N is spawned if \f$  \frac{|\cup_{O}A_i|}{|\cup_{O+N} A_i| } \f$ is above this value. >=1
							//Controls the parsimony of the final model
	double diversityFactor;	///< Given the inlier sets \f${A_i}\f$ to the objectives \f$ O \f$, an existing objective is updated/replaced with the current hypothesis, if \f$  \frac{|\cup_{O'} A_i|}{|\cup_{O} A_i|} \f$ is above this value. >=0
							//Controls the diversity of the final solution set
	//@}

	/** @name Termination */ //@{

	double minConfidence;	///< The minimum confidence that an all-inlier solution is encountered. (0,1]
	double eligibilityRatio;	///< Percentage of eligible inlier correspondences (i.e., correspondences that satisfy another, independent criterion) (0,1]

	unsigned int minIteration;	///< Minimum number of iterations. Overrides the confidence and score criteria
	unsigned int maxIteration;	///< Maximum number of iterations. >=minIteration, >0

	optional<double> topN;	///< Maximum rank of the result. (0,1]. If defined, the process runs for a fixed number of iterations to satisfy the criterion. Otherwise, the process aims to return an all-inlier solution. Overrides all other criteria.

	double maxError;	///< Any model with an error above this value is rejected
	double minError;	///< If the error for a model falls below this value, the iterations are terminated. <=maxError
	//@}

	/** @name Output */ //@{
	bool flagHistory;	///< If \c false , diagnostics does not keep a history
	//@}

	/** @brief Default constructor */
	RANSACParametersC() : nThreads(1), nLOIterations(10), minObservationSupport(8), maxErrorRatio(1.05), flagPROSAC(true), growthRate(0.01), flagWaldSAC(true), epsilon(0.2), delta(0.01), maxDeltaTolerance(0.1), minModelSimilarity(0.75), minCoverage(0.15), maxSolution(1), parsimonyFactor(1.05), diversityFactor(0.95), minConfidence(0.99), eligibilityRatio(1), minIteration(0), maxIteration(numeric_limits<unsigned int>::max()), maxError(numeric_limits<double>::infinity()), minError(-numeric_limits<double>::infinity()), flagHistory(false)
	{}
};	//struct RANSACParametersC

/**
 * @brief Diagnostics of RANSAC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct RANSACDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully

	double inlierRatio;	///< Inlier ratio (=epsilonVl/eligibilityRatio) for the validators
	double finalEpsilonOb; ///< p(eligible inlier|good model), over the observation set
	double finalEpsilonVl; ///< p(eligible inlier|good model), over the validation set
	double finalDelta;	///< p(eligible inlier|bad model)
	double finalDecisionTh;	///< Final decision threshold
	double error;	///< Best error (the corresponding solution may be removed for failing other constraints)
	unsigned int nIterations;	///< Number of iterations
	unsigned long long nLOCalls;	///< Number of local optimisation calls
	unsigned int nValidatorCalls;	///< Total number of validator calls on the validation and the observation set

	/** @name IterationT */ //@{
	//An empty generator indicates generator failure. An invalid score indicates either model instantiation, or SPRT failure
	typedef tuple<unsigned int, vector<size_t>, optional<double>, double> IterationT;	///< An iteration [Iteration no; generator; error; associated solution; eta]
	static constexpr unsigned int iIteration=0;	///< Index for the iteration number
	static constexpr unsigned int iGenerator=1;	///< Index of the generator
	static constexpr unsigned int iError=2;	///< Index for the model error. If the model does not pass the SPRT, not valid
	static constexpr unsigned int iEta=3;	///< Index of the confidence metric (ln(probability that no good model sampled))
	//@}

	list<IterationT> history;	///< History

	/**@brief Default constructor*/
	RANSACDiagnosticsC() : flagSuccess(false), inlierRatio(-1), finalEpsilonOb(-1), finalEpsilonVl(-1), finalDelta(-1), finalDecisionTh(-1), error(-1), nIterations(0), nLOCalls(0), nValidatorCalls(0)
	{}
};	//struct RANSACDiagnosticsC

/**
 * @brief RANSAC
 * @tparam ProblemT A RANSAC problem
 * @tparam RNGT A random number generation engine
 * @pre \c ProblemT is a model of RANSACProblemConceptC
 * @pre \c RNGT is a c++ pseudo-random number generator (unenforced)
 * @remarks Terminology:
 * 	- Observation set: The set from which the hypotheses are generated
 * 	- Validation set: The set on which the hypotheses are evaluated. May be identical, or overlapping with the observation set. Statistical independence requires mutually exclusive sets, though.
 * @remarks The algorithm employs the following variants and add-ons
 * - PROSAC: The original algorithm is modified to employ a fully random sampling (as opposed to semi-random)
 * - WaldSAC:
 * 		- Simultaneous parallel evaluation of hypotheses is likely to introduce delays in the update of the test parameters
 * 		- Termination condition: Instead of solving Eq 15, \f$ \alpha \f$ is assumed constant within a test. However, a test is updated as soon as the current estimate of \f$ \delta \f$ is
 * 		significantly different from the value for which the test is designed
 * - MSAC: Instead of inlier count, the quality of a hypothesis is assessed via a measure of the fitness error
 * - LO-RANSAC:
 * 		- A problem-defined kernel is executed in a RANSAC loop
 * 		- LO-RANSAC iterations are performed on the inliers in the observation set. If the size of this inner observation set is not significantly larger than the minimal generator sample, LO-RANSAC is not triggered (so that no iterations are wasted on unpromising hypotheses)
 * 		- Active only in single-objective case
 * - Multiobjective operation (MO-RANSAC): The process can return more than one hypothesis, if they are significantly different (either algebraically, or inlier sets)
 * 		- When a new model is instantiated, if it has sufficiently many inliers, compute the amount of overlap with the existing objectives
 * 		- If it is significantly different from all objectives, extend the objective set
 * 		- Otherwise, if it is better than the most similar objective, update
 * 		- Redundant mode suppression at the termination
 * 		- Epsilon pertains to the worst solution in the objective set
 * 		- Eta is updated by adding the log rejection probability for the weakest model. This is an optimistic approximation. However, eta is set to its correct value whenever a new test is designed.
 * - Validation: The validation set may be a subset of the original data (for faster operation), or an entirely different set (for statistical independence)
 * - Bucketising:
 * 		- The dataset is partitioned into buckets, to ensure that more representative samples are drawn. Sample generator returns the sample with maximum diversity, after a preset number of attempts
 * 		- The validation set can be downsampled to provide a uniform coverage of the dataset. More efficient operation and fairer evaluation.
 * 		- The validation set is reordered so that subsequent elements belong to different buckets- for faster rejection of hypotheses that explain only a local cluster
 * - Top-N criterion:
 * 		- If specified, the operation runs for a fixed number of iterations, and terminates when the result is within the top N with a specified confidence.
 * 		- Alternatively, one can specify the percentage of eligible all-inlier hypothesis, when the inlier ratio is not known.
 *
 * @remarks R1: "Randomized RANSAC with Sequential Probability Ratio Test," J. Matas, O. Chum, PAMI, 2008
 * @remarks R2: "Locally Optimized RANSAC, " O. Chum, J. Matas, J. Kittler, DAGM 2003
 * @remarks R3: "MLESAC: A New Robust Estimator with Application to Estimating Image Geometry," P. Torr, A. Zisserman, CVIU, 1998
 * @remarks R4: "Matching with PROSAC: Progressive Sample Consensus	," O. Chum, J. Matas, CVPR 2005
 * @remarks R5: "Order Statistics of RANSAC and Their Practical Application, " E. Imre, A. Hilton, 2013, under review for IJCV
 * @remarks Usage notes
 *	- Scenario: MO-RANSAC, two models
 *		- Problem: the validation set is ordered so that the validators belonging to the first model come before those belonging to the second
 *			- SPRT tests the validators sequentially. So, even the true second model is tested, it can be rejected, as it first encounters the validators of the first model
 *			- Solution: Either mix the validators well, or force longer tests by setting a very high model cost (or disable WaldSAC).
 *		- Problem: Too few observations/validators for the second model
 *			- Hypotheses belonging to the second model are not instantiated, or they score low
 *			- Solution- too few observations: Increase minIteration.
 *			- Solution- too few validators: Relax the model association criteria, or increase maxSolution
 *	- Scenario: MO-RANSAC, two distinct models with similar inlier sets.
 *		- The validator set supports both models. External information is required to resolve the ambiguity.
 *	- Scenario: The first N elements in the observation and the validation set are identical, with the same order. PROSAC is active.
 *		- Problem: Too many validations in the first couple of iterations
 *			- Consider the first iteration. Since the first generator is identical to the top validators, SPRT starts off with multiple inliers. Erosion of such a strong positive evidence takes many outliers.
 *			- Solution: Decorrelation. Remove some of the top validators, or random shuffling
 *	- Scenario: In multithreaded mode, two runs with the same random number generator seed yield different results
 *		- Problem: In multithreaded operation, the algorithm flow is not stable
 *			- When a thread finishes processing a generator, it can alter the RANSAC state- SPRT threshold, best score etc. And the order in which threads finish their tasks is not known. Therefore, since a generator can be evaluated against different SPRT thresholds, it can be accepted in one run, and rejected in another, i.e., the flow is different in each run.
 *			- Solution: If repeatability is important, consider single-threaded operation
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT, class RNGT=mt19937_64>
class RANSACC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<ProblemT>));
	//@endcond

	private:

		typedef typename ProblemT::model_type ModelT;	///< A model

		typedef vector<size_t> IndexListT;	///< An index list
		typedef iterator_range< typename vector<size_t>::const_iterator > IndexRangeT;	///< A range of indices
		typedef set<unsigned int> GeneratorT;	///< A generator

		typedef dynamic_bitset<> BitsetT;	///< Bitset type

	public:
		/** @name Solution */ //@{
		typedef tuple<double, ModelT, IndexListT, IndexListT, bool> solution_type;	///< A potential solution
		static constexpr unsigned int iModelError=0;	///< Index of the model error in solution_type
		static constexpr unsigned int iModel=1;	///< Index of the model in solution_type
		static constexpr unsigned int iInlierList=2;	///< Index of the validation set inliers in solution_type
		static constexpr unsigned int iGenerator=3;	///< Index of the generator in solution_type
		static constexpr unsigned int iFlagLO=4;	///< Index of the LO flag in solution_type . \c true if the hypothesis is generated by the LO solver
		//@}

		typedef multimap<double, solution_type> result_type;	///< Result type. [Error; Solution]

		typedef ProblemT problem_type;	///< Problem type
		typedef RNGT rng_type;	///< Random number generator type

	private:

		/** @name AuxiliaryT */
		typedef tuple<BitsetT, BitsetT > AuxiliaryT;	///< Auxiliary information for \c solution_type
		static constexpr unsigned int iInlierBitsetV=0;	///< Index of the bitset indicating the validation set inliers
		static constexpr unsigned int iInlierBitsetO=1;	///< Index of the bitset indicating the observation set inliers

		/**@name TestRecordT */ //@{
		typedef tuple<unsigned int, double> TestRecordT; ///< A test record. [run length; threshold]
		static constexpr unsigned int iRunLength=0;	///< Index of the run length in TestRecordT
		static constexpr unsigned int iThreshold=1;	///< Index of the decision threshold in TestRecordT
		//@}

		/** @name EvaluationRecordT */ //@{
		typedef tuple<double, IndexListT, unsigned int, BitsetT, BitsetT> EvaluationRecordT;	///< A model evaluation record. [error; inliers; nTested]
		static constexpr unsigned int iError=0;	///< Index of the model error in EvaluationRecordT
		static constexpr unsigned int iInliers=1;	///< Index of the inlier indices in EvaluationRecordT
		static constexpr unsigned int iTestedCount=2;	///< Index of the number of points tested in EvaluationRecordT
		static constexpr unsigned int iBitsetV=3;	///< Index of the inlier indices, as a bitset- validation set
		static constexpr unsigned int iBitsetO=4;	///< Index of the inlier indices, as a bitset- observation set
		//@}

		/**@name OperationT */ //@{
		enum ActionT{ADD, REPLACE, REMOVE};	///< Action labels: ADD inserts a new model into the current solution set. REPLACE replaces an existing solution. REMOVE removes a solution
		typedef tuple<int, ActionT> OperationT;	///< An operation. [Index in the current hypothesis set; Index in the current solution set; Action].
		static constexpr unsigned int iTarget=0;	///< Index of the target component
		static constexpr unsigned int iAction=1;	///< Index of the action component
		//@}

		/** @name Implementation details */ //@{
		static void ValidateParameters(const RANSACParametersC& parameters);	///< Validates the parameters

		//SPRT design
		static double DesignTest( double delta, double epsilon, double costPerModel);	///< Designs an SPRT
		static tuple<double, double, double> UpdateTest(const list<TestRecordT>& testRecords, double delta, double epsilonVl, double costPerModel, unsigned int sGenerator);	///< Designs an SPRT and updates various associated quantites

		typedef tuple<double, double > EtaTermT;	///< Structure for the computation of eta. [Term; per iteration attenuation]
		static constexpr unsigned int iEtaTerm=0;
		static constexpr unsigned int iAttenuation=1;

		static tuple<double, vector<EtaTermT>  > ComputeEta(const ProblemT& problem, const map<unsigned int, AuxiliaryT>& auxInfo, const list<TestRecordT>& testRecords, double eligibilityRatio, double decisionTh);	///< Computes the inconfidence measure

		//Hypothesis generation
		static GeneratorT GenerateSample(const ProblemT& problem, RNGT& rng, const IndexRangeT& activeRange, unsigned int sSample, unsigned int minDiversity);	///< Generates a sample

		//Hypothesis evaluation
		static tuple<bool, IndexListT, unsigned int, double> ApplySPRT(const ProblemT& problem, const ModelT& model, double lambdaI, double lambdaO);	///< Applies the sequential probability ratio test and evaluates the model

		//Combined model generation and evaluation
		static tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int> RunIteration(const ProblemT& problem, RNGT& rng, const IndexRangeT& , double lambdaI, double lambdaO, bool flagLO);	///< Performs a RANSAC iteration

		//LO-RANSAC
		static IndexListT MakeInlierList(const ProblemT& problem, const ModelT& model, const IndexListT& indexList);	///< Identifies the inlier observations to a given model
		static tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int> PerformLO(const ProblemT& problem, RNGT& rng, const IndexListT& activeRange, double lambdaI, double lamdaO, unsigned int nLOIteration);	///< Performs local optimisation on a hypothesis
		static bool CheckLO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, map<unsigned int, solution_type> solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, unsigned int minValidationSupport, const RANSACParametersC& parameters);	//Checks whether local optimisation should be performed on a hypothesis

		//MO-RANSAC
		static optional<list<OperationT> > DetermineUpdateActionSO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo);	///< Determines the update action for the single-objective case
		static optional<list<OperationT> > DetermineUpdateActionMO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, unsigned int minValidationSupport, const RANSACParametersC& parameters);	///< Determines the update action for the multi-objective case
		static optional<list<OperationT> > DetermineUpdateAction(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, unsigned int minValidationSupport, const RANSACParametersC& parameters);	///< Determines the update action
		static tuple<unsigned int, double> ApplyUpdate(map<unsigned int, solution_type>& solutionSet, map<unsigned int, AuxiliaryT>& auxInfo, EvaluationRecordT& record, const ProblemT& problem, const list<OperationT> & operation, const ModelT& model, const GeneratorT& generator, const IndexListT& observationSet, bool flagLOSolution, unsigned int cIteration);	///< Applies the update action to the solution set

		//Finalise
		static map<unsigned int, solution_type> SuppressObjectives(const ProblemT& problem, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo,  const IndexListT& observationSet, unsigned int minValidationSupport, const RANSACParametersC& parameters);	///< Suppresses any redundant objectives
		static result_type PruneSolutionSet(const map<unsigned int, solution_type>& solutionSet, double maxError, unsigned int minValidationSupport);	///< Prunes the solution set
		//@}

	public:

		static RANSACDiagnosticsC Run(result_type& result, ProblemT& problem, RNGT& rng, const RANSACParametersC& parameters);	///< Runs the algorithm
};	//RANSACC

/**
 * @brief Validates the parameters
 * @param[in] parameters Parameter set
 * @throws invalid_argument If any of the parameters is invalid
 */
template<class ProblemT, class RNGT>
void RANSACC<ProblemT, RNGT>::ValidateParameters(const RANSACParametersC& parameters)
{
	if(parameters.minObservationSupport<1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.minObservationSupport must be >=1 . Value: ")+lexical_cast<string>(parameters.minObservationSupport) ) );

	if(parameters.maxErrorRatio<1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.maxErrorRatio must be >1 . Value: ")+lexical_cast<string>(parameters.maxErrorRatio) ) );

	if(parameters.growthRate<=0 || parameters.growthRate>1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.growthRate must be in (0,1]. Value: ")+lexical_cast<string>(parameters.growthRate)));

	if(parameters.epsilon<=0 || parameters.epsilon>=1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.epsilon must be in (0,1). Value: ") + lexical_cast<string>(parameters.epsilon)) );

	if(parameters.delta<=0 || parameters.delta>=1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.delta must be in (0,1). Value: ") + lexical_cast<string>(parameters.delta)) );

	if(parameters.epsilon<=parameters.delta)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.delta must be < parameters.epsilon. Value: ") + lexical_cast<string>(parameters.delta)));

	if(parameters.maxDeltaTolerance<=0 || parameters.maxDeltaTolerance>1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.maxDeltaTolerance must be in (0,1]. Value: ") + lexical_cast<string>(parameters.maxDeltaTolerance)) );

	if(parameters.minModelSimilarity<0 || parameters.minModelSimilarity>1)
		throw(invalid_argument( string("RANSACC::ValidateParameters : parameters.minModelSimilarity must be in [0,1]. Value: ")  + lexical_cast<string>(parameters.minModelSimilarity) ) );

	if(parameters.minCoverage<0 || parameters.minCoverage>1)
		throw(invalid_argument( string("RANSACC::ValidateParameters : parameters.minCoverage must be in [0,1]. Value: ")  + lexical_cast<string>(parameters.minCoverage) ) );

	if(parameters.parsimonyFactor<1)
		throw(invalid_argument( string("RANSACC::ValidateParameters : parameters.parsimonyFactor must be >=1. Value: ")  + lexical_cast<string>(parameters.parsimonyFactor) ) );

	if(parameters.diversityFactor<0)
		throw(invalid_argument( string("RANSACC::ValidateParameters : parameters.diversityFactor must be >=0. Value: ")  + lexical_cast<string>(parameters.diversityFactor) ) );

	if(parameters.maxSolution==0)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.maxSolution cannot be 0. Value: ") + lexical_cast<string>(parameters.maxSolution)));

	if(parameters.minConfidence<=0 || parameters.minConfidence>1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.minConfidence must be in (0,1]. Value: ") + lexical_cast<string>(parameters.minConfidence)) );

	if(parameters.eligibilityRatio<=0 || parameters.eligibilityRatio>1)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.eligibilityRatio must be in (0,1]. Value: ") + lexical_cast<string>(parameters.eligibilityRatio)) );

	if(parameters.maxIteration==0)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.maxIteration must be positive. Value: ") + lexical_cast<string>(parameters.maxIteration)) );

	if(parameters.maxIteration < parameters.minIteration)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.maxIteration cannot be smaller than parameters.minIteration. Value: ") + lexical_cast<string>(parameters.maxIteration)) );

	if(parameters.topN && ( (*parameters.topN)<=0 || (*parameters.topN)>1) )
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.topN must be in (0,1]. Value: ") + lexical_cast<string>(*parameters.topN)) );

	if(parameters.maxError < parameters.minError)
		throw(invalid_argument(string("RANSACC::ValidateParameters : parameters.maxError cannot be smaller than parameters.minError. Value: ") + lexical_cast<string>(parameters.maxError)) );
}	//void ValidateParameters(const RANSACParametersC& parameters)

/**
 * @brief Designs an SPRT
 * @param[in] delta p(eligible inlier|bad model)
 * @param[in] epsilon p(eligible inlier|good model)
 * @param[in] costPerModel Processing cost per generator/model per generator
 * @return SPRT decision threshold
 * @pre \c 0<delta<epsilon
 * @pre \c epsilon<1
 */
template<class ProblemT, class RNGT>
double RANSACC<ProblemT, RNGT>::DesignTest( double delta, double epsilon, double costPerModel)
{
	//Preconditions
	assert(delta<epsilon && delta>0);
	assert(epsilon<1);

	double c= ( (1-delta)*log( (1-delta)/(1-epsilon) ) + delta*log(delta/epsilon)); //R1 Eq8

	//Iteratively solve R1 Eq10
	double initialTh=costPerModel*c+1;
	double prevTh=initialTh;	// Decision threshold
	double decisionTh=prevTh;
	unsigned int cIteration=0;

	do	//while(cIteration<10);
	{
		decisionTh=initialTh+log(decisionTh);

		if( fabs(decisionTh-prevTh)/decisionTh < 1e-4)
			break;

		prevTh=decisionTh;
		++cIteration;
	}while(cIteration<10);

	return decisionTh;
}	//double DesignTest( double delta, double epsilon, double costPerModel)

/**
 * @brief Designs an SPRT and updates various associated quantities
 * @param[in] testRecords Record of the past tests
 * @param[in] delta p(eligible inlier|bad model)
 * @param[in] epsilonVl p(eligible inlier|good model), validators
 * @param[in] costPerModel Processing cost per generator/model per generator
 * @param[in] sGenerator Size of the generator
 * @return A tuple [decision threshold; inlier penalty; outlier penalty]
 * @pre \c 0<delta<epsilonVl
 * @pre \c 0<epsilonVl<1
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::UpdateTest(const list<TestRecordT>& testRecords, double delta, double epsilonVl, double costPerModel, unsigned int sGenerator) -> tuple<double, double, double>
{
	//Preconditions
	assert(epsilonVl>0 && epsilonVl<1);	//Finite quantites are better for stability
	assert(delta>0 && delta<epsilonVl);	//Finite quantites are better for stability

	double decisionTh=DesignTest(delta, epsilonVl, costPerModel);	//Compute the SPRT decision threshold

	double lambdaInlier=log(delta/epsilonVl);	//Penalty term for an inlier
	double lambdaOutlier=log((1-delta)/(1-epsilonVl) );	//Penalty term for an outlier

	//Normalise
	double lnDecisionTh=log(decisionTh);
	lambdaInlier/=lnDecisionTh;
	lambdaOutlier/=lnDecisionTh;

	return make_tuple(decisionTh, lambdaInlier, lambdaOutlier);
}	//auto UpdateTest(double delta, double epsilon, double costPerModel) -> tuple<double, double, double, double, double, double, double>

/**
 * @brief Computes the inconfidence measure
 * @param[in] problem RANSAC problem
 * @param[in] auxInfo Auxiliary information
 * @param[in] testRecords Test records
 * @param[in] eligibilityRatio Eligibility ratio
 * @param[in] decisionTh Current decision threshold
 * @return A tuple: Eta, the inconfidence score, and a helper structure to update eta after each iteration
 * @remarks If there are no models, inconfidence is 1, with no attenuation. The iterations run until \c maxIteration
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::ComputeEta(const ProblemT& problem, const map<unsigned int, AuxiliaryT>& auxInfo, const list<TestRecordT>& testRecords, double eligibilityRatio, double decisionTh) -> tuple<double, vector<EtaTermT> >
{
	//If there are no models, no confidence
	if(auxInfo.empty())
		return make_tuple(1, vector<EtaTermT>(1, make_tuple(1,0.9999)));	//Attenuation term set to 0.9999 to ensure that the algorithm terminates even when there are no successful hypotheses

	size_t nModels=auxInfo.size();
	size_t nObservations=problem.GetObservationSetSize();
	size_t sGenerator=problem.GeneratorSize();

	//TODO Probably eta is no longer necessary, now that we have the structure
	//If there is a single model, shortcut
	if(nModels==1)
	{
		//R1 Eq15
		double pGood=pow( eligibilityRatio*(double)get<iInlierBitsetO>(auxInfo.begin()->second).count()/nObservations, sGenerator);
		double eta=accumulate(testRecords, 0.0, [&](double eta, const TestRecordT& current){ return eta+get<iRunLength>(current)*log(1-pGood*(1-1.0/get<iThreshold>(current)));});
		vector<EtaTermT> etaUpdater(1, make_tuple(exp(eta), 1-pGood*(1-1.0/decisionTh)));
		return make_tuple(eta, etaUpdater);
	}	//if(nModels==1)

	//Compute the inlier ratios for each model

	//Since overlap is permitted, some correspondences are shared by multiple models. Identify them, and split their effect among their owners
	Array<unsigned int, Eigen::Dynamic, Eigen::Dynamic> inlierArray(nObservations, nModels); inlierArray.setZero();
	Array<unsigned int, 1, Eigen::Dynamic> occurrenceCounts(nObservations); occurrenceCounts.setZero();
	size_t iModel=0;
	for(const auto& current : auxInfo)
	{
		for(size_t c=0; c<nObservations; ++c)
			if(get<iInlierBitsetO>(current.second)[c])
			{
				inlierArray(c, iModel)=1;
				++occurrenceCounts[c];
			}	//if(get<iInlierBitsetO>(current.second)[c])
		++iModel;
	}	//for(const auto& current : auxInfo)

	typedef Array<double, 1, Eigen::Dynamic> RowVectorT;
	RowVectorT weights(nObservations);
	for(size_t c=0; c<nObservations; ++c)
		weights[c]= (occurrenceCounts[c]!=0) ? 1.0/occurrenceCounts[c]:0;

	RowVectorT inlierRatios= (weights.matrix() * inlierArray.cast<double>().matrix()).array()/nObservations;
	RowVectorT successProbability=(eligibilityRatio * inlierRatios).pow((double)sGenerator);	//Success probabilities

	//Compute eta via the inclusion exclusion principle

	//Compute the probabilities for the intersection events
	size_t nEvents=pow(2, nModels)-1;	//Discounting the empty set
	vector<pair<unsigned int, double> > eventProbability(nEvents);	//Cardinality; probability
	for(size_t c=1; c<=nEvents; ++c)
	{
		//Compute the probability of the intersection event
		size_t eventCode=c;
		double prob=0;
		double cardinality=0;
		for(size_t c2=0; c2<nModels; ++c2, eventCode>>=1)
			if(eventCode & 1)	//Odd? The model is included
			{
				prob+=successProbability[c2];
				++cardinality;
			}	//if(eventCode & 1)

		eventProbability[c-1]=make_pair(cardinality, prob);
	}	//for(size_t c=1; c<nEvents; ++c)

	//Now, compute eta
	double eta=0;
	vector<EtaTermT> etaUpdater; etaUpdater.reserve(nEvents);
	for(const auto& currentEvent : eventProbability)
	{
		//R1 Eq15 for the constituent events
		double logProb=accumulate(testRecords, 0.0, [&](double acc, const TestRecordT& current){ return acc+get<iRunLength>(current)*log(1-currentEvent.second*(1-1.0/get<iThreshold>(current)));});
		double prob = (currentEvent.first & 1 ) ? exp(logProb) : -exp(logProb);
		eta += prob;
		etaUpdater.emplace_back(prob, 1-currentEvent.second*(1-1.0/decisionTh));

	}	//for(const auto& currentEvent : eventProbability)

	return make_tuple(log(eta), etaUpdater);
}	//double ComputeConfidence(const map<unsigned int, AuxiliaryT>& auxInfo, const list<TestRecordT>& testRecords)

/**
 * @brief Generates a sample
 * @param[in] problem Problem
 * @param[in,out] rng Random number generator
 * @param[in] activeRange Range of indices over which the sampling is performed
 * @param[in] sSample Size of the sample (minimal or non-minimal generator)
 * @param[in] minDiversity If the diversity of the set exceeds this value, terminate
 * @return Generator set. If the sample generation fails, empty
 * @remarks Diversity: The number of distinct bins represented in the sample
 * @remarks If the generator fails to get a sample with \c minDiversity , it returns the sample with best diversity so far
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::GenerateSample(const ProblemT& problem, RNGT& rng, const IndexRangeT& activeRange, unsigned int sSample, unsigned int minDiversity) -> GeneratorT
{
	GeneratorT output;

	unsigned int cIteration=0;
	size_t bestDiversity=0;

	if(activeRange.size()<sSample)
		return output;

	//Iterate until a sample with minDiversity is drawn, or the max number of iterations is reached
	do
	{
		optional<GeneratorT> proxy;
	#pragma omp critical(RANSACC_GS)
		proxy=GenerateRandomCombination(rng, activeRange.size(), sSample);	//Generate a sample of indices

		if(!proxy)
			continue;

		GeneratorT generator;
		for(auto c : *proxy)
			generator.insert(activeRange[c]);

		unsigned int diversity=problem.ComputeDiversity(generator);	//Get the set of bucket ids associated with the members of the sample

		//Update the generator, after passing through a problem-dependent validator
		if(diversity>bestDiversity && problem.ValidateGenerator(generator))
		{
			bestDiversity=diversity;
			output.swap(generator);

			if(bestDiversity>=minDiversity)	//If the diversity requirement is achieved, break
				break;
		}	//if(diversity>minDiversity)

		++cIteration;
	}while(cIteration<5);

	return output;
}	//optional<GeneratorT> GenerateSample(ProblemT& problem, RNGT& rng, const IndexRangeT& activeRange)

/**
 * @brief Applies the sequential probability ratio test and evaluates the model
 * @param[in] problem RANSAC problem
 * @param[in] model Model to be tested
 * @param[in] lambdaI Inlier penalty term (normalised w/decision threshold)
 * @param[in] lambdaO Outlier penalty term (normalised w/decision threshold)
 * @return A tuple holding the test result, inlier indices, the number of validation points tested, and the model errpr
 * @pre \c lambdaI<0 and \c lambdaO>0
 * @remarks If the test fails, the score is merely a partial evaluation.
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::ApplySPRT(const ProblemT& problem, const ModelT& model, double lambdaI, double lambdaO) -> tuple<bool, IndexListT, unsigned int, double>
{
	//Preconditions
	assert(lambdaI<0);
	assert(lambdaO>0);

	tuple<bool, IndexListT, unsigned int, double> output(false, IndexListT(), 0, numeric_limits<double>::infinity());

	//R1 Algorithm 3

	double lambda=0;	//Test score
	unsigned int cTested=0;	//Tested points
	size_t nValidation=problem.GetValidationSetSize();	//Cardinality of the validation set

	IndexListT inlierList; inlierList.reserve(nValidation);
	double modelError=0;

	//Test over the validators
	// The penalty terms are normalised by decisionTh, so the threshold is 1
	for(; cTested<nValidation && lambda<=1; ++cTested)
	{
		bool flagInlier;	//\c true if the validator is an inlier to the model
		double error;	// Error for the current validator
		tie(flagInlier, error)=problem.EvaluateValidator(model, cTested);
		modelError+=error;

		if(flagInlier)
		{
			lambda+=lambdaI;
			inlierList.push_back(cTested);
		}
		else
			lambda+=lambdaO;
	}	//for(;cTested<nValidation; ++cTested)

	//If the test succeeds, cTested==nValidation
	output=make_tuple(cTested==nValidation && !inlierList.empty(), IndexListT(), cTested, modelError);
	inlierList.shrink_to_fit();
	get<1>(output).swap(inlierList);

	return output;
}	//tuple<bool, unsigned int, unsigned int> ApplySPRT(const ProblemT& problem, const ModelT& model, size_t nValidation, double decisionTh, double lambdaI, double lambdaO)

/**
 * @brief Performs a RANSAC iteration
 * @param[in] problem RANSAC problem
 * @param[in,out] rng Random number engine
 * @param[in] activeRange A range holding the active observation set
 * @param[in] lambdaI Normalised inlier penalty for SPRT
 * @param[in] lambdaO Normalised outlier penalty for SPRT
 * @param[in] flagLO \c true if the function is called within LO
 * @return A tuple: [generator; model; evaluation record; SPRT flag; number of models]. Bitset is not initialised if SPRT fails
 * @remarks Deciphering failures
 * 	- \c flagPass=false and empty generator: Sample generation failed
 * 	- \c flagPass=false, non-empty generator, \c nModel=0 : No valid model from the sample
 * 	- \c flagPass=false but non-empty generator and \c nModel>0 : SPRT failed
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::RunIteration(const ProblemT& problem, RNGT& rng, const IndexRangeT& activeRange, double lambdaI, double lambdaO, bool flagLO) -> tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int>
{
	BitsetT defaultBitset=MakeBitset<BitsetT>(IndexListT(), problem.GetValidationSetSize());
	BitsetT emptyBitset;
	EvaluationRecordT defaultRecord(numeric_limits<double>::infinity(), IndexListT(), 0, defaultBitset, emptyBitset);
	tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int> output(GeneratorT(), ModelT(), defaultRecord, false, 0);

	//Generate a sample
	unsigned int sGenerator= flagLO ? problem.LOGeneratorSize() : problem.GeneratorSize();

	GeneratorT generator=GenerateSample(problem, rng, activeRange, sGenerator, problem.GeneratorSize());

	if(generator.empty())	//If fails, quit
		return output;
	else
		get<0>(output)=generator;

	//Generate hypotheses: Model validation assumed to be performed externally, within the problem
	list<ModelT> models= flagLO ? problem.GenerateModelLO(generator) : problem.GenerateModel(generator);

	if(models.empty())	//If fails, quit
		return output;
	else
		get<4>(output)=models.size();

	//Evaluate the hypotheses and keep the best
	EvaluationRecordT bestRecord=defaultRecord;
	size_t bestIndex=0;
	double bestError=numeric_limits<double>::infinity();
	bool bestPass=false;	// \c true if the best model passes SPRT
	size_t c=0;
	for(const auto& current : models)
	{
		bool flagPass;
		unsigned int nTested;
		double error;
		IndexListT inliers;
		tie(flagPass, inliers, nTested, error)=ApplySPRT(problem, current, lambdaI, lambdaO);

		//Update
		if(error<bestError)
		{
			bestRecord=EvaluationRecordT(error, inliers, nTested, defaultBitset, emptyBitset);
			bestIndex=c;
			bestPass=flagPass;
		}	//if(score>bestScore)
		++c;
	}	//for(const auto& current : models)

	//If SPRT passes, initialise the bitset
	if(bestPass)
		get<iBitsetV>(bestRecord)=MakeBitset<BitsetT>(get<iInliers>(bestRecord), problem.GetValidationSetSize());

	//Output
	get<1>(output)=*next(models.begin(), bestIndex);	//Save the model
	get<2>(output)=bestRecord;	//Save the evaluation record
	get<3>(output)=bestPass;	//If the best sample passed SPRT, true

	return output;
}	//tuple<list<ModelT>, GeneratorT, vector<EvaluationRecordT> > RunIteration(const ProblemT& problem, RNGT& rng, const IndexRangeT& activeRange, double decisionTh, double lambdaI, double lambdaO)

/**
 * @brief Identifies the inlier observations to a given model
 * @param[in] problem RANSAC problem
 * @param[in] model Hypothesis seeking inliers
 * @param[in] indexList Observation set
 * @return An index list holding the inliers to \c model
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::MakeInlierList(const ProblemT& problem, const ModelT& model, const IndexListT& indexList) -> IndexListT
{
	IndexListT output; output.reserve(indexList.size());
	for(auto current : indexList)
		if(problem.EvaluateObservation(model, current))
			output.push_back(current);

	output.shrink_to_fit();
	return output;
}	//IndexListT MakeInlierList(const ProblemT& problem, const ModelT& model, const IndexListT& indexList)

/**
 * @brief Performs local optimisation on a hypothesis
 * @param[in] problem RANSAC problem
 * @param[in,out] rng Random number generator
 * @param[in] activeRange Active observations
 * @param[in] lambdaI Normalised inlier penatly
 * @param[in] lambdaO Normalised outlier penalty
 * @param[in] nLOIteration Number of LO iterations
 * @return A tuple: [generator; model; evaluation record; success flag; number of tested validators]
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::PerformLO(const ProblemT& problem, RNGT& rng, const IndexListT& activeRange, double lambdaI, double lambdaO, unsigned int nLOIteration) ->  tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int>
{
	BitsetT defaultBitset=MakeBitset<BitsetT>(IndexListT(), problem.GetValidationSetSize());
	BitsetT emptyBitset;
	EvaluationRecordT defaultRecord(numeric_limits<double>::infinity(), IndexListT(), 0, defaultBitset,  emptyBitset);
	tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int> output(GeneratorT(), ModelT(), defaultRecord, false, 0);
//	tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int> output(GeneratorT(), ModelT(), EvaluationRecordT(), false, 0);

	double bestError=numeric_limits<double>::infinity();
	unsigned int nTestedCount=0;	//Total number of tested validators

	GeneratorT generator;
	ModelT model;
	EvaluationRecordT record;
	bool flagPass;
	unsigned int nModel;
	for(unsigned int c=0; c<nLOIteration; ++c)
	{
		std::tie(generator, model, record, flagPass, nModel)=RunIteration(problem, rng, activeRange, lambdaI, lambdaO, true);

		if(nModel>0)	//If the iteration yielded a model
		{
			if(get<iError>(record) < bestError )	//And if better than the current best
			{
				bestError=get<iError>(record);
				output=make_tuple(generator, model, record, flagPass, 0);
			}	//if(currentResult && bestScore < get<iScore>(get<2>(*currentResult)))

			nTestedCount+=get<iTestedCount>(record);
		}
	}	//for(unsigned int c=0; c<nLOIteration; ++c)

	get<4>(output)=nTestedCount;

	return output;

}	//optional< tuple<GeneratorT, ModelT, EvaluationRecordT> > PerformLO(const ProblemT& problem, RNGT& rng, const ModelT& model, const IndexListT& indexList, unsigned int nLOIteration)

/**
 * @brief Checks whether local optimisation should be performed on a hypothesis
 * @param[in] problem RANSAC problem
 * @param[in] model Current hypothesis
 * @param[in] record Evaluation record for the hypothesis
 * @param[in] solutionSet Current solution set. Pass-by-value, as a local copy is modified
 * @param[in] auxInfo Auxiliary info on the solutions.
 * @param[in] minValidationSupport Minimum support within the validation set for a new solution
 * @param[in] parameters RANSAC parameters
 * @return \c true if the model is eligible for LO
 */
template<class ProblemT, class RNGT>
bool RANSACC<ProblemT, RNGT>::CheckLO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record,  map<unsigned int, solution_type> solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, unsigned int minValidationSupport, const RANSACParametersC& parameters)
{
	//Not only better, but merely promising hypotheses can trigger LO. So, increase the errors for the existing solutions
	for(auto& current : solutionSet)
		get<iModelError>(current.second)*=parameters.maxErrorRatio;

	optional<list<OperationT> > updateOp=DetermineUpdateAction(problem, model, record, solutionSet, auxInfo, minValidationSupport, parameters);	//Mock update on the current state. If the hypothesis is potentially better than the existing solutions, LO

	//If there is an add or a replace, LO
	bool flagAddReplace=false;
	if(updateOp)
		for(const auto& current : *updateOp)
			flagAddReplace|= (get<iAction>(current)==ActionT::ADD) || (get<iAction>(current)==ActionT::REPLACE);

	return flagAddReplace;
}	//bool CheckLO(const EvaluationRecordT& record,  vector<solution_type> solutionSet, const RANSACParametersC& parameters, unsigned int minObservationSupport, unsigned int minValidationSupport)

/**
 * @brief Determines the update action for the single-objective case
 * @param[in] problem RANSAC problem
 * @param[in] model Current hypothesis
 * @param[in] record Evaluation record for the hypothesis
 * @param[in] solutionSet Current solution set
 * @param[in] auxInfo Auxiliary info for the solutions
 * @return A sequence of update operations. If no action to be taken, invalid
 * @pre \c solutionSet is not empty
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::DetermineUpdateActionSO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo) -> optional<list<OperationT> >
{
	assert(!solutionSet.empty());

	optional<list<OperationT>> output;
	if(get<iModelError>(solutionSet.begin()->second) > get<iError>(record))
	{
		output=list<OperationT>();
		output->push_back(OperationT(solutionSet.begin()->first, ActionT::REPLACE));
	}

	return output;
}	//auto DetermineUpdateActionSO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const vector<solution_type>& solutionSet) -> optional<OperationT>

/**
 * @brief Determines the update action for the multi-objective case
 * @param[in] problem RANSAC problem
 * @param[in] model Current hypothesis
 * @param[in] record Evaluation record for the hypothesis
 * @param[in] solutionSet Current solution set
 * @param[in] auxInfo Auxiliary info for the solutions
 * @param[in] minValidationSupport Minimum support within the validation set for a new solution
 * @param[in] parameters RANSAC parameters
 * @return A sequence of update operations. If no action to be taken, invalid
 * @pre \c record has a non-empty inlier set
 * @pre \c solutionSet is not empty
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::DetermineUpdateActionMO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, unsigned int minValidationSupport, const RANSACParametersC& parameters) -> optional<list<OperationT> >
{
	//TODO When the code stablises, divide into functions
	//Preconditions
	assert(!get<iInliers>(record).empty());
	assert(!solutionSet.empty());

	list<OperationT> operationSequence;
	optional<list<OperationT> > output;

	map<unsigned int, BitsetT> solutionInliers;
	for(const auto& current : auxInfo)
		solutionInliers[current.first]= (!get<iBitsetO>(record).empty()) ? get<iInlierBitsetO>(current.second) : get<iInlierBitsetV>(current.second);

	BitsetT recordInliers = (!get<iBitsetO>(record).empty()) ? get<iBitsetO>(record) : get<iBitsetV>(record);

	//Special cases

	//If the proposed solution is too weak, abort
	//This cannot happen if solutionSet is empty
	if(recordInliers.count()<minValidationSupport)
		return output;

	//If there is one solution, and if it does not satisfy the minimum support constraint, replace
	//This can happen only if the first solution that passes SPRT has a support below the minimum
	if( solutionInliers.begin()->second.count()<minValidationSupport)
	{
		operationSequence.push_back(OperationT(auxInfo.begin()->first, ActionT::REPLACE));
		output=operationSequence;
		return output;
	}	//if( get<iInlierBitsetV>(auxInfo.begin()->second).count()<minValidationSupport)

	//Association
	multimap<double, unsigned int> associatedObjectives;	// Solutions associated to model. [Error, model id]. Not sorted wrt similarity!
	for(const auto& current : solutionSet)
	{
		//Algebraic similarity
		if(!problem.IsSimilar(model, get<iModel>(current.second)))
			continue;

		//Inlier set similarity

		const auto& currentInliers=solutionInliers.find(current.first)->second;

		//Compute the overlap index
		size_t cardinality= (recordInliers & currentInliers ).count();
		double similarity= (double)cardinality /min(recordInliers.count(), currentInliers.count() );

		//If above the minimum similarity threshold, store
		if(similarity>=parameters.minModelSimilarity)
			associatedObjectives.emplace(get<iError>(solutionSet.find(current.first)->second), current.first);
	}	//for(const auto& current : solutionSet)

	//Association successful
	if(!associatedObjectives.empty() )
	{
		set<unsigned int> modifiedSolutions;	//Solutions to be modified in some way

		//Attempt to replace the best model
	//	unsigned int iAssociated=associatedObjectives.begin()->second;
	//	if( get<iError>(record) < get<iModelError>(solutionSet.find(iAssociated)->second) )
		if( get<iError>(record) < associatedObjectives.begin()->first )
		{
			operationSequence.push_back(OperationT(associatedObjectives.begin()->second, ActionT::REPLACE) );
			modifiedSolutions.insert(associatedObjectives.begin()->second);
		}

		/*
		//Purging: If the new hypothesis is successful, the remaining associated objectives are redundant. If unsuccessful, check overlap
		unsigned int iAssociated=associatedObjectives.begin()->second;
		associatedObjectives.erase(associatedObjectives.begin());	//The first element is already processed
		if(!operationSequence.empty())
		{
			for(const auto& current : associatedObjectives)	//Generate a remove operation for the rest
				operationSequence.push_back(OperationT(current.second, ActionT::REMOVE));
		}
		else
		{
			const AuxiliaryT& rBest=auxInfo.find(iAssociated)->second;
			for(const auto& current : associatedObjectives)
			{
				const AuxiliaryT& rCurrent=auxInfo.find(current.second)->second;

				size_t cardinality= (get<iInlierBitsetV>(rBest) & get<iInlierBitsetV>(rCurrent) ).count();
				double similarity= (double)cardinality /min(get<iInlierBitsetV>(rBest).count(), get<iInlierBitsetV>(rCurrent).count() );

				if(similarity>=parameters.minModelSimilarity)
					operationSequence.push_back(OperationT(current.second, ActionT::REMOVE));
			}
		}*/

		//Assumption: Any solutions similar to record should be similar to each other. Therefore, purge them
		associatedObjectives.erase(associatedObjectives.begin());	//The first element is already processed
		for(const auto& current : associatedObjectives)	//Generate a remove operation for the rest
		{
			operationSequence.push_back(OperationT(current.second, ActionT::REMOVE));
			modifiedSolutions.insert(current.second);
		}

		//Attempt to eliminate the weakest solution
		//Rationale: The algorithm may pick up a spurious model early on, when the total number of inliers is low (hence "significant" change), which falsely depresses the confidence.
		//Removal could be difficult if its support is split between other models, if there are more than 2 models. Slapping a REMOVE label forces a significance check
		if(solutionSet.size()>2)
		{
			//TODO Redundant: merge with the next case
			//Find the worst solution
			map<double, unsigned int, greater<double> > ranker;
			for(const auto& current : solutionSet)
				ranker.emplace(get<iModelError>(current.second), current.first);

			//If not already updated or removed, attempt to remove
			if(modifiedSolutions.find(ranker.begin()->second)==modifiedSolutions.end())
				operationSequence.push_back(OperationT(ranker.begin()->second, ActionT::REMOVE));
		}

	}	//if(!associatedObjectives.empty() )
	else //Association failure
	{
		//If there are any empty slots, add
		if(solutionSet.size()<parameters.maxSolution)
			operationSequence.push_back(OperationT(-1, ActionT::ADD));
		else	//Try to dislodge the worst solution
		{
			//Find the worst solution
			map<double, unsigned int, greater<double> > ranker;
			for(const auto& current : solutionSet)
				ranker.emplace(get<iModelError>(current.second), current.first);

			if(get<iError>(record)<ranker.begin()->first)
				operationSequence.push_back(OperationT(ranker.begin()->second, ActionT::REPLACE));
		}	//if(solutionSet.size()<parameters.maxSolution)
	}	//if(!associatedObjectives.empty() )

	//Coverage test
	if(!operationSequence.empty())
	{
		//Add is never accompanied by another action
		//Add: Association failed, but there is a free slot
		if( get<iAction>(*operationSequence.begin()) == ActionT::ADD)
		{
			BitsetT currentPool(recordInliers.size());
			for(const auto& current : solutionInliers)
				currentPool|=current.second;

			BitsetT proposedPool = currentPool | recordInliers;

			//If there is a significant increase, keep the action
			if( (double)proposedPool.count()/currentPool.count() > parameters.parsimonyFactor)
				output=operationSequence;

			operationSequence.pop_front();	//Processed, so discard
		}	//if( get<iAction>(*operationSequence.begin()) == ActionT::ADD)

		//If a replace action exists, it is always the first element
		map<unsigned int, BitsetT> postReplaceInliers=solutionInliers;		//auxInfo after the replace operation
		if( !operationSequence.empty() && get<iAction>(*operationSequence.begin()) == ActionT::REPLACE)
		{
			BitsetT commonPool(recordInliers.size());
			for(const auto& current : solutionInliers)
				if((int)current.first!=get<iTarget>(*operationSequence.begin()))
					commonPool|=current.second;

			BitsetT currentPool= commonPool | solutionInliers.find(get<iTarget>(*operationSequence.begin()))->second;
			BitsetT proposedPool = commonPool | recordInliers;

			//If there is not a significant decrease in coverage, keep the action
			if( (double)proposedPool.count()/currentPool.count() > parameters.diversityFactor)
			{
				output=list<OperationT>();
				output->push_back(*operationSequence.begin());
				postReplaceInliers[get<iTarget>(*(operationSequence.begin()))]=recordInliers;	//The key is wrong. But it does not matter, as this is just an internal temporary
			}

			operationSequence.pop_front();	//Processed, so discard

		}	//if( get<iAction>(*operationSequence.begin()) == ActionT::REPLACE)

		//Any elements remaining in operationSequence are removes
		if(!operationSequence.empty())
		{
			//If there are multiple remove operations, test them separately
			//The actual diversity loss may be above the threshold, as each test assumes that it is the only removal operation
			list<OperationT> removeList;

			//If the output is empty, no add or replace. So, use the observation inliers
			size_t nInliers = recordInliers.size();
			if(!output)
			{
				nInliers=problem.GetObservationSetSize();
				for(auto& current : postReplaceInliers)
					current.second= get<iInlierBitsetO>(auxInfo.find(current.first)->second);
			}

			for(const auto& currentOp : operationSequence)
			{
				//Build the current and the proposed pools
				BitsetT currentPool(nInliers);
				BitsetT proposedPool(nInliers);
				for(const auto& current : postReplaceInliers)
				{
					currentPool|=current.second;
					if((int)current.first!=get<iTarget>(currentOp))
						proposedPool|=current.second;
				}	//for(const auto& current : postReplaceAuxInfo)

				//If the coverage test passes, append to the output
				if( (double)proposedPool.count()/currentPool.count() >= parameters.diversityFactor)
					removeList.push_back(currentOp);
			}	//for(const auto& currentOp : removed)

			if(!removeList.empty())
			{
				if(!output)
					output=list<OperationT>();	//In case there is no prior REPLACE action

				output->splice(output->end(), removeList, removeList.begin(), removeList.end());	//Move removeList into the output
			}	//if(!removeList.empty())
		}	//if(!operationSequence.empty())
	}	//if(!operationSequence.empty())

	return output;
}	//auto DetermineUpdateActionMO(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const vector<solution_type>& solutionSet) -> optional<OperationT>

/**
 * @brief Determines the update action
 * @param[in] problem RANSAC problem
 * @param[in] model Current hypothesis
 * @param[in] record Evaluation record for the hypothesis
 * @param[in] solutionSet Current solution set
 * @param[in] auxInfo Auxiliary info for the solutions
 * @param[in] minValidationSupport Minimum support within the validation set for a new solution
 * @param[in] parameters RANSAC parameters
 * @return A sequence of update operations. If no action to be taken, invalid
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::DetermineUpdateAction(const ProblemT& problem, const ModelT& model, const EvaluationRecordT& record, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, unsigned int minValidationSupport, const RANSACParametersC& parameters) -> optional<list<OperationT> >
{
	//Preconditions
	assert(!get<iInliers>(record).empty());

	//If the solution set is empty, add
	if(solutionSet.empty())
	{
		optional<list<OperationT> > output=list<OperationT>();
		output->push_back(OperationT(-1, ActionT::ADD));
		return output;
	}

	//Single-objective
	if(parameters.maxSolution==1)
		return DetermineUpdateActionSO(problem, model, record, solutionSet, auxInfo);

	//Multi-objective
	return DetermineUpdateActionMO(problem, model, record, solutionSet, auxInfo, minValidationSupport, parameters);
}	//optional<OperationT> DetermineUpdateAction(const EvaluationRecordT& record, const vector<solution_type>& solutionSet, unsigned int minValidationSupport, unsigned int maxSolution)

/**
 * @brief Applies the update action to the solution set
 * @param[in,out] solutionSet Solution set
 * @param[in,out] auxInfo Auxiliary info for the solutions
 * @param[in, out] record Evaluation record
 * @param[in] problem RANSAC problem
 * @param[in] operation Update operations
 * @param[in] model Current hypothesis
 * @param[in] generator Generator for the current hypothesis
 * @param[in] observationSet Observation set
 * @param[in] flagLOSolution \c true if the model is generated by the LO solver
 * @param[in] cIteration Iteration count
 * @returns A tuple: [Key of the weakest model; smallest error]
 */
template<class ProblemT, class RNGT>
tuple<unsigned int, double> RANSACC<ProblemT, RNGT>::ApplyUpdate(map<unsigned int, solution_type>& solutionSet, map<unsigned int, AuxiliaryT>& auxInfo, EvaluationRecordT& record, const ProblemT& problem, const list<OperationT> & operation, const ModelT& model, const GeneratorT& generator, const IndexListT& observationSet, bool flagLOSolution, unsigned int cIteration)
{
	IndexListT generatorList(generator.begin(), generator.end());

	for(const auto& current : operation)
	{
		if(get<iAction>(current)==ActionT::ADD)
		{
			solutionSet.emplace(cIteration, solution_type(get<iError>(record), model, get<iInliers>(record), generatorList, flagLOSolution));
			auxInfo.emplace(cIteration, AuxiliaryT(get<iBitsetV>(record), get<iBitsetO>(record)));
		}	//if(get<iAction>(operation)==ActionT::ADD)

		if(get<iAction>(current)==ActionT::REPLACE)
		{
			solutionSet.erase(get<iTarget>(current));
			solutionSet.emplace(cIteration, solution_type(get<iError>(record), model, get<iInliers>(record), generatorList, flagLOSolution));

			auxInfo.erase(get<iTarget>(current));
			auxInfo.emplace(cIteration, AuxiliaryT(get<iBitsetV>(record), get<iBitsetO>(record)));
		}	//if(get<iAction>(operation)==ActionT::REPLACE)

		if(get<iAction>(current)==ActionT::REMOVE)
		{
			solutionSet.erase(get<iTarget>(current));
			auxInfo.erase(get<iTarget>(current));
		}	//if(get<iAction>(current)==ActionT::REMOVE)
	}	//for(const auto& current : operation)

	//Identify the weakest objective
	map<unsigned int, unsigned int> rankerW;
	for(const auto& current : auxInfo)
		rankerW.emplace(get<iInlierBitsetO>(current.second).count(), current.first);

	//Find the new smallest error
	set<double> rankerS;
	for(const auto& current : solutionSet)
		rankerS.insert(get<iModelError>(current.second));

	return make_tuple(rankerW.begin()->second, *rankerS.begin());
}	//void ApplyUpdate(vector<solution_type>& solutionSet, const list<OperationT>& operations, const vector<ModelT>& models, const vector<GeneratorT>& generators, const vector<EvaluationRecordT>& records)

/**
 * @brief Suppresses any redundant objectives
 * @param[in] problem RANSAC problem
 * @param[in] solutionSet Current solution set.
 * @param[in] auxInfo Auxiliary info for the solutions
 * @param[in] observationSet Observation set
 * @param[in] minValidationSupport Minimum support within the validation set for a new solution
 * @param[in] parameters RANSAC parameters
 * @return Solution set after mode suppression
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::SuppressObjectives(const ProblemT& problem, const map<unsigned int, solution_type>& solutionSet, const map<unsigned int, AuxiliaryT>& auxInfo, const IndexListT& observationSet, unsigned int minValidationSupport, const RANSACParametersC& parameters) -> map<unsigned int, solution_type>
{
	map<unsigned int, solution_type> suppressed;
	map<unsigned int, AuxiliaryT> suppressedAux;

	//Rank the solutions
	map<double, unsigned int> ranked;
	for(const auto& current : solutionSet)
		ranked.emplace(get<iModelError>(current.second), current.first);

	//Rebuild the solution set, starting from the best
	for(const auto& current : ranked)
	{
		size_t index=current.second;
		const solution_type& currentSolution=solutionSet.find(index)->second;
		const AuxiliaryT& currentAux=auxInfo.find(index)->second;

		EvaluationRecordT record(current.first, get<iInlierList>(currentSolution), problem.GetValidationSetSize(), get<iInlierBitsetV>(currentAux), get<iInlierBitsetO>(currentAux));

		optional<list<OperationT> > operation=DetermineUpdateAction(problem, get<iModel>(currentSolution), record, suppressed,  suppressedAux, minValidationSupport, parameters);

		if(operation)
		{
			GeneratorT generator(get<iGenerator>(currentSolution).cbegin(), get<iGenerator>(currentSolution).cend());
			ApplyUpdate(suppressed, suppressedAux, record, problem, *operation, get<iModel>(currentSolution), generator, observationSet, get<iFlagLO>(currentSolution), current.second);
		}
	}	//for(const auto& current : ranked)

	return suppressed;
}	//auto MergeObjectives(const ProblemT& problem, const vector<solution_type>& solutionSet, double minModelSimilarity, unsigned int minValidationSupport, unsigned int maxSolution) -> vector<solution_type>

/**
 * @brief Prunes the solution set
 * @param[in] solutionSet Solution set
 * @param[in] maxError An acceptable solution should have an error below this value
 * @param[in] minValidationSupport The minimum support an acceptable solution should achieve. The best solution is exempt from this test
 * @return The output of the RANSAC algorithm
 */
template<class ProblemT, class RNGT>
auto RANSACC<ProblemT, RNGT>::PruneSolutionSet(const map<unsigned int, solution_type>& solutionSet, double maxError, unsigned int minValidationSupport) -> result_type
{
	result_type result;
	for(const auto& current : solutionSet)
		result.emplace(get<iModelError>(current.second), current.second);

	//Apply the minimum score condition
	auto itCut=result.upper_bound(maxError);
	result.erase(itCut, result.end());

	//Apply the support condition
	if(result.size()>1)
	{
		auto itEnd=result.end();
		for(auto it=next(result.begin(),1); it!=itEnd; )
			if(get<iInlierList>(it->second).size() < minValidationSupport)
				it=result.erase(it);
			else
				advance(it,1);
	}	//if(result.size()>1)

	return result;
}	//result_type PruneSolutionSet(const vector<solution_type>& solutionSet, double minScore, unsigned int minValidationSupport)

/**
 * @brief Runs the algorithm
 * @param[out] output Output. Strongest distinct hypotheses encountered in the process
 * @param[in, out] problem RANSAC problem to be solved
 * @param[in, out] rng Random number generator
 * @param[in] parameters Algorithm parameters
 * @return RANSAC diagnostics object
 * @throws invalid_argument If the problem is invalid
 */
template<class ProblemT, class RNGT>
RANSACDiagnosticsC RANSACC<ProblemT, RNGT>::Run(result_type& output, ProblemT& problem, RNGT& rng, const RANSACParametersC& parameters)
{
	//Initialise

	ValidateParameters(parameters);	//Validate the input parameters

	if(!problem.IsValid())
		 throw(invalid_argument("RANSACC::Run : Invalid problem"));

	//Initialise the output
	output.clear();

	RANSACDiagnosticsC diagnostics;

	//Initialise the parameters

	int nThreads=min( omp_get_max_threads(), max(1, (int)parameters.nThreads) );	//No more threads than available resources

	//Parameters: Generator
	double generationCost= parameters.flagWaldSAC ? problem.Cost() : numeric_limits<double>::infinity();	// Model generation cost
	unsigned int sGenerator=problem.GeneratorSize();	//Cardinality of a generator sample

	//Parameters: Data
	size_t nValidation=problem.GetValidationSetSize();	//Cardinality of the validation set
	size_t nObservation=problem.GetObservationSetSize();	//Cardinality of the observation set

	IndexListT observationSet(nObservation);	// Indices to be sampled over. Proxy for the observation set
	iota(observationSet.begin(), observationSet.end(), (unsigned int)0);

	//Do we have enough data or validators?
	if(nObservation<sGenerator || nValidation==0)
		return diagnostics;

	//Parameters: PROSAC
	size_t growthRate=floor(max(1.0, parameters.growthRate * nObservation));	// Growth in the number of eligible observations per iteration (PROSAC)

	//Parameters: LO-RANSAC
	size_t minObservationSupport=max(problem.LOGeneratorSize()+1, (unsigned int)floor(max(1.0, parameters.minObservationSupport) * sGenerator));	//Minimum support in the observation set for launching a local optimisation process
	bool flagLOEnabled = (parameters.nLOIterations>0);// && (parameters.maxSolution==1);	//LO disabled for the MO case. LO prevents the formation of distinct objectives

	//Parameters: MO-RANSAC
	size_t minValidationSupport= floor(max(1.0, parameters.minCoverage * nValidation));	//Minimum support in the validation set for instantiating an alternative solution. The first solution is exempt from this constraint

	//Initialise the state

	//State: SPRT

	double delta=parameters.delta;	//Current estimate of p(eligible inlier|bad model)
	double epsilonOb=parameters.epsilon;	//Current estimate of p(eligible inlier|good model), over the observation set. Governs the termination. In MORANSAC, pertains to the worst solution in the set
	double epsilonVl=parameters.epsilon;	//Current estimate of p(eligible inlier|good model), over the validation set. Governs the SPRT. In MORANSAC, pertains to the worst solution in the set
	double deltaTest=delta;	// delta value for the current test
	vector<EtaTermT> etaUpdater;	//Helper structure for updating eta

	bool flagDesignNewTest=true;	// If \c true a new test is designed
	double decisionTh=numeric_limits<double>::infinity();	// SPRT decision threshold
	double lambdaI=0;	// Penalty term for an inlier (normalised by ln(decisionTh))
	double lambdaO=0;	// Penalty term for an outlier (normalised by ln(decisionTh))

	unsigned int cTestRunLength=0;	// Number of iterations performed under the current test
	list<TestRecordT> testRecord;	// A record of the past tests

	//State: PROSAC
	size_t nActive= (parameters.flagPROSAC ? problem.LOGeneratorSize() : nObservation);	// Only the first nActive observations are used for hypothesis generation

	//State: Hypothesis generation statistics
	double modelPerSample=1;	// For generators that can spawn multiple models, this implies a pessimistic decision threshold for the first couple of iterations
	unsigned int cTestedBad=0;	// Total number of tested points in rejected hypotheses
	unsigned int cInlierBad=0;	// Total number of accepted points in rejected hypotheses

	//State: Termination conditions

	unsigned int cIteration=0;	//Iteration counter
	unsigned int minIteration=parameters.minIteration;	//Minimum number of iterations
	unsigned int maxIteration=parameters.maxIteration;	//Maximum number of iterations

	//If topN is active, fixed number of iterations
	if(parameters.topN)
	{
		minIteration=ceil(log(1.0-parameters.minConfidence)/log(1.0-*parameters.topN));	//R5
		maxIteration=minIteration;
	}	//if(parameters.topN)

	double eta=0;	//ln of probability of not having encountered a good model
	double minEta=log(1-parameters.minConfidence);	//If eta is below this value, terminate

	double bestError=numeric_limits<double>::infinity();	//Best error so far

	//State: Solution set
	map<unsigned int, solution_type> solutionSet;	//Indexed by iteration count
	map<unsigned int, AuxiliaryT> auxInfo;	//Auxiliary info for the solutions. Indexed by iteration count
	double weakestInlierRatioVl=0;	//Validation inlier ratio for the weakest component. Not modified by eligibility ratio, or epsilonVl thresholds

	//Main loop
	bool flagBreak=false;	//If \c true the algorithm is terminated
	double localLambdaI;	//Thread-local copy of lambdaI
	double localLambdaO;	//Thread-local copy of lambdaO
	size_t localNActive;	//Thread-local copy of nActive
#pragma omp parallel if(nThreads>1) num_threads(nThreads) private(localLambdaI, localLambdaO, localNActive)
	do
	{
	map<unsigned int, solution_type> localSolutionSet;
	map<unsigned int, AuxiliaryT> localAuxInfo;

	#pragma omp critical(RANSACC_R1)	//Test state update. Implied flush at the end
	{
		//Design a new test
		if(flagDesignNewTest)
		{
			if(cTestRunLength>0)	//Save the test variables
				testRecord.emplace_back(cTestRunLength, decisionTh);

			tie(decisionTh, lambdaI, lambdaO)=UpdateTest(testRecord, delta, epsilonVl, generationCost/modelPerSample, sGenerator);	//Design a new test and update the asociated variables
			tie(eta, etaUpdater)=ComputeEta(problem, auxInfo, testRecord, parameters.eligibilityRatio, decisionTh);

			//Reset
			deltaTest=delta;
			flagDesignNewTest=false;
			cTestRunLength=0;
		}	//if(flagDesignNewTest)

		//PROSAC state update
		nActive=min(nObservation, nActive + growthRate);	//Expand the active observation set after every iteration
		localNActive=nActive;

		//SPRT threshold update
		localLambdaI=lambdaI;
		localLambdaO=lambdaO;

		localSolutionSet=solutionSet;
		localAuxInfo=auxInfo;
	}	//#pragma omp critical(RANSACC_R1)

		//Body: RunIteration works with its own copy of the necessary bits of the state. So, no need for firstprivate
		size_t currentTestId=testRecord.size();	//Identifies the current test

		//Perform a RANSAC iteration over the current observation set
		GeneratorT generator;	//Generator set
		ModelT model;	//Hypothesis spawned by the generator
		EvaluationRecordT evaluationRecord;	//Hypothesis evaluation record
		bool flagPass=false;	//True if the generator instantiated a hypothesis that passed SPRT
		unsigned int nModel=0;	//Number of models instantiated by the generator

		std::tie(generator, model, evaluationRecord, flagPass, nModel)=RunIteration(problem, rng, make_iterator_range(observationSet.begin(), next(observationSet.begin(), localNActive)), localLambdaI, localLambdaO, false);
		unsigned int nTestedValidation=get<iTestedCount>(evaluationRecord);

		//LO-RANSAC

		bool flagCanBeatBest=( get<iError>(evaluationRecord) <= parameters.maxErrorRatio*bestError);	//Can potentially beat the best score, and should be passed to LO

		//CheckLO checks whether the current model can beat solutions other than the best. Saves a copy of solutionSet
		bool flagApplyLO=flagLOEnabled && flagPass && (flagCanBeatBest || CheckLO(problem, model, evaluationRecord, localSolutionSet, localAuxInfo, minValidationSupport, parameters) );
		bool flagLOSolution=false;	// True if the LO solver beats the minimal solver

		if(flagApplyLO)
		{
			IndexListT inlierObservationSet=MakeInlierList(problem, model, observationSet);
			flagApplyLO = flagApplyLO && inlierObservationSet.size() >=minObservationSupport;

			if(flagApplyLO)	//If the observation set is large enough
			{
				tuple<GeneratorT, ModelT, EvaluationRecordT, bool, unsigned int> loOutput=PerformLO(problem, rng, inlierObservationSet, localLambdaI, localLambdaO, parameters.nLOIterations);

				//If there is a valid solution
				if(get<3>(loOutput))
				{
					unsigned int nTestedValidationLO;
					double errorLO=get<iError>(get<2>(loOutput));
					flagLOSolution= (errorLO < get<iError>(evaluationRecord));
					if(flagLOSolution)	//If it improves the current solution
						std::tie(generator, model, evaluationRecord, flagPass, nTestedValidationLO)=loOutput;

					nTestedValidation+= get<4>(loOutput);
				}	//if(loOutput)
			}	//if(flagApplyLO)

		}	//if(flagLO)

	#pragma omp critical(RANSACC_R1)	//Implied flush at the end
	{
		//Solution update
		optional<list<OperationT> > updateOp;

		if(flagPass)
		{

			//Using the updated, global solutionSet and auxInfo, as opposed to the local copies
			updateOp=DetermineUpdateAction(problem, model, evaluationRecord, solutionSet, auxInfo, minValidationSupport, parameters);

			if(updateOp)
			{
				//If there is an add or replace, we need a full evaluation of the observation set
				bool flagAddReplace=false;
				for(const auto& current : *updateOp)
					flagAddReplace|= (get<iAction>(current)==ActionT::ADD) || (get<iAction>(current)==ActionT::REPLACE);

				if(flagAddReplace)
				{
					get<iBitsetO>(evaluationRecord)=MakeBitset<BitsetT>(MakeInlierList(problem, model, observationSet), problem.GetObservationSetSize() );
					updateOp=DetermineUpdateAction(problem, model, evaluationRecord, solutionSet, auxInfo, floor(max(1.0, parameters.minCoverage * nObservation)), parameters);
				}
			}
		}

		double newBestError=bestError;
		double newEpsilonVl=epsilonVl;
		double newEpsilonOb=epsilonOb;
		if(updateOp)
		{
			unsigned int indexWeakest;
			tie(indexWeakest, newBestError)=ApplyUpdate(solutionSet, auxInfo, evaluationRecord, problem, *updateOp, model, generator, observationSet, flagLOSolution, cIteration);

			const AuxiliaryT& currentAux=auxInfo.find(indexWeakest)->second;
			newEpsilonVl=parameters.eligibilityRatio*get<iInlierBitsetV>(currentAux).count()/nValidation;
			newEpsilonOb=parameters.eligibilityRatio*get<iInlierBitsetO>(currentAux).count()/nObservation;
			weakestInlierRatioVl=(double)get<iInlierBitsetV>(currentAux).count()/nValidation;

		}	//if(updateOp)

		//RANSAC engine state update

		//SPRT state update
		if(!flagPass && nModel>0)
		{
			cTestedBad+=get<iTestedCount>(evaluationRecord);
			cInlierBad+=get<iInliers>(evaluationRecord).size();
			delta=(parameters.eligibilityRatio * cInlierBad)/cTestedBad;
		}	//if(get<iInliers>(current).empty())
		delta=max(0.01, min(delta, 0.95*newEpsilonVl));	//If delta>epsilon or delta==0, the algorithm breaks

		//Conditions for a new test
		//1. Another thread raised a flag OR
		//2. Epsilon or delta are different
		//Even if the test is updated in the meantime (i.e., testRecord.size()!=currentTestId), this condition can raise a request for a new test, if it identified a better solution
		flagDesignNewTest= flagDesignNewTest || updateOp || ( (epsilonVl!=newEpsilonVl) || (epsilonOb!=newEpsilonOb) || (fabs(delta-deltaTest)/deltaTest > parameters.maxDeltaTolerance) );

		epsilonVl=max(0.01, min(0.99, newEpsilonVl));	//If epsilon=1 the algorithm breaks
		epsilonOb=max(0.01, min(0.99, newEpsilonOb));	//If epsilon=1 the algorithm breaks

		if(testRecord.size()==currentTestId)
			++cTestRunLength;
		else	//Test is updated by another thread. Modify the correct record
			++get<iRunLength>(*next(testRecord.begin(), currentTestId));

		//Generator statistics update
		if(nModel!=0)
			modelPerSample= fma(modelPerSample, cIteration, nModel)/(cIteration+1);	//Running mean

		//Update termination conditions
		eta=0;
		for(auto& current : etaUpdater)
		{
			get<iEtaTerm>(current)*=get<iAttenuation>(current);
			eta+=get<iEtaTerm>(current);
		}
		eta=log(eta);

		bestError=newBestError;

		//Diagnostics update

		diagnostics.nValidatorCalls+=nTestedValidation;

		if(flagApplyLO)
		{
			diagnostics.nLOCalls++;
			diagnostics.nValidatorCalls+=nObservation;
		}

		if(!get<iBitsetO>(evaluationRecord).empty())
			diagnostics.nValidatorCalls+=nObservation;

		if(parameters.flagHistory)
		{
			typename RANSACDiagnosticsC::IterationT historyItem;
			get<RANSACDiagnosticsC::iIteration>(historyItem)=cIteration;	//Iteration was updated!
			get<RANSACDiagnosticsC::iEta>(historyItem)=eta;
			get<RANSACDiagnosticsC::iGenerator>(historyItem)=IndexListT(generator.begin(), generator.end());

			if(flagPass)
				get<RANSACDiagnosticsC::iError>(historyItem)=get<iError>(evaluationRecord);

			diagnostics.history.push_back(historyItem);
		}

		//Check termination
		++cIteration;
		flagBreak = flagBreak || (cIteration>=maxIteration) || ( cIteration>minIteration && ( (!solutionSet.empty() && bestError<parameters.minError) || eta<minEta) );
	}	//#pragma omp critical(RANSACC_R2)

	}while(!flagBreak);

	//Finalise
	if(cTestRunLength>0)	//Save the test variables for the last test
		testRecord.emplace_back(cTestRunLength, decisionTh);

	map<unsigned int, solution_type> suppressed=SuppressObjectives(problem, solutionSet, auxInfo, observationSet, minValidationSupport, parameters);	//Suppress the redundant objectives
	output=PruneSolutionSet(suppressed, parameters.maxError, minValidationSupport);

	diagnostics.flagSuccess=!output.empty();
	diagnostics.inlierRatio=weakestInlierRatioVl;
	diagnostics.finalEpsilonVl=epsilonVl;
	diagnostics.finalEpsilonOb=epsilonOb;
	diagnostics.finalDelta=delta;
	diagnostics.finalDecisionTh=decisionTh;
	diagnostics.error=bestError;
	diagnostics.nIterations=cIteration;

	return diagnostics;
}	//RANSACDiagnosticsC Run(ModelT& model, IndexListT& inliers, ProblemT& problem, const RANSACParametersC& parameters)

}	//RANSACN
}	//SeeSawN

#endif /* RANSAC_IPP_7097012 */
