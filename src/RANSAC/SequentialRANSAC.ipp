/**
 * @file SequentialRANSAC.ipp Implementation of the sequential RANSAC algorithm
 * @author Evren Imre
 * @date 15 May 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SEQUENTIAL_RANSAC_IPP_7012821
#define SEQUENTIAL_RANSAC_IPP_7012821

#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/counting_range.hpp>
#include <string>
#include <limits>
#include <cstddef>
#include <stdexcept>
#include <cmath>
#include <tuple>
#include "RANSAC.h"
#include "RANSACProblemConcept.h"

namespace SeeSawN
{
namespace RANSACN
{

using boost::lexical_cast;
using boost::dynamic_bitset;
using boost::for_each;
using boost::iota;
using boost::transform;
using boost::counting_range;
using std::numeric_limits;
using std::size_t;
using std::string;
using std::invalid_argument;
using std::max;
using std::min;
using std::tuple;
using std::make_tuple;
using std::tie;
using SeeSawN::RANSACN::RANSACProblemConceptC;
using SeeSawN::RANSACN::RANSACC;
using SeeSawN::RANSACN::RANSACDiagnosticsC;
using SeeSawN::RANSACN::RANSACParametersC;

/**
 * @brief Parameter object for SequentialRANSACC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct SequentialRANSACParametersC
{
	unsigned int maxIteration;	///< Maximum number of RANSAC passes
	double minInlierRatio;	///< If the inlier ratio (wrt original observation set) for the weakest model in a RANSAC pass falls below this value, the algorithm terminates. >0

	unsigned int nThreads;	///< Number of threads available to the algorithm. >=1

	RANSACParametersC ransacParameters;	///< RANSAC parameters

	SequentialRANSACParametersC() : maxIteration(numeric_limits<unsigned int>::max()), minInlierRatio(0.1), nThreads(1)
	{}
};	//SequentialRANSACParametersC

/**
 * @brief Diagnostics object for SequentialRANSACC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct SequentialRANSACDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully
	list<RANSACDiagnosticsC> ransacDiagnostics;	///< Diagnostics for each call to RANSAC

	SequentialRANSACDiagnosticsC() : flagSuccess(false)
	{}
};	//SequentialRANSACDiagnosticsC

/**
 * @brief Sequential RANSAC
 * @tparam ProblemT RANSAC problem type
 * @tparam RNGT Random number generator type
 * @pre \c ProblemT is a model of RANSACGeometryEstimationProblemConceptC
 * @pre \c ProblemT::data_container_type supports an insert function with the interface \c insert(iterator,value_type), which automatically resizes the container if necessary (unenforced)
 * @remarks Usage notes
 * 	- If the algorithm successfully identifies all models supported by the observation set, the last pass will be performed on a set of outliers. Therefore, the individual RANSAC passes should terminate after a reasonable number of iterations.
 * 	- The algorithm terminates successfully as long as at least one RANSAC call terminates successfully.
 * 	- Validation set update rules:
 * 		- Validator inliers to the current solution set are removed
 * 		- Removed elements are substituted by regularly sampling the observation set. If the original validation set has common elements with the observation set, this may lead to multiple instances of the same element
 * 		- If there size of the validation set is capped at that of the observation set
 * 	- The output structure of ordinary RANSAC reports the inliers as the indices into the validation set. Since the validation set changes at each iteration, this is no longer applicable. So, no inliers are reported.
 * 	- The generator indices are with respect to the original set
 * @remarks The unit test only covers the compilation, and does not verify the operation
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT, class RNGT=mt19937_64>
class SequentialRANSACC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<ProblemT>));
	//@endcond

	private:

		typedef RANSACC<ProblemT, RNGT> RANSACT;	///< Type of the RANSAC engine
		typedef typename RANSACT::result_type ResultT;	///< Type of the result

		typedef typename ProblemT::data_container_type DataContainerT;		///< Data container for the RANSAC problem

		typedef dynamic_bitset<> IndicatorArrayT;	///< An indicator array for inliers

		/**@name Implementation details */ //@{
		static void ValidateParameters(const ProblemT& problem, SequentialRANSACParametersC& parameters);	///< Validates the parameters
		static tuple<IndicatorArrayT, IndicatorArrayT, ResultT> ProcessResult(const ResultT& models, const vector<size_t>& indexMap, const ProblemT& problem, size_t minSupport);	///< Processes the output of a RANSAC pass
		static vector<size_t> UpdateData(ProblemT& problem, const vector<size_t>& indexMap, const IndicatorArrayT& observationInliers, const IndicatorArrayT& validationInliers);	///< Updates the observation and the validation sets
		//@}

	public:

		/** @name Solution */ //@{
		typedef typename RANSACT::solution_type solution_type;	///< A RANSAC solution tuple
		static constexpr unsigned int iModelError=RANSACT::iModelError;
		static constexpr unsigned int iModel=RANSACT::iModel;
		static constexpr unsigned int iInlierList=RANSACT::iInlierList;
		static constexpr unsigned int iGenerator=RANSACT::iGenerator;
		static constexpr unsigned int iFlagLO=RANSACT::iFlagLO;
		typedef ResultT result_type;	///< Type of the result, a multimap
		//@}

		typedef RNGT rng_type;	///< Random number generator type
		static SequentialRANSACDiagnosticsC Run(result_type& result, ProblemT& problem, rng_type& rng, SequentialRANSACParametersC parameters);	///< Runs the algorithm
};	//SequentialRANSACC

/**
 * @brief Validates the parameters
 * @param[in] problem
 * @param[in] parameters Parameters
 * @throws invalid_argument If there is a parameter with an invalid value
 */
template<class ProblemT, class RNGT>
void SequentialRANSACC<ProblemT, RNGT>::ValidateParameters(const ProblemT& problem, SequentialRANSACParametersC& parameters)
{
	if(!problem.IsValid())
		throw(invalid_argument("SequentialRANSACC::ValidateParameters : Invalid problem"));

	if(parameters.nThreads==0)
		throw(invalid_argument(string("SequentialRANSACC::ValidateParameters : parameters.nThreads must be >0 . Value: ")+lexical_cast<string>(parameters.nThreads) ) );

	if(parameters.minInlierRatio<=0)
		throw(invalid_argument(string("SequentialRANSACC::ValidateParameters : parameters.minInlierRatio must be >0 . Value: ")+lexical_cast<string>(parameters.minInlierRatio) ) );

	parameters.ransacParameters.nThreads=parameters.nThreads;
}	//void ValidateParameters(SequentialRANSACParametersC& parameters)

/**
 * @brief Processes the output of a RANSAC pass
 * @param[in] models Models discovered by the current pass.
 * @param[in] indexMap Indices of the observations with respect to the original observation set
 * @param[in] problem RANSAC problem
 * @param[in] minSupport Minimum support for a valid model
 * @return A tuple: A pair of indicator arrays, for the observations and for the validators. Filtered results
 * @pre \c indexMap has the same number of elements as the current observation set
 */
template<class ProblemT, class RNGT>
auto SequentialRANSACC<ProblemT, RNGT>::ProcessResult(const ResultT& models, const vector<size_t>& indexMap, const ProblemT& problem, size_t minSupport) -> tuple<IndicatorArrayT, IndicatorArrayT, ResultT>
{
	//Preconditions
	assert(problem.GetObservationSetSize()==indexMap.size() );

	size_t nObservations=problem.GetObservationSetSize();
	IndicatorArrayT observationFlags(nObservations);

	size_t nValidators=problem.GetValidationSetSize();
	IndicatorArrayT validationFlags(problem.GetValidationSetSize());

	ResultT filtered;	//Results surviving the support threshold

	//Evaluate each model on the observation set
	for(const auto& current : models)
	{

		//Observation inliers
		IndicatorArrayT currentObservationFlags(nObservations);
		for(size_t c=0; c<nObservations; ++c)
			currentObservationFlags[c]=problem.EvaluateObservation(get<iModel>(current.second), c);

		//Support test
		if(currentObservationFlags.count()<minSupport)
			continue;

		observationFlags|=currentObservationFlags;

		//Validation inliers
		IndicatorArrayT currentValidationFlags(nValidators);
		for_each(get<iInlierList>(current.second), [&](unsigned int c){validationFlags[c]=true;});

		filtered.insert(current);
	}	//for(auto& current : models)

	//Index adjustments
	for(auto& current : filtered)
	{
		get<iInlierList>(current.second).clear(); //Clear the validator indices, as they are invalidated by the top-up step
		size_t sGenerator=get<iGenerator>(current.second).size();

		//Map the generator indices to the original observation sett
		for(size_t c=0; c<sGenerator; ++c)
			get<iGenerator>(current.second)[c]=indexMap[get<iGenerator>(current.second)[c] ];
	}	//for(auto& current : filtered)

	return make_tuple(observationFlags, validationFlags, filtered);
}	//set<unsigned int> ProcessResult(ResultT& models, list<IndexPairT>& indexMap, const SequentialRANSACParametersC& parameters)

/**
 * @brief Updates the observation and the validation sets
 * @param[in, out] problem RANSAC problem. At the output, the observation and the validation sets are modified
 * @param[in] indexMap Indices of the observations within the original set
 * @param[in] observationInliers Inlier flags for the observation set
 * @param[in] validationInliers Inlier flags for the validation set
 * @return Updated index map
*/
template<class ProblemT, class RNGT>
vector<size_t> SequentialRANSACC<ProblemT, RNGT>::UpdateData(ProblemT& problem,  const vector<size_t>& indexMap, const IndicatorArrayT& observationInliers, const IndicatorArrayT& validationInliers)
{
	vector<size_t> newIndexMap; newIndexMap.reserve(indexMap.size());

	//Observation set
	DataContainerT observationOutliers;
	size_t c=0;
	for(const auto& current : problem.GetObservations())
	{
		if(!observationInliers[c])
		{
			observationOutliers.insert(observationOutliers.end(), current);
			newIndexMap.push_back(indexMap[c]);
		}

		++c;
	}	//for(const auto& current : problem.GetObservations())

	DataContainerT validationOutliers;
	size_t c2=0;
	for(const auto& current : problem.GetValidators())
	{
		if(!validationInliers[c2])
			validationOutliers.insert(validationOutliers.end(), current);

		++c2;
	}	//for(const auto& current : problem.GetObservations())

	//Top up the validation set
	size_t nObservations=observationInliers.size()-observationInliers.count();	// # outliers, the new observation set
	size_t nAdd=min(validationInliers.count(), nObservations);	// # new validators

	double step=double(nObservations-1)/nAdd;
	size_t c3=0;
	double index=0;
	for(const auto& current : observationOutliers)
	{
		if(c3==floor(index))
		{
			validationOutliers.insert(validationOutliers.end(), current);
			index+=step;
		}

		++c3;
	}	//	for(const auto& current : observationOutliers)

	//Update
	problem.SetObservations(observationOutliers);
	problem.SetValidators(validationOutliers);
	newIndexMap.shrink_to_fit();

	return newIndexMap;
}	//void UpdateData(ProblemT& problem, const set<size_t>& observationInliers, const set<size_t>& validationInliers)

/**
 * @brief Runs the algorithm
 * @param[out] result Result
 * @param[in,out] problem RANSAC problem. At the output, the problem has the original observation and the validation sets
 * @param[in,out] rng Random number generator
 * @param[in] parameters Parameters. Passed by value on purpose
 * @return Diagnostics object
 */
template<class ProblemT, class RNGT>
SequentialRANSACDiagnosticsC SequentialRANSACC<ProblemT, RNGT>::Run(result_type& result, ProblemT& problem, rng_type& rng, SequentialRANSACParametersC parameters)
{
	//Validate
	ValidateParameters(problem, parameters);

	//Save the original data
	DataContainerT originalObservations=problem.GetObservations();
	DataContainerT originalValidators=problem.GetValidators();

	SequentialRANSACDiagnosticsC diagnostics;
	unsigned int nIteration=0;
	bool flagTerminate=false;

	size_t nObservations=problem.GetObservationSetSize();
	unsigned int minSupport=max(1.0, nObservations*parameters.minInlierRatio);

	vector<size_t> indexMap(nObservations);
	iota(indexMap, 0);

	while(!flagTerminate)
	{
		//In the single-model case, it is possible to get an upper bound for the max iterations
		if(parameters.ransacParameters.maxSolution==1)
		{
			double inlierRatioBound=(double)minSupport/problem.GetObservationSetSize();	//For a valid solution, the inlier ratio cannot be below this value
			parameters.ransacParameters.maxIteration=max(1.0,log(1-parameters.ransacParameters.minConfidence)/log(1-pow(parameters.ransacParameters.eligibilityRatio*inlierRatioBound, problem.GeneratorSize()) ));
		}	//if(parameters.ransacParameters.maxSolution==1)

		ResultT currentModels;
		RANSACDiagnosticsC currentDiagnostics=RANSACT::Run(currentModels, problem, rng, parameters.ransacParameters);
		diagnostics.ransacDiagnostics.push_back(currentDiagnostics);
		++nIteration;

		//If RANSAC fails, terminate here
		if(!currentDiagnostics.flagSuccess)
			break;

		IndicatorArrayT inliersO;	//Observation inliers
		IndicatorArrayT inliersV;	//Validation inliers
		ResultT filtered;	//Surviving models
		std::tie(inliersO, inliersV, filtered)=ProcessResult(currentModels, indexMap, problem, minSupport);		//Eliminate the weak models
		result.insert(filtered.begin(), filtered.end());	//Append the survivors to the output

		//Termination conditions
		flagTerminate = (nIteration>=parameters.maxIteration);	//Max iteration
		flagTerminate = flagTerminate || (currentModels.size()!=filtered.size());	 //Any weak model triggers termination

		//Update the observation and the validation sets
		if(!flagTerminate)
			indexMap=UpdateData(problem, indexMap, inliersO, inliersV);

		//Do we have enough observations for the next pass
		size_t nRemaining=problem.GetObservationSetSize();
		flagTerminate = flagTerminate || (nRemaining<minSupport);
	}	//while(!flagTerminate)

	//Restore the original observations
	problem.SetObservations(originalObservations);
	problem.SetValidators(originalValidators);

	diagnostics.flagSuccess=!result.empty();
	return diagnostics;
}	//SequentialRANSACDiagnosticsC Run(result_type& result, ProblemT& problem, rng_type& rng, SequentialRANSACParametersC parameters)
}	//RANSACN
}	//SeeSawN

#endif /* SEQUENTIAL_RANSAC_IPP_7012821 */
