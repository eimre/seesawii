/**
 * @file TwoStageRANSAC.ipp Implementation of TwoStageRANSACC
 * @author Evren Imre
 * @date 27 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef TWO_STAGE_RANSAC_IPP_7021404
#define TWO_STAGE_RANSAC_IPP_7021404

#include <boost/concept_check.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/optional.hpp>
#include <tuple>
#include <cmath>
#include <climits>
#include <type_traits>
#include <random>
#include <iterator>
#include <vector>
#include <cstddef>
#include <stdexcept>
#include "RANSAC.h"
#include "RANSACProblemConcept.h"

namespace SeeSawN
{
namespace RANSACN
{

using boost::optional;
using std::tuple;
using std::tie;
using std::get;
using std::sqrt;
using std::pow;
using std::numeric_limits;
using std::is_same;
using std::prev;
using std::mt19937_64;
using std::vector;
using std::size_t;
using std::logic_error;
using std::distance;
using SeeSawN::RANSACN::RANSACProblemConceptC;
using SeeSawN::RANSACN::RANSACC;
using SeeSawN::RANSACN::RANSACDiagnosticsC;
using SeeSawN::RANSACN::RANSACParametersC;

/**
 * @brief Parameters for TwoStageRANSACC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct TwoStageRANSACParametersC
{
	RANSACParametersC parameters1;	///< Parameters for the first stage
	RANSACParametersC parameters2;	///< Parameters for the second stage

	//Top-N criterion
	unsigned int nThreads;	///< Number of threads
	double minConfidence;	///< Overall confidence
	double eligibilityRatio;	///< The ratio of eligible observations (e.g., satisfying a noise constraint) for stage-2 RANSAC. (0,1]

	TwoStageRANSACParametersC() : nThreads(1), minConfidence(0.99), eligibilityRatio(0.72)
	{}
};	//struct TwoStageRANSACParametersC

/**
 * @brief Diagnostics for TwoStageRANSACC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct TwoStageRANSACDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully
	RANSACDiagnosticsC diagnostics1;	///< Diagnostics for the first stage
	RANSACDiagnosticsC diagnostics2;	///< Diagnostics for the second stage

	optional<unsigned int> iStage1Solution;	///< Index of the stage-1 solution in the result. Invalid if discarded
	TwoStageRANSACDiagnosticsC() : flagSuccess(false)
	{}
};	//struct TwoStageRANSACDiagnostics

/**
 * @brief Two-stage RANSAC algorithm
 * @tparam Problem1T RANSAC problem for the first stage
 * @tparam Problem2T RANSAC problem for the second stage
 * @tparam RNGT A pseudo-random number generation engine
 * @pre \c Problem1T is a model of RANSACProblemConceptC
 * @pre \c Problem2T is a model of RANSACProblemConceptC
 * @remarks R1: "Order Statistics of RANSAC and Their Practical Application, " E. Imre, A. Hilton, 2013, under review for IJCV
 * @remarks If \c Problem1T and \c Problem2T are of different types, the result of the first stage is ignored
 * @remarks Algorithm:
 * 	- Apply the first problem to remove the outliers. This could be by employing a permissive threshold, or estimating a weaker, but cheaper geometric constraint
 * 	- Apply the second problem on the inliers of the first problem. eligibilityRatio is the ratio of acceptable observations for the second stage. It could be, for example, the percentage of observations that are only mildly corrupted by noise.
 * @remarks Usage notes:
 * 	- Generator indices in the output refer to the observation set of the stage-1 RANSAC problem
 * 	- Validation set in the stage-2 RANSAC problem is overwritten by that of the stage-1
 * @remarks Overloading hiding via enable_if is not compatible with explicit instantiations. So, part of the class is implemented as free functions
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class Problem1T, class Problem2T=Problem1T, class RNGT=mt19937_64>
class TwoStageRANSACC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<Problem1T>));
	BOOST_CONCEPT_ASSERT((RANSACProblemConceptC<Problem2T>));
	//@endcond

	private:

		typedef RANSACC<Problem1T, RNGT> RANSAC1T;	///< Stage-1 RANSAC
		typedef RANSACC<Problem2T, RNGT> RANSAC2T;	///< Stage-2 RANSAC

		typedef typename RANSAC1T::result_type Result1T;	///< Type of the result for stage-1 RANSAC
		typedef typename RANSAC2T::result_type Result2T;	///< Type of the result for stage-2 RANSAC

		/** @name Implementation details */ //@{
		static void AdjustGeneratorIndices(Result2T& resultS2, const Problem1T& problemS1, const Problem2T& problemS2);	///< Adjusts the generator indices of the stage-2 results, so that they refer to the original observation set (of stage-1)
 		//@}

	public:

		/** @name Solution */ //@{
		typedef typename RANSAC2T::solution_type solution_type;	///< A RANSAC solution tuple
		static constexpr unsigned int iModelError=RANSAC2T::iModelError;
		static constexpr unsigned int iModel=RANSAC2T::iModel;
		static constexpr unsigned int iInlierList=RANSAC2T::iInlierList;
		static constexpr unsigned int iGenerator=RANSAC2T::iGenerator;
		static constexpr unsigned int iFlagLO=RANSAC2T::iFlagLO;
		typedef typename RANSAC2T::result_type result_type;	///< Type of the result, a multimap
		//@}

		typedef RNGT rng_type;	///< Random number generator type
		static TwoStageRANSACDiagnosticsC Run(result_type& result, Problem1T& problem1, Problem2T& problem2, rng_type& rng, TwoStageRANSACParametersC parameters);	///< Runs the algorithm
};	//class TwoStageRANSACC

/********** FREE FUNCTIONS **********/
namespace DetailN
{
	using boost::optional;
	using std::enable_if;
	using std::is_same;
	using std::vector;
	using std::size_t;
	using std::get;
	using std::tie;

	//In order to make use of overload hiding, these functions are implemented outside of TwoStageRANSACC
	template<class RANSAC1T, class RANSAC2T> void EvaluateStage1Model(typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type & problemS2);	///< Evaluates the model from the first stage against the constraint of the second stage
	template<class RANSAC1T, class RANSAC2T, typename enable_if<is_same<typename RANSAC1T::problem_type, typename RANSAC2T::problem_type>::value, int>::type=0 > optional<unsigned int> InsertStage1Result(typename RANSAC2T::result_type& resultS2, typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type& problem2);	///< Attempts to replace the worst result in stage-2 by the stage-1 result
	template<class RANSAC1T, class RANSAC2T, typename enable_if<!is_same<typename RANSAC1T::problem_type, typename RANSAC2T::problem_type>::value, int>::type=0 > optional<unsigned int> InsertStage1Result(typename RANSAC2T::result_type& resultS2, typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type& problem2);	///< Dummy function for overload hiding
}	//namespace DetailN

/********** IMPLEMENTATION STARTS HERE **********/

/********** TwoStageRANSACC **********/

/**
 * @brief Adjusts the generator indices of the stage-2 results, so that they refer to the original observation set (of stage-1)
 * @param[in,out] resultS2 Result of the second stage
 * @param[in] problemS1 Problem for the first stage
 * @param[in] problemS2 Problem for the second stage
 */
template<class Problem1T, class Problem2T, class RNGT>
void TwoStageRANSACC<Problem1T, Problem2T, RNGT>::AdjustGeneratorIndices(Result2T& resultS2, const Problem1T& problemS1, const Problem2T& problemS2)
{
	//Iterate over the solutions, and adjust the indices
	for(auto& current : resultS2)
		for(auto& currentIndex : get<iGenerator>(current.second))
			currentIndex=*problemS1.GetObservationIndex(*problemS2.GetObservation(currentIndex));
}	//void AdjustGeneratorIndices(Result2T& resultS2, const Problem1T& problemS1, const Problem2T& problemS2)

/**
 * @brief Runs the algorithm
 * @param[out] result Result
 * @param[in,out] problem1 First stage RANSAC problem
 * @param[in,out] problem2 Second stage RANSAC problem
 * @param[in,out] rng Random number generator
 * @param[in] parameters Parameters. Pass-by-value, as they are modified inside the function
 * @return Diagnostics object
 * @remarks RANSAC parameter modifications:
 * 	- Stage 1 overrides \c maxSolution , \c minConfidence and \c nLOIterations
 * 	- Stage 2 overrides \c minConfidence and \c topN
 * @remarks Observation and validation sets of \c problem2 are overridden
 */
template<class Problem1T, class Problem2T, class RNGT>
TwoStageRANSACDiagnosticsC TwoStageRANSACC<Problem1T, Problem2T, RNGT>::Run(result_type& result, Problem1T& problem1, Problem2T& problem2, rng_type& rng, TwoStageRANSACParametersC parameters)
{
	TwoStageRANSACDiagnosticsC diagnostics;

	//First stage
	parameters.parameters1.nThreads=parameters.nThreads;
	parameters.parameters1.maxSolution=1;
	parameters.parameters1.minConfidence=sqrt(parameters.minConfidence);

	Result1T result1;
	diagnostics.diagnostics1=RANSAC1T::Run(result1, problem1, rng, parameters.parameters1);

	if(!diagnostics.diagnostics1.flagSuccess)
		return diagnostics;

	//Second stage
	parameters.parameters2.nThreads=parameters.nThreads;

	//Move the inliers and the validation set to the second problem
	typename Problem1T::data_container_type inliers=problem1.GetInlierObservations( get<RANSAC1T::iModel>(result1.begin()->second) );
	problem2.SetObservations(inliers);
	problem2.SetValidators(problem1.GetValidators());

	//Number of iterations for the second stage
	parameters.parameters2.minConfidence=parameters.parameters1.minConfidence;
	parameters.parameters2.topN=pow(parameters.eligibilityRatio, problem2.GeneratorSize());

	//Second stage
	diagnostics.diagnostics2=RANSAC2T::Run(result, problem2, rng, parameters.parameters2);
	AdjustGeneratorIndices(result, problem1, problem2);

	//Insert the result of the first stage, if the results are compatible
	if( is_same<Problem1T, Problem2T>::value )
		diagnostics.iStage1Solution=DetailN::InsertStage1Result<RANSAC1T, RANSAC2T>(result, result1.begin()->second, problem2);	//If the results are not compatible, the chosen overload throws

	diagnostics.flagSuccess=!result.empty();

	return diagnostics;
}	//tuple<RANSACDiagnosticsC, RANSACDiagnosticsC> Run(result_type& result, Problem1T& problem1, Problem2T& problem2, RNGT& rng, const TwoStageRANSACParametersC& parameters)

/**********  FREE FUNCTIONS **********/
namespace DetailN
{

/**
 * @brief Evaluates the model from the first stage against the constraint of the second stage
 * @tparam RANSAC1T Type of the RANSAC engine for the first stage
 * @tparam RANSAC2T Type of the RANSAC engine for the second stage
 * @param[in] solutionS1 Solution of the first stage
 * @param[in] problemS2 Problem for the second stage
 * @pre \c RANSAC1T and \c RANSAC2T are of type \c RANSACC (unenforced)
 * @remarks No dedicated tests. Tested via \c TwoStageRANSACC
 * @ingroup Algorithm
 * @internal
 */
template<class RANSAC1T, class RANSAC2T>
void EvaluateStage1Model(typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type & problemS2)
{
	//Re-evaluate the output of the first stage with the constraint of the second
	const typename RANSAC1T::problem_type::model_type& modelS1= get<RANSAC1T::iModel>(solutionS1);
	double error=0;
	size_t nValidator=problemS2.GetValidationSetSize();
	vector<size_t> inlierList; inlierList.reserve(nValidator);
	for(size_t c=0; c<nValidator; ++c)
	{
		double currentError;
		bool flagInlier;
		tie(flagInlier, currentError)=problemS2.EvaluateValidator(modelS1, c);
		error+=currentError;

		if(flagInlier)
			inlierList.push_back(c);
	}	//for(size_t c=0; c<nValidator; ++c)

	inlierList.shrink_to_fit();

	//Update the result
	get<RANSAC1T::iInlierList>(solutionS1).swap(inlierList);
	get<RANSAC1T::iModelError>(solutionS1)=error;
}	//void EvaluateStage1Model(Result1T& resultS1, const ProblemBT& problemS2, enable_if<is_same<ProblemAT, ProblemBT>::value>::type* dummy=nullptr)

/**
 * @brief Attempts to replace the worst result in stage-2 by the stage-1 result
 * @tparam RANSAC1T Type of the RANSAC engine for the first stage
 * @tparam RANSAC2T Type of the RANSAC engine for the second stage
 * @param[in,out] resultS2 Stage-2 result
 * @param[in, out] solutionS1 Stage-1 solution
 * @param[in] problem2 Stage-2 problem
 * @return Index of the stage-1 result in \c resultS2 . Invalid if the stage-1 model is worse than all solutions in \c resultS2
 * @pre \c RANSAC1T and \c RANSAC2T are of type \c RANSACC (unenforced)
 * @remarks No dedicated tests. Tested via \c TwoStageRANSACC
 * @internal
 */
template<class RANSAC1T, class RANSAC2T, typename enable_if<is_same<typename RANSAC1T::problem_type, typename RANSAC2T::problem_type>::value, int>::type=0 >
optional<unsigned int> InsertStage1Result(typename RANSAC2T::result_type& resultS2, typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type& problem2)
{
	EvaluateStage1Model<RANSAC1T, RANSAC2T>(solutionS1, problem2);	//Evaluate the stage-1 model again, against the constraint of the stage-2 problem
	auto itS1=resultS2.emplace( get<RANSAC1T::iModelError>(solutionS1), solutionS1);
	unsigned int index=distance(resultS2.begin(), itS1);

	unsigned int nSolution=resultS2.size();
	if(nSolution>1)
	{
		resultS2.erase(prev(resultS2.end(),1));	//If solutionS1 is the worst, it is deleted
		--nSolution;
	}	//if(nSolution>1)

	return index!=nSolution ? index : optional<unsigned int>();	//If index is still within the index range of resultS2, it is not deleted*/
	return optional<unsigned int>();
}	//optional<unsigned int> AddStage1Result(Result2T& resultS2, const Result1T& resultS1, const Problem2T& problem2)

/**
 * @brief Dummy function for overload hiding
 * @tparam RANSAC1T Type of the RANSAC engine for the first stage
 * @tparam RANSAC2T Type of the RANSAC engine for the second stage
 * @param[in,out] resultS2 Stage-2 result
 * @param[in, out] solutionS1 Stage-1 solution
 * @param[in] problem2 Stage-2 problem
 * @return Invalid
 * @throws logic_error Always throws
 * @internal
 */
template<class RANSAC1T, class RANSAC2T, typename enable_if<!is_same<typename RANSAC1T::problem_type, typename RANSAC2T::problem_type>::value, int>::type=0 >
optional<unsigned int> InsertStage1Result(typename RANSAC2T::result_type& resultS2, typename RANSAC1T::solution_type& solutionS1, const typename RANSAC2T::problem_type& problem2)
{
	throw(logic_error("InsertStage1Result: This function should never be called in normal operation"));
	return optional<unsigned int>();
}	//optional<unsigned int> InsertStage1Result(Result2T& resultS2, Solution1T& solutionS1, const Problem2T& problem2, typename enable_if<!is_same<Problem1T, Problem2T>::value>::type* dummy)

}

}	//RANSACN
}	//SeeSawN

#endif /* TWOSTAGERANSAC_IPP_ */
