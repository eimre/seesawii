/**
 * @file RANSACProblemConcept.ipp Implementation of RANSACProblemConceptC
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RANSAC_PROBLEM_CONCEPT_IPP_6212356
#define RANSAC_PROBLEM_CONCEPT_IPP_6212356

#include <boost/concept_check.hpp>
#include <list>
#include <set>
#include <cstddef>
#include <tuple>
#include <type_traits>

namespace SeeSawN
{
namespace RANSACN
{

using boost::optional;
using boost::DefaultConstructibleConcept;
using boost::CopyConstructible;
using boost::Assignable;
using std::list;
using std::set;
using std::size_t;
using std::tie;
using std::is_same;

/**
 * @brief RANSAC problem concept checker
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class RANSACProblemConceptC
{
	//@cond CONCEPT_CHECK

	BOOST_CONCEPT_ASSERT((Assignable<TestT>));
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<typename TestT::model_type> ));

	public:

		BOOST_CONCEPT_USAGE(RANSACProblemConceptC)
		{
			typedef typename TestT::model_type ModelT;

			typename TestT::minimal_solver_type* pmin=nullptr; (void)pmin;
			typename TestT::lo_solver_type* plo=nullptr; (void)plo;

			static_assert(is_same<typename TestT::minimal_solver_type::model_type, typename TestT::lo_solver_type::model_type>::value, "RANSACProblemConceptC : Incompatible minimal and LO solvers");

			TestT tested;

			bool flagValid=tested.IsValid(); (void)flagValid;

			double cost=tested.Cost();	(void)cost;
			unsigned int sGenerator=tested.GeneratorSize(); (void)sGenerator;
			size_t nObservation=tested.GetObservationSetSize(); (void)nObservation;
			size_t nValidation=tested.GetValidationSetSize(); (void)nValidation;

			set<unsigned int> indexList;
			unsigned int diversity=tested.ComputeDiversity(indexList); (void)diversity;
			bool flagValidG=tested.ValidateGenerator(indexList); (void)flagValidG;

			list<ModelT> models=tested.GenerateModel(indexList);

			bool flagInlier;
			double score;
			flagInlier=tested.EvaluateObservation(*models.begin(),0);
			tie(flagInlier, score)=tested.EvaluateValidator(*models.begin(),0); (void)score; (void)flagInlier;

			typedef typename TestT::data_container_type DataContainerT;
			DataContainerT inlierList=tested.GetInlierObservations(*models.begin());
			tested.SetObservations(inlierList);
			tested.SetValidators(inlierList);

			optional<unsigned int> index=tested.GetObservationIndex(*inlierList.begin());

			list<ModelT> modelsLO=tested.GenerateModelLO(indexList); (void)modelsLO;
			size_t loGeneratorSize=tested.LOGeneratorSize(); (void)loGeneratorSize;

			bool flagSimilar=tested.IsSimilar(*models.begin(), *models.end()); (void)flagSimilar;

			DataContainerT observationList=tested.GetObservations(); (void)observationList;

			typename TestT::minimal_solver_type minimalSolver=tested.GetMinimalSolver(); (void)minimalSolver;
			typename TestT::lo_solver_type loSolver=tested.GetLOSolver(); (void)loSolver;
		}	//BOOST_CONCEPT_USAGE(RANSACProblemConceptC)

	//@endcond
};	//class RANSACProblemConceptC
}	//RANSACN
}	//SeeSawN

#endif /* RANSAC_PROBLEM_CONCEPT_IPP_6212356 */
