/*
 * TestRANSAC.cpp RANSAC unit tests
 * @author Evren Imre
 * @date 20 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE RANSAC

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <Eigen/Dense>
#include <list>
#include <tuple>
#include <cmath>
#include <iterator>
#include <algorithm>
#include "RANSAC.h"
#include "RANSACGeometryEstimationProblem.h"
#include "RANSACDualAbsoluteQuadricProblem.h"
#include "TwoStageRANSAC.h"
#include "SequentialRANSAC.h"
#include "../Elements/Correspondence.h"
#include "../Elements/CorrespondenceDecimator.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Geometry/Homography2DSolver.h"
#include "../Geometry/DualAbsoluteQuadricSolver.h"
#include "../Wrappers/BoostStatisticalDistributions.h"

namespace SeeSawN
{
namespace UnitTestsN
{

namespace TestRANSACN
{

using namespace SeeSawN::RANSACN;

using Eigen::Matrix3d;
using std::list;
using std::tuple;
using std::get;
using std::sqrt;
using std::next;
using std::equal;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::CorrespondenceDecimatorC;
using SeeSawN::ElementsN::CorrespondenceDecimatorParametersC;
using SeeSawN::ElementsN::DecimatorQuantisationStrategyT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::Quadric3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::MetricsN::SymmetricTransferErrorConstraint2DT;
using SeeSawN::MetricsN::SymmetricTransferErrorH2DT;
using SeeSawN::GeometryN::Homography2DSolverC;
using SeeSawN::GeometryN::DualAbsoluteQuadricSolverC;
using SeeSawN::WrappersN::PDFWrapperC;

BOOST_AUTO_TEST_SUITE(RANSAC_Problems)
//FIXME Adding these test cases (or even an empty case) stalls ctest
/*
BOOST_AUTO_TEST_CASE(RANSAC_Geometry_Estimation_Problem)
{
	typedef RANSACGeometryEstimationProblemC<Homography2DSolverC, Homography2DSolverC> RANSACHomography2DProblemT;

	//Default constructor

	RANSACHomography2DProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	CoordinateCorrespondenceList2DT observationSet;
	CoordinateCorrespondenceList2DT validationSet;
	SymmetricTransferErrorConstraint2DT constraint(SymmetricTransferErrorH2DT(), 1, 1);
	Homography2DSolverC minimalSolver;
	Homography2DSolverC loSolver;
	RANSACHomography2DProblemT::dimension_type1 binSize1(10, 10);
	RANSACHomography2DProblemT::dimension_type2 binSize2(50, 50);

	problem1.Configure(observationSet, validationSet, constraint, minimalSolver, loSolver, binSize1, binSize2);
	BOOST_CHECK(!problem1.IsValid());

	//Generate some data and bind to the problem
	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,2), Coordinate2DT(9, 7), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5,3), Coordinate2DT(13, 8), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4,1), Coordinate2DT(11, 6), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31,25), Coordinate2DT(65, 30), MatchStrengthC(0.1, 0.25)));

	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(3,9), Coordinate2DT(9, 14), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(5,2), Coordinate2DT(0, 0), MatchStrengthC(0.1, 0.25)));	//Outlier

	//Constructor
	Homography2DSolverC::loss_type lossFunction(sqrt(5.99), sqrt(5.99));
	RANSACHomography2DProblemT problem2(observationSet, validationSet, constraint, lossFunction, minimalSolver, loSolver, 0.01, 12, binSize1, binSize2);
	BOOST_CHECK(problem2.IsValid());

	problem2.Configure(observationSet, validationSet, constraint, minimalSolver, loSolver, binSize1, binSize2);
	BOOST_CHECK(problem2.IsValid());
	BOOST_CHECK_EQUAL(problem2.GetObservationSetSize(),8);
	BOOST_CHECK_EQUAL(problem2.GetValidationSetSize(),3);

	BOOST_CHECK_EQUAL(problem2.GeneratorSize(), 4);
	BOOST_CHECK_EQUAL(problem2.LOGeneratorSize(), 12);

	problem2.SetObservations(validationSet);
	BOOST_CHECK_EQUAL(problem2.GetObservationSetSize(), 3);

	problem2.Configure(observationSet, validationSet, constraint, minimalSolver, loSolver, binSize1, binSize2);

	BOOST_CHECK_EQUAL(problem2.GetObservationSetSize(), 8);
	BOOST_CHECK_EQUAL(problem2.GetValidationSetSize(), 3);

	set<unsigned int> generator{0,2,4,6};
	BOOST_CHECK_EQUAL(problem2.ComputeDiversity(generator), 4);
	BOOST_CHECK(problem2.ValidateGenerator(generator));

	set<unsigned int> generator2{0,1,2,3};
	BOOST_CHECK_EQUAL(problem2.ComputeDiversity(generator2), 2);

	typedef RANSACHomography2DProblemT::model_type ModelT;
	ModelT mHmin= *problem2.GenerateModel(generator).begin();

	set<unsigned int> generatorLO{0,1,2,3,4,5,6,7};
	ModelT mHLO= *problem2.GenerateModel(generatorLO).begin();

	BOOST_CHECK(problem2.IsSimilar(mHmin, mHLO));
	BOOST_CHECK(!problem2.IsSimilar(mHmin, mHmin*0));

	tuple<bool, double> inlier=problem2.EvaluateValidator(mHmin,0);
	BOOST_CHECK(get<0>(inlier));
	BOOST_CHECK_SMALL(get<1>(inlier)-lossFunction(0), 1e-8);

	tuple<bool, double> outlier=problem2.EvaluateValidator(mHmin,2);
	BOOST_CHECK(!get<0>(outlier));
	BOOST_CHECK_EQUAL(get<1>(outlier),lossFunction(sqrt(5.99)));

	bool flagInlierO=problem2.EvaluateObservation(mHmin,0);
	BOOST_CHECK(flagInlierO);

	//Accessors

	CoordinateCorrespondenceList2DT problemObservationSet=problem2.GetObservations();
	BOOST_CHECK_EQUAL(observationSet.size(), problemObservationSet.size());
	BOOST_CHECK(observationSet.left==problemObservationSet.left);
	BOOST_CHECK(observationSet.right==problemObservationSet.right);

	CoordinateCorrespondenceList2DT problemValidationSet=problem2.GetValidators();
	BOOST_CHECK_EQUAL(validationSet.size(), problemValidationSet.size());
	BOOST_CHECK(validationSet.left==problemValidationSet.left);
	BOOST_CHECK(validationSet.right==problemValidationSet.right);

	CorrespondenceT singleObservation=*problem2.GetObservation(1);
	BOOST_CHECK(singleObservation== (*next(observationSet.begin(),1)) );
	BOOST_CHECK(!problem2.GetObservation(1000));

	unsigned int observationIndex=*problem2.GetObservationIndex(singleObservation);
	BOOST_CHECK_EQUAL(observationIndex,1);
	BOOST_CHECK(!problem2.GetObservationIndex(*validationSet.rbegin()));

	auto problemConstraint=problem2.GetConstraint();
	BOOST_CHECK(problemConstraint(mHmin, observationSet.begin()->left, observationSet.begin()->right));

	auto problemMap=problem2.GetLossMap();
	BOOST_CHECK_CLOSE( problemMap(get<1>(problemConstraint.Enforce(mHmin, observationSet.begin()->left, observationSet.begin()->right))), problemMap(0), 1e-8);

	RANSACHomography2DProblemT::minimal_solver_type minimalSolver2=problem2.GetMinimalSolver();
	BOOST_CHECK(minimalSolver(observationSet).begin()->isApprox(*minimalSolver2(observationSet).begin(), 1e-6));

	RANSACHomography2DProblemT::minimal_solver_type loSolver2=problem2.GetLOSolver();
	BOOST_CHECK(loSolver(observationSet).begin()->isApprox(*loSolver2(observationSet).begin(), 1e-6));

	//ComputeBinSize

	vector<Coordinate2DT> coordinates{Coordinate2DT(0,0), Coordinate2DT(3,2), Coordinate2DT(5,3), Coordinate2DT(4,1)};
	optional<RANSACHomography2DProblemT::dimension_type1> binSize3=ComputeBinSize<Coordinate2DT>(coordinates, 2);

	BOOST_CHECK(binSize3);
	BOOST_CHECK_CLOSE((*binSize3)[0], 0.935414, 1e-4);
	BOOST_CHECK_CLOSE((*binSize3)[1], 0.559017, 1e-4);

	BOOST_CHECK(!ComputeBinSize<Coordinate2DT>(vector<Coordinate2DT>(), 2));
}	//BOOST_AUTO_TEST_CASE(RANSAC_Geometry_Estimation_Problem)

BOOST_AUTO_TEST_CASE(RANSAC_Dual_Absolute_Quadric_Problem)
{
	//Data
	vector<QuaternionT> listq(4);
	listq[0]=QuaternionT(0.85154,0.0616971,-0.519809,-0.029535);
	listq[1]=QuaternionT(0.953864, 0.0283951, -0.298819, -0.00665323);
	listq[2]=QuaternionT(0.988633, 0.0457148, -0.143168, -0.00410147);
	listq[3]=QuaternionT(0.996071, 0.0144022, 0.0872254, -0.00511926 );

	vector<Coordinate3DT> listC(4);
	listC[0]=Coordinate3DT(-2.27321, -0.0101527, -2.9959);
	listC[1]=Coordinate3DT(-1.40423, -0.0169625, -4.2509);
	listC[2]=Coordinate3DT(-0.364868, -0.0446325, -5.00531);
	listC[3]=Coordinate3DT(1.63765, 0.172693, -5.39282);

	vector<CameraMatrixT> mPList(5);
	for(size_t c=0; c<4; ++c)
	{
		mPList[c].block(0,0,3,3)=Matrix3d::Identity();
		mPList[c].col(3)=listC[c];
		mPList[c] = listq[c].matrix()*mPList[c];
	}	//for(size_t c=0; c<4; ++c)

	mPList[4]=mPList[3];	//Outlier
	mPList[4]= (mPList[0]*mPList[1].transpose())*mPList[4];	//Random projective transformation

	//Constructors

	RANSACDualAbsoluteQuadricProblemC problem1;	//Default constructor
	BOOST_CHECK(!problem1.IsValid());

	DualAbsoluteQuadricSolverC solver;
	RANSACDualAbsoluteQuadricProblemC problem2(mPList, mPList, solver, solver, 4, 4, 0.01, 0.1);
	BOOST_CHECK(problem2.IsValid());

	BOOST_CHECK_EQUAL(problem2.Cost(), 1e16);

	BOOST_CHECK_EQUAL(problem2.GetObservationSetSize(), mPList.size());
	BOOST_CHECK_EQUAL(problem2.GetValidationSetSize(), mPList.size());
	BOOST_CHECK_EQUAL(problem2.GetObservations().size(), mPList.size());
	BOOST_CHECK(equal(problem2.GetObservations().begin(), problem2.GetObservations().end(), mPList.begin())  );

	set<unsigned int> generator2{0,1,2,3};
	list<Quadric3DT> mQ2List=problem2.GenerateModel(generator2);

	Quadric3DT mQ2GT=Quadric3DT::Identity(); mQ2GT(3,3)=0; mQ2GT.normalize();
	BOOST_CHECK_EQUAL(mQ2List.size(),1);
	BOOST_CHECK(mQ2List.begin()->isApprox(mQ2GT, 1e-6));

	RANSACDualAbsoluteQuadricProblemC::data_container_type inliers=problem2.GetInlierObservations(mQ2GT);
	BOOST_CHECK_EQUAL(inliers.size(), 4);
	BOOST_CHECK(equal(inliers.begin(), inliers.end(), mPList.begin()) );

	optional<unsigned int> index1=problem2.GetObservationIndex(mPList[1]);
	BOOST_CHECK(index1);
	BOOST_CHECK_EQUAL(*index1,1);

	BOOST_CHECK_EQUAL(problem2.ComputeDiversity(generator2),4);
	BOOST_CHECK(problem2.ValidateGenerator(generator2));

	BOOST_CHECK_EQUAL(problem2.GeneratorSize(), 4);
	BOOST_CHECK_EQUAL(problem2.LOGeneratorSize(), 4);

	list<Quadric3DT> mQ2LOList=problem2.GenerateModelLO(generator2);
	BOOST_CHECK_EQUAL(mQ2LOList.size(),1);
	BOOST_CHECK(mQ2LOList.begin()->isApprox(mQ2GT, 1e-6));

	list<Quadric3DT> mQ2List2=problem2.GetMinimalSolver()(inliers);
	BOOST_CHECK_EQUAL(mQ2List2.size(),1);
	BOOST_CHECK(mQ2List2.begin()->isApprox(mQ2GT, 1e-6));

	list<Quadric3DT> mQ2LOList2=problem2.GetLOSolver()(inliers);
	BOOST_CHECK_EQUAL(mQ2LOList2.size(),1);
	BOOST_CHECK(mQ2LOList2.begin()->isApprox(mQ2GT, 1e-6));

	BOOST_CHECK(problem2.EvaluateObservation(mQ2GT, 0));

	bool flagInlier2;
	double error2;
	tie(flagInlier2, error2)=problem2.EvaluateValidator(mQ2GT, 1);
	BOOST_CHECK(flagInlier2);
	BOOST_CHECK_SMALL(error2, 1e-6);

	bool flagInlier2b;
	double error2b;
	tie(flagInlier2b, error2b)=problem2.EvaluateValidator(mQ2GT, 4);
	BOOST_CHECK(!flagInlier2b);
	BOOST_CHECK_CLOSE(error2b, 0.01, 1e-4);

	Quadric3DT mQ2perturbed(mQ2GT); mQ2perturbed(0,0)=10; mQ2perturbed/=mQ2perturbed.norm();

	BOOST_CHECK(!problem2.IsSimilar(mQ2GT, mQ2perturbed));
	BOOST_CHECK(problem2.IsSimilar(mQ2GT, mQ2GT));

	problem2.SetObservations(inliers);
	BOOST_CHECK_EQUAL(problem2.GetObservations().size(), inliers.size());

	problem2.SetValidators(inliers);
}	//BOOST_AUTO_TEST_CASE(RANSAC_Dual_Absolute_Quadric_Problem)
*/
BOOST_AUTO_TEST_SUITE_END()	//RANSAC_Problems

BOOST_AUTO_TEST_SUITE(RANSAC_Engine)

BOOST_AUTO_TEST_CASE(RANSAC)
{
	//Data

	CoordinateCorrespondenceList2DT observationSet;
	CoordinateCorrespondenceList2DT validationSet;

	typedef CoordinateCorrespondenceList2DT::value_type CorrespondenceT;

	//Model1 : 2x+3, y+5
	//Clean inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(5, 6), MatchStrengthC(0.1, 0.1)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.3)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(13,18), Coordinate2DT(29, 23), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26,14), Coordinate2DT(55, 19), MatchStrengthC(0.1, 0.35)));

	//Noisy inliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5.33,3.22), Coordinate2DT(13.51, 7.46), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(26.12,13.65), Coordinate2DT(55.25, 18.90), MatchStrengthC(0.1, 0.4)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3.1,2.3), Coordinate2DT(9.45, 7.11), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(4.21,1.16), Coordinate2DT(11.23, 5.90), MatchStrengthC(0.1, 0.35)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(28.54, 22.61), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));

	//Outliers
	observationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.2)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,60), Coordinate2DT(11, 45), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,0), Coordinate2DT(30, 5), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(3,14), Coordinate2DT(90, 14), MatchStrengthC(0.1, 0.75)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5,0), Coordinate2DT(50, 0), MatchStrengthC(0.1, 0.85)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(310,65), Coordinate2DT(24,13), MatchStrengthC(0.1, 0.6)));

	//Validation
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(3,9), Coordinate2DT(9, 14), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(5,2), Coordinate2DT(13, 7), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(11,10), Coordinate2DT(25, 15), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(25,16), Coordinate2DT(53, 22), MatchStrengthC(0.1, 0.65)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(30,32), Coordinate2DT(63, 38), MatchStrengthC(0.1, 0.35)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.26)));	//Outlier
	validationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(65.24, 30.13), MatchStrengthC(0.1, 0.61)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0.78,0.51), Coordinate2DT(3, 5), MatchStrengthC(0.1, 0.15)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(21.13,6.22), Coordinate2DT(45, 11), MatchStrengthC(0.1, 0.33)));	//Noisy

	//2nd model: Identity
	observationSet.push_back(CorrespondenceT(Coordinate2DT(1,1), Coordinate2DT(1, 1), MatchStrengthC(0.1, 0.12)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(21, 6), MatchStrengthC(0.1, 0.17)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(12.88,17.91), Coordinate2DT(12.54, 17.61), MatchStrengthC(0.1, 0.45)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(31.54,24.41), Coordinate2DT(31.24, 24.13), MatchStrengthC(0.1, 0.6)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(5,0), Coordinate2DT(5, 0), MatchStrengthC(0.1, 0.25)));
	observationSet.push_back(CorrespondenceT(Coordinate2DT(25,25), Coordinate2DT(25, 25), MatchStrengthC(0.1, 0.31)));

	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(0, 0), MatchStrengthC(0.1, 0.05)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(3,9), Coordinate2DT(3, 9), MatchStrengthC(0.1, 0.45)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(5,2), Coordinate2DT(5, 2), MatchStrengthC(0.1, 0.25)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(25,16), Coordinate2DT(25, 16), MatchStrengthC(0.1, 0.55)));
	validationSet.push_back(CorrespondenceT(Coordinate2DT(0,0), Coordinate2DT(5, 30), MatchStrengthC(0.1, 0.15)));	//Outlier
	validationSet.push_back(CorrespondenceT(Coordinate2DT(32,24), Coordinate2DT(31.54, 24.41), MatchStrengthC(0.1, 0.65)));	//Noisy
	validationSet.push_back(CorrespondenceT(Coordinate2DT(21,6), Coordinate2DT(21.13, 6.22), MatchStrengthC(0.1, 0.31)));	//Noisy

	//Sort the observations wrt/ambiguity

	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT> decimator;
	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>::dimension_type1 dummy1;
	CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>::dimension_type2 dummy2;
	CorrespondenceDecimatorParametersC decimatorParameters;

	auto outputO=decimator.Run(observationSet, dummy1, dummy2, decimatorParameters);
	observationSet.swap(get<0>(outputO));

	auto outputV=decimator.Run(validationSet, dummy1, dummy2, decimatorParameters);
	validationSet.swap(get<0>(outputV));

	//Problem
	typedef RANSACGeometryEstimationProblemC<Homography2DSolverC, Homography2DSolverC> RANSACHomography2DProblemT;
	SymmetricTransferErrorConstraint2DT constraint(SymmetricTransferErrorH2DT(), sqrt(5.99), 1);
	Homography2DSolverC minimalSolver;
	Homography2DSolverC loSolver;
	RANSACHomography2DProblemT::dimension_type1 binSize1(10, 10);
	RANSACHomography2DProblemT::dimension_type2 binSize2(50, 50);

	Homography2DSolverC::loss_type lossFunction(sqrt(5.99), sqrt(5.99));
	RANSACHomography2DProblemT problem(observationSet, validationSet, constraint, lossFunction, minimalSolver, loSolver, 0.1, 6, binSize1, binSize2);

	//Default constructor
	typedef RANSACC<RANSACHomography2DProblemT> RANSACHomography2DEngineT;
	RANSACHomography2DEngineT ransac1; (void)ransac1;

	//Reference models
	Homography2DSolverC::model_type mHa; mHa<<2, 0, 3, 0, 1, 5, 0, 0, 1; mHa.normalized();

	Homography2DSolverC::model_type mHb; mHb.setIdentity(); mHb/=sqrt(3);

	//Default mode: Single hypothesis, PROSAC

	RANSACHomography2DEngineT::rng_type rng;

	RANSACParametersC parameters1;
	parameters1.minObservationSupport=2.5;

	RANSACHomography2DEngineT::result_type output1;
	RANSACDiagnosticsC diagnostics1=RANSACHomography2DEngineT::Run(output1, problem, rng, parameters1);

	BOOST_CHECK(diagnostics1.flagSuccess);
	BOOST_CHECK(diagnostics1.nIterations <= parameters1.maxIteration);
	BOOST_CHECK(diagnostics1.nIterations >= parameters1.minIteration);
	BOOST_CHECK_EQUAL(output1.size(),1);
	BOOST_CHECK( get<RANSACHomography2DEngineT::iInlierList>(output1.begin()->second).size()>=9);
	BOOST_CHECK( Homography2DSolverC::ComputeDistance(mHa, get<RANSACHomography2DEngineT::iModel>(output1.begin()->second)) < 0.01);

	//Inliers
	RANSACHomography2DProblemT::data_container_type inlierObservations=problem.GetInlierObservations( get<RANSACHomography2DEngineT::iModel>(output1.begin()->second) );
	BOOST_CHECK_EQUAL(inlierObservations.size(),12);

	//Two-stage RANSAC
	TwoStageRANSACParametersC tsParameters;
	tsParameters.parameters1=parameters1;
	tsParameters.parameters2=parameters1;
	tsParameters.minConfidence=0.99;
	tsParameters.eligibilityRatio=0.25;

	typedef TwoStageRANSACC<RANSACHomography2DProblemT> TSRANSACHomography2DEngineT;
	TSRANSACHomography2DEngineT::result_type tsResult;
	TwoStageRANSACDiagnosticsC tsDiagnostics=TSRANSACHomography2DEngineT::Run(tsResult, problem, problem, rng, tsParameters);
	BOOST_CHECK( tsDiagnostics.flagSuccess);
	BOOST_CHECK( get<RANSACHomography2DEngineT::iInlierList>(tsResult.begin()->second).size()>=9);
	BOOST_CHECK( Homography2DSolverC::ComputeDistance(mHa, get<RANSACHomography2DEngineT::iModel>(tsResult.begin()->second)) < 0.0125);

	//No PROSAC
	parameters1.flagPROSAC=false;
	RANSACHomography2DEngineT::result_type output2;
	RANSACDiagnosticsC diagnostics2=RANSACHomography2DEngineT::Run(output2, problem, rng, parameters1);
	BOOST_CHECK(diagnostics2.flagSuccess);
	BOOST_CHECK( get<RANSACHomography2DEngineT::iInlierList>(output2.begin()->second).size()>=9);
	BOOST_CHECK( Homography2DSolverC::ComputeDistance(mHa, get<RANSACHomography2DEngineT::iModel>(output2.begin()->second)) < 0.0125);

	//Multiobjective
	RANSACHomography2DProblemT problem3(observationSet, validationSet, constraint, lossFunction, minimalSolver, loSolver, 0.25, 6, binSize1, binSize2);

	RANSACParametersC parameters3;
	parameters3.minObservationSupport=2.5;
	parameters3.minModelSimilarity=0.5;
	parameters3.minCoverage=0.15;
	parameters3.maxSolution=2;
	parameters3.minIteration=2500;
	parameters3.maxIteration=5000;
	RANSACHomography2DEngineT::result_type output3;
	RANSACDiagnosticsC diagnostics3=RANSACHomography2DEngineT::Run(output3, problem3, rng, parameters3);

	BOOST_CHECK(diagnostics3.flagSuccess);
	BOOST_CHECK_EQUAL(output3.size(),2);
	BOOST_CHECK( get<RANSACHomography2DEngineT::iInlierList>(output3.begin()->second).size()>=9);
	BOOST_CHECK_LE( Homography2DSolverC::ComputeDistance(mHa, get<RANSACHomography2DEngineT::iModel>(output3.begin()->second)) , 0.02);
	BOOST_CHECK( get<RANSACHomography2DEngineT::iInlierList>(output3.rbegin()->second).size()>=4);
	BOOST_CHECK_LE( Homography2DSolverC::ComputeDistance(mHb, get<RANSACHomography2DEngineT::iModel>(output3.rbegin()->second)) , 0.02);

	//Exceptions
	RANSACHomography2DEngineT::result_type output;
	RANSACParametersC brokenParameters;

	brokenParameters.minObservationSupport=0;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minObservationSupport=1;

	brokenParameters.maxErrorRatio=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxErrorRatio=1;

	brokenParameters.growthRate=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.growthRate=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.growthRate=0.5;

	brokenParameters.epsilon=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.epsilon=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.epsilon=0.5;

	brokenParameters.delta=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.delta=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.delta=0.5;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.delta=0.2;

	brokenParameters.maxDeltaTolerance=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxDeltaTolerance=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxDeltaTolerance=0.5;

	brokenParameters.minModelSimilarity=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minModelSimilarity=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minModelSimilarity=0.5;

	brokenParameters.minCoverage=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minCoverage=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minCoverage=0.5;

	brokenParameters.parsimonyFactor=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.parsimonyFactor=1.05;

	brokenParameters.diversityFactor=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.diversityFactor=0.95;

	brokenParameters.maxSolution=0;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxSolution=1;

	brokenParameters.minConfidence=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minConfidence=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.minConfidence=0.5;

	brokenParameters.eligibilityRatio=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.eligibilityRatio=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.eligibilityRatio=0.5;

	brokenParameters.maxIteration=0;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxIteration=1;
	brokenParameters.minIteration=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxIteration=100;

	brokenParameters.topN=-1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.topN=2;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.topN=0.5;

	brokenParameters.maxError=-1;
	brokenParameters.minError=1;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, problem, rng, brokenParameters), invalid_argument);
	brokenParameters.maxError=2;

	RANSACHomography2DProblemT invalidProblem;
	BOOST_CHECK_THROW(RANSACHomography2DEngineT::Run(output, invalidProblem, rng, parameters1), invalid_argument);

	//Empty sets
	CoordinateCorrespondenceList2DT emptyObservationSet;
	CoordinateCorrespondenceList2DT emptyValidationSet;
	RANSACHomography2DProblemT problem2(problem);

	problem2.Configure(emptyObservationSet, emptyValidationSet, constraint, minimalSolver, loSolver, binSize1, binSize2);
	BOOST_CHECK(problem2.IsValid());

	//Set data
	problem2=problem;
	problem2.SetObservations(validationSet);
	problem2.SetValidators(observationSet);

	BOOST_CHECK_EQUAL(problem2.GetObservationSetSize(), validationSet.size());
	BOOST_CHECK_EQUAL(problem2.GetValidationSetSize(), observationSet.size());
}	//BOOST_AUTO_TEST_CASE(RANSAC)

BOOST_AUTO_TEST_CASE(Sequential_RANSAC)
{
	//Only a compile test
	typedef RANSACGeometryEstimationProblemC<Homography2DSolverC, Homography2DSolverC> RANSACHomography2DProblemT;
	typedef SequentialRANSACC<RANSACHomography2DProblemT> RANSACT;

	RANSACT engine; (void)engine;
}	//BOOST_AUTO_TEST_CASE(Sequential_RANSAC)

BOOST_AUTO_TEST_SUITE_END()//RANSAC_Engine

}	//TestRANSACN
}	//UnitTestsN
}	//SeeSawN

