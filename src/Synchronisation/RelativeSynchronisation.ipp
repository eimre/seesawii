/**
 * @file RelativeSynchronisation.ipp Implementation of the relative synchronisation algorithm
 * @author Evren Imre
 * @date 13 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RELATIVE_SYNCHRONISATION_IPP_1608001
#define RELATIVE_SYNCHRONISATION_IPP_1608001

#include <boost/concept_check.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/optional.hpp>
#include <boost/numeric/interval.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <boost/range/counting_range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <list>
#include <array>
#include <tuple>
#include <cstddef>
#include <map>
#include <cmath>
#include <utility>
#include <iterator>
#include <set>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <functional>
#include <type_traits>
#include "../Elements/Correspondence.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Metrics/RobustLossFunction.h"
#include "../StateEstimation/Viterbi.h"
#include "../StateEstimation/ViterbiRelativeSynchronisationProblem.h"
#include "../StateEstimation/ViterbiNormalisedHistogramMatchingProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLFrameAlignmentProblem.h"
#include "../Wrappers/BoostRange.h"
#include "../Wrappers/BoostBimap.h"
#include "../SignalProcessing/HistogramMatching.h"

namespace SeeSawN
{
namespace SynchronisationN
{

using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::set_of;
using boost::bimaps::left_based;
using boost::optional;
using boost::numeric::in;
using boost::numeric::interval;
using boost::numeric::overlap;
using boost::numeric::empty;
using boost::numeric::subset;
using boost::dynamic_bitset;
using boost::counting_range;
using boost::for_each;
using boost::transform;
using boost::copy;
using boost::accumulate;
using boost::max_element;
using boost::range_value;
using boost::adaptors::filtered;
using boost::adaptors::map_values;
using boost::lexical_cast;
using boost::ForwardRangeConcept;
using boost::RandomAccessRangeConcept;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::MatrixX2d;
using Eigen::Vector2d;
using Eigen::Hyperplane;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::get;
using std::vector;
using std::array;
using std::list;
using std::size_t;
using std::map;
using std::multimap;
using std::ceil;
using std::round;
using std::pair;
using std::make_pair;
using std::advance;
using std::next;
using std::prev;
using std::min;
using std::set;
using std::multiset;
using std::invalid_argument;
using std::string;
using std::copy_if;
using std::back_inserter;
using std::inserter;
using std::is_same;
using std::max;
using std::greater;
using std::fma;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::ImageFeatureTrajectoryT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::Line2DT;
using SeeSawN::ElementsN::MakeLine2D;
using SeeSawN::ElementsN::DecomposeLine2D;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::MetricsN::PseudoHuberLossC;
using SeeSawN::StateEstimationN::ViterbiC;
using SeeSawN::StateEstimationN::ViterbiParametersC;
using SeeSawN::StateEstimationN::ViterbiRelativeSynchronisationProblemC;
using SeeSawN::StateEstimationN::ViterbiNormalisedHistogramMatchingProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PowellDogLegParametersC;
using SeeSawN::OptimisationN::PowellDogLegDiagnosticsC;
using SeeSawN::OptimisationN::PDLFrameAlignmentProblemC;
using SeeSawN::WrappersN::MakeBinaryZipRange;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::SignalProcessingN::HistogramMatchingC;
using SeeSawN::SignalProcessingN::HistogramMatchingParametersC;

//TODO LS line fit: instead of using all points, try a balanced sampling, for more accurate fitting. Points clustered together bias the solution
//TESTME
/**
 * @brief Parameters for \c RelativeSynchronisationC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct RelativeSynchronisationParametersC
{
	unsigned int nThreads;	///< Number of threads. >=1

	/** @name Constraints */ //@{
	optional<ViterbiRelativeSynchronisationProblemC::interval_type> alphaRange;	///< Permissible range for the frame rate. >0
	optional<ViterbiRelativeSynchronisationProblemC::interval_type> tauRange;	///< Permissible range for the temporal offset
	optional<vector<double>> admissibleAlpha;		///< List of admissible alpha values. >0

	bool flagNoFrameDrop;	///< If \c true , the algorithm assumes that there are no frame drops
	//@}

	/** @name Frame matching */ //@{
	FeatureMatcherParametersC matcherParameters;	///< Feature matcher parameters
	double noiseVariance;	///< Variance of the image noise, in pixel^2. >0
	size_t minFeatureCorrespondence;	///< Minimum number of feature correspondences for a valid index pairing

	bool flagFixedCamera;	///< If \c true , the relative calibration between the cameras is fixed

	bool flagPropagateFeatureCorrespondences;	///< If \c true , the Viterbi problem propagates the feature correspondences across the feature tracks

	PowellDogLegParametersC pdlParameters;	///< Parameters for nonlinear minimisation
	//@}

	/** @name Line fitting */ //@{
	unsigned int minSegmentSupport;	///< Minimum absolute support for a line segment. >0. The effective threshold is the minimum of the relative and the absolute thresholds
	double segmentInlierTh;	///< Segment inlier threshold. >0

	//Broken-line fitting
	double tauQuantisationTh;	///< Maximum distance between the offset of a line segment, and that of a candidate from the list of permissible offsets. >0
	double maxOverlap;	///< Maximum value of the overlap coefficient between two line segments. Used in the merging step. (0,1]
	double minSpan;	///< Minimum span of a line segment, as a percentage of the span of the reference set. Span refers to the size of the interval defined by the correspondences. (0,1]

	//Frame drops
	unsigned int isolationTh;	///< Maximum permitted distance in x-axis between an index correspondences, and its closest neighbour. In frames. >0
	//@}

	/** @name Guided synchronisation */ ///@{
	optional<array<double,2> > guidedSearchRegion=array<double,2>{0.1,1};	///< Extent of the search region around the current estimate, for guided synchronisation. If there are frame-drop events, tau is an "envelope" including the minimum and the maximum shift values. If invalid, guided synchronisation is disabled. >=0
	static constexpr size_t iAlphaExtent=0;	///< Index of the extent of the frame rate for guided synchronisation
	static constexpr size_t iTauExtent=1;	///< Index of the extent of the temporal offset for guided synchronisation
	///@}

	/** Default constructor */
	RelativeSynchronisationParametersC() : nThreads(1), flagNoFrameDrop(false), noiseVariance(1), minFeatureCorrespondence(2), flagFixedCamera(true), flagPropagateFeatureCorrespondences(false),  minSegmentSupport(10), segmentInlierTh(0.1), tauQuantisationTh(0.1), maxOverlap(0.1), minSpan(0.1), isolationTh(10)
	{}
};	//struct RelativeSynchronisationParametersC

/**
 * @brief Diagnostics for \c RelativeSynchronisationC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct RelativeSynchronisationDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully

	double score;	///< Reliability of the result

	/** Default constructor */
	RelativeSynchronisationDiagnosticsC() : flagSuccess(false), score(0)
	{}
};	//struct RelativeSynchronisationDiagnosticsC

/**
 * @brief Synchronisation for a pair of image sequences
 * @remarks Algorithm
 * 	- Find the corresponding frame indices via Viterbi. The frame similarity metric is conformance to the epipolar constraint
 * 	- If the relative calibration between the sequences is fixed, refine the correspondences to subframe level
 * 	- Exhaustively search for the best line. This sets the frame rate, and the non-integer part of the temporal offset
 * 	- Fit a broken-line model to the correspondences, to identify the frame drops
 * 	- The segment that holds the earliest index correspondence defines the temporal offset
 * 	- Guided synchronisation: Run the algorithm again on a search region including the inliers to the estimated synchronisation model
 * @remarks Terminology
 * 	- Reference sequence: The first sequence. Frame rate=1, offset=0
 * 	- Target sequence: The second sequence, synchronise wrt first
 * @remarks Usage notes
 * 	- Enforcing a range in temporal offset (\c tauRange) may preclude the detection of some frame drop events, especially in the earlier sections of a sequence
 * 	- If none of the admissible frame rate values satisfy the range constraint, the operation fails
 * 	- The terminal index correspondences of a section may creep into the frame-drop intervals
 * 	- If the width of \c alphaRange=0, the algorithm operates under the assumption of known frame rate
 * 	- If the width of \c tauRange<1, it is expanded to 1. This prevents the rejection of correct index correspondences due to quantisation
 * 	- \c guidedSearchRegion is clipped at the width of \c alphaRange and \c tauRange
 * 	- Frame-drop estimates are relative as well. Therefore, if there are overlapping frame-drop events in the reference and the target sequences, the estimated windows will be erroneous
 * 	- Isolated index correspondences are not reliable: an activity is expected to give rise to a burst of correspondences, unless it is instantaneous
 * @remarks R1: "Through-the-Lens Multi-Camera Synchronisation and Frame-Drop Detection for 3D Reconstruction," E. Imre, J.-Y. Guillemaut, A. Hilton, 3DIMPVT 2012.
 * @remarks R2: "Through-the-Lens Synchronsiation of Heterogeneous Camera Networks, " E. Imre, A. Hilton, BMVC 2012.
 * @ingroup Algorithm
 * @nosubgrouping
 */
class RelativeSynchronisationC
{
	public:

		/**@name frame_drop_type definition */ //@{
		typedef tuple<unsigned int, interval<double> > frame_drop_type;		///< A frame-drop event
		static constexpr size_t iDropped=0;	///< Index of the component indicating the number of dropped frames
		static constexpr size_t iInterval=1;	///< Index of the component for the time interval in which the drop event occurred
		//@}

		/**@name result_type definition */ //@{
		typedef tuple<double, double, vector<frame_drop_type>, vector<frame_drop_type> > result_type;	///< Type of the result
		static constexpr size_t iAlpha=0;	///< Index of the frame rate component
		static constexpr size_t iTau=1;	///< Index of the temporal offset component
		static constexpr size_t iDropList1=2;	///< Index of the component for the frame drop events on the first sequence
		static constexpr size_t iDropList2=3;	///< Index of the component for the frame drop events on the second sequence
		//@}

		/** Model type */ ///@{
		typedef bimap<set_of<double>, list_of<double>, left_based> index_correspondence_list_type;	///< A list of corresponding indices

		typedef tuple<Line2DT, index_correspondence_list_type> segment_type;	///< A line segment and its support
		static constexpr size_t iLine=0;	///< Index of the line component
		static constexpr size_t iSupport=1;	///< Index of the support set component

		typedef vector<segment_type> model_type;	///< A broken line model, as an ordered set of line segments
		//@}

	private:

		typedef ViterbiRelativeSynchronisationProblemC::interval_type ParameterIntervalT;	///< A parameter interval

		typedef Line2DT::VectorType CoordinateT;	///< A line coordinate
		typedef index_correspondence_list_type IndexCorrespondenceListT;	///< A list of corresponding indices
		typedef dynamic_bitset<> IndicatorArrayT;	///< An array of indicator flags
		typedef set<size_t> MembershipT;	///< Indices of the correspondences associated with a line segment

		typedef tuple<double, MembershipT> LineSectionT;	///< A section of a broken line. [Offset;support set]
		typedef tuple<double, list<LineSectionT> > BrokenLineT;	///< Broken line model. [Slope; List of sections, ordered wrt their ranges]

		typedef PseudoHuberLossC<double> LossMapT;	///< Error-to-loss mapping

		typedef ViterbiRelativeSynchronisationProblemC::measurement_type ImageT;	///< A measurement for the Viterbi algorithm
		typedef vector<Coordinate2DT> TranslationListT;	///< A list of translation vectors
		typedef tuple<vector<ImageT>, vector<TranslationListT>, vector<TranslationListT> > ImageSequenceT;	///< Type for an image sequence

		typedef frame_drop_type FrameDropT;	///< A frame-drop event

		typedef segment_type SegmentT;	///< A line segment and its support
		typedef model_type ModelT;	///< A model
		typedef result_type ResultT;	///< A synchronisation result

		/** @name State */ //@{
		optional<ViterbiRelativeSynchronisationProblemC> viterbiProblem;	///< Viterbi problem
		map<tuple<double, double>, tuple<double, double> > subframeCache;	///< Subframe refinement results: [Integer index pair, refined index pair]
		//@}

		/** @name Implementation details */ //@{
		vector<double> FilterAdmissibleAlpha(const vector<double>& src, const ParameterIntervalT& alphaRange) const;	///< Identifies the admissible alpha values within an alpha range
		void ValidateParameters(const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC& parameters) const;	///< Validates the input parameters

		IndexCorrespondenceListT FindIndexCorrespondences(const vector<ImageT>& sequence1, const vector<ImageT>& sequence2, const RelativeSynchronisationParametersC& parameters);	///< Finds the corresponding indices between two image sequences
		IndexCorrespondenceListT RefineIndexCorrespondences(const IndexCorrespondenceListT& integerCorrespondences, const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, const RelativeSynchronisationParametersC& parameters);	///< Subframe refinement for the index correspondences

		static IndexCorrespondenceListT HijackFindIndexCorrespondences(const vector<ImageT>& sequence1, const vector<ImageT>& sequence2, const RelativeSynchronisationParametersC& parameters);

		MembershipT MakeMembership(const IndicatorArrayT& flags) const;	///< Converts an indicator array to a membership list

		bool ValidateLine(const Line2DT& line, const RelativeSynchronisationParametersC& parameters) const;	///< Validates a line
		tuple<double, IndicatorArrayT> EvaluateLine(const Line2DT& line, const vector<CoordinateT>& indexCorrespondences, const LossMapT& lossMap, const RelativeSynchronisationParametersC& parameters) const;	///< Evaluates a line
		Line2DT FitLine(const vector<CoordinateT>& indexCorrespondences, const IndicatorArrayT& supportIndicators) const;	///< Fits a line to a set of inliers
		Line2DT FitOffset(const vector<CoordinateT>& indexCorrespondences, const IndicatorArrayT& supportIndicators, double alpha) const;	///< Fits a line to a set of inliers, with known slope

		optional<tuple<Line2DT, IndicatorArrayT>> IdentifyMainSegment(const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const;	///< Computes the main segment for unconstrained alpha
		multimap<double, Line2DT> ComputeLineSegments(IndicatorArrayT& supportIndicators, const vector<CoordinateT>& indexCorrespondences, const vector<double>& admissibleAlpha, const RelativeSynchronisationParametersC& parameters) const;	///< Exhaustively instantiates and ranks all feasible line segments implied by the index correspondences, over the set of admissible alpha
		map<double, pair<double, Line2DT> > QuantiseOffsets(const Line2DT& mainSegment, const multimap<double, Line2DT>& segments, const RelativeSynchronisationParametersC& parameters) const;	///< Quantises the offset values and returns the best representatives
		multimap<double, tuple<Line2DT, IndicatorArrayT> > IdentifyDominantSegments(const Line2DT& mainSegment, const multimap<double, Line2DT>& segments, const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const;	///< Identifies the dominant segments in the set
		void ResolveConflict(MembershipT& set1, MembershipT& set2) const;	///< Resolves the conflicting assignments due to overlapping support regions
		map< size_t, tuple<Line2DT, MembershipT> > AssignCorrespondences(const multimap<double, tuple<Line2DT, IndicatorArrayT> >& segments, const vector<CoordinateT>& indexCorrespondences, size_t span, const RelativeSynchronisationParametersC& parameters) const;	///< Assigns the correspondences to the line segments mutually independent intervals
		MembershipT RemoveIsolated(const MembershipT& membership, const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const;	///< Removes isolated index correspondences
		optional<BrokenLineT> FitBrokenLine(const vector<CoordinateT>& indexCorrespondences, size_t sSequence1, const RelativeSynchronisationParametersC& parameters) const;	///< Fits a broken line model to the correspondences

		tuple<double, double> AdjustInterval(double lower, double upper, unsigned int nDropped) const;	///< Adjusts an interval so that it can hold the dropped frames
		void InsertDrop(list<FrameDropT>& dst, double lower, double upper, unsigned int nDropped) const;	///< Inserts a frame drop event
		tuple<list<FrameDropT>, list<FrameDropT> > MakeFrameDrop(const BrokenLineT& model, const vector<CoordinateT>& indexCorrespondences) const;	///< Computes the frame drop events corresponding to the gaps between the sections

		optional<tuple<ResultT, ModelT, double> > Synchronise(const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, const RelativeSynchronisationParametersC& parameters);	///< Runs the synchronisation pipeline
		tuple<ResultT, ModelT, double> MakeResult(const BrokenLineT& brokenLine, const list<FrameDropT>& dropList1, const list<FrameDropT>& dropList2, const IndexCorrespondenceListT& indexCorrespondences ) const;	///< Builds the output structure
		RelativeSynchronisationParametersC MakeGuidedSynchronisationParameters(const ModelT& model, RelativeSynchronisationParametersC parameters) const;	///< Prepares a parameter object for the guided synchronisation pass
		//@}

	public:

		/**@name image_sequence_type definition */ //@{
		typedef ImageSequenceT image_sequence_type;	///< Type for an image sequence
		static constexpr size_t iObservation=0;	///< Index for the image observation component
		static constexpr size_t iForward=1;	///< Index for the forward translation vectors
		static constexpr size_t iBackward=2;	///< Index for the backward translation vectors
		static constexpr size_t iTrackId=3;	///< Index for the track id vector
		//@}

		/** @name Operations */
		RelativeSynchronisationDiagnosticsC Run(ResultT& result, ModelT& model, const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC parameters);	///< Runs the algorithm

		/** @name Utility */ //@{
		void Clear();	///< Clears the state
		template<class TrajectoryRangeT, class CameraRangeT> static ImageSequenceT MakeImageSequence(const TrajectoryRangeT& trajectoryList, const CameraRangeT& cameraList, double minActivityTh);	///< Constructs an input structure
		//@}
};	//class RelativeSynchronisationC

/********** IMPLEMENTATION STARTS HERE **********/

//TODO This should probably be under the viterbi problem
/**
 * @brief Constructs an input sequence
 * @tparam TrajectoryRangeT A range of feature trajectories
 * @tparam CameraRangeT A range of camera trajectories
 * @param[in] trajectoryList Trajectories
 * @param[in] cameraList Cameras. If it has a single element, the function assumes that the camera is fixed throughout the sequence
 * @param[in] minActivityTh The minimum number of features for a frame with a significant level of activity, relative to the frame with max feature count. Frames failing the test are eliminated. [0,1]
 * @return Input structure for \c RelativeSynchronisationC
 * @pre \c cameraList has a camera for each frame in \c trajectoryList . Or a single camera
 * @pre \c TrajectoryRangeT is a forward range holding elements of type \c ImageFeatureTrajectoryT
 * @pre \c CameraRangeT is a random access range holding elements of type \c CameraMatrixT
 * @pre \c minActivityTh is in [0,1]
 */
template<class TrajectoryRangeT, class CameraRangeT>
auto RelativeSynchronisationC::MakeImageSequence(const TrajectoryRangeT& trajectoryList, const CameraRangeT& cameraList, double minActivityTh) -> ImageSequenceT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<TrajectoryRangeT>));
	static_assert(is_same<ImageFeatureTrajectoryT, typename range_value<TrajectoryRangeT>::type>::value, "RelativeSynchronisationC::MakeImageSequence : TrajectoryRangeT must hold elements of type ImageFeatureTrajectoryT");

	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<CameraRangeT>));
	static_assert(is_same<CameraMatrixT, typename range_value<CameraRangeT>::type>::value, "RelativeSynchronisationC::MakeImageSequence : CameraRangeT must hold elements of type CameraMatrixT");

	assert(minActivityTh>=0 && minActivityTh<=1);

	ImageSequenceT output;	//Output

	//Scan the trajectory list to identify the number of features per image
	map<size_t, unsigned int> featureAcc;	//Accumulator keeping the number of features per frame
	for(const auto& currentTrajectory : trajectoryList)
		for(const auto& currentPoint : currentTrajectory)
		{
			auto it=featureAcc.emplace(currentPoint.first,1);	//Attempt to insert
			if(!it.second)	//Increment an existing value
				++(it.first->second);
		}	//for(const auto& currentPoint : currentTrajectory)

	bool flagFixed=boost::distance(cameraList)==1;
	assert(flagFixed || featureAcc.crbegin()->first <= boost::distance(cameraList));	//There must be a camera for each frame in trajectoryList

	multiset<unsigned int, greater<unsigned int> > sorter;	//Sorts the feature counts
	transform(featureAcc | map_values, inserter(sorter, sorter.begin()), [](unsigned int c){return c;});

	//Empty trajectory
	if(sorter.empty())
		return output;

	unsigned int minFeatureCount=minActivityTh * (sorter.size()==1  ? *sorter.cbegin() : *next(sorter.cbegin(),1) );	//Minimum no. features for a frame with meaningful level of activity.
																											//Second from the top for robustness. Unlikely to make a difference in practice

	//Allocate the memory for the output

	constexpr size_t iFeatureList=ViterbiRelativeSynchronisationProblemC::iFeatureList;
	constexpr size_t iCamera=ViterbiRelativeSynchronisationProblemC::iCamera;
	constexpr size_t iTrackId=ViterbiRelativeSynchronisationProblemC::iTrackId;
	size_t nFrames=featureAcc.crbegin()->first+1;

	dynamic_bitset<> activityFlags(nFrames);	//Frames with an activity are marked as true

	get<iObservation>(output)=vector<ImageT>(nFrames);
	get<iForward>(output)=vector<TranslationListT>(nFrames);
	get<iBackward>(output)=vector<TranslationListT>(nFrames);

	typedef typename ViterbiRelativeSynchronisationProblemC::track_map_type TrackMapT;
	for(size_t c=0; c<nFrames; ++c)
		get<iTrackId>(get<iObservation>(output)[c])=TrackMapT();

	for(const auto& current : featureAcc)
	{

		if(current.second<minFeatureCount)
			continue;

		get<iFeatureList>(get<iObservation>(output)[current.first]).reserve(current.second);
		get<iForward>(output)[current.first].reserve(current.second);
		get<iBackward>(output)[current.first].reserve(current.second);

		activityFlags[current.first]=true;
	}	//for(const auto& current : featureAcc)

	//Build the output structure

	//Cameras
	for(size_t c=0; c<nFrames; ++c)
		get<iCamera>(get<iObservation>(output)[c])=(flagFixed ? cameraList[0] : cameraList[c]);

	//Features and displacement vectors
	unsigned int iTrack=0;	//Track id
	for(const auto& currentTrajectory : trajectoryList)
	{
		auto itB=currentTrajectory.cbegin();
		auto itE=currentTrajectory.cend();
		for(auto it=itB; it!=itE; advance(it,1))
		{
			if(!activityFlags[it->first])	//Check the activity condition
				continue;

			get<iFeatureList>(get<iObservation>(output)[it->first]).push_back(it->second);
			size_t iFeature=get<iFeatureList>(get<iObservation>(output)[it->first]).size()-1;
			get<iTrackId>(get<iObservation>(output)[it->first])->push_back(typename TrackMapT::value_type(iFeature, iTrack) );	//Feature index and track id

			//Backward translation vector
			if(it!=itB)
				get<iBackward>(output)[it->first].push_back(prev(it,1)->second.Coordinate()-it->second.Coordinate());
			else	//No translation for the first point
				get<iBackward>(output)[it->first].push_back(Coordinate2DT(0,0));

			//Forward translation vector
			if(next(it,1)!=itE)
				get<iForward>(output)[it->first].push_back(next(it,1)->second.Coordinate()-it->second.Coordinate());
			else
				get<iForward>(output)[it->first].push_back(Coordinate2DT(0,0));
		}	//for(auto it=itB; it!=itE; advance(it,1))

		++iTrack;
	}	//for(const auto& currentTrajectory : trajectoryList)

	return output;
}	//auto MakeImageSequence(const ImageFeatureTrajectoryT& trajectoryList, const vector<CameraMatrixT>& cameraList) -> ImageSequenceT

}	//SynchronisationN
}	//SeeSawN



#endif /* RELATIVESYNCHRONISATION_IPP_ */
