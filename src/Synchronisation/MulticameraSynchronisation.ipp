/**
 * @file MulticameraSynchronisation.ipp Implementation of \c MulticameraSynchronisationC
 * @author Evren Imre
 * @date 29 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTICAMERA_SYNCHRONISATION_IPP_2858102
#define MULTICAMERA_SYNCHRONISATION_IPP_2858102

#include <boost/numeric/interval.hpp>
#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/lexical_cast.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/random_spanning_tree.hpp>
#include <boost/property_map/vector_property_map.hpp>
#include <boost/format.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <omp.h>
#include <vector>
#include <tuple>
#include <cstddef>
#include <map>
#include <set>
#include <iterator>
#include <algorithm>
#include <string>
#include <stdexcept>
#include <cmath>
#include <random>
#include <climits>
#include <functional>
#include <iostream>
#include "RelativeSynchronisation.h"
#include "../StateEstimation/ViterbiRelativeSynchronisationProblem.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Geometry/Rotation.h"

namespace SeeSawN
{
namespace SynchronisationN
{

using boost::optional;
using boost::numeric::interval;
using boost::numeric::in;
using boost::range::copy;
using boost::range::count_if;
using boost::range::iota;
using boost::transform;
using boost::for_each;
using boost::accumulate;
using boost::irange;
using boost::max_element;
using boost::adaptors::map_values;
using boost::lexical_cast;
using boost::dynamic_bitset;
using boost::random_spanning_tree;
using boost::adjacency_matrix;
using boost::edge;
using boost::undirectedS;
using boost::vecS;
using boost::add_edge;
using boost::connected_components;
using boost::graph_traits;
using boost::vertex_index_t;
using boost::edge_index_t;
using boost::edge_index;
using boost::put;
using boost::vector_property_map;
using boost::associative_property_map;
using boost::property_map;
using boost::property;
using boost::no_property;
using boost::format;
using boost::str;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::JacobiSVD;
using Eigen::Array;
using std::back_inserter;
using std::copy_if;
using std::set;
using std::vector;
using std::map;
using std::tuple;
using std::get;
using std::make_tuple;
using std::tie;
using std::size_t;
using std::all_of;
using std::any_of;
using std::invalid_argument;
using std::string;
using std::sqrt;
using std::round;
using std::isinf;
using std::mt19937_64;
using std::numeric_limits;
using std::greater;
using std::cout;
using SeeSawN::SynchronisationN::RelativeSynchronisationC;
using SeeSawN::SynchronisationN::RelativeSynchronisationDiagnosticsC;
using SeeSawN::SynchronisationN::RelativeSynchronisationParametersC;
using SeeSawN::StateEstimationN::ViterbiRelativeSynchronisationProblemC;
using SeeSawN::ElementsN::MakeLine2D;
using SeeSawN::ElementsN::Line2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;

//NOTES: Differences from the 1st generation
//Alpha equation systems do not constraint the reference sequence to 1. Therefore, the equation system is homogeneous, and solved via SVD. Since the system is homogeneous, no weighting
//Tau is estimated by using the original pairwise alpha estimates, not the absolute alpha. Independence
//Tau: offset for the reference sequence is 0. Extra constraint keeps the minimal set size smaller
//TODO Do we truly need weighted solvers?
//TODO Max focal length ratio could be constrained as well (maxScaleDifference). However, for cameras with different frame sizes, this does not work. Maybe some form of normalised focal length ratios (i.e. focalLength/image diagonal)
//TESTME
/**
 * @brief Parameters object for \c MulticameraSynchronisationC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct MulticameraSynchonisationParametersC
{
	unsigned int nThreads;	///< Number of threads available to the algorithm

	optional<double> maxAttitudeDifference=0.7;	///< For a pair of static cameras, if the attitude difference is above this value, the relative synchronisation step is skipped. For projective cameras, should be left uninitialised or set to infinity. In radians, >0. 1.25 is a sensible value
	RelativeSynchronisationParametersC relativeSynchronisationParameters;	///< Default parameters for relative synchronisation

	int seed;	///< Random number generator seed
	unsigned int nHypothesis;	///< Number of absolute synchronisation hypotheses to be instantiated
	double measurementInlierTh;	///< A pairwise synchronisation measurement is an inlier to an absolute synchronisation hypothesis, if the percentage of the support set that is compatible with the prediction is above this threshold [0,1]

	//Validation
	double alphaTolerance;	///< Effective minimum tolerance (range/2) of alpha for absolute synchronisation. Prevents the rejection of a good hypothesis due to a small error. In the same scale as \c referenceFrameRage. >0
	double tauTolerance;	///< Effective minimum tolerance (range/2) of tau for absolute synchronisation. Prevents the rejection of a good hypothesis due to a small error. >0

	//Reference
	unsigned int referenceSequence;	///< Index of the sequence which defines the reference timeline (frame rate=1, temporal offset=0). <\# sequences
	double referenceFrameRate;	///< Actual frame rate of the reference sequence. Acts as a scaling factor to the solution. >0

	bool flagVerbose;	///< If \c true , verbose operation

	/** @brief Default constructor */
	MulticameraSynchonisationParametersC() : nThreads(1), seed(0), nHypothesis(10000), measurementInlierTh(0.8), alphaTolerance(1e-3), tauTolerance(5e-3), referenceSequence(0), referenceFrameRate(25), flagVerbose(false)
	{}

};	//class MulticameraSynchonisationParametersC

/**
 * @brief Diagnostics object for \c MulticameraSynchronisationC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct MulticameraSynchonisationDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully
	double score;	///< Fitness score for the result

	MulticameraSynchonisationDiagnosticsC() : flagSuccess(false), score(-1)
	{}
};	//class MulticameraSynchonisationDiagnosticsC

/**
 * @brief Estimates the synchronsiation parameters for a multicamera setup
 * @remarks Usage notes
 * 	- Viewpoint difference test: In the case of metric camera matrices, if the attitude difference between a pair of cameras is too high, no relative synchronisation is estimated.
 * @ingroup Algorithm
 * @nosubgrouping
 */
class MulticameraSynchonisationC
{
	private:

		typedef ViterbiRelativeSynchronisationProblemC::interval_type RealIntervalT;	///< A real interval

	public:

		/** @name Initial estimate definition */ //@{
		typedef tuple<optional<RealIntervalT>, optional<RealIntervalT>, optional<vector<double> > > initial_estimate_type;	///< Initial synchronisation estimate type
		static constexpr size_t iAlphaRange=0;	///< Index of the component for alpha
		static constexpr size_t iTauRange=1;	///< Index of the component for tau
		static constexpr size_t iAdmissibleAlpha=2;	///< Index of the component indicating the admissible values of alpha
		//@}

	private:

		typedef typename ViterbiRelativeSynchronisationProblemC::measurement_type ObservationT;	///< An image observation
		typedef RelativeSynchronisationC::image_sequence_type ImageSequenceT;	///< An image sequence
		static constexpr size_t iObservation=RelativeSynchronisationC::iObservation;	///< Index of the observation component

		typedef RelativeSynchronisationC::result_type RelativeSynchronisationT;	///< A relative synchronisation
		static constexpr size_t iAlpha=RelativeSynchronisationC::iAlpha;	///< Index of the component for the frame rate
		static constexpr size_t iTau=RelativeSynchronisationC::iTau;	///< Index of the component for the temporal offset

		typedef RelativeSynchronisationC::index_correspondence_list_type IndexCorrespondenceListT;	///< A list of index correspondences
		typedef RelativeSynchronisationC::model_type ModelT;	///< A broken-line model
		typedef RelativeSynchronisationC::segment_type SegmentT;	///< A line segment
		static constexpr size_t iLine=RelativeSynchronisationC::iLine;	///< Index of the line segment component
		static constexpr size_t iSupport= RelativeSynchronisationC::iSupport;	///< Index of the support component

		typedef RelativeSynchronisationC::frame_drop_type FrameDropT;	///< A frame drop event
		static constexpr size_t iDropped=RelativeSynchronisationC::iDropped;	///< Index of the component indicating the number of dropped frames
		static constexpr size_t iInterval=RelativeSynchronisationC::iInterval;	///< Index of the component indicating the frame-drop interval

		/** @name Drop signature */ //@{
		typedef tuple<unsigned int, unsigned int> DropIdentifierT;	///< Identifier for a drop event
		static constexpr size_t iLost=0;	///< Index of the number of lost frames
		static constexpr size_t iRank=1;	///< Index of the rank of the drop event in the timeline- in the case of multiple drops
		//@}

		typedef initial_estimate_type InitialEstimateT;	///< An initial estimate

		typedef tuple<size_t, size_t> IndexPairT;	///< An index pair
		typedef tuple<double, double> RealPairT;	///< A pair of real values
		typedef dynamic_bitset<> IndicatorArrayT;	///< A binary indicator array
		typedef Line2DT::VectorType CoordinateT;	///< A coordinate on a synchronisation line


		/** @name Task definition */ //@{
		typedef tuple<double, IndexPairT, RelativeSynchronisationParametersC> TaskT;	///< A task
		static constexpr size_t iComplexity=0;	///< Index of the complexity factor component
		static constexpr size_t iPairId=1;	///< Index of the pair id component
		static constexpr size_t iParameter=2;	///< Index of the parameter component
		//@}

		/** @name Pairwise measurement definition */ //@{
		typedef tuple<IndexPairT, double, RelativeSynchronisationT, ModelT> PairwiseMeasurementT;	///< The output of a relative calibration task
		static constexpr size_t iId=0;	///< Index of the pair Id component
		static constexpr size_t iScore=1;	///< Index of the score component
		static constexpr size_t iSynchronisation=2;		///< Index of the synchronisation component
		static constexpr size_t iModel=3;	///< Index of the model component
		//@}

		typedef adjacency_matrix<undirectedS, no_property, property<edge_index_t, size_t> > GraphT;	///< Synchronisation graph type

		/** @name Implementation details */ //@{
		static void ValidateParameters(MulticameraSynchonisationParametersC& parameters, const vector<ImageSequenceT>& sequenceList, const map<unsigned int, InitialEstimateT>& initialEstimates);	///< Validates the input parameters

		template<class ValueT> static tuple<ValueT, ValueT> MakeRelativeSynchronisation(const ValueT& alpha1, const ValueT& tau1, const ValueT& alpha2, const ValueT& tau2);	///< Computes the relative synchronisation between two sequences

		static InitialEstimateT MakeRelativeInitialEstimate(const InitialEstimateT& sequence1, const InitialEstimateT& sequence2);	///< Makes an initial relative synchronisation estimate
		static vector<TaskT> MakeTasks(const vector<ImageSequenceT>& sequenceList, const map<unsigned int, InitialEstimateT>& initialEstimates, const MulticameraSynchonisationParametersC& parameters);	///< Prepares the relative synchronisation tasks

		static tuple<vector<PairwiseMeasurementT>, map<unsigned int, InitialEstimateT>, vector<size_t> > RemoveUnconnected(const vector<PairwiseMeasurementT>& measurements, const map<unsigned int, InitialEstimateT>& initialEstimates, unsigned int nSequences, MulticameraSynchonisationParametersC& parameters);	///< Removes the cameras that cannot be synchronised wrt the reference camera (i.e. missing the necessary pairwise measurements)

		static VectorXd QuantiseAlpha(const VectorXd& alphaList, const map<unsigned int, InitialEstimateT>& initialEstimates);	///< Quantises the estimated alpha to the admissible value
		static optional<VectorXd> SolveAlpha(const vector<PairwiseMeasurementT>& constraints, const vector<size_t>& indexList, unsigned int nSequences);	///< Estimates the absolute frame rate values from a set of relative frame rates
		static optional<VectorXd> SolveTau(const vector<PairwiseMeasurementT>& constraints,  const vector<size_t>& indexList,  unsigned int nSequences, const MulticameraSynchonisationParametersC& parameters);	///< Estimates the absolute frame rate values from a set of relative frame rates
		static bool ValidateHypothesis(const VectorXd& absoluteAlpha, const VectorXd& absoluteTau, const map<unsigned int, InitialEstimateT>& initialEstimates, const MulticameraSynchonisationParametersC& parameters);	///< Validates a synchronisation hypothesis
		static tuple<double, IndicatorArrayT> EvaluateSynchronisation(const VectorXd& absoluteAlpha, const VectorXd& absoluteTau, const vector<PairwiseMeasurementT>& measurements, const MulticameraSynchonisationParametersC& parameters);	///< Evaluates an absolute synchronisation hypothesis
		static tuple<vector<RealPairT>, double, IndicatorArrayT> Synchronise(const vector<PairwiseMeasurementT> measurements, map<unsigned int, InitialEstimateT> initialEstimates, unsigned int nSequences, const MulticameraSynchonisationParametersC& parameters);	///< Computes the absolute synchronisation from the pairwise measurements

		static void InsertDropHypothesis(map<DropIdentifierT, double>& dst, const DropIdentifierT& identifier, double score);	///< Inserts a drop hypothesis at a time instant
		static void ProcessDropList(vector<map<DropIdentifierT, double> >& timeline, const vector<FrameDropT>& dropList, double score);	///< Processes a drop list
		static vector<FrameDropT> ParseTimeline(const vector<DropIdentifierT>& timeline);	///< Extracts the frame drop events from a timeline
		static map<unsigned int, vector<FrameDropT> > IdentifyFrameDrops(const vector<PairwiseMeasurementT>& measurements, const vector<size_t>& sequenceLengths, const MulticameraSynchonisationParametersC& parameters);	///< Fuses the pairwise frame drop measurements
		//@}

	public:

		/**@name Result type */ //@{
		typedef vector<optional<tuple<double, double, vector<FrameDropT> > > > result_type;	// Type of the result
		static constexpr size_t iFrameRate=0;	///< Index of the frame rate
		static constexpr size_t iTemporalOffset=1;	///< Index of the temporal offset
		static constexpr size_t iDropList=2;	///< Index of the frame drop list
		//@}

		/** @name Operations */ //@{
		static MulticameraSynchonisationDiagnosticsC Run(result_type& synchronisation, const vector<ImageSequenceT>& sequenceList, const map<unsigned int, InitialEstimateT>& initialEstimates, MulticameraSynchonisationParametersC parameters);	///< Runs the synchronisation algorithm

		//@}
};	//class MulticameraSynchonisationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Computes the relative synchronisation between two sequences
 * @tparam ValueT Value type
 * @param[in] alpha1 Frame rate for the first sequence
 * @param[in] tau1 Temporal offset for the first sequence
 * @param[in] alpha2 Frame rate for the second sequence
 * @param[in] tau2 Temporal offset for the second sequence
 * @return A tuple: [alpha, tau]
 * @pre \c ValueT is floating-point and supports the basic arithmetic operations (unenforced)
 * @pre \c alpha1 does not contain 0 (unenforced)
 * @remarks The function can operate over real values, or intervals
 */
template<class ValueT>
auto MulticameraSynchonisationC::MakeRelativeSynchronisation(const ValueT& alpha1, const ValueT& tau1, const ValueT& alpha2, const ValueT& tau2) -> tuple<ValueT, ValueT>
{
	ValueT alpha=alpha2/alpha1;
	ValueT tau = tau2 - alpha * tau1;

	return make_tuple(alpha, tau);
}	//tuple<ValueT, ValueT> MakeRelativeSynchronisation(const ValueT& alpha1, const ValueT& tau1, const ValueT& alpha2, const ValueT& tau2)


}	//SynchronisationN
}	//SeeSawN


#endif /* MULTICAMERA_SYNCHRONISATION_IPP_2858102 */
