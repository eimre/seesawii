/**
 * @file MulticameraSynchronisation.cpp Implementation of \c MulticameraCalibrationC
 * @author Evren Imre
 * @date 29 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include "../Elements/Camera.h"

#include "MulticameraSynchronisation.h"
namespace SeeSawN
{
namespace SynchronisationN
{

/**
 * @brief Validates the input parameters
 * @param[in,out] parameters Parameters to be validated
 * @param[in] sequenceList List of sequences
 * @param[in] initialEstimates List of initial estimates
 * @throws invalid_argument If there is an invalid parameter
 */
void MulticameraSynchonisationC::ValidateParameters(MulticameraSynchonisationParametersC& parameters, const vector<ImageSequenceT>& sequenceList, const map<unsigned int, InitialEstimateT>& initialEstimates)
{
	unsigned int nSequences=sequenceList.size();

	parameters.nThreads=min( max(parameters.nThreads,(unsigned int)1), (unsigned int)omp_get_max_threads());

	if(parameters.maxAttitudeDifference && *parameters.maxAttitudeDifference<=0)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.maxAttitude difference must be either uninitialised, or a positive value. Value=")+lexical_cast<string>(*parameters.maxAttitudeDifference)));

	if(nSequences<2)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: The algorithm requires at least two sequences. Value=")+lexical_cast<string>(nSequences)));

	if(parameters.referenceSequence >= nSequences)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.referenceSequence must point to a valid sequence. Value=")+lexical_cast<string>(parameters.referenceSequence)));

	if(parameters.nHypothesis==0)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.nHypothesis must be a positive value. Value=")+lexical_cast<string>(parameters.nHypothesis)));

	if(parameters.measurementInlierTh<0 || parameters.measurementInlierTh>1)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.measurementInlierTh must be in [0,1]. Value=")+lexical_cast<string>(parameters.measurementInlierTh)));

	if(parameters.referenceFrameRate<=0)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.referenceFrameRate must be a positive value. Value=")+lexical_cast<string>(parameters.referenceFrameRate)));

	if(parameters.alphaTolerance<=0)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.alphaTolerance must be a positive value. Value=")+lexical_cast<string>(parameters.alphaTolerance)));

	if(parameters.tauTolerance<=0)
		throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: parameters.tauTolerance must be a positive value. Value=")+lexical_cast<string>(parameters.tauTolerance)));

	//No empty sequences
	for(size_t c=0; c<nSequences; ++c)
		if(get<iObservation>(sequenceList[c]).empty())
			throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: Sequence ")+lexical_cast<string>(c)+string(" is empty.")));

	//Validate the initial estimates
	for(const auto& current : initialEstimates)
	{
		if(get<iAlphaRange>(current.second) && get<iAlphaRange>(current.second)->lower()<=0)
			throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters: Invalid initial estimate. Alpha must be a positive value. Value=")+lexical_cast<string>(get<iAlphaRange>(current.second)->lower())));

		if(get<iAlphaRange>(current.second) && get<iAdmissibleAlpha>(current.second))
		{
			vector<double> filteredAlpha; filteredAlpha.reserve(get<iAdmissibleAlpha>(current.second)->size());
			for(auto currentAlpha : *get<iAdmissibleAlpha>(current.second))
				if(in(currentAlpha, *get<iAlphaRange>(current.second)))
					filteredAlpha.push_back(currentAlpha);

			if(filteredAlpha.empty())
				throw(invalid_argument(string("MulticameraSynchronisationC::ValidateParameters : No valid admissible frame rate values for sequence ")+lexical_cast<string>(current.first)));
		}	//if(get<iAlphaRange>(current) && get<iAdmissibleAlpha>(current))
	}	//for(const auto current : initialEstimates)

	if(nSequences==2)
		parameters.nHypothesis=1;
}	//void ValidateParameters(MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Makes an initial relative synchronisation estimate
 * @param[in] sequence1 Initial estimate for the first sequence
 * @param[in] sequence2 Initial estimate for the second sequence
 * @return Initial estimate for the relative synchronisation
 */
auto MulticameraSynchonisationC::MakeRelativeInitialEstimate(const InitialEstimateT& sequence1, const InitialEstimateT& sequence2) -> InitialEstimateT
{
	//Initial estimate

	bool flagInitialAlpha=get<iAlphaRange>(sequence1) && get<iAlphaRange>(sequence2);	//Both sequences have an initial estimate for alpha
	bool flagInitialTau = get<iTauRange>(sequence1) && get<iTauRange>(sequence2);	//Both sequences have an initial estimate for tau

	optional<RealIntervalT> alphaRange;
	optional<RealIntervalT> tauRange;

	if(flagInitialAlpha && flagInitialTau)
	{
	//	tie(*alphaRange, *tauRange) = MakeRelativeSynchronisation(*get<iAlphaRange>(sequence1), *get<iTauRange>(sequence1), *get<iAlphaRange>(sequence2), *get<iTauRange>(sequence2));
		auto tmp = MakeRelativeSynchronisation(*get<iAlphaRange>(sequence1), *get<iTauRange>(sequence1), *get<iAlphaRange>(sequence2), *get<iTauRange>(sequence2));	//Tying directly to the optional variables does not work!
		alphaRange=get<0>(tmp);
		tauRange=get<1>(tmp);
	}

	//Admissible alpha
	optional<vector<double> > admissibleAlpha;
	bool flagAdmissible1=(bool)get<iAdmissibleAlpha>(sequence1);
	bool flagAdmissible2=(bool)get<iAdmissibleAlpha>(sequence2);
	set<double> admissibleSet;	//Admissible alpha values
	if(flagAdmissible1 && flagAdmissible2)
	{
		for(auto a1 : *get<iAdmissibleAlpha>(sequence1))
			for(auto a2 : *get<iAdmissibleAlpha>(sequence2))
			{
				double alpha=a2/a1;
				if(!alphaRange || (flagInitialAlpha && in(alpha, *alphaRange)) )
					admissibleSet.insert(alpha);
			}	//for(auto a2 : *get<iAdmissibleAlpha>(sequence2))

		admissibleAlpha=vector<double>(admissibleSet.begin(), admissibleSet.end());
	}	//if(flagAdmissible1 && flagAdmissible2)

	return InitialEstimateT(alphaRange, tauRange, admissibleAlpha);
}	//InitialEstimateT MakeRelativeInitialEstimate(const InitialEstimateT& sequence1, const InitialEstimateT& sequence2)

/**
 * @brief Prepares the relative synchronisation tasks
 * @param[in] sequenceList List of image sequences
 * @param[in] initialEstimates Initial estimates
 * @param[in] parameters Parameters
 * @return A list of tasks, ordered wrt/decreasing complexity
 */
auto MulticameraSynchonisationC::MakeTasks(const vector<ImageSequenceT>& sequenceList, const map<unsigned int, InitialEstimateT>& initialEstimates, const MulticameraSynchonisationParametersC& parameters) -> vector<TaskT>
{
	size_t nSequence=sequenceList.size();

	static const size_t iCamera=ViterbiRelativeSynchronisationProblemC::iCamera;
	static const size_t iFeatureList=ViterbiRelativeSynchronisationProblemC::iFeatureList;

	//Process sequenceList

	dynamic_bitset<> fixed(nSequence);	//If true, a camera is fixed throughout the sequence
	vector<double> featureCount(nSequence,0);	//Feature count for each sequence
	vector<optional<RotationMatrix3DT> > orientations(nSequence);
	size_t index=0;
	for(const auto& currentSqn : sequenceList)
	{
		const CameraMatrixT& firstCamera=get<iCamera>(get<iObservation>(currentSqn)[0]);
		fixed[index]=all_of(get<iObservation>(currentSqn).cbegin(), get<iObservation>(currentSqn).cend(), [&](const ObservationT& current){return get<iCamera>(current)==firstCamera;});
		featureCount[index]=accumulate(get<iObservation>(currentSqn), 0.0, [](double op1, const ObservationT& op2){return op1+get<iFeatureList>(op2).size();});

		//If fixed, decompose
		if(fixed[index])
		{
			IntrinsicCalibrationMatrixT mK;
			RotationMatrix3DT mR;
			Coordinate3DT vC;
			tie(mK, vC, mR)=DecomposeCamera(firstCamera, false);
			orientations[index]=mR;
		}	//if(fixed[index])


		++index;
	}	//for(const auto& currentSqn : sequenceList)

	//Generate the tasks
	map<double, TaskT, greater<double> > taskList;
	auto itE=initialEstimates.end();
	double totalComplexity=0;
	for(size_t c1=0; c1<nSequence; ++c1)
		for(size_t c2=c1+1; c2<nSequence; ++c2)
		{
			//Relative calibration parameters
			RelativeSynchronisationParametersC rParameters=parameters.relativeSynchronisationParameters;
			rParameters.flagFixedCamera=fixed[c1]&&fixed[c2];	//Fixed relative calibration? This does not cover the motion of two rigidly attached cameras

			//If the viewpoint difference is too high, feature matching is unlikely to work. Skip. Only for fixed cameras
			if(rParameters.flagFixedCamera && parameters.maxAttitudeDifference)
				if( RotationMatrixToRotationVector( *orientations[c2] * orientations[c1]->transpose()).norm() > *parameters.maxAttitudeDifference  )
					continue;

			double complexityFactor= featureCount[c1]*featureCount[c2];	//Complexity: Effectively, similarity/epipolar constraint is evaluated on all feature pairs

			auto it1=initialEstimates.find(c1);
			auto it2=initialEstimates.find(c2);
			if(it1!=itE && it2!=itE )
				std::tie(rParameters.alphaRange, rParameters.tauRange, rParameters.admissibleAlpha)=MakeRelativeInitialEstimate(it1->second, it2->second);

			//If no admissible alpha values, skip
			if(rParameters.admissibleAlpha && rParameters.admissibleAlpha->empty())
				continue;

			totalComplexity+=complexityFactor;
			taskList.emplace(complexityFactor, TaskT(complexityFactor, IndexPairT(c1,c2), rParameters) );
		}	//for(size_t c2=1; c2<nSequence; ++c2)

	//Thread assignment
	size_t nTasks=taskList.size();
	vector<TaskT> output; output.reserve(nTasks);

	if(nTasks<parameters.nThreads && nTasks>0)
	{
		unsigned int assigned=0;
		double t=(parameters.nThreads-nTasks)/totalComplexity;

		map<double, TaskT, greater<double> > tmp; tmp.swap(taskList);	//Temporary storage
		for(auto& current : tmp)
		{
			get<iParameter>(current.second).nThreads=1+floor(t*current.first);
			taskList.emplace(current.first/get<iParameter>(current.second).nThreads, current.second);	//Reordering with the updated costs
			assigned+=get<iParameter>(current.second).nThreads;
		}	//for(auto& current : tmp)

		//Assign the leftovers
		unsigned int nExcess=parameters.nThreads-assigned;
		for(auto& current : taskList)
			if(nExcess>0)
			{
				++get<iParameter>(current.second).nThreads;
				--nExcess;
			}
			else
				break;
	}	//if(nTasks<parameters.nThreads && nTasks>0)

	copy(taskList | map_values, back_inserter(output));

	return output;
}	//vector<TaskT> MakeTasks(const vector<ImageSequenceT>& sequenceList, const MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Quantises the estimated alpha to the admissible value
 * @param[in] alphaList Frame rate estimates
 * @param[in] initialEstimates Initial estimates
 * @return Quantised frame rate values
 */
VectorXd MulticameraSynchonisationC::QuantiseAlpha(const VectorXd& alphaList, const map<unsigned int, InitialEstimateT>& initialEstimates)
{
	VectorXd output=alphaList;

	auto itE=initialEstimates.end();
	size_t nSequence=alphaList.size();
	for(size_t c=0; c<nSequence; ++c)
	{
		//Is there an initial estimate
		auto it=initialEstimates.find(c);

		if(it==itE || !get<iAdmissibleAlpha>(it->second))
			continue;

		//Seek the best frame rate
		double bestAlpha=alphaList[c];
		double bestDistance=numeric_limits<double>::infinity();
		for(auto current : *get<iAdmissibleAlpha>(it->second))
		{
			double currentDistance=fabs(alphaList[c]-current);
			if( currentDistance < bestDistance)
			{
				bestAlpha=current;
				bestDistance=currentDistance;
			}	//if( currentDistance < bestDistance)
		}	//for(auto current : *get<iAdmissibleAlpha>(it->second))

		output[c]=bestAlpha;
	}	//for(size_t c=0; c<nSequence; ++c)

	return output;
}	//VectorXd QuantiseAlpha(const optional<VectorXd>& alphaList, const map<unsigned int, InitialEstimateT>& initialEstimates)

/**
 * @brief Estimates the absolute frame rate values from a set of relative frame rates
 * @param[in] constraints Relative synchronisation constraints
 * @param[in] indexList Indices of the constraints to be used to set up the equation system
 * @param[in] nSequences Number of sequences
 * @return Estimated absolute frame rate values. If there is no unique solution (underconstrained system), invalid
 * @pre All elements of \c indexList point to valid elements in \c constraints (unenforced)
 * @remarks \c Ax=0 via SVD
 */
optional<VectorXd> MulticameraSynchonisationC::SolveAlpha(const vector<PairwiseMeasurementT>& constraints,  const vector<size_t>& indexList, unsigned int nSequences)
{
	optional<VectorXd> output;

	size_t nEq=indexList.size();

	//Underconstrained?
	if(nEq<nSequences-1)
		return output;

	//Set up the equation system
	MatrixXd mA; mA.setZero(nEq, nSequences);
	size_t iEq=0;
	for(auto c : indexList)
	{
		mA(iEq, get<1>(get<iId>(constraints[c])) )= 1;
		mA(iEq, get<0>(get<iId>(constraints[c])) )=-get<iAlpha>(get<iSynchronisation>(constraints[c]));
		++iEq;
	}	//for(size_t c=0; c<nEq; ++c)

	//If not minimal, weighted LS
	if(nSequences<=nEq)
	{
		size_t iEq2=0;
		for(auto c : indexList)
		{
			double w=sqrt(get<iScore>(constraints[c]));
			mA(iEq2, get<1>(get<iId>(constraints[c])) ) *= w;
			mA(iEq2, get<0>(get<iId>(constraints[c])) ) *= w;
			++iEq2;
		}	//for(auto c : indexList)
	}	//if(nSequences<=nEq)

	//SVD solution

	JacobiSVD<MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> svd(mA, Eigen::ComputeFullU | Eigen::ComputeFullV);

	//mA full rank?
	if(svd.nonzeroSingularValues() == min((unsigned int)nEq, nSequences))
		output=svd.matrixV().rightCols(1);

	return output;
}	//VectorXd SolveAlpha(const vector<RelativeSynchronisationT>& relativeSynchronisations, bool flagMinimal)

/**
 * @brief Estimates the absolute frame rate values from a set of relative frame rates
 * @param[in] constraints Relative synchronisation constraints
 * @param[in] indexList Indices of the constraints to be used to set up the equation system
 * @param[in] nSequences Number of sequences
 * @param[in] parameters Parameters
 * @return Estimated absolute frame rate values. If there is no unique solution (underconstrained system), invalid
 * @pre All elements of \c indexList point to valid elements in \c constraints (unenforced)
 * @remarks In order to keep the size of the minimal set equal to that of the frame rate, the temporal offset for the reference sequence is set to 0
 */
optional<VectorXd> MulticameraSynchonisationC::SolveTau(const vector<PairwiseMeasurementT>& constraints,  const vector<size_t>& indexList,  unsigned int nSequences, const MulticameraSynchonisationParametersC& parameters)
{
	optional<VectorXd> output;

	size_t nEq=indexList.size();

	//Underconstrained?
	if(nEq<nSequences-1)
		return output;

	//Set up the equation system
	MatrixXd mA; mA.setZero(nEq, nSequences-1);
	VectorXd vb; vb.setZero(nEq);
	size_t iEq=0;
	for(auto c : indexList)
	{
		size_t i1,i2;
		tie(i1,i2)=get<iId>(constraints[c]);

		if(i2!=parameters.referenceSequence)
			mA(iEq, (i2<parameters.referenceSequence) ? i2:i2-1 )=1;

		if(i1!=parameters.referenceSequence)
			mA(iEq, (i1<parameters.referenceSequence) ? i1:i1-1 )=-get<iAlpha>(get<iSynchronisation>(constraints[c]));;

		vb(iEq)= get<iTau>(get<iSynchronisation>(constraints[c]));
		++iEq;
	}	//for(size_t c=0; c<nEq; ++c)

	//Minimal solution
	VectorXd sol;
	if(nSequences>nEq)
		sol=mA.fullPivLu().solve(vb);
	else	//Nonminimal solution
	{
		//Weights
		size_t iEq2=0;
		for(auto c : indexList)
		{
			size_t i1,i2;
			tie(i1,i2)=get<iId>(constraints[c]);

			double w=sqrt(get<iScore>(constraints[c]));

			if(i2!=parameters.referenceSequence)
				mA(iEq2, (i2<parameters.referenceSequence) ? i2:i2-1 ) *= w;

			if(i1!=parameters.referenceSequence)
				mA(iEq2, (i1<parameters.referenceSequence) ? i1:i1-1 ) *= w;

			vb(iEq2) *= w;
			++iEq2;
		}	//for(auto c : indexList)

		//Least-squares solution via SVD
		sol=mA.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(vb);
	}	//if(nSequences>nEq)

	//Output
	output=VectorXd(nSequences);
	output->head(parameters.referenceSequence)=sol.head(parameters.referenceSequence);
	(*output)[parameters.referenceSequence]=0;
	output->tail(nSequences-parameters.referenceSequence-1)=sol.tail(nSequences-parameters.referenceSequence-1);

	return output;
}	//optional<VectorXd> SolveTau(const vector<PairwiseMeasurementT>& constraints,  const vector<size_t>& indexList,  unsigned int nSequences, const MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Evaluates an absolute synchronisation hypothesis
 * @param[in] absoluteAlpha Frame rates
 * @param[in] absoluteTau Temporal offsets
 * @param[in] measurements Pairwise measurements
 * @param[in] parameters Parameters
 * @return Score for the absolute synchronisation hypothesis
 * @pre \c absoluteAlpha and \c absoluteTau have the same number of elements
 * @pre For each sequence represented in \c measurements , there is an element in \c absoluteAlpha and \c absoluteTau (unenforced)
 */
auto MulticameraSynchonisationC::EvaluateSynchronisation(const VectorXd& absoluteAlpha, const VectorXd& absoluteTau, const vector<PairwiseMeasurementT>& measurements, const MulticameraSynchonisationParametersC& parameters) -> tuple<double, IndicatorArrayT>
{
	//Preconditions
	assert(absoluteAlpha.size()==absoluteTau.size());

	size_t nSequence=absoluteAlpha.size();

	double score=0;
	IndicatorArrayT inlierFlags(nSequence);

	Line2DT defaultLine=MakeLine2D(1,0,0);	//To suppress the compiler warnings, synchLine needs a default value

	//Iterate over the measurements
	size_t index=0;
	for(const auto& current : measurements)
	{
		size_t i1,i2;
		tie(i1,i2)=get<iId>(current);

		double alpha;
		double tau;
		tie(alpha, tau)=MakeRelativeSynchronisation(absoluteAlpha[i1], absoluteTau[i1], absoluteAlpha[i2], absoluteTau[i2]);

		double deltaAlpha=alpha-get<iAlpha>(get<iSynchronisation>(current));
		double deltaTau=tau-get<iTau>(get<iSynchronisation>(current));

		//Count the inliers. This requires rebuilding all segments of the broken line model
		unsigned int nInlier=0;
		unsigned int nTotal=0;
		double prevTau=tau;
		for(const auto& currentSegment : get<iModel>(current))
		{
			//Update the segment
			double currentTau=get<iLine>(currentSegment).coeffs()[2];
			Line2DT synchLine=defaultLine;

			double offset=currentTau;	//Tau
			offset+= (currentTau==prevTau) ? 0 : ( (currentTau<prevTau) ? deltaTau : deltaTau + (currentTau-prevTau)*deltaAlpha/alpha );	//No frame drop; drop in the target; drop in the reference

			synchLine=MakeLine2D(alpha, -1, offset);

			//Support set
			typedef IndexCorrespondenceListT::value_type ItemT;
			nInlier+=count_if(get<iSupport>(currentSegment), [&](const ItemT& currentPoint){ return synchLine.absDistance(CoordinateT(currentPoint.left, currentPoint.right)) < parameters.relativeSynchronisationParameters.segmentInlierTh;} );

			nTotal+=get<iSupport>(currentSegment).size();
			prevTau=currentTau;
		}	//for(const auto& currentSegment : get<iModel>(current))

		//Is the measurement an inlier to the hypothesis?
		if((double)nInlier/nTotal >= parameters.measurementInlierTh)
			inlierFlags[index]=true;

		score+=nInlier;	//Update the score
		++index;
	}	//for(const auto& current : measurements)

	return make_tuple(score, inlierFlags);
}	//double EvaluateSynchronisation(const VectorXd& absoluteAlpha, const VectorXd& absoluteTau, const vector<PairwiseMeasurementT>& measurements)

/**
 * @brief Validates a synchronisation hypothesis
 * @param[in] absoluteAlpha Frame rate estimates. Scaled!
 * @param[in] absoluteTau Temporal offset estimates
 * @param[in] initialEstimates Initial estimates
 * @param[in] parameters Parameters
 * @return \c false if any parameters have invalid values or out of range
 * @pre \c absoluteAlpha and \c absoluteTau have the same number of elements
*/
bool MulticameraSynchonisationC::ValidateHypothesis(const VectorXd& absoluteAlpha, const VectorXd& absoluteTau, const map<unsigned int, InitialEstimateT>& initialEstimates, const MulticameraSynchonisationParametersC& parameters)
{
	//Preconditions
	assert(absoluteAlpha.size()==absoluteTau.size());

	bool flagSuccess=true;
	size_t nSequences=absoluteAlpha.size();
	auto itE=initialEstimates.cend();

	//Iterate over the sequences and validate
	for(size_t c=0; (c<nSequences) && flagSuccess ; ++c)
	{
		flagSuccess = flagSuccess && (absoluteAlpha[c]>0) && !isinf(absoluteAlpha[c]);

		auto it=initialEstimates.find(c);
		if(it!=itE)
		{
			flagSuccess = flagSuccess && in(absoluteAlpha[c], *get<iAlphaRange>(it->second));
			flagSuccess = flagSuccess && in(absoluteTau[c], *get<iTauRange>(it->second));
		}	//if(it!=itE)

	}	//for(size_t c=0; c<nSequences; ++c);

	return flagSuccess;
}	//bool ValidateHypothesis(const VectorXd& absoluteAlpha, const VectorXd& absoluteTau, const map<unsigned int, InitialEstimateT>& initialEstimates, const MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Removes the cameras that cannot be synchronised wrt the reference camera (i.e. missing the necessary pairwise measurements)
 * @param[in] measurements Pairwise measurements
 * @param[in] initialEstimates Initial estimates
 * @param[in] nSequences Number of sequences
 * @param[in, out] parameters Parameters
 * @return A tuple: Filtered measurement list and initial estimates; the mapping between the new and the original sequence indices
 * @remarks Adjusts the index of the reference sequence
 */
auto MulticameraSynchonisationC::RemoveUnconnected(const vector<PairwiseMeasurementT>& measurements, const map<unsigned int, InitialEstimateT>& initialEstimates, unsigned int nSequences, MulticameraSynchonisationParametersC& parameters) -> tuple<vector<PairwiseMeasurementT>, map<unsigned int, InitialEstimateT>, vector<size_t> >
{
	//Build the synchronsiation graph
	GraphT synchronisationGraph(nSequences);
	for_each(measurements, [&](const PairwiseMeasurementT& current){ add_edge( get<0>(get<iId>(current)), get<1>(get<iId>(current)), synchronisationGraph);} );

	//Find the connected components
	vector_property_map<size_t> vertexToClusterMap(nSequences);
	size_t nComponents=connected_components(synchronisationGraph, vertexToClusterMap);

	//Single component? Everything is connected
	if(nComponents==1)
	{
		vector<size_t> indexMap(nSequences);
		iota(indexMap,0);
		return make_tuple(measurements, initialEstimates, indexMap);
	}	//if(nComponents==1)

	//Remove the sequences not connected to the reference

	size_t referenceCluster=vertexToClusterMap[parameters.referenceSequence];

	vector<size_t> indexMap; indexMap.reserve(nSequences);	//New->old index
	vector<int> reverseIndexMap(nSequences,-1);	//Old->new index
	size_t cConnected=0;
	for(size_t c=0; c<nSequences; ++c)
		if(vertexToClusterMap[c]==referenceCluster)
		{
			indexMap.push_back(c);
			reverseIndexMap[c]=cConnected;
			++cConnected;
		}	//if(vertexToClusterMap[c]==referenceCluster)

	indexMap.shrink_to_fit();

	//Filter the initial estimates
	map<unsigned int, InitialEstimateT> filteredInitialEstimates;
	for(const auto& current : initialEstimates)
		if(vertexToClusterMap[current.first]==referenceCluster)
			filteredInitialEstimates.emplace(reverseIndexMap[current.first], current.second);

	//Filter the pairwise measurements
	vector<PairwiseMeasurementT> filtered; filtered.reserve(measurements.size());
	for(const auto& current : measurements)
	{
		size_t i1,i2;
		tie(i1,i2)=get<iId>(current);

		if(vertexToClusterMap[i1]==referenceCluster && vertexToClusterMap[i2]==referenceCluster)
			filtered.emplace_back(make_tuple(reverseIndexMap[i1], reverseIndexMap[i2]), get<1>(current), get<2>(current), get<3>(current));
	}	//for(const auto& current : measurements)

	parameters.referenceSequence=reverseIndexMap[parameters.referenceSequence];

	return make_tuple(filtered, filteredInitialEstimates, indexMap);
}	//tuple<vector<PairwiseMeasurementT>, vector<size_t> > RemoveUnconnected(const vector<PairwiseMeasurementT>& measurements, const MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Computes the absolute synchronisation from the pairwise measurements
 * @param[in] measurements Pairwise synchronisation measurements
 * @param[in] initialEstimates Initial estimates for absolute synchronisation. Pass-by-value on purpose
 * @param[in] nSequences Number of sequences
 * @param[in] parameters Parameters
 * @return A tuple: A vector of 3-tuples; alpha, tau for each sequence. Hypothesis score. Indicator array for the inlying pairwise measurements
 */
auto MulticameraSynchonisationC::Synchronise(const vector<PairwiseMeasurementT> measurements, map<unsigned int, InitialEstimateT> initialEstimates, unsigned int nSequences, const MulticameraSynchonisationParametersC& parameters) -> tuple<vector<RealPairT>, double, IndicatorArrayT>
{
	//Build the graph
	size_t nMeasurements=measurements.size();
	GraphT graph(nSequences);	//Graph
	map<graph_traits<GraphT>::edge_descriptor, double> edgeWeightsContainer;	//Underlying container
	associative_property_map<map<graph_traits<GraphT>::edge_descriptor, double> > edgeWeights(edgeWeightsContainer);	//Weights

	for(size_t c=0; c<nMeasurements; ++c)
	{
		 auto desc=add_edge(get<0>(get<iId>(measurements[c])), get<1>(get<iId>(measurements[c])), c, graph);	//Always succeeds
		 put(edgeWeights, desc.first, get<iScore>(measurements[c]));
	}	//for(size_t c=0; c<nMeasurements; ++c)

	property_map<GraphT, edge_index_t>::type edgeDescriptorToIndexMap=get(edge_index, graph);

	//Effective admissible ranges
	for(auto& current : initialEstimates)
	{
		optional<RealIntervalT>& alphaRange=get<iAlphaRange>(current.second);
		if(alphaRange && (0.5*(alphaRange->upper() - alphaRange->lower()) < parameters.alphaTolerance))
		{
			double midPoint=0.5*(alphaRange->upper() + alphaRange->lower());
			double lower=max(0.0, midPoint-parameters.alphaTolerance);
			double upper= midPoint + parameters.alphaTolerance + alphaRange->lower() + ( (lower==0) ? parameters.alphaTolerance-midPoint : 0);
			get<iAlphaRange>(current.second)=RealIntervalT(lower, upper);
		}	//if(alphaRange && (0.5*(alphaRange->upper() - alphaRange->lower()) < parameters.alphaTolerance))

		optional<RealIntervalT>& tauRange=get<iTauRange>(current.second);
		if(tauRange && (0.5*(tauRange->upper() - tauRange->lower()) < parameters.alphaTolerance))
		{
			double midPoint=0.5*(tauRange->upper() + tauRange->lower());
			get<iTauRange>(current.second)=RealIntervalT(midPoint-parameters.tauTolerance,  midPoint+parameters.tauTolerance);
		}	//if(tauRange && (0.5*(tauRange->upper() - tauRange->lower()) < parameters.alphaTolerance))
	}	//for(auto& current : initialEstimates)

	//Random sampling

	set<IndicatorArrayT> treeCache;	// Cache for sampled trees. Although checking a cache of bitset is potentially an expensive operation, still it will be cheaper than actually solving for synchronisation
										// In practice, a full graph of uniform weights is very unlikely, so the max size of the cache will be low

	//State
	double bestScore=-1;
	IndicatorArrayT bestInliers;
	VectorXd bestAlpha;
	VectorXd bestTau;
	bool flagSolution=false;	//If a valid solution exists, true

	mt19937_64 rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	size_t c=0;
	vector<size_t> sample(nSequences-1);
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) firstprivate(sample) num_threads(parameters.nThreads)
	for(c=0; c<parameters.nHypothesis; ++c)
	{
		//Instantiate a random spanning tree
		vector_property_map<graph_traits<GraphT>::vertex_descriptor, property_map<GraphT, vertex_index_t>::const_type> spanningTree(nSequences);

	#pragma omp critical(MS_S1)
		random_spanning_tree(graph, rng, predecessor_map(spanningTree).weight_map(edgeWeights));	//rng is shared

		size_t indexConstraint=0;
		for(size_t c2=0; c2<nSequences; ++c2)
			if(spanningTree[c2]!=graph_traits<GraphT>::null_vertex())
			{
				sample[indexConstraint]=edgeDescriptorToIndexMap[edge(spanningTree[c2], c2, graph).first];	//The edge is guaranteed to exist
				++indexConstraint;
			}	//if(spanningTree[c2]!=graph_traits<GraphT>::null_vertex())

		//Already visited?
		IndicatorArrayT tree(nMeasurements);
		for_each(sample, [&](size_t current){tree[current]=true;});

		if(treeCache.find(tree)!=treeCache.end())
			continue;

	#pragma omp critical (MS_S2)
		treeCache.insert(tree);	//Push into the cache

		//Estimate the synchronisation parameters
		optional<VectorXd> alpha=SolveAlpha(measurements, sample, nSequences);

		if(!alpha)
			continue;

		optional<VectorXd> tau=SolveTau(measurements, sample, nSequences, parameters);

		if(!tau)
			continue;

		alpha = (*alpha)*( parameters.referenceFrameRate / (*alpha)[parameters.referenceSequence] );	//Scale to the frame rate of the reference sequence

		//Validate
		if(!ValidateHypothesis(*alpha, *tau, initialEstimates, parameters))
			continue;

		//Quantise to the closest admissible value
		alpha=QuantiseAlpha(*alpha, initialEstimates);

		//Evaluate
		double score;
		IndicatorArrayT inliers;
		std::tie(score, inliers)=EvaluateSynchronisation(*alpha, *tau, measurements, parameters);

		//Update best
		if(score>bestScore)
		{
		#pragma omp critical (MS_S3)
		{
			bestScore=score;
			bestInliers=inliers;
			bestAlpha=*alpha;
			bestTau=*tau;
			flagSolution=true;
		}
		}	//if(score>bestScore)
	}	//for(size_t c=0; c<parameters.nHypothesis; ++c)

	//No solution found, abort
	if(!flagSolution)
		return make_tuple(vector<RealPairT>(), 0, IndicatorArrayT());

	//Nonminimal least squares estimate
	size_t nInliers=bestInliers.count();
	vector<size_t> bestInlierIndices(nInliers);
	for_each(irange<size_t>(0, nInliers), [&](size_t c){ if(bestInliers[c]) bestInlierIndices.push_back(c);});

	optional<VectorXd> alpha2=SolveAlpha(measurements, bestInlierIndices, nSequences);
	optional<VectorXd> tau2=SolveTau(measurements, bestInlierIndices, nSequences, parameters);

	double score2=-1;
	IndicatorArrayT inliers2;
	if(alpha2 && tau2)
	{
		alpha2 = (*alpha2)*( parameters.referenceFrameRate / (*alpha2)[parameters.referenceSequence] );	//Scale to the frame rate of the reference sequence
		alpha2=QuantiseAlpha(*alpha2, initialEstimates);
		std::tie(score2, inliers2)=EvaluateSynchronisation(*alpha2, *tau2, measurements, parameters);
	}	//if(alpha2 && tau2)

	//Output
	tuple<vector<RealPairT>, double, IndicatorArrayT> output;
	get<0>(output).resize(nSequences);

	if(bestScore<score2)
		transform(irange<size_t>(0, nSequences), get<0>(output).begin(), [&](size_t c){return RealPairT( (*alpha2)[c], (*tau2)[c]); } );
	else
		transform(irange<size_t>(0, nSequences), get<0>(output).begin(), [&](size_t c){return RealPairT( bestAlpha[c], bestTau[c]); } );

	get<1>(output)=bestScore;
	get<2>(output)=bestInliers;

	return output;
}	//vector<RealPairT> Synchronise(const vector<PairwiseMeasurementT> measurements, const map<unsigned int, InitialEstimateT>& initialEstimates, unsigned int nSequences, const MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Inserts a drop hypothesis at a time instant
 * @param[in,out] dst Destination map
 * @param[in] identifier Drop identifier
 * @param[in] score Score
 */
void MulticameraSynchonisationC::InsertDropHypothesis(map<DropIdentifierT, double>& dst, const DropIdentifierT& identifier, double score)
{
	auto it=dst.emplace(identifier, score);

	if(!it.second)
		it.first->second += score;
}	//map<DropIdentifierT, double> InsertDropHypothesis(const DropIdentifierT& identifier, double score)

/**
 * @brief Processes a drop list
 * @param[in,out] timeline Timeline to be updated
 * @param[in] dropList Frame drop list to be processed
 * @param[in] score Hypothesis strength
 */
void MulticameraSynchonisationC::ProcessDropList(vector<map<DropIdentifierT, double> >& timeline, const vector<FrameDropT>& dropList, double score)
{
	size_t nDrops=dropList.size();
	for(size_t c=0; c<nDrops; ++c)
	{
		DropIdentifierT dropEvent(get<iDropped>(dropList[c]), c+1);	//rank 0 is the no-drop hypothesis

		size_t lower=round(get<iInterval>(dropList[c]).lower());
		size_t upper=round(get<iInterval>(dropList[c]).upper());

		for(size_t c2=lower; c2<=upper; ++c2)
			InsertDropHypothesis(timeline[c2], dropEvent, score);
	}	//for(size_t c=0; c<nDrops; ++c)
}	//void ProcessDropList(vector<map<DropIdentifierT, double> >& timeline, const vector<FrameDropT>& dropList, double score)

/**
 * @brief Extracts the frame drop events from a timeline
 * @param[in] timeline Timeline to be parsed
 * @return A list of frame drop events in the timeline
 */
auto MulticameraSynchonisationC::ParseTimeline(const vector<DropIdentifierT>& timeline) -> vector<FrameDropT>
{
	typedef interval<double> IntervalT;

	vector<FrameDropT> output;
	if(timeline.empty())
		return output;

	//Extract the intervals
	map<DropIdentifierT, list<IntervalT> > collector;

	size_t endIndex=timeline.size()-1;
	size_t currentLower=0;	//Lower bound of the current interval
	for(size_t c=0; c<endIndex; ++c)
		if(timeline[c]!=timeline[c+1])	//Look forward
		{
			//Bracket ends
			auto it=collector.emplace(timeline[c], list<IntervalT>());
			it.first->second.push_back(IntervalT((double)currentLower, (double)c));

			currentLower=c+1;
		}	//f(timeline[c]!=timeline[c+1])

	//The individual intervals cannot overlap by construction. However, there might be multiple intervals associated with a drop event, sometimes broken by other events
	//Currently, just unite the intervals, allowing for overlaps
	output.reserve(collector.size());
	for(const auto& current : collector)
	{
		size_t nDropped=get<iLost>(current.first);

		//Skip the no-drop
		if(nDropped==0)
			continue;

		//Unite the associated intervals

		double lower=current.second.begin()->lower();
		double upper=current.second.rbegin()->upper();

		if(upper-lower < nDropped)
			continue;

		output.emplace_back(nDropped, RealIntervalT(lower, upper));
	}	//for(const auto& current : collector)

	return output;
}	//vector<FrameDropT> ParseTimeline(const vector<DropIdentifierT>& timeline)

/**
 * @brief Fuses the pairwise frame drop measurements
 * @param[in] measurements Pairwise measurements
 * @param[in] sequenceLengths Length of each sequence
 * @param[in] parameters Parameters
 * @return A list of detected frame drop events. [Sequence; Event list]
 */
auto MulticameraSynchonisationC::IdentifyFrameDrops(const vector<PairwiseMeasurementT>& measurements, const vector<size_t>& sequenceLengths, const MulticameraSynchonisationParametersC& parameters) -> map<unsigned int, vector<FrameDropT> >
{
	//Prepare the vote data structure
	size_t nSequences=sequenceLengths.size();
	typedef vector<map<DropIdentifierT, double> > HypothesisAccumulatorT;	//Holds the drop hypotheses for each time point
	vector<HypothesisAccumulatorT> votes(nSequences);
	for_each(irange<size_t>(0, nSequences), [&](size_t c){votes[c].resize(sequenceLengths[c]);} );

	constexpr size_t iDropList1=RelativeSynchronisationC::iDropList1;
	constexpr size_t iDropList2=RelativeSynchronisationC::iDropList2;

	//Iterate through the pairwise measurements
	for(const auto& current : measurements)
	{
		size_t i1,i2;
		tie(i1,i2)=get<iId>(current);

		double score=get<iScore>(current);

		//Mark the broken line segments as no-drop
		DropIdentifierT noDrop(0,0);
		for(const auto& currentSegment : get<iModel>(current))
		{
			for_each(counting_range(round(get<iSupport>(currentSegment).begin()->left), round(get<iSupport>(currentSegment).rbegin()->left)), [&](size_t c){InsertDropHypothesis(votes[i1][c], noDrop, score);} );
			for_each(counting_range(round(get<iSupport>(currentSegment).begin()->right), round(get<iSupport>(currentSegment).rbegin()->right)), [&](size_t c){InsertDropHypothesis(votes[i2][c], noDrop, score);} );
		}	//for(const auto& currentSegment : get<iModel>(current))

		//Now, mark the drop events
		ProcessDropList(votes[i1], get<iDropList1>(get<iSynchronisation>(current)), score);
		ProcessDropList(votes[i2], get<iDropList2>(get<iSynchronisation>(current)), score);
	}	//for(const auto& current : measurements)

	//Build the timelines
	typedef vector<DropIdentifierT> TimelineT;
	map<size_t, TimelineT> timelines;
	for(size_t c=0; c<nSequences; ++c)
	{
		size_t length=votes[c].size();
		DropIdentifierT noDrop(0,0);
		TimelineT timeline=TimelineT(length, noDrop);	//Default, no drop

		bool flagDrop=false;	//true if the timeline contains a drop event
		size_t iFrame=0;
		for(const auto& current : votes[c])
		{
			if(!current.empty())
			{
				if(current.size()==1)
					timeline[iFrame]=current.begin()->first;
				else
				{
					typedef HypothesisAccumulatorT::value_type::value_type ItemT;
					timeline[iFrame]= max_element(current, [&](const ItemT& item1, const ItemT& item2){  return item1.second < item2.second; })->first;
				}	//if(current.size()==1)
			}	//if(!current.empty())

			flagDrop |= (timeline[iFrame]!=noDrop);
			++iFrame;
		}	//for(const auto& current : votes[c])

		if(flagDrop)
			timelines[c].swap(timeline);
	}	//for(size_t c=0; c<nSequences ++c)

	//Parse the timelines
	map<unsigned int, vector<FrameDropT> > output;
	for(const auto& current : timelines)
	{
		vector<FrameDropT> dropList=ParseTimeline(current.second);

		if(!dropList.empty())
			output.emplace(current.first, dropList);
	}	//for(size_t c=0; c<nSequences; ++c)

	return output;
}	//vector<FrameDropT> IdentifyFrameDrops(const vector<PairwiseMeasurementT>& measurements, const MulticameraSynchonisationParametersC& parameters)

/**
 * @brief Runs the synchronisation algorithm
 * @param[out] result Synchronisation parameters and frame drops. An invalid entry indicates a synchronisation failure for the corresponding sequence.
 * @param[in] sequenceList List of image sequences
 * @param[in] initialEstimates Initial synchronisation estimates. [Camera Id; Initial estimate]. Frame rates at the same scale as \c referenceFrameRate
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @warning Initial frame rates estimates are assumed to be at the same scale as \c referenceFrameRate
 */
MulticameraSynchonisationDiagnosticsC MulticameraSynchonisationC::Run(result_type& result, const vector<ImageSequenceT>& sequenceList, const map<unsigned int, InitialEstimateT>& initialEstimates, MulticameraSynchonisationParametersC parameters)
{
	result.clear();

	size_t nSequence=sequenceList.size();
	ValidateParameters(parameters, sequenceList, initialEstimates);

	MulticameraSynchonisationDiagnosticsC diagnostics;

	//Relative synchronisations

	vector<TaskT> taskList=MakeTasks(sequenceList, initialEstimates, parameters);	//Build the tasks

	if(parameters.flagVerbose)
		cout<<"MCS: Estimating the relative synchronisation for each camera pair...\n";

	size_t nTasks=taskList.size();
	size_t c=0;
	vector<PairwiseMeasurementT> pairwiseMeasurements; pairwiseMeasurements.reserve(nTasks);	// Pairwise measurements;
#pragma omp parallel for if(parameters.nThreads>1) schedule(dynamic) private(c) num_threads(parameters.nThreads)
	for(c=0; c<nTasks; ++c)
	{
		size_t i1;
		size_t i2;
		tie(i1,i2)=get<iPairId>(taskList[c]);

		RelativeSynchronisationT relativeSynchronisation;
		ModelT model;
		RelativeSynchronisationC synchroniser;
		RelativeSynchronisationDiagnosticsC rDiagnostics=synchroniser.Run(relativeSynchronisation, model, sequenceList[i1], sequenceList[i2], get<iParameter>(taskList[c]));

		if(rDiagnostics.flagSuccess)
		#pragma omp critical(MS_RUN1)
		{
			if(parameters.flagVerbose)
			{
				cout<<"MCS: "<<"Camera Pair "<<i1<<" "<<i2<<" Relative rate: "<<str(format("%.3f")%get<iAlpha>(relativeSynchronisation))<<" Relative offset: "<<str(format("%.3f")%get<iTau>(relativeSynchronisation));
				cout<<" #frame drops 1/2: "<<get<RelativeSynchronisationC::iDropList1>(relativeSynchronisation).size()<<"/"<<get<RelativeSynchronisationC::iDropList2>(relativeSynchronisation).size()<<" Support: "<<rDiagnostics.score<<"\n";
			}	//if(parameters.flagVerbose)

			pairwiseMeasurements.emplace_back( get<iPairId>(taskList[c]), rDiagnostics.score, relativeSynchronisation, model);
		}	//#pragma omp critical(MS_RUN1)
		else
		#pragma omp critical(MS_RUN1)
			if(parameters.flagVerbose)
				cout<<"MCS: "<<"Camera Pair "<<i1<<" "<<i2<<" Relative synchronisation failed \n";
	}	//for(c=0; c<nTasks; ++c)

	//No measurements, no synchronisation
	if(pairwiseMeasurements.empty())
		return diagnostics;

	if(parameters.flagVerbose)
		cout<<"MCS: Estimating the absolute synchronisation... \n";

	//Remove the sequences that are not connected to the reference
	vector<size_t> filteredToOriginal;	//Index map, filtered->original
	map<unsigned int, InitialEstimateT> filteredInitialEstimates;	//Initial estimates with adjusted keys
	vector<PairwiseMeasurementT> filteredMeasurements;
	std::tie(filteredMeasurements, filteredInitialEstimates, filteredToOriginal)=RemoveUnconnected(pairwiseMeasurements, initialEstimates, nSequence, parameters);

	//If no measurements are connected to the reference, quit
	if(filteredMeasurements.empty())
		return diagnostics;

	//Absolute synchronisation
	size_t nFiltered=filteredToOriginal.size();
	vector<RealPairT> absoluteSynchronisation;
	IndicatorArrayT inliers;
	std::tie(absoluteSynchronisation, diagnostics.score, inliers)=Synchronise(filteredMeasurements, filteredInitialEstimates, nFiltered, parameters);

	//Absolute synchronisation failed
	if(absoluteSynchronisation.empty())
		return diagnostics;

	//Synchronisation complete, moving to frame-drop detection

	//Filter the inlier measurements
	vector<PairwiseMeasurementT> inlierMeasurements; inlierMeasurements.reserve(filteredMeasurements.size());
	for_each(counting_range((size_t)0, filteredMeasurements.size()) | filtered([&](size_t c){return inliers[c];}), [&](size_t c){inlierMeasurements.push_back(filteredMeasurements[c]);});
	inlierMeasurements.shrink_to_fit();

	//Absolute frame drops
	bool flagFrameDrop=any_of(inlierMeasurements.begin(), inlierMeasurements.end(), [](const PairwiseMeasurementT& current){return get<iModel>(current).size()>1;});
	map<unsigned int, vector<FrameDropT> > frameDrops;
	if(flagFrameDrop)
	{
		vector<size_t> sequenceLengths(nFiltered,0);
		for_each(irange<size_t>(0,nFiltered), [&](size_t c){ sequenceLengths[c]=get<iObservation>(sequenceList[filteredToOriginal[c]]).size();} );
		frameDrops=IdentifyFrameDrops(inlierMeasurements, sequenceLengths, parameters);
	}	//if(flagFrameDrop)

	//Output
	result.resize(nSequence);
	auto itE=frameDrops.end();
	for(size_t c=0; c<nFiltered; ++c)
	{
		result[filteredToOriginal[c]]=make_tuple(get<0>(absoluteSynchronisation[c]), get<1>(absoluteSynchronisation[c]), vector<FrameDropT>());

		auto it=frameDrops.find(c);
		if(it!=itE)
			get<iDropList>(*result[filteredToOriginal[c]])=it->second;
	}	//for(size_t c=0; c<nFiltered; ++c)

	diagnostics.flagSuccess=true;

	return diagnostics;
}	//MulticameraSynchonisationDiagnosticsC Run(const vector<ImageSequenceT>& sequenceList, MulticameraSynchonisationParametersC parameters)

}	//SynchronisationN
}	//SeeSawN

