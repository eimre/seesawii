/**
 * @file RelativeSynchronisation.h Public interface for the relative synchronisation algorithm
 * @author Evren Imre
 * @date 13 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef RELATIVE_SYNCHRONISATION_H_7901901
#define RELATIVE_SYNCHRONISATION_H_7901901

#include "RelativeSynchronisation.ipp"
namespace SeeSawN
{
namespace SynchronisationN
{

class RelativeSynchronisationC;	///< Synchronisation for a pair of image sequences
struct RelativeSynchronisationParametersC;	///< Parameters for \c RelativeSynchronisationC
struct RelativeSynchronisationDiagnosticsC;	///< Diagnostics for \c RelativeSynchronisationC

/********** EXTERN TEMPLATES **********/
extern template RelativeSynchronisationC::image_sequence_type  RelativeSynchronisationC::MakeImageSequence(const vector<ImageFeatureTrajectoryT>&, const vector<CameraMatrixT>&, double);

}	//SynchronisationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
extern template class std::vector<double>;
extern template class std::list<unsigned int>;
extern template class std::set<std::size_t>;
extern template class std::multiset<std::size_t>;
extern template class std::map<std::size_t, unsigned int>;
extern template class std::tuple<double, double>;
extern template class boost::optional<double>;
extern template class std::array<double,2>;

#endif /* RELATIVE_SYNCHRONISATION_H_7901901 */
