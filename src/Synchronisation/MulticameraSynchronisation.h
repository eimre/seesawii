/**
 * @file MulticameraSynchronisation.h Public interface for \c MulticameraSynchronisationC
 * @author Evren Imre
 * @date 29 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTICAMERA_SYNCHRONISATION_H_5691023
#define MULTICAMERA_SYNCHRONISATION_H_5691023

#include "MulticameraSynchronisation.ipp"
namespace SeeSawN
{
namespace SynchronisationN
{

struct MulticameraSynchonisationParametersC;	///< Parameters object for \c MulticameraSynchronisationC
struct MulticameraSynchonisationDiagnosticsC;	///< Diagnostics object for \c MulticameraSynchronisationC
class MulticameraSynchonisationC;	///< Estimates the synchronsiation parameters for a multicamera setup

}	//SynchronisationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
extern template class std::vector<double>;
extern template class std::vector<std::size_t>;
extern template class std::tuple<double, double>;
extern template class std::array<double,2>;

#endif /* MULTICAMERA_SYNCHRONISATION_H_5691023 */
