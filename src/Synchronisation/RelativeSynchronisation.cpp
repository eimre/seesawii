/**
 * @file RelativeSynchronisation.cpp Implementation of the relative synchronisation algorithm
 * @author Evren Imre
 * @date 13 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RelativeSynchronisation.h"
namespace SeeSawN
{
namespace SynchronisationN
{

/**
 * @brief Identifies the admissible alpha values within an alpha range
 * @param[in] src Source
 * @param[in] alphaRange Alpha range
 * @return Filtered output
 */
vector<double> RelativeSynchronisationC::FilterAdmissibleAlpha(const vector<double>& src, const ParameterIntervalT& alphaRange) const
{
	vector<double> output; output.reserve(src.size());
	copy_if(src.cbegin(), src.cend(), back_inserter(output), [&](double c){return in(c, alphaRange);});
	output.shrink_to_fit();
	return output;
}	//vector<double> FilterAdmissibleAlpha(const vector<double>& src, const ParameterIntervalT& alphaRange)

/**
 * @brief Validates the input parameters
 * @param[in] sequence1 First sequence
 * @param[in] sequence2 Second sequence
 * @param[in] parameters Parameters
 * @throws invalid_argument If the parameter values are invalid
 */
void RelativeSynchronisationC::ValidateParameters(const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC& parameters) const
{
	if(get<iObservation>(sequence1).size()!=get<iForward>(sequence1).size())
		throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : Observation and forward translation components of sequence1 must have the same size."));

	if(get<iObservation>(sequence1).size()!=get<iBackward>(sequence1).size())
		throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : Observation and backward translation components of sequence1 must have the same size."));

	if(get<iObservation>(sequence2).size()!=get<iForward>(sequence2).size())
		throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : Observation and forward translation components of sequence2 must have the same size."));

	if(get<iObservation>(sequence2).size()!=get<iBackward>(sequence2).size())
		throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : Observation and backward translation components of sequence2 must have the same size."));

	if(get<iObservation>(sequence1).empty())
		throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : Sequence1 is empty."));

	if(get<iObservation>(sequence2).empty())
		throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : Sequence2 is empty."));

	if(parameters.nThreads<1)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.nThreads must be >0. Value=")+lexical_cast<string>(parameters.nThreads)) );

	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.noiseVariance must be >0. Value=")+lexical_cast<string>(parameters.noiseVariance)) );

	if(parameters.segmentInlierTh<=0)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.segmentInlierTh must be >0. Value=")+lexical_cast<string>(parameters.segmentInlierTh)) );

	if(parameters.tauQuantisationTh<=0)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.tauQuantisationTh must be >0. Value=")+lexical_cast<string>(parameters.tauQuantisationTh)) );

	if(parameters.maxOverlap<=0 && parameters.maxOverlap>1)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.maxOverlap must be in (0,1]. Value=")+lexical_cast<string>(parameters.maxOverlap) ));

	if(parameters.minSpan<=0 && parameters.minSpan>1)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.minSpan must be in (0,1]. Value=")+lexical_cast<string>(parameters.minSpan) ));

	if(parameters.isolationTh==0)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.isolationTh must be in >0. Value=")+lexical_cast<string>(parameters.isolationTh) ));

	if(parameters.alphaRange && parameters.alphaRange->lower()<=0)
		throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.alphaRange cannot cover non-positive values. Value=")+lexical_cast<string>(parameters.alphaRange->lower()) ));

	if(parameters.admissibleAlpha)
		for(const auto c : *parameters.admissibleAlpha)
			if(c<=0)
				throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : parameters.admissibleAlpha cannot have non-positive elements"));

	//Remove any admissible alpha outside of the alpha range
	if(parameters.alphaRange && parameters.admissibleAlpha)
	{
		parameters.admissibleAlpha=FilterAdmissibleAlpha(*parameters.admissibleAlpha, *parameters.alphaRange);
		if(parameters.admissibleAlpha->empty())
			throw(invalid_argument("RelativeSynchronisationC::ValidateParameters : No admissible alpha value within parameters.alphaRange."));
	}	//if(parameters.alphaRange && parameters.admissibleAlpha)

	//Known alpha due to 0-wide range?
	if(parameters.alphaRange && parameters.alphaRange->lower()==parameters.alphaRange->upper())
		parameters.admissibleAlpha=vector<double>(1, parameters.alphaRange->lower());

	//If tauRange is too narrow, the feasible region may exclude some correspondences due to quantisation
	if(parameters.tauRange && parameters.tauRange->upper()-parameters.tauRange->lower()<1)
	{
		double width=parameters.tauRange->upper()-parameters.tauRange->lower();
		(*parameters.tauRange) += ParameterIntervalT(-0.5, 0.5)*(1-width);	//Set the width to 1
	}	//if(parameters.tauRange && parameters.tauRange->upper()-parameters.tauRange->lower()<2)

	//Guided matching
	if(parameters.guidedSearchRegion)
	{
		if((*parameters.guidedSearchRegion)[0]<0)
			throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.guidedSearchRegion[0] must have non-negative values. Value=")+lexical_cast<string>((*parameters.guidedSearchRegion)[0])));

		if((*parameters.guidedSearchRegion)[1]<0)
			throw(invalid_argument(string("RelativeSynchronisationC::ValidateParameters : parameters.guidedSearchRegion[1] must have non-negative values. Value=")+lexical_cast<string>((*parameters.guidedSearchRegion)[1])));

		//Clipping
		if(parameters.alphaRange)
		{
			double width=parameters.alphaRange->upper()-parameters.alphaRange->lower();
			(*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iAlphaExtent]=min(width/2, (*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iAlphaExtent]);
		}	//if(parameters.alphaRange)

		if(parameters.tauRange)
		{
			double width=parameters.tauRange->upper()-parameters.tauRange->lower();
			(*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iTauExtent]=min(width/2, (*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iTauExtent]);
		}	//if(parameters.tauRange)
	}	//if(parameters.guidedSearchRegion)

	parameters.pdlParameters.flagCovariance=false;
}	//void ValidateParameters(const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC parameters)

/**
 * @brief Converts an indicator array to a membership list
 * @param[in] flags Inlier flags
 * @return Indices of the inliers
 */
auto RelativeSynchronisationC::MakeMembership(const IndicatorArrayT& flags) const -> MembershipT
{
	MembershipT output;
	copy(counting_range((size_t)0, flags.size()) | filtered([&](size_t c){return flags[c];} ), inserter(output, output.begin()));
	return output;
}	//MembershipT MakeMembership(const IndicatorArrayT& flags) const

/**
 *
 * @param sequence1
 * @param sequence2
 * @param parameters
 * @return
 */
auto RelativeSynchronisationC::HijackFindIndexCorrespondences(const vector<ImageT>& sequence1, const vector<ImageT>& sequence2, const RelativeSynchronisationParametersC& parameters) -> IndexCorrespondenceListT
{
	IndexCorrespondenceListT output;	// Output

	//Build the normalised histograms
	vector<double> histogram1; histogram1.reserve(sequence1.size());
	size_t sum1=0;
	for(const auto& current : sequence1)
	{
		sum1+=get<ViterbiRelativeSynchronisationProblemC::iFeatureList>(current).size();
		histogram1.push_back(get<ViterbiRelativeSynchronisationProblemC::iFeatureList>(current).size());
	}	//for(const auto& current : sequence1)

	for_each(histogram1, [&](double& current){current/=sum1;});

	vector<double> histogram2; histogram2.reserve(sequence2.size());
	size_t sum2=0;
	for(const auto& current : sequence2)
	{
		sum2+=get<ViterbiRelativeSynchronisationProblemC::iFeatureList>(current).size();
		histogram2.push_back(get<ViterbiRelativeSynchronisationProblemC::iFeatureList>(current).size());
	}	//for(const auto& current : sequence2)

	for_each(histogram2, [&](double& current){current/=sum2;});

	//CDF for histogram1
	vector<double> cdf1(sequence1.size());
	boost::partial_sum(histogram1, cdf1.begin());

	double maxCDFDifference=1;//5e-3;
	double minPDFValue=1.0/min(histogram1.size(), histogram2.size());
	ViterbiNormalisedHistogramMatchingProblemC nhmProblem(histogram2, maxCDFDifference, minPDFValue);

	ViterbiParametersC viterbiParameters;
	viterbiParameters.flagMeasurementFirst=false;
	ViterbiC<ViterbiNormalisedHistogramMatchingProblemC> viterbiEngine(viterbiParameters);

	bool flagSuccess=true;
	for(auto current : MakeBinaryZipRange(histogram1, cdf1))
	{
		flagSuccess = flagSuccess && viterbiEngine.Update(nhmProblem, make_tuple(current.get<0>(), current.get<1>()));

		if(!flagSuccess)	//If the update fails, quit
			break;
	}	//for(auto current : histogram1)

	if(!flagSuccess)
		return output;

	//Extract the corresponding indices
	list<unsigned int> indexList=viterbiEngine.GetOptimalSequence();

	double probTh=exp(-5e-4);
	size_t iSeq1=0;
	for(auto c : indexList)
	{
		double measurementProb=nhmProblem.ComputeMeasurementProbability(make_tuple(histogram1[iSeq1], cdf1[iSeq1]), c);

		if(measurementProb>probTh)
			output.insert(IndexCorrespondenceListT::value_type(iSeq1,c));

		++iSeq1;
	}	//for(auto c : indexList)

	return output;
}

/**
 * @brief Finds the corresponding indices between two image sequences
 * @param[in] sequence1 Reference sequence
 * @param[in] sequence2 Target sequence
 * @param[in] parameters Parameters
 * @return List of the indices of the corresponding frames
 * @remarks The output is ordered wrt sequence1
 */
auto RelativeSynchronisationC::FindIndexCorrespondences(const vector<ImageT>& sequence1, const vector<ImageT>& sequence2, const RelativeSynchronisationParametersC& parameters) -> IndexCorrespondenceListT
{
	IndexCorrespondenceListT output;	// Output

	//Unless there is an existing Viterbi problem, set up one
	optional<tuple<ParameterIntervalT, ParameterIntervalT>> bounds;
	if(parameters.alphaRange && parameters.tauRange)
		bounds=make_tuple(*parameters.alphaRange, *parameters.tauRange);

	if(!viterbiProblem)
		viterbiProblem=ViterbiRelativeSynchronisationProblemC(sequence2, bounds, parameters.flagFixedCamera, parameters.matcherParameters, parameters.noiseVariance, parameters.minFeatureCorrespondence, parameters.flagPropagateFeatureCorrespondences);
	else
	{
		//If we are here, this is the guided pass. A valid bounds is guaranteed
		viterbiProblem->ResetMeasurementCounter();
		viterbiProblem->SetBounds(*bounds);
	}	//if(!viterbiProblem)

	//Find the correspondences

	ViterbiParametersC viterbiParameters;
	viterbiParameters.flagMeasurementFirst=false;
	viterbiParameters.nThreads= parameters.flagPropagateFeatureCorrespondences ? 1 : parameters.nThreads;

	ViterbiC<ViterbiRelativeSynchronisationProblemC> viterbiEngine(viterbiParameters);

	bool flagSuccess=true;

	for(const auto& current : sequence1)
	{
		flagSuccess = flagSuccess && viterbiEngine.Update(*viterbiProblem, current);

		if(!flagSuccess)	//If the update fails, quit
			break;
	}	//for(const auto& current : sequence1)

	if(!flagSuccess)
		return output;

	//Extract the corresponding indices
	list<unsigned int> indexList=viterbiEngine.GetOptimalSequence();

	//Weed out the weak indices and move the rest to the output

	double probTh=viterbiProblem->GetMinMeasurementProbability();
	constexpr size_t iScore=ViterbiRelativeSynchronisationProblemC::iScore;

	size_t iSeq1=0;
	for(auto c : indexList)
	{
		if( get<iScore>( *viterbiProblem->GetCache().find(iSeq1)->second[c] ) > probTh)
			output.insert(IndexCorrespondenceListT::value_type(iSeq1,c));

		++iSeq1;
	}	//for(auto c : indexList)

	return output;
}	//IndexCorrespondenceListT FindIndexCorrespondences(const vector<ImageT>& sequence1, const vector<ImageT>& sequence2, RelativeSynchronisationParametersC& parameters)

/**
 * @brief Subframe refinement for the index correspondences
 * @param integerCorrespondences Integer index correspondences
 * @param sequence1 First sequence
 * @param sequence2 Second sequence
 * @param parameters Parameters
 * @return Refined index correspondences
 * @pre \c viterbiProblem is valid
 */
auto RelativeSynchronisationC::RefineIndexCorrespondences(const IndexCorrespondenceListT& integerCorrespondences, const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, const RelativeSynchronisationParametersC& parameters) -> IndexCorrespondenceListT
{
	//Preconditions
	assert(viterbiProblem);

	constexpr size_t iCamera=ViterbiRelativeSynchronisationProblemC::iCamera;
	constexpr size_t iCorrespondence=ViterbiRelativeSynchronisationProblemC::iCorrespondence;

	PDLFrameAlignmentProblemC::model_type initialEstimate{{0,0}};
	EpipolarMatrixT mF=CameraToFundamental(get<iCamera>(get<iObservation>(sequence1)[0]), get<iCamera>(get<iObservation>(sequence2)[0]));

	//For each element, solve a frame alignment problem
	IndexCorrespondenceListT output;
	auto itE=subframeCache.end();
	for(const auto& current : integerCorrespondences)
	{
		//If already exists in the cache, skip
		auto it=subframeCache.find(make_tuple(current.left, current.right));

		if(it!=itE)
		{
			output.insert(IndexCorrespondenceListT::value_type(get<0>(it->second), get<1>(it->second)));
			continue;
		}	//if(it!=itE)

		//Problem
		size_t i1=current.left;
		size_t i2=current.right;
		const optional<ViterbiRelativeSynchronisationProblemC::node_type>& currentNode=viterbiProblem->GetCache().find(i1)->second[i2];

		PDLFrameAlignmentProblemC problem(initialEstimate, get<iCorrespondence>(*currentNode), optional<MatrixXd>(), mF, get<iForward>(sequence1)[i1], get<iBackward>(sequence1)[i1], get<iForward>(sequence2)[i2], get<iBackward>(sequence2)[i2]);

		//Estimate the shift
		typedef PowellDogLegC<PDLFrameAlignmentProblemC> PDLT;
		PDLT::result_type shifts;
		PDLT::covariance_type covariance;
		PowellDogLegDiagnosticsC diagnostics=PDLT::Run(shifts, covariance, problem, parameters.pdlParameters);

		//Subframe implies a 1x1 region centred on the index pair. So, shifts larger than 0.5 in any direction are not allowed
		bool flagApply= diagnostics.flagSuccess && fabs(shifts[0])<0.5 && fabs(shifts[1])<0.5;

		double shifted1= i1 + (flagApply ? shifts[PDLFrameAlignmentProblemC::iShift1] : 0) ;
		double shifted2= i2 + (flagApply ? shifts[PDLFrameAlignmentProblemC::iShift2] : 0) ;
		output.insert( IndexCorrespondenceListT::value_type(shifted1,shifted2));

		subframeCache[make_tuple(i1,i2)]=make_tuple(shifted1,shifted2);	//Store for future use
	}	//for(const auto& current : integerCorrespondences)

	return output;
}	//IndexCorrespondenceListT RefineIndexCorrespondences(const IndexCorrespondenceListT& integerCorrespondences, const RelativeSynchronisationParametersC& parameters) const

/**
 * @brief Evaluates a line
 * @param[in] line Line to be evaluated
 * @param[in] indexCorrespondences Frame index correspondences
 * @param[in] lossMap Error-to-loss map
 * @param[in] parameters Parameters
 * @return A tuple: loss; inlier flags
 */
auto RelativeSynchronisationC::EvaluateLine(const Line2DT& line, const vector<CoordinateT>& indexCorrespondences, const LossMapT& lossMap, const RelativeSynchronisationParametersC& parameters) const -> tuple<double, IndicatorArrayT>
{
	tuple<double, IndicatorArrayT> output;
	size_t nCorr=indexCorrespondences.size();
	get<1>(output).resize(nCorr);

	double totalLoss=0;
	for(size_t c=0; c<nCorr; ++c)
	{
		double dist=line.absDistance(indexCorrespondences[c]);
		totalLoss+=lossMap(dist);
		get<1>(output)[c] = (dist < parameters.segmentInlierTh);
	}	//for(size_t c=0; c<nCorr; ++c)

	get<0>(output)=totalLoss;
	return output;
}	//tuple<double, IndicatorArrayT> EvaluateLine(const Line2DT& line, const vector<CoordinateT>& coordinateList)

/**
 * @brief Fits a line to a set of inliers
 * @param[in] indexCorrespondences Index correspondences
 * @param[in] supportIndicators Array indicating the support set
 * @return Estimated line
 * @pre \c indexCorrespondences and \c supportIndicators have the same number of elements
 */
auto RelativeSynchronisationC::FitLine(const vector<CoordinateT>& indexCorrespondences, const IndicatorArrayT& supportIndicators) const -> Line2DT
{
	//Preconditions
	assert(indexCorrespondences.size()==supportIndicators.size());

	//Set up the equation system
	size_t sSupport=supportIndicators.count();
	MatrixX2d mA; mA.setZero(sSupport,2); mA.col(1).setConstant(1);
	VectorXd vb; vb.setZero(sSupport);

	size_t nCorr=indexCorrespondences.size();
	size_t cEq=0;
	for(size_t c=0; c<nCorr; ++c)
		if(supportIndicators[c])
		{
			mA(cEq,0)=indexCorrespondences[c][0];
			vb[cEq]=indexCorrespondences[c][1];
			++cEq;
		}	//if(supportIndicators[c])

	Vector2d sol=mA.fullPivHouseholderQr().solve(vb);

	return MakeLine2D(sol[0], -1, sol[1]);
}	//Line2DT FitLine(const vector<CoordinateT>& indexCorrespondences, const IndicatorArrayT& supportIndicators)

/**
 * @brief Fits a line to a set of inliers, with known slope
 * @param[in] indexCorrespondences Corresponding frame indices
 * @param[in] supportIndicators Array indicating the support set
 * @param[in] alpha Known slope
 * @return Estimated line
 * @pre \c indexCorrespondences and \c supportIndicators have the same number of elements
 */
auto RelativeSynchronisationC::FitOffset(const vector<CoordinateT>& indexCorrespondences, const IndicatorArrayT& supportIndicators, double alpha) const -> Line2DT
{
	//Preconditions
	assert(indexCorrespondences.size()==supportIndicators.size());

	//Offset is the mean error
	CoordinateT tmp(-alpha,1);
	double offsetAcc=accumulate(counting_range((size_t)0, supportIndicators.size()) | filtered([&](size_t c){return supportIndicators[c];}), 0.0, [&](double acc, size_t c){return acc+indexCorrespondences[c].dot(tmp);}  );
	return MakeLine2D(alpha, -1, offsetAcc/supportIndicators.count());
}	//Line2DT FitOffset(const vector<CoordinateT>& indexCorrespondences, const IndicatorArrayT& supportIndicators, double alpha)

/**
 * @brief Exhaustively instantiates and ranks all feasible line segments implied by the index correspondences, over the set of admissible alpha
 * @param[out] supportArray Array indicating the supporters of the strongest segment
 * @param[in] indexCorrespondences Index correspondences
 * @param[in] admissibleAlpha Admissible frame rate values
 * @param[in] parameters Parameters
 * @return An ordered list of line segments. [Loss, segment]
 */
auto RelativeSynchronisationC::ComputeLineSegments(IndicatorArrayT& supportArray, const vector<CoordinateT>& indexCorrespondences, const vector<double>& admissibleAlpha, const RelativeSynchronisationParametersC& parameters) const -> multimap<double, Line2DT>
{
	//Loss function for evaluation
	size_t nCorr=indexCorrespondences.size();
	LossMapT lossMap(parameters.segmentInlierTh, parameters.segmentInlierTh);
	double maxTotalLoss=nCorr*lossMap(parameters.segmentInlierTh);

	//Iterate over all alpha, and all coordinates
	double bestLoss=maxTotalLoss;
	supportArray=IndicatorArrayT(nCorr);
	multimap<double, Line2DT> output;
	for(auto alpha : admissibleAlpha)
		for(const auto& currentPoint : indexCorrespondences)
		{
			double tau=fma(-alpha, currentPoint[0], currentPoint[1]);	//Tau is the only free parameter

			//Make and validate the line
			Line2DT currentLine=MakeLine2D(alpha,-1,tau);
			if(!ValidateLine(currentLine, parameters))
				continue;

			//Evaluate
			double totalLoss;
			IndicatorArrayT inlierFlags;
			std::tie(totalLoss, inlierFlags)=EvaluateLine(currentLine, indexCorrespondences, lossMap, parameters);

			//Save
			if(inlierFlags.count()>=parameters.minSegmentSupport)
				output.emplace(totalLoss, currentLine);
			else
				continue;

			if(totalLoss<bestLoss)
			{
				supportArray.swap(inlierFlags);
				bestLoss=totalLoss;
			}
		}	//for(const auto& currentPoint : indexCorrespondences)

	return output;
}	//multimap<double, Line2DT> ComputeLineSegments(const IndexCorrespondenceListT& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const

/**
 * @brief Identifies the dominant segments in the set
 * @param[in] mainSegment Main segment defining the permissible shifts
 * @param[in] segments Line segments supported by the correspondence set
 * @param[in] indexCorrespondences List of corresponding frame indices
 * @param[in] parameters Parameters
 * @return Filtered line segments and the inliers
 * @remarks For each permissible shift, if possible, find the best representative
 */
auto RelativeSynchronisationC::IdentifyDominantSegments(const Line2DT& mainSegment, const multimap<double, Line2DT>& segments, const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const -> multimap<double, tuple<Line2DT, IndicatorArrayT> >
{
	multimap<double, tuple<Line2DT, IndicatorArrayT> >  output;

	map<double, pair<double, Line2DT> > representatives=QuantiseOffsets(mainSegment, segments, parameters);

	//Evaluate and remove the weak segments
	LossMapT lossMap(parameters.segmentInlierTh, parameters.segmentInlierTh);
	for(const auto& current : representatives)
	{
		double totalLoss;
		IndicatorArrayT inlierFlags;
		std::tie(totalLoss, inlierFlags)=EvaluateLine(current.second.second, indexCorrespondences, lossMap, parameters);

		if(inlierFlags.count()>=parameters.minSegmentSupport)
			output.emplace(totalLoss, make_tuple(current.second.second, inlierFlags));	//The best segment may have a lower loss than the main segment
	}	//for(const auto& current : representatives)

	//Merge similar segments via the overlap index. Unlikely in the normal operation, but might happen with low outlier thresholds.
	auto itE=output.end();
	for(auto it1=output.begin(); it1!=itE; std::advance(it1,1))
	{
		const IndicatorArrayT& flags1=get<1>(it1->second);
		size_t cardinality1=flags1.count();

		for(auto it2=next(it1,1); it2!=itE;)
		{
			const IndicatorArrayT& flags2=get<1>(it2->second);
			double overlapIndex=(double)(flags1 & flags2).count()/min(cardinality1, flags2.count());

			if(overlapIndex<=parameters.maxOverlap)
				std::advance(it2,1);
			else
				it2=output.erase(it2);
		}	//for(auto it2=next(it1,1); it2!=itE;)
	}	//for(auto it1=output.begin(); it1!=itE; advance(it1,1))

	return output;
}	//auto IdentifyDominantSegments(const Line2DT& mainSegment, const multimap<double, Line2DT>& segments, const RelativeSynchronisationParametersC& parameters) const -> multimap<double, Line2DT>

/**
 * @brief Resolves the conflicting assignments due to overlapping support regions
 * @param[in, out] set1 First set
 * @param[in, out] set2 Second set
 */
void RelativeSynchronisationC::ResolveConflict(MembershipT& set1, MembershipT& set2) const
{
	//Remove members until the intersection is empty
	do
	{
		if(set1.empty() || set2.empty())
			break;

		interval<size_t> interval1(*set1.begin(), *set1.rbegin());
		interval<size_t> interval2(*set2.begin(), *set2.rbegin());
		if(!overlap(interval1, interval2))
			break;

		unsigned int opCode=4;	//Remove lower 1, remove upper 1, remove lower 2, remove upper 2, none

		//Subset?

		bool flagSubset=false;

		//Case I: set1 subset of set2
		if(subset(interval1, interval2))
		{
			//Closest on the lower boundary?
			bool flagLower = (interval1.lower()-interval2.lower()) < (interval2.upper()-interval1.upper());
			opCode= flagLower ? 2:3;	//Remove the lower or upper sentinel of set2
			flagSubset=true;
		}	//if(subset(interval1, interval2))

		//Case II: set2 subset of set1
		if(subset(interval2, interval1))
		{
			//Closest on the left?
			bool flagLower = (interval2.lower()-interval1.lower()) < (interval1.upper()-interval2.upper());
			opCode= flagLower ? 0:1;	//Remove the lower or upper sentinel of set1
			flagSubset=true;
		}	//if(subset(interval1, interval2))

		//Simple overlap
		if(!flagSubset)
		{
			//Density: cardinality/span
			double density1 = (double)set1.size()/ (interval1.upper()-interval1.lower());
			double density2 = (double)set2.size()/ (interval2.upper()-interval2.lower());

			//Case III: set1 is to the right of set2
			if(in(interval1.lower(), interval2))
				opCode= (density1<density2) ? 0:3;	//Preserve the "denser" segment. It is more likely that segment with stray elements is causing the overlap

			//Case IV: set1 is to the left of set2
			if(in(interval1.upper(), interval2))
				opCode= (density1<density2) ? 1:2;	//Preserve the "denser" segment. It is more likely that segment with stray elements is causing the overlap
		}	//if(!flagSubset)

		//Remove
		switch(opCode)
		{
			case 0: set1.erase(set1.begin()); break;
			case 1: set1.erase(prev(set1.end(),1)); break;
			case 2: set2.erase(set2.begin()); break;
			case 3: set2.erase(prev(set2.end(),1)); break;
			default: break;
		}	//switch(opCode)

	}while(true);
}	//tuple<MembershipT, MembershipT> ResolveConflict(const MembershipT& set1, const MembershipT& set2)

/**
 * @brief Removes isolated index correspondences
 * @param[in] membership Membership
 * @param[in] indexCorrespondences Index correspondences
 * @param[in] parameters Parameters
 * @return Processed membership list
 * @remarks Isolated correspondences are unreliable, because an activity is not instantaneous- it should give rise to a burst of correspondences
 */
auto RelativeSynchronisationC::RemoveIsolated(const MembershipT& membership, const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const -> MembershipT
{
	if(membership.size()<=3)
		return membership;

	MembershipT output;
	auto itB=membership.cbegin();
	auto itL=itB;	//Trailing
	auto itR=next(itB,2);	//Leading
	auto itT=prev(membership.cend(),1);	//Terminate

	for(auto it=next(itB,1); it!=itT; advance(it,1), advance(itR,1), advance(itL,1))
	{
		unsigned int distanceL= (indexCorrespondences[*it][0]-indexCorrespondences[*itL][0]);	//Distance to the left neighbour
		unsigned int distanceR= (indexCorrespondences[*itR][0]-indexCorrespondences[*it][0]);	//Distance to the right neighbour

		if(min(distanceL, distanceR)<=parameters.isolationTh)
			output.insert(*it);
	}	//for(auto it=next(itB,1); it!=itT; advance(it,1), advance(itR,1), advance(itL,1))

	//Terminal points
	if( (indexCorrespondences[*itB][0]-indexCorrespondences[*next(itB,1)][0])<=parameters.isolationTh)
		output.insert(*itB);

	if( (indexCorrespondences[*itT][0]-indexCorrespondences[*prev(itT,1)][0])<=parameters.isolationTh)
		output.insert(*itT);

	return output;
}	//MembershipT EliminateIsolated(const MembershipT& correspondences, const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters)

/**
 * @brief Assigns the correspondences to the line segments as mutually exclusive intervals
 * @param[in] segments Segments to be assigned an interval
 * @param[in] indexCorrespondences Index correspondences
 * @param[in] span Length of the reference sequence
 * @param[in] parameters Parameters
 * @return Temporally ordered segments and the associated support. [Index of the first correspondence; [Line; Supporters] ]
 * @remarks Also filters the segments wrt support and span
 */
auto RelativeSynchronisationC::AssignCorrespondences(const multimap<double, tuple<Line2DT, IndicatorArrayT> >& segments, const vector<CoordinateT>& indexCorrespondences, size_t span, const RelativeSynchronisationParametersC& parameters) const -> map< size_t, tuple<Line2DT, MembershipT> >
{
	map< size_t, tuple<Line2DT, MembershipT> > output;
	if(segments.empty())
		return output;

	//Assign correspondences to the segments
	size_t nSegments=segments.size();
	vector<MembershipT> memberStack; memberStack.reserve(nSegments);

	for(const auto& currentSegment : segments)
	{
		const IndicatorArrayT& inliers=get<1>(currentSegment.second);
		if(inliers.count()==0)
			continue;

		//Build the members list
		MembershipT segmentSupport=MakeMembership(inliers);

		//Find the intersections with the previous intervals
		for(auto& current : memberStack)
			ResolveConflict(segmentSupport, current);

		memberStack.push_back(segmentSupport);
	}	//for(const auto& current : segments)

	//Eliminate any isolated correspondences
	for(auto& current : memberStack)
		current=RemoveIsolated(current, indexCorrespondences, parameters);

	//Filter the surviving segments
	double minSpan = span*parameters.minSpan;
	size_t index=0;
	for(const auto& current : segments)
	{
		if(memberStack[index].empty())
		{
			++index;
			continue;
		}	//if(memberStack[index].empty())

		//Minimum inlier test. Also, minimum span test
		size_t iE=*memberStack[index].crbegin();
		size_t iB=*memberStack[index].cbegin();
		double segmentSpan = indexCorrespondences[iE][0] - indexCorrespondences[iB][0];	//Last element - fist element

		if(memberStack[index].size()>=parameters.minSegmentSupport &&  segmentSpan>=minSpan )
			output[*memberStack[index].begin()]= make_tuple(get<0>(current.second), memberStack[index]);	//Temporal order

		++index;
	}	//for(const auto& current : segments)

	return output;
}	//void BuildTimeline(const multimap<double, tuple<Line2DT, IndicatorArrayT> >& segments, const RelativeSynchronisationParametersC& parameters) const

/**
 * @brief Validates a line
 * @param[in] line Line to be validated
 * @param[in] parameters Parameters
 * @return \c false if the line fails the constraints
 */
bool RelativeSynchronisationC::ValidateLine(const Line2DT& line, const RelativeSynchronisationParametersC& parameters) const
{
	//Slope infinity?
	if(line.coeffs()[1]==0)
		return false;

	double alpha;
	double tau;
	tie(alpha, tau)=*DecomposeLine2D(line);	//Slope is not infinity

	if(alpha<=0)
		return false;

	//Is within the parameter ranges?
	if(parameters.alphaRange && !in(alpha, *parameters.alphaRange))
		return false;

	if(parameters.tauRange && !in(tau, *parameters.tauRange))
		return false;

	return true;
}	//bool ValidateLine(const Line2DT& line, const RelativeSynchronisationParametersC& parameters)

/**
 * @brief Identifies the main segment for unconstrained alpha
 * @param[in] indexCorrespondences Index correspondences
 * @param[in] parameters Parameters
 * @return A tuple: The dominant segment and its supporters. Invalid if no line satisfies the constraints
 */
auto RelativeSynchronisationC::IdentifyMainSegment(const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters) const -> optional<tuple<Line2DT, IndicatorArrayT>>
{
	//Loss function for evaluation
	size_t nCorr=indexCorrespondences.size();
	LossMapT lossMap(parameters.segmentInlierTh, parameters.segmentInlierTh);
	double maxTotalLoss=nCorr*lossMap(parameters.segmentInlierTh);

	//Iterate over all pairs
	Line2DT bestLine=MakeLine2D(0,0,0);	//Dominant segment. Initialisation to silence the compiler
	optional<IndicatorArrayT> bestSupport;	//Support for the dominant segment
	double bestLoss=maxTotalLoss;
	for(size_t c1=0; c1<nCorr; ++c1)
		for(size_t c2=c1+1; c2<nCorr; ++c2)
		{
			Line2DT currentLine=Line2DT::Through(indexCorrespondences[c1], indexCorrespondences[c2]);	//Instantiate the line

			//Validate
			if(!ValidateLine(currentLine, parameters))
				continue;

			//Evaluate
			double totalLoss;
			IndicatorArrayT inlierFlags;
			std::tie(totalLoss, inlierFlags)=EvaluateLine(currentLine, indexCorrespondences, lossMap, parameters);

			//Update the best
			if(totalLoss<bestLoss && (inlierFlags.count()>=parameters.minSegmentSupport))
			{
				bestLoss=totalLoss;
				bestLine=currentLine;
				bestSupport=inlierFlags;
			}
		}	//for(const auto& dest : indexCorrespondences)

	optional<tuple<Line2DT, IndicatorArrayT> > output;

	if(!bestSupport)
		return output;

	output=make_tuple(bestLine, *bestSupport);
	return output;
}	//tuple<Line2DT, IndicatorArrayT> ComputeMainSegment(const vector<CoordinateT>& indexCorrespondences, const RelativeSynchronisationParametersC& parameters)

/**
 * @brief Quantises the offset values and returns the best representatives
 * @param[in] mainSegment Main segment
 * @param[in] segments Segments to be quantised
 * @param[in] parameters Parameters
 * @return A map: Shift; [loss, line]
 */
map<double, pair<double, Line2DT> > RelativeSynchronisationC::QuantiseOffsets(const Line2DT& mainSegment, const multimap<double, Line2DT>& segments, const RelativeSynchronisationParametersC& parameters) const
{
	double alpha;
	double tau;
	tie(alpha,tau)=*DecomposeLine2D(mainSegment);	// Guaranteed to be valid, as lines with b=0 are not allowed

	//Find the representatives for the permissible shifts
	map<double, pair<double, Line2DT> > representatives;	//[shift; [loss, line] ]
	for(const auto& current : segments)
	{
		double dummy;
		double currentTau;
		tie(dummy, currentTau)=*DecomposeLine2D(current.second);

		//Quantisation: A shift can be explained by a drop in either of the sequences

		array<double,2> proposedTau;	//Quantised tau values
		proposedTau[1] = tau + round(tau-currentTau) * ((currentTau<=tau) ? -1:1);	//Drop in sequence 2. Quantise to tau+-D
		proposedTau[0] = tau + round(-(tau-currentTau)/alpha)*alpha* ((currentTau<=tau) ? -1:1);	//Drop in sequence 1. Quantise to tau-+D*alpha

		auto tauValidator=[&](double tested){ return fabs(currentTau-tested)<=parameters.tauQuantisationTh && ( !(parameters.tauRange) || ((parameters.tauRange) && in(tested, *parameters.tauRange)) ); };
		for(auto quantised : proposedTau)
			if(tauValidator(quantised))
			{
				//Challenge the existing representative
				pair<double, Line2DT> item=make_pair(current.first, MakeLine2D(alpha, -1, quantised));
				auto it=representatives.emplace(quantised, item);	//Attempt to insert

				if(!it.second)	//Failure? Challenge the existing representative
					if(current.first < it.first->second.first)	//Compare the losses
						representatives[quantised]=item;
			}	//if(tauValidator(c))
	}	//for(const auto& current : segments)

	return representatives;
}	//map<double, pair<double, Line2DT> > QuantiseOffsets(const Line2DT& mainSegment, const multimap<double, Line2DT>& segments, const RelativeSynchronisationParametersC& parameters) const

/**
 * @brief Fits a broken line model to the correspondences
 * @param[in] indexCorrespondences Correspondences
 * @param[in] sSequence1 Size of the first sequence
 * @param[in] parameters Parameters
 * @return A broken line structure. Invalid if the operation fails
 * @remarks The model is composed of separate non-overlapping sections with identical slope.
 */
auto RelativeSynchronisationC::FitBrokenLine(const vector<CoordinateT>& indexCorrespondences, size_t sSequence1, const RelativeSynchronisationParametersC& parameters) const -> optional<BrokenLineT>
{
	optional<BrokenLineT> output;

	IndicatorArrayT supporters;	//Support indicators for the strongest segment
	Line2DT mainSegment;	//Segment with the best support: Defines alpha and the subframe part of the offset

	//If alpha is free
	vector<double> admissibleAlpha;
	if(!parameters.admissibleAlpha)
	{
		optional<tuple<Line2DT, IndicatorArrayT>> estimated=IdentifyMainSegment(indexCorrespondences, parameters);

		if(!estimated)	//Failure
			return output;

		std::tie(mainSegment, supporters)=*estimated;
		mainSegment=FitLine(indexCorrespondences, supporters);	//Overwrite the main segment by the best LS solution

		//Admissible alpha, single entry
		admissibleAlpha.resize(1);
		double dummyTau;
		tie(admissibleAlpha[0], dummyTau)=*DecomposeLine2D(mainSegment);
	}	//if(!parameters.admissibleAlpha)
	else
		admissibleAlpha=*parameters.admissibleAlpha;

	//At this point, alpha is constrained to a finite number of hypotheses (or even 1)
	//Estimate all line segments supported by the correspondence set, and having a specified slope
	multimap<double, Line2DT> segmentList=ComputeLineSegments(supporters, indexCorrespondences, admissibleAlpha, parameters);

	if(segmentList.empty())
		return output;

	//Reestimate the main segment

	mainSegment=segmentList.begin()->second;	//Current estimate

	double alpha;
	double tau;
	tie(alpha, tau)=*DecomposeLine2D(mainSegment);

	mainSegment=FitOffset(indexCorrespondences, supporters, alpha);	//LS estimate
	tie(alpha,tau)=*DecomposeLine2D(mainSegment);

	//If there are no frame drops, the broken line model is composed of a single section
	if(parameters.flagNoFrameDrop)
	{
		output=BrokenLineT(alpha, list<LineSectionT>{make_tuple(tau, MakeMembership(supporters))});
		return output;
	}	//if(parameters.flagNoFrameDrop)

	//Else, identify the other significant segments
	multimap<double, tuple<Line2DT, IndicatorArrayT> > filteredSegments=IdentifyDominantSegments(mainSegment, segmentList, indexCorrespondences, parameters);
	map<size_t, tuple<Line2DT, MembershipT> > orderedSegments=AssignCorrespondences(filteredSegments, indexCorrespondences, sSequence1, parameters);

	if(orderedSegments.empty())
		return output;

	//Build the model
	output=BrokenLineT();
	get<0>(*output)=alpha;
	for(const auto& current : orderedSegments)
	{
		double currentAlpha;
		double currentTau;
		tie(currentAlpha, currentTau)=*DecomposeLine2D(get<0>(current.second));
		get<1>(*output).emplace_back(currentTau, get<1>(current.second));
	}	//for(const auto& current : orderedSegments)

	return output;
}	//auto FitBrokenLine(const vector<CoordinateT>& indexCorrespondences, size_t sSequence1, const RelativeSynchronisationParametersC& parameters) const -> BrokenLineT

/**
 * @brief Adjusts an interval so that it can hold the dropped frames
 * @param[in] lower Lower bound
 * @param[in] upper Upper bound
 * @param[in] nDropped Number of dropped frames
 * @return A tuple: lower and upper bound
 */
tuple<double, double> RelativeSynchronisationC::AdjustInterval(double lower, double upper, unsigned int nDropped) const
{
	double delta= (double)nDropped - (upper-lower);	//Overflow
	double shiftLower = (lower >= delta/2) ? delta/2 : lower;	//Cannot go below 0
	double shiftUpper = delta-shiftLower;

	return make_tuple(lower-shiftLower, upper+shiftUpper);
}	//tuple<double, double> AdjustInterval(double lower, double upper, unsigned int nDropped)

/**
 * @brief Inserts a frame drop event
 * @param[in,out] dst Destination
 * @param[in] lower Lower bound, open interval
 * @param[in] upper Upper bound, open interval
 * @param[in] nDropped Number of dropped frames
 */
void RelativeSynchronisationC::InsertDrop(list<FrameDropT>& dst, double lower, double upper, unsigned int nDropped) const
{
	//Closed interval
	double cLower=round(lower)+1;
	double cUpper=round(upper)-1;

	if(cUpper-cLower < nDropped)
		tie(cLower, cUpper)=AdjustInterval(cLower, cUpper, nDropped);	//Enlarge the interval so that it can hold the dropped frames. May lead to overlaps between the gap intervals and the support of the neighbouring line segments

	dst.emplace_back(nDropped, interval<double>(cLower, cUpper));
}	//void InsertDrop(list<FrameDropT>& dst, double lower, double upper, unsigned int nDropped) const

/**
 * @brief Computes the frame drop events corresponding to the gaps between the sections
 * @param[in] model The broken-line explaining the correspondences
 * @param[in] indexCorrespondences Correspondences
 * @return A tuple: Frame drop events in the reference sequence, and those in the target sequence
 */
auto RelativeSynchronisationC::MakeFrameDrop(const BrokenLineT& model, const vector<CoordinateT>& indexCorrespondences) const -> tuple<list<FrameDropT>, list<FrameDropT> >
{
	tuple<list<FrameDropT>, list<FrameDropT>> output;

	const list<LineSectionT>& sections= get<1>(model);
	double alpha=get<0>(model);

	//Iterate over the gaps
	auto itE=sections.cend();
	double prevTau=get<0>(*sections.cbegin());
	for(auto it=next(sections.cbegin(),1); it!=itE; advance(it,1))
	{
		double currentTau=get<0>(*it);
		double deltaTau=currentTau-prevTau;

		size_t ip= *get<1>(*prev(it,1)).crbegin();
		size_t ic= *get<1>(*it).cbegin();

		if(deltaTau>0)	//Drop in the reference sequence
			InsertDrop(get<0>(output), indexCorrespondences[ip][0], indexCorrespondences[ic][0], round(deltaTau)/alpha);
		else	//Drop in the target sequence
			InsertDrop(get<1>(output), indexCorrespondences[ip][1], indexCorrespondences[ic][1], -round(deltaTau));
	}	//for(auto it=next(sections.cbegin(),1); it!=itE; advance(it,1))

	return output;
}	//tuple<vector<FrameDropT>, vector<FrameDropT> > MakeFrameDrop(const BrokenLineT& model)

/**
 * @brief Runs the synchronisation pipeline
 * @param sequence1 Reference sequence
 * @param sequence2 Target sequence to be synchronisation with the reference
 * @param parameters Parameters
 * @return A tuple: Synchronisation result; broken-line model and support; score
 * @post The state is modified
 */
auto RelativeSynchronisationC::Synchronise(const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, const RelativeSynchronisationParametersC& parameters) -> optional<tuple<ResultT, ModelT, double> >
{
	optional<tuple<ResultT, ModelT, double> > output;

	if(get<iObservation>(sequence1).empty() || get<iObservation>(sequence2).empty())
		return output;

	//Find and refine the index correspondences
	IndexCorrespondenceListT integerCorrespondences=FindIndexCorrespondences(get<iObservation>(sequence1), get<iObservation>(sequence2), parameters);

	IndexCorrespondenceListT indexCorrespondences;	//Final index correspondences
	if(parameters.flagFixedCamera)
		indexCorrespondences=RefineIndexCorrespondences(integerCorrespondences, sequence1, sequence2, parameters);
	else
		indexCorrespondences.swap(integerCorrespondences);

	if(indexCorrespondences.empty())	//If no correspondences, quit
		return output;

	//Convert to coordinates
	vector<CoordinateT> coordinateList; coordinateList.reserve(indexCorrespondences.size());
	for(const auto& current : indexCorrespondences)
		coordinateList.emplace_back(current.left, current.right);

	//Fit a broken-line model
	optional<BrokenLineT> brokenLine=FitBrokenLine(coordinateList, get<iForward>(sequence1).size(), parameters);

	if(!brokenLine)
		return output;

	//If multiple segments, find the frame-drop events
	list<FrameDropT> dropList1;
	list<FrameDropT> dropList2;
	if(get<1>(*brokenLine).size()>1)
		tie(dropList1, dropList2)=MakeFrameDrop(*brokenLine, coordinateList);

	//Output
	output=MakeResult(*brokenLine, dropList1, dropList2, indexCorrespondences);
	return output;
}	//RelativeSynchronisationDiagnosticsC Run(result_type& result, const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC parameters)

/**
 * @brief Builds the output structure
 * @param brokenLine The broken-line fit to the correspondences
 * @param dropList1 List of drop events for the reference sequence
 * @param dropList2 List of drop events for the target sequence
 * @param indexCorrespondences Index correspondences
 * @return A tuple: Result structure, model structure, fitness score
 */
auto RelativeSynchronisationC::MakeResult(const BrokenLineT& brokenLine, const list<FrameDropT>& dropList1, const list<FrameDropT>& dropList2, const IndexCorrespondenceListT& indexCorrespondences ) const -> tuple<ResultT, ModelT, double>
{
	ResultT synchronisation;
	get<iAlpha>(synchronisation)=get<0>(brokenLine);
	get<iTau>(synchronisation)=get<0>(*get<1>(brokenLine).cbegin());

	get<iDropList1>(synchronisation).reserve(dropList1.size());
	copy(dropList1, back_inserter(get<iDropList1>(synchronisation)));

	get<iDropList2>(synchronisation).reserve(dropList2.size());
	copy(dropList2, back_inserter(get<iDropList2>(synchronisation)));

	ModelT model; model.reserve(get<1>(brokenLine).size());
	unsigned int score=0;
	for(const auto& current : get<1>(brokenLine))
	{
		model.emplace_back(MakeLine2D(get<iAlpha>(synchronisation), -1, get<0>(current)), FilterBimap(indexCorrespondences, get<1>(current)));	//Line and support set
		score+=get<1>(current).size();
	}	//for(const auto& current : get<1>(brokenLine))

	return make_tuple(synchronisation, model, score);
}	//tuple<ResultT, ModelT, double> MakeResult(const BrokenLineT& brokenLine, const list<FrameDropT>& dropList1, const list<FrameDropT>& dropList2, const IndexCorrespondenceListT& indexCorrespondences )

/**
 * @brief Prepares a parameter object for the guided synchronisation pass
 * @param[in] model Broken-line model generated by the first pass
 * @param[in] parameters Parameters
 * @return Relative synchronisation parameters for the second pass
 * @pre \c parameters.guidedSearchRegion is valid
 * @pre \c model is not empty
 */
RelativeSynchronisationParametersC RelativeSynchronisationC::MakeGuidedSynchronisationParameters(const ModelT& model, RelativeSynchronisationParametersC parameters) const
{
	//Preconditions
	assert(parameters.guidedSearchRegion);
	assert(!model.empty());

	double alpha;
	double tau;
	tie(alpha, tau)=*DecomposeLine2D(get<iLine>(model[0]));

	//AlphaRange
	double alphaMin=max(1e-16, alpha - (*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iAlphaExtent]);
	double alphaMax = alpha + (*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iAlphaExtent];
	parameters.alphaRange=ParameterIntervalT(alphaMin, alphaMax);

	//Admissible alpha
	if(parameters.admissibleAlpha)
		parameters.admissibleAlpha=FilterAdmissibleAlpha(*parameters.admissibleAlpha, *parameters.alphaRange);

	//Tau range
	double tauMin=tau;
	double tauMax=tau;
	for_each(model, [&](const SegmentT& current){ tauMin=min(tauMin, get<iLine>(current).coeffs()[2]); tauMax=max(tauMax, get<iLine>(current).coeffs()[2]);});

	parameters.tauRange=ParameterIntervalT(tauMin, tauMax) + (*parameters.guidedSearchRegion)[RelativeSynchronisationParametersC::iTauExtent]*ParameterIntervalT(-1,1);

	return parameters;
}	//RelativeSynchronisationParametersC MakeGuidedSynchronisationParameters(const ResultT& synchronisationPass1, RelativeSynchronisationParametersC parameters) const

/**
 * @brief Runs the synchronisation pipeline
 * @param[out] result Output of the algorithm. Synchronisation parameters and frame drops
 * @param[out] model The broken-line associated with the result, and the inliers
 * @param[in] sequence1 Reference sequence
 * @param[in] sequence2 Target sequence to be synchronisation with the reference
 * @param[in] parameters Parameters
 * @return Diagnostics object
 * @post The state is modified
 * @remarks Two-pass synchronisation. The first pass guides a second pass, presumably with a smaller search region
 */
auto RelativeSynchronisationC::Run(result_type& result, ModelT& model, const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC parameters) -> RelativeSynchronisationDiagnosticsC
{
	RelativeSynchronisationDiagnosticsC diagnostics;

	//Validate the parameters
	ValidateParameters(sequence1, sequence2, parameters);

	//Synchronisation, first pass
	optional< tuple<ResultT, ModelT, double> > pass1=Synchronise(sequence1, sequence2, parameters);

	//If failed, quit
	if(!pass1)
		return diagnostics;

	//Else, guided synchronisation pass
	optional< tuple<ResultT, ModelT, double> > pass2;
	if(parameters.guidedSearchRegion)
	{
		RelativeSynchronisationParametersC parameters2=MakeGuidedSynchronisationParameters(get<1>(*pass1), parameters);	//Adjust the parameter ranges
		pass2=Synchronise(sequence1, sequence2, parameters2);
	}	//if(parameters.guidedSearchRegion)

	//Choose the best result
	bool flagWinnerP1 = !pass2 || (pass2 && get<2>(*pass2) < get<2>(*pass2));	//Pass1 wins?
	std::tie(result, model, diagnostics.score) = flagWinnerP1 ? *pass1 : *pass2;

	diagnostics.flagSuccess=true;
	return diagnostics;
}	//RelativeSynchronisationDiagnosticsC Run(result_type& result, const ImageSequenceT& sequence1, const ImageSequenceT& sequence2, RelativeSynchronisationParametersC parameters)

/**
 * @brief Clears the state
 * @post The state is cleared
 */
void RelativeSynchronisationC::Clear()
{
	viterbiProblem=optional<ViterbiRelativeSynchronisationProblemC>();
	subframeCache.clear();
}	//void Clear()

/********** EXPLICIT INSTANTIATIONS **********/
template RelativeSynchronisationC::image_sequence_type  RelativeSynchronisationC::MakeImageSequence(const vector<ImageFeatureTrajectoryT>&, const vector<CameraMatrixT>&, double);

}	//SynchronisationN
}	//SeeSawN

