/**
 * @file ScaledUnscentedTransformation.ipp Implementation of ScaledUnscentedTransformationC
 * @author Evren Imre
 * @date 4 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCALED_UNSCENTED_TRANSFORMATION_IPP_0909212
#define SCALED_UNSCENTED_TRANSFORMATION_IPP_0909212

#include <Eigen/SVD>
#include <Eigen/Dense>
#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/lexical_cast.hpp>
#include <cmath>
#include <type_traits>
#include <vector>
#include <cstddef>
#include <stdexcept>
#include <string>
#include <omp.h>
#include "ScaledUnscentedTransformation.h"
#include "MultivariateGaussian.h"
#include "../Numeric/PerturbationAnalysisProblemConcept.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace UncertaintyEstimationN
{

using boost::optional;
using boost::math::pow;
using boost::lexical_cast;
using Eigen::Matrix;
using std::fma;
using std::sqrt;
using std::min;
using std::max;
using std::true_type;
using std::false_type;
using std::vector;
using std::size_t;
using std::invalid_argument;
using std::string;
using SeeSawN::NumericN::PerturbationAnalysisProblemConceptC;
using SeeSawN::NumericN::ComputeSquareRootSVD;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;

/**
 * @brief Parameters for \c ScaledUncentedTransformationC
 * @ingroup Parameters
 */
struct ScaledUnscentedTransformationParametersC
{
	unsigned int nThreads;	///< Number of threads
	optional<double> alpha;	///< Scale factor that determines the sample radius. If undefined, automatically determined. (0, 1] is recommended, small is better, especially when the distribution is not Gaussian. However, too small alpha may result in an ill-conditioned covariance matrix.
	double kappa;	///< Scale factor that pulls in, or pushes out the samples. Similar to \c alpha, but nonzero values introduces errors in the higher order moments. Negative values can lead to non-positive definite covariance matrices
	double beta;	///< Adjusts the weight of the input mean in the computation of the covariance. Can be used to incorporate any prior knowledge of higher order moments. 2, for a Gaussian distribution

	ScaledUnscentedTransformationParametersC() : nThreads(1), kappa(0), beta(2)
	{}
};	//struct ScaledUnscentedTransformationParametersC

/**
 * @brief Implementation of scaled unscented transformation algorithm
 * @tparam ProblemT Problem
 * @pre \c ProblemT is a model of PerturbationAnalysisProblemConceptC
 * @remarks R. m. d. Merwe, A. Doucet, N. d. Freitas, E. Wan, "The Unscented Particle Filter, " Cambridge Universtiy Engineering Department, Technical Report, CUED/F-INFENG/TR 380, Aug. 2000
 * @remarks SVD instead of Cholesky for the factorisation of the input covariance matrix. SVD is more stable, and can work with ill-conditioned covariance matrices
 * @remarks SUT does not work well with ill-conditioned covariance matrices.
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class ScaledUnscentedTransformationC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((PerturbationAnalysisProblemConceptC<ProblemT>));
	///@endcond

	private:

		typedef typename ProblemT::input_covariance_type InputCovarianceT;	///< Type of the input covariance matrix

		/** @name Implementation details */ //@{
		static InputCovarianceT DecomposeCovariance(const ProblemT& problem, true_type dummy);	///< Delegates the decomposition to the problem
		static InputCovarianceT DecomposeCovariance(const ProblemT& problem, false_type dummy);	///< Decomposes the covariance matrix

		static void ValidateParameters(const ScaledUnscentedTransformationParametersC& parameters);	///< Validates the input parameters
		//@}
	public:

		typedef optional<typename ProblemT::transformed_gaussian_type> result_type;	///< Type of the transformed Gaussian

		static result_type Run(ProblemT& problem, const ScaledUnscentedTransformationParametersC& parameters);	///< Applies SUT

};	//class ScaledUnscentedTransformationC

/**
 * @brief Validates the input parameters
 * @param[in] parameters Parameters
 * @throws invalid_argument If \c parameters has invalid values
 */
template<class ProblemT>
void ScaledUnscentedTransformationC<ProblemT>::ValidateParameters(const ScaledUnscentedTransformationParametersC& parameters)
{
	if(parameters.beta<0)
		throw(invalid_argument(string("ScaledUnscentedTransformationC::ValidateParameters : Beta must be a non-negative number. Value:") + lexical_cast<string>(parameters.beta)));

	if(parameters.alpha && *parameters.alpha<=0 )
		throw(invalid_argument(string("ScaledUnscentedTransformationC::ValidateParameters : Alpha must be a positive number. Value:") + lexical_cast<string>(*parameters.alpha)));
}	//void ValidateParameters(const ScaledUnscentedTransformationParametersC& parameters)

/**
 * @brief Delegates the decomposition to the problem
 * @param[in] problem Problem
 * @param[in] dummy Dummy parameter for overload resolution
 * @return "Square root" of the input covariance
 */
template<class ProblemT>
auto ScaledUnscentedTransformationC<ProblemT>::DecomposeCovariance(const ProblemT& problem, true_type dummy) -> InputCovarianceT
{
	return problem.DecomposeCovariance();
}	//InputCovarianceT DecomposeCovariance(ProblemT& problem, true_type dummy)

/**
 * @brief Decomposes the covariance matrix
 * @param[in] problem Problem
 * @param[in] dummy Dummy parameter for overload resolution
 * @return "Square root" of the input covariance
 */
template<class ProblemT>
auto ScaledUnscentedTransformationC<ProblemT>::DecomposeCovariance(const ProblemT& problem, false_type dummy) -> InputCovarianceT
{
	return ComputeSquareRootSVD<Eigen::FullPivHouseholderQRPreconditioner>(problem.GetCovariance());
}	//InputCovarianceT DecomposeCovariance(ProblemT& problem, true_type dummy)

/**
 * @brief Applies SUT
 * @param[in,out] problem
 * @param[in] parameters Algorithm parameters
 * @return Transformed random variable. Invalid if the operation fails
 * @pre \c problem is valid
 * @remarks The performance is reported to be insensitive to \c kappa , hence it is set to 0
 * @remarks For Gaussian input variable and \c kappa=0 ,  \c alpha=sqrt(3/ \#dim) minimises the kurtosis error (S. J. Julier, J. K. Uhlmann, "Unscented Filtering and  Nonlinear Estimation," Proceedings of the IEEE, vol. 92, pp. 401-422, March 2004, footnote 5)
 * @remarks Small values of \c alpha lead to large weights, which may compromise the accuracy of the output mean (and by proxy, that of the covariance). In such cases, a viable approximation is the transformation of the input mean (i.e. a single sample) or a uniform weighting scheme (just for the mean)
 */
template<class ProblemT>
auto ScaledUnscentedTransformationC<ProblemT>::Run(ProblemT& problem, const ScaledUnscentedTransformationParametersC& parameters) -> result_type
{
	if(!problem.IsValid())
		throw(invalid_argument("ScaledUnscentedTransformationC::Run : Invalid problem."));

	ValidateParameters(parameters);

	double alpha= (parameters.alpha) ? *parameters.alpha : problem.Alpha();
	double beta=parameters.beta;
	double kappa=parameters.kappa;
	int nThreads=min( omp_get_max_threads(), max(1, (int)parameters.nThreads) );	//No more threads than available resources

	//Compute the weights (Eq.15)

	unsigned int nDim=problem.InputDimensionality();

	double alpha2=pow<2>(alpha);
	double lambda=fma(alpha2, nDim+kappa, -int(nDim));
	double nxpLambda=nDim+lambda;

	double wPeriphery=0.5/nxpLambda;	// Weight of the peripheral samples
	double wMeanCentre=lambda/nxpLambda;	// Weight of the central sample for mean estimation
	double wCovCentre=wMeanCentre + 1-alpha2+beta;	// Weight of the central sample for covariance estimation

	//Decompose the covariance matrix
	InputCovarianceT mC=DecomposeCovariance(problem, typename ProblemT::can_decompose_covariance());

	//Perturb and transform
	vector<typename ProblemT::transformed_sample_type> transformedSamples(2*nDim+1);

	Matrix<typename ValueTypeM<InputCovarianceT>::type, RowsM<InputCovarianceT>::value, 1> zeroVector(mC.rows()); zeroVector.setZero();
	bool flagSuccessO=problem.TransformPerturbed(transformedSamples[0], zeroVector);	// No perturbation
	if(!flagSuccessO)
		return result_type();

	double sqnxpLambda=sqrt(nxpLambda);
	bool flagBreak=false;
	size_t index=1;
	size_t localIndex;	//Thread-local copy of index
	bool flagSuccess=true;	//Indicates whether the opreation is successful
#pragma omp parallel if(nThreads>1) num_threads(nThreads) private(localIndex)
	while(!flagBreak)
	{
	#pragma omp critical(SUT_R2)
	{
		localIndex=index;	//Thread-local copy of index
		++index;
	}
		bool flagPastEnd= localIndex>nDim;	//If true, the indices are exhausted, and the loop should terminate

		typename ProblemT::transformed_sample_type transformed1;
		bool flagSuccessP= flagPastEnd ? false : problem.TransformPerturbed(transformed1, sqnxpLambda*mC.col(localIndex-1));

		typename ProblemT::transformed_sample_type transformed2;
		bool flagSuccessM= flagPastEnd ? false : problem.TransformPerturbed(transformed2, -sqnxpLambda*mC.col(localIndex-1));

	#pragma omp critical(SUT_R1)
	{
		if(flagSuccessP)
			transformedSamples[localIndex]=transformed1;	//If localIndex>nDim, cannot execute this line

		if(flagSuccessM)
			transformedSamples[localIndex+nDim]=transformed2;

		flagSuccess = flagSuccess && (flagPastEnd || (flagSuccessP && flagSuccessM));
		flagBreak= flagBreak || !flagSuccess || (index>nDim);
	#pragma omp flush(flagBreak)
	}	//#pragma omp critical(SUT_R1)

	}	//	while(!flagBreak)

	result_type output;
	if(flagBreak && !flagSuccess)
		return result_type();
	else
		//Compute the mean and the covariance
		output=problem.ComputeTransformedGaussian(transformedSamples, wMeanCentre, wCovCentre, wPeriphery);

	if(!output || !(output->IsValid()))
		return result_type();

	return output;
}	//Run(ProblemT& problem, const input_type& input, double alpha=1, double kappa=0, double gamma=2) -> optional<result_type>

}	//UncertaintyEstimationN
}	//SeeSawN

#endif /* SCALED_UNSCENTED_TRANSFORMATION_IPP_0909212 */
