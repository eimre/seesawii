/**
 * @file SUTGeometryEstimationProblem.ipp Implementation of SUTGeometryEstimationProblemC
 * @author Evren Imre
 * @date 5 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SUT_GEOMETRY_ESTIMATION_PROBLEM_IPP_8012304
#define SUT_GEOMETRY_ESTIMATION_PROBLEM_IPP_8012304

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <type_traits>
#include <vector>
#include <list>
#include <map>
#include <cmath>
#include <cstddef>
#include <climits>
#include <iterator>
#include "../Geometry/GeometrySolverConcept.h"
#include "../Geometry/Homography2DSolver.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Elements/Correspondence.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace UncertaintyEstimationN
{

using boost::optional;
using Eigen::Matrix;
using std::true_type;
using std::vector;
using std::list;
using std::multimap;
using std::sqrt;
using std::size_t;
using std::numeric_limits;
using std::next;
using std::is_same;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::GeometryN::Homography2DSolverC;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ColsM;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::NumericN::ComputeSquareRootSVD;

/**
 * @brief SUT problem for geometry estimators
 * @tparam GeometrySolverT Geometry solver type
 * @pre \c GeometrySolverT is a model of \c GeometrySolverConceptC
 * @pre \c GeometrySolverT::coordinate_type1 is a fixed-size Eigen dense object (unenforced)
 * @pre \c GeometrySolverT::coordinate_type2 is a fixed-size Eigen dense object (unenforced)
 * @remarks Input covariance matrix is not fixed-size, as it is not possible to predict the generator size for non-minimal problems at compile-time
 * @ingroup Problem
 */
template<class GeometrySolverT>
class SUTGeometryEstimationProblemC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<GeometrySolverT>));
	static_assert(RowsM<typename GeometrySolverT::coordinate_type1>::value!=Eigen::Dynamic, "SUTGeometryEstimationProblemC : GeometrySolverT::coordinate_type1 must be a fixed size vector.");
	static_assert(ColsM<typename GeometrySolverT::coordinate_type1>::value==1, "SUTGeometryEstimationProblemC : GeometrySolverT::coordinate_type1 must be a fixed size vector.");
	static_assert(RowsM<typename GeometrySolverT::coordinate_type2>::value!=Eigen::Dynamic, "SUTGeometryEstimationProblemC : GeometrySolverT::coordinate_type2 must be a fixed size vector.");
	static_assert(ColsM<typename GeometrySolverT::coordinate_type2>::value==1, "SUTGeometryEstimationProblemC : GeometrySolverT::coordinate_type2 must be a fixed size vector.");
	///@endcond

	private:

		typedef typename GeometrySolverT::real_type RealT;	///< Floating point type
		typedef typename GeometrySolverT::coordinate_type1 Coordinate1T;	///< Type of a coordinate in the first domain
		typedef typename GeometrySolverT::coordinate_type2 Coordinate2T;	///< Type of a coordinate in the second domain
		typedef typename GeometrySolverT::error_type GeometricErrorT;	///< Type of the geometric error

		//Coordinates are fixed size, as promised by GeometrySolverConceptC
		static constexpr unsigned int nDim1=RowsM<Coordinate1T>::value;	///< Dimensionality of the first domain
		static constexpr unsigned int nDim2=RowsM<Coordinate2T>::value;	///< Dimensionality of the second domain

		typedef Matrix<RealT, Eigen::Dynamic, 1> VectorT;	///< A real vector

		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CorrespondenceContainerT;	///< A container for correspondences
		typedef Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> InputCovarianceT;	///< Type of the input covariance matrix
		typedef typename GeometrySolverT::model_type ModelT;	///< A transformed sample

		/** @name Configuration */ //@{
		bool flagValid;	///< \c true if the problem is initialised

		GeometrySolverT solver;	///< Geometry solver

		CorrespondenceContainerT generator;	///< Generator set
		InputCovarianceT generatorCovariance;	///< Generator covariance matrix
		bool flagDiagonal;	///< Diagonal input covariance matrix

		unsigned int inputDimensionality;	///< Dimensionality of the generator

		constexpr static bool flagValidator=GeometrySolverT::MaxSolution()>1;	///< \c true if the geometry solver can return multiple solutions

		//If multiple solutions possible
		optional<CorrespondenceContainerT> validators;	///< Validation set
		optional<GeometricErrorT> evaluator;	///< Geometric error functor
		//@}

	public:

		/** @name PerturbationAnalysisProblemInterfaceC */ //@{

		SUTGeometryEstimationProblemC();	///< Default constructor

		bool IsValid() const;	///< Returns \c flagValid

		static bool NeedValidator();	///< Returns \c flagValidator

		typedef InputCovarianceT input_covariance_type;	///< Type of the input covariance matrix

		typedef true_type can_decompose_covariance;	///< Dedicated covariance factorisation
		const input_covariance_type& GetCovariance() const;	///< Returns a constant reference to the input covariance matrix
		input_covariance_type DecomposeCovariance() const;	///< Factorises the input covariance matrix
		unsigned int InputDimensionality();	///< Returns the dimensionality of the input space (i.e., generator set)

		RealT Alpha() const;	///< Returns the recommended alpha

		typedef ModelT transformed_sample_type;	///< Model estimated from the generator
		bool TransformPerturbed(transformed_sample_type& transformed, const VectorT& perturbation) const;	///< Estimates a model from a perturbed generator

		typedef typename GeometrySolverT::uncertainty_type transformed_gaussian_type;	///< Type of the Gaussian for the estimated model
		transformed_gaussian_type ComputeTransformedGaussian(const vector<transformed_sample_type>& models, RealT wMean0, RealT wCovariance0, RealT wPeripheral) const;	///< Computes the sample statistics of the perturbed models
		//@}

		SUTGeometryEstimationProblemC(const CorrespondenceContainerT& ggenerator, const InputCovarianceT& ggeneratorCovariance, const GeometrySolverT& ssolver, const optional<CorrespondenceContainerT>& vvalidators, const optional<GeometricErrorT>& eevaluator);	///< Constructor
		SUTGeometryEstimationProblemC(const CorrespondenceContainerT& ggenerator, RealT noiseVariance, const GeometrySolverT& ssolver, const optional<CorrespondenceContainerT>& vvalidators, const optional<GeometricErrorT>& eevaluator);	///< Constructor, diagonal covariance matrix variant
};	//class SUTGeometryEstimationProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 */
template<class GeometrySolverT>
SUTGeometryEstimationProblemC<GeometrySolverT>::SUTGeometryEstimationProblemC() : flagValid(false), flagDiagonal(false), inputDimensionality(0)
{}

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class GeometrySolverT>
bool SUTGeometryEstimationProblemC<GeometrySolverT>::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Returns \c flagValidator
 * @return \c flagValidator
 */
template<class GeometrySolverT>
bool SUTGeometryEstimationProblemC<GeometrySolverT>::NeedValidator()
{
	return flagValidator;
}	//bool NeedValidator()

/**
 * @brief Returns a constant reference to the input covariance matrix
 * @return A constant reference to \c generatorCovariance
 * @pre \c flagValid=true
 */
template<class GeometrySolverT>
auto SUTGeometryEstimationProblemC<GeometrySolverT>::GetCovariance() const -> const input_covariance_type&
{
	//Preconditions
	assert(flagValid);

	return generatorCovariance;
}	//auto GetCovariance() const -> const input_covariance_type&

/**
 * @brief Factorises the input covariance matrix
 * @return Square-root of the covariance matrix
 * @pre \c flagValid=true
 */
template<class GeometrySolverT>
auto SUTGeometryEstimationProblemC<GeometrySolverT>::DecomposeCovariance() const -> input_covariance_type
{
	//Preconditions
	assert(flagValid);

	InputCovarianceT output(generatorCovariance.rows(), generatorCovariance.cols());
	output.setZero();

	//Block-diagonal
	size_t sGenerator=generator.size();	//Generator might be nonminimal!
	if(!flagDiagonal)
	{
		size_t index=0;
		for(size_t c=0; c<sGenerator; ++c)
		{
			output.block(index, index, nDim1, nDim1)=ComputeSquareRootSVD<Eigen::ColPivHouseholderQRPreconditioner>(generatorCovariance.block(index, index, nDim1, nDim1).eval());
			index+=nDim1;

			output.block(index, index, nDim2, nDim2)=ComputeSquareRootSVD<Eigen::ColPivHouseholderQRPreconditioner>(generatorCovariance.block(index, index, nDim2, nDim2).eval());
			index+=nDim2;
		}	//for(size_t c=0; c<sGenerator; ++c)
	}	//if(!flagDiagonal)
	else
	{
		//Diagonal
		for(size_t c=0; c<inputDimensionality; ++c)
			output(c,c)=sqrt(generatorCovariance(c,c));
	}	//if(!flagDiagonal)

	return output;
}	//input_covariance_type DecomposeCovariance() const

/**
 * @brief Returns the dimensionality of the input space (i.e., generator set)
 * @return \c inputDimensionality
 */
template<class GeometrySolverT>
unsigned int SUTGeometryEstimationProblemC<GeometrySolverT>::InputDimensionality()
{
	return inputDimensionality;
}	//unsigned int InputDimensionality()

/**
 * @brief Returns the recommended alpha
 * @return Alpha
 * @pre \c flagValid=true
 */
template<class GeometrySolverT>
auto SUTGeometryEstimationProblemC<GeometrySolverT>::Alpha() const -> RealT
{
	//Preconditions
	assert(flagValid);

	//is_same type detection for special cases

	//TODO For stability, eventually may be set to 1 for all projective entities
	if(is_same<GeometrySolverT, Homography2DSolverC>::value)
		return 1;

	//Recommended value
	return sqrt(3.0/inputDimensionality);
}	//RealT Alpha()

/**
 * @brief Estimates a model from a perturbed generator
 * @param[out] transformed Estimated model
 * @param[in] perturbation Perturbation to the generator
 * @return \c false if the estimator fails to return any models, or there are multiple models, but no validators.
 * @pre \c flagValid=true
 */
template<class GeometrySolverT>
bool SUTGeometryEstimationProblemC<GeometrySolverT>::TransformPerturbed(transformed_sample_type& transformed, const VectorT& perturbation) const
{
	//Preconditions
	assert(flagValid);

	//Perturb the generator
	CorrespondenceContainerT perturbedGenerator(generator);

	size_t c=0;
	for(auto& current : perturbedGenerator)
	{
		current.left+=perturbation.segment(c, nDim1);
		c+=nDim1;

		current.right+=perturbation.segment(c, nDim2);
		c+=nDim2;
	}	//for(auto& current : perturbedGenerator)

	//Estimate
	list<ModelT> models=solver(perturbedGenerator);

	if(models.empty())
		return false;

	//In the case of multiple models, choose the best
	if(models.size()>1)
	{
		if(validators)
		{
			multimap<RealT, size_t> errorMap;
			size_t modelIndex=0;
			for(const auto& current : models)
			{
				RealT error=GeometrySolverT::EvaluateModel(current, *validators, *evaluator);
				errorMap.emplace(error, modelIndex);
				++modelIndex;
			}	//for(const auto& current : models)

			transformed= *next(models.begin(), errorMap.begin()->second);
		}	//if(validators)
		else
			return false;
	}	//if(models.size()>1)
	else
		transformed=*models.begin();

	return true;
}	//bool TransformPerturbed(transformed_sample_type& transformed, const VectorT& perturbation) const

/**
 * @brief Computes the sample statistics of the perturbed models
 * @param[in] models Models
 * @param[in] wMean0 Weight of the central sample for mean
 * @param[in] wCovariance0 Weight of the central sample for covariance
 * @param[in] wPeripheral Weight for the peripheral samples
 * @return Sample statistics as a Gaussian
 */
template<class GeometrySolverT>
auto SUTGeometryEstimationProblemC<GeometrySolverT>::ComputeTransformedGaussian(const vector<transformed_sample_type>& models, RealT wMean0, RealT wCovariance0, RealT wPeripheral) const ->transformed_gaussian_type
{
	//Preconditions
	assert(flagValid);

	size_t nSample=boost::distance(models);
	vector<RealT> wMean(nSample, wPeripheral); wMean[0]=wMean0;
	vector<RealT> wCovariance(wMean); wCovariance[0]=wCovariance0;
	return GeometrySolverT::ComputeSampleStatistics(models, wMean, wCovariance, validators ? *validators : CorrespondenceContainerT());
}	//auto ComputeTransformedGaussian(const vector<transformed_sample_type>& models, RealT wMean0, RealT wCovariance0, RealT wPeripehral) const ->transformed_gaussian_type

/**
 * @brief Constructor
 * @param[in] ggenerator Generator set
 * @param[in] ggeneratorCovariance Covariance of the generator set
 * @param[in] ssolver Geometry solver
 * @param[in] vvalidators Validation set for solvers yielding multiple hypotheses. If not needed, invalid
 * @param[in] eevaluator Evaluator for the validators. If not needed, invalid
 * @post A valid object
 */
template<class GeometrySolverT>
SUTGeometryEstimationProblemC<GeometrySolverT>::SUTGeometryEstimationProblemC(const CorrespondenceContainerT& ggenerator, const InputCovarianceT& ggeneratorCovariance, const GeometrySolverT& ssolver, const optional<CorrespondenceContainerT>& vvalidators, const optional<GeometricErrorT>& eevaluator) : flagValid(true), solver(ssolver), generator(ggenerator), generatorCovariance(ggeneratorCovariance), flagDiagonal(false)
{
	inputDimensionality=generator.size() * (nDim1+nDim2);
	if(flagValidator && vvalidators && eevaluator)
	{
		validators=vvalidators;
		evaluator=eevaluator;
	}	//if(GeometrySolverT::MaxSolution()>1 && vvalidators && eevaluator)
}	//SUTGeometryEstimationProblemC(const CorrespondenceContainerT& ggenerator, const InputCovarianceT& ggeneratorCovariance, const GeometrySolverT& ssolver, const optional<CorrespondenceContainerT>& vvalidators, const optional<GeometricErrorT>& eevaluator)

/**
 * @brief Constructor, diagonal covariance matrix variant
 * @param[in] ggenerator Generator set
 * @param[in] noiseVariance Variance of the coordinate noise
 * @param[in] ssolver Geometry solver
 * @param[in] vvalidators Validation set for solvers yielding multiple hypotheses. If not needed, invalid
 * @param[in] eevaluator Evaluator for the validators. If not needed, invalid
 * @post A valid object
 */
template<class GeometrySolverT>
SUTGeometryEstimationProblemC<GeometrySolverT>::SUTGeometryEstimationProblemC(const CorrespondenceContainerT& ggenerator, RealT noiseVariance, const GeometrySolverT& ssolver, const optional<CorrespondenceContainerT>& vvalidators, const optional<GeometricErrorT>& eevaluator) : flagValid(true), solver(ssolver), generator(ggenerator), flagDiagonal(true)
{
	inputDimensionality=generator.size() * (nDim1+nDim2);
	VectorT diag(inputDimensionality); diag.setConstant(noiseVariance);
	generatorCovariance=diag.asDiagonal();
	if( flagValidator && vvalidators && eevaluator)
	{
		validators=vvalidators;
		evaluator=eevaluator;
	}	//if(GeometrySolverT::MaxSolution()>1 && vvalidators && eevaluator)
}	//SUTGeometryEstimationProblemC(const CorrespondenceContainerT& ggenerator, const InputCovarianceT& ggeneratorCovariance, const GeometrySolverT& ssolver, const optional<CorrespondenceContainerT>& vvalidators, const optional<GeometricErrorT>& eevaluator)

}	//UncertaintyEstimationN
}	//SeeSawN

#endif /* SUT_GEOMETRY_ESTIMATION_PROBLEM_IPP_8012304 */
