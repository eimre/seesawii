/**
 * @file TestUncertaintyEstimation.cpp Unit tests for UncertaintyEstimation
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE UNCERTAINTY_ESTIMATION

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <stdexcept>
#include <cmath>
#include "../Elements/Correspondence.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Camera.h"
#include "../Geometry/Homography2DSolver.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/NumericFunctor.h"
#include "MultivariateGaussian.h"
#include "ScaledUnscentedTransformation.h"
#include "SUTGeometryEstimationProblem.h"
#include "SUTTriangulationProblem.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestUncertaintyEstimationN
{

using namespace SeeSawN::UncertaintyEstimationN;

using boost::optional;
using Eigen::Matrix2d;
using Eigen::MatrixXd;
using Eigen::Vector2d;
using Eigen::Matrix;
using std::invalid_argument;
using std::sqrt;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::GeometryN::Homography2DSolverC;
using SeeSawN::MetricsN::SymmetricTransferErrorH2DT;

BOOST_AUTO_TEST_SUITE(Distributions)

BOOST_AUTO_TEST_CASE(Multivariate_Gaussian)
{
	MultivariateGaussianC<MatrixDifferenceC<Vector2d>,2 > gaussian;
	BOOST_CHECK(!gaussian.IsValid());

	Matrix2d mCov; mCov<<3, 1, 1, 3;
	Vector2d vMean; vMean<<2, 0;
	MultivariateGaussianC<MatrixDifferenceC<Vector2d>,2 > gaussian2(vMean, mCov);
	BOOST_CHECK(gaussian2.GetMean()==vMean);
	BOOST_CHECK(gaussian2.GetCovariance()==mCov);
	BOOST_CHECK(gaussian2.IsValid());

	gaussian.SetMean(vMean);
	gaussian.SetCovariance(mCov);
	BOOST_CHECK(gaussian.GetMean()==vMean);
	BOOST_CHECK(gaussian.GetCovariance()==mCov);

	Vector2d vSample; vSample<<1,1;
	BOOST_CHECK_EQUAL(*gaussian.ComputeMahalonobisDistance(vSample), *gaussian2.ComputeMahalonobisDistance(vSample));
	BOOST_CHECK_EQUAL(*gaussian.EvaluatePDF(vSample), *gaussian2.EvaluatePDF(vSample));
	BOOST_CHECK_CLOSE(*gaussian.ComputeMahalonobisDistance(vSample), 1, 1e-6);
	BOOST_CHECK_CLOSE(*gaussian.EvaluatePDF(vSample), 0.034129, 1e-3);

	Matrix2d mCovDeg; mCovDeg<<1, 0, 0, 0;
	gaussian.SetCovariance(mCovDeg);
	BOOST_CHECK(gaussian.GetMean()==vMean);
	BOOST_CHECK(gaussian.GetCovariance()==mCovDeg);
	BOOST_CHECK(!gaussian.ComputeMahalonobisDistance(vSample));
	BOOST_CHECK(!gaussian.EvaluatePDF(vSample));
}	//BOOST_AUTO_TEST_CASE(Multivariate_Gaussian)

BOOST_AUTO_TEST_SUITE_END() //Distributions

BOOST_AUTO_TEST_SUITE(Scaled_Unscented_Transformation)

BOOST_AUTO_TEST_CASE(SUT_Geometry_Estimation_Problem)
{
	//Constructors

	//Default
	typedef SUTGeometryEstimationProblemC<Homography2DSolverC> SUTHomography2DProblemT;
	SUTHomography2DProblemT problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Diagonal covariance

	CoordinateCorrespondenceList2DT generator;
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(1,1), Coordinate2DT(3,4)));
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(1,-1), Coordinate2DT(3,-2)));
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-2,1)));
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(0,0), Coordinate2DT(0.5,1)));

	Homography2DT mH; mH<<2.5, 0, 0.5, 0, 3, 1, 0, 0, 1; mH.normalize();

	Homography2DSolverC solver;
	optional<CoordinateCorrespondenceList2DT> validator;
	optional<SymmetricTransferErrorH2DT> evaluator;

	SUTHomography2DProblemT problem2(generator, 4, solver, validator, evaluator);

	BOOST_CHECK(problem2.IsValid());

	//General covariance
	SUTHomography2DProblemT::input_covariance_type mCovariance(16,16); mCovariance.setIdentity(); mCovariance*=4;
	SUTHomography2DProblemT problem3(generator, 4, solver, validator, evaluator);

	//Operations

	BOOST_CHECK_EQUAL(problem2.InputDimensionality(),16);

	BOOST_CHECK(mCovariance==problem2.GetCovariance());
	BOOST_CHECK(mCovariance.isApprox(problem2.DecomposeCovariance()*problem2.DecomposeCovariance().transpose()));

	BOOST_CHECK(mCovariance==problem3.GetCovariance());
	BOOST_CHECK(mCovariance.isApprox(problem3.DecomposeCovariance()*problem3.DecomposeCovariance().transpose()));

	SUTHomography2DProblemT::transformed_sample_type mHP1;
	Matrix<Homography2DSolverC::real_type, 16, 1> vPerturbation1; vPerturbation1.setZero();
	bool flagPerturbed1=problem2.TransformPerturbed(mHP1, vPerturbation1);

	BOOST_CHECK(flagPerturbed1);
	BOOST_CHECK(mH.isApprox(mHP1, 1e-6));

	SUTHomography2DProblemT::transformed_sample_type mHP2;
	Matrix<Homography2DSolverC::real_type, 16, 1> vPerturbation2; vPerturbation2.setZero(); vPerturbation2[0]=1;
	bool flagPerturbed2=problem2.TransformPerturbed(mHP2, vPerturbation2);

	Homography2DT mHP2r; mHP2r<<  0.465923, -0.232961, 0.107521, 0.0358402, 0.788485, 0.215041, 0.0358402, -0.0179201, 0.215041;

	BOOST_CHECK(flagPerturbed2);
	BOOST_CHECK(mHP2r.isApprox(mHP2, 1e-4));

	vector<Homography2DT> sampleSet(8);
	sampleSet[0]<<1, 0, 2, 0, 1, 0, 0, 0, 3;
	sampleSet[1]<<2, 1, 2, 0, 1, 0, 06, 0, 3;
	sampleSet[2]<<1, 0, 2, 9, 1, 2, 1, 0, 3;
	sampleSet[3]<<1, 0, 3, 4, 1, 0, 0, 0, 3;
	sampleSet[4]<<1, 0, 2, 0, 1, 4, 0, 0, 3;
	sampleSet[5]<<1, 6, 2, 3, 1, 0, 2, 0, 3;
	sampleSet[6]<<1, 0, 2, 1, 1, 0, 8, 4, 3;
	sampleSet[7]<<1, 9, 2, 3, 1, 2, 0, 5, 3;

	SUTHomography2DProblemT::transformed_gaussian_type sampleStatistics;
	sampleStatistics=problem2.ComputeTransformedGaussian(sampleSet, 2.0/8, 2.0/8, 3.0/28);

	BOOST_CHECK(!SUTHomography2DProblemT::NeedValidator());

	BOOST_CHECK_CLOSE(problem2.Alpha(), 1, 1e-4);	//For homography estimation problem, set to 1 for stability
}	//BOOST_AUTO_TEST_CASE(SUT_Geometry_Estimation_Problem)

BOOST_AUTO_TEST_CASE(SUT_Triangulation_Problem)
{
	//Dummy constructor
	SUTTriangulationProblemC problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Constructor

	CameraMatrixT mP1; mP1<<1000, 1e-5, 960, 0, 0, 1001, 540, 0, 0, 0, 1, 0;
	CameraMatrixT mP2; mP2<< -5.4429e+02, 1.2602e+03, -1.9320e+02, -6.7811e+02, 7.8476e+02, 6.7894e+02, -4.6560e+02, -9.0110e+02, -2.5070e-01, 5.2925e-01, -8.1058e-01, 8.3148e-01;
	Coordinate2DT x1(1960,1541);
	Coordinate2DT x2(-519,324);

	SUTTriangulationProblemC problemO(x1, x2, 1, mP1, mP2, false);
	SUTTriangulationProblemC problemF(x1, x2, 1, mP1, mP2, true);

	//Operations

	BOOST_CHECK(problemO.IsValid());
	BOOST_CHECK_EQUAL(problemO.InputDimensionality(), 4);
	BOOST_CHECK(!problemO.NeedValidator());
	BOOST_CHECK_EQUAL(problemO.Alpha(), sqrt(0.75));

	SUTTriangulationProblemC::input_covariance_type mC; mC.setIdentity(); mC*=1;
	BOOST_CHECK(mC==problemO.GetCovariance());
	BOOST_CHECK(mC.cwiseSqrt()==problemO.DecomposeCovariance());

	Coordinate3DT z1;
	BOOST_CHECK(problemO.TransformPerturbed(z1, problemO.DecomposeCovariance().diagonal()));

	Coordinate3DT z1gt(1.0002, 0.999922, 0.999099);
	BOOST_CHECK(z1.isApprox(z1gt, 1e-4));

	vector<Coordinate3DT> listZ(4);
	listZ[0]=Coordinate3DT(1,2,1);
	listZ[1]=Coordinate3DT(3,3,2);
	listZ[2]=Coordinate3DT(5,4,7);
	listZ[3]=Coordinate3DT(2,1,4);

	SUTTriangulationProblemC::transformed_gaussian_type gaussian=problemO.ComputeTransformedGaussian(listZ, 0.4, 0.4, 0.2);

	Coordinate3DT meanGT(2.4, 2.4, 3);
	BOOST_CHECK(gaussian.GetMean().isApprox(meanGT, 1e-6));

	SUTTriangulationProblemC::transformed_gaussian_type::covariance_type mCT=gaussian.GetCovariance();
	SUTTriangulationProblemC::transformed_gaussian_type::covariance_type mCTgt; mCTgt<<2.2400, 1.2400, 3.0000, 1.2400, 1.0400, 1.2000, 3.0000, 1.2000, 5.2000;
	BOOST_CHECK(gaussian.GetCovariance().isApprox(mCTgt, 1e-4));

	//Numerical Jacobian interface
	Matrix<double, 4, 1> inputVector=problemO.GetInputVector();
	BOOST_CHECK(inputVector.segment(0,2)==x1);
	BOOST_CHECK(inputVector.segment(2,2)==x2);

	Coordinate3DT outputVector=problemO.MakeOutputVector(z1);
	BOOST_CHECK(outputVector==z1);

	//Engine
	ScaledUnscentedTransformationParametersC parameters;
	ScaledUnscentedTransformationC<SUTTriangulationProblemC>::Run(problemO, parameters);

}	//BOOST_AUTO_TEST_CASE(SUT_Triangulation_Problem)

BOOST_AUTO_TEST_CASE(SUT_Engine)
{
	typedef SUTGeometryEstimationProblemC<Homography2DSolverC> SUTHomography2DProblemT;

	//Problem
	CoordinateCorrespondenceList2DT generator;
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(1,1), Coordinate2DT(3,4)));
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(1,-1), Coordinate2DT(3,-2)));
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(-1,0), Coordinate2DT(-2,1)));
	generator.push_back(CoordinateCorrespondenceList2DT::value_type(Coordinate2DT(0,0), Coordinate2DT(0.5,1)));

	Homography2DT mH; mH<<2.5, 0, 0.5, 0, 3, 1, 0, 0, 1;

	Homography2DSolverC solver;
	optional<CoordinateCorrespondenceList2DT> validator;
	optional<SymmetricTransferErrorH2DT> evaluator;

	SUTHomography2DProblemT problem(generator, 4, solver, validator, evaluator);

	//Engine

	ScaledUnscentedTransformationParametersC parameters;

	typedef ScaledUnscentedTransformationC<SUTHomography2DProblemT> SUTHomography2DEngineT;
	SUTHomography2DEngineT::result_type transformedGaussian1=SUTHomography2DEngineT::Run(problem, parameters);

	BOOST_CHECK(transformedGaussian1);
	BOOST_CHECK_SMALL(*transformedGaussian1->ComputeMahalonobisDistance(transformedGaussian1->GetMean()), 1e-6);

	Homography2DSolverC::minimal_model_type minimalH=Homography2DSolverC::MakeMinimal(mH);
	BOOST_CHECK_CLOSE(*transformedGaussian1->ComputeMahalonobisDistance(minimalH), 0.4149755, 5e-4);

	//Invalid problem
	SUTHomography2DProblemT problem2;
	BOOST_CHECK_THROW(SUTHomography2DEngineT::Run(problem2, parameters), invalid_argument);
}	//BOOST_AUTO_TEST_CASE(SUT_Engine)

BOOST_AUTO_TEST_SUITE_END()	//Scaled_Unscented_Transformation

}   //TestUncertaintyEstimationN
}   //UnitTestsN
}	//SeeSawN


