/**
 * @file ScaledUnscentedTransformation.h Public interface for TransformedCovarianceEstimatorC
 * @author Evren Imre
 * @date 4 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCALED_UNSCENTED_TRANSFORMATION_H_0909212
#define SCALED_UNSCENTED_TRANSFORMATION_H_0909212

#include "ScaledUnscentedTransformation.ipp"
namespace SeeSawN
{
namespace UncertaintyEstimationN
{

template<class ProblemT> class ScaledUnscentedTransformationC;	///< Implementation of scaled unscented transformation algorithm

}	//UncertaintyEstimationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
#endif /* SCALED_UNSCENTED_TRANSFORMATION_H_0909212 */
