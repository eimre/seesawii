/**
 * @file SUTTriangulationProblem.ipp Implementation of the SUT problem for 2-view triangulation
 * @author Evren Imre
 * @date 6 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SUT_TRIANGULATION_PROBLEM_IPP_3989245
#define SUT_TRIANGULATION_PROBLEM_IPP_3989245

#include <Eigen/Dense>
#include <type_traits>
#include <vector>
#include <cmath>
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Geometry/Triangulation.h"
#include "../Numeric/NumericFunctor.h"
#include "MultivariateGaussian.h"

namespace SeeSawN
{
namespace UncertaintyEstimationN
{

using Eigen::Matrix;
using std::true_type;
using std::vector;
using std::sqrt;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::GeometryN::TriangulatorC;
using SeeSawN::NumericN::MatrixDifferenceC;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;

/**
 * @brief SUT problem for 2-view triangulation
 * @remarks A model of \c PerturbationAnalysisProblemConceptC
 * @remarks A model of \c NumericalJacobianProblemConceptC
 * @ingroup Problem
 * @nosubgrouping
 */
class SUTTriangulationProblemC
{
	private:

		static constexpr unsigned int NDIM=4;	///< Input dimensionality
		typedef ValueTypeM<Coordinate2DT>::type RealT;	///< Floating point type
		typedef Matrix<RealT, NDIM, NDIM> InputCovarianceT;	///< Input covariance type
		typedef Matrix<RealT, NDIM, 1> PerturbationT;	///< Perturbation type

		/** @name Configuration */ //@{
		bool flagValid;	///< \c true if the object is initialised
		double inputVariance;	///< Variance of the noise on the image coordinates
		Coordinate2DT x1;	///< Projection in the first image
		Coordinate2DT x2;	///< Projection in the second image
		CameraMatrixT mP1;	///< First camera matrix
		CameraMatrixT mP2;	///< Second camera matrix
		bool flagFast;	///< If \c true , fast triangulation. Otherwise, optimal triangulation
		//@}

	public:

		/**@name PerturbationAnalysisProblemConceptC interface */ //@{

		typedef InputCovarianceT input_covariance_type;	///< Input covariance type
		typedef Coordinate3DT transformed_sample_type;	///< Transformed sample
		typedef MultivariateGaussianC< MatrixDifferenceC<Coordinate3DT>, 3, RealT> transformed_gaussian_type;	///< Transformed Gaussian
		typedef true_type can_decompose_covariance;	///< Has a dedicated facility for covariance decomposition

		bool IsValid() const;	///< Returns \c flagValid
		InputCovarianceT GetCovariance() const;	///< Returns the input covariance
		InputCovarianceT DecomposeCovariance() const;	///< Decomposes the covariance into two symmetric factors
		static unsigned int InputDimensionality();	///< Dimensionality of the input sample, which is 4

		bool TransformPerturbed(Coordinate3DT& transformed, const PerturbationT& perturbation) const;	///< Transforms a perturbed sample

		transformed_gaussian_type ComputeTransformedGaussian(const vector<Coordinate3DT>& transformed, double wMean0, double wCovariance0, double wPeripheral) const;	///< Computes the transformed Gaussian

		static bool NeedValidator();	///< Returns \c false
		static double Alpha();	///< Computes the recommended alpha value
		//@}

		/** @name NumericalJacobianProblemConceptC interface */ ///@{
		typedef RealT real_type;	///< Floating point type
		Matrix<RealT, NDIM, 1> GetInputVector() const;	///< Returns the 2D projections as an vector
		static Coordinate3DT MakeOutputVector(const Coordinate3DT& src);	///< Converts a transformed sample into a vector
		///@}

		/**@name Constructors */ //@{
		SUTTriangulationProblemC();	///< Default constructor
		SUTTriangulationProblemC(const Coordinate2DT& xx1, const Coordinate2DT& xx2, RealT noiseVariance, const CameraMatrixT& mmP1, const CameraMatrixT& mmP2, bool fflagFast=true);
		//@}

};	//class SUTTriangulationProblemC

}	//UncertaintyEstimationN
}	//SeeSawN

#endif /* SUT_TRIANGULATION_PROBLEM_IPP_3989245 */
