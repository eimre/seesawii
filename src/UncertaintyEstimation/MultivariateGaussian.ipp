/**
 * @file MultivariateGaussian.ipp Implementation of MultivariateGaussianC
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTIVARIATE_GAUSSIAN_IPP_7897096
#define MULTIVARIATE_GAUSSIAN_IPP_7897096

#include <boost/concept_check.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <cmath>
#include <type_traits>
#include "MultivariateGaussian.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace UncertaintyEstimationN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::math::constants::root_two_pi;
using boost::logic::tribool;
using boost::logic::indeterminate;
using boost::optional;
using Eigen::Matrix;
using std::pow;
using std::is_floating_point;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;

/**
 * @brief Multivariate Gaussian distribution
 * @tparam DifferenceFunctorT A difference functor for computing the difference between two samples
 * @tparam DIM Dimensionality of the space
 * @tparam RealT Floating point type
 * @pre \c DifferenceFunctorT is an adaptable binary function
 * @pre \c DIM is a positive value, or -1 (unenforced)
 * @pre \c RealT is a floating point type
 * @remarks \c mean is not necessarily a vector
 * @remarks If \c DIM is a positive value, the covariance matrix is a fixed-size structure. If -1, it is dynamic.
 * @remarks Lazy invertibility check means that the structure can hold a degenerate covariance matrix
 * @warning A valid covariance matrix should be symmetric positive definite
 * @ingroup Statistics
 * @nosubgrouping
 */
template<class DifferenceFunctorT, int DIM=Eigen::Dynamic, typename RealT=double>
class MultivariateGaussianC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DifferenceFunctorT, typename DifferenceFunctorT::result_type, typename DifferenceFunctorT::first_argument_type, typename DifferenceFunctorT::second_argument_type>));
	static_assert(is_floating_point<RealT>::value, "MultivariateGaussianC : RealT must be a floating point type");
	//@endcond

	public:
		typedef typename DifferenceFunctorT::first_argument_type mean_type;	///< Mean type
		typedef Matrix<RealT, DIM, DIM> covariance_type; ///< Covariance type

	private:

		DifferenceFunctorT difference;	///< Sample difference functor
		mean_type mean;	///< Mean
		covariance_type covariance;	///< Covariance
		covariance_type inverseCovariance;	///< Inverse of the covariance
		RealT scale;	///< Scale term for the Gaussian

		bool flagMeanSet;	///< \c true if \c mean is set
		bool flagCovarianceSet;	///< \c true if \c covariance is set
		tribool flagValidInverse;	///< \c true if the covariance matrix is invertible

		void InitialiseInverse();	///< Initialises \c inverseCovariance and related variables

	public:

		MultivariateGaussianC();	///< Default constructor
		MultivariateGaussianC(const mean_type& mmean, const covariance_type& ccovariance);	///< Constructor

		/**@name Accessors */ //@{
		void SetMean(const mean_type& mmean);	///< Sets \c mean
		const mean_type& GetMean() const;	///< Returns a constant reference to \c mean
		const covariance_type& GetCovariance() const;	///< Returns a constant reference to \c covariance
		void SetCovariance(const covariance_type& ccovariance);	///< Sets \c covariance
		bool IsValid() const;	///< \c true if the mean and the covariance are set
		//@}

		/**@name Operations */ //@{
		optional<RealT> EvaluatePDF(const mean_type& sample);	///< Evaluates the Gaussian at \c sample
		optional<RealT> ComputeMahalonobisDistance(const mean_type& sample);	///< Computes the Mahalonobis distance between the mean and a sample
		//@}
};	//class MultivariateGaussianC

/********** IMPLEMENTATION STARTS HERE **********/

/********** MultivariateGaussianC **********/

/**
 * @brief Initialises \c inverseCovariance and related variables
 * @pre \c flagCovarianceSet=true
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
void MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::InitialiseInverse()
{
	//Preconditions
	assert(flagCovarianceSet);

	RealT detC=covariance.determinant();
	flagValidInverse= (detC>0);

	if(flagValidInverse)
	{
		scale=1.0/(pow(root_two_pi<RealT>(), covariance.rows()) * sqrt(detC));
		inverseCovariance=covariance.inverse(); //TODO more stable inverse needed
	}	//if(flagValidInverse)
}	//void InitialiseInverse()

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::MultivariateGaussianC() : scale(-1), flagMeanSet(false), flagCovarianceSet(false)
{
	flagValidInverse=indeterminate;
}	//MultivariateGaussianC()

/**
 * @brief Constructor
 * @param[in] mmean Mean
 * @param[in] ccovariance Covariance
 * @post Valid object
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::MultivariateGaussianC(const mean_type& mmean, const covariance_type& ccovariance) : scale(-1)
{
	SetMean(mmean);
	SetCovariance(ccovariance);
} //MultivariateGaussianC(const mean_type& mmean, const covariance_type& ccovariance)

/**
 * @brief Sets \c mean
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
void MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::SetMean(const mean_type& mmean)
{
	mean=mmean;
	flagMeanSet=true;
}	//mean_type& Mean()

/**
 * @brief Returns a constant reference to \c mean
 * @returns A constant reference to mean
 * @pre \c flagMeanSet=true
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
auto MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::GetMean() const -> const mean_type&
{
	//Preconditions
	assert(flagMeanSet);
	return mean;
}	//const mean_type& Mean()

/**
 * @brief Returns a constant reference to \c covariance
 * @return A constant reference to covariance
 * @pre \c flagCovarianceSet=true
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
auto MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::GetCovariance() const -> const covariance_type&
{
	//Preconditions
	assert(flagCovarianceSet);
	return covariance;
}	//GetCovariance() const -> const covariance_type&

/**
 * @brief Sets \c covariance
 * @param[in] ccovariance New covariance
 * @pre \c ccovariance is a square matrix
 * @post \c flagValidInverse becomes indeterminate
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
void MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::SetCovariance(const covariance_type& ccovariance)
{
	//Preconditions
	assert(ccovariance.rows()!=ccovariance.cols());

	covariance=ccovariance;
	flagCovarianceSet=true;
	flagValidInverse=indeterminate;
}	//bool SetCovariance(const covariance_type& ccovariance)

/**
 * @brief \c true if the mean and the covariance are set
 * @return \c flagValid
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
bool MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::IsValid() const
{
	return flagMeanSet && flagCovarianceSet;
}	//bool IsValid() const

/**
 * @brief Computes the Mahalonobis distance between the mean and a sample
 * @param[in] sample A sample
 * @return Mahalonobis distance between \c mean and \c sample . If the covariance is not invertible, invalid
 * @pre \c IsValid()=true
 * @post Members associated with the inverse are initialised, if necessary
 * @warning If the covariance matrix is ill-conditioned, or not valid, the return value could be negative
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
optional<RealT> MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::ComputeMahalonobisDistance(const mean_type& sample)
{
	assert(IsValid());

	//Lazy inverse initialisation
	if(indeterminate(flagValidInverse))
		InitialiseInverse();

	if(!flagValidInverse)
		return optional<RealT>();

	typename DifferenceFunctorT::result_type dif=difference(sample, mean);
	return optional<RealT>(dif.dot((inverseCovariance*dif).eval()));
}	//double ComputeMahalonobis(const mean_type& sample)

/**
 * @brief Evaluates the Gaussian at \c sample
 * @param[in] sample Sample
 * @return The value of the pdf at \c sample . If the covariance is not invertible, invalid
 * @pre \c IsValid()=true
 */
template<class DifferenceFunctorT, int DIM, typename RealT>
optional<RealT> MultivariateGaussianC<DifferenceFunctorT, DIM, RealT>::EvaluatePDF(const mean_type& sample)
{
	assert(IsValid());

	optional<RealT> mahalonobisDistance=ComputeMahalonobisDistance(sample);

	if(!flagValidInverse)
		return optional<RealT>();
	else
		return optional<RealT>(scale*exp(-0.5 * (*mahalonobisDistance)));
}	//double EvaluatePDF(const mean_type& sample)

}	//UncertaintyEstimationN
}	//SeeSawN

#endif /* MULTIVARIATE_GAUSSIAN_IPP_7897096 */
