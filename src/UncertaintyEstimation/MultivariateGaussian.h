/**
 * @file MultivariateGaussian.h Public interface for MultivariateGaussianC
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTIVARIATE_GAUSSIAN_H_4589809
#define MULTIVARIATE_GAUSSIAN_H_4589809

#include "MultivariateGaussian.ipp"
#include "../Numeric/NumericFunctor.h"
#include "../Wrappers/EigenMetafunction.h"
namespace SeeSawN
{
namespace UncertaintyEstimationN
{
template<class DifferenceFunctorT, int DIM, typename RealT> struct MultivariateGaussianC;	///< Multivariate Gaussian distribution

/********** EXTERN TEMPLATES **********/
using SeeSawN::NumericN::MatrixDifferenceC;
using SeeSawN::WrappersN::RowsM;
using Eigen::VectorXd;
extern template class MultivariateGaussianC<MatrixDifferenceC<VectorXd>, RowsM<VectorXd>::value, double>;
}	//UncertaintyEstimationN
}	//SeeSawN

#endif /* MULTIVARIATE_GAUSSIAN_H_4589809 */
