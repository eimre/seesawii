/**
 * @file SUTTriangulationProblem.cpp Implementation of the SUT problem for 2-view triangulation
 * @author Evren Imre
 * @date 6 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SUTTriangulationProblem.h"
namespace SeeSawN
{
namespace UncertaintyEstimationN
{

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool SUTTriangulationProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid() const;

/**
 * @brief Returns the input covariance
 * @return Input covariance matrix
 * @pre \c flagValid=true
 */
auto SUTTriangulationProblemC::GetCovariance() const -> InputCovarianceT
{
	//Preconditions
	assert(flagValid);

	PerturbationT vCov; vCov.setConstant(inputVariance);
	return vCov.asDiagonal();
}	//InputCovarianceT GetCovariance() const

/**
 * @brief Decomposes the covariance into two symmetric factors
 * @return A matrix Q such that covariance=QQ'
 * @pre \c flagValid=true
 */
auto SUTTriangulationProblemC::DecomposeCovariance() const -> InputCovarianceT
{
	//Preconditions
	assert(flagValid);
	return GetCovariance().cwiseSqrt();
}	//InputCovarianceT DecomposeCovariance() const

/**
 * @brief Dimensionality of the input sample, which is 4
 * @return Input dimensionality, which is 4
 */
unsigned int SUTTriangulationProblemC::InputDimensionality()
{
	return NDIM;
}	//unsigned int InputDimensionality() const

/**
 * @brief Returns \c false
 * @return \c false
 */
bool SUTTriangulationProblemC::NeedValidator()
{
	return false;
}	//bool NeedValidator()

/**
 * @brief Computes the recommended alpha value
 * @return \c sqrt(3/NDIM)
 */
double SUTTriangulationProblemC::Alpha()
{
	return sqrt(3.0/InputDimensionality());
}	//double Alpha();

/**
 * @brief Transforms a perturbed sample
 * @param[out] transformed Transformed point
 * @param[in] perturbation Perturbation to be applied to the correspondence
 * @return \c true
 * @pre \c flagValid=true
 */
bool SUTTriangulationProblemC::TransformPerturbed(Coordinate3DT& transformed, const PerturbationT& perturbation) const
{
	//Preconditions
	assert(flagValid);
	transformed=TriangulatorC::Triangulate(mP1, mP2, x1+perturbation.segment(0,2), x2+perturbation.segment(2,2), flagFast);
	return true;
}	//bool TransformPerturbed(Coordinate3DT& transformed, const PerturbationT& perturbation)

/**
 * @brief Computes the transformed Gaussian
 * @param[in] transformed Transformed points
 * @param[in] wMean0 Weight of the first element for the computation of the mean
 * @param[in] wCovariance0 Weight of the first element for the computation of the covariance
 * @param[in] wPeripheral Weight of the remaining elements
 * @return Sample mean and covariance for \c transformed
 * @pre \c flagValid=true
 */
auto SUTTriangulationProblemC::ComputeTransformedGaussian(const vector<Coordinate3DT>& transformed, double wMean0, double wCovariance0, double wPeripheral) const -> transformed_gaussian_type
{
	//Preconditions
	assert(flagValid);

	transformed_gaussian_type::mean_type meanAcc;
	transformed_gaussian_type::covariance_type covAcc;

	meanAcc=wMean0*transformed[0];
	covAcc=wCovariance0* (transformed[0]*transformed[0].transpose());

	size_t nSample=transformed.size();
	for(size_t c=1; c<nSample; ++c)
		meanAcc+=wPeripheral*transformed[c];

	transformed_gaussian_type::mean_type delta=transformed[0]-meanAcc;
	covAcc=wCovariance0* (delta*delta.transpose());
	for(size_t c=1; c<nSample; ++c)
	{
		delta=transformed[c]-meanAcc;
		covAcc+=wPeripheral*(delta*delta.transpose());
	}	//for(size_t c=1; c<nSample; ++c)

	return transformed_gaussian_type(meanAcc, covAcc);
}	//transformed_gaussian_type ComputeTransformedGaussian(const vector<Coordinate3DT>& transformed, double wMean0, double wCovariance0, double wPeripheral) const

/**
 * @brief Returns the 2D projections as an vector
 * @return Input in vector form
 */
auto SUTTriangulationProblemC::GetInputVector() const -> Matrix<RealT, NDIM, 1>
{
	Matrix<RealT, NDIM, 1> output;
	output.segment(0,2)=x1;
	output.segment(2,2)=x2;
	return output;
}	//PerturbationT GetInputVector()

/**
 * @brief Converts a transformed sample into a vector
 * @param[in] src Transformed sample
 * @return \c src
 */
Coordinate3DT SUTTriangulationProblemC::MakeOutputVector(const Coordinate3DT& src)
{
	return src;
}	//Coordinate3DT MakeOutputVector(const Coordinate3DT& src)

/**
 * @brief Default constructor
 * @post Invalid object
 */
SUTTriangulationProblemC::SUTTriangulationProblemC() : flagValid(false), inputVariance(0), flagFast(false)
{}

/**
 * @brief Constructor
 * @post Object is valid
 */
SUTTriangulationProblemC::SUTTriangulationProblemC(const Coordinate2DT& xx1, const Coordinate2DT& xx2, RealT noiseVariance, const CameraMatrixT& mmP1, const CameraMatrixT& mmP2, bool fflagFast) : flagValid(true), inputVariance(noiseVariance), x1(xx1), x2(xx2), mP1(mmP1), mP2(mmP2), flagFast(fflagFast)
{}


}	//UncertaintyEstimationN
}	//SeeSawN

