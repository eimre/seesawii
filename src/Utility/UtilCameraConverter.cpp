/**
 * @file UtilCameraConverter.cpp Implementation for cameraConverter
 * @author Evren Imre
 * @date 20 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <Eigen/Dense>
#include <set>
#include "../Application/Application.h"
#include "../IO/CameraIO.h"
#include "../IO/ArrayIO.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace  UtilCameraConverterN
{

using namespace SeeSawN::ApplicationN;

using Eigen::ArrayXXd;
using std::set;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ArrayIOC;

/**
 * @brief Camera converter parameters
 * @ingroup Utility
 */
struct UtilCameraConverterParametersC
{
	string sourceType;	///< Type of the input file
	string destinationType;	///< Type of the output file
	string source;	///< Filename for the source
	string destination;	///< Destination filename
	unsigned int width;	///< Image width
	unsigned int height;	///< Image height
};	//struct UtilCameraConverterParametersC

/**
 * @brief Implementation of cameraConverter
 * @ingroup Utility
 */
class UtilCameraConverterImplementationC
{
	private:
		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
		UtilCameraConverterParametersC parameters;	///< Parameters
		//@}
															  // Option description line cannot be se
	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		UtilCameraConverterImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, bool flagBasic);  ///< Dummy function to meet the concept requirements
		bool SetParameters(const ptree& tree);  ///< Dummy function to meet the concept requirements
		bool Run(); ///< Runs the application
    	//@}
};	//class UtilCameraConverterImplementationC

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
UtilCameraConverterImplementationC::UtilCameraConverterImplementationC()
{
	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("SourceType", value<string>()->default_value("UniS"), "Type of the input file. Valid types: UniS, SeeSaw, Mat (a list of camera matrices)")
	("DestinationType", value<string>()->default_value("SeeSaw"), "Type of the input file. Valid types: UniS, SeeSaw")
	("Source", value<string>(), "Source filename")
	("Destination", value<string>(), "Destination filename")
	("Width", value<string>()->default_value("1920"), "Width of the image, if not specified by the input file" )
	("Height", value<string>()->default_value("1080"), "Height of the image, if not specified by the input file" )

	;
}	//UtilCameraConverterImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& UtilCameraConverterImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Dummy function to meet the concept requirements
 * @param[in] filename Filename
 * @param[in] flagBasic \c true if only the basic interface is requested
 * @throws runtime_error If the write operation fails
 */
void UtilCameraConverterImplementationC::GenerateConfigFile(const string& filename, bool flagBasic)
{
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return Always true
 */
bool UtilCameraConverterImplementationC::SetParameters(const ptree& tree)
{
	parameters.sourceType=tree.get<string>("SourceType", "INV");
	parameters.destinationType=tree.get<string>("DestinationType", "INV");
	parameters.source=tree.get<string>("Source", "");
	parameters.destination=tree.get<string>("Destination","");
	parameters.height=tree.get<unsigned int>("Height", 1080);
	parameters.width=tree.get<unsigned int>("Width", 1920);

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool UtilCameraConverterImplementationC::Run()
{
	//Validation
	set<string> inputLabels{"UniS", "Mat", "SeeSaw"};
	set<string> outputLabels{"UniS", "SeeSaw"};

	if(inputLabels.find(parameters.sourceType)==inputLabels.end())
		throw(invalid_argument(string("UtilCameraConverterImplementationC::Run : Invalid source type ")+parameters.sourceType));

	if(outputLabels.find(parameters.destinationType)==outputLabels.end())
		throw(invalid_argument(string("UtilCameraConverterImplementationC::Run : Invalid destination type ")+parameters.destinationType));

	vector<CameraC> cameraList;
	if(parameters.sourceType=="UniS")
		cameraList=CameraIOC::ReadUniSFile(parameters.source);

	if(parameters.sourceType=="Mat")
	{
		ArrayXXd sourceArray=ArrayIOC<ArrayXXd>::ReadArray(parameters.source);

		//Chop into 3-row camera matrices
		size_t nLines=sourceArray.rows();
		cameraList.reserve(nLines/3);
		optional<FrameT> frame=FrameT(Coordinate2DT(0,0), Coordinate2DT((int)(parameters.width>0 ? parameters.width-1:0), (int)(parameters.height>0 ? parameters.height-1 :0)) );
		for(size_t c=0; c<nLines; c+=3)
			cameraList.push_back( CameraC(sourceArray.block(c,0,3,4), optional<LensDistortionC>(), frame) );
	}	//if(parameters.sourceType="Mat")

	if(parameters.sourceType=="SeeSaw")
		cameraList=CameraIOC::ReadCameraFile(parameters.source);

	//Output
	if(parameters.destinationType=="SeeSaw")
		CameraIOC::WriteCameraFile(cameraList, parameters.destination, "", true);

	if(parameters.destinationType=="UniS")
		CameraIOC::WriteUniSFile(cameraList, parameters.destination);

	return true;
}	//bool Run()

}	//UtilCameraConverterN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("140220");
    std::string header("cameraConverter: Various camera conversions to and from the native SeeSaw format (.cam)");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::UtilCameraConverterN::UtilCameraConverterImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
