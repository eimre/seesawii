/**
 * @file Complexity.ipp Implementation of computational complexity estimation tools
 * @author Evren Imre
 * @date 26 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COMPLEXITY_IPP_6302391
#define COMPLEXITY_IPP_6302391

#include <boost/math/special_functions/pow.hpp>
#include <map>
#include <array>
#include <cstddef>
#include <iterator>

namespace SeeSawN
{
namespace NumericN
{

using boost::math::pow;
using std::map;
using std::array;
using std::size_t;
using std::prev;

/**
 * @brief Estimates the computational complexity of various linear algebra operations
 * @remarks Mostly untested, because the costs are volatile
 * @ingroup Numerics
 * @nosubgrouping
 */
class ComplexityEstimatorC
{
	private:

		typedef array<double,4> RecordT;	///< A complexity record for a case. [double; single; GPU double; GPU single]
		typedef map<unsigned int, RecordT> TableT;	///< A table of measured costs for an operation. Indexed by complexity factor

		/** @name Configuration */ //@{
		static const TableT innerProduct;	///< Measured costs for \f$ \mathbf{x'y} \f$
		static const TableT l2Norm;	///< Measured costs for \f$ \mathbf{x'x} \f$
		static const TableT vectorAdd;	///<  Measured costs for \f$ \mathbf{z}=\mathbf{x+y} \f$
		static const TableT vectorAddAssign;	///< Measured costs for \f$ \mathbf{y+=x} \f$
		static const TableT vectorScale;	///< Measured costs for \f$ \mathbf{y}=a\mathbf{x} \f$
		static const TableT matrixVectorMultiplication;	///< Measured costs for \f$ \mathbf{y}=\mathbf{Ax} \f$
		static const TableT outerProduct;	///< Measured costs for \f$ \mathbf{xy'} \f$
		static const TableT frobeniusNorm;	///< Measured costs for frobenius norm
		static const TableT matrixScale;	///< Measured costs for \f$ \mathbf{B}=a\mathbf{A}\f$
		static const TableT matrixAdd;	///< Measured costs for \f$ \mathbf{C=A+B}\f$
		static const TableT matrixMultiplication;	///< Measured costs for \f$ \mathbf{C}=\mathbf{AB} \f$
		static const TableT inverse;	///< Measured costs for \f$ \mathbf{B}=\mathbf{A^{-1}} \f$
		static const TableT determinant;	///< Measured costs for \f$ |\mathbf{A}| \f$
		static const TableT minLinSol;	///< Measured costs for the solution of \f$ \mathbf{Ax=b}\f$
		static const TableT svd;	///< ///< Measured costs for SVD
		//@}

		/** @name Implementation details */ ///@{
		template<unsigned int N> static double Interpolate(const TableT& table, size_t n, bool flagDouble, bool flagGPU);	///< Interpolates the cost for an operation, given a table of cost measurements
		///@}

	public:

		/** @name Simple operations */ //@{
		static double InnerProduct(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{x'y} \f$
		static double L2Norm(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{x'x} \f$
		static double VectorAdd(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{z=x+y} \f$
		static double VectorAddAssign(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{y+=x} \f$
		static double VectorScale(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{y}=a\mathbf{x}\f$
		static double MatrixVectorMultiplication(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{y}=\mathbf{Ax} \f$
		static double OuterProduct(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{x'y} \f$
		static double FrobeniusNorm(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for Frobenius norm
		static double MatrixScale(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{B}=a\mathbf{A}\f$
		static double MatrixAdd(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{C=A+B}\f$
		static double MatrixMultiplication(size_t m, size_t n, size_t k, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{C}=\mathbf{AB} \f$
		static double Inverse(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ \mathbf{B}=\mathbf{A^{-1}} \f$
		static double Determinant(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for \f$ |\mathbf{A}| \f$
		static double MinimalLinearSolver(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for the solution of \f$ \mathbf{Ax=b}\f$
		static double EVD(size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for eigenvalue decomposition
		static double SVD(size_t m, size_t n, bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for SVD
		//@}

		/** @name Composite operations */ //@{
		static double VectorSampleMean(size_t n, size_t nSample,  bool flagDouble=true, bool flagGPU=false);	///< Estimates the cost for the mean of a set of vectors
		static double VectorSampleVariance(size_t n, size_t nSample,  bool flagDouble=true, bool flagGPU=false);	///< Estimates the costs for the variance of a set of vectors
		//@}
};	//class ComputationalComplexityC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Interpolates the cost for an operation, given a table of cost measurements
 * @param[in] table Source table
 * @param[in] n Complexity factor of the operation
 * @param[in] flagDouble \c true if the cost is to be estimated for a double precision operation
 * @param[in] flagGPU \c true if the cost is to be estimated for GPU
 * @return Interpolated cost
 * @pre Table has an entry for the complexity factor 0 (unenforced)
 */
template<unsigned int N>
double ComplexityEstimatorC::Interpolate(const TableT& table, size_t n, bool flagDouble, bool flagGPU)
{
	size_t index= (flagDouble ? 0 : 2) + (flagGPU ? 1 : 0);	//Select the column

	//Find the interval containing n
	auto itU=table.lower_bound(n);
	bool flagIn= (itU!=table.end());	//If true, n is in the range covered by the table

	if(flagIn)
	{
		//Special case: n in the table
		if(itU->first == n)
			return itU->second[index];
	}
	else
		itU=prev(itU,1);	//Move to the last valid entry on the table

	auto itL=prev(itU,1);

	//Interpolation
	if(flagIn || itU->second[index] > itL->second[index] )
	{
		double alpha=double(n - itL->first) / (itU->first - itL->first);
		return pow<N>(alpha)*(itU->second[index]-itL->second[index]) + itL->second[index];
	}	//if(flagIn || itU->second[index] > itL->second[index] )

	//Out of range, and negative slope: Assume that any constants are negligible, and scale
	return itU->second[index] * pow<N>(double(n)/itU->first);
}	//double Interpoloate(const TableT& table, size_t n, bool flagDouble, bool flagGPU)

}	//NumericN
}	//SeeSawN

#endif /* COMPLEXITY_IPP_6302391 */
