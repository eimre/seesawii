/**
 * @file NumericalApproximations.ipp Implementation for numeric approximation functions
 * @author Evren Imre
 * @date 22 Jan 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERICAL_APPROXIMATIONS_IPP_8019853
#define NUMERICAL_APPROXIMATIONS_IPP_8019853

#include <boost/concept_check.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <cmath>
#include <climits>
#include <type_traits>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>

#include <iostream>
namespace SeeSawN
{
namespace NumericN
{

using boost::math::constants::pi;
using boost::math::constants::ln_two;
using boost::RandomAccessRangeConcept;
using boost::range_value;
using boost::distance;
using std::all_of;
using std::sqrt;
using std::pow;
using std::fabs;
using std::max;
using std::isinf;
using std::numeric_limits;
using std::is_floating_point;
using std::is_arithmetic;
using std::vector;
using std::multimap;
using std::greater_equal;
using std::greater;
using std::bind;

/**
 * @brief Computes the arithmetic-geometric mean
 * @tparam RealT A floating point type
 * @param[in] x First operand
 * @param[in] y Second operand
 * @param[in] maxIteration Maximum number of iterations for convergence
 * @param[in] maxRelativeError Max relative difference between the arithmetic and the geometric mean, for convergence
 * @return Arithmetic-geometric mean
 * @pre \c RealT is a floating point type
 * @pre \c x and \c y are both non-negative
 * @remarks http://en.wikipedia.org/wiki/Arithmetic%E2%80%93geometric_mean
 */
template<typename RealT>
RealT ComputeArithmeticGeometricMean(RealT x, RealT y, unsigned int maxIteration, RealT maxRelativeError=numeric_limits<RealT>::epsilon())
{
	static_assert(is_floating_point<RealT>::value, "ComputeArithmeticMean: RealT must be a floating point type");
	assert(x>=0 && y>=0);

	if(x==0 || y==0)
		return 0;

	if(isinf(x) || isinf(y))
		return numeric_limits<RealT>::infinity();

	unsigned int cIteration=0;
	RealT error=fabs(x-y)/max(x,y);
	while( error>maxRelativeError && cIteration<maxIteration)
	{
		RealT xNext=0.5*(x+y);
		y=sqrt(x*y);
		x=xNext;

		++cIteration;
		error=fabs(x-y)/max(x,y);
	}	//while( fabs(x-y)/max(x,y)>relativeDifference && cIteration<maxIteration)

	return 0.5*(x+y);
}	//double ComputeArithmeticGeometricMean(double x, double y, unsigned int nIteration)

/**
 * @brief Approximates the natural logarithm function
 * @param[in] x Operand
 * @param[in] fidelity Determines the accuracy of the approximation. Higher values imply better accuracy
 * @return An approximation to log(x)
 * @pre \c RealT is a floating point type
 * @pre \c x>=0
 * @remarks http://en.wikipedia.org/wiki/Natural_logarithm
 */
template<typename RealT>
RealT ApproximateLog(RealT x, unsigned int fidelity=8)
{
	static_assert(is_floating_point<RealT>::value, "ApproximateLog: RealT must be a floating point type");
	assert(x>=0);

	if(x==0)
		return -numeric_limits<RealT>::infinity();

	if(x==numeric_limits<RealT>::infinity())
		return numeric_limits<RealT>::infinity();

	double s=x*pow(2,fidelity);
	double denum=ComputeArithmeticGeometricMean(1.0, 4.0/s, 50);	//AGM converges very rapidly. 50 iterations is more than enough

	return 0.5*pi<RealT>()/denum - fidelity*ln_two<RealT>();
}	//double ApproximateLog(double x, unsigned int precision)

/**
 * @brief Applies d'Hondt's method for approximate proportional distribution
 * @tparam ValueT An arithmetic value
 * @param[in] votes Votes for each party. >=0
 * @param[in] nSeats Number of seats
 * @return Number of seats per party
 * @remarks Distribution of a number of threads among tasks with varying complexities
 * @pre \c RandomAccessRangeT is a random access range of arithmetic elements
 * @pre \c votes does not have any negative elements
 */
template<class RandomAccessRangeT>
vector<unsigned int> ApplyDHondt(const RandomAccessRangeT& votes, unsigned int nSeats)
{
	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<RandomAccessRangeT>));
	static_assert(is_arithmetic< typename range_value<RandomAccessRangeT>::type >::value, "ApproximateLog: RandomAccessRangeT must hold elements of arithmetic type");

	typedef typename range_value<RandomAccessRangeT>::type ValueT;
	assert(all_of(votes.begin(), votes.end(), bind(greater_equal<ValueT>(),_1,0)));

	size_t nParties=distance(votes);
	vector<unsigned int> output(nParties,0);

	if(nParties==0 || nSeats==0)
		return output;

	multimap<ValueT, unsigned int, greater<ValueT> > quotients;
	for(size_t c=0; c<nParties; ++c)
		quotients.emplace(votes[c],c);

	do
	{

		auto it=quotients.begin();

		size_t index=it->second;
		quotients.erase(it);

		++output[index];
		quotients.emplace(votes[index]/(output[index]+1), index);

		--nSeats;
	}while (nSeats>0);

	return output;
}	//vector<unsigned int> ApplyDHondt(const vector<RealT>& votes, unsigned int nSeats)

}	//NumericN
}	//SeeSawN

#endif /* NUMERICAL_APPROXIMATIONS_IPP_8019853 */
