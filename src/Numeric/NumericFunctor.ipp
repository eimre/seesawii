/**
 * @file NumericFunctor.ipp Implementation of various numeric functors
 * @author Evren Imre
 * @date 22 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERIC_FUNCTOR_IPP_3162564
#define NUMERIC_FUNCTOR_IPP_3162564

#include <type_traits>

namespace SeeSawN
{
namespace NumericN
{

using std::is_floating_point;

/**
 * @brief Computes the reciprocal of a floating point value
 * @tparam ValueT A floating point type
 * @pre \c ValueT is a floating point type
 * @remarks A model of adaptable unary function and InvertibleUnaryFunctionConceptC
 * @ingroup Utility
 * @nosubgrouping
 */
template<typename ValueT>
struct ReciprocalC
{
    //@cond CONCEPT_CHECK
    static_assert(is_floating_point<ValueT>::value, "ReciprocalC : ValueT must be a floating point type");
    //@endcond

    /**@name Adaptable unary function interface */ //@{
    typedef ValueT first_argument_type;
    typedef ValueT result_type;
    ValueT operator()(ValueT operand) const;    ///< Computes \c 1/operand
    //@}

    /** @name InvertibleUnaryFunctionC interface */ //@{
    static ValueT Invert(ValueT operand);	///< Computes \c 1/operand
    //@}
};  //struct ReciprocalC

/**
 * @brief Computes the difference of two matrices
 * @tparam MatrixT A vector
 * @pre \c MatrixT has the difference operator (unenforced)
 * @ingroup Utility
 * @nosubgrouping
 */
template<class MatrixT>
struct MatrixDifferenceC
{
	/**@name Adaptable binary function interface */ //@{
	typedef MatrixT first_argument_type;
	typedef MatrixT second_argument_type;
	typedef MatrixT result_type;
	MatrixT operator()(const MatrixT& operand1, const MatrixT& operand2) const;	///< Returns \c operand1-operand2
	//@}
};	//struct MatrixDifferenceC

/********** IMPLEMENTATION STARTS HERE **********/

/********** ReciprocalC **********/

/**
 * @brief Computes \c 1/operand
 * @param[in] operand Value to be inverted
 * @returns 1/operand
 */
template<typename ValueT>
ValueT ReciprocalC<ValueT>::operator()(ValueT operand) const
{
    return 1.0/operand;
}   //ValueT operator()(ValueT operand)

/**
 * @brief Computes \c 1/operand
 * @param[in] operand Value to be inverted
 * @returns 1/operand
 */
template<typename ValueT>
ValueT ReciprocalC<ValueT>::Invert(ValueT operand)
{
    return 1.0/operand;
}   //ValueT Invert(ValueT operand)

/********** MatrixDifferenceC **********/

/**
 * @brief Returns \c operand1-operand2
 * @param[in] operand1 First operand
 * @param[in] operand2 Second operand
 * @return \c operand1-operand2
 */
template<class MatrixT>
auto MatrixDifferenceC<MatrixT>::operator()(const MatrixT& operand1, const MatrixT& operand2) const -> MatrixT
{
	return operand1-operand2;
}	//MatrixT operator()(const MatrixT& operand1, const MatrixT& operand2)

}   //NumericN
}	//SeeSawN

#endif /* NUMERIC_FUNCTOR_IPP_3162564 */
