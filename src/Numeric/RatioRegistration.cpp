/**
 * @file RatioRegistration.cpp Implementation of \c RatioRegistrationC
 * @author Evren Imre
 * @date 12 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RatioRegistration.h"
#include <iostream>
namespace SeeSawN
{
namespace NumericN
{

/**
 * @brief Constructs the equation system
 * @param[in] observations Observations
 * @param[in] nElements Number of elements to be registered
 * @param[in] flagWeighted If \c true , weighted LS
 * @return Coefficient matrix
 */
auto RatioRegistrationC::MakeCoefficientMatrix(const vector<ObservationT>& observations, size_t nElements, bool flagWeighted) -> MatrixXd
{
	size_t nEquations=observations.size();

	MatrixXd mA(nEquations, nElements); mA.setZero();
	for(size_t c=0; c<nEquations; ++c)
	{
		size_t i1;
		size_t i2;
		double ratio;
		double weight;
		tie(i1, i2, ratio, weight)=observations[c];

		mA(c, i2)= flagWeighted ? -1 : -weight;
		mA(c, i1)= flagWeighted ? ratio : weight*ratio;
	}	//for(size_t c=0; c<nEquations; ++c)

	return mA;
}	//MatrixT MakeCoefficientMatrix(const vector<ObservationT>& observations, bool flagWeighted)

/**
 * @brief LS solver
 * @param[in] observations Observations
 * @param[in] nElements Number of elements
 * @param[in] flagWeighted flagWeighted If \c false , the weight components are ignored. Otherwise, weighted LS solution
 * @return A vector holding an absolute scale for each element. Empty if the equation system is ill-conditioned
 */
vector<double> RatioRegistrationC::SolveLS(const vector<ObservationT>& observations, size_t nElements, bool flagWeighted)
{
	//Build the equation system
	MatrixXd mA=MakeCoefficientMatrix(observations, nElements, flagWeighted);

	//Solve the equation system
	//TODO This is a sparse system
	JacobiSVD<MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> svd(mA, Eigen::ComputeFullU | Eigen::ComputeFullV);

	//If the null space has more than one basis vector, no unique solution
	svd.setThreshold(3*numeric_limits<double>::epsilon());
	if(svd.rank()<mA.cols()-1)
		return vector<double>();

	VectorXd solution=svd.matrixV().rightCols(1);

	vector<double> output(nElements);
	for(size_t c=0; c<nElements; ++c)
		output[c]=solution[c];

	return output;
}	//vector<RotationMatrix3DT> SolveLS(const vector<ObservationT>& observations, bool flagWeighted)

/**
 * @brief Minimal solver
 * @param[in] observations Observations
 * @return A vector holding an absolute scale for each element. Empty if the observations do not form a tree
 */
vector<double> RatioRegistrationC::SolveMinimal(const vector<ObservationT>& observations)
{
	size_t nObservations=observations.size();
	size_t nElements=nObservations+1;
	vector<bool> flagFixed(nElements, false);

	list<tuple<unsigned int, unsigned int, size_t> > edges;
	for(size_t c=0; c<nObservations; ++c)
		edges.emplace_back( get<iIndex1>(observations[c]), get<iIndex2>(observations[c]), c);

	vector<double> output(nElements);
	flagFixed[0]=true;
	output[0]=1;

	//Loop through the edges, and fix the scale
	auto it=edges.begin();
	auto itE=edges.end();
	unsigned int i1, i2;
	size_t index;

	do
	{
		tie(i1, i2, index)=*it;

		//Skip for the moment
		if( !flagFixed[i1] && !flagFixed[i2])
			advance(it,1);
		else
		{
			if(flagFixed[i1])
			{
				output[i2] = get<iRatio>(observations[index]) * output[i1];
				flagFixed[i2]=true;
			}
			else
			{
				output[i1] = 1.0/get<iRatio>(observations[index])* output[i2];
				flagFixed[i1]=true;
			}	//if(flagFixed[i1])

			it=edges.erase(it);
			itE=edges.end();
		}	//if( !flagFixed[i1] && !flagFixed[i2])

		if(it==itE)
			it=edges.begin();
	}while(!edges.empty());

	return output;
}	//vector<RotationMatrix3DT> SolveMinimal(const vector<ObservationT>& minimalSolver)

/**
 * @brief Runs the algorithm
 * @param[in] observations Observations
 * @param[in] flagWeighted If \c false , the weight components are ignored. Otherwise, weighted LS solution. The minimal solver ignores the weights
 * @pre The observations form a connected graph (unenforced)
 * @return A vector holding the absolute orientation for each camera. Empty if the problem has too few constraints
 */
vector<double> RatioRegistrationC::Run(const vector<ObservationT>& observations, bool flagWeighted)
{
	//Get the number of cameras
	unordered_set<unsigned int> indexList;
	for(const auto& current : observations)
	{
		//Invalid constraint
		if(get<iIndex1>(current) == get<iIndex2>(current) )
			return vector<double>();

		indexList.insert(get<iIndex1>(current));
		indexList.insert(get<iIndex2>(current));
	}	//for(const auto& current : observations)

	size_t nElements=indexList.size();

	//There must be at least two elements and one observation
	if(nElements<2 || (observations.size()<nElements-1) )
		return vector<double>();

	//Minimal solver
	if(observations.size()==(nElements-1) )
		return SolveMinimal(observations);

	//LS solver
	return SolveLS(observations, nElements, flagWeighted);
}	//vector<RotationMatrix3DT> Run(const vector<ObservationT>& observations)

/**
 * @brief Checks whether an observation set satisfies the preconditions
 * @param[in] observations Observations
 * @return \c false if the preconditions are not satisfied
 */
bool RatioRegistrationC::ValidateObservations(const vector<ObservationT>& observations)
{
	//Edge map
	typedef pair<unsigned int, unsigned int> EdgeT;
	vector<EdgeT> edgeList; edgeList.reserve(observations.size());
	for(const auto& current : observations)
	{
		//Relative scale to itself
		if(get<iIndex1>(current)==get<iIndex2>(current) )
			return false;

		edgeList.emplace_back(get<iIndex1>(current), get<iIndex2>(current));
	}	//for(const auto& current : observations)

	//Connectendness
	vector<list<EdgeT> > components = FindConnectedComponents(edgeList, true);

	return (components[0].size() == observations.size() );
}	//bool ValidateObservations(const vector<ObservationT>& observations)

}	//namespace NumericN
}	//namespace SeeSawN
