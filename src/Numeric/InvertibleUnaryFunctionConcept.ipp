/**
 * @file InvertibleUnaryFunctionConcept.ipp Implementation of InvertibleUnaryFunctionConceptC
 * @author Evren Imre
 * @date 18 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INVERTIBLE_UNARY_FUNCTION_CONCEPT_IPP_0870983
#define INVERTIBLE_UNARY_FUNCTION_CONCEPT_IPP_0870983

#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace NumericN
{

using boost::UnaryFunctionConcept;

/**
 * @brief Concept checker for the invertible unary function concept
 * @tparam TestT Type to be tested
 * @tparam ResultT Result type
 * @tparam OperandT Operand type
 * @ingroup Concept
 */
template<class TestT, typename ResultT, typename OperandT>
class InvertibleUnaryFunctionConceptC
{
	///@cond CONCEPT_CHECK

	BOOST_CONCEPT_ASSERT((UnaryFunctionConcept<TestT, ResultT, OperandT>));

	public:

		BOOST_CONCEPT_USAGE(InvertibleUnaryFunctionConceptC)
		{
			OperandT inv=TestT::Invert(ResultT()); (void)inv;
		}	//BOOST_CONCEPT_USAGE(InvertibleUnaryFunctionConceptC)

	///@endcond
};	//class InvertibleUnaryFunctionConceptC

}	//NumericN
}	//SeeSawN

#endif /* INVERTIBLEUNARYFUNCTIONCONCEPT_IPP_ */
