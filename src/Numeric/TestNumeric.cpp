/**
 * @file TestNumeric.cpp Unit tests for Numeric
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE NUMERIC

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/SVD>
#include <array>
#include <complex>
#include <cmath>
#include <tuple>
#include <cstddef>
#include <set>
#include <random>
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Camera.h"
#include "../UncertaintyEstimation/SUTTriangulationProblem.h"
#include "LinearAlgebra.h"
#include "LinearAlgebraJacobian.h"
#include "NumericFunctor.h"
#include "Polynomial.h"
#include "Combinatorics.h"
#include "Complexity.h"
#include "NumericalDifferentiation.h"
#include "NumericalApproximations.h"
#include "Graph.h"
#include "RatioRegistration.h"

#include <iostream>

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestNumericN
{

using namespace SeeSawN::NumericN;
using boost::adjacency_matrix;
using boost::add_edge;
using boost::undirectedS;
using Eigen::Matrix3d;
using Eigen::Matrix2d;
using Eigen::MatrixXd;
using Eigen::Vector2d;
using Eigen::Vector3d;
using Eigen::RowVector3d;
using Eigen::Quaternion;
using std::array;
using std::complex;
using std::sqrt;
using std::isinf;
using std::tie;
using std::size_t;
using std::set;
using std::mt19937_64;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::UncertaintyEstimationN::SUTTriangulationProblemC;

BOOST_AUTO_TEST_SUITE(Numeric_Functor)

BOOST_AUTO_TEST_CASE(Numeric_Functor)
{
	//ReciprocalC
    ReciprocalC<double> r;
    BOOST_CHECK_EQUAL(r(5), 1.0/5.0);
    BOOST_CHECK_EQUAL(r.Invert(1.0/5.0), 5.0);

    //MatrixDifferenceC
    MatrixDifferenceC<Matrix3d> matrixDifference;
    Matrix3d m1=Matrix3d::Random();
    Matrix3d m2=Matrix3d::Random();

    BOOST_CHECK(matrixDifference(m1,m2).isApprox((m1-m2),1e-8));
}   //BOOST_AUTO_TEST_CASE(Numeric_Functor)

BOOST_AUTO_TEST_SUITE_END() //Numeric_Functor

BOOST_AUTO_TEST_SUITE(Polynomial_Solvers)

BOOST_AUTO_TEST_CASE(Quadratic)
{
    //Real roots

    array<double,3> coeff1{{1, -5, 6}};

    array<complex<double>,2> roots1=QuadraticPolynomialRoots(coeff1);

    BOOST_CHECK_EQUAL(roots1[0], complex<double>(3,0));
    BOOST_CHECK_EQUAL(roots1[1], complex<double>(2,0));

    //Complex roots

    array<double,3> coeff2{{1, -3, 3}};
    array<complex<double>,2> roots2=QuadraticPolynomialRoots(coeff2);

    BOOST_CHECK_EQUAL(roots2[0], complex<double>(1.5, sqrt(3)/2));
    BOOST_CHECK_EQUAL(roots2[1], complex<double>(1.5, -sqrt(3)/2));
}   //BOOST_AUTO_TEST_CASE(Quadratic)

BOOST_AUTO_TEST_CASE(Cubic)
{
    array<double,4> coefficients1{{1, -6, 11, -6}};
    array<complex<double>,3> roots1;
    roots1=CubicPolynomialRoots(coefficients1);

    array<complex<double>, 3> roots1r={{ complex<double>(3,0), complex<double>(1,0), complex<double>(2,0)}};
    for(size_t c=0; c<3; ++c)
    {
        BOOST_CHECK_CLOSE(roots1[c].real(), roots1r[c].real(), 1e-13);
        BOOST_CHECK_CLOSE(roots1[c].imag(), roots1r[c].imag(), 1e-13);
    }   //for(size_t c=0; c<3; ++c)

    array<double,4> coefficients2{{1, -3, 7, -5}};
    array<complex<double>,3> roots2;
    roots2=CubicPolynomialRoots(coefficients2);

    array<complex<double>, 3> roots2r{{complex<double>(1,0), complex<double>(1,2), complex<double>(1,-2)}};
    for(size_t c=0; c<3; ++c)
    {
        BOOST_CHECK_CLOSE(roots2[c].real(), roots2r[c].real(), 1e-13);
        BOOST_CHECK_CLOSE(roots2[c].imag(), roots2r[c].imag(), 1e-13);
    }   //for(size_t c=0; c<3; ++c)

    array<double,4> coefficients3{{1, 3, 3, 1}};
    array<complex<double>,3> roots3;
    roots3=CubicPolynomialRoots(coefficients3);

    array<complex<double>, 3> roots3r{{complex<double>(-1,0), complex<double>(-1,0), complex<double>(-1,0)}};
    for(size_t c=0; c<3; ++c)
    {
        BOOST_CHECK_CLOSE(roots3[c].real(), roots3r[c].real(), 1e-13);
        BOOST_CHECK_CLOSE(roots3[c].imag(), roots3r[c].imag(), 1e-13);
    }   //for(size_t c=0; c<3; ++c)
}   //BOOST_AUTO_TEST_CASE(Cubic)

BOOST_AUTO_TEST_CASE(General_Polynomials)
{
	array<double, 5> coefficients{{1, 4, 6, 5, 1}};
	array<complex<double>, 4> roots=PolynomialRoots<4>(coefficients);
	array<complex<double>, 4> rootsGT={ { {-0.75187393, 1.03398}, {-0.75187393, -1.03398}, {-0.27551,0}, {-2.22074,0} } };

	for(size_t c=0; c<4; ++c)
	{
		BOOST_CHECK_CLOSE(roots[c].real(), rootsGT[c].real(), 1e-3);
		BOOST_CHECK_CLOSE(roots[c].imag(), rootsGT[c].imag(), 1e-3);
	}	//for(size_t c=0; c<4; ++c)
}	//BOOST_AUTO_TEST_CASE(General_Polynomials)

BOOST_AUTO_TEST_SUITE_END() //Polynomial_Solvers

BOOST_AUTO_TEST_SUITE(Linear_Algebra)

BOOST_AUTO_TEST_CASE(Make_Rank_N)
{
	Matrix3d mR3; mR3.setIdentity();

	Matrix3d mR2;
	unsigned int rank2;
	tie(rank2, mR2)=MakeRankN<Eigen::FullPivHouseholderQRPreconditioner>(mR3, 2);

	Matrix3d mR2r; mR2r<<1, 0, 0, 0, 1, 0, 0, 0, 0;
	BOOST_CHECK(mR2.isApprox(mR2r, 1e-16));
	BOOST_CHECK_EQUAL(rank2, 2);

	Matrix3d mR0;
	unsigned int rank0;
	tie(rank0, mR0)=MakeRankN<Eigen::NoQRPreconditioner>(mR3, 2, 2);
	BOOST_CHECK(mR0.isApproxToConstant(0, 1e-16));
	BOOST_CHECK_EQUAL(rank0, 0);

	Matrix3d mRf;
	unsigned int rank3;
	tie(rank3, mRf)=MakeRankN<Eigen::FullPivHouseholderQRPreconditioner>(mR3, 3);
	BOOST_CHECK(mRf.isApprox(mR3, 1e-16));
	BOOST_CHECK_EQUAL(rank3, 3);
}	//BOOST_AUTO_TEST_CASE(Make_Rank_N)

BOOST_AUTO_TEST_CASE(Compute_Pseudo_Inverse)
{
	Matrix3d mA; mA.setIdentity(); mA(0,2)=0.5;
	Matrix3d mAp=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mA);
	BOOST_CHECK(mAp.isApprox(mA.inverse(), 1e-16));

	Matrix3d mA2(mA); mA2(2,2)=0;
	Matrix3d mA2p=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mA2, 1e-8);
	Matrix3d mA2r; mA2r.setIdentity(); mA2r(2,2)=0;
	BOOST_CHECK(mA2r.isApprox(mA2*mA2p, 1e-15));

	Matrix3d mA3(mA); mA3(0,0)=10;
	Matrix3d mA3p=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mA3, 1);
	Matrix3d mA3r; mA3r<<0.99997,0.00000,0.00504,0.00000,0.00000,0.00000,0.00504,0.00000,0.0003;
	BOOST_CHECK(mA3r.isApprox(mA3*mA3p, 1e-3));
}	//BOOST_AUTO_TEST_CASE(Compute_Pseudo_Inverse)

BOOST_AUTO_TEST_CASE(RQ_Decomposition)
{
	Matrix3d mF1; mF1<<1000, 1, 200, 0, 1020, 250, 0, 0, 1;

	Quaternion<double> q(0.5, 0.25, 0.5, 0.75);
	q.normalize();
	Matrix3d mF2=q.matrix();

	Matrix3d mF12=mF1*mF2;

	Matrix3d mR;
	Matrix3d mQ;
	tie(mR, mQ)=RQDecomposition33(mF12);

	Matrix3d mI; mI.setIdentity();
	BOOST_CHECK(mI.isApprox( mQ*mQ.transpose() , 1e-12));
	BOOST_CHECK_SMALL(mR(1,0), 1e-16);
	BOOST_CHECK_SMALL(mR(2,0), 1e-16);
	BOOST_CHECK_SMALL(mR(2,1), 1e-16);
	BOOST_CHECK(mF12.isApprox(mR*mQ, 1e-12));
}	//BOOST_AUTO_TEST_CASE(RQ_Decomposition)

BOOST_AUTO_TEST_CASE(Upper_Cholesky_Decomposition)
{
	Matrix3d mK; mK<<1000, 1, 200, 0, 1020, 250, 0, 0, 1;
	Matrix3d mKKt = mK * mK.transpose();

	optional<Matrix3d> mU=UpperCholeskyDecomposition33(mKKt);
	BOOST_CHECK(mU);
	BOOST_CHECK(mK.isApprox(*mU, 1e-6));

	Matrix3d mKKt2(mKKt); mKKt2(2,2)=-1;	//no longer positive definite
	BOOST_CHECK(!UpperCholeskyDecomposition33(mKKt2));
}	//BOOST_AUTO_TEST_CASE(Upper_Cholesky_Decomposition)

BOOST_AUTO_TEST_CASE(Square_Root_Decomposition)
{
	Matrix3d mA; mA.setRandom(); mA=mA*mA.transpose();

	Matrix3d mF=ComputeSquareRootSVD<Eigen::FullPivHouseholderQRPreconditioner, Matrix3d>(mA);
	BOOST_CHECK(mA.isApprox(mF*mF.transpose(), 1e-6));
}	//BOOST_AUTO_TEST_CASE(Square_Root_Decomposition)

BOOST_AUTO_TEST_CASE(Closest_Rotation)
{
	RotationMatrix3DT mR1r; mR1r<<-0.0022473, -0.0023999, 0.9999950, -0.1968880, -0.9804220, -0.0027957, 0.9804230, -0.1968940, 0.0017298;

	RotationMatrix3DT mR1=ComputeClosestRotationMatrix(mR1r);
	BOOST_CHECK(mR1r.isApprox(mR1, 1e-6));

	RotationMatrix3DT mR2=ComputeClosestRotationMatrix<RotationMatrix3DT>(3*mR1r);
	BOOST_CHECK(mR1r.isApprox(mR2, 1e-6));
}	//BOOST_AUTO_TEST_CASE(Closest_Rotation)

BOOST_AUTO_TEST_CASE(Householder_Transformation)
{
	Vector3d x1(1,2,3);

	Vector3d v=ComputeHouseholderVector(x1, 2);
	Vector3d vr(0.140789, 0.281579, 0.949153);
	BOOST_CHECK(vr.isApprox(v, 1e-5));

	Vector3d x1rot=ApplyHouseholderTransformationV(v, x1);
	Vector3d x1rotr(0,0,-x1.norm());
	BOOST_CHECK(x1rotr.isApprox(x1rot, 1e-5));

	Matrix3d mX2; mX2<<-0.0022473, -0.0023999, 0.9999950, -0.1968880, -0.9804220, -0.0027957, 0.9804230, -0.1968940, 0.0017298;
	Matrix3d mX2rot=ApplyHouseholderTransformationM(v, mX2);
	Matrix3d mX2rotr; mX2rotr<< -0.248577, 0.128052, 0.960111, -0.689547, -0.719519, -0.0825631, -0.680246, 0.682565, -0.267152;
	BOOST_CHECK(mX2rotr.isApprox(mX2rot, 1e-5));
}	//BOOST_AUTO_TEST_CASE(Householder_Rotation)

BOOST_AUTO_TEST_CASE(Projection_To_Tangent_Plane)
{
	Vector3d x1(1,2,3);
	Vector3d vContact1(1,1,1);

	optional<Vector2d> y1=ProjectHomogeneousToTangentPlane<Vector2d>(x1, vContact1);
	Vector2d y1r(0.394338, 0.105662);
	BOOST_CHECK(y1);
	BOOST_CHECK(y1r.isApprox(*y1, 1e-5));

	Vector3d vZero=Vector3d::Zero();
	BOOST_CHECK(!ProjectHomogeneousToTangentPlane<Vector2d>(x1, vZero));
	BOOST_CHECK(!ProjectHomogeneousToTangentPlane<Vector2d>(vZero, vContact1));

	Vector3d x2(0,0,1);
	optional<Vector2d> y2=ProjectHomogeneousToTangentPlane<Vector2d>(x2, vContact1);
	BOOST_CHECK(y2);
	Vector2d y2r(1, 1);
	BOOST_CHECK(y2r.isApprox(*y2, 1e-5));

	//Contact point is the origin
	optional<Vector2d> y3=ProjectHomogeneousToTangentPlane<Vector2d>(vContact1, vContact1);
	BOOST_CHECK(y3);
	BOOST_CHECK_SMALL((*y3)[0], 1e-7);
	BOOST_CHECK_SMALL((*y3)[1], 1e-7);
}	//BOOST_AUTO_TEST_CASE(Projection_To_Tangent_Plane)

BOOST_AUTO_TEST_CASE(Coordinate_Conversions)
{
	//CartesianToSpherical
	Vector3d cartesian1(2,3,1);
	Vector3d spherical1=CartesianToSpherical<Vector3d>(cartesian1);
	Vector3d spherical1GT(3.741, 0.983, 1.300);

	BOOST_CHECK(spherical1.isApprox(spherical1GT, 1e-2));

	BOOST_CHECK(CartesianToSpherical(Vector3d(0,0,0))==Vector3d::Zero() );

	//SphericalToCartesian
	BOOST_CHECK(cartesian1.isApprox(SphericalToCartesian(spherical1),1e-8));

}	//Coordinate_Conversions

BOOST_AUTO_TEST_CASE(Jacobians)
{
	//Inverse
	Matrix3d mA3x3; mA3x3<<0.91454, 0.95151, 0.55467, 0.91276, 0.29077, 0.42185, 0.96252, 0.52204, 0.33601;
	MatrixXd mJdAidA=JacobiandAidA<MatrixXd>(mA3x3);
	MatrixXd mJdAidAr(9,9); mJdAidAr<<-1.79153,1.45261,2.8751,-0.440962,0.357542,0.70767,3.51099,-2.84679,-5.63455,-0.440962,-3.3132,6.4107,-0.108537,-0.815502,1.57791,0.864186,6.49312,-12.5635,3.51099,1.76172,-8.81107,0.864186,0.433624,-2.16873,-6.88075,-3.45256,17.2677,1.45261,-1.17781,-2.3312,-3.3132,2.68641,5.31713,1.76172,-1.42844,-2.82726,0.357542,2.68641,-5.19793,-0.815502,-6.12733,11.8557,0.433624,3.25806,-6.30401,-2.84679,-1.42844,7.1442,6.49312,3.25806,-16.2949,-3.45256,-1.7324,8.66443,2.8751,-2.3312,-4.61405,6.4107,-5.19793,-10.2881,-8.81107,7.1442,14.1403,0.70767,5.31713,-10.2881,1.57791,11.8557,-22.9396,-2.16873,-16.2949,31.5289,-5.63455,-2.82726,14.1403,-12.5635,-6.30401,31.5289,17.2677,8.66443,-43.3344;
	BOOST_CHECK(mJdAidA.isApprox(mJdAidAr, 1e-4));

	//Normalisation
	Matrix2d mA2x2=mA3x3.block(0,0,2,2);
	MatrixXd mJNormalisation=JacobianNormalise<MatrixXd>(mA2x2);
	MatrixXd mJNr(4,4); mJNr<<  0.420354, -0.200647, -0.192476, -0.0613153, -0.200647, 0.404447, -0.200256, -0.0637939, -0.192476, -0.200256, 0.421104, -0.061196, -0.0613153, -0.0637939, -0.061196, 0.59371;
	BOOST_CHECK(mJNormalisation.isApprox(mJNr, 1e-4));

	//dABdA
	Matrix2d mB2x2; mB2x2<<0.437994, 0.191516, 0.332701, 0.492029;
	MatrixXd mJdABdA=JacobiandABdA<MatrixXd>(mA2x2, mB2x2);

	MatrixXd mJdABdAr(4,4); mJdABdAr<<0.437994, 0.332701, 0, 0, 0.191516, 0.492029, 0, 0, 0, 0, 0.437994, 0.332701, 0, 0, 0.191516, 0.492029;
	BOOST_CHECK(mJdABdAr==mJdABdA);

	//dABdB
	MatrixXd mJdABdB=JacobiandABdB<MatrixXd>(mA2x2, mB2x2);
	MatrixXd mJdABdBr(4,4); mJdABdBr<<0.91454, 0, 0.95151, 0, 0, 0.91454, 0, 0.95151, 0.91276, 0, 0.29077, 0, 0, 0.91276, 0, 0.29077;
	BOOST_CHECK(mJdABdBr==mJdABdB);

	//dAda
	MatrixXd mJdAda=JacobiandAda<MatrixXd>(4, {1,3});
	MatrixXd mJdAdar=MatrixXd::Zero(4,2); mJdAdar(1,0)=1; mJdAdar(3,1)=1;
	BOOST_CHECK(mJdAda==mJdAdar);

	//dA/an dA
	MatrixXd mJdAoaNdA=JacobianAOveraNdA<MatrixXd>(mA2x2, 0, 1);
	MatrixXd mJdAoaNdAr(4,4);
	mJdAoaNdAr<<  1.05096, -1.01013, 0, 0,
	        	  0,         0,         0,         0,
	        	  0,  -1.00816,   1.05096,         0,
	        	  0, -0.321161,         0,   1.05096;
	BOOST_CHECK(mJdAoaNdAr.isApprox(mJdAoaNdA, 1e-4));

	//Transpose
	MatrixXd mJdAtdA=JacobiandAtdA<MatrixXd>(mA3x3.block(0,0,2,3));
	MatrixXd mJdAtdAr(6,6); mJdAtdAr.setZero();
	mJdAtdAr(0,0)=1; mJdAtdAr(2,1)=1; mJdAtdAr(4,2)=1; mJdAtdAr(1,3)=1; mJdAtdAr(3,4)=1; mJdAtdAr(5,5)=1;
	BOOST_CHECK(mJdAtdAr==mJdAtdA);

	//dx'Ax dx
	MatrixXd mS=mA3x3+mA3x3.transpose();
	RowVector3d mJdxtAxdx=JacobiandxtAxdx<RowVector3d>(mS, mA3x3.col(0));
	RowVector3d mJdxtAxdxr(9.66945, 6.28854, 5.79182);
	BOOST_CHECK(mJdxtAxdx.isApprox(mJdxtAxdxr, 1e-5));
}	//BOOST_AUTO_TEST_CASE(Jacobians)

BOOST_AUTO_TEST_SUITE_END()	//Linear_Algebra

BOOST_AUTO_TEST_SUITE(Combinatorics)

BOOST_AUTO_TEST_CASE(Combinatorics)
{
	//GenerateCombinations
	typedef set<unsigned int> SetT;

	vector<SetT> setList1=GenerateCombinations(4,2);
	vector<SetT> setList1r{ {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3} };

	BOOST_CHECK_EQUAL(setList1.size(), 6);
	for(size_t c=0; c<6; ++c)
		BOOST_CHECK_EQUAL_COLLECTIONS(setList1[c].begin(), setList1[c].end(), setList1r[c].begin(), setList1r[c].end());

	//GetCombinationIndex
	vector<unsigned int> indexr{0,1,3,2,4,5};
	for(size_t c=0; c<6; ++c)
		BOOST_CHECK_EQUAL(GetCombinationIndex(setList1[c]), indexr[c]);

	//GenerateRandomCombination

	mt19937_64 rng;
	SetT randomSample=*GenerateRandomCombination(rng, 10, 4);
	BOOST_CHECK_EQUAL(randomSample.size(), 4);
}	//BOOST_AUTO_TEST_CASE(Combinatorics)

BOOST_AUTO_TEST_SUITE_END() //Combinatorics

BOOST_AUTO_TEST_SUITE(Complexity_Estimation)

BOOST_AUTO_TEST_CASE(Computational_Complexity)
{
	//Linear
	BOOST_CHECK_CLOSE(ComplexityEstimatorC::InnerProduct(8), 26.74, 1e-3);
	BOOST_CHECK_CLOSE(ComplexityEstimatorC::InnerProduct(9), 16.415, 1e-3);
	BOOST_CHECK_CLOSE(ComplexityEstimatorC::InnerProduct(16384), 18076.8, 1e-3);

	//Quadratic

	//Cubic
}	//BOOST_AUTO_TEST_CASE(Operations)

BOOST_AUTO_TEST_SUITE_END()	//Complexity_Estimation

BOOST_AUTO_TEST_SUITE(Numerical_Differentiation)

BOOST_AUTO_TEST_CASE(Numerical_Jacobian)
{
	//Problem
	CameraMatrixT mP1; mP1<<1000, 1e-5, 960, 0, 0, 1001, 540, 0, 0, 0, 1, 0;
	CameraMatrixT mP2; mP2<< -5.4429e+02, 1.2602e+03, -1.9320e+02, -6.7811e+02, 7.8476e+02, 6.7894e+02, -4.6560e+02, -9.0110e+02, -2.5070e-01, 5.2925e-01, -8.1058e-01, 8.3148e-01;
	Coordinate2DT x1(1960,1541);
	Coordinate2DT x2(-519,324);

	SUTTriangulationProblemC problem(x1, x2, 1, mP1, mP2, true);

	NumericalJacobianParametersC parameters;

	NumericalJacobianC<SUTTriangulationProblemC>::jacobian_type mJ;
	NumericalJacobianDiagnosticsC diagnostics=NumericalJacobianC<SUTTriangulationProblemC>::Run(mJ, problem, parameters);

	NumericalJacobianC<SUTTriangulationProblemC>::jacobian_type mJgt(3,4); mJgt<< 0.000114077, -0.000189939, -5.10486e-05,  0.000266579,
																			 	 -0.000334568, -0.000108896,  0.000141633,  0.000225987,
																			 	 -0.00063002, -0.000616022,  3.83828e-05,  0.000247739;

	BOOST_CHECK(mJgt.isApprox(mJ, 1e-4));
}	//BOOST_AUTO_TEST_CASE(Numerical_Jacobian)

BOOST_AUTO_TEST_SUITE_END()	//Numerical_Differentiation

BOOST_AUTO_TEST_SUITE(Numeric_Approximations)

BOOST_AUTO_TEST_CASE(Functions)
{
	//Arithmetic-geometric mean
	BOOST_CHECK_CLOSE(ComputeArithmeticGeometricMean(24.0, 6.0, 10), 13.4582, 1e-3);
	BOOST_CHECK_EQUAL(ComputeArithmeticGeometricMean(24.0, 0.0, 10), 0.0);
	BOOST_CHECK(isinf(ComputeArithmeticGeometricMean(24.0, numeric_limits<double>::infinity(), 10)));

	//ApproximateLog
	BOOST_CHECK_CLOSE(log(0.5), ApproximateLog(0.5, 12), 1e-3);
	BOOST_CHECK_EQUAL(ApproximateLog(0.0), -numeric_limits<double>::infinity());
	BOOST_CHECK_EQUAL(ApproximateLog(numeric_limits<double>::infinity()), numeric_limits<double>::infinity());

	//D'Hondt
	vector<unsigned int> votes{100, 80, 30, 20};
	vector<unsigned int> seats=ApplyDHondt(votes, 8);
	vector<unsigned int> seatsR{4,3,1,0};
	BOOST_CHECK_EQUAL_COLLECTIONS(seats.begin(), seats.end(), seatsR.begin(), seatsR.end());
}	//BOOST_AUTO_TEST_CASE(Functions)

BOOST_AUTO_TEST_SUITE_END()	//Numeric_Approximations

BOOST_AUTO_TEST_SUITE(Graph_Analysis)

BOOST_AUTO_TEST_CASE(Graph_Analysis_Functions)
{
	adjacency_matrix<undirectedS> graph1(4);

	add_edge(0,1,graph1);
	add_edge(1,2,graph1);
	add_edge(2,3,graph1);
	add_edge(3,0,graph1);
	add_edge(0,2,graph1);

	//Laplacian
	MatrixXi mL=ConstructGraphLaplacian(graph1);

	MatrixXi mLr(4,4); mLr<< 3,-1,-1,-1,
						-1, 2,-1, 0,
						-1,-1, 3,-1,
						-1, 0,-1, 2;

	BOOST_CHECK(mL.isApprox(mLr, 1e-15));

	//CountSpanningTrees
	BOOST_CHECK_EQUAL(round(CountSpanningTrees(graph1)), 8);

	adjacency_matrix<undirectedS> graph2(1);	//1-vertex graph
	BOOST_CHECK_EQUAL(CountSpanningTrees(graph2), 0);

	adjacency_matrix<undirectedS> graph3(4);	//disconnected graph
	add_edge(0,1,graph3);
	add_edge(2,3,graph3);
	BOOST_CHECK_EQUAL(CountSpanningTrees(graph3), 0);

}	//BOOST_AUTO_TEST_CASE(Graph_Analysis_Functions)

BOOST_AUTO_TEST_SUITE_END()	//Graph_Analysis

BOOST_AUTO_TEST_SUITE(Ratio_Registration)

BOOST_AUTO_TEST_CASE(Ratio_Registration_Operation)
{
	vector<double> values{1,2.5,1.6,4};

	typedef RatioRegistrationC::observation_type ObservationT;
	constexpr size_t iIndex1=RatioRegistrationC::iIndex1;
	constexpr size_t iIndex2=RatioRegistrationC::iIndex2;
	constexpr size_t iRatio=RatioRegistrationC::iRatio;

	vector<ObservationT> observations; observations.reserve(6);
	for(size_t c1=0; c1<4; ++c1)
		for(size_t c2=c1+1; c2<4; ++c2)
			observations.emplace_back(c1, c2, values[c2]/values[c1], c1+c2);

	//Basic operation, non-minimal

	vector<double> result1=RatioRegistrationC::Run(observations, false);
	BOOST_CHECK_EQUAL(result1.size(), 4);
	for(const auto& current : observations)
		BOOST_CHECK_CLOSE(get<iRatio>(current), result1[get<iIndex2>(current)]/result1[get<iIndex1>(current)], 1e-6);

	vector<double> result2=RatioRegistrationC::Run(observations, true);
	BOOST_CHECK_EQUAL(result2.size(), 4);
	for(const auto& current : observations)
		BOOST_CHECK_CLOSE(get<iRatio>(current), result2[get<iIndex2>(current)]/result2[get<iIndex1>(current)], 1e-6);

	//Minimal solver
	vector<ObservationT> incompleteObservations{observations[2], observations[4], observations[5]};
	vector<double> result3=RatioRegistrationC::Run(incompleteObservations, false);
	BOOST_CHECK_EQUAL(result3.size(), 4);
	for(const auto& current : observations)
		BOOST_CHECK_CLOSE(get<iRatio>(current), result3[get<iIndex2>(current)]/result3[get<iIndex1>(current)], 1e-6);

	//Failure
	vector<ObservationT> underconstrained{observations[0], observations[5]};
	BOOST_CHECK(RatioRegistrationC::Run(underconstrained, true).empty());

	//Validation

	BOOST_CHECK(RatioRegistrationC::ValidateObservations(observations));

	vector<ObservationT> invalidObservations1(2);
	invalidObservations1[0]=observations[0];
	invalidObservations1[1]=observations[5];
	BOOST_CHECK(!RatioRegistrationC::ValidateObservations(invalidObservations1));

	vector<ObservationT> invalidObservations2(observations);
	get<iIndex1>(invalidObservations2[2])=get<iIndex2>(invalidObservations2[2]);
	BOOST_CHECK(!RatioRegistrationC::ValidateObservations(invalidObservations2));

}	//BOOST_AUTO_TEST_CASE(Ratio_Registration_Operation)

BOOST_AUTO_TEST_SUITE_END()	//Ratio_Registration
}   //TestNumericN
}   //UnitTestsN
}	//SeeSawN


