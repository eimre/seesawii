/**
 * @file RatioRegistration.h Public interface for \c RatioRegistrationC
 * @author Evren Imre
 * @date 12 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RATIO_REGISTRATION_H_6979012
#define RATIO_REGISTRATION_H_6979012

#include "RatioRegistration.ipp"

namespace SeeSawN
{
namespace NumericN
{

class RatioRegistrationC;	///< Registers a set of relative scale measurements to an absolute scale

}	//namespace NumericN
}	//namespace SeeSawN



#endif /* RATIO_REGISTRATION_H_6979012 */
