/**
 * @file RatioRegistration.ipp Implementation details for \c RatioRegistrationC
 * @author Evren Imre
 * @date 12 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RATIO_REGISTRATION_IPP_1001221
#define RATIO_REGISTRATION_IPP_1001221

#include <Eigen/Dense>
#include <Eigen/SVD>
#include <tuple>
#include <vector>
#include <list>
#include <unordered_set>
#include <utility>
#include <limits>
#include "../Wrappers/BoostGraph.h"

namespace SeeSawN
{
namespace NumericN
{

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::JacobiSVD;
using std::tuple;
using std::get;
using std::tie;
using std::vector;
using std::list;
using std::unordered_set;
using std::pair;
using std::numeric_limits;
using SeeSawN::WrappersN::FindConnectedComponents;

/**
 * @brief Registers a set of relative scale measurements to an absolute scale
 * @remarks The algorithm assumes that the observations form a connected graph, and for weighted operation, non-zero weights
 * @remarks An observation is the ratio Element2/Element1
 * @remarks The minimum number of constraints is one less than the number of elements
 * @remarks The solvers
 * 	- Minimal solver: When the number of constraints is one less than the number of cameras, just chaining the observations yields a consistent solution
 * 	- LS Solver:  When there are at least as many relative constraints as the number of elements.
 * @remarks The solution has a sign ambiguity
 * @ingroup Algorithm
 * @nosubgrouping
 */

class RatioRegistrationC
{
	private:

		typedef tuple<unsigned int, unsigned int, double, double> ObservationT;	///< Type describing an observation

		/**@name Implementation details */ //@{
		static MatrixXd MakeCoefficientMatrix(const vector<ObservationT>& observations, size_t nElements, bool flagWeighted);	///< Constructs the equation system
		static vector<double> SolveLS(const vector<ObservationT>& observations, size_t nElements, bool flagWeighted);	///< LS solver
		static vector<double> SolveMinimal(const vector<ObservationT>& observations);	///< Minimal solver
		//@}

	public:

		/** @name ObservationT definition*/ //@{
		typedef ObservationT observation_type;
		static constexpr size_t iIndex1=0;	///< Index of the component holding the id of the first element.
		static constexpr size_t iIndex2=1;	///< Index of the component holding the id of the second element
		static constexpr size_t iRatio=2;	///< Index of the component for the ratio of the second element to the first
		static constexpr size_t iWeight=3;	///< Index of the component for the weight of individual observations
		//@}

		/** @name Operations */ //@{
		static vector<double> Run(const vector<ObservationT>& observations, bool flagWeighted);	///< Runs the algorithm
		//@}

		/** @name Utility */ ///@{
		static bool ValidateObservations(const vector<ObservationT>& observations);	///< Checks whether an observation set satisfies the preconditions
		//@}


};	//class RatioRegistrationC

}	//namespace NumericN
}	//namespace SeeSawN





#endif /* RATIO_REGISTRATION_IPP_1001221 */
