/**
 * @file LinearAlgebra.ipp Implementation of various linear algebra functions
 * @author Evren Imre
 * @date 18 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LINEAR_ALGEBRA_IPP_0803332
#define LINEAR_ALGEBRA_IPP_0803332

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/math/special_functions/sign.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/SVD>
#include <Eigen/Geometry>
#include <tuple>
#include <climits>
#include <cstddef>
#include <cmath>
#include <type_traits>
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"

namespace SeeSawN
{
namespace NumericN
{

using boost::optional;
using boost::math::pow;
using boost::math::sign;
using boost::math::constants::two_pi;
using boost::math::constants::pi;
using Eigen::Matrix;
using Eigen::Matrix4d;
using Eigen::Matrix3d;
using Eigen::JacobiSVD;
using Eigen::Quaternion;
using std::tuple;
using std::make_tuple;
using std::get;
using std::numeric_limits;
using std::size_t;
using std::is_floating_point;
using std::hypot;
using std::sqrt;
using std::atan2;
using std::acos;
using std::sin;
using std::cos;
using std::remainder;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ColsM;
using SeeSawN::WrappersN::MakeCrossProductMatrix;

/**
 * @brief Reduces the rank of a matrix to a specified value by removing the weakest components
 * @tparam QRPreconditionerT An Eigen QR preconditioner type
 * @tparam MatrixT A matrix
 * @param[in] input Matrix to be reduced
 * @param[in] targetRank Maximum rank after the operation
 * @param[in] zero If the absolute value of a singluar value is below \c zero , it is considered to be 0
 * @return A tuple consisting of the final rank and the reduced matrix
 * @pre \c MatrixT is an Eigen dense matrix type (unenforced)
 * @pre \c QRPreconditionerT is a valid Eigen preconditioner type (unenforced)
 * @ingroup Numerical
 */
template<int QRPreconditionerT, class MatrixT>
tuple<unsigned int, MatrixT> MakeRankN(const MatrixT& input, unsigned int targetRank, typename ValueTypeM<MatrixT>::type zero=numeric_limits<typename ValueTypeM<MatrixT>::type>::epsilon())
{
	JacobiSVD<MatrixT, QRPreconditionerT> svd(input, Eigen::ComputeFullU | Eigen::ComputeFullV);	//SVD

	tuple<unsigned int, MatrixT> output;

	//Eliminate the weak singular values
	auto singularValues=svd.singularValues();
	unsigned int nNonzero=svd.nonzeroSingularValues();
	get<0>(output)=nNonzero;
	for(size_t c=0; c<nNonzero; ++c)
		if( c>=targetRank || fabs(singularValues(c))<=zero)
		{
			get<0>(output)=c;	//So, c+1th sv is zero

			//Ordered
			for(size_t c2=c; c2<nNonzero; ++c2)
				singularValues(c2)=0;

			break;
		}	//if(c>=targetRank || singularValues(c)<=zero)

	//Non-square matrices require special handling
	Matrix<typename ValueTypeM<MatrixT>::type, RowsM<MatrixT>::value, ColsM<MatrixT>::value > mS(input.rows(), input.cols()); mS.setZero();
	mS.block(0, 0, singularValues.size(), singularValues.size())=singularValues.asDiagonal();

	//Recompose
	get<1>(output)=svd.matrixU()*(mS*svd.matrixV().transpose()).eval();

	return output;
}	//tuple<unsigned int, MatrixT> MakeRankN(const MatrixT& input, unsigned int targetRank, typename ValueTypeM<MatrixT>::type zero=numerical_limits<typename ValueTypeM<MatrixT>::type>::epsilon())

/**
 * @brief Computes the Moore-Penrose pseudo-inverse
 * @tparam QRPreconditionerT An Eigen QR preconditioner type
 * @tparam MatrixT A matrix
 * @param[in] operand Matrix to be inverted
 * @param[in] zero Any singular values with an absolute value below this are assumed to be 0
 * @return Pseudo-inverse
 * @pre \c MatrixT is an Eigen dense matrix type (unenforced)
 * @pre \c QRPreconditionerT is a valid Eigen preconditioner type (unenforced)
 * @ingroup Numerical
 */
template<int QRPreconditionerT, class MatrixT>
Matrix<typename ValueTypeM<MatrixT>::type, ColsM<MatrixT>::value, RowsM<MatrixT>::value> ComputePseudoInverse(const MatrixT& operand, typename ValueTypeM<MatrixT>::type zero=numeric_limits<typename ValueTypeM<MatrixT>::type>::epsilon())
{
	JacobiSVD<MatrixT, QRPreconditionerT> svd(operand, Eigen::ComputeFullU | Eigen::ComputeFullV);	//SVD

	//Invert the singular values
	auto singularValues=svd.singularValues();
	unsigned int nNonzero=svd.nonzeroSingularValues();
	for(size_t c=0; c<nNonzero; ++c)
		singularValues(c)= (fabs(singularValues(c))>zero) ? 1.0/singularValues(c) : 0;

	//Recompose
	Matrix<typename ValueTypeM<MatrixT>::type, ColsM<MatrixT>::value, RowsM<MatrixT>::value> mS(operand.cols(), operand.rows());
	mS.setZero();
	mS.block(0, 0, singularValues.size(), singularValues.size()).diagonal()=singularValues;

	return svd.matrixV()*(mS*svd.matrixU().transpose()).eval();
}	//MatrixT ComputePseudoInverse(const MatrixT& operand, typename ValueTypeM::type zero=numeric_limits<typename ValueTypeM<MatrixT>::type>::epsilon())

/**
 * @brief RQ decomposition for a 3x3 matrix
 * @tparam A matrix
 * @param[in] src Matrix to be decomposed
 * @return A tuple: Upper triangular factor and orthonormal factor
 * @pre \c MatrixT is an Eigen dense matrix type holding floating point values (unenforced)
 * @pre \c src is a 3x3 matrix
 * @ingroup Numerical
 */
template<class MatrixT>
tuple<MatrixT, MatrixT> RQDecomposition33(const MatrixT& src)
{
	//Preconditions
	static_assert(is_floating_point<typename ValueTypeM<MatrixT>::type>::value, "RQDecomposition : MatrixT must be a floating point matrix.");
	static_assert(RowsM<MatrixT>::value==Eigen::Dynamic || RowsM<MatrixT>::value==3, "RQDecomposition : MatrixT must be either a dynamic or a 3x3 matrix");
	static_assert(ColsM<MatrixT>::value==Eigen::Dynamic || ColsM<MatrixT>::value==3, "RQDecomposition : MatrixT must be either a dynamic or a 3x3 matrix");

	assert(src.row()==3 && src.col()==3);

	MatrixT mR(src);

	//Compute the Givens rotations to annihilate the subdiagonal elements

	typedef typename ValueTypeM<MatrixT>::type RealT;
	MatrixT mRx; mRx.setIdentity(3,3);
	MatrixT mRy; mRy.setIdentity(3,3);
	MatrixT mRz; mRz.setIdentity(3,3);

	if(mR(2,1)!=0)
	{
		RealT scalex=hypot(mR(2,1), mR(2,2));
		mRx(1,2)=mR(2,1)/scalex; mRx(2,1)=-mRx(1,2);
		mRx(1,1)=mR(2,2)/scalex; mRx(2,2)= mRx(1,1);
		mR*=mRx;
	}	//if(src(2,1)!=0)

	if(mR(2,0)!=0)
	{
		RealT scaley=hypot(mR(2,0), mR(2,2));
		mRy(0,2)=mR(2,0)/scaley; mRy(2,0)=-mRy(0,2);
		mRy(0,0)=mR(2,2)/scaley; mRy(2,2)= mRy(0,0);
		mR*=mRy;
	}	//if(mR(2,0)!=0)

	if(mR(1,0)!=0)
	{
		RealT scalez=hypot(mR(1,0), mR(1,1));
		mRz(1,0)= mR(1,0)/scalez; mRz(0,1)=-mRz(1,0);
		mRz(0,0)=-mR(1,1)/scalez; mRz(1,1)= mRz(0,0);
		mR*=mRz;
	}	//if(mR(1,0)!=0)

	mR(1,0)=0; mR(2,0)=0; mR(2,1)=0;

	//Compose mQ
	MatrixT mQ(mRx); mQ*=mRy; mQ*=mRz;
	mQ.transposeInPlace();

	return make_tuple(mR, mQ);
}	//bool RQDecomposition(MatrixT& mR, MatrixT& mQ, const MatrixT src)

/**
 * @brief UU^T variant of the Cholesky decomposition for a 3x3 matrix
 * @tparam MatrixT A matrix
 * @param[in] src Matrix to be decomposed
 * @return An upper triangular matrix such that \f$ \mathbf{A = UU^T} \f$. Invalid if the input is not positive definite
 * @pre \c MatrixT is either a 3x3 or a dynamic matrix of floating point values
 * @pre \c MatrixT is an Eigen dense object
 * @pre \c src is a 3x3 matrix
 * @remarks If \c src(2,2) is negative, \c src=-src
 * @ingroup Numerical
 */
template<class MatrixT>
optional<MatrixT> UpperCholeskyDecomposition33(MatrixT src)
{
	//Preconditions
	static_assert(is_floating_point<typename ValueTypeM<MatrixT>::type>::value, "UpperCholeskyDecomposition33 : MatrixT must be a floating point matrix.");
	static_assert(RowsM<MatrixT>::value==Eigen::Dynamic || RowsM<MatrixT>::value==3, "UpperCholeskyDecomposition33 : MatrixT must be either a dynamic or a 3x3 matrix");
	static_assert(ColsM<MatrixT>::value==Eigen::Dynamic || ColsM<MatrixT>::value==3, "UpperCholeskyDecomposition33 : MatrixT must be either a dynamic or a 3x3 matrix");
	assert(src.row()==3 && src.col()==3);

	typedef typename ValueTypeM<MatrixT>::type RealT;

	MatrixT output(3,3); output.setZero();

	if(src(2,2)==0)
		return optional<MatrixT>();

	if(src(2,2)<0)
		src=-src;

	output(2,2)=sqrt(src(2,2));
	output(1,2)=src(1,2)/output(2,2);
	output(0,2)=src(0,2)/output(2,2);

	RealT tmp1=src(1,1)-pow<2>(output(1,2));

	if(tmp1<=0)
		return optional<MatrixT>();

	output(1,1)=sqrt(tmp1);
	output(0,1)=(src(0,1)-output(0,2)*output(1,2))/output(1,1);

	RealT tmp0=src(0,0)-pow<2>(output(0,1))-pow<2>(output(0,2));

	if(tmp0<0)
		return optional<MatrixT>();

	output(0,0)=sqrt(tmp0);
	return output;
}	//optional<MatrixT> UpperCholeskyDecomposition33(const MatrixT& src)

/**
 * @brief A=FF' via SVD, for a symmetric positive (semi-)definite matrix
 * @tparam QRPreconditionerT An Eigen QR preconditioner type
 * @tparam MatrixT A floating point matrix
 * @param[in] operand Matrix to be decomposed
 * @return Left factor
 * @pre \c QRPreconditionerT is a valid Eigen SVD preconditioner label (unenforced)
 * @pre \c MatrixT is an Eigen dense object (unenforced)
 * @pre \c operand is symmetric positive semi-definite (unenforced)
 * @remarks If the matrix has negative eigenvalues, left and right factors differ in the signs of some of the columns
 * @ingroup Numerical
 */
template<int QRPreconditionerT, class MatrixT>
MatrixT ComputeSquareRootSVD(const MatrixT& operand)
{
	//Preconditions
	assert(operand.rows()==operand.cols());

	JacobiSVD<MatrixT, QRPreconditionerT> svd(operand, Eigen::ComputeFullU | Eigen::ComputeFullV);

	//Square root of the singular values
	auto singularValues=svd.singularValues();
	size_t nV=singularValues.size();
	for(size_t c=0; c<nV; ++c)
		singularValues(c)=sqrt(singularValues(c));

	return svd.matrixU()*singularValues.asDiagonal();
}	//MatrixT ComputeSquareRootSVD(const MatrixT& operand)

/**
 * @brief Computes the rotation matrix closest to the input in the Frobenius norm sense
 * @tparam MatrixT A matrix
 * @param[in] mW Input
 * @return The rotation matrix closest to \c mW in the Frobenius norm sense
 * @remarks Solves \f$ \min_{\mathbf{R}}{\|\mathbf{R-W}\|} \f$ such that \f$ \mathbf{R} \f$ is a rotation matrix
 * @remarks J. Weng, T. S. Huang, N. Ahuja, "Motion and Structure from Two Perspective Views: Algorithms, Error Analysis and Error Estimation, " PAMI, Vol. 11, pp. 451-474, May 1989
 * @pre \c MatrixT is an Eigen dense object (unenforced)
 * @pre \c MatrixT is a floating point matrix
 * @pre \c mW is 3x3
 * @ingroup Numerical
 */
template<class MatrixT>
MatrixT ComputeClosestRotationMatrix(const MatrixT& mW)
{
	//Preconditions
	static_assert(is_floating_point<typename ValueTypeM<MatrixT>::type>::value, "ComputeClosestRotationMatrix : MatrixT must be a floating point matrix.");
	assert(mW.rows()==3 && mW.cols()==3);

	//Equations 2.20 and 2.21
	Matrix3d mI; mI.setIdentity();

	MatrixT mIpmW=mW+mI;
	MatrixT mImmW=mI-mW;

	Matrix4d mB; mB.setZero();

	for(size_t c=0; c<3; ++c)
	{
		Matrix4d mBi;
		mBi(0,0)=0;

		mBi.block(0,1,1,3)= (mImmW.col(c)).transpose();
		mBi.block(1,0,3,1)= -mBi.block(0,1,1,3).transpose();
		mBi.block(1,1,3,3)=MakeCrossProductMatrix<typename ValueTypeM<MatrixT>::type>( mIpmW.col(c));

		mB+= mBi.transpose() * mBi;
	}	//for(size_t c=0; c<3; ++c)

	//Smallest left singular vector is the rotation quaternion
	JacobiSVD<Matrix4d, Eigen::FullPivHouseholderQRPreconditioner> svd(mB, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Quaternion<double> q;
	q.vec()=(svd.matrixV().col(3).segment(1,3));
	q.w()=svd.matrixV().col(3)[0];

	return q.toRotationMatrix();
}	//MatrixT ComputeClosestRotationMatrix(const MatrixT& mW)

/**
 * @brief Computes the Householder vector that transforms \c x to an elementary vector with sole non-zero entry at \c i
 * @tparam VectorT A vector
 * @param[in] x Input
 * @param[in] i Index of the component to survive
 * @return Householder vector
 * @pre \c VectorT is an Eigen dense object (unenforced)
 * @pre \c VectorT has floating point entries
 * @pre \c i<x.size()
 * @remarks R. Hartley, A. Zissermann, "Multiple View Geometry in Computer Vision, " 2nd Ed., 2003, A4.1.2
 * @remarks \c v is a unit vector. The corresponding Householder matrix is \f$  \mathbf{H}=\mathbf{I}-2\mathbf{v v^T} \f$
 * @remarks \f$ \mathbf{Hx}=-\| \mathbf{x} \| \mathbf{e_i}\f$
 * @warning According to http://www.ee.ic.ac.uk/hp/staff/dmb/matrix/special.html , the surviving element has the value -|x|, not |x|, which is indeed what the function does!
 * @ingroup Numerical
 */
template<class VectorT>
VectorT ComputeHouseholderVector(const VectorT& x, unsigned int i)
{
	//Preconditions
	static_assert(is_floating_point<typename ValueTypeM<VectorT>::type>::value, "ComputeHouseholderVector : VectorT must be a floating point vector.");
	static_assert(ColsM<VectorT>::value==1, "ComputeHouseholderVector : Number of columns for VectorT must be fixed to 1");
	assert(i<x.size());

	typename ValueTypeM<VectorT>::type normx=x.norm();
	if(normx==0)	//Zero vector?
		return x;

	int signxi= sign(x[i])==-1 ? -1 : 1;  //This guarantees a positive ith entry in H*v (can be derived easily: if xi=0, |v|^2 = 2 |x|^2 and v^t x =|x|^2, for unnormalized v. ai - vi = -|x| )

	VectorT v(x);
	v[i] += signxi * x.norm();

	return v.normalized();
}	//VectorT ComputeHouseholderVector(const VectorT& v, unsigned int i)

/**
 * @brief Applies a Householder transformation to a vector
 * @tparam VectorT A vector
 * @param[in] v Householder transformation vector
 * @param[in] x Vector to be transformed
 * @return Rotated vector
 * @pre \c VectorT is an Eigen dense object (unenforced)
 * @pre \c VectorT has floating point entries
 * @pre \c v.size()=x.size()
 * @pre \c v is unit norm
 * @remarks R. Hartley, A. Zissermann, "Multiple View Geometry in Computer Vision, " 2nd Ed., 2003, A4.1.2
 * @remarks \f$ \mathbf{H_v x} = \mathbf{x} - 2 (\mathbf{v^T x}) \mathbf{v} \f$
 * @ingroup Numerical
 */
template<class VectorT>
VectorT ApplyHouseholderTransformationV(const VectorT& v, const VectorT& x)
{
	static_assert(is_floating_point<typename ValueTypeM<VectorT>::type>::value, "ApplyHouseholderTransformationV : VectorT must be a floating point vector.");
	static_assert(ColsM<VectorT>::value==1, "ApplyHouseholderTransformationV : Number of columns for VectorT must be fixed to 1");
	assert(v.size()==x.size());
	assert(fabs(v.norm()-1) < 3*numeric_limits<typename ValueTypeM<VectorT>::type>::epsilon() );

	return x - 2*x.dot(v)*v;
}	//VectorT ApplyHouseholderRotationV(const VectorT& v, const VectorT& x)

/**
 * @brief Applies a Householder transformation to a matrix
 * @tparam VectorT A vector
 * @tparam MatrixT A matrix
 * @param[in] v Householder transformation vector
 * @param[in] mX Matrix to be transformed
 * @return Rotated matrix
 * @pre \c VectorT is an Eigen dense object (unenforced)
 * @pre \c VectorT has floating point entries
 * @pre \c MatrixT is an Eigen dense object (unenforced)
 * @pre \c MatrixT has floating point entries
 * @pre \c v.size()=mX.rows()=mX.cols()
 * @pre \c v is unit norm
 * @remarks R. Hartley, A. Zissermann, "Multiple View Geometry in Computer Vision, " 2nd Ed., 2003, A4.1.2
 * @remarks \f$ \mathbf{H_v X} = \mathbf{X} - 2 \mathbf{v v^T} \mathbf{X} \f$
 * @ingroup Numerical
 */
template<class VectorT, class MatrixT>
MatrixT ApplyHouseholderTransformationM(const VectorT& v, const MatrixT mX)
{
	static_assert(is_floating_point<typename ValueTypeM<VectorT>::type>::value, "ApplyHouseholderTransformationM : VectorT must be a floating point vector.");
	static_assert(is_floating_point<typename ValueTypeM<MatrixT>::type>::value, "ApplyHouseholderTransformationM : MatrixT must be a floating point matrix.");
	static_assert(ColsM<VectorT>::value==1, "ApplyHouseholderTransformationM : Number of columns for VectorT must be fixed to 1");

	assert(v.size()==mX.rows());
	assert(mX.cols()==mX.rows());
	assert(fabs(v.norm()-1) < 3*numeric_limits<typename ValueTypeM<VectorT>::type>::epsilon() );

	return mX - 2* (v*v.transpose()) * mX;
}	//MatrixT ApplyHouseholderRotationM(const VectorT& v, const MatrixT mX)

/**
 * @brief Projects a homogeneous vector to a plane tangent to the unit sphere at a specified point
 * @tparam VectorT Type of the input vectors
 * @tparam VectorRT Type of the return vector
 * @param[in] x Vector to be projected
 * @param[in] contact Contact point on the unit sphere
 * @return Projection of \c x on the tangent plane. Invalid if \c contact has zero norm, or \c x is at infinity
 * @pre \c VectorT and \c VectorRT are Eigen dense objects (unenforced)
 * @pre The number of columns of \c VectorRT is fixed to 1
 * @pre \c VectorRT can be resized to \c x.size()-1
 * @pre \c contact.size()=x.size()
 * @pre \c x.size()>1
 * @remarks R. Hartley, A. Zissermann, "Multiple View Geometry in Computer Vision, " 2nd Ed., 2003, A4.1.2, A6.9.3
 * @remarks W. Forstner, "Minimal Representations for Uncertainty and Estimation in Projective Spaces, " ACCV 2010,  pp. 619-632, Nov. 2010
 * @remarks The origin of the tangent plane is at the contact point
 * @ingroup Numerical
 */
template<class VectorRT, class VectorT>
optional<VectorRT> ProjectHomogeneousToTangentPlane(const VectorT& x, const VectorT& contact)
{
	//Preconditions
	static_assert(ColsM<VectorT>::value==1, "ProjectHomogeneousToTangentPlane : Number of columns for VectorRT must be fixed to 1");

	assert(x.size()>1);
	assert(contact.size()==x.size());

	optional<VectorRT> projected;

	unsigned int sz=x.size();
	if(contact.norm()==0 || x[sz-1]==0)
		return projected;

	//Nullspace of vContact spans its tangent plane. First n-1 rows of the Householder transformation are the basis vectors. And a Householder transformation can be represented by a single vector
	VectorT v=ComputeHouseholderVector(contact, sz-1);

	VectorT hProjected=ApplyHouseholderTransformationV(v, x);	//Move the point to the tangent plane
	projected=hProjected.hnormalized();

	assert(projected->size()==x.size()-1);

	return projected;
}	//VectorT ProjectHomogeneousToTangentPlane(const VectorT& x, const VectorT& contact)

/**
 * @brief Converts a set of Cartesian coordinates to spherical
 * @tparam VectorT A floating point vector
 * @param[in] cartesian Cartesian coordinates
 * @return Spherical coordinates. [radial, azimuthal, polar ]. The azimuthal range is [0, 2*pi), whereas the polar range is [0, pi]
 * @pre \c VectorT is an Eigen dense object (unenforced)
 * @pre \c VectorT holds floating point elements
 * @pre \c cartesian is 3x1
 * @remarks http://mathworld.wolfram.com/SphericalCoordinates.html
 */
template<class VectorT>
VectorT CartesianToSpherical(const VectorT& cartesian)
{
	//Preconditions
	assert(cartesian.rows()==1 && cartesian.cols()==1);
	static_assert(is_floating_point<typename ValueTypeM<VectorT>::type>::value, "CartesianToSpherical: VectorT must be a floating point vector");

	typedef typename ValueTypeM<VectorT>::type RealT;
	VectorT output; output.resize(3);

	output[0]=cartesian.norm();
	output[1]=output[0]==0 ? 0 : remainder(atan2(cartesian[1], cartesian[0]) + two_pi<RealT>(), two_pi<RealT>());	//atan2 range [-pi, pi]. Conversion to the range [0,2pi]
	output[2]=output[0]==0 ? 0 : acos(cartesian[2]/output[0]);	//acos range 0,pi

	return output;
}	//array<typename ValueTypeM<VectorT>::type,3> CartesianToSpherical(const VectorT& cartesian)

/**
 * @brief Converts a set of spherical coordinates to Cartesian
 * @tparam VectorT A floating point vector
 * @param[in] spherical Spherical coordinates. [radial, azimuthal, polar]
 * @return Cartesian coordinates
 * @pre \c spherical[0]>=0, \c 0<=spherical[1]<2pi, \c 0<=spherical[2]<=pi
 *  @pre \c VectorT is an Eigen dense object (unenforced)
 * @pre \c VectorT holds floating point elements
 * @pre \c spherical is 3x1
 */
template<class VectorT>
VectorT SphericalToCartesian(const VectorT& spherical)
{
	typedef typename ValueTypeM<VectorT>::type RealT;

	//Preconditions
	assert(spherical.rows()==1 && spherical.cols()==1);
	assert(spherical[0]>=0);
	assert(spherical[1]>=0 && spherical[1]<2*pi<RealT>());
	assert(spherical[2]>=0 && spherical[2]<=pi<RealT>());

	static_assert(is_floating_point<typename ValueTypeM<VectorT>::type>::value, "SphericalToCartesian: VectorT must be a floating point vector");

	if(spherical[0]==0)
		return VectorT(0,0,0);


	VectorT output; output.resize(3);

	RealT rSinPhi=spherical[0]*sin(spherical[2]);
	output[0]=rSinPhi * cos(spherical[1]);
	output[1]=rSinPhi * sin(spherical[1]);
	output[2]=spherical[0] * cos(spherical[2]);

	return output;
}	//VectorT SphericalToCartesian(const VectorT& spherical)

}	//NumericN
}	//SeeSawN

#endif /* LINEAR_ALGEBRA_IPP_0803332 */
