/*
 * @file numeric.seesaw.dox Documentation for Numeric
 * @author Evren Imre
 * @date 24 Oct 2013
 */
 
  /**
 * @page Numeric
 * - @subpage Jacobian_MatrixInverse
 * - @subpage Jacobian_MatrixNormalise
 * - @subpage Jacobian_dABdA
 * - @subpage Jacobian_dABdB
 * - @subpage Jacobian_dxtAxdx
 */

 /**
  * @page Jacobian_MatrixInverse Jacobian of matrix inversion

Given an \f$ M \times M \f$ matrix \b A, and \f$ \mathbf{B=A^{-T}}\f$

\f{equation*}{
    \mathbf{\nabla_A A^{-1}} =
    \begin{bmatrix}
        -b_{11}\mathbf{B} & -b_{21}\mathbf{B} & \dotsb & -b_{M1}\mathbf{B} \\
        \vdots           & \vdots           &        & \vdots \\
        -b_{1M}\mathbf{B} & -b_{2M}\mathbf{B} & \dotsb & -b_{MM}\mathbf{B}
    \end{bmatrix}_{M^2 \times M^2}
\f} 
 */
 
 /** @page Jacobian_MatrixNormalise Jacobian of matrix normalisation
 
 \f{equation}{
 \nabla_{\mathbf{x}} \frac{\mathbf{x}}{\lVert \mathbf{x} \rVert} = \frac{1}{ \lVert \mathbf{x} \rVert^3 } ( \lVert \mathbf{x} \rVert^2 \mathbf{I} - \mathbf{x} \mathbf{x}^T )
\f}

 */
 
 /** @page Jacobian_dABdA d AB dA

Given a matrix \b A w/dimensions \f$ M \times N \f$, and \b B, w/dimensions \f$ N \times K \f$,

\f{equation*}{

\begin{aligned}
 \mathbf{ \nabla_A AB}  &= 	\begin{bmatrix}
								\mathbf{B}^T       & \mathbf{0}_{K \times N} & \dotsb & \mathbf{0}_{K \times N} \\
								\mathbf{0}_{K \times N} & \mathbf{B}^T& \dotsb & \mathbf{0}_{K \times N} \\
								\vdots             & \vdots                  & \vdots & \vdots \\
								\mathbf{0}_{K \times N} & \mathbf{0}_{K \times N} & \dotsb & \mathbf{B}^T 
							\end{bmatrix}_{MK \times NM}
\end{aligned}

\f}
*/

 /** @page Jacobian_dABdB dAB dB

Given a matrix \b A w/dimensions \f$ M \times N \f$, and \b B, w/dimensions \f$ N \times K \f$,

\f{equation*}{

\begin{aligned}
 \mathbf{ \nabla_B AB}  &= 	\begin{bmatrix}
								a_{11} \mathbf{I}_{K \times K}       & a_{12} \mathbf{I}_{K \times K} & \dotsb & a_{1N} \mathbf{I}_{K \times K} \\
								a_{21} \mathbf{I}_{K \times K}       & a_{22} \mathbf{I}_{K \times K} & \dotsb & a_{2N} \mathbf{I}_{K \times K} \\
								\vdots             & \vdots                  & \vdots & \vdots \\
								a_{M1} \mathbf{I}_{K \times K}       & a_{M2} \mathbf{I}_{K \times K} & \dotsb & a_{MN} \mathbf{I}_{K \times K} \\ 
							\end{bmatrix}_{MK \times NK}
\end{aligned}
\f}


/** @page Jacobian_dxtAxdx d x^t(Ax) dx

Given the symmetric matrix \b A and the vector \b x

\f{equation}{
	\mathbf{\nabla_x x^TAx} =2* \mathbf{ (Ax)^T }
\f}
*/
*/