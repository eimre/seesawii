/**
 * @file PerturbationAnalysisProblemConcept.ipp Implementation of PerturbationAnalysisProblemConceptC
 * @author Evren Imre
 * @date 4 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PERTURBATION_ANALYSIS_PROBLEM_CONCEPT_IPP_7807212
#define PERTURBATION_ANALYSIS_PROBLEM_CONCEPT_IPP_7807212

#include <boost/concept_check.hpp>
#include <Eigen/Dense>
#include <vector>

namespace SeeSawN
{
namespace NumericN
{

using boost::CopyConstructible;
using boost::Assignable;
using std::vector;

/**
 * @brief  Problem concept for various algorithms analysing how a transformation reacts to perturbations
 * @tparam TestT Type to be tested
 * @ingroup Concept
 */
template <class TestT>
class PerturbationAnalysisProblemConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

	public:

		BOOST_CONCEPT_USAGE(PerturbationAnalysisProblemConceptC)
		{
			TestT tested;

			bool flagValid=tested.IsValid(); (void)flagValid;

			typedef typename TestT::input_covariance_type InputCovarianceT;
			InputCovarianceT mC=tested.GetCovariance(); (void)mC;
			InputCovarianceT mF=tested.DecomposeCovariance(); (void)mF;
			unsigned int nDim=tested.InputDimensionality(); (void)nDim;

			typename TestT::can_decompose_covariance covIndicator; (void)covIndicator;

			typedef typename TestT::transformed_sample_type TransformedT;
			TransformedT transformed;
			bool flagSuccess=tested.TransformPerturbed(transformed, mC.col(0)); (void)flagSuccess;

			typedef typename TestT::transformed_gaussian_type TransformedGaussianT;
			TransformedGaussianT result=tested.ComputeTransformedGaussian(vector<TransformedT>(), double(), double(), double()); (void) result;

			bool flagV=tested.NeedValidator(); (void)flagV;

			double alpha=tested.Alpha(); (void)alpha;
		}	//BOOST_CONCEPT_USAGE(PerturbationAnalysisProblemConceptC)
	///@endcond
};	//class PerturbationAnalysisProblemConceptC

}	//NumericN
}	//SeeSawN

#endif /* PERTURBATION_ANALYSIS_PROBLEM_CONCEPT_IPP_7807212 */
