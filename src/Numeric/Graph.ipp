/**
 * @file Graph.ipp Implementation of functions for numerical characterisation of graphs
 * @author Evren Imre
 * @date 1 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef GRAPH_IPP_4129843
#define GRAPH_IPP_4129843

#include <boost/graph/graph_concepts.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/concept_check.hpp>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <cstddef>
#include <iterator>
#include <utility>
#include <type_traits>

namespace SeeSawN
{
namespace NumericN
{

using boost::VertexAndEdgeListGraphConcept;
using boost::num_vertices;
using boost::out_degree;
using boost::vertices;
using boost::graph_traits;
using boost::vertex_index_t;
using boost::vertex_index;
using boost::edges;
using boost::source;
using boost::target;
using boost::undirected_tag;
using boost::property_map;
using Eigen::MatrixXi;
using Eigen::MatrixXd;
using Eigen::SelfAdjointEigenSolver;
using std::size_t;
using std::advance;
using std::pair;
using std::is_same;

/**
 * @brief Returns the Laplacian of a graph
 * @tparam GraphT A graph
 * @param[in] graph Graph to be analysed
 * @pre \c GraphT satisfies the vertex and edge list graph concept
 * @pre \c GraphT is an undirected graph
 * @return The Laplacian
 * @ingroup Numerical
 */
template <class GraphT>
MatrixXi ConstructGraphLaplacian(const GraphT& graph)
{
	BOOST_CONCEPT_ASSERT((VertexAndEdgeListGraphConcept<GraphT>));
	static_assert(is_same<typename graph_traits<GraphT>::directed_category, undirected_tag>::value, "ConstructGraphLaplacian: The graph must be undirected" );

	size_t nVertex=num_vertices(graph);
	MatrixXi mL(nVertex, nVertex); mL.setZero();

	//Iterate over the vertices
	typedef typename graph_traits<GraphT>::vertex_iterator VertexIteratorT;
	typedef typename property_map<GraphT, vertex_index_t>::type IndexMapT;
	IndexMapT indexMap=boost::get(vertex_index, graph);

	pair<VertexIteratorT, VertexIteratorT> itPairV=vertices(graph);
	for(; itPairV.first!=itPairV.second; advance(itPairV.first,1))
	{
		auto i=indexMap[*itPairV.first];
		mL(i,i)=out_degree(*itPairV.first, graph);
	}	//for(; itPairV.first!=itPairV.second; advance(itPairV.first,1))

	//Iterate over the edges
	typedef typename graph_traits<GraphT>::edge_iterator EdgeIteratorT;
	pair<EdgeIteratorT, EdgeIteratorT> itPairE=edges(graph);
	for(; itPairE.first!=itPairE.second; std::advance(itPairE.first,1))
	{
		auto i1=indexMap[source(*itPairE.first, graph)];
		auto i2=indexMap[target(*itPairE.first, graph)];
		mL(i1,i2)=mL(i2,i1)=-1;
	}	//for(; itPairE.first!=itPairE.second; advance(itPairE.first,1))

	return mL;
}	//MatrixXi MakeGraphLaplacian(const GraphT& graph)

/**
 * @brief Returns the number of spanning trees
 * @tparam GraphT A graph
 * @param[in] graph Graph to be analysed
 * @returns The number of spanning trees for the graph. 0 indicates a disconnected graph (or one with a single vertex)
 * @pre \c graph is a simple graph (unenforced)
 * @remarks https://en.wikipedia.org/wiki/Kirchhoff's_theorem
 * @remarks Beware of overflows in large and densely connected graphs
 */
template<class GraphT>
long double CountSpanningTrees(const GraphT& graph)
{
	MatrixXi mL=ConstructGraphLaplacian(graph);
	MatrixXd mLd=mL.cast<double>();

	if(mL.rows()==1)
		return 0;

	SelfAdjointEigenSolver<MatrixXd> eigenSolver(mLd, Eigen::EigenvaluesOnly);

	typename SelfAdjointEigenSolver<MatrixXd>::RealVectorType eigenvalues=eigenSolver.eigenvalues();

	//The number of 0 eigenvalues indicate the number of connected components. If the second smallest eigenvalue is zero, the graph is not connected, and there are no trees that can span the entire graph
	if(eigenvalues[1]==0)
		return 0;

	//In a connected graph, only one eigenvalue is 0. The number of spanning trees is the multiplication of the remanining eigenvalues
	size_t nVertex=mL.rows();
	long double output=1;
	for(size_t c=1; c<nVertex; ++c)
		output*=eigenvalues[c];

	return output/nVertex;
}	//optional<long double> CountSpanningTrees(const GraphT& graph)

}	//NumericN
}	//SeeSawN




#endif /* GRAPH_IPP_4129843 */
