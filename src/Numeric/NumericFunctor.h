/**
 * @file NumericFunctor.h Public interface for various numeric functors
 * @author Evren Imre
 * @date 22 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERIC_FUNCTOR_H_9131720
#define NUMERIC_FUNCTOR_H_9131720

#include "NumericFunctor.ipp"
namespace SeeSawN
{
namespace NumericN
{

template<typename ValueT> struct ReciprocalC;
template<class MatrixT> struct MatrixDifferenceC;

/********** EXTERN TEMPLATES **********/
extern template class ReciprocalC<double>;
extern template class ReciprocalC<float>;
}   //NumericN
}	//SeeSawN

#endif /* NUMERIC_FUNCTOR_H_9131720 */
