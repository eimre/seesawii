/**
 * @file Polynomial.h Public interface for various polynomial operations
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POLYNOMIAL_H_2561235
#define POLYNOMIAL_H_2561235

#include "Polynomial.ipp"
namespace SeeSawN
{
namespace NumericN
{

array<complex<double>,2> QuadraticPolynomialRoots(const array<double,3>& coefficients, double zero=3*numeric_limits<double>::epsilon());    ///< Computes the roots of a real quadratic polynomial
array<complex<double>,3> CubicPolynomialRoots(const array<double,4>& coefficients, double zero=3*numeric_limits<double>::epsilon());    ///< Computes the roots of a real cubic polynomial

template<unsigned int N> array<complex<double>,N> PolynomialRoots(const array<double,N+1>& coefficients, double zero);	///< Computes the roots of a real polynomial via the companion matrix

}   //NumericN
}	//SeeSawN


/********** EXTERN TEMPLATES **********/
extern template class std::array<double,2>;
extern template class std::array<double,3>;
extern template class std::array<double,4>;
extern template class std::array<std::complex<double>,2>;
extern template class std::array<std::complex<double>,3>;
#endif /* POLYNOMIAL_H_ */
