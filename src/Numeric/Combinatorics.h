/**
 * @file Combinatorics.h Public interface for various combinatorics functions
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COMBINATORICS_H_6987621
#define COMBINATORICS_H_6987621

#include "Combinatorics.ipp"
namespace SeeSawN
{
namespace NumericN
{

vector<set<unsigned int> > GenerateCombinations(unsigned int n, unsigned int k);	///< Generates all k-element subsets of an n-element set
long double GetCombinationIndex(const set<unsigned int>& combination);	///< Maps a combination to a natural number

template<class RNGT> optional<set<unsigned int> > GenerateRandomCombination(RNGT& rng, unsigned int n, unsigned int k);	///< Generates a random k-element combination from an n-element set

}	//NumericN
}	//SeeSawN

#endif /* COMBINATORICS_H_6987621 */
