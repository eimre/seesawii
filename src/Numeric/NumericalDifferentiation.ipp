/**
 * @file NumericalDifferentiation.ipp Implementation of the numerical differentiation module
 * @author Evren Imre
 * @date 7 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERICAL_DIFFERENTIATION_IPP_3090232
#define NUMERICAL_DIFFERENTIATION_IPP_3090232

#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <vector>
#include <cstddef>
#include <cmath>
#include <stdexcept>
#include <string>
#include "NumericalJacobianProblemConcept.h"

namespace SeeSawN
{
namespace NumericN
{

using boost::lexical_cast;
using Eigen::Matrix;
using std::vector;
using std::size_t;
using std::max;
using std::fabs;
using std::invalid_argument;
using std::string;
using SeeSawN::NumericN::NumericalJacobianProblemConceptC;

/**
 * @brief Parameter object for \c NumericalJacobianC
 * @ingroup Parameters
 */
struct NumericalJacobianParametersC
{
	double relativePerturbation;	///< Relative perturbation. >0
	double minAbsolutePerturbation;	///<  Minimum absolute value of the perturbation >0

	NumericalJacobianParametersC() : relativePerturbation(1e-4), minAbsolutePerturbation(1e-6)
	{}
};	//struct NumericalJacobianParametersC

/**
 * @brief Diagnostics object for \c NumericalJacobianC
 * @ingroup Diagnostics
 */
struct NumericalJacobianDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully
	vector<double> perturbations;	///< Magnitude of the perturbations applies to the input parameters
};	//struct NumericalJacobianDiagnosticsC

/**
 * @brief Computes the Jacobian of a function numerically
 * @tparam ProblemT Problem type
 * @remarks Central difference scheme
 * @pre \c ProblemT is a model of \c NumericalJacobianProblemConceptC
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class NumericalJacobianC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((NumericalJacobianProblemConceptC<ProblemT>));
	///@endcond

	private:

		typedef typename ProblemT::real_type RealT;	///< Floating point type
		typedef Matrix<RealT, Eigen::Dynamic, 1> VectorT;	///< A floating point vector type

		/** @name Implementation details */ ///@{
		static void ValidateParameters(const NumericalJacobianParametersC& parameters);	///< Verifies the validity of the input parameters
		static bool ApplyPerturbation(VectorT& output, const ProblemT& problem, const VectorT& perturbation);	///< Applies a perturbation to the input and returns the corresponding output
		///@}

	public:

		typedef Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> jacobian_type;	///< Type of the Jacobian matrix

		static NumericalJacobianDiagnosticsC Run(jacobian_type& jacobian, const ProblemT& problem, const NumericalJacobianParametersC& parameters);	///< Numerical differentiation

};	//class NumericalJacobianC

/**
 * @brief Verifies the validity of the input parameters
 * @param[in] parameters Parameters
 * @throws invalid_argument If any of the input parameters has an invalid value
 */
template<class ProblemT>
void NumericalJacobianC<ProblemT>::ValidateParameters(const NumericalJacobianParametersC& parameters)
{
	if(parameters.relativePerturbation<=0)
		throw(invalid_argument(string("NumericalJacobianC::ValidateParameters : relativePerturbation must be positive. Value=")+lexical_cast<string>(parameters.relativePerturbation)));

	if(parameters.minAbsolutePerturbation<=0)
		throw(invalid_argument(string("NumericalJacobianC::ValidateParameters : minAbsolutePerturbation must be positive. Value=")+lexical_cast<string>(parameters.minAbsolutePerturbation)));
}	//void ValidateInput(const NumericalJacobianParametersC& parameters)

/**
 * @brief Applies a perturbation to the input and returns the corresponding output
 * @param[out] output Output vector
 * @param[in] problem Problem
 * @param[in] perturbation Perturbation
 * @return \c false if the transformation fails
 */
template<class ProblemT>
bool NumericalJacobianC<ProblemT>::ApplyPerturbation(VectorT& output, const ProblemT& problem, const VectorT& perturbation)
{
	typename ProblemT::transformed_sample_type transformed;
	bool flagSuccess=problem.TransformPerturbed(transformed, perturbation);

	if(!flagSuccess)
		return false;

	output=problem.MakeOutputVector(transformed);
	return true;
}	//VectorT ApplyPerturbation(const ProblemT& problem, const VectorT& perturbation)

/**
 * @brief Numerical differentiation
 * @param[out] jacobian Jacobian matrix
 * @param[in] problem Problem
 * @param[in] parameters Parameters object
 * @return Diagnostics object
 * @throws invalid_argument If invalid problem or input parameters
 */
template<class ProblemT>
NumericalJacobianDiagnosticsC NumericalJacobianC<ProblemT>::Run(jacobian_type& jacobian, const ProblemT& problem, const NumericalJacobianParametersC& parameters)
{
	//Preconditions
	if(!problem.IsValid())
		throw(invalid_argument("NumericalJacobianC::Run : Invalid problem"));

	ValidateParameters(parameters);

	NumericalJacobianDiagnosticsC diagnostics;
	diagnostics.flagSuccess=false;

	VectorT vInput=problem.GetInputVector();
	size_t sInput=vInput.size();

	VectorT perturbationVector(sInput); perturbationVector.setZero();
	diagnostics.perturbations.reserve(sInput);

	for(size_t c=0; c<sInput; ++c)
	{
		//Negative

		perturbationVector[c] = -max(fabs(vInput[c])*parameters.relativePerturbation, parameters.minAbsolutePerturbation);

		VectorT vM;
		bool flagSuccessM=ApplyPerturbation(vM, problem, perturbationVector);

		if(!flagSuccessM)
			return diagnostics;

		//Positive

		perturbationVector[c]=-perturbationVector[c];

		VectorT vP;
		bool flagSuccessP=ApplyPerturbation(vP, problem, perturbationVector);

		if(!flagSuccessP)
			return diagnostics;

		if(c==0)
			jacobian.resize(vP.size(), sInput);

		jacobian.col(c)=(vP-vM)/(2*perturbationVector[c]);
		diagnostics.perturbations.push_back(perturbationVector[c]);
		perturbationVector[c]=0;
	}	//for(size_t c=0; c<sInput; ++c)

	diagnostics.flagSuccess=true;
	return diagnostics;
}	//jacobian_type Run(const ProblemT& problem, double relativePerturbation, double minAbsolutePerturbation)

}	//NumericN
}	//SeeSawN

#endif /* NUMERICAL_DIFFERENTIATION_IPP_3090232 */
