/**
 * @file Combinatorics.ipp Details of various combinatorics functions
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COMBINATORICS_IPP_0812231
#define COMBINATORICS_IPP_0812231

#include <boost/math/special_functions/binomial.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <cstddef>
#include <climits>
#include <random>

namespace SeeSawN
{
namespace NumericN
{

using boost::math::binomial_coefficient;
using boost::optional;
using std::vector;
using std::set;
using std::iota;
using std::mismatch;
using std::pair;
using std::size_t;
using std::numeric_limits;
using std::uniform_int_distribution;

/**
 * @brief Generates a random k-element combination from an n-element set
 * @tparam RNGT A random number generation engine
 * @param[in,out] generator Random number generator
 * @param[in] n Number of elements in the set
 * @param[in] k Number of elements in the combination
 * @return A randomly generated combination. If n<k, invalid
 * @pre \c GeneratorT is an stl pseudo-random number engine (unenforced)
 * @remarks This is an alternative to SGI's nonstandard extension function random_sample_n. It is not efficient when n is close to k
 * @ingroup Numerical
 */
template<class RNGT>
optional<set<unsigned int> > GenerateRandomCombination(RNGT& generator, unsigned int n, unsigned int k)
{
	if(n<k)
		return optional<set<unsigned int> >();

	set<unsigned int> output;

	uniform_int_distribution<unsigned int> dist(0, n-1);
	//auto urn=bind(dist, generator);	//Warning: bind takes a copy of its arguments. So the state of rng is not advanced!

	while(output.size()<k)
		output.insert(dist(generator));

	return output;
}	//set<unsigned int> GenerateRandomCombination(RNGT& generator, unsigned int n, unsigned int k)

}	//NumericN
}	//SeeSawN

#endif /* COMBINATORICS_IPP_0812231 */
