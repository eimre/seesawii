/**
 * @file LinearAlgebra.h Public interface for various linear algebra functions
 * @author Evren Imre
 * @date 18 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LINEAR_ALGEBRA_H_6792103
#define LINEAR_ALGEBRA_H_6792103

#include "LinearAlgebra.ipp"
#include <Eigen/Dense>

namespace SeeSawN
{
namespace NumericN
{

template<int QRPreconditionerT, class MatrixT> tuple<unsigned int, MatrixT> MakeRankN(const MatrixT& input, unsigned int targetRank, typename ValueTypeM<MatrixT>::type zero);	///< Reduces the rank of a matrix to a specified value by removing the weakest components
template<int QRPreconditionerT, class MatrixT> Matrix<typename ValueTypeM<MatrixT>::type, ColsM<MatrixT>::value, RowsM<MatrixT>::value> ComputePseudoInverse(const MatrixT& operand, typename ValueTypeM<MatrixT>::type zero);	///< Computes the Moore-Penrose pseudo-inverse
template<class MatrixT> tuple<MatrixT, MatrixT> RQDecomposition33(const MatrixT& src);	///< RQ decomposition for a 3x3 matrix
template<class MatrixT> optional<MatrixT> UpperCholeskyDecomposition33(MatrixT src);	///< UU^T variant of the Cholesky decomposition for a 3x3 matrix

template<int QRPreconditionerT, class MatrixT> MatrixT ComputeSquareRootSVD(const MatrixT& operand);	///< A=FF' via SVD, for a symmetric positive (semi-)definite matrix

template<class MatrixT> MatrixT ComputeClosestRotationMatrix(const MatrixT& mW);	///< Computes the rotation matrix closest to the input in the Frobenius norm sense

template<class VectorT> VectorT ComputeHouseholderVector(const VectorT& x, unsigned int i);	///< Computes the Householder vector that transforms a \c x to an elementary vector with sole non-zero entry at \c i
template<class VectorT> VectorT ApplyHouseholderTransformationV(const VectorT& v, const VectorT& x);	///< Applies a Householder transformation to a vector
template<class VectorT, class MatrixT> MatrixT ApplyHouseholderTransformationM(const VectorT& v, const MatrixT mX);	///< Applies a Householder transformation to a matrix

template<class VectorRT, class VectorT> optional<VectorRT> ProjectHomogeneousToTangentPlane(const VectorT& x, const VectorT& contact);	///< Projects a homogeneous vector to a plane tangent to the unit sphere at a specified point

template<class VectorT> VectorT CartesianToSpherical(const VectorT& cartesian);	///< Converts a set of Cartesian coordinates to spherical
template<class VectorT> VectorT SphericalToCartesian(const VectorT& spherical);	///< Converts a set of spherical coordinates to Cartesian

/********** EXTERN TEMPLATES **********/
using Eigen::Matrix3d;
extern template tuple<unsigned int, Matrix3d> MakeRankN<Eigen::FullPivHouseholderQRPreconditioner>(const Matrix3d&, unsigned int, double zero);

using Eigen::MatrixXd;
//extern template MatrixXd ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(const MatrixXd&, double);

extern template tuple<Matrix3d, Matrix3d> RQDecomposition33(const Matrix3d&);

extern template optional<Matrix3d> UpperCholeskyDecomposition33(Matrix3d);

extern template Matrix3d ComputeClosestRotationMatrix(const Matrix3d&);

using Eigen::Vector3d;
extern template Vector3d CartesianToSpherical(const Vector3d&);
extern template Vector3d SphericalToCartesian(const Vector3d&);

}	//NumericN
}	//SeeSawN

#endif /* LINEAR_ALGEBRA_H_6792103 */
