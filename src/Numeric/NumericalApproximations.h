/**
 * @file NumericalApproximations.h Public interface for numeric approximation functions
 * @author Evren Imre
 * @date 22 Jan 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERICAL_APPROXIMATIONS_H_5080912
#define NUMERICAL_APPROXIMATIONS_H_5080912

#include "NumericalApproximations.ipp"
namespace SeeSawN
{
namespace NumericN
{

template<typename RealT> RealT ComputeArithmeticGeometricMean(RealT x, RealT y, unsigned int maxIteration, RealT maxRelativeError);	///< Computes the arithmetic-geometric mean
template<typename RealT> RealT ApproximateLog(RealT x, unsigned int fidelity);	///< Approximates the natural logarithm function

template<class RandomAccessRangeT> vector<unsigned int> ApplyDHondt(const RandomAccessRangeT& votes, unsigned int nSeats);	///< Applies d'Hondt's method for approximate proportional distribution

/********** EXTERN TEMPLATES **********/
extern template double ComputeArithmeticGeometricMean(double, double, unsigned int, double);
extern template double ApproximateLog(double, unsigned int);
extern template vector<unsigned int> ApplyDHondt(const vector<unsigned int>&, unsigned int);

}	//NumericN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<unsigned int>;

#endif /* NUMERICAL_APPROXIMATIONS_H_5080912 */
