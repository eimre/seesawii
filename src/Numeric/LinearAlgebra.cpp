/**
 * @file LinearAlgebra.cpp Instantiations of various linear algebra functions
 * @author Evren Imre
 * @date 18 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "LinearAlgebra.h"

namespace SeeSawN
{
namespace NumericN
{

/********** EXPLICIT INSTANTIATIONS **********/
template tuple<unsigned int, Matrix3d> MakeRankN<Eigen::FullPivHouseholderQRPreconditioner>(const Matrix3d&, unsigned int, double zero);
//template MatrixXd ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(const MatrixXd&, double);
template tuple<Matrix3d, Matrix3d> RQDecomposition33(const Matrix3d&);
template optional<Matrix3d> UpperCholeskyDecomposition33(Matrix3d);
template Matrix3d ComputeClosestRotationMatrix(const Matrix3d&);

template Vector3d CartesianToSpherical(const Vector3d&);
template Vector3d SphericalToCartesian(const Vector3d&);

}	//NumericN
}	//SeeSawN


