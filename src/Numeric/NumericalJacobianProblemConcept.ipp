/**
 * @file NumericalJacobianProblemConcept.ipp Implementation of \c NumericalJacobianProblemConceptC
 * @author Evren Imre
 * @date 8 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERICAL_JACOBIAN_PROBLEM_CONCEPT_IPP_4590212
#define NUMERICAL_JACOBIAN_PROBLEM_CONCEPT_IPP_4590212

#include <Eigen/Dense>
#include <boost/concept_check.hpp>
#include <type_traits>

namespace SeeSawN
{
namespace NumericN
{

using boost::CopyConstructible;
using boost::Assignable;
using Eigen::Matrix;
using std::is_floating_point;

/**
 * @brief Problem concept for numerical Jacobian computation
 * @tparam TestT Type to be tested
 * @ingroup Concept
 */
template <class TestT>
class NumericalJacobianProblemConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

	public:

		BOOST_CONCEPT_USAGE(NumericalJacobianProblemConceptC)
		{
			typedef typename TestT::real_type RealT;
			static_assert(is_floating_point<RealT>::value, "NumericalJacobianProblemConceptC: RealT must be a floating point type");
			RealT temp=0; (void)temp;

			TestT tested;
			Matrix<RealT, Eigen::Dynamic, -1> inputVector=tested.GetInputVector(); (void)inputVector;

			Matrix<RealT, Eigen::Dynamic, -1> perturbation;
			typename TestT::transformed_sample_type transformed;
			bool flagSuccess=tested.TransformPerturbed(transformed, perturbation); (void)flagSuccess;
			Matrix<RealT, Eigen::Dynamic, -1> outputVector=tested.MakeOutputVector(transformed); (void)outputVector;
		}	//BOOST_CONCEPT_USAGE(NumericalJacobianProblemConceptC)

	///@endcond
};	//class NumericalJacobianProblemConceptC

}	//NumericN
}	//SeeSawN

#endif /* NUMERICAL_JACOBIAN_PROBLEM_CONCEPT_IPP_4590212 */
