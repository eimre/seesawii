/**
 * @file Polynomial.ipp Details of various polynomial solvers
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POLYNOMIAL_IPP_7221356
#define POLYNOMIAL_IPP_7221356

#include <boost/math/special_functions/pow.hpp>
#include <boost/math/special_functions/sign.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/Polynomials>
#include <array>
#include <complex>
#include <cmath>
#include <limits>

namespace SeeSawN
{
namespace NumericN
{
using boost::math::pow;
using boost::math::sign;
using boost::math::constants::pi;
using Eigen::Matrix;
using Eigen::PolynomialSolver;
using std::array;
using std::complex;
using std::fabs;
using std::sqrt;
using std::cbrt;
using std::acos;
using std::fma;
using std::numeric_limits;

/**
 * @brief Computes the roots of a real polynomial via the companion matrix
 * @tparam N Degree of the polynomial
 * @param[in] coefficients Coefficients. ax^n+bx^(n-1)...=0
 * @param[in] zero If the absolute value of the discriminant is below this value, it is set to zero
 * @return Roots
 * @remarks Unsupported Eigen module. Accuracy may suffer if there are several roots with the same modulus. Should be ok at double precision and N<20
 */
template<unsigned int N>
array<complex<double>,N> PolynomialRoots(const array<double,N+1>& coefficients, double zero=3*numeric_limits<double>::epsilon())
{
	Matrix<double, N+1, 1> input;
	for(size_t c=0; c<=N; ++c)
		input[N-c]=coefficients[c];

	PolynomialSolver<double,N> solver(input);
	auto roots=solver.roots();

	array<complex<double>, N> output;
	for(size_t c=0; c<N; ++c)
		output[c] = complex<double>(roots[c].real(), (fabs(roots[c].imag()) < zero) ? 0 : roots[c].imag());

	return output;
}	//array<complex<double>,N> PolynomialRoots(const array<double,N>& coefficients, double zero=3*numeric_limits<double>::epsilon())

}   //NumericN
}	//SeeSawN

#endif /* POLYNOMIAL_IPP_ */
