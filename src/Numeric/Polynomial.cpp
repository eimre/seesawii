/**
 * @file Polynomial.cpp Implementation of various polynomial operations
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Polynomial.h"

namespace SeeSawN
{
namespace NumericN
{

/**
 * @brief Computes the roots of a real quadratic polynomial
 * @param[in] coefficients Coefficients. ax^2+bx+c=0
 * @param[in] zero If the absolute value of the discriminant is below this value, it is set to zero
 * @return Roots
 * @pre \c coefficients[0]!=0
 * @warning Due to round-off errors, even real roots might have a complex component
 * @ingroup Numerical
 */
array<complex<double>,2> QuadraticPolynomialRoots(const array<double,3>& coefficients, double zero)
{
    //Preconditions
    assert(coefficients[0]!=0);

    double denum=2*coefficients[0];
    double t1=-coefficients[1]/denum;

    double delta = pow<2>(t1) - coefficients[2]/coefficients[0];

    if(fabs(delta)<zero)
        delta=0;

    //Real roots
    array<complex<double>,2> output;
    if(delta>=0)
    {
        double t2=sqrt(delta);
        output[0]=complex<double>( t1+t2,0);
        output[1]=complex<double>( t1-t2,0);
    }   //if(delta>=0)
    else    //Complex roots
    {
        double t2=sqrt(-delta);
        output[0]=complex<double>(t1, t2);
        output[1]=complex<double>(t1, -t2);
    }   //if(delta>=0)

    return output;
}   //array<complex<double>,2> QuadraticPolynomialRoots(const array<double,3>& coefficients)

/**
 * @brief Computes the roots of a real cubic polynomial
 * @param[in] coefficients Coefficients. ax^3+bx^2+cx+d=0
 * @param[in] zero If the absolute value of the discriminant is below this value, it is set to zero
 * @return Roots
 * @pre \c coefficients[0]!=0
 * @return Roots
 * @remarks http://en.wikipedia.org/wiki/Cubic_equation#Cardano.27s_method . Exact solution
 * @ingroup Numerical
 */
array<complex<double>,3> CubicPolynomialRoots(const array<double,4>& coefficients, double zero)
{
    //Preconditions
    assert(coefficients[0]!=0);

    //Normalise
    double b=coefficients[1]/coefficients[0];
    double c=coefficients[2]/coefficients[0];
    double d=coefficients[3]/coefficients[0];

    double q,r,dd,s,t;   //temporary variables
    double q3,sqd,sqq;
    double th;
    double a23;

    array<complex<double>, 3> output;

    //compute polynomial discriminant

    q=fma(3, c, -pow<2>(b))/9;
    r=(fma(9, c*b, -27*d)-2*pow<3>(b))/54;

    q3=pow<3>(q);
    dd=q3+pow<2>(r);

    a23=-b/3;

    if(fabs(dd)<zero)
        dd=0;

    //Complex or duplicate roots
    if(dd>=0)
    {
        double im;  //Imaginary part of the root
        double re;  //Real part of the root

        sqd=sqrt(dd);

        s=sign(r+sqd)*cbrt(fabs(r+sqd));
        t=sign(r-sqd)*cbrt(fabs(r-sqd));

        im=sqrt(3.0)*(s-t)*0.5; //imaginary part
        re=fma(-0.5, s+t, a23);   //real part

        if(fabs(im)<zero)
            im=0;

        output[0]=complex<double>(a23+s+t,0);
        output[1]=complex<double>(re, im);
        output[2]=complex<double>(re, -im);
    }   //if(dd>=0)
    else    //Distinct real roots
    {
        sqq=2*sqrt(-q);

        th=acos(r/sqrt(-q3));

        output[0]=complex<double>(fma(sqq,cos(th/3),a23),0);
        output[1]=complex<double>(fma(sqq,cos((th+2*pi<double>())/3),a23));
        output[2]=complex<double>(fma(sqq,cos((th+4*pi<double>())/3),a23));
    }   //if(dd>=0)

    return output;
}   //array<complex<double>,3> CubicPolynomialRoots(const array<double,4>& coefficients, double zero=3*numeric_limits<double>::epsilon())

}   //NumericN
}	//SeeSawN


