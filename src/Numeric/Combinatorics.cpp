/**
 * @file Combinatorics.cpp Implementation and instantiations of various combinatorics functions
 * @author Evren Imre
 * @date 27 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Combinatorics.h"
namespace SeeSawN
{
namespace NumericN
{

/**
 * @brief Generates all k-element subsets of an n-element set
 * @param[in] n Cardinality of the set
 * @param[in] k Cardinality of the subset
 * @return All k-element subsets of an n element set
 * @pre \c n>=k
 * @pre \c C(n;k) is below max \c size_t
 * @remarks Based on the code at http://www.aconnect.de/friends/editions/computer/combinatoricode_e.html
 * @ingroup Numerical
 */
vector<set<unsigned int> > GenerateCombinations(unsigned int n, unsigned int k)
{
	//Preconditions
	assert(n>=k);
	assert(binomial_coefficient<double>(n,k)<=numeric_limits<size_t>::max());

	typedef set<unsigned int> SetT;
	vector<SetT> setList;

	//Empty set
	if(k==0)
		return setList;

	//Initialise
	setList.resize((size_t)binomial_coefficient<double>(n,k));
	vector<unsigned int> currentSet(k);
	iota(currentSet.begin(), currentSet.end(), 0);

	setList[0].insert(currentSet.begin(), currentSet.end());
	vector<unsigned int> masterIndexList(k);	//< Indices from n-k to n-1, in reverse order
	iota(masterIndexList.rbegin(), masterIndexList.rend(), n-k);

	auto milEnd=masterIndexList.end();

    //Lexicographical order
    unsigned int nm1=n-1;
    unsigned int km1=k-1;
    unsigned int nmk=n-k;
    size_t index=1;
    while(true)
    {
        //Increase the rightmost element, if possible
        if(currentSet[km1]<nm1)
            currentSet[km1]++;
        else
        {
        	//Find the rightmost eligible element and increase it by 1
        	auto it=mismatch(masterIndexList.begin(), masterIndexList.end(), currentSet.rbegin());

        	 if(it.first==milEnd)
				return setList; //we are done

        	 size_t iUpdate= (size_t)(*(it.first) - nmk);  //Index to be updated
			 currentSet[iUpdate]++;

			 for(size_t c=iUpdate+1; c<k; ++c)
				 currentSet[c]=currentSet[c-1]+1;
        }	//if(currentSet[km1]<nm1)

        setList[index].insert(currentSet.cbegin(), currentSet.cend());
        ++index;
    }	//while(true)

    return setList;
}	//vector<vector<unsigned int> > GenerateCombinations(unsigned int n, unsigned int k)

/**
 * @brief  Maps a combination to a natural number
 * @param[in] combination Input set
 * @return Index of the input
 * @remarks Combinatorial number system, http://en.wikipedia.org/wiki/Combinatorial_number_system (17.03.2011)
 * @ingroup Numerical
 */
long double GetCombinationIndex(const set<unsigned int>& combination)
{
	long double result=0;
	unsigned int index=0;
	for(auto current : combination)
	{
		++index;

		if(current<index)
			continue;

		result+=binomial_coefficient<long double>(current, index);
	}	//for(auto current : combination)

	return result;
}	//unsigned long long GetCombinationIndex(const set<unsigned int>& combination)

}	//NumericN
}	//SeeSawN

