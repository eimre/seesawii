/**
 * @file NumericalDifferentiation.h Public interface for the numerical differentiation module
 * @author Evren Imre
 * @date 7 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NUMERICAL_DIFFERENTIATION_H_4902342
#define NUMERICAL_DIFFERENTIATION_H_4902342

#include "NumericalDifferentiation.ipp"
namespace SeeSawN
{
namespace NumericN
{

struct NumericalJacobianParametersC;	///< Parameter object for \c NumericalJacobianC
struct NumericalJacobianDiagnosticsC;	///<  Diagnostics object for \c NumericalJacobianC
template<class ProblemT> class NumericalJacobianC;	///< Computes the Jacobian of a function numerically
}	//NumericN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<double>;
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template std::string boost::lexical_cast<std::string, float>(const float&);

#endif /* NUMERICAL_DIFFERENTIATION_H_4902342 */
