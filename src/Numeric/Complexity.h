/**
 * @file Complexity.h Public interface for computational complexity estimation tools
 * @author Evren Imre
 * @date 26 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COMPLEXITY_H_5209402
#define COMPLEXITY_H_5209402

#include "Complexity.ipp"
namespace SeeSawN
{
namespace NumericN
{

class ComplexityEstimatorC;	///< Estimates the computational complexity of various linear algebra operations

/********** EXTERN TEMPLATES **********/
extern template double ComplexityEstimatorC::Interpolate<1>(const TableT&, size_t, bool, bool);
extern template double ComplexityEstimatorC::Interpolate<2>(const TableT&, size_t, bool, bool);
extern template double ComplexityEstimatorC::Interpolate<3>(const TableT&, size_t, bool, bool);


}	//NumericN
}	//SeeSawN

#endif /* COMPLEXITY_H_5209402 */
