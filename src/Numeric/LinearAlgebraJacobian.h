/**
 * @file LinearAlgebraJacobian.h Public interface for the Jacobians of common linear algebra functions
 * @author Evren Imre
 * @date 25 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LINEAR_ALGEBRA_JACOBIAN_H_5687122
#define LINEAR_ALGEBRA_JACOBIAN_H_5687122

#include "LinearAlgebraJacobian.ipp"

namespace SeeSawN
{
namespace NumericN
{

template<class MatrixJT, class MatrixAT> MatrixJT JacobiandAidA(const MatrixAT& mA);	///< Jacobian of the matrix inverse operation
template<class MatrixJT, class MatrixAT> MatrixJT JacobianNormalise(const MatrixAT& mA);	///< Jacobian of division of a matrix by its Frobenius norm
template<class MatrixJT, class MatrixAT, class MatrixBT> MatrixJT JacobiandABdA(const MatrixAT& mA, const MatrixBT& mB);	///< Jacobian of the matrix multiplication operation wrt/ first operand
template<class MatrixJT, class MatrixAT, class MatrixBT> MatrixJT JacobiandABdB(const MatrixAT& mA, const MatrixBT& mB);	///< Jacobian of the matrix multiplication operation wrt/ second operand
template<class MatrixJT, class MatrixAT> MatrixJT JacobianAOveraNdA(const MatrixAT& mA, size_t iRow, size_t iCol);	///< Jacobian of the division of a matrix with one of its elements
template<class MatrixJT, class MatrixAT> MatrixJT JacobiandAtdA(const MatrixAT& mA);	///< Jacobian of the transposition operation
template<class MatrixJT, class SymmetricMatrixT, class VectorT> MatrixJT JacobiandxtAxdx(const SymmetricMatrixT& mA, const VectorT& x);	///< Jacobian of the quadratic form (x^t A x) wrt x

template<class MatrixJT> MatrixJT JacobiandAda(size_t nCells, const vector<size_t>& indexList);	///< Jacobian of a matrix with respect to some of its elements

/********** EXTERN TEMPLATES **********/
using Eigen::MatrixXd;
using Eigen::Matrix3d;
extern template MatrixXd JacobiandAidA(const Matrix3d&);

extern template Matrix<double,9,9> JacobianNormalise(const Matrix3d&);

}	//NumericN
}	//SeeSawN

#endif /* LINEAR_ALGEBRA_JACOBIAN_H_5687122 */
