/**
 * @file PerturbationAnalysisProblemConcept.h Public interface for PerturbationAnalysisProblemConceptC
 * @author Evren Imre
 * @date 4 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef PERTURBATION_ANALYSIS_PROBLEM_CONCEPT_H_0909212
#define PERTURBATION_ANALYSIS_PROBLEM_CONCEPT_H_0909212

#include "PerturbationAnalysisProblemConcept.ipp"
namespace SeeSawN
{
namespace NumericN
{

template <class TestT> class PerturbationAnalysisProblemConceptC;	///< Problem concept for various algorithms analysing how a transformation reacts to perturbations
}	//NumericN
}	//SeeSawN

#endif /* PERTURBATION_ANALYSIS_PROBLEM_CONCEPT_H_0909212 */
