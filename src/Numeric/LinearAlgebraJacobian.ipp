/**
 * @file LinearAlgebraJacobian.ipp Implementation of Jacobians for common linear algebra functions
 * @author Evren Imre
 * @date 25 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef LINEAR_ALGEBRA_JACOBIAN_IPP_0541213
#define LINEAR_ALGEBRA_JACOBIAN_IPP_0541213

#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <cstddef>
#include <vector>
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"

namespace SeeSawN
{
namespace NumericN
{

using boost::math::pow;
using Eigen::Matrix;
using std::size_t;
using std::vector;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::ColsM;
using SeeSawN::WrappersN::Reshape;

/**
 * @brief Jacobian of the matrix inverse operation
 * @tparam MatrixAT Input matrix
 * @tparam MatrixJT Jacobian matrix
 * @param[in] mA Input matrix
 * @return Jacobian
 * @pre \c mA is invertible (not enforced)
 * @ingroup Jacobian
 */
template<class MatrixJT, class MatrixAT>
MatrixJT JacobiandAidA(const MatrixAT& mA)
{
	MatrixAT mAiT=mA.inverse();
	mAiT.transposeInPlace();

	size_t sz=mA.rows();
	MatrixJT mJ(sz*sz, sz*sz);
    size_t iResult1=0;
    size_t iResult2=0;
    for(size_t c1=0; c1<sz; ++c1, iResult1+=sz, iResult2=0)
        for(size_t c2=0; c2<sz; ++c2, iResult2+=sz)
        	mJ.block(iResult2, iResult1, sz, sz)=-mAiT(c1, c2)*mAiT;	//Scalar factors come from mAi, not mAiT!

    return mJ;
}	//bool JacobiandAidA(MatrixJT& result, const MatrixAT& mA)

/**
 * @brief Jacobian of division of a matrix by its Frobenius norm
 * @tparam MatrixAT Input matrix
 * @tparam MatrixJT Jacobian matrix
 * @param[in] mA Input matrix
 * @return Jacobian
 * @ingroup Jacobian
 */
template<class MatrixJT, class MatrixAT>
MatrixJT JacobianNormalise(const MatrixAT& mA)
{
	typedef typename ValueTypeM<MatrixAT>::type RealT;

	RealT normA=mA.norm();

	//If the norm is 0, so is the Jacobian. (0/inf->0)
	if(normA==0)
	{
		MatrixJT mZ(mA.size(), mA.size()); mZ.setZero();
		return mZ;
	}

	typedef Matrix<RealT, Eigen::Dynamic, 1> VectorT;
	VectorT vA=Reshape<VectorT>(mA, mA.size(), 1);

	typedef Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> MatrixT;
	MatrixT mI(mA.size(), mA.size()); mI.setIdentity();

	return (pow<2>(normA)*mI - vA*vA.transpose())/pow<3>(normA);
}	//MatrixJT JacobianNormalise(const MatrixAT& mA)

/**
 * @brief Jacobian of the matrix multiplication operation wrt/ first operand
 * @tparam MatrixAT First operand
 * @tparam MatrixAT Second operand
 * @tparam MatrixJT Jacobian matrix
 * @param[in] mA First operand
 * @param[in] mB Second operand
 * @return Jacobian of the product wrt/ \c mA
 * @pre \c mA and \c mB can be multiplied
 * @ingroup Jacobian
 */
template<class MatrixJT, class MatrixAT, class MatrixBT>
MatrixJT JacobiandABdA(const MatrixAT& mA, const MatrixBT& mB)
{
	//Preconditions
	assert(mA.cols()==mB.rows());

	size_t rA=mA.rows();
	size_t cA=mA.cols();
	size_t rB=mB.rows();
	size_t cB=mB.cols();

	MatrixJT mJ(rA*cB, mA.size()); mJ.setZero();
	auto mBt=mB.transpose();

	size_t i1=0;
	size_t i2=0;
	for(size_t c=0; c<cA; ++c, i1+=cB, i2+=rB)
		mJ.block(i1, i2, cB, rB)=mBt;

	return mJ;
}	//MatrixJT JacobiandABdA(const MatrixAT& mA, const MatrixBT& mB)

/**
 * @brief Jacobian of the matrix multiplication operation wrt/ first operand
 * @tparam MatrixAT First operand
 * @tparam MatrixBT Second operand
 * @tparam MatrixJT Jacobian matrix
 * @param[in] mA First operand
 * @param[in] mB Second operand
 * @return Jacobian of the product wrt/ \c mA
 * @pre \c mA and \c mB can be multiplied
 * @ingroup Jacobian
 */
template<class MatrixJT, class MatrixAT, class MatrixBT>
MatrixJT JacobiandABdB(const MatrixAT& mA, const MatrixBT& mB)
{
	//Preconditions
	assert(mA.cols()==mB.rows());

	size_t rA=mA.rows();
	size_t cA=mA.cols();
	size_t cB=mB.cols();

	MatrixJT mJ(rA*cB, mB.size()); mJ.setZero();

	Matrix<typename ValueTypeM<MatrixAT>::type, ColsM<MatrixBT>::value, 1> vI(cB);
	vI.setConstant(1);

	size_t i1=0;
	size_t i2=0;
	for(size_t c1=0; c1<rA; ++c1, i1+=cB, i2=0)
		for(size_t c2=0; c2<cA; ++c2, i2+=cB)
			mJ.block(i1, i2, cB, cB)= (mA(c1,c2)*vI).asDiagonal();

	return mJ;
}	//MatrixJT JacobiandABdB(const MatrixAT& mA, const MatrixBT& mB)

/**
 * @brief Jacobian of the division of a matrix with one of its elements
 * @tparam MatrixAT Matrix type
 * @tparam MatrixJT Jacobian type
 * @param[in] mA Matrix
 * @param[in] iRow Row index
 * @param[in] iCol Column index
 * @return Jacobian
 * @pre \c iRow and \c iCol denote an element within \c mA
 * @ingroup Jacobian
 */
template<class MatrixJT, class MatrixAT>
MatrixJT JacobianAOveraNdA(const MatrixAT& mA, size_t iRow, size_t iCol)
{
	//Preconditions
	assert(iRow<mA.rows());
	assert(iCol<mA.cols());

	typedef typename ValueTypeM<MatrixAT>::type RealT;
	RealT scalar=mA(iRow, iCol);

	// I/s
	size_t nElements=mA.size();
	MatrixJT mJ=MatrixJT::Identity(nElements, nElements);
	mJ.diagonal().setConstant(1.0/scalar);

	//Only for the row belonging to the normalising element, -vector form of the matrix / s^2
	size_t index= iRow*mA.cols() + iCol;
	mJ.col(index)=Reshape< Matrix<RealT,Eigen::Dynamic,1> >(mA,nElements,1)/(-pow<2>(scalar));
	mJ(index, index)=0;	//For the normaliser, 0

	return mJ;
}	//MatrixJT JacobianAOveraN(const MatrixAT& mA, size_t iRow, size_t iCol)

/**
 * @brief Jacobian of the transposition operation
 * @tparam MatrixAT Matrix type
 * @tparam MatrixJT Jacobian type
 * @param[in] mA Matrix
 * @return Jacobian
 * @ingroup Jacobian
 */
template<class MatrixJT, class MatrixAT>
MatrixJT JacobiandAtdA(const MatrixAT& mA)
{
	size_t nRow=mA.rows();
	size_t nCol=mA.cols();
	size_t nElements=mA.size();

	//Just an exchange matrix
	MatrixJT mJ=MatrixJT::Zero(nElements, nElements);
	for(size_t c2=0; c2<nCol; ++c2)
		for(size_t c1=0; c1<nRow; ++c1)
			mJ( c2*nRow+c1, c1*nCol+c2 )=1;

	return mJ;
}	//MatrixJT JacobiandAtdA(const MatrixAT& mA)

/**
 * @brief Jacobian of the quadratic form (x^t A x) wrt x
 * @tparam MatrixJT Type of the Jacobian matrix
 * @tparam SymmetricMatrixT A symmetric matrix
 * @tparam VectorT A vector
 * @param[in] mA Coefficient defining the quadratic form
 * @param[in] x Variables
 * @return Jacobian wrt \c x
 * @pre \c mA is symmetric
 * @pre \c x and \c mA have the same number of columns
 * @ingroup Jacobian
 */
template<class MatrixJT, class SymmetricMatrixT, class VectorT>
MatrixJT JacobiandxtAxdx(const SymmetricMatrixT& mA, const VectorT& x)
{
	//Preconditions
	assert(mA==mA.transpose());
	assert(mA.rows()==x.size());

	return 2*(mA*x).transpose();
}	//MatrixJT JacobiandxtAxdx(const SymmetricMatrixT& mA, const VectorT& x)

/**
 * @brief Jacobian of a matrix with respect to some of its elements
 * @tparam MatrixJT Jacobian matrix
 * @param[in] nCells Number of cells in the matrix
 * @param[in] indexList Index of elements wrt/ which the Jacobian is computed
 * @return Jacobian
 * @pre \c indexList does not have any elements \c >=nElements
 * @ingroup Jacobian
 */
template<class MatrixJT>
MatrixJT JacobiandAda(size_t nCells, const vector<size_t>& indexList)
{
	size_t nD=indexList.size();
	MatrixJT mJ=MatrixJT::Zero(nCells, nD);

	for(size_t c=0; c<nD; ++c)
		mJ(indexList[c],c)=1;

	return mJ;
}	//MatrixJT JacobiandAda(size_t nRow, size_t nCol, const vector<size_t>& indexList)

}	//NumericN
}	//SeeSawN

#endif /* LINEAR_ALGEBRA_JACOBIAN_IPP_0541213 */
