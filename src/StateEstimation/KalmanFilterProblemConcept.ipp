/**
 * @file KalmanFilterProblemConcept.ipp Implementation of \c KalmanFilterProblemConceptC
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KALMAN_FILTER_PROBLEM_CONCEPT_IPP_8921394
#define KALMAN_FILTER_PROBLEM_CONCEPT_IPP_8921394

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <cstddef>

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using std::vector;
using std::size_t;

/**
 * @brief Concept definition and checker for Kalman filter problems
 * @tparam TestT Type to be tested
 * @ingroup Concept
 * @nosubgrouping
 */
template<class TestT>
class KalmanFilterProblemConceptC
{
	///@cond CONCEPT_CHECK
	public:

		BOOST_CONCEPT_USAGE(KalmanFilterProblemConceptC)
		{
			TestT tested;

			bool flagValid=tested.IsValid(); (void)flagValid;

			typedef typename TestT::state_type StateT;
			StateT state;

			typedef typename TestT::state_vector_type StateVectorT;
			optional<StateVectorT> sMeanP=tested.PropagateStateMean(state); (void)sMeanP;

			typedef typename TestT::propagation_jacobian_type MatrixPJT;
			optional<MatrixPJT> mJF=tested.ComputePropagationJacobian(state); (void)mJF;

			typedef typename TestT::state_covariance_type StateCovarianceT;
			StateCovarianceT mCState=tested.GetStateCovariance(state); (void)mCState;

			StateVectorT deltaMean;
			StateCovarianceT deltaCovariance;
			bool flagSuccessU=tested.Update(state, deltaMean, deltaCovariance); (void)flagSuccessU;

			typedef typename TestT::measurement_type MeasurementT;
			optional<MeasurementT> prediction=tested.PredictMeasurement(state);

			MeasurementT observation;

			typedef typename TestT::innovation_covariance_type InnovationCovarianceT;
			InnovationCovarianceT mCMeasurement=tested.GetMeasurementCovariance(observation); (void)mCMeasurement;

			typedef typename TestT::innovation_vector_type InnovationVectorT;
			InnovationVectorT vy=tested.ComputeInnovationVector(observation, *prediction); (void)vy;

			typedef typename TestT::measurement_jacobian_type MatrixMJT;
			optional<MatrixMJT> mJH=tested.ComputeMeasurementFunctionJacobian(state); (void)mJH;
		}	//BOOST_CONCEPT_USAGE(KalmanFilterProblemConceptC)
	///@endcond
};	//class KalmanFilterProblemConceptC

}	//StateEstimationN
}	//SeeSawN

#endif /* KALMAN_FILTER_PROBLEM_CONCEPT_IPP_8921394 */
