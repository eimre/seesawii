/**
 * @file TestStateEstimation.cpp Unit tests for state estimation
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE STATE_ESTIMATION

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/optional.hpp>
#include <boost/range/numeric.hpp>
#include <Eigen/Dense>
#include <list>
#include <vector>
#include <tuple>
#include <cmath>
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Metrics/Distance.h"
#include "../Geometry/Rotation.h"
#include "../Geometry/Rotation3DSolver.h"
#include "KFSimpleProblemBase.h"
#include "KFVectorMeasurementFusionProblem.h"
#include "KFFeatureTrackingProblem.h"
#include "KalmanFilter.h"
#include "UKFCameraTrackingProblemBase.h"
#include "UKFOrientationTrackingProblem.h"
#include "UnscentedKalmanFilter.h"
#include "Viterbi.h"
#include "ViterbiFeatureSequenceMatchingProblem.h"
#include "ViterbiNormalisedHistogramMatchingProblem.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestStateEstimationN
{

using namespace SeeSawN::StateEstimationN;

using boost::optional;
using boost::partial_sum;
using Eigen::MatrixXd;
using Eigen::Vector3f;
using std::list;
using std::vector;
using std::tuple;
using std::get;
using std::exp;
using std::fabs;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::GeometryN::RotationVectorT;
using SeeSawN::GeometryN::Rotation3DMinimalDifferenceC;

BOOST_AUTO_TEST_SUITE(Kalman_Filter_Problems)

BOOST_AUTO_TEST_CASE(KF_Simple_Problem_Base)
{
	typedef KFSimpleProblemBaseC<2, 3, double> ProblemT;

	typedef ProblemT::state_type StateT;

	ProblemT::state_vector_type stateMean; stateMean<<0,2;
	ProblemT::state_covariance_type stateCovariance; stateCovariance.setIdentity(2,2);
	StateT state(stateMean, stateCovariance);

	typedef ProblemT::measurement_type MeasurementT;
	ProblemT::innovation_vector_type measurementMean; measurementMean<<1,3,4;
	ProblemT::innovation_covariance_type measurementCovariance; measurementCovariance.setIdentity(3,3);
	MeasurementT measurement(measurementMean, measurementCovariance);

	BOOST_CHECK(ProblemT::GetStateMean(state)==stateMean);
	BOOST_CHECK(ProblemT::GetStateCovariance(state)==stateCovariance);
	BOOST_CHECK(ProblemT::GetMeasurementCovariance(measurement)==measurementCovariance);

	ProblemT::innovation_vector_type predictionMean; predictionMean<<1,2,1;
	MeasurementT prediction(predictionMean, measurementCovariance);

	ProblemT::innovation_vector_type innovation=ProblemT::ComputeInnovationVector(measurement, prediction);
	BOOST_CHECK(innovation== ProblemT::innovation_vector_type(0,1,3) );

	ProblemT::state_covariance_type deltaCov; deltaCov.setIdentity();
	ProblemT::state_vector_type deltaState; deltaState<<0,1;
	bool flagUpdate=ProblemT::Update(state, deltaState, deltaCov);

	BOOST_CHECK(flagUpdate);
	BOOST_CHECK(ProblemT::GetStateMean(state)==ProblemT::state_vector_type(0,3));
	BOOST_CHECK(ProblemT::GetStateCovariance(state)==2*ProblemT::state_covariance_type::Identity());

	BOOST_CHECK(ProblemT::IsValid());
}	//KF_Simple_Problem_Base

BOOST_AUTO_TEST_CASE(KF_Vector_Measurement_Fusion_Problem)
{
	typedef KFVectorMeasurementFusionProblemC<Coordinate3DT> ProblemT;

	typedef ProblemT::state_type StateT;
	ProblemT::state_vector_type stateMean; stateMean<<0,2,1;
	ProblemT::state_covariance_type stateCovariance = ProblemT::state_covariance_type::Identity();
	StateT state(stateMean, stateCovariance);

	BOOST_CHECK(*ProblemT::PropagateStateMean(state)==stateMean);
	BOOST_CHECK(*ProblemT::ComputePropagationJacobian(state) == ProblemT::propagation_jacobian_type::Identity());

	BOOST_CHECK(ProblemT::PredictMeasurement(state)->GetMean()==stateMean);
	BOOST_CHECK(ProblemT::PredictMeasurement(state)->GetCovariance()==stateCovariance);
	BOOST_CHECK(*ProblemT::ComputeMeasurementFunctionJacobian(state) == ProblemT::measurement_jacobian_type::Identity());
}	//KF_Vector_Measurement_Fusion_Problem

BOOST_AUTO_TEST_CASE(KF_Feature_Tracking_Problem)
{
	typedef KFImageFeatureTrackingProblemC ProblemT;
	typedef ProblemT::state_type StateT;
	ProblemT::state_vector_type stateMean; stateMean<<0,2,0.5,0.25;
	ProblemT::state_covariance_type stateCovariance = ProblemT::state_covariance_type::Identity();
	StateT state(stateMean, stateCovariance);

	typedef ProblemT::state_vector_type StateVectorT;
	StateVectorT propagated; propagated<<0.5,2.25,0.5,0.25;
	BOOST_CHECK(*ProblemT::PropagateStateMean(state)==propagated);

	typedef ProblemT::propagation_jacobian_type PropagationJaocbianT;
	PropagationJaocbianT mJp; mJp<<1,0,1,0,0,1,0,1,0,0,1,0,0,0,0,1;
	BOOST_CHECK(*ProblemT::ComputePropagationJacobian(state) == mJp);

	BOOST_CHECK(ProblemT::PredictMeasurement(state)->GetMean()==stateMean.head(2));
	BOOST_CHECK(ProblemT::PredictMeasurement(state)->GetCovariance()==stateCovariance.topLeftCorner(2,2));

	typedef ProblemT::measurement_jacobian_type MeasurementJaocbianT;
	MeasurementJaocbianT mJm; mJm<<1,0,0,0,0,1,0,0;
	BOOST_CHECK(*ProblemT::ComputeMeasurementFunctionJacobian(state) == mJm);
}	//BOOST_AUTO_TEST_CASE(KF_Feature_Tracking_Problem)

BOOST_AUTO_TEST_SUITE_END()	//Kalman_Filter_Problems

BOOST_AUTO_TEST_SUITE(Kalman_Filter)

BOOST_AUTO_TEST_CASE(Kalman_Filter_Engine)
{
	typedef KFVectorMeasurementFusionProblemC<Coordinate2DT> ProblemT;
	typedef KalmanFilterC<ProblemT> KalmanFilterT;

	ProblemT problem;
	KalmanFilterT engine;

	typedef ProblemT::state_covariance_type StateCovarianceT;
	StateCovarianceT processNoise=0.01*StateCovarianceT::Identity();

	typedef ProblemT::state_type StateT;
	StateT initialState(Coordinate2DT(0,0), StateCovarianceT::Identity());
	engine.SetState(initialState, processNoise);
	BOOST_CHECK(engine.IsValid());

	bool flagPredict=engine.PredictState(problem);
	BOOST_CHECK(flagPredict);
	BOOST_CHECK(engine.GetState().GetMean()==Coordinate2DT(0,0));
	BOOST_CHECK(engine.GetState().GetCovariance()== 1.01*StateCovarianceT::Identity());

	typedef ProblemT::measurement_type MeasurementT;
	typedef ProblemT::innovation_covariance_type MeasurementCovarianceT;
	optional<MeasurementT> predicted=engine.PredictMeasurement(problem);
	BOOST_CHECK(predicted);
	BOOST_CHECK(predicted->GetMean()==engine.GetState().GetMean());
	BOOST_CHECK(predicted->GetCovariance()==engine.GetState().GetCovariance());

	MeasurementT measurement(Coordinate2DT(0.25, 0.25), 2*MeasurementCovarianceT::Identity());
	bool flagUpdate=engine.Update(problem, measurement, *predicted, 0);
	BOOST_CHECK(flagUpdate);
	BOOST_CHECK(Coordinate2DT(0.083887, 0.083887).isApprox(engine.GetState().GetMean(), 1e-4));
	BOOST_CHECK(engine.GetState().GetCovariance().isApprox(0.671096*StateCovarianceT::Identity(), 1e-4));

	bool flagUpdate2=engine.Update(problem, measurement, *predicted, 1);
	BOOST_CHECK(flagUpdate2);
	BOOST_CHECK(Coordinate2DT(0.196897, 0.196897).isApprox(engine.GetState().GetMean(), 1e-4));
	BOOST_CHECK(engine.GetState().GetCovariance().isApprox(0.40159*StateCovarianceT::Identity(), 1e-4));

	StateT currentState=engine.GetState();
	engine.Clear();
	BOOST_CHECK(!engine.IsValid());
	engine.SetState(currentState, processNoise);

	list<MeasurementT> measurements;
	measurements.emplace_back(Coordinate2DT(0.1, -0.1), 2*MeasurementCovarianceT::Identity());
	measurements.emplace_back(Coordinate2DT(-0.2, 0.3), 1.5*MeasurementCovarianceT::Identity());
	bool flagRun=engine.Run(problem, measurements, 0);
	BOOST_CHECK(flagRun);
	BOOST_CHECK(Coordinate2DT(0.108176, 0.175408).isApprox(engine.GetState().GetMean(), 1e-4));
	BOOST_CHECK(engine.GetState().GetCovariance().isApprox(0.284666*StateCovarianceT::Identity(), 1e-4));
}	//Kalman_Filter_Engine

BOOST_AUTO_TEST_SUITE_END()	//Kalman_Filter

BOOST_AUTO_TEST_SUITE(UKF_Problems)

BOOST_AUTO_TEST_CASE(UKF_Orientation_Tracking)
{
	typedef UKFOrientationTrackingProblemC UKFProblemT;

	//State

	typedef UKFProblemT::state_type StateT;
	typedef UKFProblemT::state_vector_type StateVectorT;
	typedef UKFProblemT::state_covariance_type StateCovarianceT;

	StateT state;
	RotationVectorT orientation(0.25, 0.5, 0.1);
	RotationVectorT rotation(0.05, -0.05, 0.01);
	StateVectorT vP; vP<<orientation,rotation;
	state.SetMean(vP);

	StateCovarianceT mP; mP.setIdentity();
	mP(0,0)=1e-4; mP(1,1)=1e-4; mP(2,2)=1e-4;
	mP(3,3)=1e-6; mP(4,4)=1e-6; mP(5,5)=1e-6;
	state.SetCovariance(mP);

	//Measurement

	typedef UKFProblemT::measurement_type MeasurementT;
	typedef UKFProblemT::measurement_vector_type MeasurementVectorT;
	typedef UKFProblemT::measurement_covariance_type MeasurementCovarianceT;

	MeasurementT measurement1(vP.head(3), mP.topLeftCorner(3,3));
	MeasurementT measurement2(vP.tail(3), mP.bottomRightCorner(3,3));

	//UKFCameraTrackingProblemBase

	UKFProblemT problem;
	BOOST_CHECK_EQUAL(problem.StateDimensionality(),6);
	BOOST_CHECK_EQUAL(problem.MeasurementDimensionality(),3);

	BOOST_CHECK(problem.GetStateCovariance(state)==mP);
	BOOST_CHECK(problem.GetMeasurementCovariance(measurement1)==mP.topLeftCorner(3,3));

	Rotation3DMinimalDifferenceC measurementSubtractor;
	BOOST_CHECK(problem.ComputeInnovationVector(measurement1, measurement2) == measurementSubtractor(vP.head(3), vP.tail(3)) );

	//UKFOrientationTrackingProblem

	BOOST_CHECK(problem.IsValid());

	//Propagation
	StateVectorT deltaP; deltaP.setZero();
	StateVectorT deltaQ; deltaQ.setZero();
	optional<StateT> propagated=problem.ApplyPropagationFunction(state, deltaP, deltaQ);

	BOOST_CHECK(propagated);
	BOOST_CHECK(propagated->GetCovariance()==state.GetCovariance());

	StateVectorT propagatedR; propagatedR << 0.293259, 0.449453, 0.128313, 0.05, -0.05, 0.01;
	BOOST_CHECK(propagatedR.isApprox(propagated->GetMean(), 1e-6));

	StateVectorT deltaP1; deltaP1<<0,0.1,0,0,0.01,0;
	StateVectorT deltaQ1; deltaQ1<<0,0,0,0.01,0.01,0.01;
	optional<StateT> propagated1=problem.ApplyPropagationFunction(state, deltaP1, deltaQ1);

	BOOST_CHECK(propagated1);
	BOOST_CHECK(propagated1->GetCovariance()==state.GetCovariance());

	StateVectorT propagatedR1; propagatedR1 << 0.298945, 0.55923, 0.11739, 0.06, -0.04, 0.02;
	BOOST_CHECK(propagatedR1.isApprox(propagated1->GetMean(), 1e-6));

	typedef UKFProblemT::state_sample_type StateSigmaT;
	vector<StateSigmaT> stateSigmaSet(3); stateSigmaSet[0]=vP; stateSigmaSet[1]=propagatedR; stateSigmaSet[2]=propagatedR1;
	optional<StateT> predicted=problem.ComputeState(stateSigmaSet, 0.5, 0.5, 0.25);
	BOOST_CHECK(predicted);

	StateVectorT predictedR; predictedR<<0.273075, 0.502195, 0.111441, 0.0525, -0.0475, 0.0125;
	BOOST_CHECK(predicted->GetMean().isApprox(predictedR, 1e-6));

	StateVectorT mPyyRow2r; mPyyRow2r<<5.01716e-05,  0.00151676, 6.69984e-05, 0.000143647, 0.000143647, 0.000143647;
	BOOST_CHECK(predicted->GetCovariance().row(1).isApprox(mPyyRow2r.transpose(), 1e-4));

	//Measurement

	typedef UKFProblemT::measurement_sample_type MeasurementSigmaT;

	StateVectorT deltaP2; deltaP2.setZero();
	MeasurementVectorT deltaR; deltaR.setConstant(0.01);
	optional<tuple<StateSigmaT, MeasurementSigmaT> > predictedSigma=problem.ApplyMeasurementFunction(state, deltaP, deltaR);

	MeasurementSigmaT measurementSigmaR(0.257907, 0.510831, 0.111056);
	BOOST_CHECK(get<0>(*predictedSigma).isApprox(vP, 1e-6) );
	BOOST_CHECK(get<1>(*predictedSigma).isApprox(measurementSigmaR, 1e-6));

	vector<MeasurementSigmaT> measurementSigmaSet(3);
	measurementSigmaSet[0]=vP.head(3); measurementSigmaSet[1]=vP.tail(3); measurementSigmaSet[2]=measurementSigmaR;
	optional<MeasurementT> measurement3=problem.ComputeMeasurement(measurementSigmaSet, 0.5, 0.5, 0.25);

	BOOST_CHECK(measurement3);

	MeasurementVectorT measurement3r(0.203459, 0.368222, 0.0808748);
	MeasurementCovarianceT mR3r;
	mR3r<< 0.00744022,  0.0205854, 0.00423051, 0.0205854,   0.056998,  0.0116913, 0.00423051,  0.0116913, 0.00240971;

	BOOST_CHECK(measurement3r.isApprox(measurement3->GetMean(), 1e-6));
	BOOST_CHECK(mR3r.isApprox(measurement3->GetCovariance(), 1e-6));

	//Cross-covariance

	typedef UKFProblemT::cross_covariance_type CrossCovarianceT;
	optional<CrossCovarianceT> mPxy=problem.ComputeCrossCovariance(state, stateSigmaSet, *measurement3, measurementSigmaSet, 0.5, 0.25);
	BOOST_CHECK(mPxy);

	CrossCovarianceT mPxyr;
	mPxyr<< -0.0012008,  -0.00345901, -0.000639853, 0.00270312,   0.00732735,    0.0015846, -0.000204468, -0.000599895, -0.000105526, 0.000137297,  0.000353528,  8.63405e-05, 0.000137297,  0.000353528,  8.63405e-05, 0.000137297,  0.000353528,  8.63405e-05;

	BOOST_CHECK(mPxy->isApprox(mPxyr,1e-6));

	//Update
	bool flagSuccess=problem.Update(state, deltaP1, mP);
	BOOST_CHECK(flagSuccess);
	BOOST_CHECK(state.GetMean().isApprox(vP+deltaP1, 1e-6));

	BOOST_CHECK(state.GetCovariance()==2*mP);
}	//BOOST_AUTO_TEST_CASE(UKF_Orientation_Tracking)

BOOST_AUTO_TEST_SUITE_END()	//UKF_Problems

BOOST_AUTO_TEST_SUITE(Unscented_Kalman_Filter)

BOOST_AUTO_TEST_CASE(UKF_Engine)
{
	typedef UKFOrientationTrackingProblemC ProblemT;
	ProblemT problem;
	UnscentedKalmanFilterParametersC parameters;

	//Default constructor
	typedef UnscentedKalmanFilterC<ProblemT> UKFT;
	UKFT engine1;
	BOOST_CHECK(!engine1.IsValid());

	//Constructor
	UKFT engine2(parameters);
	BOOST_CHECK(!engine2.IsValid());	//Still invalid

	//Initialisation and access
	typedef typename ProblemT::state_type StateT;
	typedef typename ProblemT::state_vector_type StateVectorT;
	typedef typename ProblemT::state_covariance_type StateCovarianceT;

	StateT state;
	RotationVectorT orientation(0.25, 0.5, 0.1);
	RotationVectorT rotation(0.05, -0.05, 0.01);
	StateVectorT vP; vP<<orientation,rotation;
	state.SetMean(vP);

	StateCovarianceT mP; mP.setIdentity();
	mP(0,0)=1e-4; mP(1,1)=1e-4; mP(2,2)=1e-4;
	mP(3,3)=1e-6; mP(4,4)=1e-6; mP(5,5)=1e-6;
	state.SetCovariance(mP);

	StateCovarianceT mQ; mQ.setZero(); mQ.bottomRightCorner(3,3)=mP.bottomRightCorner(3,3);

	engine2.SetState(problem, StateT(vP, mP), mQ);

	BOOST_CHECK(engine2.IsValid());
	BOOST_CHECK(engine2.GetState().GetMean()==vP);
	BOOST_CHECK(engine2.GetState().GetCovariance()==mP);

	engine2.Clear();
	BOOST_CHECK(!engine2.IsValid());

	engine2.SetState(problem, StateT(vP, mP), mQ);

	//UKF operation

	UKFT engine3(engine2);

	//Prediction
	typedef ProblemT::measurement_type MeasurementT;
	optional<MeasurementT> prediction;
	bool flagPredict=engine2.Predict(prediction, problem, true);
	BOOST_CHECK(flagPredict);
	BOOST_CHECK(prediction);

	MeasurementT::mean_type predictedMeanR; predictedMeanR<<0.293263, 0.449448, 0.128316;
	MeasurementT::covariance_type predictedCovR;
	predictedCovR<<0.00040074, -2.50221e-07,  5.00493e-08,-2.50221e-07,   0.00040074, -5.00388e-08, 5.00493e-08, -5.00388e-08, 0.0004005;

	BOOST_CHECK(prediction->GetMean().isApprox(predictedMeanR, 1e-6));
	BOOST_CHECK(prediction->GetCovariance().isApprox(predictedCovR, 1e-6));

	bool flagPredict2=engine3.Predict(prediction, problem, false);
	BOOST_CHECK(flagPredict2);
	BOOST_CHECK(!prediction);

	StateVectorT vPredicted2r; vPredicted2r<<0.293263, 0.449448, 0.128316, 0.05, -0.05, 0.01;
	StateT predicted2=engine3.GetState();
	BOOST_CHECK(predicted2.GetMean().isApprox(vPredicted2r, 1e-6));
	BOOST_CHECK(predicted2.ComputeMahalonobisDistance(vP));
	BOOST_CHECK_CLOSE(*predicted2.ComputeMahalonobisDistance(vP), 12.73662, 1e-4);

	ProblemT::measurement_covariance_type mR=10*mP.topLeftCorner(3,3);
	bool flagPredict3=engine3.Predict(prediction, problem, true, mR);
	BOOST_CHECK(flagPredict3);
	BOOST_CHECK(prediction);

	MeasurementT::mean_type predictedMean3r; predictedMean3r<< 0.33624, 0.398551, 0.156499;
	BOOST_CHECK(predictedMean3r.isApprox(prediction->GetMean(), 1e-6));
	BOOST_CHECK(prediction->ComputeMahalonobisDistance(vP.head(3)));
	BOOST_CHECK_CLOSE(*prediction->ComputeMahalonobisDistance(vP.head(3)), 7.819986, 1e-4);

	//Update
	MeasurementT observation(vP.head(3), mR);
	bool flagUpdate=engine2.Update(problem, observation);

	BOOST_CHECK(flagUpdate);

	StateVectorT updatedMeanR; updatedMeanR<<0.27895,   0.463761,   0.125453,  0.0499643, -0.0499643, 0.00999286;
	BOOST_CHECK(updatedMeanR.isApprox(engine2.GetState().GetMean(), 1e-5));

	StateT updated=engine2.GetState();
	BOOST_CHECK(updated.ComputeMahalonobisDistance(vP));
	BOOST_CHECK_CLOSE(*updated.ComputeMahalonobisDistance(vP), 9.5564178, 1e-6);

	//Bulk update
	bool flagRun=engine2.Run(problem, vector<MeasurementT>(2, observation));

	BOOST_CHECK(flagRun);

	StateVectorT updatedMean2R; updatedMean2R<<0.260012, 0.471792, 0.14373, 0.0496577, -0.0496448, 0.00988233;
	BOOST_CHECK(updatedMean2R.isApprox(engine2.GetState().GetMean(), 1e-5));

	StateT updated2=engine2.GetState();
	BOOST_CHECK(updated2.ComputeMahalonobisDistance(vP));
	BOOST_CHECK_CLOSE(*updated2.ComputeMahalonobisDistance(vP), 4.11654367, 1e-6);
}	//BOOST_AUTO_TEST_CASE(UKF_Engine)

BOOST_AUTO_TEST_SUITE_END()	//Unscented_Kalman_Filter

BOOST_AUTO_TEST_SUITE(Viterbi_Problems)

BOOST_AUTO_TEST_CASE(Viterbi_Feature_Sequence_Matching_Problem)
{
	vector<ImageFeatureC> reference(3);
	reference[0].Descriptor()=Vector3f(0.25, 0.15, 0.4).normalized();
	reference[1].Descriptor()=Vector3f(0.2, 0.4, 0.3).normalized();
	reference[2].Descriptor()=Vector3f(0.6, 0.1, 0.2).normalized();

	typedef ViterbiFeatureSequenceMatchingProblemC<ImageFeatureC, EuclideanDistanceIDT> ProblemT;

	ProblemT problem;
	BOOST_CHECK(!problem.IsValid());

	EuclideanDistanceIDT distanceMetric;
	ProblemT problem2(reference, distanceMetric, 1, 1e-5);
	BOOST_CHECK(problem2.IsValid());

	BOOST_CHECK_EQUAL(problem2.GetStateCardinality(),3);
	BOOST_CHECK_EQUAL(problem2.ComputeTransitionProbability(0,1), 1e-5);

	ImageFeatureC measurement; measurement.Descriptor()=Vector3f(0.1, 0.4, 0.2).normalized();

	BOOST_CHECK_CLOSE(problem2.ComputeMeasurementProbability(measurement,0), exp(-distanceMetric(reference[0].Descriptor(), measurement.Descriptor())), 1e-5);
	BOOST_CHECK_CLOSE(problem2.ComputeMeasurementProbability(measurement,2), exp(-distanceMetric(reference[2].Descriptor(), measurement.Descriptor())), 1e-5);

	problem.InitialiseUpdate();
	problem.FinaliseUpdate();
}	//BOOST_AUTO_TEST_CASE(Viterbi_Feature_Sequence_Matching_Problem)

BOOST_AUTO_TEST_CASE(Viterbi_Normalised_Histogram_Matching_Problem)
{
	vector<double> hLeft{  0.0044330, 0.0540056, 0.2420362, 0.3990503, 0.2420362, 0.0540056, 0.0044330};
	vector<double> hRight{ 0.0022182, 0.0087731, 0.0270232, 0.0648252, 0.1211094, 0.1762131, 0.1996756, 0.1762131, 0.1211094, 0.0648252, 0.0270232, 0.0087731, 0.0022182};

	vector<double> cRight(13);
	partial_sum(hRight, cRight.begin());

	//Constructor
	ViterbiNormalisedHistogramMatchingProblemC problem1;
	BOOST_CHECK(!problem1.IsValid());

	ViterbiNormalisedHistogramMatchingProblemC problem2(hLeft, 0.2, 0.05);
	BOOST_CHECK(problem2.IsValid());

	BOOST_CHECK_EQUAL(problem2.GetStateCardinality(), 7);
	BOOST_CHECK_EQUAL(problem2.GetMinMeasurementProbability(), exp(-1));

	BOOST_CHECK_EQUAL(problem2.ComputeTransitionProbability(0,1), 1);
	BOOST_CHECK_EQUAL(problem2.ComputeTransitionProbability(1,0), 0);

	typedef ViterbiNormalisedHistogramMatchingProblemC::measurement_type MeasurementT;
	BOOST_CHECK_EQUAL(problem2.ComputeMeasurementProbability(MeasurementT(hRight[4], cRight[4]), 2), exp(-fabs(hLeft[2]-hRight[4])));
	BOOST_CHECK_EQUAL(problem2.ComputeMeasurementProbability(MeasurementT(hRight[2], cRight[2]), 2), problem2.GetMinMeasurementProbability());
	BOOST_CHECK_EQUAL(problem2.ComputeMeasurementProbability(MeasurementT(hRight[0], cRight[0]), 2), problem2.GetMinMeasurementProbability());
	BOOST_CHECK_EQUAL(problem2.ComputeMeasurementProbability(MeasurementT(hRight[1], cRight[1]), 0), problem2.GetMinMeasurementProbability());
}	//BOOST_AUTO_TEST_CASE(Viterbi_Normalised_Histogram_Matching_Problem)

BOOST_AUTO_TEST_SUITE_END()	//Viterbi_Problems

BOOST_AUTO_TEST_SUITE(Viterbi)

BOOST_AUTO_TEST_CASE(Viterbi_Operation)
{
	vector<ImageFeatureC> reference(3);
	reference[0].Descriptor()=Vector3f(0.25, 0.15, 0.4).normalized();
	reference[1].Descriptor()=Vector3f(0.2, 0.4, 0.3).normalized();
	reference[2].Descriptor()=Vector3f(0.6, 0.1, 0.2).normalized();

	typedef ViterbiFeatureSequenceMatchingProblemC<ImageFeatureC, EuclideanDistanceIDT> ProblemT;

	EuclideanDistanceIDT distanceMetric;
	ProblemT problem(reference, distanceMetric, 1, 1e-5);

	//Constructor

	typedef ViterbiC<ProblemT> ViterbiT;
	ViterbiT engine;

	ViterbiParametersC parameters;
	ViterbiT engine2(parameters);

	//Self-matching
	for(const auto& current : reference)
		BOOST_CHECK(engine2.Update(problem, current));

	list<unsigned int> result2=engine2.GetOptimalSequence();
	BOOST_CHECK_EQUAL(result2.size(), 3);

	list<unsigned int> result2r{0,1,2};
	BOOST_CHECK_EQUAL_COLLECTIONS(result2.begin(), result2.end(), result2r.begin(), result2r.end());

	//Clear
	engine2.Clear();
	BOOST_CHECK(engine2.GetOptimalSequence().empty());

	//Feed new measurements
	ImageFeatureC measurement; measurement.Descriptor()=Vector3f(0.1, 0.4, 0.2).normalized();
	BOOST_CHECK(engine2.Update(problem, measurement));

	ImageFeatureC measurement2; measurement2.Descriptor()=Vector3f(0.6, 0.4, 0.1).normalized();
	BOOST_CHECK(engine2.Update(problem, measurement2));

	list<unsigned int> result3=engine2.GetOptimalSequence();
	BOOST_CHECK_EQUAL(result3.size(), 2);

	list<unsigned int> result3r{1,2};
	BOOST_CHECK_EQUAL_COLLECTIONS(result3.begin(), result3.end(), result3r.begin(), result3r.end());
}	//BOOST_AUTO_TEST_CASE(Viterbi_Operation)

BOOST_AUTO_TEST_SUITE_END()	//Viterbi


}	//TestStateEstimationN
}	//UnitTestsN
}	//SeeSawN

