/**
 * @file KFFeatureTrackingProblem.h Public interface for \c KFFeatureTrackingProblemC
 * @author Evren Imre
 * @date 12 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KF_FEATURE_TRACKING_PROBLEM_H_5801203
#define KF_FEATURE_TRACKING_PROBLEM_H_5801203

#include "KFFeatureTrackingProblem.ipp"
#include "../Wrappers/EigenMetafunction.h"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace StateEstimationN
{

template<unsigned int DIMM, typename RealT> class KFFeatureTrackingProblemC;	///< Kalman filter problem for feature tracking

/********** EXTERN TEMPLATES **********/

using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::ValueTypeM;
extern template class KFFeatureTrackingProblemC<2, ValueTypeM<Coordinate2DT>::type>;
typedef KFFeatureTrackingProblemC<2, ValueTypeM<Coordinate2DT>::type> KFImageFeatureTrackingProblemC;


}	//StateEstimationN
}	//SeeSawN

#endif /* KF_FEATURE_TRACKING_PROBLEM_H_5801203 */
