/**
 * @file KFSimpleProblemBase.ipp Implementation of \c KFSimpleProblemBaseC
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KF_SIMPLE_PROBLEM_BASE_IPP_5802391
#define KF_SIMPLE_PROBLEM_BASE_IPP_5802391

#include <Eigen/Dense>
#include <type_traits>
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using Eigen::Matrix;
using std::is_floating_point;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::NumericN::MatrixDifferenceC;

/**
 * @brief Base for Kalman filter problems conforming to the generic formulation
 * @tparam DIMS State dimensionality. If unknown, set Eigen::Dynamic
 * @tparam DIMM Measurement dimensionality, If unknown, set Eigen::Dynamic
 * @tparam RealT Floating point type
 * @pre \c RealT is a floating point type
 * @remarks This class provides most of the problem interface for vector-valued functions (as opposed to state objects with components of multiple types)
 * @ingroup Problem
 * @nosubgrouping
 */
template<int DIMS, int DIMM, typename RealT>
class KFSimpleProblemBaseC
{
	///@cond CONCEPT_CHECK
	static_assert(is_floating_point<RealT>::value, "KFSimpleProblemBaseC: RealT must be a floating-point value");
	///@endcond

	private:

		typedef Matrix<RealT, DIMS, 1> StateVectorT;	///< State vector type
		typedef Matrix<RealT, DIMM, 1> MeasurementVectorT;	///< Measurement vector type

		typedef MultivariateGaussianC<MatrixDifferenceC<StateVectorT>, DIMS, RealT> StateT;	///< State type
		typedef typename StateT::covariance_type StateCovarianceT;	///< Type of the state covariance

		typedef MultivariateGaussianC<MatrixDifferenceC<MeasurementVectorT>, DIMM, RealT> MeasurementT;	///< Measurement type
		typedef typename MeasurementT::covariance_type MeasurementCovarianceT;	///< Type of the measurement covariance
	public:

		/** @name KalmanFilterProblemConceptC interface */ //@{
		typedef StateT state_type;	///< State type
		typedef StateVectorT state_vector_type;	///< Type of the state vector
		typedef StateCovarianceT state_covariance_type;	///< Type of the state covariance

		typedef MeasurementT measurement_type;	///< Measurement type

		typedef Matrix<RealT, DIMS, DIMS> propagation_jacobian_type;	///< Type of the Jacobian matrix for the propagation funciton
		typedef Matrix<RealT, DIMM, DIMS> measurement_jacobian_type;	///< Type of the Jacobian matrix for the measurement funciton

		typedef MeasurementVectorT innovation_vector_type;	///< Type of the innovation vector
		typedef MeasurementCovarianceT innovation_covariance_type;	///< Type of the innovation covariance matrix

		static StateVectorT GetStateMean(const StateT& state);	///< Returns the state mean
		static StateCovarianceT GetStateCovariance(const StateT& state);	///< Returns the state covariance

		static MeasurementCovarianceT GetMeasurementCovariance(const MeasurementT& measurement);	///< Returns the measurement covariance matrix
		static MeasurementVectorT ComputeInnovationVector(const MeasurementT& observation, const MeasurementT& prediction);	///< Computes the innovation

		static bool Update(StateT& state, const StateVectorT& deltaMean, const StateCovarianceT& deltaCovariance);	///< Applies an update to the state

		static bool IsValid();	///< Reports the validity of the object. Always \c true
		//@}
};	//class KFSimpleProblemBaseC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Returns the state mean
 * @param[in] state State
 * @return State covariance
 */
template<int DIMS, int DIMM, typename RealT>
auto KFSimpleProblemBaseC<DIMS, DIMM, RealT>::GetStateMean(const StateT& state) -> StateVectorT
{
	return state.GetMean();
}	//state_vector_type GetStateMean(const state_type& state)

/**
 * @brief Returns the state covariance
 * @param[in] state State
 * @return State mean
 */
template<int DIMS, int DIMM, typename RealT>
auto KFSimpleProblemBaseC<DIMS, DIMM, RealT>::GetStateCovariance(const StateT& state) -> StateCovarianceT
{
	return state.GetCovariance();
}	//state_covariance_type GetStateCovariance(const state_type& state)

/**
 * @brief Returns the measurement covariance matrix
 * @param[in] measurement Measurement
 * @return Measurement covariance
 */
template<int DIMS, int DIMM, typename RealT>
auto KFSimpleProblemBaseC<DIMS, DIMM, RealT>::GetMeasurementCovariance(const MeasurementT& measurement) -> MeasurementCovarianceT
{
	return measurement.GetCovariance();
}	//innovation_covariance_type GetMeasurementCovariance(const measurement_type& measurement)

/**
 * @brief Applies an update to the state
 * @param[in] state State
 * @param[in] deltaMean Update term to the mean
 * @param[in] deltaCovariance Update term to the covariance
 * @return \c true
 */
template<int DIMS, int DIMM, typename RealT>
bool KFSimpleProblemBaseC<DIMS, DIMM, RealT>::Update(StateT& state, const StateVectorT& deltaMean, const StateCovarianceT& deltaCovariance)
{
	state.SetMean(state.GetMean()+deltaMean);
	state.SetCovariance(state.GetCovariance()+deltaCovariance);

	return true;
}	//bool Update(GaussianT& state, const VectorT& deltaMean, const CovarianceT& deltaCovariance)

/**
 * @brief Computes the innovation
 * @param[in] observation Observation
 * @param[in] prediction Prediction
 * @return Innovation vector
 */
template<int DIMS, int DIMM, typename RealT>
auto KFSimpleProblemBaseC<DIMS, DIMM, RealT>::ComputeInnovationVector(const MeasurementT& observation, const MeasurementT& prediction) -> MeasurementVectorT
{
	return observation.GetMean()-prediction.GetMean();
}	//VectorT ComputeInnovationVector(const GaussianT& observation, const GaussianT& prediction)

/**
 * @brief Reports the validity of the object. Always \c true
 * @return \c true
 */
template<int DIMS, int DIMM, typename RealT>
bool KFSimpleProblemBaseC<DIMS, DIMM, RealT>::IsValid()
{
	return true;
}	//bool IsValid()


}	//StateEstimationN
}	//SeeSawN

#endif /* KF_SIMPLE_PROBLEM_BASE_IPP_5802391 */
