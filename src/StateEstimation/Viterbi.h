/**
 * @file Viterbi.h Public interface for the Viterbi algorithm
 * @author Evren Imre
 * @date 30 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_H_8034201
#define VITERBI_H_8034201

#include "Viterbi.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{

struct ViterbiParametersC;	///< Parameters for the Viterbi algorithm
template<class ProblemT> class ViterbiC;	///< Viterbi algorithm

}	//StateEstimationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<double>;
extern template class std::list<unsigned int>;
#endif /* VITERBI_H_8034201 */
