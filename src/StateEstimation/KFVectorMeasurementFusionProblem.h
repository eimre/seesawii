/**
 * @file KFVectorMeasurementFusionProblem.h Public interface for \c KFVectorFusionMeasurementProblemC
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KF_VECTOR_MEASUREMENT_FUSION_PROBLEM_H_4901432
#define KF_VECTOR_MEASUREMENT_FUSION_PROBLEM_H_4901432

#include "KFVectorMeasurementFusionProblem.ipp"
#include "../Elements/Coordinate.h"
namespace SeeSawN
{
namespace StateEstimationN
{

template<class VectorT> class KFVectorMeasurementFusionProblemC;	///< Kalman filter problem for fusing a set of vector measurements

/********** EXTERN TEMPLATES **********/
using SeeSawN::ElementsN::Coordinate3DT;
extern template class KFVectorMeasurementFusionProblemC<Coordinate3DT>;

}	//StateEstimationN
}	//SeeSawN


#endif /* KF_VECTOR_MEASUREMENT_FUSION_PROBLEM_H_4901432 */
