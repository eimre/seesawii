/**
 * @file UnscentedKalmanFilter.ipp Implementation of UKF
 * @author Evren Imre
 * @date 9 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UNSCENTED_KALMAN_FILTER_IPP_9601231
#define UNSCENTED_KALMAN_FILTER_IPP_9601231

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <stdexcept>
#include <cstddef>
#include <tuple>
#include <cmath>
#include <utility>
#include <vector>
#include <type_traits>
#include <string>
#include "../Numeric/LinearAlgebra.h"
#include "UnscentedKalmanFilterProblemConcept.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using boost::ForwardRangeConcept;
using boost::range_value;
using boost::lexical_cast;
using boost::math::pow;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::size_t;
using std::invalid_argument;
using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;
using std::min;
using std::max;
using std::sqrt;
using std::fma;
using std::true_type;
using std::false_type;
using std::vector;
using std::is_same;
using std::string;
using SeeSawN::NumericN::ComputeSquareRootSVD;
using SeeSawN::StateEstimationN::UnscentedKalmanFilterProblemConceptC;

/**
 * @brief Parameters for \c UnscentedKalmanFilterC
 * @ingroup Parameters
 */
struct UnscentedKalmanFilterParametersC
{
	unsigned int nThreads;	///< Number of threads
	double alpha;	///< Scale factor that determines the sample radius. (0, 1] is recommended, small is better, especially when the distribution is not Gaussian. However, too small alpha may result in an ill-conditioned covariance matrix.
	double kappa;	///< Scale factor that pulls in, or pushes out the samples. Similar to \c alpha, but nonzero values introduces errors in the higher order moments. Negative values can lead to non-positive definite covariance matrices, so, >=0
	double beta;	///< Adjusts the weight of the input mean in the computation of the covariance. Can be used to incorporate any prior knowledge of higher order moments. 2, for a Gaussian distribution

	UnscentedKalmanFilterParametersC() : nThreads(1), alpha(1), kappa(0), beta(2)
	{}

};	//struct ScaledUnscentedTransformationParametersC

/**
 * @brief A generic implementation of UKF
 * @tparam ProblemT A UKF problem
 * @pre \c ProblemT is a model of \c UnscentedKalmanFilterProblemConceptC
 * @remarks Usage notes
 * 	- Batch operation, when all measurements are available: SetState, then Run
 * 	- Sequential operation, when measurements become available sequentially, after each update
 * 		- Initialisation: SetState
 * 		- Time update: Advance the state, and generate a measurement prediction
 * 		- Measurement update: Update the state with the measurement
 * 	- Adding or deleting elements from the state: Modify the state as necessary externally, and then SetState
 * 	- It is up to the problem to convert the current state vector to the native problem representation, and back
 * @remarks R. m. d. Merwe, A. Doucet, N. d. Freitas, E. Wan, "The Unscented Particle Filter, " Cambridge Universtiy Engineering Department, Technical Report, CUED/F-INFENG/TR 380, Aug. 2000
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class UnscentedKalmanFilterC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((UnscentedKalmanFilterProblemConceptC<ProblemT>));
	///@endcond

	private:

		typedef typename ProblemT::state_type StateT;	///< Type of the state
		typedef typename ProblemT::state_vector_type StateVectorT;	///< Type of the state vector
		typedef typename ProblemT::state_covariance_type StateCovarianceT;	///< Type of the state covariance
		typedef typename ProblemT::measurement_type MeasurementT;	///< Type of the measurement
		typedef typename ProblemT::measurement_vector_type MeasurementVectorT;	///< Type of the measurement vector
		typedef typename ProblemT::measurement_covariance_type MeasurementCovarianceT;	///< Type of the measurement covariance
		typedef typename ProblemT::cross_covariance_type CrossCovarianceT;	///< Type of the cross-covariance matrix

		typedef typename ProblemT::state_sample_type StateSampleT;	///< A state sample
		typedef typename ProblemT::measurement_sample_type MeasurementSampleT;	///< A measurement sample

		/** @name State */ //@{
		StateT state;	///< State
		//@}

		/**@name Configuration */ //@{
		bool flagValid;	///< \c true if the UKF engine is initialised
		optional<StateCovarianceT> mQsq;	///< Square-root of the process noise
		UnscentedKalmanFilterParametersC parameters;	///< Parameters
		//@}

		/** @name Implementation details */ //@{
		void ValidateParameters(const UnscentedKalmanFilterParametersC& parameters);	///< Validates the parameters

		StateCovarianceT DecomposeStateCovariance(const ProblemT& problem, true_type dummy) const;	///< Delegates the decomposition of the state covariance to the problem
		StateCovarianceT DecomposeProcessCovariance(const ProblemT& problem, const StateCovarianceT& mQ, true_type dummy) const;	///< Delegates the decomposition of the process noise covariance to the problem
		MeasurementCovarianceT DecomposeMeasurementCovariance(const ProblemT& problem, const MeasurementCovarianceT& mR, true_type dummy) const;	///< Delegates the decomposition of the measurement covariance to the problem

		template<class CovarianceT> static CovarianceT DecomposeCovariance(const CovarianceT& mC);	///< Decomposes a covariance matrix
		StateCovarianceT DecomposeStateCovariance(const ProblemT& problem, false_type dummy) const;	///< Decomposes the state covariance matrix
		StateCovarianceT DecomposeProcessCovariance(const ProblemT& problem, const StateCovarianceT& mQ, false_type dummy) const;	///< Decomposes the process noise covariance matrix
		MeasurementCovarianceT DecomposeMeasurementCovariance(const ProblemT& problem, const MeasurementCovarianceT& mR, false_type dummy) const;	///< Decomposes the measurement covariance matrix

		optional<tuple<StateSampleT, MeasurementSampleT> > ApplyTransformation(const ProblemT& problem, const VectorXd& perturbationP, const VectorXd& perturbationQ, const VectorXd& perturbationR, bool flagPropagate) const;	///< Applies the update functions to a sample
		tuple<double, double, double> ComputeWeights(const ProblemT& problem) const;	///< Computes the sample weights for SUT
		tuple<vector<StateSampleT>, vector<MeasurementSampleT> > ApplySUT(const ProblemT& problem, const MeasurementCovarianceT& mR, bool flagPropagate) const;	///< Applies SUT to the augmented state
		//@}

	public:

		typedef StateT state_type;	///< Type of the state
		typedef MeasurementT measurement_type;	///< Type of the measurements

		/** @name Constructors */ //@{
		UnscentedKalmanFilterC();	///< Default constructor
		UnscentedKalmanFilterC(const UnscentedKalmanFilterParametersC& pparameters);	///< Constructor
		//@}

		/** @name Operations */ //@{
		bool Predict(optional<MeasurementT>& predictedMeasurement, const ProblemT& problem, bool flagMeasurement, const optional<MeasurementCovarianceT>& mR=optional<MeasurementCovarianceT>() );	///< Advances the state of the filter, and generates a measurement prediction
		bool Update(const ProblemT& problem, const MeasurementT& observed);	///< Updates the state

		template<class MeasurementRangeT> bool Run(const ProblemT& problem, const MeasurementRangeT& measurements);	///< Batch operation
		//@}

		/** @name Utilities */ //@{
		void SetState(const ProblemT& problem, const StateT& sstate, const optional<StateCovarianceT>& mQ);	///< Sets the state
		const StateT& GetState() const;	///< Returns a constant reference to the state

		bool IsValid();	///< Returns \c flagValid
		void Clear();	///< Clears the state
		//@}
};	//class UnscentedKalmanFilter

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @param[in] parameters Parameters
 * @throws invalid_argument If \c parameters has invalid values
 */
template<class ProblemT>
void UnscentedKalmanFilterC<ProblemT>::ValidateParameters(const UnscentedKalmanFilterParametersC& parameters)
{
	if(parameters.beta<0)
		throw(invalid_argument(string("UnscentedKalmanFilterC::ValidateParameters : Beta must be a non-negative number. Value:") + lexical_cast<string>(parameters.beta)));

	if(parameters.alpha<=0 )
		throw(invalid_argument(string("UnscentedKalmanFilterC::ValidateParameters : Alpha must be a positive number. Value:") + lexical_cast<string>(parameters.alpha)));

	if(parameters.kappa<0 )
		throw(invalid_argument(string("UnscentedKalmanFilterC::ValidateParameters : Kappa must be a non-negative number. Value:") + lexical_cast<string>(parameters.kappa)));
}	//void ValidateParameters(const UnscentedKalmanFilterParametersC& parameters)

/**
 * @brief Delegates the decomposition of the state covariance to the problem
 * @param[in] problem Problem
 * @param[in] dummy Dummy variable for overload resolution
 * @return Left factor of the matrix square-root
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::DecomposeStateCovariance(const ProblemT& problem, true_type dummy) const ->StateCovarianceT
{
	assert(flagValid);
	return problem.DecomposeStateCovariance(problem.GetStateCovariance(state));
}	//StateCovarianceT DecomposeStateCovariance(const ProblemT& problem, true_type dummy) const

/**
 * @brief Delegates the decomposition of the process noise covariance to the problem
 * @param[in] problem Problem
 * @param[in] mQ Process noise covariance
 * @param[in] dummy Dummy variable for overload resolution
 * @return Left factor of the matrix square-root
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::DecomposeProcessCovariance(const ProblemT& problem, const StateCovarianceT& mQ, true_type dummy) const -> StateCovarianceT
{
	assert(flagValid);
	return problem.DecomposeProcessCovariance(mQ);
}	//StateCovarianceT DecomposeProcessCovariance(const ProblemT& problem, true_type dummy) const

/**
 * @brief Decomposes a covariance matrix
 * @param[in] problem Problem
 * @param[in] mR Measurement covariance matrix
 * @param[in] dummy Dummy variable for overload resolution
 * @return Left factor of the matrix square-root
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::DecomposeMeasurementCovariance(const ProblemT& problem, const MeasurementCovarianceT& mR, true_type dummy) const -> MeasurementCovarianceT
{
	assert(flagValid);
	return problem.DecomposeMeasurementCovariance(mR);
}	//MeasurementCovarianceT DecomposeMeasurementCovariance(const ProblemT& problem, const MeasurementCovarianceT& mR, true_type dummy) const

/**
 * @brief Decomposes a covariance matrix
 * @tparam CovarianceT Covariance matrix
 * @param[in] mC Covariance matrix
 * @return Left factor of the matrix square root
 */
template<class ProblemT>
template<class CovarianceT>
CovarianceT UnscentedKalmanFilterC<ProblemT>::DecomposeCovariance(const CovarianceT& mC)
{
	return ComputeSquareRootSVD<Eigen::FullPivHouseholderQRPreconditioner>(mC);
}	//CovarianceT DecomposeCovariance(const CovarianceT& mC)

/**
 * @brief Decomposes the state covariance matrix
 * @param[in] problem Problem
 * @param[in] dummy Dummy parameter for overload resolution
 * @return Left factor of the matrix square root
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::DecomposeStateCovariance(const ProblemT& problem, false_type dummy) const -> StateCovarianceT
{
	assert(flagValid);
	return DecomposeCovariance(problem.GetStateCovariance(state));
}	//StateCovarianceT DecomposeStateCovariance(const ProblemT& problem, false_type dummy) const

/**
 * @brief Decomposes the process noise covariance matrix
 * @param[in] problem Problem
 * @param[in] mQ Process noise covariance
 * @param[in] dummy Dummy variable for overload resolution
 * @return Left factor of the matrix square-root
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::DecomposeProcessCovariance(const ProblemT& problem, const StateCovarianceT& mQ, false_type dummy) const -> StateCovarianceT
{
	assert(flagValid);
	return DecomposeCovariance(mQ);
}	//StateCovarianceT DecomposeProcessCovariance(const ProblemT& problem, false_type dummy) const

/**
 * @brief Decomposes the measurement noise covariance matrix
 * @param[in] problem Problem
 * @param[in] mR Measurement covariance matrix
 * @param[in] dummy Dummy variable for overload resolution
 * @return Left factor of the matrix square-root
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::DecomposeMeasurementCovariance(const ProblemT& problem, const MeasurementCovarianceT& mR, false_type dummy) const -> MeasurementCovarianceT
{
	assert(flagValid);
	return DecomposeCovariance(mR);
}	//MeasurementCovarianceT DecomposeMeasurementCovariance(const ProblemT& problem, const MeasurementCovarianceT& mR, false_type dummy) const

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class ProblemT>
UnscentedKalmanFilterC<ProblemT>::UnscentedKalmanFilterC() : flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] pparameters Parameters
 * @post Valid object, but the filter is not initialised
 */
template<class ProblemT>
UnscentedKalmanFilterC<ProblemT>::UnscentedKalmanFilterC(const UnscentedKalmanFilterParametersC& pparameters) : flagValid(false), parameters(pparameters)
{
	ValidateParameters(pparameters);
}	//UnscentedKalmanFilterC(const UnscentedKalmanFilterParametersC& pparameters)

/**
 * @brief Computes the sample weights for SUT
 * @param[in] problem Problem
 * @return A tuple of weights: [Central sample for mean; Central sample for covariance; Peripheral samples]
 */
template<class ProblemT>
tuple<double, double, double> UnscentedKalmanFilterC<ProblemT>::ComputeWeights(const ProblemT& problem) const
{
	unsigned int nDim=2*problem.StateDimensionality() + problem.MeasurementDimensionality();
	double alpha=parameters.alpha;
	double beta=parameters.beta;
	double kappa=parameters.kappa;

	double alpha2=pow<2>(alpha);
	double lambda=fma(alpha2, nDim+kappa, -int(nDim));
	double nxpLambda=nDim+lambda;
	double wPeriphery=0.5/nxpLambda;	// Weight of the peripheral samples
	double wMeanCentre=lambda/nxpLambda;	// Weight of the central sample for mean estimation
	double wCovCentre=wMeanCentre + 1-alpha2+beta;	// Weight of the central sample for covariance estimation

	return make_tuple(wMeanCentre, wCovCentre, wPeriphery);
}	//tuple<double, double, double> ComputeWeights(const UnscentedKalmanFilterParametersC& parameters, unsigned int nDim)

/**
 * @brief Applies the update functions to a sample
 * @param[in] problem Problem
 * @param[in] perturbationP Perturbation due to the state covariance
 * @param[in] perturbationQ Perturbation due to the process noise
 * @param[in] perturbationR Perturbation due to the measurement noise
 * @param[in] flagPropagate If \c true, state propagation function is applied
 * @return A tuple of sigma points: [State;Measurement]. Invalid, if the propagation or the measurement function fails
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::ApplyTransformation(const ProblemT& problem, const VectorXd& perturbationP, const VectorXd& perturbationQ, const VectorXd& perturbationR, bool flagPropagate) const ->optional<tuple<StateSampleT, MeasurementSampleT> >
{
	if(flagPropagate)
	{
		optional<StateT> propagated=problem.ApplyPropagationFunction(state, perturbationP, perturbationQ);

		if(!propagated)
			return optional<tuple<StateSampleT, MeasurementSampleT> >();

		return problem.ApplyMeasurementFunction(*propagated, perturbationP, perturbationR);
	}
	else	//if(flagPropagate)
		return problem.ApplyMeasurementFunction(state, perturbationP, perturbationR);
}	//optional<tuple<StateSampleT, MeasurementSampleT> > ApplyTransformation(const VectorXd& perturbation, bool flagPropagate)

/**
 * @brief Applies SUT to the augmented state
 * @param[in] problem Problem
 * @param[in] mR Measurement noise
 * @param[in] flagPropagate If \c true , SUT applies the propagation function as well
 * @return A tuple of sigma points: [State; Measurement]
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::ApplySUT(const ProblemT& problem, const MeasurementCovarianceT& mR, bool flagPropagate) const -> tuple<vector<StateSampleT>, vector<MeasurementSampleT> >
{
	int nThreads=min( omp_get_max_threads(), max(1, (int)parameters.nThreads) );	//No more threads than available resources
	unsigned int sState=problem.StateDimensionality();
	unsigned int sMeasurement=problem.MeasurementDimensionality();
	unsigned int nDim=2*sState+sMeasurement;
	double spread= parameters.alpha *sqrt(nDim + parameters.kappa );

	//Decomposition of the augmented covariance matrix
	MatrixXd mCa=MatrixXd::Zero(nDim, nDim);
	mCa.block(0,0,sState,sState)=DecomposeStateCovariance(problem, typename ProblemT::can_decompose_state_covariance());	//State component

	//If time update, and there is process noise
	if(flagPropagate && mQsq)
		mCa.block(sState,sState,sState,sState)=*mQsq;	//Process noise component

	mCa.block(2*sState, 2*sState, sMeasurement, sMeasurement)=DecomposeMeasurementCovariance(problem, mR, typename ProblemT::can_decompose_measurement_covariance());	//Measurement noise component

	//Perturb and transform
	vector<StateSampleT> stateSigma(2*nDim+1);
	vector<MeasurementSampleT> measurementSigma(2*nDim+1);
	VectorXd perturbation=VectorXd::Zero(nDim);

	//First sample

	tuple<vector<StateSampleT>, vector<MeasurementSampleT> > output;

	typedef optional<tuple<StateSampleT, MeasurementSampleT> > SigmaPairT;
	SigmaPairT sigma=ApplyTransformation(problem, perturbation.head(sState), perturbation.segment(sState,sState), perturbation.tail(sMeasurement), flagPropagate);
	if(sigma)
		tie(stateSigma[0], measurementSigma[0])=*sigma;
	else
		return output;

	bool flagBreak=false;
	size_t index=1;
	size_t localIndex;
#pragma omp parallel if(nThreads>1) num_threads(nThreads) private(localIndex)
	while(!flagBreak)
	{
#pragma omp critical(UKF_AS2)
	{
		localIndex=index;	//Thread-local copy of index
		++index;
	}

		perturbation=spread*mCa.col(localIndex-1);

		//If no perturbation, copy sigma
		bool flagZero=(perturbation.array()>0).all();

		SigmaPairT sigmaP = flagZero ? sigma : ApplyTransformation(problem, perturbation.head(sState), perturbation.segment(sState,sState), perturbation.tail(sMeasurement), flagPropagate);
		SigmaPairT sigmaM = flagZero ? sigma : ApplyTransformation(problem, -perturbation.head(sState), -perturbation.segment(sState,sState), -perturbation.tail(sMeasurement), flagPropagate);

	#pragma omp critical(UKF_AS1)
	{
		if(sigmaP)
			tie(stateSigma[localIndex], measurementSigma[localIndex])=*sigmaP;

		if(sigmaM)
			tie(stateSigma[localIndex+nDim], measurementSigma[localIndex+nDim])=*sigmaM;

		index -= !(sigmaP && sigmaM) ? 1:0;	//index!=nDim at exit signifies a failure
		flagBreak= flagBreak || (!(sigmaP && sigmaM)) || (index>nDim);
	#pragma	 omp flush(flagBreak)
	}	//#pragma omp critical(SUT_R1)
	}	//while(!flagBreak)

	if(flagBreak && index<=nDim)
		return output;

	get<0>(output).swap(stateSigma);
	get<1>(output).swap(measurementSigma);
	return output;
}	//tuple<vector<StateSampleT>, vector<MeasurementSampleT> > ApplySUT(const ProblemT& problem, const MeasurementCovarianceT& mR, const ScaledUnscentedTransformationParametersC& parameters)

/**
 * @brief Advances the state of the filter, and generates a measurement prediction
 * @param[out] predictedMeasurement Predicted measurement. Invalid if measurement prediction fails, or \c flagMeasurement=false
 * @param[in] problem Problem
 * @param[in] flagMeasurement If \c true , a measurement prediction is computed
 * @param[in] mR Measurement covariance. If invalid, assumed to be 0
 * @return \c true if the state propagation is successful (even if the measurement prediction fails).
 * @pre \c flagValid=true
 * @pre \c problem is valid
 * @post If prediction succeeds, \c state is modified
 * @throws invalid_argument If \c problem is not valid
 */
template<class ProblemT>
bool UnscentedKalmanFilterC<ProblemT>::Predict(optional<MeasurementT>& predictedMeasurement, const ProblemT& problem, bool flagMeasurement, const optional<MeasurementCovarianceT>& mR)
{
	//Preconditions
	assert(flagValid);
	predictedMeasurement=optional<MeasurementT>();

	if(!problem.IsValid())
		throw(invalid_argument("UnscentedKalmanFilterC::PredictState : Invalid problem."));

	//SUT

	unsigned int sMeasurement=problem.MeasurementDimensionality();
	MeasurementCovarianceT mCovR = (mR && flagMeasurement) ? *mR : MeasurementCovarianceT::Zero(sMeasurement, sMeasurement);	//Measurement covariance

	vector<StateSampleT> stateSigma;
	vector<MeasurementSampleT> measurementSigma;
	tie(stateSigma,measurementSigma)=ApplySUT(problem, mCovR, true);

	if(stateSigma.empty())
		return false;

	//If SUT succeeds compute the state and the measurement

	//State prediction
	double wMeanCentre;
	double wCovCentre;
	double wPeriphery;
	tie(wMeanCentre, wCovCentre, wPeriphery)=ComputeWeights(problem);
	optional<StateT> predictedState=problem.ComputeState(stateSigma, wMeanCentre, wCovCentre, wPeriphery);

	if(!predictedState)
		return false;

	state=*predictedState;	//Apply the state update

	//Measurement prediction
	if(flagMeasurement && !measurementSigma.empty())
		predictedMeasurement=problem.ComputeMeasurement(measurementSigma, wMeanCentre, wCovCentre, wPeriphery);

	return true;
}	//bool PredictState(const ProblemT& problem)

/**
 * @brief Updates the state
 * @param[in] problem Problem
 * @param[in] observed Observed measurement
 * @return \c true if the operation is successful
 * @pre \c flagValid=true
 * @pre \c problem is valid
 * @post If update succeeds, \c state is modified and, \c sigmaState and \c sigmaMeasurement are cleared
 * @throws invalid_argument If \c problem is not valid
 */
template<class ProblemT>
bool UnscentedKalmanFilterC<ProblemT>::Update(const ProblemT& problem, const MeasurementT& observed)
{
	//Preconditions
	assert(flagValid);

	if(!problem.IsValid())
		throw(invalid_argument("KalmanFilterC::Update : Invalid problem."));

	//SUT
	vector<StateSampleT> stateSigma;
	vector<MeasurementSampleT> measurementSigma;
	tie(stateSigma,measurementSigma)=ApplySUT(problem, problem.GetMeasurementCovariance(observed), false);

	if(stateSigma.empty())
		return false;

	//Measurement covariance
	double wMeanCentre;
	double wCovCentre;
	double wPeriphery;
	tie(wMeanCentre, wCovCentre, wPeriphery)=ComputeWeights(problem);
	optional<MeasurementT> measurement=problem.ComputeMeasurement(measurementSigma, wMeanCentre, wCovCentre, wPeriphery);

	if(!measurement)
		return false;

	//Cross covariance
	optional<CrossCovarianceT> mPxy=problem.ComputeCrossCovariance(state, stateSigma, *measurement, measurementSigma, wCovCentre, wPeriphery);

	if(!mPxy)
		return false;

	//Kalman update equations
	MeasurementVectorT innovation=problem.ComputeInnovationVector(observed, *measurement);

	MeasurementCovarianceT mPyy=problem.GetMeasurementCovariance(*measurement);	//Innovation covariance
	CrossCovarianceT mK= (*mPxy)*mPyy.inverse();	//Kalman gain

	StateVectorT deltaMean=mK*innovation;	//State mean update
	StateCovarianceT deltaCovariance= -(*mPxy) * mK.transpose();

	return problem.Update(state, deltaMean, deltaCovariance);
}	//bool Update(const MeasurementT& observed, const MeasurementT& predicted, const ProblemT& problem, unsigned int nIteration)

/**
 * @brief Batch operation
 * @tparam MeasurementRangeT A range of measurements
 * @param[in] problem Problem
 * @param[in] measurements Measurements
 * @return \c false if any of the steps fails
 * @pre \c MeasurementRangeT is a forward range holding elements of type \c measurement_type
 * @pre \c flagValid=true
 */
template<class ProblemT>
template<class MeasurementRangeT>
bool UnscentedKalmanFilterC<ProblemT>::Run(const ProblemT& problem, const MeasurementRangeT& measurements)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<MeasurementRangeT>));
	static_assert(is_same<MeasurementT, typename range_value<MeasurementRangeT>::type>::value, "UnscentedKalmanFilterT::Run : MeasurementRangeT must hold elements of type MeasurementT");
	assert(flagValid);

	//Feed the measurements sequentially
	for(const auto& current : measurements)
	{
		optional<MeasurementT> predicted;
		bool flagPredict=Predict(predicted, problem, false);	//Time update

		if(!flagPredict)
			return false;

		bool flagSuccess=Update(problem, current);

		if(!flagSuccess)
			return false;
	}	//for(const auto& current : measurements)

	return true;
}	//bool Run(const MeasurementRangeT& measurements, const ProblemT& problem)

/**
 * @brief Sets the state
 * @param[in] problem Problem
 * @param[in] sstate New state
 * @param[in] mQ Process noise. If not valid, not set
 * @pre If \c mQ is valid, it has the same size as the state covariance matrix
 * @post Object is valid
 */
template<class ProblemT>
void UnscentedKalmanFilterC<ProblemT>::SetState(const ProblemT& problem, const StateT& sstate, const optional<StateCovarianceT>& mQ)
{
	//Preconditions
	assert(mQ ? mQ->rows()==problem.StateDimensionality() && mQ->cols()==problem.StateDimensionality() : true);

	state=sstate;

	if(mQ)
		mQsq=DecomposeProcessCovariance(problem, *mQ, typename ProblemT::can_decompose_process_covariance());

	flagValid=true;
}	//void SetState(const StateT& sstate)

/**
 * @brief Returns a constant reference to the state
 * @return Filter state
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto UnscentedKalmanFilterC<ProblemT>::GetState() const -> const StateT&
{
	assert(flagValid);
	return state;
}	//StateT& GetState()

/**
 * @brief Clears the state
 * @post Object is invalid
 */
template<class ProblemT>
void UnscentedKalmanFilterC<ProblemT>::Clear()
{
	flagValid=false;
	state=StateT();
}	//void Clear()

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class ProblemT>
bool UnscentedKalmanFilterC<ProblemT>::IsValid()
{
	return flagValid;
}	//void Clear()

}	//StateEstimationN
}	//SeeSawN

#endif /* UNSCENTED_KALMAN_FILTER_IPP_9601231 */
