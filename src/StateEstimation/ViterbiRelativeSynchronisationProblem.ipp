/**
 * @file ViterbiRelativeSynchronisationProblem.ipp Implementation of the Viterbi relative synchronisation problem
 * @author Evren Imre
 * @date 15 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_RELATIVE_SYNCHRONISATION_PROBLEM_IPP_1760290
#define VITERBI_RELATIVE_SYNCHRONISATION_PROBLEM_IPP_1760290

#include <boost/optional.hpp>
#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/numeric/interval.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <boost/bimap.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/bimap/vector_of.hpp>
#include <map>
#include <list>
#include <array>
#include <tuple>
#include <type_traits>
#include <cmath>
#include <set>
#include <iterator>
#include <cmath>
#include <utility>
#include <algorithm>
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Camera.h"
#include "../Elements/Descriptor.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/Distance.h"
#include "../Metrics/Similarity.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Numeric/NumericFunctor.h"
#include "../Geometry/FundamentalSolver.h"
#include "../Wrappers/EigenMetafunction.ipp"
#include "../Wrappers/BoostRange.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::numeric::interval;
using boost::numeric::in;
using boost::dynamic_bitset;
using boost::bimaps::bimap;
using boost::bimaps::set_of;
using boost::bimaps::vector_of;
using boost::bimaps::left_based;
using std::is_same;
using std::map;
using std::vector;
using std::list;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::get;
using std::exp;
using std::multiset;
using std::set;
using std::next;
using std::max;
using std::min;
using std::fma;
using std::pair;
using std::make_pair;
using std::any_of;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::ValueTypeM;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::ElementsN::ImageDescriptorT;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::GeometryN::Fundamental7SolverC;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::MakeBinaryZipRange;

/**
 * @brief Viterbi problem for relative synchronisation
 * @remarks An instance of \c ViterbiProblemConceptC
 * @remarks \c constraint defines the admissible state-measurement pairs as a region between two lines. The lines correspond to the uncertainty of the frame rate and the temporal offset
 * @remarks Frame similarity metric: Median loss
 * @remarks Measurement probability: \f$ e^{-loss}\f$ . For the stability of the synchronisation operation, this should be non-zero
 * @remarks Correspondence propagation: Each feature association suggest an association between two tracks.
 * 	- The problem stores the association with the best score. The output of the feature matcher is augmented by the implied correspondences from the tracks, subject to the epipolar constraint
 * 	- Helps when there are long and reliable trajectories. However, with lower quality trajectories, it can actually degrade the performance: mismatched features affect the measurement probabilities, modifying the optimal path through the trellis
 * @remarks No dedicated unit tests. Too complicated
 * @ingroup Problem
 * @nosubgrouping
 */
class ViterbiRelativeSynchronisationProblemC
{
	private:

		typedef vector<ImageFeatureC> FeatureListT;	///< A list of image features
		typedef bimap<vector_of<size_t>, set_of<size_t>, left_based>  TrackMapT;	///< Bimap between the feature indices and tracks
		typedef tuple<FeatureListT, CameraMatrixT, optional<TrackMapT> > MeasurementT;	///< A frame in an image sequence. Features; camera matrix; track Ids
																									//An invalid track id component implies that correspondences will not be propagated across trajectories

		typedef ValueTypeM<Coordinate2DT>::type RealT;	///< Floating point type

		typedef tuple<RealT, CoordinateCorrespondenceList2DT> NodeT;	///< A node. Score; Correspondences;

		/** @name Configuration */ ///@{
		bool flagValid;	///< \c true if the object is valid

		bool flagFixedCameras;	///< \c true if the relative calibration of the camera pair is constant

		bool flagPropagate;	///< If \c true , the algorithm attempts to propagate correspondence across feature trajectories

		vector<MeasurementT> stateList;	///< List of states

		typedef interval<RealT> IntervalT;	///< An interval
		optional<IntervalT> alphaRange;	///< Range for the frame rate
		optional<IntervalT> tauRange;	///< Range for the temporal offset

		FeatureMatcherParametersC matcherParameters;	///< Parameters for the feature matcher
		InverseEuclideanIDConverterT featureSimilarityMetric;	///< Feature similarity metric

		RealT inlierTh;	///< Inlier threshold for the epipolar constraint
		optional<EpipolarSampsonConstraintT> epipolarConstraint;	///< Constraint imposed by the relative calibration between the cameras

		size_t minCorrespondence;	///< The minimum number of correspondences for a successful frame match
		Fundamental7SolverC::loss_type lossMap;	///< Error-to-loss map
		RealT minMeasurementProbability;	///< Measurement probability associated with a failed frame match
		///@}

		/** @name State */ //@{

		typedef vector<optional<NodeT> > NodeVectorT;	///< A vector of nodes. Each element corresponds to a state
		typedef map<unsigned int, NodeVectorT> NodeArrayT;	///< An array of nodes. [Measurement index, Associated node vector]
		NodeArrayT cache;	///< Cache for the state-measurement evaluations

		interval<unsigned int> admissibleStateInterval;	///< Admissible states for the current value of \c cMeasurement
		unsigned int cMeasurement;	///< Measurement count

		map<size_t, pair<size_t, double> > trackCorrespondences;	///< Track correspondences. [measurement track id; state track id; score]
		//@}

		/** @name Implementation details */ //@{

		tuple<CoordinateCorrespondenceList2DT, multiset<RealT> > PropagateCorrespondences(const MeasurementT& measurement, const CorrespondenceListT& matcherCorrespondences, unsigned int stateIndex );	///< Propagates the correspondences across the tracks

		//@}

	public:

		/** @name ViterbiProblemConceptC interface */ //@{

		typedef MeasurementT measurement_type;	///< A frame in an image sequence

		ViterbiRelativeSynchronisationProblemC();	///< Default constructor

		double ComputeMeasurementProbability(const MeasurementT& measurement, unsigned int currentState);	///< Computes the probability that a measurement originates from a specified state
		double ComputeTransitionProbability(unsigned int source, unsigned int destination) ;	///< Computes the source->destination transition probability
		unsigned int GetStateCardinality() const;	///< Returns the number of states

		bool IsValid() const;	///< Returns \c flagValid

		void InitialiseUpdate();	///< Initialises a measurement update
		void FinaliseUpdate();	///< Finalises a measurement update
		//@}

		/** @name Constructor */
		template<class MeasurementRangeT> ViterbiRelativeSynchronisationProblemC(const MeasurementRangeT& sstates, const optional<tuple<IntervalT, IntervalT> >& bounds, bool fflagFixedCameras,  const FeatureMatcherParametersC& mmatcherParameters, RealT nnoiseVariance, size_t mminCorrespondence, bool fflagPropagate);	///< Constructor

		/** @name Accessors */ //@{
		RealT GetMinMeasurementProbability() const;	///< Returns the minimum measurement probability
		const NodeArrayT& GetCache() const;	///< Returns a constant reference to \c cache
		void SetBounds(const tuple<IntervalT, IntervalT>& newBounds);	///< Modifies \c bounds
		//@}

		/** @name Utility */ //@{
		void ResetMeasurementCounter();	///< Resets \c cMeasurement
		void Clear();	///< Clears the state
		//@}

		typedef IntervalT interval_type;	///< A parameter range

		/** @name Measurement type components */ //@{
		typedef TrackMapT track_map_type;	///< Bimap between the feature indices and tracks
		static constexpr size_t iFeatureList=0;	///< Index of the feature list component
		static constexpr size_t iCamera=1;	///< Index of the camera matrix component
		static constexpr size_t iTrackId=2;	///< Index of the track id component
		//@}

		/**@name node_type definition */ //@{
		typedef NodeT node_type;	///< A node holding the evaluation record for a pair
		static constexpr size_t iScore=0;	///< Index of the score component
		static constexpr size_t iCorrespondence=1;	///< Index of the feature correspondence component
		//@}
};	//class ViterbiRelativeSynchronisationProblemC

/********** IMPLEMENTATION STARTS HERE  **********/

/**
 * @brief Constructor
 * @tparam MeasurementRangeT A range of measurements
 * @param[in] sstates Target (second) sequence
 * @param[in] bounds Lower and upper bounds of the admissible region, defined by the upper and lower bounds of the synchronisation parameters. [Lower; Upper]
 * @param[in] fflagFixedCameras If \c true , the relative calibration between the cameras is fixed at the first measurement-state pair
 * @param[in] mmatcherParameters Feature matcher parameters
 * @param[in] noiseVariance Image noise variance, as pixel^2
 * @param[in] mminCorrespondence Minimum number of correspondences
 * @param[in] fflagPropagate If \c true, feature correspondences are propagated across the tracks. false if the elements of \c sstates does not have a valid track id component
 * @pre \c MeasurementRangeT is a single pass range holding values of type \c MeasurementT
 * @pre \c noiseVariance>0
 * @pre \c mminCorrespondence>0
 * @pre If \c fflagPropagate and \c sstates holds valid track id components, for each feature there is a track id entry (unenforced)
 * @post Valid object
 */
template<class MeasurementRangeT>
ViterbiRelativeSynchronisationProblemC::ViterbiRelativeSynchronisationProblemC(const MeasurementRangeT& sstates, const optional<tuple<IntervalT, IntervalT> >& bounds, bool fflagFixedCameras, const FeatureMatcherParametersC& mmatcherParameters, RealT noiseVariance, size_t mminCorrespondence, bool fflagPropagate) : flagValid(true), flagFixedCameras(fflagFixedCameras), flagPropagate(fflagPropagate), matcherParameters(mmatcherParameters), minCorrespondence(mminCorrespondence)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<MeasurementRangeT>));
	static_assert(is_same< typename range_value<MeasurementRangeT>::type, MeasurementT >::value, "ViterbiRelativeSynchronisationProblemC::ViterbiRelativeSynchronisationProblemC : MeasurementRangeT must hold values of type measurement_type");

	assert(noiseVariance>0);
	assert(mminCorrespondence>0);

	ResetMeasurementCounter();

	//State list
	stateList.reserve(boost::distance(sstates));
	for(const auto& current : sstates)
		stateList.push_back(current);

	//Bounds
	if(bounds)
		SetBounds(*bounds);

	//Image matching problem
	ReciprocalC<typename ValueTypeM<ImageDescriptorT>::type> distanceToSimilarityMap;
	featureSimilarityMetric=InverseEuclideanIDConverterT(EuclideanDistanceIDT(), distanceToSimilarityMap);

	//Matcher configuration
	matcherParameters.flagStatic=true;
	matcherParameters.nThreads=1;

	inlierTh=EpipolarSampsonConstraintT::error_type::ComputeOutlierThreshold(0.05, noiseVariance);
	lossMap=Fundamental7SolverC::loss_type(inlierTh, inlierTh);
	minMeasurementProbability=exp(-lossMap(inlierTh));

	//If no track ids component, turn off correspondence propagation
	if(!stateList.empty() && flagPropagate)
		flagPropagate=any_of(stateList.begin(), stateList.end(), [](const MeasurementT& item){return !!get<iTrackId>(item); });

	//epipolarConstraint: lazy initialisation
}	//ViterbiRelativeSynchronisationProblemC(const MeasurementRangeT& sstates)

}	//StateEstimationN
}	//SeeSawN

#endif /* VITERBI_RELATIVE_SYNCHRONISATION_PROBLEM_IPP_1760290 */
