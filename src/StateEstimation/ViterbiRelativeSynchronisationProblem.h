/**
 * @file ViterbiRelativeSynchronisationProblem.h Public interface for the Viterbi relative synchronisation problem
 * @author Evren Imre
 * @date 15 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_RELATIVE_SYNCHRONISATION_PROBLEM_H_7809123
#define VITERBI_RELATIVE_SYNCHRONISATION_PROBLEM_H_7809123

#include "ViterbiRelativeSynchronisationProblem.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{

class ViterbiRelativeSynchronisationProblemC;	///< Viterbi problem for relative synchronisation

/********** EXTERN TEMPLATES **********/
extern template ViterbiRelativeSynchronisationProblemC::ViterbiRelativeSynchronisationProblemC(const vector<ViterbiRelativeSynchronisationProblemC::measurement_type>&, const optional<tuple<ViterbiRelativeSynchronisationProblemC::interval_type, ViterbiRelativeSynchronisationProblemC::interval_type>>&, bool, const FeatureMatcherParametersC&, double, size_t, bool);
}	//StateEstimationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::multiset<double>;
extern template class std::multiset<float>;

#endif /* VITERBI_RELATIVE_SYNCHRONISATION_PROBLEM_H_7809123 */
