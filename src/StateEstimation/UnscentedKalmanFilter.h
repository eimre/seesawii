/**
 * @file UnscentedKalmanFilter.h Public interface for UKF
 * @author Evren Imre
 * @date 9 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UNSCENTED_KALMAN_FILTER_H_4890213
#define UNSCENTED_KALMAN_FILTER_H_4890213

#include "UnscentedKalmanFilter.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{

template<class ProblemT> class UnscentedKalmanFilterC;	///< A generic implementation of UKF
}	//StateEstimationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
#endif /* UNSCENTED_KALMAN_FILTER_H_4890213 */
