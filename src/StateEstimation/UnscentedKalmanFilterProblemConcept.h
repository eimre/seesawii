/**
 * @file UnscentedKalmanFilterProblemConcept.h Public interface for \c UnscentedKalmanFilterProblemConceptC
 * @author Evren Imre
 * @date 20 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UNSCENTED_KALMAN_FILTER_PROBLEM_CONCEPT_H_6012314
#define UNSCENTED_KALMAN_FILTER_PROBLEM_CONCEPT_H_6012314

#include "UnscentedKalmanFilterProblemConcept.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{
template<class TestT> class UnscentedKalmanFilterProblemConceptC;	///< Concept definition and checker for UKF problems
}	//StateEstimationN
}	//SeeSawN

#endif /* UNSCENTED_KALMAN_FILTER_PROBLEM_CONCEPT_H_6012314 */
