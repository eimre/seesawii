/**
 * @file UKFOrientationTrackingProblem.cpp Implementation of \c UKFOrientationTrackingProblemC
 * @author Evren Imre
 * @date 27 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "UKFOrientationTrackingProblem.h"
namespace SeeSawN
{
namespace StateEstimationN
{

/********** UKFOrientationTrackingProblemC **********/

/**
 * @brief Returns \c true if the object is valid
 * @return Always \c true
 */
bool UKFOrientationTrackingProblemC::IsValid()
{
	return true;
}	//bool IsValid()

/**
 * @brief Perturbs the state
 * @param[in] state State
 * @param[in] perturbation Perturbation
 * @return Perturbed state vector
 */
auto UKFOrientationTrackingProblemC::PerturbState(const StateT& state, const StateVectorT& perturbation) -> StateVectorT
{
	StateVectorT currentState=state.GetMean();

	//Perturb the orientation
	QuaternionT orientation=RotationVectorToQuaternion(currentState.segment(iOrientation,3));
	QuaternionT delta=RotationVectorToQuaternion(perturbation.segment(iOrientation,3));

	StateVectorT perturbed;
	perturbed.segment(iOrientation,3)=QuaternionToRotationVector(delta*orientation);

	//Perturb the rotation
	perturbed.segment(iRotation,3)=currentState.segment(iRotation,3)+perturbation.segment(iRotation,3);

	return perturbed;
}	//StateVectorT PerturbState(const StateT& state, const StateVectorT& perturbation)

/**
 * @brief Perturbs and propagates the state
 * @param[in] state State
 * @param[in] deltaP Perturbation due to the state covariance
 * @param[in] deltaQ Perturbation due to the process noise
 * @return Propagated state
 */
auto UKFOrientationTrackingProblemC::ApplyPropagationFunction(const StateT& state, const StateVectorT& deltaP, const StateVectorT& deltaQ) -> optional<StateT>
{
	StateT propagated(state);	//Propagated state

	//Perturb the state
	StateVectorT perturbedP=PerturbState(propagated, deltaP);

	//Propagate the state mean

	QuaternionT orientation=RotationVectorToQuaternion(perturbedP.segment(iOrientation,3));
	QuaternionT rotation=RotationVectorToQuaternion(perturbedP.segment(iRotation,3));

	StateVectorT propagatedMean=state.GetMean();
	propagatedMean.segment(iOrientation,3)=QuaternionToRotationVector(rotation*orientation);
	propagated.SetMean(propagatedMean);

	//Apply the process noise
	StateVectorT perturbedQ=PerturbState(propagated,deltaQ);
	propagated.SetMean(perturbedQ);

	optional<StateT> output=propagated;
	return output;
}	//optional<StateT> ApplyPropagationFunction(const StateT& state, const StateVectorT& deltaP, const StateVectorT& deltaQ)

/**
 * @brief Perturbs and applies the measurement function
 * @param[in] state State
 * @param[in] deltaP Perturbation due to the state covariance
 * @param[in] deltaR Perturbation due to the measurement noise
 * @return A tuple, state and measurement sigma points
 */
auto UKFOrientationTrackingProblemC::ApplyMeasurementFunction(const StateT& state, const StateVectorT& deltaP, const MeasurementVectorT& deltaR) -> optional<tuple<StateSigmaT, MeasurementSigmaT> >
{
	//Perturb the state
	StateVectorT perturbed=PerturbState(state, deltaP);

	//Apply the measurement function
	QuaternionT orientation=RotationVectorToQuaternion(perturbed.segment(iOrientation,3));
	QuaternionT rotation=RotationVectorToQuaternion(deltaR);

	MeasurementVectorT measurement=QuaternionToRotationVector(rotation*orientation);

	optional<tuple<StateSigmaT, MeasurementSigmaT> > output=make_tuple(perturbed, measurement);
	return output;
}	//optional<tuple<StateSigmaT, MeasurementSigmaT> > ApplyMeasurementFunction(const StateT& state, const StateVectorT& deltaP, const MeasurementVectorT& deltaR)

/**
 * @brief Computes the state corresponding to a sigma point set
 * @param[in] sigmaSet State sigma point set
 * @param[in] wMeanCentral Weight of the central sample (mean)
 * @param[in] wCovarianceCentral Weight of the central sample (covariance)
 * @param[in] wPeripheral Weights of the peripheral samples
 * @return State
 */
auto UKFOrientationTrackingProblemC::ComputeState(const vector<StateSigmaT>& sigmaSet, double wMeanCentral, double wCovarianceCentral, double wPeripheral) -> optional<StateT>
{
	//Weights
	size_t nSample=sigmaSet.size();
	vector<double> wMean(nSample, wPeripheral); wMean[0]=wMeanCentral;
	vector<double> wCovariance(nSample, wPeripheral); wCovariance[0]=wCovarianceCentral;

	//Mean

	vector<QuaternionT> qList; qList.reserve(nSample);
	RotationVectorT rotationAcc=RotationVectorT::Zero();
	size_t index=0;

	for(const auto& current : MakeBinaryZipRange(sigmaSet, wMean) )
	{
		qList.push_back(RotationVectorToQuaternion(get<0>(current).segment(iOrientation,3)));
		rotationAcc+= get<1>(current)*get<0>(current).segment(iRotation,3);
		++index;
	}	//for(const auto& current : sigmaSet)

	//Quaternion averaging
	QuaternionT qMean;
	MeasurementCovarianceT dummy;
	tie(qMean, dummy)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	//Compose the mean
	StateVectorT mean;
	mean.segment(iOrientation,3)=QuaternionToRotationVector(qMean);
	mean.segment(iRotation,3)=rotationAcc;

	//Covariance
	StateCovarianceT mCovAcc=StateCovarianceT::Zero();
	UKFOrientationTrackingProblemStateDifferenceC subtractor;
	for(const auto& current : MakeBinaryZipRange(sigmaSet, wCovariance))
	{
		StateVectorT dif=subtractor(get<0>(current), mean);
		mCovAcc+=get<1>(current)*(dif*dif.transpose());
	}

	optional<StateT> output=StateT(mean, mCovAcc);
	return output;
}	//optional<StateT> ComputeState(const vector<StateSigmaT>& sigmaSet, double wMeanCentral, double wCovarianceCentral, double wPeripheral)

/**
 * @brief Computes the measurement corresponding to a sigma point set
 * @param[in] sigmaSet Measurement sigma point set
 * @param[in] wMeanCentral Weight of the central sample (mean)
 * @param[in] wCovarianceCentral Weight of the central sample (covariance)
 * @param[in] wPeripheral Weights of the peripheral samples
 * @return Measurement
 */
auto UKFOrientationTrackingProblemC::ComputeMeasurement(const vector<MeasurementSigmaT>& sigmaSet, double wMeanCentral, double wCovarianceCentral, double wPeripheral) -> optional<MeasurementT>
{
	//Convert to quaternions
	vector<QuaternionT> qList; qList.reserve(sigmaSet.size());
	for(const auto& current : sigmaSet)
		qList.push_back(RotationVectorToQuaternion(current));

	vector<double> wMean(qList.size(), wPeripheral); wMean[0]=wMeanCentral;
	vector<double> wCovariance(qList.size(), wPeripheral); wCovariance[0]=wCovarianceCentral;

	QuaternionT qMean;
	MeasurementCovarianceT mCov;
	tie(qMean, mCov)=ComputeQuaternionSampleStatistics(qList, wMean, wCovariance);

	return MeasurementT(QuaternionToRotationVector(qMean), mCov);
}	//optional<MeasurementT> ComputeMeasurement(const vector<MeasurementSigmaT>& sigmaSet, double wMeanCentral, double wCovarianceCentral, double wPeripheral)

/**
 * @brief Updates the state
 * @param[in] state State
 * @param[in] deltaMean Update to the mean
 * @param[in] deltaCovariance Update to the covariance
 * @return Updated state
 */
bool UKFOrientationTrackingProblemC::Update(StateT& state, const StateVectorT& deltaMean, const StateCovarianceT& deltaCovariance)
{
	//Rescale the update term for sane operation

	//Clip the orientation update so that its magnitude is less than pi. R2, "Attitude Error Representations"
	double normO=deltaMean.segment(iOrientation,3).norm();
	double scaleO= (normO < pi<double>()) ? 1 : (0.99*pi<double>())/normO;	//0.01 pi safety margin

	//Clip the magnitude of the rotation update at that of the orientation update. Prevents jumps in the rotation
	double normR=deltaMean.segment(iRotation,3).norm();
	double scaleR = (normR < scaleO*normO) ? 1 : scaleO*normO/normR;

	//Compute the overall scale term, and scale the mean and the covariance update
	//This is equivalent to multiplying the Kalman gain by a constant attenuation factor

	double scale=min(scaleO,scaleR);

	//Mean update
	StateVectorT newMean=state.GetMean() + scale*deltaMean;

	//Map the orientation back to the 0-2pi range
	QuaternionT q=RotationVectorToQuaternion(newMean.segment(iOrientation,3));
	newMean.segment(iOrientation,3)=QuaternionToRotationVector(q);

	state.SetMean(newMean);
	state.SetCovariance(state.GetCovariance() + scale*deltaCovariance);

	return true;
}	//bool Update(StateT& state, const StateVectorT& deltaMean, const StateCovarianceT& deltaCovariance)

/********** UKFOrientationTrackingProblemStateDifferenceC **********/
/**
 * @brief Computes the difference between operands
 * @param[in] op1 First operand
 * @param[in] op2 Second operand
 * @return Difference vector
 */
auto UKFOrientationTrackingProblemStateDifferenceC::operator ()(const first_argument_type& op1, const second_argument_type& op2) -> result_type
{
	result_type output;

	//Orientation component
	Rotation3DMinimalDifferenceC subtractor;
	output.head(3)=subtractor(op1.head(3), op2.head(3));

	//Rotation component
	output.tail(3)=op1.tail(3)-op2.tail(3);

	return output;
}	//result_type operator ()(const first_argument_type& op1, const second_argument_type& op2)

}	//StateEstimationN
}	//SeeSawN

