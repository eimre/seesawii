/**
 * @file KalmanFilter.ipp Implementation of a generic Kalman filter
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KALMAN_FILTER_IPP_0409231
#define KALMAN_FILTER_IPP_0409231

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/optional.hpp>
#include <stdexcept>
#include <type_traits>
#include "KalmanFilterProblemConcept.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using boost::ForwardRangeConcept;
using boost::range_value;
using std::is_same;
using std::invalid_argument;
using SeeSawN::StateEstimationN::KalmanFilterProblemConceptC;

/**
 * @brief A generic implementation of Kalman filter, EKF and IEKF
 * @tparam ProblemT A Kalman filter problem
 * @pre \c ProblemT is a model of \c KalmanFilterProblemConceptC
 * @remarks Usage notes
 * 	- Batch operation, when all measurements are available: SetState, then Run
 * 	- Sequential operation, when measurements become available sequentially, after each update
 * 		- Initialisation: SetState
 * 		- Before a measurement, advance the state by the state dynamics (predict)
 * 		- Predict the measurements
 * 		- When the measurement is available, update
 * 	- Adding or deleting elements from the state: Modify the state as necessary externally, and then SetState
 * 	- It is up to the problem to convert the current state vector to the native problem representation, and back
 * 	- Depending on the problem, the engine can be used for KF or EKF.
 * 	- Iterated mode should be used only in EKF
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class KalmanFilterC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((KalmanFilterProblemConceptC<ProblemT>));
	///@endcond

	private:

		typedef typename ProblemT::state_type StateT;	///< Type of the state
		typedef typename ProblemT::state_vector_type StateVectorT;	///< Type of the state vector
		typedef typename ProblemT::state_covariance_type StateCovarianceT;	///< Type of the state covariance
		typedef typename ProblemT::measurement_type MeasurementT;	///< Type of the measurement

		/** @name State */ //@{
		StateT state;	///< State
		//@}

		/**@name Configuration */ //@{
		bool flagValid;	///< \c true if the Kalman filter engine is initialised
		optional<StateCovarianceT> processNoise;	///< Process noise
		//@}

		/** @name Implementation details */ //@{

		//@}

	public:

		typedef StateT state_type;	///< Type of the state
		typedef MeasurementT measurement_type;	///< Type of the measurements

		/** @name Constructors */ //@{
		KalmanFilterC();	///< Default constructor
		//@}

		/** @name Operations */ //@{
		bool PredictState(const ProblemT& problem);	///< Advances the state of the filter
		optional<MeasurementT> PredictMeasurement(const ProblemT& problem) const;	///< Predicts the next measurement from the filter state
		bool Update(const ProblemT& problem, const MeasurementT& observed, const MeasurementT& predicted, unsigned int nIteration=0);	///< Updates the state

		template<class MeasurementRangeT> bool Run(const ProblemT& problem, const MeasurementRangeT& measurements, unsigned int nIteration=0);	///< Batch operation
		//@}

		/** @name Utilities */ //@{
		void SetState(const StateT& sstate, const optional<StateCovarianceT>& pprocessNoise);	///< Sets the state
		const StateT& GetState() const;	///< Returns a constant reference to the state

		bool IsValid();	///< Returns \c flagValid
		void Clear();	///< Clears the state
		//@}
};	//class KalmanFilterC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class ProblemT>
KalmanFilterC<ProblemT>::KalmanFilterC() : flagValid(false)
{}

/**
 * @brief Advances the state of the filter
 * @param[in] problem Problem
 * @return \c false if the operation fails
 * @pre \c flagValid=true
 * @pre \c problem is valid
 * @post If prediction succeeds, \c state is modified
 * @throws invalid_argument If \c problem is not valid
 */
template<class ProblemT>
bool KalmanFilterC<ProblemT>::PredictState(const ProblemT& problem)
{
	//Preconditions
	assert(flagValid);

	if(!problem.IsValid())
		throw(invalid_argument("KalmanFilterC::PredictState : Invalid problem."));

	//Propagate the state mean
	optional<StateVectorT> propagated=problem.PropagateStateMean(state);

	if(!propagated)
		return false;

	StateVectorT deltaMean= *propagated - problem.GetStateMean(state);	//Update term

	//Propagate the covariance
	optional<typename ProblemT::propagation_jacobian_type> mJF=problem.ComputePropagationJacobian(state);

	if(!mJF)
		return false;

	StateCovarianceT mP=problem.GetStateCovariance(state);
	mP=0.5*(mP+mP.transpose()).eval();	//Resymmetrise

	StateCovarianceT deltaCovariance= ( (*mJF) * mP * (mJF->transpose())) - problem.GetStateCovariance(state);	//Prediction covariance - original covariance

	if(processNoise)
		deltaCovariance+=*processNoise;

	return problem.Update(state, deltaMean, deltaCovariance);
}	//bool Predict(const ProblemT& problem)

/**
 * @brief Updates the state
 * @param[in] problem Problem
 * @param[in] observed Observed measurement
 * @param[in] predicted Predicted measurement
 * @param[in] nIteration Number of IEKF iterations (0 for the standard Kalman update)
 * @return \c true if the operation is successful
 * @pre \c flagValid=true
 * @pre \c problem is valid
 * @post If update fails, state is not modified
 * @throws invalid_argument If \c problem is not valid
 */
template<class ProblemT>
bool KalmanFilterC<ProblemT>::Update(const ProblemT& problem, const MeasurementT& observed, const MeasurementT& predicted, unsigned int nIteration)
{
	//Preconditions
	assert(flagValid);

	if(!problem.IsValid())
		throw(invalid_argument("KalmanFilterC::Update : Invalid problem."));

	typedef typename ProblemT::innovation_covariance_type InnovationCovarianceT;
	InnovationCovarianceT mR=problem.GetMeasurementCovariance(observed);	//Measurement covariance
	optional<MeasurementT> currentPrediction=predicted;
	unsigned int cIteration=0;

	//If IKF, multiple passes
	do
	{
		typedef typename ProblemT::innovation_vector_type InnovationVectorT;
		InnovationVectorT vy=problem.ComputeInnovationVector(observed, *currentPrediction);	//Measurement error
		optional<typename ProblemT::measurement_jacobian_type> mJH=problem.ComputeMeasurementFunctionJacobian(state);	//Jacobian of the measurement function

		if(!mJH)
			return false;

		StateCovarianceT mP=problem.GetStateCovariance(state);
		mP=0.5*(mP+mP.transpose()).eval();	//Resymmetrise

		auto mHP=(*mJH) * mP;
		InnovationCovarianceT mS= mHP * (mJH->transpose()) + mR;	//Innovation covariance
		auto mK=mHP.transpose() * mS.inverse();	// Kalman gain. Covariance matrix is symmetric: (HP)' = PH'
		StateVectorT deltaMean=mK*vy;	//Mean update term
		StateCovarianceT deltaCovariance=-mK*mHP;	//Covariance update term

		if(deltaMean.hasNaN() || deltaCovariance.hasNaN())
			return false;

		bool flagSuccess=problem.Update(state, deltaMean, deltaCovariance);	//Update the state

		if(!flagSuccess)
			return false;

		++cIteration;

		//Predict the measurements at the new state
		if(cIteration<nIteration)
		{
			currentPrediction=PredictMeasurement(problem);

			if(!currentPrediction)
				return false;
		}	//if(cIteration<nIteration)

	}while(cIteration<=nIteration);	//IKF iterations

	return true;
}	//bool Update(const MeasurementT& measurement, const ProblemT& problem, unsigned int nIteration)

/**
 * @brief Batch operation
 * @tparam MeasurementRangeT A range of measurements
 * @param[in] problem Problem
 * @param[in] measurements Measurements
 * @param[in] nIteration Number of IEKF iterations. 0 means standard KF
 * @return \c false if any of the steps fails
 * @pre \c MeasurementRangeT is a forward range holding elements of type \c measurement_type
 * @pre \c flagValid=true
 */
template<class ProblemT>
template<class MeasurementRangeT>
bool KalmanFilterC<ProblemT>::Run(const ProblemT& problem, const MeasurementRangeT& measurements, unsigned int nIteration)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<MeasurementRangeT>));
	static_assert(is_same<MeasurementT, typename range_value<MeasurementRangeT>::type>::value, "KalmanFilterC::Run : MeasurementRangeT must hold elements of type MeasurementT");
	assert(flagValid);

	//Feed the measurements sequentially
	for(const auto& current : measurements)
	{
		bool flagSuccess=PredictState(problem);

		if(!flagSuccess)
			return false;

		optional<MeasurementT> predicted=PredictMeasurement(problem);

		if(!predicted)
			return false;

		flagSuccess = flagSuccess && Update(problem, current, *predicted, nIteration);

		if(!flagSuccess)
			return false;
	}	//for(const auto& current : measurements)

	return true;
}	//bool Run(const MeasurementRangeT& measurements, const ProblemT& problem, unsigned int nIteration)

/**
 * @brief Sets the state
 * @param[in] sstate New state
 * @param[in] pprocessNoise Process noise. If not valid, not set
 * @pre If \c pprocessNoise is valid, it has the same size as the state covariance (unenforced)
 * @post Object is valid
 */
template<class ProblemT>
void KalmanFilterC<ProblemT>::SetState(const StateT& sstate, const optional<StateCovarianceT>& pprocessNoise)
{
	state=sstate;

	if(pprocessNoise)
		processNoise=*pprocessNoise;

	flagValid=true;
}	//void SetState(const StateT& sstate)

/**
 * @brief Returns a constant reference to the state
 * @return Filter state
 * @pre \c flagValid=true
 */
template<class ProblemT>
auto KalmanFilterC<ProblemT>::GetState() const -> const StateT&
{
	assert(flagValid);
	return state;
}	//StateT& GetState()

/**
 * @brief Predicts the next measurement from the filter state
 * @param[in] problem Problem
 * @return Predicted measurement. Invalid, if the operation fails
 * @pre \c flagValid=true
 * @pre \c problem is valid
 * @throws invalid_argument If \c problem is not valid
 */
template<class ProblemT>
auto KalmanFilterC<ProblemT>::PredictMeasurement(const ProblemT& problem) const -> optional<MeasurementT>
{
	//Preconditions
	assert(flagValid);

	if(!problem.IsValid())
		throw(invalid_argument("KalmanFilterC::PredictMeasurement : Invalid problem."));

	return problem.PredictMeasurement(state);
}	//optional<MeasurementT> PredictMeasurement() const

/**
 * @brief Clears the state
 * @post Object is invalid
 */
template<class ProblemT>
void KalmanFilterC<ProblemT>::Clear()
{
	flagValid=false;
	state=StateT();
}	//void Clear()

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class ProblemT>
bool KalmanFilterC<ProblemT>::IsValid()
{
	return flagValid;
}	//void Clear()

}	//StateEstimationN
}	//SeeSawN

#endif /* KALMAN_FILTER_IPP_0409231 */
