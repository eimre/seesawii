/**
 * @file ViterbiNormalisedHistogramMatchingProblem.ipp Implementation of \c ViterbiNormalisedHistogramMatchingProbemC
 * @author Evren Imre
 * @date 13 Feb 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_NORMALISED_HISTOGRAM_MATCHING_PROBLEM_IPP_0681923
#define VITERBI_NORMALISED_HISTOGRAM_MATCHING_PROBLEM_IPP_0681923

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <type_traits>
#include <vector>
#include <tuple>
#include <cmath>
#include <climits>
#include <cstddef>

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using std::is_floating_point;
using std::vector;
using std::tuple;
using std::get;
using std::exp;
using std::fabs;
using std::max;
using std::numeric_limits;
using std::size_t;

//TESTME
/**
 * @brief Viterbi problem for matching the bins of two normalised histograms
 * @remarks The algorithm
 * 	- Measurement: Bin frequency
 * 	- Measurement error: Absolute difference between the bin frequencies
 * 	- Constraints: Ordering, maximum distance in the normalised cumulative histogram
 * @remarks Usage notes:
 * 	- The algorithm is not symmetric: swapping the histograms will yield different results
 * @remarks The matching criteria is not the same as \c HistogramMatchingC
 * @ingroup Problems
 * @nosubgrouping
 */
class ViterbiNormalisedHistogramMatchingProblemC
{
	public:

		/**@name measurement_type description */ //@{
		typedef tuple<double, double> measurement_type;	///< A measurement. [pdf value; cdf value]
		static constexpr unsigned int iPDF=0;		///< Index of the PDF component of a measurement
		static constexpr unsigned int iCDF=1;		///< Index of the CDF component of a measurement
		//@}

	private:

		/**@name Configuration */ //@{
		typedef measurement_type	MeasurementT;
		vector<MeasurementT> stateList; ///< States, the reference histogram

		bool flagValid;	///< \c true if the object is valid
		double maxCDFDifference;	///< Maximum absolute difference between the CDF values for a valid bin match
		double minPDFValue;	///< If the PDF value for a state or a measurement is below this threshold, no valid match

		double minMeasurementProbability;	///< Minimum measurement probability
		//@}

	public:

		/** @name ViterbiProblemConceptC interface */ //@{
		ViterbiNormalisedHistogramMatchingProblemC();	///< Default constructor

		double ComputeMeasurementProbability(const measurement_type& measurement, unsigned int currentState) const;	///< Computes the probability that a measurement originates from a specified state
		double ComputeTransitionProbability(unsigned int source, unsigned int destination) const;	///< Computes the source->destination transition probability
		unsigned int GetStateCardinality() const;	///< Returns the number of states

		bool IsValid() const;	///< Returns \c flagValid

		static void InitialiseUpdate();	///< Initialises a measurement update
		static void FinaliseUpdate();	///< Finalises a measurement update
		//@}

		template<class HistogramT> ViterbiNormalisedHistogramMatchingProblemC(const HistogramT& histogram, double mmaxCDFDifference, double mminPDFValue);	///< Constructor

		double GetMinMeasurementProbability() const;	///< Returns the minimum measurement probability
};	//class ViterbiNormalisedHistogramMatchingProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @param[in] histogram Histogram
 * @param[in] mmaxCDFDifference Maximum CDF difference
 * @param[in] mminPDFValue If the PDF value for a state or a measurement is below this threshold, no valid match
 * @pre \c HistogramT is a forward range of floating point elements
 * @pre All entries of \c histogram are in [0,1]
 * @post The object is valid
 */
template<class HistogramT>
ViterbiNormalisedHistogramMatchingProblemC::ViterbiNormalisedHistogramMatchingProblemC(const HistogramT& histogram, double mmaxCDFDifference, double mminPDFValue) : maxCDFDifference(mmaxCDFDifference), minPDFValue(mminPDFValue)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<HistogramT>));
	static_assert(is_floating_point<typename range_value<HistogramT>::type >::value, "ViterbiNormalisedHistogramMatchingProblemC::ViterbiNormalisedHistogramMatchingProblemC : HistogramT must hold floating point elements" );

	flagValid=true;
	minMeasurementProbability=exp(-1);

	stateList.reserve(boost::distance(histogram));
	double currentCDF=0;
	for(auto current : histogram)
	{
		currentCDF+=current;
		stateList.emplace_back(current, currentCDF);

		assert(current>=0 && current<=1 && currentCDF<=1);	//Precondition
	}	//for(auto current : histogram)
}	//ViterbiNormalisedHistogramMatchingProblemC(const HistogramT& histogram, double mmaxCDFDifference)

}	//StateEstimationN
}	//SeeSawN

#endif /* VITERBI_NORMALISED_HISTOGRAM_MATCHING_PROBLEM_IPP_0681923 */
