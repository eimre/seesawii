/**
 * @file UKFOrientationTrackingProblem.h Public interface for \c UKFOrientationTrackingProblemC
 * @author Evren Imre
 * @date 22 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UKF_ORIENTATION_TRACKING_PROBLEM_H_7032412
#define UKF_ORIENTATION_TRACKING_PROBLEM_H_7032412

#include "UKFOrientationTrackingProblem.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{
class UKFOrientationTrackingProblemC;	///< Orientation tracking problem for UKF
struct UKFOrientationTrackingProblemStateDifferenceC;	///< Difference functor for the state for \c UKFOrientationTrackingProblemC
}	//StateEstimationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<double>;

#endif /* UKF_ORIENTATION_TRACKING_PROBLEM_H_7032412 */
