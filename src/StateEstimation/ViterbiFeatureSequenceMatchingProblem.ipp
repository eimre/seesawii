/**
 * @file ViterbiFeatureSequenceMatchingProblem.ipp Implementation of \c ViterbiFeatureSequenceMatchingProblemC
 * @author Evren Imre
 * @date 30 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_FEATURE_SEQUENCE_MATCHING_PROBLEM_IPP_5128381
#define VITERBI_FEATURE_SEQUENCE_MATCHING_PROBLEM_IPP_5128381

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <vector>
#include <cassert>
#include <cmath>
#include <type_traits>
#include <cstddef>
#include "../Elements/FeatureConcept.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::ForwardRangeConcept;
using boost::BinaryFunctionConcept;
using boost::range_value;
using boost::copy;
using std::vector;
using std::exp;
using std::is_same;
using std::size_t;
using SeeSawN::ElementsN::FeatureConceptC;

/**
 * @brief Viterbi problem for identifying the matching elements in two feature sequences
 * @tparam FeatureT A feature
 * @tparam DistanceT A feature distance metric
 * @pre \c FeatureT is a feature
 * @pre \c DistanceT is  a binary functor receiving two feature descriptors, and returning a floating point value
 * @remarks Use cases: Stereo matching with order constraint, trajectory alignment, trajectory matching
 * @ingroup Problem
 */
template<class FeatureT, class DistanceT>
class ViterbiFeatureSequenceMatchingProblemC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((FeatureConceptC<FeatureT>));
	BOOST_CONCEPT_ASSERT((BinaryFunctionConcept<DistanceT, double, typename FeatureT::descriptor_type, typename FeatureT::descriptor_type>));
	///@endcond

	private:

		/** @name Configuration */ //@{
		bool flagValid;	///< \c true if the problem is initialised
		vector<FeatureT> reference;	///< Reference sequence
		DistanceT distanceMetric;	///< Distance metric
		double maxDistance;	///< Maximum value of the feature distance
		double transitionProbability;	///< Transition probability
		//@}

	public:

		/** @name Constructors */ //@{
		template<class FeatureRangeT> ViterbiFeatureSequenceMatchingProblemC(const FeatureRangeT& rreference, const DistanceT& ddistanceMetric, double mmaxDistance, double ttransitionProbability);	///< Constructor
		//@}

		/** @name \c ViterbiProblemConceptC interface */ //@{

		typedef FeatureT measurement_type;	///< Measurement type

		ViterbiFeatureSequenceMatchingProblemC();	///< Default constructor

		double ComputeMeasurementProbability(const FeatureT& measurement, unsigned int stateId) const;	///< Computes the measurement probability
		double ComputeTransitionProbability(unsigned int sourceId, unsigned int destinationId) const;	///< Computes the transition probability

		unsigned int GetStateCardinality() const;	///< Returns the number of distinct states
		bool IsValid() const;	///< Returns \c flagValid

		void InitialiseUpdate();	///< Dummy function to satisfy the concept requirements
		void FinaliseUpdate();	///< Dummy function to satisfy the concept requirements
		//@}
};	//class ViterbiFeatureSequenceMatchingProblemT

/**
 * @brief Constructor
 * @tparam FeatureRangeT A range of measurements
 * @param[in] rreference Reference set
 * @param[in] ddistanceMetric Distance metric
 * @param[in] mmaxDistance Maximum distance
 * @param[in] ttransitionProbability Transition probability
 * @pre \c MeasurementRangeT is a forward range of elements of type \c FeatureT
 * @pre \c mmaxDistance>0
 * @pre \c ttransitionProbability>0
 * @post Valid object
 */
template<class FeatureT, class DistanceT>
template<class FeatureRangeT>
ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::ViterbiFeatureSequenceMatchingProblemC(const FeatureRangeT& rreference, const DistanceT& ddistanceMetric, double mmaxDistance, double ttransitionProbability) : flagValid(true), distanceMetric(ddistanceMetric), maxDistance(mmaxDistance), transitionProbability(ttransitionProbability)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
	static_assert(is_same<typename range_value<FeatureRangeT>::type, FeatureT>::value, "FeatureRangeT must hold elements of type FeatureT");
	assert(mmaxDistance>0);
	assert(ttransitionProbability>0);

	size_t sReference=boost::distance(rreference);
	reference.resize(sReference);
	copy(rreference, reference.begin());
}	//ViterbiFeatureSequenceMatchingProblemC(const MeasurementRangeT& rreference, const DistanceT& ddistanceMetric, double mmaxDistance, double ttransitionProbability)

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<class FeatureT, class DistanceT>
ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::ViterbiFeatureSequenceMatchingProblemC() : flagValid(false), maxDistance(1), transitionProbability(0)
{}

/**
 * @brief Computes the measurement probability
 * @param[in] measurement Measurement
 * @param[in] stateId State id
 * @return Measurement probability
 * @pre \c flagValid=true
 */
template<class FeatureT, class DistanceT>
double ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::ComputeMeasurementProbability(const FeatureT& measurement, unsigned int stateId) const
{
	assert(flagValid);
	assert(stateId<GetStateCardinality());

	return exp(-distanceMetric(reference[stateId].Descriptor(), measurement.Descriptor())/maxDistance);
}	//double ComputeMeasurementProbability(const FeatureT& measurement, unsigned int stateId)

/**
 * @brief Computes the transition probability
 * @param[in] sourceId Id of the source state
 * @param[in] destinationId Id of the destination state
 * @return Transition probability
 * @pre \c flagValid=true
 */
template<class FeatureT, class DistanceT>
double ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::ComputeTransitionProbability(unsigned int sourceId, unsigned int destinationId) const
{
	assert(flagValid);
	return sourceId<=destinationId ? transitionProbability : 0;
}	//double ComputeTransitionProbability(unsigned int sourceId, unsigned int targetId)

/**
 * @brief Returns the number of distinct states
 * @return Number of distinct states
 * @pre \c flagValid=true
 */
template<class FeatureT, class DistanceT>
unsigned int ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::GetStateCardinality() const
{
	assert(flagValid);
	return reference.size();
}	//unsigned int GetStateCardinality()

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
template<class FeatureT, class DistanceT>
bool ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Dummy function to satisfy the concept requirements
 */
template<class FeatureT, class DistanceT>
void ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::InitialiseUpdate()
{}

/**
 * @brief Dummy function to satisfy the concept requirements
 */
template<class FeatureT, class DistanceT>
void ViterbiFeatureSequenceMatchingProblemC<FeatureT, DistanceT>::FinaliseUpdate()
{}

}	//StateEstimationN
}	//SeeSawN

#endif /* VITERBIFEATURESEQUENCEMATCHINGPROBLEM_IPP_ */
