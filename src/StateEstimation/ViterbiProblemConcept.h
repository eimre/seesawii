/**
 * @file ViterbiProblemConcept.h Public interface for \c ViterbiProblemConceptC
 * @author Evren Imre
 * @date 30 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_PROBLEM_CONCEPT_H_9981232
#define VITERBI_PROBLEM_CONCEPT_H_9981232

#include "ViterbiProblemConcept.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{

template<class TestT> class ViterbiProblemConceptC;	///< Concept checker for Viterbi problems

}	//StateEstimationN
}	//SeeSawN

#endif /* VITERBIPROBLEMCONCEPT_H_ */
