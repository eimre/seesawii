/**
 * @file UKFCameraTrackingProblemBase.h Public interface for \c UKFCameraTrackingProblemBaseC
 * @author Evren Imre
 * @date 21 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UKF_CAMERA_TRACKING_PROBLEM_BASE_H_6429093
#define UKF_CAMERA_TRACKING_PROBLEM_BASE_H_6429093

#include "UKFCameraTrackingProblemBase.ipp"

namespace SeeSawN
{
namespace StateEstimationN
{
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS> class UKFCameraTrackingProblemBaseC;	///< Base class for UKF camera tracking problems
}	//StateEstimationN
}	//SeeSawN

#endif /* UKF_CAMERA_TRACKING_PROBLEM_BASE_H_6429093*/
