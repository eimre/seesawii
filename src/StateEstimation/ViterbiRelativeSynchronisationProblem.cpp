/**
 * @file ViterbiRelativeSynchronisationProblem.cpp Implementation of the Viterbi relative synchronisation problem
 * @author Evren Imre
 * @date 15 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ViterbiRelativeSynchronisationProblem.h"
namespace SeeSawN
{
namespace StateEstimationN
{

/**
 * @brief Propagates the correspondences across the tracks
 * @param[in] measurement Measurement
 * @param[in] matcherCorrespondences Feature correspondences from the matcher
 * @param[in] stateIndex State index
 * @return Tuple: Propagated correspondences and the error
 * @pre The object is valid
 * @pre \c measurement has a valid track id component
 * @pre \c epipolarConstraint is valid
 */
auto ViterbiRelativeSynchronisationProblemC::PropagateCorrespondences(const MeasurementT& measurement, const CorrespondenceListT& matcherCorrespondences, unsigned int stateIndex) -> tuple<CoordinateCorrespondenceList2DT, multiset<RealT> >
{
	//Preconditions
	assert(flagValid);
	assert(!!get<iTrackId>(measurement));
	assert(!!epipolarConstraint);

	tuple<CoordinateCorrespondenceList2DT, multiset<RealT> > output;

	//For each feature, check whether there is a track correspondence
	CorrespondenceListT propagated;
	list< map<size_t, pair<size_t, double> >::iterator> searchCache;	//Reference to the track correspondence entries for the propagated correspondences
	auto itE=trackCorrespondences.cend();
	auto itSE=get<iTrackId>(stateList[stateIndex])->right.end();
	for(const auto& current : get<iTrackId>(measurement)->left)	//If we are in this function, iTrackId denotes a valid component
	{
		size_t indexM=current.second;	//Track id
		auto itM = trackCorrespondences.find(indexM);

		if(itM==itE)
			continue;

		//A track correspondence exists. Now, find the corresponding feature index for the state
		size_t indexS=itM->second.first;
		auto itS = get<iTrackId>(stateList[stateIndex])->right.find(indexS);

		//Track does not exist in this frame
		if(itS==itSE)
			continue;

		propagated.push_back(CorrespondenceListT::value_type( current.first, itS->second ) );
		searchCache.push_back(itM);
	}	//for(const auto& current : get<iTrackId>(measurement).left)

	if(propagated.empty())
		return output;

	//Filter for existing correspondences
	set<tuple<size_t, size_t> > filter;
	for(const auto& current : matcherCorrespondences)
		filter.emplace(current.left, current.right);

	auto itFilterE=filter.cend();

	//Redundancy and error tests

	const FeatureListT& featureList1=get<iFeatureList>(measurement);
	const FeatureListT& featureList2=get<iFeatureList>(stateList[stateIndex]);
	const EpipolarSampsonConstraintT::error_type& errorMetric=epipolarConstraint->GetErrorMetric();

	for(const auto& current : MakeBinaryZipRange(propagated, searchCache))
	{
		//Is it already in?
		auto it=filter.find(make_tuple(boost::get<0>(current).left, boost::get<0>(current).right));
		if(it!=itFilterE)
			continue;

		//Is it an inlier?
		const Coordinate2DT& coordM=featureList1[boost::get<0>(current).left].Coordinate();
		const Coordinate2DT& coordS=featureList2[boost::get<0>(current).right].Coordinate();
		RealT error=errorMetric(coordM, coordS);

		//Update the track correspondence error
		if(boost::get<1>(current)->second.second>error)
			boost::get<1>(current)->second.second=error;

		if(error<=inlierTh)
		{
			get<0>(output).push_back(CoordinateCorrespondenceList2DT::value_type(coordM, coordS));
			get<1>(output).insert(error);
		};	//if(error<inlierTh)
	}	//for(const auto& current : propagated)

	return output;
}	//tuple<CoordinateCorrespondenceList2DT, vector<size_t>, vector<size_t> > PropagateCorrespondences(const MeasurementT& measurement, unsigned int stateIndex) const

/**
 * @brief Default constructor
 * @post Invalid object
 */
ViterbiRelativeSynchronisationProblemC::ViterbiRelativeSynchronisationProblemC() : flagValid(false), flagFixedCameras(false), flagPropagate(false), inlierTh(0), minCorrespondence(0), minMeasurementProbability(0), cMeasurement(0)
{}

/**
 * @brief Computes the probability that a measurement originates from a specified state
 * @param[in] measurement A frame belonging to the first (reference) sequence
 * @param[in] currentState A frame belonging to the target sequence
 * @pre \c flagValid=true
 * @pre \c currentState<stateList.size()
 * @pre If \c flagPropagate , \c measurement has a valid track id component
 * @post Modifies \c cache and \c epipolarConstraint
 */
double ViterbiRelativeSynchronisationProblemC::ComputeMeasurementProbability(const MeasurementT& measurement, unsigned int currentState)
{
	//Preconditions
	assert(flagValid);
	assert(currentState<stateList.size());
	assert(!flagPropagate || (flagPropagate && get<iTrackId>(measurement)));
	assert(!get<iTrackId>(measurement) || (get<iTrackId>(measurement) && get<iTrackId>(measurement)->size()==get<iFeatureList>(measurement).size()));

	//If there is a cache entry, reuse it
	optional<NodeT>& currentNode=cache[cMeasurement][currentState];	//This is guaranteed to exist

	if(currentNode)
		return get<iScore>(*currentNode);

	//Default cache entry, for all visited nodes
	currentNode=NodeT(); get<iScore>(*currentNode)=minMeasurementProbability;

	//If not enough features, quit
	if(min(get<iFeatureList>(measurement).size(),  get<iFeatureList>(stateList[currentState]).size()) < minCorrespondence)
		return minMeasurementProbability;

	//Else, compute the score

	//Epipolar constraint
	if(!epipolarConstraint || !flagFixedCameras )
	{
		EpipolarMatrixT mF=CameraToFundamental(get<iCamera>(measurement), get<iCamera>(stateList[currentState]));
		EpipolarSampsonConstraintT::error_type error(mF);
		epipolarConstraint=EpipolarSampsonConstraintT(error, inlierTh, 1);
	}	//if(!epipolarConstraint || !flagFixedCameras )

	//Matching problem
	typedef ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> MatchingProblemT;
	optional<MatchingProblemT> matchingProblem;

	//Correspondences between the dynamic features
	const FeatureListT& featureList1=get<iFeatureList>(measurement);
	const FeatureListT& featureList2=get<iFeatureList>(stateList[currentState]);

	CorrespondenceListT correspondences;
	FeatureMatcherC<MatchingProblemT>::Run(correspondences, matchingProblem, matcherParameters, featureSimilarityMetric, epipolarConstraint,featureList1, featureList2);	//Diagnostics are not needed

	//Propagation
	CoordinateCorrespondenceList2DT propagatedCorrespondences;
	multiset<RealT> errorArray;
	if(flagPropagate)
		std::tie(propagatedCorrespondences, errorArray)=PropagateCorrespondences(measurement, correspondences, currentState);

	size_t nCorrespondences=correspondences.size()+propagatedCorrespondences.size();

	//Termination: Matching returns too few correspondences. Matching failure is not an issue as long as there are correspondences!
	if(nCorrespondences<minCorrespondence)
		return minMeasurementProbability;

	//Convert to coordinate correspondences
	for(const auto& current : correspondences)
		get<iCorrespondence>(*currentNode).push_back(CoordinateCorrespondenceList2DT::value_type(featureList1[current.left].Coordinate(), featureList2[current.right].Coordinate()));

	for(const auto& current : propagatedCorrespondences)
		get<iCorrespondence>(*currentNode).push_back(current);

	//Compute the median error
	const EpipolarSampsonConstraintT::error_type& errorMetric=epipolarConstraint->GetErrorMetric();

	for(const auto& current : correspondences)
	{
		double corrError=errorMetric(featureList1[current.left].Coordinate(), featureList2[current.right].Coordinate());
		errorArray.insert(corrError);

		//Update the track correspondences
		if(flagPropagate && corrError<inlierTh)
		{
			size_t trackIdS= (*get<iTrackId>(stateList[currentState]))[current.right].right;
			size_t trackIdM= (*get<iTrackId>(measurement))[current.left].right;
			auto it=trackCorrespondences.emplace(trackIdM, make_pair(trackIdS, corrError));

			//If there is already an element, challenge
			if(!it.second && (it.first->second.second > corrError) )
				it.first->second=make_pair(trackIdS, corrError);
		}	//if(flagPropagate && corrError<inlierTh)
	}	//for(const auto& current : correspondences)

	get<iScore>(*currentNode)=exp(-lossMap(*next(errorArray.begin(), nCorrespondences/2)));	//unit of the loss is pixel^2. So this has a Gaussian "feel"*/

	return get<iScore>(*currentNode);

	/*
	CorrespondenceListT correspondences;
	multiset<RealT> errorArray;
	FeatureMatcherDiagnosticsC diagnostics;
	CorrespondenceListT propagatedCorrespondences;

	if(flagPropagate)
	{
		//Attempt to make use of the existing track correspondences
		vector<size_t> unassociatedS;
		vector<size_t> unassociatedM;

		std::tie(propagatedCorrespondences, unassociatedS, unassociatedM, errorArray)=PropagateCorrespondences(measurement, currentState);

		if(!unassociatedS.empty() && !unassociatedM.empty())
		{
			CorrespondenceListT correspondences2;
			diagnostics=FeatureMatcherC<MatchingProblemT>::Run(correspondences2, matchingProblem, matcherParameters, featureSimilarityMetric, epipolarConstraint, MakePermutationRange(featureList1, unassociatedM), MakePermutationRange(featureList2, unassociatedS) );

			//Adjust the indices
			for(const auto& current : correspondences2)
				correspondences.push_back(CorrespondenceListT::value_type(unassociatedM[current.left], unassociatedS[current.right]));
		}	//if(!unassociatedS.empty() && !unassociatedM.empty())
	}	//if(flagPropagate)
	else
		diagnostics=FeatureMatcherC<MatchingProblemT>::Run(correspondences, matchingProblem, matcherParameters, featureSimilarityMetric, epipolarConstraint,featureList1, featureList2);

	size_t nCorrespondences=correspondences.size()+propagatedCorrespondences.size();

	//Termination: Matching returns too few correspondences. Matching failure is not an issue as long as there are correspondences!
	if(nCorrespondences<minCorrespondence)
		return minMeasurementProbability;

	//Make a coordinate correspondence list
	for(const auto& current : correspondences)
		get<iCorrespondence>(*currentNode).push_back(CoordinateCorrespondenceList2DT::value_type(featureList1[current.left].Coordinate(), featureList2[current.right].Coordinate()));

	//Compute the median error
	const EpipolarSampsonConstraintT::error_type& errorMetric=epipolarConstraint->GetErrorMetric();

	for(const auto& current : correspondences)
	{
		double corrError=errorMetric(featureList1[current.left].Coordinate(), featureList2[current.right].Coordinate());
		errorArray.insert(corrError);

		//Update the track correspondences
		if(flagPropagate && corrError<inlierTh)
		{
			unsigned int trackIdS= (*get<iTrackId>(stateList[currentState]))[current.right].right;
			unsigned int trackIdM= (*get<iTrackId>(measurement))[current.left].right;
			auto it=trackCorrespondences.emplace(trackIdM, make_pair(trackIdS, corrError));

			//If there is already an element, challenge
			if(!it.second && (it.first->second.second > corrError) )
				it.first->second=make_pair(trackIdS, corrError);
		}	//if(flagPropagate && corrError<inlierTh)
	}	//for(const auto& current : correspondences)


	get<iScore>(*currentNode)=exp(-lossMap(*next(errorArray.begin(), nCorrespondences/2)));	//unit of the loss is pixel^2. So this has a Gaussian "feel"*/
/*
	//Add the propagated correspondences
	for(const auto& current : propagatedCorrespondences)
		get<iCorrespondence>(*currentNode).push_back(CoordinateCorrespondenceList2DT::value_type(featureList1[current.left].Coordinate(), featureList2[current.right].Coordinate()));
*/
	//Weighted total error
/*	RealT totalLoss=0;
	for(const auto& current : correspondences)
		totalLoss += lossMap(errorMetric(featureList1[current.left].Coordinate(), featureList2[current.right].Coordinate()));

	get<iScore>(*currentNode)=exp(-totalLoss/(nCorrespondences*sqrt(nCorrespondences) ) );


	return get<iScore>(*currentNode);*/
}	//double ComputeMeasurementProbability(const MeasurementT& measurement, unsigned int currentState)

/**
 * @brief Computes the source->destination transition probability
 * @param[in] source Source state
 * @param[in] destination Destination state
 * @return Transition probability. If the transition is permissible, 1. Otherwise 0
 * @pre \c flagValid=true
 */
double ViterbiRelativeSynchronisationProblemC::ComputeTransitionProbability(unsigned int source, unsigned int destination)
{
	//Preconditions
	assert(flagValid);
	return ((source<=destination) && in(destination, admissibleStateInterval) ? 1:0);
}	//double ComputeTransitionProbability(unsigned int source, unsigned int destination) const

/**
 * @brief Returns the number of states
 * @return \c stateList.size()
 * @pre \c flagValid=true
 */
unsigned int ViterbiRelativeSynchronisationProblemC::GetStateCardinality() const
{
	//Preconditions
	assert(flagValid);
	return stateList.size();
}	//unsigned int GetStateCardinality() const

/**
 * @brief Returns \c flagValid
 */
bool ViterbiRelativeSynchronisationProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Initialises a measurement update
 * @post State is modified
 * @pre \c flagValid=true
 */
void ViterbiRelativeSynchronisationProblemC::InitialiseUpdate()
{
	//Preconditions
	assert(flagValid);

	//Add an entry for the new measurement to the cache
	if(cache.find(cMeasurement)==cache.end())
		cache[cMeasurement]=NodeVectorT(GetStateCardinality());

	//Initialise the admissible state interval
	if(alphaRange && tauRange)
	{
		unsigned int lower = max(0.0, fma(alphaRange->lower(), cMeasurement, tauRange->lower()) );
		unsigned int upper = max(0.0, fma(alphaRange->upper(), cMeasurement, tauRange->upper()) );
		admissibleStateInterval=interval<unsigned int>(lower, upper);
	}
	else
		admissibleStateInterval=interval<unsigned int>(0, GetStateCardinality());
}	//void InitialiseUpdate()

/**
 * @brief Finalises a measurement update
 * @post State is modified
 * @pre \c flagValid=true
 */
void ViterbiRelativeSynchronisationProblemC::FinaliseUpdate()
{
	//Preconditions
	assert(flagValid);
	++cMeasurement;
}	//void FinaliseUpdate()

/**
 * @brief Returns the minimum measurement probability
 * @return \c minMeasurementProbability
 */
auto ViterbiRelativeSynchronisationProblemC::GetMinMeasurementProbability() const -> RealT
{
	return minMeasurementProbability;
}	//RealT GetMinMeasurementProbability() const

/**
 * @brief Returns a constant reference to \c cache
 * @return \c cache
 */
auto ViterbiRelativeSynchronisationProblemC::GetCache() const -> const NodeArrayT&
{
	return cache;
}	//const NodeArrayT& GetCache() const

/**
 * @brief Modifies \c bounds
 * @param[in] newBounds New bounds
 * @pre \c flagValid=true
 * @post \c constraint is modified as well
 */
void ViterbiRelativeSynchronisationProblemC::SetBounds(const tuple<IntervalT, IntervalT>& newBounds)
{
	//Preconditions
	assert(flagValid);

	alphaRange=get<0>(newBounds);
	tauRange=get<1>(newBounds);
}	//void SetBounds(const tuple<LineT, LineT>& newBounds)

/**
 * @brief Resets \c cMeasurement
 * @post \c cMeasurement=0
 */
void ViterbiRelativeSynchronisationProblemC::ResetMeasurementCounter()
{
	cMeasurement=0;
}	//void ResetMeasurementCounter()

/**
 * @brief Clears the state
 * @post The problem and the configuration are still valid
 */
void ViterbiRelativeSynchronisationProblemC::Clear()
{
	cMeasurement=0;
	cache.clear();

	trackCorrespondences.clear();
	//admissibleStateInterval is rebuilt prior to the update. So, no need to clear
}	//void Clear()

/********** EXPLICIT INSTANTIATION **********/
template ViterbiRelativeSynchronisationProblemC::ViterbiRelativeSynchronisationProblemC(const vector<ViterbiRelativeSynchronisationProblemC::measurement_type>&, const optional<tuple<ViterbiRelativeSynchronisationProblemC::interval_type, ViterbiRelativeSynchronisationProblemC::interval_type>>&,  bool, const FeatureMatcherParametersC&, double, size_t, bool);

}	//StateEstimationN
}	//SeeSawN

