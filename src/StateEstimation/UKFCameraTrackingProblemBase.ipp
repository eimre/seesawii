/**
 * @file UKFCameraTrackingProblemBase.ipp Implementation of \c UKFCameraTrackingProblemBaseC
 * @author Evren Imre
 * @date 21 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UKF_CAMERA_TRACKING_PROBLEM_BASE_IPP_6239073
#define UKF_CAMERA_TRACKING_PROBLEM_BASE_IPP_6239073
#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <utility>
#include <vector>
#include "../UncertaintyEstimation/MultivariateGaussian.h"
#include "../Geometry/GeometrySolverConcept.h"
#include "../Wrappers/BoostRange.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::optional;
using Eigen::Matrix;
using std::false_type;
using std::vector;
using std::get;
using SeeSawN::UncertaintyEstimationN::MultivariateGaussianC;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::WrappersN::MakeBinaryZipRange;

/**
 * @brief Base class for UKF camera tracking problems
 * @tparam UKFProblemT A UKF problem for camera calibration estimation
 * @tparam GeometrySolverT A geometry solver
 * @tparam StateDifferenceFunctorT Subtraction operation for state
 * @tparam MeasurementDifferenceFunctorT Subtraction operation for the measurement
 * @tparam DIMS Dimensionality of the state vector
 * @pre \c StateDifferenceFunctorT is an adaptable binary function
 * @pre \c GeometrySolverT is a model of \c GeometrySolverConceptC
 * @ingroup Problem
 * @nosubgrouping
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
class UKFCameraTrackingProblemBaseC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<StateDifferenceFunctorT, typename StateDifferenceFunctorT::result_type, typename StateDifferenceFunctorT::first_argument_type, typename StateDifferenceFunctorT::second_argument_type> ));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<GeometrySolverT>));
	///@endcond

	private:

		typedef typename GeometrySolverT::real_type RealT;	///< Floating point type
		static constexpr unsigned int sMeasurement=GeometrySolverT::DoF();	///< Dimensionality of a measurement

	protected:

		typedef GeometrySolverT geometry_solver_type;	///< Type of the geometry solver

	public:

		/** @name UnscentedKalmanFilterProblemConceptC interface */ //@{
		typedef MultivariateGaussianC<StateDifferenceFunctorT, DIMS, RealT> state_type;	///< Type of the state
		typedef typename StateDifferenceFunctorT::result_type state_vector_type;	///< Type of the state vector
		typedef typename state_type::covariance_type state_covariance_type;	///< Type of the state covariance matrix
		typedef typename StateDifferenceFunctorT::first_argument_type state_sample_type;	///< Type of a state sigma point

		typedef typename GeometrySolverT::uncertainty_type measurement_type;	///< Type of the measurement
		typedef typename MeasurementDifferenceFunctorT::result_type measurement_vector_type;	///< Type of the measurement vector
		typedef typename measurement_type::covariance_type measurement_covariance_type;	///< Type of the measurement covariance matrix
		typedef typename MeasurementDifferenceFunctorT::first_argument_type measurement_sample_type;	///< Type of a measurement sigma point

		typedef Matrix<RealT, DIMS, sMeasurement> cross_covariance_type;	///< Type of the state-innovation cross-covariance matrix

		typedef false_type can_decompose_state_covariance;
		typedef false_type can_decompose_measurement_covariance;
		typedef false_type can_decompose_process_covariance;

		static constexpr unsigned int StateDimensionality();	///< Returns the dimensionality of the state
		static constexpr unsigned int MeasurementDimensionality();	///< Returns the dimensionality of the measurements

		static const state_covariance_type& GetStateCovariance(const state_type& state);	///< Returns a constant reference to the state covariance
		static const measurement_covariance_type& GetMeasurementCovariance(const measurement_type& measurement);	///< Returns a constant reference to the measurement covariance

		static measurement_vector_type ComputeInnovationVector(const measurement_type& observation, const measurement_type& prediction);	///< Computes the innovation vector
		static optional<cross_covariance_type> ComputeCrossCovariance(const state_type& state, const vector<state_sample_type>& stateSigmaSet, const measurement_type& measurement, const vector<measurement_sample_type>& measurementSigmaSet, RealT wCovarianceCentral, RealT wPeripheral);	///< Computes the state-innovation cross-covariance matrix
		//@}
};	//class UKFCameraTrackingProblemBaseC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Returns the dimensionality of the state
 * @return \c DIMS
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
constexpr unsigned int UKFCameraTrackingProblemBaseC<UKFProblemT, GeometrySolverT, StateDifferenceFunctorT, MeasurementDifferenceFunctorT, DIMS>::StateDimensionality()
{
	return DIMS;
}	//unsigned int StateDimensionality()

/**
 * @brief Returns the dimensionality of the measurements
 * @return \c sMeasurement
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
constexpr unsigned int UKFCameraTrackingProblemBaseC<UKFProblemT, GeometrySolverT, StateDifferenceFunctorT, MeasurementDifferenceFunctorT, DIMS>::MeasurementDimensionality()
{
	return sMeasurement;
}	//unsigned int MeasurementDimensionality()

/**
 * @brief Returns a constant reference to the state covariance
 * @param[in] state State
 * @return A constant reference to the state covariance
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
auto UKFCameraTrackingProblemBaseC<UKFProblemT, GeometrySolverT, StateDifferenceFunctorT, MeasurementDifferenceFunctorT, DIMS>::GetStateCovariance(const state_type& state) -> const state_covariance_type&
{
	return state.GetCovariance();
}	//const state_covariance_type& GetStateCovariance(const state_type& state)

/**
 * @brief Returns a constant reference to the measurement covariance
 * @param[in] measurement Measurement
 * @return A constant reference to the measurement covariance
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
auto UKFCameraTrackingProblemBaseC<UKFProblemT, GeometrySolverT, StateDifferenceFunctorT, MeasurementDifferenceFunctorT, DIMS>::GetMeasurementCovariance(const measurement_type& measurement) -> const measurement_covariance_type&
{
	return measurement.GetCovariance();
}	//const measurement_covariance_type& GetMeasurementCovariance(const measurement_type& measurement)

/**
 * @brief Computes the innovation vector
 * @param[in] observation Observed measurement
 * @param[in] prediction Predicted measurement
 * @return Innovation vector
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
auto UKFCameraTrackingProblemBaseC<UKFProblemT, GeometrySolverT, StateDifferenceFunctorT, MeasurementDifferenceFunctorT, DIMS>::ComputeInnovationVector(const measurement_type& observation, const measurement_type& prediction) -> measurement_vector_type
{
	MeasurementDifferenceFunctorT subtractor;
	return subtractor(observation.GetMean(), prediction.GetMean());
}	//measurement_vector_type ComputeInnovationVector(const measurement_type& observation, const measurement_type& prediction)

/**
 * @brief Computes the state-innovation cross-covariance matrix
 * @param[in] state State
 * @param[in] stateSigmaSet Sigma points for the state
 * @param[in] measurement Measurement
 * @param[in] measurementSigmaSet Sigma points for the measurement
 * @param[in] wCovarianceCentral Weight of the central sample
 * @param[in] wPeripheral Weights of the peripheral samples
 * @return Cross covariance matrix
 */
template<class UKFProblemT, class GeometrySolverT, class StateDifferenceFunctorT, class MeasurementDifferenceFunctorT, unsigned int DIMS>
auto UKFCameraTrackingProblemBaseC<UKFProblemT, GeometrySolverT, StateDifferenceFunctorT, MeasurementDifferenceFunctorT, DIMS>::ComputeCrossCovariance(const state_type& state, const vector<state_sample_type>& stateSigmaSet, const measurement_type& measurement, const vector<measurement_sample_type>& measurementSigmaSet, RealT wCovarianceCentral, RealT wPeripheral) ->optional<cross_covariance_type>
{
	//Extract the means
	state_vector_type stateMean=state.GetMean();
	measurement_vector_type measurementMean=measurement.GetMean();

	//Compute the cross-covariance
	cross_covariance_type acc=cross_covariance_type::Zero();
	StateDifferenceFunctorT stateSubtractor;
	MeasurementDifferenceFunctorT measurementSubtractor;

	unsigned int index=0;
	for(const auto& current : MakeBinaryZipRange(stateSigmaSet, measurementSigmaSet))
	{
		state_vector_type difS=stateSubtractor(get<0>(current), stateMean);
		measurement_vector_type difM=measurementSubtractor(get<1>(current), measurementMean);
		acc+= (index==0 ? wCovarianceCentral : wPeripheral)*(difS*difM.transpose());
		++index;
	}	//for(const auto& current : MakeBinaryZipRange(stateSigmaSet, measurementSigmaSet))

	optional<cross_covariance_type> output=acc;
	return output;
}	//optional<CrossCovarianceT> ComputeCrossCovariance(const StateT& state, const vector<StateSigmaT>& stateSigmaSet, const MeasurementT& measurement, const vector<MeasurementSigmaT>& measurementSigmaSet)

}	//StateEstimationN
}	//SeeSawN

#endif /* UKFCAMERATRACKINGPROBLEMBASE_IPP_ */
