/**
 * @file ViterbiNormalisedHistogramMatchingProblem.cpp Implementation of \c ViterbiNormalisedHistogramMatchingProbemC
 * @author Evren Imre
 * @date 13 Feb 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include <iostream>
#include "ViterbiNormalisedHistogramMatchingProblem.h"
namespace SeeSawN
{
namespace StateEstimationN
{

/**
 * @brief Default constructor
 * @post The object is invalid
 */
ViterbiNormalisedHistogramMatchingProblemC::ViterbiNormalisedHistogramMatchingProblemC() : flagValid(false), maxCDFDifference(0), minPDFValue(0), minMeasurementProbability(0)
{}

/**
 * @brief Computes the probability that a measurement originates from a specified state
 * @param[in] measurement Measurement
 * @param[in] currentState Current state
 * @return Measurement probability
 * @pre The object is valid
 * @pre Both members of \c measurement are in [0,1]
 * @pre \c currentState is a valid state index
 */
double ViterbiNormalisedHistogramMatchingProblemC::ComputeMeasurementProbability(const measurement_type& measurement, unsigned int currentState) const
{
	//Preconditions
	assert(flagValid);
	assert(get<iPDF>(measurement)>=0 && get<iPDF>(measurement)<=1);
	assert(get<iCDF>(measurement)>=0 && get<iCDF>(measurement)<=1);
	assert(currentState<stateList.size());

	if( get<iPDF>(measurement)<minPDFValue || get<iPDF>(stateList[currentState])<minPDFValue )
		return minMeasurementProbability;

	if(fabs(get<iCDF>(measurement)-get<iCDF>(stateList[currentState]))>maxCDFDifference)
		return minMeasurementProbability;

	return max(minMeasurementProbability, exp(-fabs(get<iPDF>(measurement)-get<iPDF>(stateList[currentState])) ) );
}	//double ComputeMeasurementProbability(const measurement_type& measurement, unsigned int currentState)

/**
 * @brief Computes the source->destination transition probability
 * @param[in] source Source state
 * @param[in] destination Destination state
 * @return 1 if the ordering constraint is satisfied. Else 0
 * @pre The object is valid
 */
double ViterbiNormalisedHistogramMatchingProblemC::ComputeTransitionProbability(unsigned int source, unsigned int destination) const
{
	assert(flagValid);
	return (source<=destination) ? 1:0;	//Ordering constraint
}	//double ComputeTransitionProbability(unsigned int source, unsigned int destination)

/**
 * @brief Returns the number of states
 * @return Number of states
 * @pre The object is valid
 */
unsigned int ViterbiNormalisedHistogramMatchingProblemC::GetStateCardinality() const
{
	assert(flagValid);
	return stateList.size();
}	//unsigned int GetStateCardinality() const

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool ViterbiNormalisedHistogramMatchingProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid() const

/**
 * @brief Initialises a measurement update
 */
void ViterbiNormalisedHistogramMatchingProblemC::InitialiseUpdate()
{}

/**
 * @brief Finalises a measurement update
 */
void ViterbiNormalisedHistogramMatchingProblemC::FinaliseUpdate()
{}

/**
 * @brief Returns the minimum measurement probability
 * @return Returns \c minMeasurementProbability
 */
double ViterbiNormalisedHistogramMatchingProblemC::GetMinMeasurementProbability() const
{
	return minMeasurementProbability;
}	//double GetMinMeasurementProbability() const

}	//StateEstimationN
}	//SeeSawN

