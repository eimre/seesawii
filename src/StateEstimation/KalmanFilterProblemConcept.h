/**
 * @file KalmanFilterProblemConcept.h Public interface for \c KalmanFilterProblemConceptC
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KALMAN_FILTER_PROBLEM_CONCEPT_H_8912032
#define KALMAN_FILTER_PROBLEM_CONCEPT_H_8912032

#include "KalmanFilterProblemConcept.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{

template<class TestT> class KalmanFilterProblemConceptC;	///< Concept definition and checker for Kalman filter problems

}	//StateEstimationN
}	//SeeSawN

#endif /* KALMAN_FILTER_PROBLEM_CONCEPT_H_8912032 */
