/**
 * @file KFSimpleProblemBase.h Public interface for \c KFSimpleProblemBaseC
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KF_SIMPLE_PROBLEM_BASE_H_9012075
#define KF_SIMPLE_PROBLEM_BASE_H_9012075

#include "KFSimpleProblemBase.ipp"
namespace SeeSawN
{
namespace StateEstimationN
{

template<int DIMS, int DIMM, typename RealT> class KFSimpleProblemBaseC;	///< Base for Kalman filter problems conforming to the generic formulation

}	//StateEstimationN
}	//SeeSawN

#endif /* KF_SIMPLE_PROBLEM_BASE_H_9012075 */
