/**
 * @file KFVectorMeasurementFusionProblem.ipp Implementation of \c KFVectorFusionMeasurementProblemC
 * @author Evren Imre
 * @date 14 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KF_VECTOR_MEASUREMENT_FUSION_PROBLEM_IPP_4398214
#define KF_VECTOR_MEASUREMENT_FUSION_PROBLEM_IPP_4398214

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include "../Wrappers/EigenMetafunction.h"
#include "KFSimpleProblemBase.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using Eigen::Matrix;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::StateEstimationN::KFSimpleProblemBaseC;

/**
 * @brief Kalman filter problem for fusing a set of vector measurements
 * @tparam VectorT Type of the vector state/measurements
 * @ingroup Problem
 */
template<class VectorT>
class KFVectorMeasurementFusionProblemC : public KFSimpleProblemBaseC< RowsM<VectorT>::value, RowsM<VectorT>::value, typename ValueTypeM<VectorT>::type >
{
	private:

		typedef KFSimpleProblemBaseC<RowsM<VectorT>::value, RowsM<VectorT>::value, typename ValueTypeM<VectorT>::type > BaseT;

		typedef typename BaseT::state_type GaussianT;	///< State and measurement Gaussian type
		typedef typename BaseT::state_covariance_type CovarianceT;	///< State, measurement and innovation covariance tpye
		typedef typename BaseT::propagation_jacobian_type JacobianT;	///< Measurement and propagation jacoian type

	public:

		/** @name KalmanFilterProblemConceptC interface */ //@{
		static optional<VectorT> PropagateStateMean(const GaussianT& state);	///< Propagates the state mean
		static optional<JacobianT> ComputePropagationJacobian(const GaussianT& state);	///< Computes the Jacobian for the state propagation function

		static optional<GaussianT> PredictMeasurement(const GaussianT& state);	///< Predicts the next measurement
		static optional<JacobianT> ComputeMeasurementFunctionJacobian(const GaussianT& state);	///< Computes the Jacobian for the measurement function
		//@}
};	//class KFVectorMeasurementFusionProblemC

/**
 * @brief Propagates the state mean
 * @param[in] state State
 * @return Mean of \c state
 */
template<class VectorT>
optional<VectorT> KFVectorMeasurementFusionProblemC<VectorT>::PropagateStateMean(const GaussianT& state)
{
	return optional<VectorT>(state.GetMean());
}	//optional<VectorT> PropagateStateMean(const VectorT& stateMean)

/**
 * @brief Computes the Jacobian for the state propagation function
 * @param[in] state State
 * @return Identity
 */
template<class VectorT>
auto KFVectorMeasurementFusionProblemC<VectorT>::ComputePropagationJacobian(const GaussianT& state) -> optional<JacobianT>
{
	return optional<JacobianT>(JacobianT::Identity(state.GetMean().rows(), state.GetMean().rows()));
}	//optional<propagation_jacobian_type> ComputePropagationJacobian(const VectorT& stateMean)

/**
 * @brief Predicts the next measurement
 * @param[in] state State
 * @return \c state
 */
template<class VectorT>
auto KFVectorMeasurementFusionProblemC<VectorT>::PredictMeasurement(const GaussianT& state) -> optional<GaussianT>
{
	return optional<GaussianT>(state);
}	//optional<VectorT> PredictMeasurement(const state_type& stateMean)

/**
 * @brief Computes the Jacobian for the measurement function
 * @param[in] state State
 * @return Identity
 */
template<class VectorT>
auto KFVectorMeasurementFusionProblemC<VectorT>::ComputeMeasurementFunctionJacobian(const GaussianT& state) -> optional<JacobianT>
{
	return optional<JacobianT>(JacobianT::Identity(state.GetMean().rows(), state.GetMean().rows()));
}	//optional<propagation_jacobian_type> ComputePropagationJacobian(const VectorT& stateMean)


}	//StateEstimationN
}	//SeeSawN

#endif /* KF_VECTOR_MEASUREMENT_FUSION_PROBLEM_IPP_4398214 */
