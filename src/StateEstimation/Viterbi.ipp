/**
 * @file Viterbi.ipp Implementation of the Viterbi algorithm
 * @author Evren Imre
 * @date 30 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_IPP_9022312
#define VITERBI_IPP_9022312

#include <boost/concept_check.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/algorithm.hpp>
#include <omp.h>
#include <algorithm>
#include <vector>
#include <list>
#include <tuple>
#include <cmath>
#include <climits>
#include <stdexcept>
#include <iterator>
#include "../Wrappers/BoostRange.h"
#include "ViterbiProblemConcept.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using boost::adjacency_list;
using boost::listS;
using boost::directedS;
using boost::property_map;
using boost::property;
using boost::no_property;
using boost::vertex_name_t;
using boost::edge_weight_t;
using boost::vertex_name;
using boost::edge_weight;
using boost::num_vertices;
using boost::graph_traits;
using boost::add_vertex;
using boost::add_edge;
using boost::out_edges;
using boost::target;
using boost::range::transform;
using std::vector;
using std::list;
using std::tuple;
using std::get;
using std::any_of;
using std::log;
using std::min;
using std::max;
using std::min_element;
using std::invalid_argument;
using std::numeric_limits;
using std::back_inserter;
using SeeSawN::WrappersN::MakeBinaryZipRange;
using SeeSawN::StateEstimationN::ViterbiProblemConceptC;

/**
 * @brief Parameters for the Viterbi algorithm
 * @ingroup Parameters
 */
struct ViterbiParametersC
{
	unsigned int nThreads;	///< Number of threads
	bool flagMeasurementFirst;	///< If \c true , the algorithm computes the measurement probabilities first. Else, it starts with the state transition probabilities

	ViterbiParametersC() : nThreads(1), flagMeasurementFirst(false)
	{}

};	//struct ViterbiParametersC

/**
 * @brief Viterbi algorithm
 * @tparam ProblemT A Viterbi problem
 * @pre \c ProblemT is a model of \c ViterbiProblemConceptC
 * @remarks Multi-threaded operation should be reserved for the cases where measurement or transition probability computations are very expensive
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class ViterbiC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((ViterbiProblemConceptC<ProblemT>));
	///@endcond

	private:

		typedef typename ProblemT::measurement_type MeasurementT;	///< A measurement

		typedef adjacency_list<listS, listS, directedS, property<vertex_name_t, int>, property<edge_weight_t, double> > GraphT;	///< Type of the graph
		typedef typename graph_traits<GraphT>::vertex_descriptor VertexDescriptorT;	///< A vertex descriptor
		typedef typename graph_traits<GraphT>::edge_descriptor EdgeDescriptorT;	///< An edge descriptor

		/** @name Node definition */ //@{

		typedef tuple<int, VertexDescriptorT, double> NodeT;	///< A node
		static constexpr int iState=0;	///< Index of the state
		static constexpr int iVertex=1;	///< Index of the vertex
		static constexpr int iScore=2;	///< Index of the score

		static constexpr int rootId=-1;	///< Id of the entry node
		//@}

		/** @name Configuration */ //@{
		ViterbiParametersC parameters;	///< Parameters
		//@}

		/** @name State */ //@{
		GraphT trellis;	///< Trellis graph
		list<NodeT> prevLayer;	///< Previous layer added to the trellis
		//@}

	public:

		/** @name Constructors */ //@{
		ViterbiC();	///< Default constructor
		ViterbiC(const ViterbiParametersC& pparameters);	///< Constructor
		//@}

		/** @name Operations */ //@{
		bool Update(ProblemT& problem, const MeasurementT& measurement);	///< Updates the trellis graph
		list<unsigned int> GetOptimalSequence();	///< Returns the optimal path through the trellis graph
		//@}

		/** @name Utility */ //@{
		void Clear();	///< Clears the state
		//@}
};	//class ViterbiC

/**
 * @brief Default constructor
 */
template<class ProblemT>
ViterbiC<ProblemT>::ViterbiC()
{}

/**
 * @brief Constructor
 * @param[in] pparameters Parameters
 */
template<class ProblemT>
ViterbiC<ProblemT>::ViterbiC(const ViterbiParametersC& pparameters) : parameters(pparameters)
{
	parameters.nThreads= min(omp_get_max_threads(), max(1, (int)parameters.nThreads));
}	//ViterbiC(const ViterbiParametersC& pparameters)

/**
 * @brief Updates the trellis graph
 * @param[in, out] problem Problem
 * @param[in] measurement Measurement
 * @return \c false if after the update, no feasible paths remain (i.e., the graph is broken into two components)
 * @post \c trellis is modified if the update is successful
 * @throws invalid_argument If \c problem is not valid
 */
template<class ProblemT>
bool ViterbiC<ProblemT>::Update(ProblemT& problem, const MeasurementT& measurement)
{
	if(!problem.IsValid())
		throw(invalid_argument("ViterbiC::Update : Invalid problem"));

	//If the trellis is empty, insert the entry node
	if(num_vertices(trellis)==0)
	{
		VertexDescriptorT vertexId=add_vertex(rootId, trellis);
		prevLayer.emplace_back(rootId,vertexId,0);	//Root node
	}	//if(num_vertices(trellis)==0)

	problem.InitialiseUpdate();	//Notify the problem

	//For all states
	vector<double> transitionProbability(prevLayer.size());
	size_t nStates=problem.GetStateCardinality();	//Number of distinct states
	list<NodeT> currentLayer;
	unsigned int c2;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c2) firstprivate(transitionProbability) num_threads(parameters.nThreads)
	for(c2=0; c2<nStates; ++c2)
	{
		//Measurement probability
		double measurementProbability=0;
		if(parameters.flagMeasurementFirst)
			measurementProbability=problem.ComputeMeasurementProbability(measurement, c2);

		//Transition probability from the previous layer

		bool flagReachable=false;
		if(!parameters.flagMeasurementFirst || measurementProbability>0 )
		{
			//Compute the transition probabilities
			transform(prevLayer, transitionProbability.begin(), [&](const NodeT& current){return (get<iState>(current)==rootId) ? 1 : problem.ComputeTransitionProbability(get<iState>(current), c2);});

			//If it is possible to reach the current state, compute the measurement probability
			flagReachable=any_of(transitionProbability.begin(), transitionProbability.end(), [](double v){return v>0;} );

			//If the measurement probability is not computed yet, do it now
			if(measurementProbability==0 && flagReachable)
				measurementProbability= problem.ComputeMeasurementProbability(measurement, c2);
		}	//if(!parameters.flagMeasurementFirst || measurementProbability>0 )

		//If the state cannot be reached, or the measurement is impossible, continue
		if(!flagReachable || measurementProbability<=0)
			continue;

		//Else, update the trellis
		double measurementScore= (measurementProbability==1) ? 0  : -log(measurementProbability);	// Negative log-likelihood as score. Avoid log if you can

		//Find the best predecessor
		NodeT bestNode;
		double bestScore=numeric_limits<double>::infinity();
		double bestTransitionScore=0;
		for(const auto& current : MakeBinaryZipRange(prevLayer, transitionProbability) )
		{
			//If an impossible transition, skip
			if(get<1>(current)==0)
				continue;

			double transitionScore=(get<1>(current)==1) ? 0 : -log(get<1>(current));	//Transition component of the score. Negative log-likelihood. Avoid log if you can
			double aggScore=transitionScore + get<iScore>(get<0>(current));	//Aggregate score

			if(aggScore < bestScore)
			{
				bestScore=aggScore;
				bestTransitionScore=transitionScore;
				bestNode=get<0>(current);
			}	//if(aggScore > bestScore)
		}	//for(const auto& current : MakeBinaryZipRange(prevLayer, transitionProbability) )

		//Insert the current state as a new vertex, and link it with its best predecessor

	#pragma omp critical(Vi_Up_1)
	{
		VertexDescriptorT vertexId=add_vertex(c2, trellis);
		add_edge(vertexId, get<iVertex>(bestNode), bestTransitionScore+measurementScore, trellis);
		currentLayer.emplace_back(c2, vertexId, bestScore+measurementScore);
	}	//#pragma omp critical(V_Up_1)
	}	//for(size_t c=0; c<nStates; ++c)

	problem.FinaliseUpdate();	//Notifty the problem

	//If currentLayer is empty, none of the state elements are modified
	if(currentLayer.empty())
		return false;

	prevLayer.swap(currentLayer);

	return true;
}	//bool Update(const ProblemT& problem, const MeasurementT& measurement)

/**
 * @brief  Returns the optimal path through the trellis graph
 * @return Optimal state sequence through the trellis
 */
template<class ProblemT>
list<unsigned int>  ViterbiC<ProblemT>::GetOptimalSequence()
{
	list<unsigned int> output;

	//If no measurements yet
	if(prevLayer.empty())
		return output;

	//Find the best terminal vertex
	auto comparator=[](const NodeT& node1, const NodeT& node2){return get<iScore>(node1)<get<iScore>(node2);};
	NodeT terminal=*min_element(prevLayer.begin(), prevLayer.end(), comparator);

	//Backtrack until root

	VertexDescriptorT currentVertex=get<iVertex>(terminal);
	int currentState=get<iState>(terminal);
	typename property_map<GraphT, vertex_name_t>::type vertexToStateMap=get(vertex_name, trellis);	// Maps the vertex ids to the states

	while(currentState!=rootId)
	{
		output.push_front(currentState);	//Update

		//Next vertex
		auto edge=out_edges(currentVertex, trellis);	//There is only one edge
		currentVertex=target(*edge.first, trellis);
		currentState=vertexToStateMap[currentVertex];
	}	//while(vertexToStateMap[currentVertex]!=rootId)

	return output;
}	//optional<vector<unsigned int> > GetPath()

/**
 * @brief Clears the state
 * @post The state parameters are reset
 */
template<class ProblemT>
void ViterbiC<ProblemT>::Clear()
{
	trellis.clear();
	prevLayer.clear();
}	//void Clear()

}	//StateEstimationN
}	//SeeSawN

#endif /* VITERBI_IPP_9022312 */
