/**
 * @file UnscentedKalmanFilterProblemConcept.ipp Implementation of \c UnscentedKalmanFilterProblemConceptC
 * @author Evren Imre
 * @date 20 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UNSCENTED_KALMAN_FILTER_PROBLEM_CONCEPT_IPP_7213904
#define UNSCENTED_KALMAN_FILTER_PROBLEM_CONCEPT_IPP_7213904

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include <cstddef>

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using std::vector;
using std::size_t;
using std::tie;
using std::tuple;

/**
 * @brief Concept definition and checker for UKF problems
 * @tparam TestT Type to be tested
 * @ingroup Concept
 * @nosubgrouping
 */
template<class TestT>
class UnscentedKalmanFilterProblemConceptC
{
	///@cond CONCEPT_CHECK
	public:

		BOOST_CONCEPT_USAGE(UnscentedKalmanFilterProblemConceptC)
		{

			typedef typename TestT::state_type StateT;
			typedef typename TestT::state_vector_type StateVectorT;
			typedef typename TestT::state_covariance_type StateCovarianceT;
			typedef typename TestT::measurement_type MeasurementT;
			typedef typename TestT::measurement_vector_type MeasurementVectorT;
			typedef typename TestT::measurement_covariance_type MeasurementCovarianceT;
			typedef typename TestT::cross_covariance_type CrossCovarianceT;

			typedef typename TestT::state_sample_type StateSampleT;
			typedef typename TestT::measurement_sample_type MeasurementSampleT;

			TestT tested;

			bool flagValid=tested.IsValid(); (void)flagValid;

			size_t sState=tested.StateDimensionality(); (void)sState;
			size_t sMeasurement=tested.MeasurementDimensionality(); (void)sMeasurement;

			StateT state;
			VectorXd perturbationP;
			VectorXd perturbationQ;
			optional<StateT> propagated=tested.ApplyPropagationFunction(state, perturbationP, perturbationQ);

			VectorXd perturbationR;
			optional<tuple<StateSampleT, MeasurementSampleT> > sigmaPair=tested.ApplyMeasurementFunction(state, perturbationP, perturbationR);

			typename TestT::can_decompose_state_covariance dummy1; (void)dummy1;
			typename TestT::can_decompose_measurement_covariance dummy2; (void)dummy2;
			typename TestT::can_decompose_process_covariance dummy3; (void)dummy3;

			StateCovarianceT mP=tested.GetStateCovariance(state); (void)mP;

			MeasurementT measurement;
			MeasurementCovarianceT mR=tested.GetMeasurementCovariance(measurement); (void)mR;

			vector<MeasurementSampleT> measurementSigmaSet;
			double wMean0;
			double wCov0;
			double wPeripheral;
			optional<MeasurementT> predictedMeasurement=tested.ComputeMeasurement(measurementSigmaSet, wMean0, wCov0, wPeripheral);

			vector<StateSampleT> stateSigmaSet;
			optional<StateT> predictedState=tested.ComputeState(stateSigmaSet, wMean0, wCov0, wPeripheral);

			optional<CrossCovarianceT> mPxy=tested.ComputeCrossCovariance(state, stateSigmaSet, *predictedMeasurement, measurementSigmaSet, wCov0, wPeripheral);

			MeasurementVectorT innovationVector=tested.ComputeInnovationVector(measurement, *predictedMeasurement);

			StateVectorT deltaMean;
			StateCovarianceT deltaCovariance;
			bool flagUpdate=tested.Update(state, deltaMean, deltaCovariance); (void)flagUpdate;

			//Optional functions
			/*
            StateCovarianceT mQ;
			StateCovarianceT mPs=tested.DecomposeStateCovariance(mP);
			StateCovarianceT mQs=tested.DecomposeProcessCovariance(mQ);
			MeasurementCovarianceT mR=tested.DecomposeMeasurementCovariance(mR);
			 */

		}	//BOOST_CONCEPT_USAGE(UnscentedKalmanFilterProblemConceptC)
	///@endcond
};	//class UnscentedKalmanFilterProblemConceptC

}	//StateEstimatorN
}	//SeeSawN

#endif /* UNSCENTED_KALMAN_FILTER_PROBLEM_CONCEPT_IPP_7213904 */
