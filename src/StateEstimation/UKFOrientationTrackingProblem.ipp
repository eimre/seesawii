/**
 * @file UKFOrientationTrackingProblem.ipp Implementation of \c UKFOrientationTrackingProblemC
 * @author Evren Imre
 * @date 22 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef UKF_ORIENTATION_TRACKING_PROBLEM_IPP_6512980
#define UKF_ORIENTATION_TRACKING_PROBLEM_IPP_6512980

#include <boost/optional.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include <cmath>
#include <cstddef>
#include "../Elements/GeometricEntity.h"
#include "../Geometry/Rotation3DSolver.h"
#include "../Geometry/Rotation.h"
#include "UKFCameraTrackingProblemBase.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using boost::math::constants::pi;
using Eigen::Matrix;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::vector;
using std::min;
using std::size_t;
using SeeSawN::StateEstimationN::UKFCameraTrackingProblemBaseC;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::GeometryN::Rotation3DMinimalDifferenceC;
using SeeSawN::GeometryN::RotationVectorToQuaternion;
using SeeSawN::GeometryN::QuaternionToRotationVector;
using SeeSawN::GeometryN::ComputeQuaternionSampleStatistics;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::AxisAngleT;
using SeeSawN::ElementsN::QuaternionT;

/**
 * @brief Difference functor for the state for \c UKFOrientationTrackingProblemC
 * @ingroup Problem
 * @nosubgrouping
 */
struct UKFOrientationTrackingProblemStateDifferenceC
{
	private:
		typedef Matrix<typename Rotation3DSolverC::real_type,6,1> StateVectorT;

	public:
		/** @name Adaptable binary function interface */ //@{
		typedef StateVectorT first_argument_type;
		typedef StateVectorT second_argument_type;
		typedef StateVectorT result_type;	///< Difference in vector form

		result_type operator()(const first_argument_type& op1, const second_argument_type& op2);	///< Computes the difference between operands
	//@}
};	//struct UKFOrientationTrackingProblemStateDifferenceC

/**
 * @brief Orientation tracking problem for UKF
 * @remarks E. Kraft, "A Quaternion-based Unscented Kalman Filter for Orientation Tracking, " 6th International Conference on Information Fusion, 8-10 July 2003, Cairns, Australia, pp. 47-54
 * @remarks F. L. Markley, "Attitude Error Representations for Kalman Filtering, " Journal of Guidance, Control, and Dynamics, Vol. 26, No. 2, pp. 311-317 March 2003
 * @ingroup Problem
 * @nosubgrouping
 */
class UKFOrientationTrackingProblemC : public UKFCameraTrackingProblemBaseC<UKFOrientationTrackingProblemC, Rotation3DSolverC, UKFOrientationTrackingProblemStateDifferenceC, Rotation3DMinimalDifferenceC, 6>
{
	private:

		typedef UKFCameraTrackingProblemBaseC<UKFOrientationTrackingProblemC, Rotation3DSolverC, UKFOrientationTrackingProblemStateDifferenceC, Rotation3DMinimalDifferenceC, 6> BaseT;	///< Type of the base
		typedef typename BaseT::state_type StateT;	///< Type of the state
		typedef typename BaseT::state_vector_type StateVectorT;	///< Type of the state vector
		typedef typename BaseT::state_covariance_type StateCovarianceT;	///< Type of the state covariance

		typedef typename BaseT::measurement_type MeasurementT;	///< Type of the measurements
		typedef typename BaseT::measurement_vector_type MeasurementVectorT;	///< Type of the measurement vector
		typedef typename BaseT::measurement_covariance_type MeasurementCovarianceT;	///< Type of the measurement covariance matrix

		typedef typename BaseT::state_sample_type StateSigmaT;	///< Type of a state sigma point
		typedef typename BaseT::measurement_sample_type MeasurementSigmaT;	///< Type of a measurement sigma point

		/**@name Layout of the state vector */ //@{
		static constexpr unsigned int iOrientation=0;
		static constexpr unsigned int iRotation=3;
		//@}

		static StateVectorT PerturbState(const StateT& state, const StateVectorT& perturbation);	///< Perturbs the state

	public:

		/**@name \c UnscentedKalmanFilterProblemConceptC */ //@{
		static bool IsValid();	///< Returns \c true if the object is valid

		static optional<StateT> ApplyPropagationFunction(const StateT& state, const StateVectorT& deltaP, const StateVectorT& deltaQ);	///< Perturbs and propagates the state
		static optional<tuple<StateSigmaT, MeasurementSigmaT> > ApplyMeasurementFunction(const StateT& state, const StateVectorT& deltaP, const MeasurementVectorT& deltaR);	///< Perturbs and applies the measurement function

		static optional<StateT> ComputeState(const vector<StateSigmaT>& sigmaSet, double wMeanCentral, double wCovarianceCentral, double wPeripheral);	///< Computes the state corresponding to a sigma point set
		static optional<MeasurementT> ComputeMeasurement(const vector<MeasurementSigmaT>& sigmaSet, double wMeanCentral, double wCovarianceCentral, double wPeripheral);	///< Computes the measurement corresponding to a sigma point set

		static bool Update(StateT& state, const StateVectorT& deltaMean, const StateCovarianceT& deltaCovariance);	///< Updates the state
		//@}


};	//class UKFOrientationTrackingProblemC

}	//StateEstimationN
}	//SeeSawN

#endif /* UKFORIENTATIONTRACKINGPROBLEM_IPP_ */
