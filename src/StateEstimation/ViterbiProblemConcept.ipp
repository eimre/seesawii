/**
 * @file ViterbiProblemConcept.ipp Implementation of \c ViterbiProblemConceptC
 * @author Evren Imre
 * @date 30 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VITERBI_PROBLEM_CONCEPT_IPP_6919923
#define VITERBI_PROBLEM_CONCEPT_IPP_6919923

#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace StateEstimationN
{

/**
 * @brief  Concept checker for Viterbi problems
 * @tparam TestT Type to be tested
 * @ingroup Concept
 * @nosubgrouping
 */
template<class TestT>
class ViterbiProblemConceptC
{
	///@cond CONCEPT_CHECK

	public:

		BOOST_CONCEPT_USAGE(ViterbiProblemConceptC)
		{
			TestT tested;

			bool flagValid=tested.IsValid(); (void)flagValid;

			typedef typename TestT::measurement_type MeasurementT;
			MeasurementT* pMeasurement;

			unsigned int currentState;
			double probMeasurement=tested.ComputeMeasurementProbability(*pMeasurement, currentState); (void)probMeasurement;

			unsigned int sourceState;
			double probTransition=tested.ComputeTransitionProbability(sourceState,currentState); (void)probTransition;

			unsigned int nState=tested.GetStateCardinality(); (void)nState;

			tested.InitialiseUpdate();
			tested.FinaliseUpdate();
		}	//BOOST_CONCEPT_USAGE(ViterbiProblemConceptC)

	///@endcond
};	//class ViterbiProblemConceptC

}	//StateEstimationN
}	//SeeSawN

#endif /* VITERBIPROBLEMCONCEPT_IPP_ */
