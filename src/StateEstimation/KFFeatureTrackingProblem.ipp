/**
 * @file KFFeatureTrackingProblem.ipp Implementation of \c KFFeatureTrackingProblemC
 * @author Evren Imre
 * @date 12 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef KF_FEATURE_TRACKING_PROBLEM_IPP_1709302
#define KF_FEATURE_TRACKING_PROBLEM_IPP_1709302

#include <boost/optional.hpp>
#include <type_traits>
#include <cstddef>
#include "KFSimpleProblemBase.h"

namespace SeeSawN
{
namespace StateEstimationN
{

using boost::optional;
using std::is_floating_point;
using std::size_t;
using SeeSawN::StateEstimationN::KFSimpleProblemBaseC;

/**
 * @brief Kalman filter problem for feature tracking
 * @tparam DIMM Dimensionality of the measurement
 * @tparam RealT floating point type
 * @ingroup Problem
 * @nosubgrouping
 */
template<unsigned int DIMM, typename RealT>
class KFFeatureTrackingProblemC : public KFSimpleProblemBaseC<2*DIMM, DIMM, RealT>
{
	static_assert(is_floating_point<RealT>::value, "KFFeatureTrackingProblemC: RealT must be a floating point value.");

	private:

		typedef KFSimpleProblemBaseC<2*DIMM, DIMM, RealT> BaseT;	///< Type of the  base

		typedef typename BaseT::state_type StateT;	///< Type of the state
		typedef typename BaseT::state_vector_type StateVectorT;	///< Type of the state vector
		typedef typename BaseT::propagation_jacobian_type PropagationJacobianT;	///< Type of the Jacobian for the propagation function
		typedef typename BaseT::measurement_type MeasurementT;	///< Type of a measurement
		typedef typename BaseT::measurement_jacobian_type MeasurementJacobianT;	///< Type of the Jacobian of the measurement function


	public:

		/** @name KalmanFilterProblemConceptC interface */ //@{
		static optional<StateVectorT> PropagateStateMean(const StateT& state);	///< Propagates the state mean
		static optional<PropagationJacobianT> ComputePropagationJacobian(const StateT& state);	///< Computes the Jacobian for the state propagation function

		static optional<MeasurementT> PredictMeasurement(const StateT& state);	///< Predicts the next measurement
		static optional<MeasurementJacobianT> ComputeMeasurementFunctionJacobian(const StateT& state);	///< Computes the Jacobian for the measurement function
		//@}
};	//class KFFeatureTrackingProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Propagates the state mean
 * @param[in] state State
 * @return Propagated state mean
 */
template<unsigned int DIMM, typename RealT>
auto KFFeatureTrackingProblemC<DIMM, RealT>::PropagateStateMean(const StateT& state) -> optional<StateVectorT>
{
	optional<StateVectorT> output(state.GetMean());
	output->head(DIMM)+=output->tail(DIMM);
	return output;
}	//optional<StateVectorT> PropagateStateMean(const StateT& state)

/**
 * @brief Computes the Jacobian for the state propagation function
 * @param[in] state State
 * @return Jacobian of the propagation function
 */
template<unsigned int DIMM, typename RealT>
auto KFFeatureTrackingProblemC<DIMM, RealT>::ComputePropagationJacobian(const StateT& state) -> optional<PropagationJacobianT>
{
	optional<PropagationJacobianT> mJ(PropagationJacobianT::Identity());
	mJ->diagonal(DIMM).setConstant(1);

	return mJ;
}	//optional<PropagationJacobianT> ComputePropagationJacobian(const StateT& state)

/**
 * @brief Predicts the next measurement
 * @param[in] state State
 * @return Predicted measurement
 */
template<unsigned int DIMM, typename RealT>
auto KFFeatureTrackingProblemC<DIMM, RealT>::PredictMeasurement(const StateT& state) -> optional<MeasurementT>
{
	return optional<MeasurementT>( MeasurementT(state.GetMean().head(DIMM), state.GetCovariance().topLeftCorner(DIMM,DIMM)));
}	//optional<MeasurementT> PredictMeasurement(const StateT& state);

/**
 * @brief Computes the Jacobian for the measurement function
 * @param[in] state State
 * @return Jacobian of the measurement function
 */
template<unsigned int DIMM, typename RealT>
auto KFFeatureTrackingProblemC<DIMM, RealT>::ComputeMeasurementFunctionJacobian(const StateT& state) -> optional<MeasurementJacobianT>
{
	return optional<MeasurementJacobianT>(MeasurementJacobianT::Identity());
}	//optional<MeasurementJacobianT> ComputeMeasurementFunctionJacobian(const StateT& state)

}	//StateEstimationN
}	//SeeSawN

#endif /* KF_FEATURE_TRACKING_PROBLEM_IPP_1709302 */
