/**
 * @file FeatureTracker.h Public interface for \c FeatureTrackerC
 * @author Evren Imre
 * @date 1 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_TRACKER_H_5125321
#define FEATURE_TRACKER_H_5125321

#include "FeatureTracker.ipp"
namespace SeeSawN
{
namespace FeatureTrackerN
{

template<class ProblemT> class FeatureTrackerC;	///< Feature tracker
struct FeatureTrackerParametersC;	///< Parameter obejct for \c FeatureTrackerC
struct FeatureTrackerDiagnosticsC;	///< Diagnostics object for \c FeatureTrackerC


}	//FeatureTrackerN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template std::string boost::lexical_cast<std::string, int>(const int&);
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
extern template class std::map<unsigned int, unsigned int>;
extern template class std::vector<unsigned int>;
extern template class std::array<double,2>;
extern template class std::set<unsigned int>;
#endif /* FEATURE_TRACKER_H_5125321 */
