/**
 * @file ImageFeatureTrackingPipeline.h Public interface for \c ImageFeatureTrackingPipelineC
 * @author Evren Imre
 * @date 16 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_TRACKING_PIPELINE_H_5801203
#define IMAGE_FEATURE_TRACKING_PIPELINE_H_5801203

#include "ImageFeatureTrackingPipeline.ipp"
namespace SeeSawN
{
namespace TrackerN
{

class ImageFeatureTrackingPipelineC;	///< Image feature tracking pipeline
struct ImageFeatureTrackingPipelineParametersC;	///< Parameters for \c ImageFeatureTrackingPipelineC
struct ImageFeatureTrackingPipelineDiagnosticsC;	///< Diagnostics for \c ImageFeatureTrackingPipelineC

}	//TrackerN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<unsigned int>;


#endif /* IMAGE_FEATURE_TRACKING_PIPELINE_H_5801203 */
