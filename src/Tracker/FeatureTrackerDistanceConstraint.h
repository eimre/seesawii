/**
 * @file FeatureTrackerDistanceConstraint.h Public interface for \c FeatureTrackerDistanceConstraintC
 * @author Evren Imre
 * @date 7 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_TRACKER_DISTANCE_CONSTRAINT_H_6709012
#define FEATURE_TRACKER_DISTANCE_CONSTRAINT_H_6709012

#include "FeatureTrackerDistanceConstraint.ipp"
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace TrackerN
{

template<typename RealT, unsigned int DIM> class FeatureTrackerDistanceConstraintC;	///< Constraint for the distance between a predicted track and a measurement candidate

/********** EXPLICIT INSTANTIATIONS **********/
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::ValueTypeM;
extern template class FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2>;
typedef FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2> ImageFeatureTrackerDistanceConstraintT;

extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic> FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2>::BatchConstraintEvaluation(const vector<Coordinate2DT>&, const vector<Coordinate2DT>&) const;
extern template FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2>::FeatureTrackerDistanceConstraintC(const vector<ImageFeatureTrackerDistanceConstraintT::kernel_type>&, ValueTypeM<Coordinate2DT>::type, unsigned int, unsigned int);
}	//TrackerN
}	//SeeSawN

#endif /* FEATURE_TRACKER_DISTANCE_CONSTRAINT_H_6709012 */
