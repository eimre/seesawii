/**
 * @file ImageFeatureTrackingProblem.cpp Implementation of \c ImageFeatureTrackingProblemC
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ImageFeatureTrackingProblem.h"
namespace SeeSawN
{
namespace TrackerN
{

/**
 * @brief Returns a constant reference to \c kfProblem
 * @return A constant reference to \c kfProblem
 */
auto ImageFeatureTrackingProblemC::KalmanProblem() const -> const KFProblemT&
{
	return kfProblem;
}	//kalman_problem_type KalmanProblem()

/**
 * @brief Extracts the position component from a state
 * @param[in] state State
 * @return Position component of \c state
 */
Coordinate2DT ImageFeatureTrackingProblemC::GetPosition(const StateT& state) const
{
	return kfProblem.GetStateMean(state).head(2);
}	//Coordinate2DT GetPosition(const StateT& state)

/**
 * @brief Returns a constant reference to \c mQ
 * @return A constant reference to \c mQ
 */
auto ImageFeatureTrackingProblemC::ProcessNoise() const -> const ProcessNoiseT&
{
	return mQ;
}	//const ProcessNoiseT& ProcessNoise() const

/**
 * @brief Returns the initial state for a track to be created at \c position
 * @param[in] position Initial position of the track
 * @return Kalman filter state describing the track
 */
auto ImageFeatureTrackingProblemC::MakeInitialState(const Coordinate2DT& position) const -> StateT
{
	KFProblemT::state_vector_type stateVector; stateVector<<position[0], position[1],0,0;

	KFProblemT::state_covariance_type stateCovariance=mQi;
	stateCovariance.topLeftCorner(2,2)+=mR;

	return StateT(stateVector, stateCovariance);	//MultivariateGaussianC
}	//StateT MakeInitialState(const Coordinate2DT& position) const

/**
 * @brief Computes the predicted measurement
 * @param[in] state State
 * @return A tuple: Position; Covariance. Invalid, if the measurement prediction fails
 */
auto ImageFeatureTrackingProblemC::PredictMeasurement(const StateT& state) const -> optional<tuple<Coordinate2DT, MeasurementNoiseT> >
{
	optional<tuple<Coordinate2DT, MeasurementNoiseT> > output;

	//Predicted measurement
	optional<KFProblemT::measurement_type> measurement=kfProblem.PredictMeasurement(state);
	if(!measurement)
		return output;

	//Add the measurement noise
	MeasurementNoiseT mCov=kfProblem.GetMeasurementCovariance(*measurement)+mR;	//Innovation covariance
	return make_tuple(measurement->GetMean(), mCov);
}	//tuple<Coordinate2DT, MeasurementNoiseT> PredictMeasurement(const StateT& state) const

/**
 * @brief Makes a measurement from an image coordinate
 * @param[in] position Image coordinates
 * @param[in] covariance Coordinate covariance matrix. If invalid, \c mR is used
 * @return A Kalman filter measurement
 */
auto ImageFeatureTrackingProblemC::MakeMeasurement(const Coordinate2DT& position, const optional<MeasurementNoiseT>& covariance ) const -> MeasurementT
{
	return MeasurementT(position, covariance ? *covariance:mR);
}	//MeasurementT MakeMeasurement(const Coordinate2DT& position )

/**
 * @brief Returns a constant reference to \c similarity
 * @return A constant reference to \c similarity
 */
auto ImageFeatureTrackingProblemC::SimilarityMetric() const -> const SimilarityT&
{
	return similarity;
}	//const SimilarityT& SimilarityMetric() const

/**
 * @brief Computes a measurement noise term for the predictions from the actual measurement covariances
 * @param[in] covarianceList A vector of covariance measurements. Dummy.
 * @return \c mR
 * @remarks For the image feature tracking problems, the measurement covariance is identical for all points
 */
auto ImageFeatureTrackingProblemC::ComputeMeasurementNoise(const vector<MeasurementNoiseT>& covarianceList) const -> MeasurementNoiseT
{
	return mR;
}	//MeasurementNoiseT ComputeMeasurementNoise(const vector<MeasurementNoiseT>& covarianceList) const

/**
 * @brief Returns \c true if the object is initialised
 * @return \c flagValid
 */
bool ImageFeatureTrackingProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid() const

/**
 * @brief Default constructor
 * @post Invalid object
 */
ImageFeatureTrackingProblemC::ImageFeatureTrackingProblemC() : flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] mmQ Process noise
 * @param[in] mmQi Process noise for a new track
 * @param[in] mmR Measurement noise
 * @pre \c mmQ and mmQi are positive semi-definite matrices (unenforced)
 * @pre \c mmR is a positive semi-definite matrix (unenforced). A positive definite matrix is preferable to guarantee stable operation
 * @post The object is valid
 */
ImageFeatureTrackingProblemC::ImageFeatureTrackingProblemC(const ProcessNoiseT &mmQ, const ProcessNoiseT& mmQi, const MeasurementNoiseT& mmR) : flagValid(true), mQ(mmQ), mQi(mmQi), mR(mmR)
{
	kfProblem=KFProblemT();
	similarity=InverseEuclideanIDConverterT(EuclideanDistanceIDT(), ReciprocalC<EuclideanDistanceIDT::result_type>());
}	//ImageFeatureTrackingProblemC(const ProcessNoiseT &mmQ, const ProcessNoiseT& mmQi, const MeasurementNoiseT& mmR)

}	//TrackerN
}	//SeeSawN

