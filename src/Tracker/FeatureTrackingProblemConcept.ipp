/**
 * @file FeatureTrackingProblemConcept.ipp
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_TRACKING_PROBLEM_CONCEPT_IPP_7092303
#define FEATURE_TRACKING_PROBLEM_CONCEPT_IPP_7092303

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <tuple>
#include "../Elements/Coordinate.h"
#include "../Elements/FeatureConcept.h"
#include "../StateEstimation/KalmanFilterProblemConcept.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"

namespace SeeSawN
{
namespace TrackerN
{

using boost::optional;
using std::tuple;
using SeeSawN::StateEstimationN::KalmanFilterProblemConceptC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::ElementsN::FeatureConceptC;

/**
 * @brief Feature tracking problem concept checker
 * @tparam TestT Type to be tested
 * @ingroup Concept
 * @nosubgrouping
 */
template<class TestT>
class FeatureTrackingProblemConceptC
{
	///@cond CONCEPT_CHECK
	public:

		BOOST_CONCEPT_USAGE(FeatureTrackingProblemConceptC)
		{
			TestT tested;

			typedef typename TestT::kalman_problem_type KalmanProblemT;
			BOOST_CONCEPT_ASSERT((KalmanFilterProblemConceptC<KalmanProblemT>));

			typedef typename TestT::feature_type FeatureT;
			BOOST_CONCEPT_ASSERT((FeatureConceptC<FeatureT>));
			FeatureT* pFeature; (void)pFeature;

			typedef typename TestT::time_stamp_type TimeStampT;
			TimeStampT timeStamp; (void)timeStamp;

			typedef typename TestT::rtree_point_type PointT;
			PointT* point; (void)point;

			typedef typename TestT::matching_problem_type MatchingProblemT;
			BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<MatchingProblemT>));
			MatchingProblemT* pMatchingProblem; (void)pMatchingProblem;

			typedef typename MatchingProblemT::similarity_type SimilarityT;

			KalmanProblemT kalmanProblem=tested.KalmanProblem(); (void)kalmanProblem;

			typename FeatureT::coordinate_type position=tested.GetPosition(typename KalmanProblemT::state_type());

			typename KalmanProblemT::state_covariance_type mQ=tested.ProcessNoise(); (void)mQ;
			typename KalmanProblemT::state_type state=tested.MakeInitialState(position);

			optional<tuple<typename FeatureT::coordinate_type, typename KalmanProblemT::innovation_covariance_type> > measurement=tested.PredictMeasurement(state); (void)measurement;

			typename KalmanProblemT::measurement_type kalmanMeasurement=tested.MakeMeasurement(position); (void)kalmanMeasurement;

			SimilarityT similarity=tested.SimilarityMetric();	(void)similarity;

			typename KalmanProblemT::innovation_covariance_type mR=tested.ComputeMeasurementNoise(vector<typename KalmanProblemT::innovation_covariance_type>() ); (void)mR;

			bool flagValid=tested.IsValid(); (void)flagValid;
		}	//BOOST_CONCEPT_USAGE(FeatureTrackingProblemConceptC)


	///@endcond
};	//class FeatureTrackingProblemConceptC

}	//TrackerN
}	//SeeSawN

#endif /* FEATURETRACKINGPROBLEMCONCEPT_IPP_ */
