/**
 * @file ImageFeatureTrackingPipeline.cpp Implementation of \c ImageFeatureTrackingPipelineC
 * @author Evren Imre
 * @date 16 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ImageFeatureTrackingPipeline.h"

namespace SeeSawN
{
namespace TrackerN
{

/**
 * @brief Validates the parameters
 * @throws invalid_argument If any parameter values are invalid
 */
void ImageFeatureTrackingPipelineC::ValidateParameters()
{
	if(parameters.noiseVariance<=0)
		throw(invalid_argument(string("ImageFeatureTrackingPipelineC::ValidateParameters : parameters.noiseVariance must be positive. Value:")+lexical_cast<string>(parameters.noiseVariance)));

	if(parameters.pRejection<0 || parameters.pRejection>=1)
		throw(invalid_argument(string("ImageFeatureTrackingPipelineC::ValidateParameters : parameters.pRejection must be in [0,1). Value:")+lexical_cast<string>(parameters.pRejection)));

	if(parameters.predictionErrorNoise<0)
		throw(invalid_argument(string("ImageFeatureTrackingPipelineC::ValidateParameters : parameters.predictionErrorNoise must be non-negative. Value:")+lexical_cast<string>(parameters.predictionErrorNoise)));

	if(parameters.predictionFailureTh<0 || parameters.predictionFailureTh>1)
		throw(invalid_argument(string("ImageFeatureTrackingPipelineC::ValidateParameters : parameters.predictionFailureTh must be in [0,1]. Value:")+lexical_cast<string>(parameters.predictionFailureTh)));

	parameters.matcherParameters.flagBucketFilter=false;
	parameters.matcherParameters.flagStatic=true;
	parameters.matcherParameters.nnParameters.neighbourhoodSize12=1;
	parameters.matcherParameters.nnParameters.neighbourhoodSize21=1;

	parameters.homographyParameters.flagVerbose=false;
	parameters.homographyParameters.flagCovariance=false;
	parameters.homographyParameters.matcherParameters.nnParameters.neighbourhoodSize12=1;
	parameters.homographyParameters.matcherParameters.nnParameters.neighbourhoodSize21=1;
	parameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagHistory=false;
	parameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagHistory=false;
	parameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxSolution=1;
}	//void VerifyParameters()

/**
 * @brief Applies the distance constraint to identify the static features
 * @param[in] featureList Measurements
 * @return  A tuple: Diagnostics object for the geometry estimator, and the correspondences
 */
tuple<FeatureMatcherDiagnosticsC, CorrespondenceListT> ImageFeatureTrackingPipelineC::ApplyDistanceConstraint(const RAFeatureRangeT& featureList) const
{
	ReciprocalC<EuclideanDistanceIDT::result_type> mapper;
	InverseEuclideanIDConverterT similarityMetric(EuclideanDistanceIDT(), mapper);

	//Distance constraint can be interpreted as one-sided transfer error (H=I)
	double inlierTh=TransferErrorH2DT::ComputeOutlierThreshold(parameters.pRejection, 2*parameters.noiseVariance);	//Noise on both coordinates
	EuclideanDistanceConstraint2DT constraint(EuclideanDistance2DT(), inlierTh, parameters.matcherParameters.nThreads);

	typedef ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT> MatchingProblemT;
	optional<MatchingProblemT> matchingProblem;

	typedef FeatureMatcherC<MatchingProblemT> MatcherT;
	CorrespondenceListT correspondences;
	FeatureMatcherDiagnosticsC diagnostics=MatcherT::Run(correspondences, matchingProblem, parameters.homographyParameters.matcherParameters, similarityMetric, constraint, prevFeatureList, featureList);

	return make_tuple(diagnostics, correspondences);
}	//FeatureMatcherDiagnosticsC ApplyDistanceConstraint(CorrespondenceListT& correspondences, const vector<ImageFeatureC>& featureList) const

/**
 * @brief Applies the homography constraint to identify the static features
 * @param[in] featureList Measurements
 * @pre \c featureList is not empty
 * @pre \c prevFeatureList is not empty
 * @return A tuple: Diagnostics object for the geometry estimator, the correspondences, and the estimated homography
 * @post \c binSize1 is modified
 */
tuple<GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, optional<Homography2DT> > ImageFeatureTrackingPipelineC::ApplyHomographyConstraint(const RAFeatureRangeT& featureList)
{
	//Preconditions
	assert(!featureList.empty());
	assert(!prevFeatureList.empty());

	//Compute the RANSAC bin size for the measurements
	vector<Coordinate2DT> coordinateList=MakeCoordinateVector(featureList);
	RANSACHomography2DProblemT::dimension_type2 binSize2=*ComputeBinSize<Coordinate2DT>(coordinateList, parameters.homographyParameters.binDensity2);	//Cannot fail, as featureList is non-empty

	//This function cannot be called before the second frame. In this first call, binSize1 is uninitialised
	if(!binSize1)
	{
		vector<Coordinate2DT> prevCoordinateList=MakeCoordinateVector(prevFeatureList);
		binSize1=ComputeBinSize<Coordinate2DT>(prevCoordinateList, parameters.homographyParameters.binDensity1);	//Cannot fail, as prevFeatureList is non-empty
	}

	unsigned int loGeneratorSize1=max(Homography2DPipelineProblemT::GetMinimalGeneratorSize(), parameters.loGeneratorSize1);
	unsigned int loGeneratorSize2=max(Homography2DPipelineProblemT::GetMinimalGeneratorSize(), parameters.loGeneratorSize2);

	RANSACHomography2DProblemT::data_container_type dummyList;
	RANSACHomography2DProblemT ransacProblem1=MakeRANSACHomography2DProblem(dummyList, dummyList, parameters.noiseVariance, parameters.pRejection, 0, loGeneratorSize1, *binSize1, binSize2);
	RANSACHomography2DProblemT ransacProblem2=MakeRANSACHomography2DProblem(dummyList, dummyList, parameters.noiseVariance, parameters.pRejection, 0, loGeneratorSize2, *binSize1, binSize2);

	PDLHomography2DProblemT optimisationProblem=MakePDLHomography2DProblem(Homography2DT::Zero(), dummyList, optional<MatrixXd>());

	optional<Homography2DT> initialEstimate=prevH;	//If no valid prediction, prevH is invalid
	Homography2DEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	Homography2DPipelineProblemT homographyProblem=MakeGeometryEstimationPipelineHomography2DProblem(prevFeatureList, featureList, geometryEstimationProblem, parameters.noiseVariance, parameters.pRejection, initialEstimate, parameters.noiseVariance+parameters.predictionErrorNoise, parameters.pRejection, parameters.homographyParameters.matcherParameters.nThreads);

	Homography2DPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	Homography2DT model;
	optional<MatrixXd> covariance;
	CorrespondenceListT correspondences;
	GeometryEstimationPipelineDiagnosticsC diagnostics=Homography2DPipelineT::Run(model, covariance, correspondences, homographyProblem, rng, parameters.homographyParameters);

	optional<Homography2DT> mH;
	if(diagnostics.flagSuccess)
		mH=model;

	binSize1=binSize2;
	return make_tuple(diagnostics, correspondences, mH);
}	//GeometryEstimationPipelineDiagnosticsC ApplyHomographyConstraint(CorrespondenceListT& correspondences, const vector<ImageFeatureC>& featureList) const

/**
 * @brief Updates the homography prediction
 * @param[in] mH Estimated homography. Invalid, if the estimator failed
 * @param[in] correspondences Inlier list for \c mH
 * @return \c false if the prediction fails
 * @post If the prediction is successful, \c prevH and \c prevInlierCount are updated . Else, they are invalidated
 */
bool ImageFeatureTrackingPipelineC::UpdatePredictedHomography(const optional<Homography2DT>& mH, const CorrespondenceListT& correspondences)
{
	//If the homography estimation failed, no success
	if(!mH)
		return false;

	bool flagSuccess= !prevH;	//If no prediction, success
	flagSuccess |= prevInlierCount==0;	//Prevents a division by 0
	flagSuccess |= (double)correspondences.size()/(prevInlierCount)>=parameters.predictionFailureTh; //There is a valid homography and a prediction. Check the prediction quality

	prevH = flagSuccess ? mH : optional<Homography2DT>();
	prevInlierCount = flagSuccess ? correspondences.size() :0;

	return flagSuccess;
}	//bool UpdatePredictedHomography(const optional<Homography2DT>& mH, const CorrespondenceListT& correspondences)

/**
 * @brief Identifies the static features
 * @param[in,out] diagnostics Diagnostics object
 * @param[in] featureList Feature list
 * @return A bitset indicating the indices of the static features in \c featureList
 */
dynamic_bitset<> ImageFeatureTrackingPipelineC::IdentifyStaticFeatures(ImageFeatureTrackingPipelineDiagnosticsC& diagnostics, const vector<ImageFeatureC>& featureList)
{
	//Extract the associated measurements
	vector<unsigned int> dummy2;
	vector<unsigned int> associatedIds;	//Ids of the associated measurements
	tie(dummy2, associatedIds)=BimapToVector<unsigned int, unsigned int>(diagnostics.trackerDiagnostics.associations);
	RAFeatureRangeT associatedFeatures=MakePermutationRange(featureList, associatedIds);

	//Distance-constrained matching
	CorrespondenceListT correspondences;
	if(filter==FilterT::DISTANCE_FILTER && !prevFeatureList.empty())
		std::tie(diagnostics.matcherDiagnostics, correspondences)=ApplyDistanceConstraint(associatedFeatures);

	//Homography estimation
	if(filter==FilterT::HOMOGRAPHY_FILTER && !prevFeatureList.empty())
	{
		optional<Homography2DT> mH;
		std::tie(diagnostics.homographyDiagnostics, correspondences, mH)=ApplyHomographyConstraint(associatedFeatures);

		//If the prediction fails, try again, without an initial estimate
		bool flagPredictionSuccess=UpdatePredictedHomography(mH, correspondences);
		if(!flagPredictionSuccess)
		{
			std::tie(diagnostics.homographyDiagnostics, correspondences, mH)=ApplyHomographyConstraint(associatedFeatures);
			UpdatePredictedHomography(mH, correspondences);	//We do not care about the return type
		}
	}	//if(filter==FilterT::HOMOGRAPHY_FILTER && !prevFeatureList.empty())

	//Recover the original indices
	dynamic_bitset<> output(featureList.size());
	for(const auto& current : correspondences)
		output[associatedIds[current.right]]=true;

	return output;
}	//vector<unsigned int> IdentifyStaticFeatures(ImageFeatureTrackingPipelineDiagnosticsC& diagnostics, const vector<ImageFeatureC>& featureList)

/**
 * @brief Applies the filter to the tracks
 * @param[in] passed Indices of the measurements satisfying the filter conditions
 * @param[in] diagnostics Diagnostics for the feature tracker
 */
void ImageFeatureTrackingPipelineC::ApplyFilter(const dynamic_bitset<>& passed, const FeatureTrackerDiagnosticsC& diagnostics)
{
	set<unsigned int> featureIndices;	// Indices of the features that satisfy the filter
	for_each(counting_range((size_t)0, passed.size()), [&](size_t c){if(passed[c]) featureIndices.insert(c);});

	auto itEI=featureIndices.end();
	auto itEP=passCounterActive.end();
	//Update the success counters
	for(const auto& current : diagnostics.associations)
		if(featureIndices.find(current.right)!=itEI)
		{
			auto insertReport=passCounterActive.emplace(current.left,1);	//Attempt to insert. diagnostics.associations does not record the creation of a new track. Conservatively assumed as a failure
			insertReport.first->second += insertReport.second ? 0 : 1;	//If success, new item . Otherwise, increment the existing item
		}	//if(featureIndices.find(current.right)!=itEI)

	//Remove the inactives
	auto itP=passCounterActive.begin();
	auto itERemoved=diagnostics.removed.end();
	while(itP!=itEP)
		if(diagnostics.removed.find(itP->first) != itERemoved)
		{
			passCounterInactive.insert(*itP);
			itP=passCounterActive.erase(itP);
		}
		else
			advance(itP,1);
}	//void ApplyFilter(const vector<unsigned int>& passed)

/**
 * @brief Constructor
 * @param[in] pparameters Parameter object
 */
ImageFeatureTrackingPipelineC::ImageFeatureTrackingPipelineC(const ImageFeatureTrackingPipelineParametersC& pparameters) : parameters(pparameters)
{
	ValidateParameters();

	parameters.trackerParameters.matcherParameters.nThreads=parameters.nThreads;
	parameters.homographyParameters.matcherParameters.nThreads=parameters.nThreads;
	parameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.nThreads=parameters.nThreads;
	parameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nThreads=parameters.nThreads;

	parameters.matcherParameters.flagStatic=true;
	parameters.matcherParameters.flagBucketFilter=false;	//Turned off, because spatial consistency is not necessary for tracks in the same neighbourhood
	parameters.homographyParameters.flagVerbose=false;
	parameters.homographyParameters.flagCovariance=false;

	mR=parameters.noiseVariance * MeasurementCovarianceT::Identity();

	filter= (parameters.flagTrackStatic &&parameters.flagTrackDynamic) ? FilterT::NO_FILTER : (parameters.flagStaticCamera ? FilterT::DISTANCE_FILTER : FilterT::HOMOGRAPHY_FILTER);
	tracker=TrackerT(parameters.trackerParameters);

	prevInlierCount=0;
}	//ImageFeatureTrackingPipelineC(const ImageFeatureTrackingPipelineParametersC& pparameters)

/**
 * @brief Updates the tracks with the image measurements
 * @param[in] problem Feature tracking problem
 * @param[in] featureList Image features
 * @param[in] timeStamp Time stamp
 * @return Diagnostics object
 * @post \c tracker and \c prevFeatureList are modified
 */
ImageFeatureTrackingPipelineDiagnosticsC ImageFeatureTrackingPipelineC::Update(ImageFeatureTrackingProblemC& problem, const vector<ImageFeatureC>& featureList, TimeStampT timeStamp)
{
	ImageFeatureTrackingPipelineDiagnosticsC diagnostics;

	//Update the tracker
	vector<MeasurementCovarianceT> covarianceList(featureList.size(), mR);
	diagnostics.trackerDiagnostics=tracker.Update(problem, featureList, covarianceList, timeStamp);

	//Filtering
	if(!featureList.empty())
		if( ! (filter==FilterT::NO_FILTER || prevFeatureList.empty()) )
		{
			dynamic_bitset<> flagsStatic=IdentifyStaticFeatures(diagnostics, featureList);	//Identify the measurements belonging to static objects
			dynamic_bitset<> flagsPass= parameters.flagTrackStatic ? flagsStatic : flagsStatic.flip();	//Pass flags
			ApplyFilter(flagsPass, diagnostics.trackerDiagnostics);	//Apply the filter
		}	//if( ! (filter==FilterT::NO_FILTER || prevFeatureList.empty()) )

	//Update the pipeline state
	prevFeatureList=featureList;

	return diagnostics;
}	//ImageFeatureTrackingPipelineDiagnosticsC Update(const vector<ImageFeatureC>& featureList)

/**
 * @brief Clears the state
 * @post The state is cleared
 */
void ImageFeatureTrackingPipelineC::Clear()
{
	tracker.Clear();
	prevFeatureList.clear();
}	//void Clear();

/**
 * @brief Returns the active and inactive trajectories
 * @param[in] minSuccessRatio Minimum ratio of measurements satisfying the filter conditions to the total number of measurements in a track
 * @param[in] from Initial time stamp
 * @param[in] to Final time stamp (inclusive)
 * @param[in] minMeasurement Minimum number of measurements (trajectory length)
 * @param[in] minLength Minimum track length
 * @param[in] maxMissRatio Maximum ratio of misses to the track length
 * @return A vector of trajectories
 * @pre \c successRatio >=0
 * @remarks \c maxMissRatio should be (at least) slightly higher than the \c maxMissRatio of the tracker: A track retired for failing the miss ratio condition will have a 1/(length+1) excess over the threshold.
 */
auto ImageFeatureTrackingPipelineC::GetTrajectories(double minSuccessRatio, TimeStampT from, TimeStampT to, unsigned int minMeasurement, unsigned int minLength, double maxMissRatio) const -> vector<TrajectoryT>
{
	//Preconditions
	assert(minSuccessRatio>=0);

	//Get the tracks
	map<unsigned int, TrajectoryT> tracks=tracker.GetTrajectories(from, to, minMeasurement, minLength, maxMissRatio);

	//Output
	vector<TrajectoryT> output; output.reserve(tracks.size());

	map<size_t, unsigned int> successCounters(passCounterActive);
	successCounters.insert(passCounterInactive.begin(), passCounterInactive.end());

	auto itE=successCounters.end();

	for(const auto& current : tracks)
	{
		bool flagPass = (filter==FilterT::NO_FILTER);

		//If there is a filter, apply
		if(!flagPass)
		{
			//Seek in the success counters
			auto it=successCounters.find(current.first);

			if(it==itE)	//If no success, skip
				continue;

			flagPass = (double)it->second / current.second.size() >=minSuccessRatio;
		}	//if(!flagPass)

		if(flagPass)
			output.emplace_back(current.second);
	}	//for(const auto& current : tracks)

	output.shrink_to_fit();

	return output;
}	//vector<typename TrackerT::TrajectoryT> GetTrajectories(double successRatio, TimeStampT from, TimeStampT to, unsigned int minMeasurement, unsigned int minLength, double maxMissRatio) const

}	//TrackerN
}	//SeeSawN

