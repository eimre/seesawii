/**
 * @file ImageFeatureTrackingProblem.ipp Implementation of \c ImageFeatureTrackingProblemC
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_TRACKING_PROBLEM_IPP_1089312
#define IMAGE_FEATURE_TRACKING_PROBLEM_IPP_1089312

#include <boost/optional.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <tuple>
#include <vector>
#include "FeatureTrackerDistanceConstraint.h"
#include "../StateEstimation/KFFeatureTrackingProblem.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace TrackerN
{

using boost::optional;
using boost::geometry::cs::cartesian;
using boost::geometry::model::point;
using std::tuple;
using std::vector;
using SeeSawN::StateEstimationN::KFImageFeatureTrackingProblemC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::TrackerN::FeatureTrackerDistanceConstraintC;

/**
 * @brief Tracking problem for image features
 * @ingroup Problem
 * @nosubgrouping
 */
class ImageFeatureTrackingProblemC
{
	private:

		typedef ValueTypeM<Coordinate2DT>::type RealT;	///< Floating point type

		typedef KFImageFeatureTrackingProblemC KFProblemT;	///< Type of the Kalman filter problem
		typedef KFProblemT::state_type StateT;	///< Type of the state
		typedef KFProblemT::measurement_type MeasurementT;	///< Type of a measurement
		typedef KFProblemT::state_covariance_type ProcessNoiseT;	///< Type of the process noise covariance
		typedef KFProblemT::innovation_covariance_type MeasurementNoiseT;	///< Type of the measurement noise covariance

		typedef ImageFeatureMatchingProblemC< InverseEuclideanIDConverterT,  FeatureTrackerDistanceConstraintC<RealT, 2> > MatchingProblemT;	///< Type of the matching problem
		typedef MatchingProblemT::similarity_type SimilarityT;	///< Type of the similarity metric

		/** @name Configuration */ //@{
		bool flagValid;	///< \c true if the object is valid

		KFProblemT kfProblem;	///< Kalman filter problem
		ProcessNoiseT mQ;	///< Process noise
		ProcessNoiseT mQi;	///< Process noise for a new track
		MeasurementNoiseT mR;	///< Measurement noise

		SimilarityT similarity;	///< Feature similarity metric
		//@}

	public:

		/** @name FeatureTrackingProblemConceptC interface */ //@{

		typedef unsigned int time_stamp_type;	///< Type of the time stamp

		typedef KFProblemT kalman_problem_type;	///< Type of the Kalman filter problem

		typedef ImageFeatureC feature_type;	///< Type of the features being tracked
		typedef point<RealT, 2, cartesian> rtree_point_type;	///< Boost.Geometry point type

		typedef MatchingProblemT matching_problem_type;	///< Type of the matching problem

		const KFProblemT& KalmanProblem() const;	///< Returns a constant reference to \c kfProblem
		Coordinate2DT GetPosition(const StateT& state) const;	///< Extracts the position component from a state
		const ProcessNoiseT& ProcessNoise() const;	///< Returns a constant reference to \c mQ
		StateT MakeInitialState(const Coordinate2DT& position) const;	///< Returns the initial state for a track to be created at \c position
		optional<tuple<Coordinate2DT, MeasurementNoiseT> > PredictMeasurement(const StateT& state) const;	///< Computes the predicted measurement
		MeasurementT MakeMeasurement(const Coordinate2DT& position, const optional<MeasurementNoiseT>& covariance=optional<MeasurementNoiseT>() ) const;	///< Makes a measurement from an image coordinate

		MeasurementNoiseT ComputeMeasurementNoise(const vector<MeasurementNoiseT>& covarianceList) const;	///< Computes a measurement noise term for the predictions from the actual measurement covariances

		const SimilarityT& SimilarityMetric() const;	///< Returns a constant reference to \c similarity

		bool IsValid() const;	///< Returns \c true if the object is initialised

		//@}

		ImageFeatureTrackingProblemC();	///< Default constructor
		ImageFeatureTrackingProblemC(const ProcessNoiseT &mmQ, const ProcessNoiseT& mmQi, const MeasurementNoiseT& mmR);	///< Constructor

};	//class ImageFeatureTrackingProblemC



}	//TrackerN
}	//SeeSawN

#endif /* IMAGE_FEATURE_TRACKING_PROBLEM_IPP_1089312 */
