/**
 * @file FeatureTrackerDistanceConstraint.ipp Implementation of \c FeatureTrackerDistanceConstraintC
 * @author Evren Imre
 * @date 7 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_TRACKER_DISTANCE_CONSTRAINT_IPP_1709012
#define FEATURE_TRACKER_DISTANCE_CONSTRAINT_IPP_1709012

#include <boost/concept_check.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <vector>
#include <type_traits>
#include <tuple>
#include <cmath>
#include <cstddef>
#include <list>
#include <map>
#include <functional>
#include <iterator>
#include "../Elements/Coordinate.h"
#include "../Wrappers/BoostGeometry.h"

namespace SeeSawN
{
namespace TrackerN
{

using boost::geometry::index::rtree;
using boost::geometry::index::dynamic_rstar;
using boost::geometry::index::satisfies;
using boost::geometry::cs::cartesian;
using boost::geometry::model::point;
using boost::ForwardRangeConcept;
using boost::range_value;
using boost::range::copy;
using Eigen::Array;
using Eigen::Matrix;
using std::is_floating_point;
using std::is_same;
using std::tuple;
using std::get;
using std::make_tuple;
using std::vector;
using std::max;
using std::size_t;
using std::list;
using std::less;
using std::map;
using std::back_inserter;
using std::function;
using SeeSawN::ElementsN::CoordinateVectorT;
using SeeSawN::WrappersN::MakePoint;
using SeeSawN::WrappersN::MakeCoordinate;
using SeeSawN::WrappersN::operator <;

/**
 * @brief Constraint for the distance between a predicted track and a measurement candidate
 * @tparam RealT Floating point type
 * @tparam DIM Dimensionality of the input coordinates
 * @pre \c RealT is a floating point type
 * @remarks Usage notes:
 * 	- The constraint is the Mahalanobis distance between a measurement and a prediction, with respect to the covariance of the prediction error (innovation covariance)
 * 	- A kernel is the inverse of the covariance matrix for the prediction error. \c FeatureTrackerC assumes fixed measurement covariance
 * 	- If no kernel is specified, it is assumed to be all-zeros. So, the test automatically succeeds
 * 	- The batch evaluation uses an r-tree, which is more suited to spaces with a lower dimensionality
 * @remarks The class is designed to work within \c FeatureMatcherC. Hence the awkward interface.
 * @ingroup Metrics
 * @nosubgrouping
 */
template<typename RealT, unsigned int DIM>
class FeatureTrackerDistanceConstraintC
{
	///@cond CONCEPT_CHECK
	static_assert(is_floating_point<RealT>::value, "FeatureTrackerDistanceConstraintC : RealT must be a floating point type.");
	///@endcond

	private:

		typedef CoordinateVectorT<RealT, DIM> CoordinateT;	///< Coordinate type
		typedef Matrix<RealT, DIM, DIM> KernelT;	///< Mahalanobis kernel type

		typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT; ///< A boolean array

        /** @name Configuration */ //@{
		double threshold;	///< Mahalanobis distance threshold
		vector<KernelT> kernels;	///< Mahalanobis distance kernels (inverse of the covariance matrix for the prediction error)
        unsigned int nThreads; ///< Number of threads
        unsigned int nElementsPerPage;	///< Number of elements per page of the rtree
        bool flagValid;	///< \c true if the object is valid
        //@}

	public:

	    typedef KernelT kernel_type;	///< Type of the Mahalanobis kernel

		/** @name Constructors */ //@{
	   	FeatureTrackerDistanceConstraintC();	///< Default constructor
	   	template<class KernelRangeT> explicit FeatureTrackerDistanceConstraintC(const KernelRangeT& kkernels, RealT threshold=3, unsigned int nnThreads=1, unsigned int nnElementsPerPage=32);   ///< Constructor
		//@}

		 /**@name Adaptable binary predicate interface */ //@{
		typedef CoordinateT first_argument_type;
		typedef CoordinateT second_argument_type;
		typedef bool result_type;
		bool operator()(const first_argument_type& measurement, const second_argument_type& prediction, const KernelT& kernel=KernelT::Zero()) const;   ///< Checks whether the operands satisfy the constraint
		//@}

        /**@name BinaryConstraintConceptC interface */ //@{
        typedef RealT distance_type;	///< Type of the distance between two objects
        template<class InputRangeT> BoolArrayT BatchConstraintEvaluation(const InputRangeT& measurements, const vector<CoordinateT>& predictions) const;  ///< Evaluates the constraint for all measurement-prediction pairs
        tuple<bool, distance_type> Enforce(const first_argument_type& measurement, const second_argument_type& prediction,  const KernelT& kernel=KernelT::Zero()) const;	///< Verifies the constraint, and returns the distance
        //@}
};	//class EuclideanDistanceConstraintC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post Invalid object
 */
template<typename RealT, unsigned int DIM>
FeatureTrackerDistanceConstraintC<RealT, DIM>::FeatureTrackerDistanceConstraintC() :threshold(3), nThreads(1), nElementsPerPage(32), flagValid(false)
{}

/**
 * @brief Constructor
 * @tparam KernelRangeT A range of kernels
 * @param[in] kkernels List of Mahalanobis kernels for the prediction error. It either has an entry for each element, or a single entry, which is used for all elements
 * @param[in] tthreshold Mahalanobis distance threshold
 * @param[in] nnThreads Number of threads available to the object
 * @param[in] nnElementsPerPage Maximum number of elements per page of the rtree
 * @pre \c KernelRangeT is a forward range of elements of type \c KernelT
 * @post A valid object
 * @remarks In \c FeatureTrackerC, the Mahalanobis kernel for a prediction is computed as the inverse of the innovation covariance
 */
template<typename RealT, unsigned int DIM>
template<class KernelRangeT>
FeatureTrackerDistanceConstraintC<RealT, DIM>::FeatureTrackerDistanceConstraintC(const KernelRangeT& kkernels, RealT tthreshold, unsigned int nnThreads, unsigned int nnElementsPerPage) : threshold(tthreshold), nThreads(nnThreads), nElementsPerPage(nnElementsPerPage), flagValid(true)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<KernelRangeT>));
	static_assert(is_same<KernelT, typename range_value<KernelRangeT>::type >::value, "FeatureTrackerDistanceConstraintC::FeatureTrackerDistanceConstraintC : KernelRangeT holds elements of type KernelT ");

	kernels.resize(boost::distance(kkernels));
	copy(kkernels, kernels.begin());

	nElementsPerPage=max(4, (int)nElementsPerPage);
	nThreads=max(omp_get_max_threads(), (int)nThreads);
}	//EuclideanDistanceConstraintC(const vector<RealT> tthresholds, unsigned int nnThreads, unsigned int nnElementsPerPage)

/**
 * @brief Checks whether the operands satisfy the constraint
 * @param[in] measurement Measurement
 * @param[in] prediction Prediction
 * @param[in] kernel Mahalanobis kernel
 * @return \c false if the distance constraint is violated
 * @pre \c flagValid=true
 */
template<typename RealT, unsigned int DIM>
bool FeatureTrackerDistanceConstraintC<RealT, DIM>::operator()(const first_argument_type& measurement, const second_argument_type& prediction, const KernelT& kernel) const
{
	assert(flagValid);
	return get<0>(Enforce(measurement, prediction, kernel));
}	//bool operator()(const first_argument_type& item1, const second_argument_type& item2, const KernelT& kernel) const

/**
 * @brief Verifies the constraint, and returns the distance
 * @param[in] measurement Measurement
 * @param[in] prediction Prediction
 * @param[in] kernel Mahalanobis kernel
 * @return A tuple, constraint success and the value of the distance.
 * @pre \c flagValid=true
 */
template<typename RealT, unsigned int DIM>
auto FeatureTrackerDistanceConstraintC<RealT, DIM>::Enforce(const first_argument_type& measurement, const second_argument_type& prediction,  const KernelT& kernel) const -> tuple<bool, distance_type>
{
	assert(flagValid);
	RealT val=(measurement-prediction).dot(kernel *(measurement-prediction));
	return make_tuple(val<threshold, val);
}	//auto Enforce(const first_argument_type& measurement, const second_argument_type& prediction,  const KernelT& kernel) const -> tuple<bool, distance_type>

/**
 * @brief Evaluates the constraint for all measurement-prediction pairs
 * @tparam InputRangeT A range of inputs elements
 * @param[in] measurements Measurements
 * @param[in] predictions Predictions
 * @return A boolean array indicating the conformance to the constraint
 * @pre \c IndexRangeT is a forward range of elements of type \c first_argument_type
 * @pre \c flagValid=true
 * @pre \c predictions and \c kernels have the same length, or \c kernels have a single element
 */
template<typename RealT, unsigned int DIM>
template<class InputRangeT>
auto FeatureTrackerDistanceConstraintC<RealT, DIM>::BatchConstraintEvaluation(const InputRangeT& measurements, const vector<CoordinateT>& predictions) const -> BoolArrayT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRangeT>));
	static_assert(is_same<CoordinateT, typename range_value<InputRangeT>::type>::value, "FeatureTrackerDistanceConstraintC::BatchConstraintEvaluation : InputRangeT must hold elements of type first_argument_type" );

	assert(flagValid);
	assert(predictions.size()==kernels.size() || kernels.size()==1);

	size_t nMeasurement=boost::distance(measurements);
	size_t nPrediction=predictions.size();

	//Set up the r-tree
	typedef point<RealT, DIM, cartesian> PointT;
	vector<PointT> sourceList; sourceList.reserve(nMeasurement);
	for(const auto& current : measurements)
		sourceList.push_back(MakePoint(current));

	rtree<PointT, dynamic_rstar> tree(sourceList, dynamic_rstar(nElementsPerPage));

	//Coordinate-to-index map
	auto lexicalCompare=[](const PointT& value1, const PointT& value2){return value1<value2;};
	map<PointT, unsigned int, function<bool(const PointT&, const PointT&)> > indexMap(lexicalCompare);
	for(size_t c=0; c<nMeasurement; ++c)
		indexMap.emplace(sourceList[c],c);

	//Test the constraint

	bool flagUniformKernel=kernels.size()==1;
	BoolArrayT output(nMeasurement, nPrediction); output.setConstant(false);
	size_t c2=0;
#pragma omp parallel for schedule(static) if(nThreads>1) private(c2) num_threads(nThreads)
    for(c2=0; c2<nPrediction; ++c2)
    {
    	KernelT mC = flagUniformKernel ? kernels[0] : kernels[c2];

    	auto query=[&](const PointT& current){return this->operator()(MakeCoordinate(current), predictions[c2],mC); };

    	list<PointT> hitList;
		tree.query(satisfies(query), back_inserter(hitList));

	#pragma omp critical(MDC_BCE)
		for(const auto& current : hitList)
			output(indexMap[current], c2)=true;
    }	//for(c2=0; c2<sSet2; ++c2)

	return output;
}	//BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const

}	//TrackerN
}	//SeeSawN

#endif /* FEATURE_TRACKER_DISTANCE_CONSTRAINT_IPP_1709012 */
