/**
 * @file ImageFeatureTrackingPipeline.ipp Implementation details for \c ImageFeatureTrackingPipelineC
 * @author Evren Imre
 * @date 16 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_TRACKING_PIPELINE_IPP_9370124
#define IMAGE_FEATURE_TRACKING_PIPELINE_IPP_9370124

#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/lexical_cast.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <Eigen/Dense>
#include <string>
#include <cmath>
#include <tuple>
#include <cstddef>
#include <stdexcept>
#include <iterator>
#include <vector>
#include "FeatureTracker.h"
#include "ImageFeatureTrackingProblem.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Geometry/Homography2DSolver.h"
#include "../GeometryEstimationComponents/Homography2DComponents.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Elements/Feature.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/FeatureUtility.h"
#include "../Metrics/Distance.h"
#include "../Metrics/Constraint.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/NumericFunctor.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/BoostRange.h"
#include "../Wrappers/BoostDynamicBitset.h"

namespace SeeSawN
{
namespace TrackerN
{

using boost::optional;
using boost::counting_range;
using boost::range::for_each;
using boost::lexical_cast;
using boost::dynamic_bitset;
using Eigen::MatrixXd;
using std::string;
using std::max;
using std::tie;
using std::size_t;
using std::invalid_argument;
using std::back_inserter;
using std::vector;
using SeeSawN::TrackerN::FeatureTrackerC;
using SeeSawN::TrackerN::FeatureTrackerDiagnosticsC;
using SeeSawN::TrackerN::FeatureTrackerParametersC;
using SeeSawN::TrackerN::ImageFeatureTrackingProblemC;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineHomography2DProblem;
using SeeSawN::GeometryN::MakeRANSACHomography2DProblem;
using SeeSawN::GeometryN::MakePDLHomography2DProblem;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::RANSACHomography2DProblemT;
using SeeSawN::GeometryN::Homography2DEstimatorProblemT;
using SeeSawN::GeometryN::Homography2DPipelineProblemT;
using SeeSawN::GeometryN::PDLHomography2DProblemT;
using SeeSawN::GeometryN::Homography2DPipelineT;
using SeeSawN::GeometryN::Homography2DSolverC;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::MetricsN::EuclideanDistance2DT;
using SeeSawN::MetricsN::EuclideanDistanceConstraint2DT;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::TransferErrorH2DT;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::MakePermutationRange;
using SeeSawN::WrappersN::MakeBitset;

/**
 * @brief Parameters for \c ImageFeatureTrackingPipelineC
 * @ingroup Parameters
 */
struct ImageFeatureTrackingPipelineParametersC
{
	unsigned int nThreads;	///< Number of threads available to the pipeline

	bool flagStaticCamera;	///< \c true if the camera is static
	bool flagTrackStatic;	///< \c true if static features are to be tracked
	bool flagTrackDynamic;	///< \c true if dynamic features are to be tracked

	FeatureTrackerParametersC trackerParameters;	///< Parameters for the feature tracker

	//Measurement filter
	double noiseVariance;	///< Noise variance on the image coordinates. >0
	double pRejection;	///< Probability of rejection for an inlier. [0,1)
	FeatureMatcherParametersC matcherParameters;	///< Parameters for the feature matcher. Required only if the camera is static and track filtering is active

	//Homography estimator
	unsigned int loGeneratorSize1;	///< Size of the generator for the LO-RANSAC for the first stage
	unsigned int loGeneratorSize2;	///< Size of the generator for the LO-RANSAC for the second stage
	int seed;	///< Seed for the random number generator. If <0 , rng default
	GeometryEstimationPipelineParametersC homographyParameters;	///< Parameters for the homography estimator. Required only if the camera is moving, and track filtering is active
	bool flagPrediction;	///< The previous homography estimate is used as the prediction for the current one
	double predictionErrorNoise;	///< Additive noise variance due to the prediction error. >=0
	double predictionFailureTh;	///< If the relative drop in the inlier count is above this value, the homography estimation is repeated without a prediction

	ImageFeatureTrackingPipelineParametersC() : nThreads(1), flagStaticCamera(true), flagTrackStatic(true), flagTrackDynamic(true), noiseVariance(4), pRejection(0.05), loGeneratorSize1(4*Homography2DSolverC::GeneratorSize()), loGeneratorSize2(4*Homography2DSolverC::GeneratorSize()), seed(0), flagPrediction(true), predictionErrorNoise(9), predictionFailureTh(0.9)
	{}
};	//struct ImageFeatureTrackingPipelineParametersC

/**
 * @brief Diagnostics for \c ImageFeatureTrackingPipelineC
 * @ingroup Diagnostics
 */
struct ImageFeatureTrackingPipelineDiagnosticsC
{
	bool flagSuccess;	///< \c true if the last update operation is completed successfully

	FeatureMatcherDiagnosticsC matcherDiagnostics;	///< Diagnostics for the feature matcher. Valid only if the camera is static and track filtering is active
	GeometryEstimationPipelineDiagnosticsC homographyDiagnostics;	///< Diagnostics for the geometry estimator. Valid only if the camera is not static and track filtering is active

	FeatureTrackerDiagnosticsC trackerDiagnostics;	///< Diagnostics for the feature tracker
};	//struct ImageFeatureTrackingPipelineDiagnosticsC

/**
 * @brief Image feature tracking pipeline
 * @remarks Static/dynamic feature detection:
 * 	- Keep the features for the previous frame
 * 	- If the camera is static, static features do not move between frames. Distance-constrained feature matching.
 * 	- If the camera is moving, the motion between frames can be approximated by a homography (as long as the narrow baseline conditions are satisfied). Estimate the homography, and return the inliers
 * 	- Any features failing the above constraints either belong to moving objects, or outliers. However, outliers are unlikely to establish stable trajectories
 * @remarks No dedicated unit tests
 * @ingroup Algorithm
 * @nosubgrouping
 */
class ImageFeatureTrackingPipelineC
{
	private:

		enum class FilterT{NO_FILTER, DISTANCE_FILTER, HOMOGRAPHY_FILTER};

		typedef ImageFeatureTrackingProblemC::time_stamp_type TimeStampT;	///< Type of the time stamps
		typedef ImageFeatureTrackingProblemC::feature_type FeatureT;	///< Type of the image features
		typedef FeatureTrajectoryT<TimeStampT, FeatureT> TrajectoryT;	///< A trajectory

		typedef decltype(MakePermutationRange(vector<FeatureT>(), vector<unsigned int>())) RAFeatureRangeT;	///< A random-access range of features

		/** @name Configuration */ //@{
		ImageFeatureTrackingPipelineParametersC parameters;	///< Parameters
		FilterT filter;	///< Type of the geometry filter to be applied

		typedef ImageFeatureTrackingProblemC::kalman_problem_type::innovation_covariance_type MeasurementCovarianceT;	///< Measurement covariance
		MeasurementCovarianceT mR;	///< Measurement covariance matrix
		//@}

		/** @name State */ //@{
		vector<ImageFeatureC> prevFeatureList;	///< Features from the previous image
		optional<RANSACHomography2DProblemT::dimension_type1> binSize1;	///< RANSAC bin sizes for \c prevFeatureList

		map<size_t, unsigned int> passCounterActive;	///< Keeps the count of measurements satisfying the filter, for the active tracks
		map<size_t, unsigned int> passCounterInactive;	///< Keeps the count of measurements satisfying the filter, for the inactive tracks. A superset of the inactive tracks in \c tracker

		typedef FeatureTrackerC<ImageFeatureTrackingProblemC> TrackerT;	///< Type of the tracker
		TrackerT tracker;	///< Tracker

		optional<Homography2DT> prevH;	///< Previous homography. If valid, used as an initial estimate
		unsigned int prevInlierCount;	///< Number of inliers to \c prevH
		//@}

		/** @name Implementation details */ //@{

		void ValidateParameters();	///< Validates the parameters

		tuple<FeatureMatcherDiagnosticsC, CorrespondenceListT> ApplyDistanceConstraint(const RAFeatureRangeT& featureList) const;		///< Applies the distance constraint to identify the static features
		tuple<GeometryEstimationPipelineDiagnosticsC, CorrespondenceListT, optional<Homography2DT> > ApplyHomographyConstraint(const RAFeatureRangeT& featureList);	///< Applies the homography constraint to identify the static features
		bool UpdatePredictedHomography(const optional<Homography2DT>& mH, const CorrespondenceListT& correspondences);	///< Updates the homography prediction
		dynamic_bitset<> IdentifyStaticFeatures(ImageFeatureTrackingPipelineDiagnosticsC& diagnostics, const vector<ImageFeatureC>& featureList);	///< Identifies the static features

		void ApplyFilter(const dynamic_bitset<>& passed, const FeatureTrackerDiagnosticsC& diagnostics);	///< Applies the filter to the tracks
		//@}

	public:

		explicit ImageFeatureTrackingPipelineC(const ImageFeatureTrackingPipelineParametersC& pparameters=ImageFeatureTrackingPipelineParametersC());	///< Constructor

		ImageFeatureTrackingPipelineDiagnosticsC Update(ImageFeatureTrackingProblemC& problem, const vector<ImageFeatureC>& featureList, TimeStampT timeStamp);	///< Updates the tracks with the image measurements
		vector<TrajectoryT> GetTrajectories(double minSuccessRatio, TimeStampT from, TimeStampT to, unsigned int minMeasurement, unsigned int minLength, double maxMissRatio) const;	///< Returns the active and inactive trajectories

		void Clear();	///< Clears the state
};	//class ImageFeatureTrackingPipelineC


}	//TrackerN
}	//SeeSawN

#endif /* IMAGE_FEATURE_TRACKING_PIPELINE_IPP_9370124 */
