/**
 * @file TestTracker.cpp
 * @author Evren Imre
 * @date 11 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE TRACKER

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include "FeatureTrackerDistanceConstraint.h"
#include "ImageFeatureTrackingProblem.h"
#include "FeatureTracker.h"
#include "ImageFeatureTrackingPipeline.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Descriptor.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace TestTrackerN
{

using namespace TrackerN;
using boost::optional;
using Eigen::Array;
using Eigen::Matrix;
using std::vector;
using std::tuple;
using std::tie;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::ImageDescriptorT;
using SeeSawN::WrappersN::ValueTypeM;


BOOST_AUTO_TEST_SUITE(Tracker_Problems)

BOOST_AUTO_TEST_CASE(Feature_Tracker_Distance_Constraint)
{
	typedef ValueTypeM<Coordinate2DT>::type RealT;
	typedef FeatureTrackerDistanceConstraintC<RealT, 2> ConstraintT;

	//Constructors

	ConstraintT constraint;	//Default constructor

	typedef ConstraintT::kernel_type KernelT;
	vector<KernelT> kernels(2);
	kernels[0]=KernelT::Identity();
	kernels[1]<<2,1,1,2; kernels[1]=kernels[1].inverse();

	ConstraintT constraint2(kernels, 3, 1, 32);

	//Operations
	Coordinate2DT track1(20,12);
	Coordinate2DT measurement1(0,0);
	Coordinate2DT measurement2(19,11.5);

	BOOST_CHECK(!constraint2(measurement1, track1, kernels[0]));
	BOOST_CHECK(constraint2(measurement2, track1, kernels[0]));

	BOOST_CHECK(!get<0>(constraint2.Enforce(measurement1, track1, kernels[1])));
	BOOST_CHECK_CLOSE(get<1>(constraint2.Enforce(measurement1, track1, kernels[1])), 138.666666, 1e-6);

	BOOST_CHECK(get<0>(constraint2.Enforce(measurement2, track1, kernels[1])));
	BOOST_CHECK_CLOSE(get<1>(constraint2.Enforce(measurement2, track1, kernels[1])), 0.388888888, 1e-6);

	BOOST_CHECK(constraint2(measurement1, track1));
	BOOST_CHECK(get<0>(constraint2.Enforce(measurement1, track1)));
	BOOST_CHECK_SMALL(get<1>(constraint2.Enforce(measurement1, track1)), 1e-6);

	//Batch operation
	vector<Coordinate2DT> measurements3(5);
	measurements3[0]<<5,2; measurements3[1]<<6,6; measurements3[2]<<1,3; measurements3[3]<<10,8; measurements3[4]<<12,14;

	vector<Coordinate2DT> tracks3(2);
	tracks3[0]<<10.5, 7.75; tracks3[1]<<20,4;

	typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT;
	BoolArrayT output3=constraint2.BatchConstraintEvaluation(measurements3, tracks3);

	BoolArrayT output3r(5,2); output3r.setConstant(false); output3r(3,0)=true;
	BOOST_CHECK(output3.matrix()==output3r.matrix());

	vector<KernelT> kernels4(1, kernels[0]);
	ConstraintT constraint4(kernels4);
	BoolArrayT output4=constraint4.BatchConstraintEvaluation(measurements3, tracks3);
	BOOST_CHECK(output3.matrix()==output4.matrix());
}	//BOOST_AUTO_TEST_CASE(Feature_Tracker_Distance_Metric)

BOOST_AUTO_TEST_CASE(Image_Feature_Tracking_Problem)
{
	//Constructors
	ImageFeatureTrackingProblemC invalidProblem;
	BOOST_CHECK(!invalidProblem.IsValid());

	typedef KFImageFeatureTrackingProblemC::state_covariance_type StateCovarianceT;
	typedef KFImageFeatureTrackingProblemC::innovation_covariance_type InnovationCovarianceT;

	StateCovarianceT mQ=0.25*StateCovarianceT::Identity(); mQ(0,0)=0; mQ(1,1)=0;
	StateCovarianceT mQi=10*mQ;
	InnovationCovarianceT mR=InnovationCovarianceT::Identity();
	ImageFeatureTrackingProblemC problem(mQ, mQi, mR);
	BOOST_CHECK(problem.IsValid());

	//Operations

	Coordinate2DT position(3,4);
	KFImageFeatureTrackingProblemC::state_type state=problem.MakeInitialState(position);
	BOOST_CHECK(position==problem.GetPosition(state));

	StateCovarianceT mPr=mQi;
	mPr.topLeftCorner(2,2)+=mR;
	BOOST_CHECK(state.GetCovariance()==mPr);
	BOOST_CHECK(mQ==problem.ProcessNoise());

	KFImageFeatureTrackingProblemC::state_vector_type stateVector; stateVector.setZero();
	stateVector.head(2)=position;
	BOOST_CHECK( *problem.KalmanProblem().PropagateStateMean(state)== stateVector);

	optional<tuple<Coordinate2DT, InnovationCovarianceT> > measurement=problem.PredictMeasurement(state);
	BOOST_CHECK(measurement);
	BOOST_CHECK(get<0>(*measurement)==position);
	BOOST_CHECK(get<1>(*measurement)==2*mR);

	KFImageFeatureTrackingProblemC::measurement_type measurement2=problem.MakeMeasurement(position);
	BOOST_CHECK(position==measurement2.GetMean());
	BOOST_CHECK(mR==measurement2.GetCovariance());

	InnovationCovarianceT mR3=3*mR;
	KFImageFeatureTrackingProblemC::measurement_type measurement3=problem.MakeMeasurement(position, mR3);
	BOOST_CHECK(position==measurement3.GetMean());
	BOOST_CHECK(mR3==measurement3.GetCovariance());


	ImageDescriptorT desc1(4); desc1<<0,1,2,1;
	ImageDescriptorT desc2(4); desc2<<1,3,1,0;

	BOOST_CHECK_CLOSE(problem.SimilarityMetric()(desc1.normalized(), desc2.normalized()), 1.14028347, 1e-6);

	BOOST_CHECK(problem.ComputeMeasurementNoise(vector<InnovationCovarianceT>())==mR);

}	//BOOST_AUTO_TEST_CASE(Image_Feature_Tracking_Problem)

BOOST_AUTO_TEST_SUITE_END()	//Tracker_Problems

BOOST_AUTO_TEST_SUITE(Feature_Tracker)

BOOST_AUTO_TEST_CASE(Feature_Tracker_Compile_Test)
{
	typedef KFImageFeatureTrackingProblemC::state_covariance_type StateCovarianceT;
	typedef KFImageFeatureTrackingProblemC::innovation_covariance_type InnovationCovarianceT;

	ImageFeatureTrackingProblemC problem(StateCovarianceT::Identity(), StateCovarianceT::Identity(), InnovationCovarianceT::Identity());
	FeatureTrackerParametersC parameters;

	FeatureTrackerC<ImageFeatureTrackingProblemC> tracker1;
	FeatureTrackerC<ImageFeatureTrackingProblemC> tracker2(parameters);

	vector<ImageFeatureC> measurements;
	vector<InnovationCovarianceT> measurementCovariances;
	FeatureTrackerDiagnosticsC diagnostics=tracker2.Update(problem, measurements, measurementCovariances, 0); (void)diagnostics;

	tracker2.GetTrajectories(0, 100, 10, 10, 0.25);
	tracker2.Clear();

	vector<unsigned int> activeList=tracker2.GetActive();
	vector<unsigned int> updatedList=tracker2.GetObserved(0);

}	//BOOST_AUTO_TEST_CASE(Feature_Tracker_Compile_Test)

BOOST_AUTO_TEST_SUITE_END()	//Feature_Tracker

BOOST_AUTO_TEST_SUITE(Feature_Tracking_Pipeline)

BOOST_AUTO_TEST_CASE(Feature_Tracking_Pipeline_Compile_Test)
{
	ImageFeatureTrackingPipelineParametersC parameters;

	ImageFeatureTrackingPipelineC engine1;
	ImageFeatureTrackingPipelineC engine2(parameters);

	typedef KFImageFeatureTrackingProblemC::state_covariance_type StateCovarianceT;
	typedef KFImageFeatureTrackingProblemC::innovation_covariance_type InnovationCovarianceT;

	ImageFeatureTrackingProblemC problem(StateCovarianceT::Identity(), StateCovarianceT::Identity(), InnovationCovarianceT::Identity());

	vector<ImageFeatureC> featureList;
	ImageFeatureTrackingPipelineDiagnosticsC diagnostics=engine2.Update(problem, featureList, 0);

	engine2.GetTrajectories(0.5, 0, 100, 10, 10, 0.25);
}	//BOOST_AUTO_TEST_CASE(Feature_Tracking_Pipeline_Compile_Test)

BOOST_AUTO_TEST_SUITE_END()

}	//TestTrackerN
}	//SeeSawN

