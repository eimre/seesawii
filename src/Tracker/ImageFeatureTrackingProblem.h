/**
 * @file ImageFeatureTrackingProblem.h Public interface for \c ImageFeatureTrackingProblemC
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_TRACKING_PROBLEM_H_9309115
#define IMAGE_FEATURE_TRACKING_PROBLEM_H_9309115

#include "ImageFeatureTrackingProblem.ipp"
namespace SeeSawN
{
namespace TrackerN
{

class ImageFeatureTrackingProblemC;	///< Tracking problem for image features
}	//TrackerN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<unsigned int>;
extern template class std::set<unsigned int>;
extern template std::string boost::lexical_cast<std::string, double>(const double&);


#endif /* IMAGE_FEATURE_TRACKING_PROBLEM_H_9309115 */
