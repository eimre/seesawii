/**
 * @file FeatureTracker.ipp Implementation of \c FeatureTrackerC
 * @author Evren Imre
 * @date 1 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_TRACKER_IPP_6898903
#define FEATURE_TRACKER_IPP_6898903

#include <boost/optional.hpp>
#include <boost/range/join.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/algorithms/distance.hpp>
#include <tuple>
#include <list>
#include <array>
#include <vector>
#include <map>
#include <set>
#include <iterator>
#include <stdexcept>
#include <string>
#include <cmath>
#include "../Elements/Feature.h"
#include "../Elements/Correspondence.h"
#include "../StateEstimation/KalmanFilter.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/BoostGeometry.h"
#include "../Wrappers/EigenMetafunction.h"
#include "FeatureTrackerDistanceConstraint.h"
#include "FeatureTrackingProblemConcept.h"

namespace SeeSawN
{
namespace TrackerN
{

using boost::optional;
using boost::range::join;
using boost::range::set_difference;
using boost::range::transform;
using boost::counting_range;
using boost::lexical_cast;
using boost::geometry::index::rtree;
using boost::geometry::index::dynamic_rstar;
using boost::geometry::index::nearest;
using boost::geometry::distance;
using std::list;
using std::vector;
using std::tuple;
using std::get;
using std::tie;
using std::array;
using std::map;
using std::set;
using std::inserter;
using std::advance;
using std::invalid_argument;
using std::string;
using std::max;
using std::prev;
using SeeSawN::ElementsN::FeatureTrajectoryT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::StateEstimationN::KalmanFilterC;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::MakePoint;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::TrackerN::FeatureTrackerDistanceConstraintC;
using SeeSawN::TrackerN::FeatureTrackingProblemConceptC;

/**
 * @brief Parameter obejct for \c FeatureTrackerC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct FeatureTrackerParametersC
{
	double minNeighbourDistance;	///< The minimum admissible distance between a new track and its closest neighbour. >=0
	unsigned int nFeaturesPerPage;	///< Number of features per page of the r-tree for neighbourhood check. Affects speed. >0

	double maxMissRatio;	///< If the ratio of the misses to the track length goes above this value, the track is retired. Removes inactive tracks. [0,1]
	unsigned int maxConsecutiveMiss;	///< Maximum number of iterations a track can go without receiving a measurement, before it is retired. Removes inactive tracks
	unsigned int gracePeriod;	///< The number of iterations for which a track is guaranteed to survive. Prevents premature deletions

	unsigned int minLength;	///< If a track is shorter than this value at retirement, it is deleted

	bool flagStaticDescriptor;	///< If \c true, the descriptor for a measurement prediction is that of the first element in the track. Otherwise, the last element

	FeatureMatcherParametersC matcherParameters;	///< Feature matcher parameters

	//Distance constraint parameters
	double maxMahalanobisDistance;	///< Maximum mahalanobis distance for track-measurement association. >=0

	FeatureTrackerParametersC() : minNeighbourDistance(9), nFeaturesPerPage(32),  maxMissRatio(0.25), maxConsecutiveMiss(3), gracePeriod(0), minLength(10), flagStaticDescriptor(false), maxMahalanobisDistance(5.99)
	{}
};	//struct FeatureTrackerParameterC

/**
 * @brief Diagnostics object for \c FeatureTrackerC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct FeatureTrackerDiagnosticsC
{
	bool flagSuccess;	///< \c true if the track update operation is completed successfully for all tracks. Failure means only a partial update, but tracks are left in a valid state

	size_t nActiveTracks;	///< Number of active tracks
	size_t nInactiveTracks;	///< Number of inactive tracks

	CorrespondenceListT associations;	///< Track-measurement associations. [Track Id; Measurement]. New tracks are not included
	map<unsigned int, bool> removed;	///< Indices of tracks removed from the active list. [Id; flagRetired]. If the data element is \c true, the track is in \c inactiveTracks

	FeatureMatcherDiagnosticsC fmDiagnostics;	///< Diagnostics for the feature matcher
};	//struct FeatureTrackerDiagnosticsC;

//Notes: Minimum distance constraint at initialisation can be violated by tracks initiated simultaneously

/**
 * @brief Feature Tracker
 * @tparam ProblemT Feature tracker problem
 * @pre \c ProblemT is a model of \c FeatureTrackingProblemConceptC
 * @remarks Usage notes
 * - A track is never removed as long as it receives measurements. Therefore, a track with a poor grace period might defy the maximum miss ratio condition, and become an inactive track.
 * @remarks No dedicated unit tests
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class FeatureTrackerC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((FeatureTrackingProblemConceptC<ProblemT>));
	///@endcond

	private:

		typedef KalmanFilterC<typename ProblemT::kalman_problem_type> KalmanFilterT;	///< A Kalman filter
		typedef typename ProblemT::feature_type FeatureT;	///< A feature
		typedef typename FeatureT::coordinate_type CoordinateT;	///< A coordinate
		typedef typename ProblemT::time_stamp_type TimeStampT;	///< A time stamp
		typedef FeatureTrajectoryT<TimeStampT, FeatureT> TrajectoryT;	///< A trajectory

		typedef FeatureTrackerDistanceConstraintC< typename ValueTypeM<CoordinateT>::type, RowsM<CoordinateT>::value > ConstraintT;	///< Type of the distance constraint
		typedef typename ConstraintT::kernel_type SearchRegionT;	///< Type of the matrix defining the search regions
		typedef SearchRegionT InnovationCovarianceT;	///< Type of the innovation covariance


		/** @name Track statistics definition */ //@{
		typedef array<unsigned int, 2> TrackStatisticsT;
		static constexpr unsigned int iMiss=0;	///< Index of the component holding the consecutive misses, where the track received no measurement
		static constexpr unsigned int iLength=1;	///< Index of the component holding the length of the track
		//@}

		/** @name Track definition */ //@{
		typedef tuple<unsigned int, KalmanFilterT, TrajectoryT, TrackStatisticsT> TrackT;

		static constexpr unsigned int iTrackId=0;	///< Id of the track
		static constexpr unsigned int iState=1;	///< Index of the state component
		static constexpr unsigned int iTrajectory=2;	///< Index of the trajectory component
		static constexpr unsigned int iStatistics=3;	///< Index of the track statistics component
		//@}

		/** @name State */ //@{
		list<TrackT> tracks;	///< Tracks
		list<TrackT> inactiveTracks;	///< Inactive tracks
		unsigned int nextTrackId;	///< Id to be assigned to the next new track
		//@}

		/** @name Configuration */ //@{
		FeatureTrackerParametersC parameters;	///< Parameters
		//@}

		/** @name Implementation details */ //@{
		void ValidateParameters();	///< Validates the parameters
		bool CheckRemoval(const TrackT& track) const;	///< Verifies whether a track should be removed from the active set
		bool UpdateTracks(ProblemT& problem, FeatureTrackerDiagnosticsC& diagnostics, const CorrespondenceListT& correspondences,const vector<unsigned int>& indexList, const vector<FeatureT>& predictedFeatures, const vector<InnovationCovarianceT>& predictionCovariances, const vector<FeatureT>& measurements, TimeStampT timeStamp);	///< Updates the tracks
		void CreateTracks(ProblemT& problem, const CorrespondenceListT& correspondences, const vector<FeatureT>& measurements, TimeStampT timeStamp);	///< Creates new tracks

		tuple<vector<unsigned int>, vector<unsigned int>, vector<FeatureT>, vector<InnovationCovarianceT> > Predict(ProblemT& problem);	///< Time update
		FeatureTrackerDiagnosticsC Update(ProblemT& problem, const vector<unsigned int>& indexList, const vector<unsigned int>& trackIds, const vector<FeatureT>& predictedFeatures, const vector<InnovationCovarianceT>& predictionCovariances, const vector<FeatureT>& measurements, const vector<InnovationCovarianceT>& measurementCovariances, TimeStampT timeStamp);	///< Measurement update
		//@}

	public:

		typedef TrajectoryT trajectory_type;	///< Type of a feature trajectory

		explicit FeatureTrackerC(const FeatureTrackerParametersC& pparameters=FeatureTrackerParametersC());	///< Constructor

		/** @name Operations */ //@{
		FeatureTrackerDiagnosticsC Update(ProblemT& problem, const vector<FeatureT>& measurements, const vector<InnovationCovarianceT>& measurementCovariances, TimeStampT timeStamp);	///< Updates the tracks
		//@}

		/** @name Utilities */ //@{
		void Clear();	///< Clears the state
		map<unsigned int, TrajectoryT> GetTrajectories(TimeStampT from, TimeStampT to, unsigned int minMeasurement, unsigned int minLength, double maxMissRatio) const;	///< Returns the active and inactive trajectories satisfying a filter

		vector<unsigned int> GetActive() const;	///< Returns the ids of the active tracks
		vector<unsigned int> GetObserved(TimeStampT timeStamp) const;	///< Returns the ids of the tracks that received a measurement at a specified time
		//@}
};	//class FeatureTrackerC


/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @throws invalid_argument if an invalid parameter value is encountered
 */
template<class ProblemT>
void FeatureTrackerC<ProblemT>::ValidateParameters()
{
	if(parameters.maxMissRatio<0 || parameters.maxMissRatio>1)
		throw(invalid_argument(string("FeatureTrackerC::ValidateParameters : maxMissRatio must be in [0,1]. Value:")+lexical_cast<string>(parameters.maxMissRatio)));

	if(parameters.minNeighbourDistance<0)
		throw(invalid_argument(string("FeatureTrackerC::ValidateParameters : minNeighbourDistance must be a non-negative value. Value:")+lexical_cast<string>(parameters.minNeighbourDistance)));

	if(parameters.nFeaturesPerPage<=0)
		throw(invalid_argument(string("FeatureTrackerC::ValidateParameters : nFeaturesPerPage must be a positive value. Value:")+lexical_cast<string>(parameters.nFeaturesPerPage)));

	parameters.nFeaturesPerPage=max((unsigned int)4, parameters.nFeaturesPerPage);

	if(parameters.maxMahalanobisDistance<=0)
		throw(invalid_argument(string("FeatureTrackerC::ValidateParameters : maxMahalanobisDistance must be a positive value. Value:")+lexical_cast<string>(parameters.maxMahalanobisDistance)));

	//1:1 one-shot matching
	parameters.matcherParameters.flagStatic=true;
	parameters.matcherParameters.nnParameters.neighbourhoodSize12=1;
	parameters.matcherParameters.nnParameters.neighbourhoodSize21=1;
}	//void ValidateParameters()

/**
 * @brief Verifies whether a track should be removed from the active set
 * @param[in] track Track to be verified for removal
 * @return \c true if the track should be removed
 */
template<class ProblemT>
bool FeatureTrackerC<ProblemT>::CheckRemoval(const TrackT& track) const
{
	//Removal logic

	unsigned int length=get<iLength>(get<iStatistics>(track));
	bool flagKeep=(length <= parameters.gracePeriod) || (get<iMiss>(get<iStatistics>(track))==0);	//If in the grace period, or just received a measurement, keep

	if(!flagKeep)
	{
		double missRatio=1-(double)get<iTrajectory>(track).size()/length;
		flagKeep= (missRatio <= parameters.maxMissRatio);
		flagKeep= flagKeep && (get<iMiss>(get<iStatistics>(track))<=parameters.maxConsecutiveMiss);
	}

	return !flagKeep;
}	//bool CheckRemoval(const TrackT& track)

/**
 * @brief Updates the tracks
 * @param[in] problem Tracking problem
 * @param[out] diagnostics Diagnostics
 * @param[in] correspondences Track-measurement associations
 * @param[in] indexList Indicies of the tracks in \c tracks corresponding to the elements of \c predictedFeatures
 * @param[in] predictedFeatures Predicted features
 * @param[in] predictionCovariances Innovation covariances for the predicted features
 * @param[in] measurements Measurements
 * @param[in] timeStamp Time stamp
 * @pre \c predictedFeatures and \c indexList have the same number of elements
 * @pre \c predictedCovariances and \c indexList have the same number of elements
 * @pre \c problem is valid
 * @post \c tracks are modified through updates and deletions
 * @throws invalid_argument If \c problem is invalid
 * @return \c false if Kalman update operation fails for at least one track
 */
template<class ProblemT>
bool FeatureTrackerC<ProblemT>::UpdateTracks(ProblemT& problem, FeatureTrackerDiagnosticsC& diagnostics, const CorrespondenceListT& correspondences, const vector<unsigned int>& indexList, const vector<FeatureT>& predictedFeatures,  const vector<InnovationCovarianceT>& predictionCovariances, const vector<FeatureT>& measurements, TimeStampT timeStamp)
{
	//Preconditions

	if(!problem.IsValid())
		throw(invalid_argument("FeatureTrackerC::UpdateTracks: Invalid problem."));

	assert(indexList.size()==predictedFeatures.size());
	assert(indexList.size()==predictionCovariances.size());

	typedef tuple<unsigned int, unsigned int> PairT;
	map<unsigned int, PairT> associated;	///< Indices of the tracks associated with a measurement
	for(const auto& current : correspondences)
		associated[ indexList[current.right] ]=make_tuple(current.right, current.left);

	//Update the tracks
	auto itAE=associated.end();
	auto itTE=tracks.end();
	unsigned int c=0;
	auto it=tracks.begin();
	bool flagSuccess=true;

	//Iterate over the tracks
	while(it!=itTE)
	{
		//If there is an associated track, update

		auto itA=associated.find(c); ++c;

		if(itA!=itAE)
		{

			unsigned int iP=get<0>(itA->second);
			unsigned int iM=get<1>(itA->second);

			bool flagUpdate=get<iState>(*it).Update(problem.KalmanProblem(), problem.MakeMeasurement(measurements[iM].Coordinate()), problem.MakeMeasurement(predictedFeatures[iP].Coordinate(), predictionCovariances[iP]) );	//Update the track
			flagSuccess = flagSuccess && flagUpdate;

			if(flagUpdate)
			{
				get<iTrajectory>(*it).emplace(timeStamp, measurements[iM]);
				get<iMiss>(get<iStatistics>(*it))=0;	//Reset the consecutive miss counter
			}
			else
				continue;
		}	//if(it!=itE)
		else
			++get<iMiss>(get<iStatistics>(*it));	// Increment the miss counter

		++get<iLength>(get<iStatistics>(*it));

		if(CheckRemoval(*it))	//Remove from the active set?
		{
			bool flagRetire=get<iLength>(get<iStatistics>(*it)) >=parameters.minLength;	//Retire to the inactive set?

			if(flagRetire)
				inactiveTracks.push_back(*it);

			diagnostics.removed[get<iTrackId>(*it)]=flagRetire;
			it=tracks.erase(it);
		}
		else
			advance(it,1);

	}	//for(auto& it=tracks.begin(); it!=itET; ++c)

	return flagSuccess;
}	//void TrackUpdate(const CorrespondenceListT& correspondences, const vector<FeatureT>& measurements, const vector<unsigned int>& predictionToTrackMap)

/**
 * @brief Creates new tracks
 * @param[in] problem Problem
 * @param[in] correspondences Correspondences
 * @param[in] measurements Measurements
 * @param[in] timeStamp Time stamp
 * @pre \c problem is valid
 * @throws invalid_argument If \c problem is invalid
 * @remarks Attempts to create new tracks from unassociated measurements
 */
template<class ProblemT>
void FeatureTrackerC<ProblemT>::CreateTracks(ProblemT& problem, const CorrespondenceListT& correspondences, const vector<FeatureT>& measurements, TimeStampT timeStamp)
{
	if(!problem.IsValid())
		throw(invalid_argument("FeatureTrackerC::CreateTracks : Invalid problem."));

	//Build an r-tree of the existing tracks, for neighbourhood check

	typedef typename ProblemT::rtree_point_type PointT;
	vector<PointT> rtreeInput; rtreeInput.reserve(tracks.size());
	for(const auto& current : tracks)
		rtreeInput.push_back( MakePoint(problem.GetPosition(get<iState>(current).GetState())) );

	 rtree<PointT, dynamic_rstar> tree(rtreeInput, dynamic_rstar(parameters.nFeaturesPerPage));

	//Get the indices of the unassociated measurements
	vector<unsigned int> predictionIndices;
	vector<unsigned int> measurementIndices;
	tie(measurementIndices, predictionIndices)=BimapToVector<unsigned int, unsigned int>(correspondences);

	set<unsigned int> associated(measurementIndices.begin(), measurementIndices.end());
	set<unsigned int> unassociated;
	set_difference(counting_range((size_t)0, measurements.size()), associated, inserter(unassociated, unassociated.end()));

	//Initialise new tracks
	PointT nearestPoint;
	TrackStatisticsT initialStatistics{{0,1}};
	for(auto current : unassociated)
	{
		//Check the distance to the nearest track
		if(!tree.empty())
		{
			PointT currentPoint=MakePoint(measurements[current].Coordinate());
			tree.query(nearest(currentPoint,1), &nearestPoint );

			if(distance(currentPoint, nearestPoint)<parameters.minNeighbourDistance)
				continue;
		}

		KalmanFilterT state;
		state.SetState(problem.MakeInitialState(measurements[current].Coordinate()), problem.ProcessNoise());

		tracks.emplace_back(nextTrackId, state, TrajectoryT{{timeStamp, measurements[current]}}, initialStatistics);
		++nextTrackId;
	}	//for(auto current : unassociated)
}	//void CreateTracks(ProblemT& problem, const CorrespondenceListT& correspondences, const vector<FeatureT>& measurements)

/**
 * @brief Constructor
 * @param[in] pparameters Parameters
 * @post Valid object
 */
template<class ProblemT>
FeatureTrackerC<ProblemT>::FeatureTrackerC(const FeatureTrackerParametersC& pparameters) : nextTrackId(0), parameters(pparameters)
{
	ValidateParameters();
}	//FeatureTrackerC(const FeatureTrackerParametersC& pparameters)

/**
 * @brief Time update
 * @param[in] problem Tracking problem
 * @return A tuple: Indices of the predicted measurements in \c tracks; Track ids; predicted measurements; prediction covariance
 * @pre \c problem is valid
 * @post Advances the state
 * @throws invalid_argument If \c problem is invalid
 * @remarks The output only contains the predictions for the tracks for which the prediction operation succeeds
 */
template<class ProblemT>
auto FeatureTrackerC<ProblemT>::Predict(ProblemT& problem) -> tuple<vector<unsigned int>, vector<unsigned int>, vector<FeatureT>, vector<InnovationCovarianceT> >
{
	if(!problem.IsValid())
		throw(invalid_argument("FeatureTrackerC::Predict : Invalid problem."));

	size_t nTracks=tracks.size();
	vector<FeatureT> featureList; featureList.reserve(nTracks);
	vector<unsigned int> indices; indices.reserve(nTracks);
	vector<unsigned int> trackIds; trackIds.reserve(nTracks);
	vector<SearchRegionT> predictionCovariances; predictionCovariances.reserve(nTracks);
	unsigned int c=0;
	for(auto& current : tracks)
	{
		//Advance the state- time update
		bool flagSuccess=get<iState>(current).PredictState(problem.KalmanProblem());

		if(!flagSuccess)
		{
			++c;
			continue;
		}

		//Predicted track
		optional<tuple<CoordinateT, SearchRegionT> > predictedMeasurement=problem.PredictMeasurement(get<iState>(current).GetState());	//Mean and innovation covariance
		if(!predictedMeasurement)
		{
			++c;
			continue;
		}

		featureList.push_back(FeatureT());
		featureList.rbegin()->Coordinate()=get<0>(*predictedMeasurement);

		auto it= parameters.flagStaticDescriptor ? get<iTrajectory>(current).begin() : prev(get<iTrajectory>(current).end(),1);
		featureList.rbegin()->RoI() = it->second.RoI();
		featureList.rbegin()->Descriptor() = it->second.Descriptor();

		predictionCovariances.push_back(get<1>(*predictedMeasurement));

		indices.push_back(c);
		trackIds.push_back(get<iTrackId>(current));

		++c;
	}	//for(auto& current : tracks)

	featureList.shrink_to_fit();
	indices.shrink_to_fit();
	trackIds.shrink_to_fit();

	tuple<vector<unsigned int>, vector<unsigned int>,  vector<FeatureT>, vector<SearchRegionT> > output;
	get<0>(output).swap(indices);
	get<1>(output).swap(trackIds);
	get<2>(output).swap(featureList);
	get<3>(output).swap(predictionCovariances);

	return output;
}	//vector<FeatureT> Predict()

/**
 * @brief Measurement update
 * @param[in] problem Tracking problem
 * @param[in] indexList Indicies of the tracks in \c tracks corresponding to the elements of \c predictedFeatures
 * @param[in] trackIds Track Ids
 * @param[in] predictedFeatures Predicted features
 * @param[in] predictionCovariances Innovation covariance for the predicted features
 * @param[in] measurements Measurements
 * @param[in] measurementCovariances Measurement covariances
 * @param[in] timeStamp Current time stamp
 * @return \c false if the state update fails for any of the tracks
 * @pre \c indexList , \c predictedFeatures and \c predictionCovariances have the same number of elements
 * @pre \c measurements and \c measurementCoariances have the same number of elements
 * @pre \c problem is valid
 * @post State is updated
 * @throws invalid_argument If \c problem is invalid
 */
template<class ProblemT>
FeatureTrackerDiagnosticsC FeatureTrackerC<ProblemT>::Update(ProblemT& problem,  const vector<unsigned int>& indexList, const vector<unsigned int>& trackIds, const vector<FeatureT>& predictedFeatures, const vector<InnovationCovarianceT>& predictionCovariances, const vector<FeatureT>& measurements, const vector<InnovationCovarianceT>& measurementCovariances, TimeStampT timeStamp)
{
	//Preconditions

	if(!problem.IsValid())
		throw(invalid_argument("FeatureTrackerC::Update : Invalid problem."));

	assert(indexList.size()==predictedFeatures.size());
	assert(indexList.size()==predictionCovariances.size());
	assert(measurements.size()==measurementCovariances.size());

	//Associate
	vector<SearchRegionT> searchRegions(predictionCovariances.size());
	InnovationCovarianceT mR=problem.ComputeMeasurementNoise(measurementCovariances);	//Measurement covariance term for the search regions
	boost::transform(predictionCovariances, searchRegions.begin(), [&](const InnovationCovarianceT& current){return (current+mR).inverse();});

	ConstraintT constraint(searchRegions, parameters.maxMahalanobisDistance, parameters.matcherParameters.nThreads, parameters.nFeaturesPerPage);

	typedef typename ProblemT::matching_problem_type MatchingProblemT;
	typedef FeatureMatcherC<MatchingProblemT> FeatureMatcherT;
	optional<MatchingProblemT> matchingProblem;
	CorrespondenceListT correspondences;
	FeatureMatcherDiagnosticsC matcherDiagnostics=FeatureMatcherT::Run(correspondences, matchingProblem, parameters.matcherParameters, problem.SimilarityMetric(), constraint, measurements, predictedFeatures);

	//Update/Delete
	FeatureTrackerDiagnosticsC diagnostics;
	bool flagSuccess=UpdateTracks(problem, diagnostics, correspondences, indexList, predictedFeatures, predictionCovariances, measurements, timeStamp);

	//Add new tracks
	CreateTracks(problem, correspondences, measurements, timeStamp);

	diagnostics.fmDiagnostics=matcherDiagnostics;
	diagnostics.flagSuccess=flagSuccess;
	diagnostics.nActiveTracks=tracks.size();
	diagnostics.nInactiveTracks=inactiveTracks.size();

	//Associations
	//Assumption:A track that receives a feature cannot be deleted
	for(const auto& current : correspondences)
		diagnostics.associations.push_back(typename CorrespondenceListT::value_type(trackIds[current.right], current.left, current.info));

	return diagnostics;
}	//bool Update(ProblemT& problem, const vector<FeatureT>& measurements, TimeStampT timeStamp)

/**
 * @brief Clears the state
 * @post State is cleared
 */
template<class ProblemT>
void FeatureTrackerC<ProblemT>::Clear()
{
	tracks.clear();
	inactiveTracks.clear();
	nextTrackId=0;
}	//void Clear()

/**
 * @brief Returns the active and inactive trajectories satisfying a filter
 * @param[in] from Initial time stamp
 * @param[in] to Final time stamp (inclusive)
 * @param[in] minMeasurement Minimum number of measurements (trajectory length)
 * @param[in] minLength Minimum track length
 * @param[in] maxMissRatio Maximum ratio of misses to the track length
 * @return A map of trajectories satisfying the filter conditions. [Track Id; Trajectory]
 * @remarks \c maxMissRatio should be (at least) slightly higher than the \c maxMissRatio of the tracker: A track retired for failing the miss ratio condition will have a 1/(length+1) excess over the threshold.
 */
template<class ProblemT>
auto FeatureTrackerC<ProblemT>::GetTrajectories(TimeStampT from, TimeStampT to, unsigned int minMeasurement, unsigned int minLength, double maxMissRatio) const -> map<unsigned int, TrajectoryT>
{
	map<unsigned int, TrajectoryT> output;

	for(const auto& current : join(tracks, inactiveTracks))
	{
		//Check the conditions
		bool flagPass= (get<iTrajectory>(current).begin()->first) >= from && (get<iTrajectory>(current).rbegin()->first) <= to;	//Interval
		flagPass = flagPass && get<iTrajectory>(current).size() >=minMeasurement;	//Minimum trajectory size
		flagPass = flagPass && get<iLength>(get<iStatistics>(current)) >=minLength;	// Trajectory length
		flagPass = flagPass && (1-(double)(get<iTrajectory>(current).size())) /get<iLength>(get<iStatistics>(current)) <= maxMissRatio;	//Miss ratio

		if(flagPass)
			output.emplace(get<iTrackId>(current), get<iTrajectory>(current));

	}	//for(const auto& current : join(tracks, inactiveTracks))

	return output;
}	//vector<TrajectoryT> GetTrajectories(TimeStampT from, TimeStampT to, unsigned int minMeasurement, unsigned int minLength, double maxMissRatio) const

/**
 * @brief Returns the ids of the active tracks
 * @return A vector containing the ids of the active tracks
 */
template<class ProblemT>
vector<unsigned int> FeatureTrackerC<ProblemT>::GetActive() const
{
	vector<unsigned int> output; output.reserve(tracks.size());

	for(const auto& current : tracks)
		output.push_back(get<iTrackId>(current));

	output.shrink_to_fit();
	return output;
}	//vector<unsigned int> GetActive()

/**
 * @brief Returns the ids of the tracks that received a measurement at a specified time
 * @param timeStamp
 * @return A vector containing the ids of the trackes that received a measurement
 */
template<class ProblemT>
vector<unsigned int> FeatureTrackerC<ProblemT>::GetObserved(TimeStampT timeStamp) const
{
	vector<unsigned int> output; output.reserve(tracks.size() + inactiveTracks.size());

	for(const auto& current : join(tracks, inactiveTracks))
		if(get<iTrajectory>(current).find(timeStamp)!=get<iTrajectory>(current).end())
			output.push_back(get<iTrackId>(current));

	output.shrink_to_fit();
	return output;
}	//vector<unsigned int> GetObserved(TimeStampT timeStamp)

/**
 * @brief Updates the tracks
 * @param[in] problem Problem
 * @param[in] measurements Measurements
 * @param[in] measurementCovariances Measurement covariances
 * @param[in] timeStamp Time stamp
 * @return Diagnostics object
 */
template<class ProblemT>
FeatureTrackerDiagnosticsC FeatureTrackerC<ProblemT>::Update(ProblemT& problem, const vector<FeatureT>& measurements, const vector<InnovationCovarianceT>& measurementCovariances, TimeStampT timeStamp)
{
	//Time update
	vector<unsigned int> indexList;
	vector<unsigned int> trackIds;
	vector<FeatureT> predictedFeatures;
	vector<InnovationCovarianceT> predictionCovariances;
	tie(indexList, trackIds, predictedFeatures, predictionCovariances)=Predict(problem);

	FeatureTrackerDiagnosticsC diagnostics=Update(problem, indexList, trackIds, predictedFeatures, predictionCovariances, measurements, measurementCovariances, timeStamp);

	return diagnostics;
}	//FeatureMatcherDiagnosticsC Update(ProblemT& problem, const vector<FeatureT>& measurements, TimeStampT timeStamp)


}	//TrackerN
}	//SeeSawN

#endif /* FEATURE_TRACKER_IPP_6898903 */
