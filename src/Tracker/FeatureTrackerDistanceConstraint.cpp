/**
 * @file FeatureTrackerDistanceConstraint.cpp Instantiations for \c FeatureTrackerDistanceConstraintC
 * @author Evren Imre
 * @date 10 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "FeatureTrackerDistanceConstraint.h"
namespace SeeSawN
{
namespace TrackerN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic> FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2>::BatchConstraintEvaluation(const vector<Coordinate2DT>&, const vector<Coordinate2DT>&) const;
template FeatureTrackerDistanceConstraintC< ValueTypeM<Coordinate2DT>::type, 2>::FeatureTrackerDistanceConstraintC(const vector<ImageFeatureTrackerDistanceConstraintT::kernel_type>&, ValueTypeM<Coordinate2DT>::type, unsigned int, unsigned int);
}	//TrackerN
}	//SeeSawN

