/**
 * @file ZeroReconstructionEvaluationModule.cpp Implementation of the performance evaluation module for 3D cloth reconstruction for Zero
 * @author Evren Imre
 * @date 9 Dec 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ZeroReconstructionEvaluationModule.h"
namespace SeeSawN
{
namespace ZeroN
{

/**
 * @brief Preprocesses the image tracks
 * @param[in] tracks Tracks
 * @param[in] nCameras Number of cameras
 * @return  A tuple: Image observations for each camera; observation tracks by observation ids; indices of the images without an associated camera
 */
auto ZeroReconstructionEvaluationModuleC::PreprocessTracks(const map<unsigned int, TrackT>& tracks, unsigned int nCameras) -> tuple<CoordinateStackT, IdTrackStackT, set<unsigned int> >
{
	tuple<CoordinateStackT, IdTrackStackT, set<unsigned int> > output;
	get<0>(output).resize(nCameras);

	size_t nPoints=tracks.size();	//Maximum number of points per camera
	for_each(get<0>(output), [&](CoordinateList2DT& current){current.reserve(nPoints);});

	vector<unsigned int> counter(nCameras,0);	//< Number of points per camera
	for(const auto& currentTrack : tracks)
	{
		IdTrackT& pTrack=get<1>(output)[currentTrack.first];	//Current id track

		for(const auto& current : currentTrack.second)
		{
			unsigned int cameraIndex=current.first;

			//If no associated cameras, skip
			if(nCameras<=cameraIndex)
			{
				get<2>(output).insert(cameraIndex);
				continue;
			}

			get<0>(output)[cameraIndex].push_back(current.second);
			pTrack.emplace(cameraIndex, counter[cameraIndex]);
			++counter[cameraIndex];
		}	//for(const auto& current : currentTrack)
	}	//for(const auto& currentTrack : trackList)

	return output;
}	//tuple<CoordinateStackT, IdTrackStackT > PreprocessTracks(const map<unsigned int, TrackT>& tracks)

/**
 * @brief Preprocesses the cameras
 * @param[in] cameraList Cameras
 * @return A tuple: Camera matrices; Distortions
 * @pre Each member of \c cameraList has a valid camera matrix
 * @throw invalid_argument If the camera matrix extraction fails for any member of \c cameraList
 */
auto ZeroReconstructionEvaluationModuleC::PreprocessCameras(const vector<CameraC>& cameraList) -> tuple<CameraMatrixListT, DistortionListT>
{
	size_t nCamera=cameraList.size();

	LensDistortionC defaultDistortion;
	defaultDistortion.kappa=0;
	defaultDistortion.modelCode=LensDistortionCodeT::NOD;

	CameraMatrixListT mPList(nCamera);
	DistortionListT distortionList(nCamera, defaultDistortion);

	for(size_t c=0; c<nCamera; ++c)
	{
		//Camera matrix

		optional<CameraMatrixT> mP=cameraList[c].MakeCameraMatrix();

		if(!mP)
			throw(invalid_argument(string("ZeroReconstructionEvaluationModuleC::PreprocessCameras : No valid camera matrix for the camera ") + lexical_cast<string>(c)));

		mPList[c]=*mP;

		//Distortion
		optional<LensDistortionC> distortion=cameraList[c].Distortion();

		if(!distortion)
			distortionList[c].centre=*cameraList[c].Intrinsics()->principalPoint;	//Valid, as otherwise mP would not exist
		else
			distortionList[c]=*distortion;
	}	//for(size_t c=0; c<nCamera; ++c)

	return make_tuple(mPList, distortionList);
}	//tuple<CameraMatrixListT, DistortionListT> PreprocessCameras(const vector<CameraC>& cameraList)

/**
 * @brief Preprocesses the input data
 * @param[in] cameraList Cameras
 * @param[in] trackList  Observation tracks
 * @return A tuple: Camera matrices; undistorted observation coordinates; observation correspondences for each camera pair; observation tracks as a sequence of indices into the coordinate containers
 */
auto ZeroReconstructionEvaluationModuleC::PreprocessData(const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList) -> tuple<CameraMatrixListT, CoordinateStackT, CorrespondenceStackT, IdTrackStackT>
{
	//Camera matrices and distortions
	CameraMatrixListT mPList;
	DistortionListT distortionList;
	tie(mPList, distortionList)=PreprocessCameras(cameraList);

	//Reformat the input
	size_t nCameras=cameraList.size();

	CoordinateStackT distortedCoordinateStack;
	IdTrackStackT tracks;
	set<unsigned int> excessImages;
	tie(distortedCoordinateStack, tracks, excessImages)=PreprocessTracks(trackList, nCameras);

	if(!excessImages.empty())
	{
		cout<<"ZeroReconstructionEvaluationModuleC::PreprocessData : No camera parameters for the images ";
		for(const auto& current : excessImages)
			cout<<current<<" ";
		cout<<"\n";
		throw(invalid_argument(""));
	}	//if(!excessImages.empty())

	//At this point, it is guaranteed that for each image, there is an associated camera
	//Apply distortion
	CoordinateStackT coordinateStack=UndistortObservations(distortedCoordinateStack, distortionList);

	//Tracks to image correspondences
	CorrespondenceStackT chainCorrespondenceStack=MakePairwiseCorrespondences(tracks);	//Correspondences from consecutive track points

	//Correspondence fusion
	CorrespondenceFusionC::unified_view_type clusters;
	CorrespondenceStackT observed;
	CorrespondenceStackT inferred;
	std::tie(clusters, observed, inferred)=CorrespondenceFusionC::Run(chainCorrespondenceStack, 2, true);

	//TODO A separate facility offered by CorrespondenceFusion
	CorrespondenceStackT pairwiseCorrespondenceStack;
	auto itE=inferred.end();
	for(auto& current : observed)
	{
		auto itCurrent=pairwiseCorrespondenceStack.emplace(current.first, current.second);

		auto it=inferred.find(current.first);
		if(it!=itE)
			itCurrent.first->second.insert(itCurrent.first->second.end(), it->second.begin(), it->second.end());
	}	//for(auto& current : pairwiseCorrespondenceStack)

	for(const auto& current : inferred)
		pairwiseCorrespondenceStack.insert(current);	//If already processed above, insertion fails.

	return make_tuple(mPList, coordinateStack, pairwiseCorrespondenceStack, tracks);
}	//tuple<vector<CameraMatrixListT>, CoordinateStackT, CorrespondenceStackT, IdTrackStackT> PreprocessData(const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList)

/**
 * @brief Builds pairwise correspondence lists from consecutive track points
 * @param[in] trackList Image tracks
 * @return Image correspondence lists
 */
auto ZeroReconstructionEvaluationModuleC::MakePairwiseCorrespondences(const IdTrackStackT& trackList) -> CorrespondenceStackT
{
	CorrespondenceStackT output;

	//For each track
	for(const auto& currentTrack : trackList)
	{
		//Add the consecutive track points as corresponding observations
		if(currentTrack.second.size()<2)
			continue;

		auto itP=currentTrack.second.cbegin();
		auto itE=currentTrack.second.cend();
		for(auto it=next(currentTrack.second.cbegin()); it!=itE; itP=it, advance(it,1))
			output[ CameraPairIdT(itP->first, it->first) ].push_back(CorrespondenceContainerT::value_type(itP->second, it->second, currentTrack.first) );

	}	//for(const auto& currentTrack : trackList)

	return output;
}	//CorrespondenceStackT MakePairwiseCorrespondences(const const map<unsigned int, TrackT>& trackList)

/**
 * @brief Undistorts the image observations
 * @param[in] distorted Distorted coordinates
 * @param[in] distortionList Lens distortion parameters
 * @return Undistorted coordinates
 * @pre \c distorted and \c distortionList have the same number of elements
 */
auto ZeroReconstructionEvaluationModuleC::UndistortObservations(const CoordinateStackT& distorted, const DistortionListT& distortionList) -> CoordinateStackT
{
	//Preconditions
	assert(distorted.size()==distortionList.size());

	size_t nCamera=distortionList.size();
	CoordinateStackT undistorted; undistorted.resize(nCamera);
	for(size_t c1=0; c1<nCamera; ++c1)
	{
		const LensDistortionC& pDistortion=distortionList[c1];

		//No distortion
		if(pDistortion.modelCode==LensDistortionCodeT::NOD || pDistortion.kappa==0)
		{
			undistorted[c1]=distorted[c1];
			continue;
		}	//if(!distortion || distortion->modelCode==LensDistortionCodeT::NOD || distortion->kappa==0)

		//Undistort
		vector<optional<Coordinate2DT> > buffer=ApplyDistortionCorrection(distorted[c1], pDistortion.modelCode, pDistortion.centre, pDistortion.kappa);

		//If distortion correction fails, revert to the original coordinates
		size_t nPoints=buffer.size();
		const CoordinateList2DT& pCurrentDistorted=distorted[c1];
		CoordinateList2DT& pCurrentUndistorted=undistorted[c1]; pCurrentUndistorted.resize(nPoints);

		for(size_t c2=0; c2<nPoints; ++c2)
			pCurrentUndistorted[c2] = buffer[c2] ? *buffer[c2] : pCurrentDistorted[c2];
	}	//for(size_t c1=0; c1<nCamera; ++c1)

	return undistorted;
}	//CoordinateStackT UndistortObservations(const CoordinateStackT& coordinateStack, const vector<CameraC>& cameraList)

/**
 * @brief Links each observation to its corresponding scene point
 * @param[in] pairwiseCorrespondences Image correspondences
 * @return Image to scene point map
 */
auto ZeroReconstructionEvaluationModuleC::MakeObservationToSceneMap(const CorrespondenceStackT& pairwiseCorrespondences) -> ObservationToScenePointMapT
{
	ObservationToScenePointMapT output;

	for(const auto& currentList : pairwiseCorrespondences)
	{
		size_t index1;
		size_t index2;
		tie(index1, index2)=currentList.first;

		for(const auto& current : currentList.second)
		{
			output.emplace(make_pair(index1, current.left), current.info);
			output.emplace(make_pair(index2, current.right), current.info);
		}	//for(const auto& current : currentList)
	}	//for(const auto& currentList : pairwiseCorrespondences)

	return output;
}	//ImageToScenePointMapT MakeImageToSceneMap(const CorrespondenceStackT& pairwiseCorrespondences)

/**
 * @brief Finds the corresponding track for each scene point
 * @param[in] inliers Observations associated with the scene points
 * @param[in] tracks Observed image tracks
 * @return 3D-track correspondences
 */
auto ZeroReconstructionEvaluationModuleC::FindPointTrackCorrespondences(const CorrespondenceStackT& inliers, const IdTrackStackT& tracks) -> CorrespondenceListT
{
	CorrespondenceListT output;

	ObservationToScenePointMapT observationToSceneMap=MakeObservationToSceneMap(inliers);

	MatchStrengthC defaultStrength(1,0);

	auto itE=observationToSceneMap.end();
	for(const auto& currentTrack : tracks)
		for(const auto& current : currentTrack.second)
		{
			auto it=observationToSceneMap.find(current);

			//Track associated. Move to the next.
			if(it!=itE)
			{
				output.push_back(CorrespondenceListT::value_type(it->second, currentTrack.first, defaultStrength));
				break;
			}	//if(it!=itE)
		}	//for(const auto& current : currentTrack)

	return output;
}	//CorrespondenceListT FindPointTrackCorrespondences(const CorrespondenceStackT& inliers, const IdTrackStackT& tracks)

/**
 * @brief Computes the reprojection error for image observations
 * @param[in] pointCloud 3D point cloud
 * @param[in] trackList Image observation tracks
 * @param[in] correspondences Point-to-track correspondences, as indices into \c point cloud and \c trackList
 * @param[in] cameraList Cameras
 * @return Reprojection errors for each observation in each camera
 */
vector<vector<double> > ZeroReconstructionEvaluationModuleC::ComputeReprojectionError(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, TrackT>& trackList, const CorrespondenceListT& correspondences, const vector<CameraC>& cameraList)
{
	CameraMatrixListT mPList;
	DistortionListT distortionList;
	tie(mPList, distortionList)=PreprocessCameras(cameraList);

	size_t nCamera=cameraList.size();
	vector<Projection32C> projectors(nCamera);
	transform(mPList, projectors.begin(), [](const CameraMatrixT& current){return Projection32C(current);});

	size_t nPoint=trackList.rbegin()->first;
	vector<vector<double> > output(nCamera);
	for_each(output, [&](vector<double>& current){current.reserve(nPoint);});

	//For each Point-track correspondence
	for(const auto& currentCorrespondence : correspondences)
		//For each observation
		for(const auto& current : trackList.find(currentCorrespondence.right)->second)
		{
			size_t cameraIndex=current.first;
			optional<Coordinate2DT> projected=projectors[cameraIndex](pointCloud[currentCorrespondence.left], distortionList[cameraIndex]);

			if(projected)
				output[cameraIndex].push_back( (*projected-current.second).norm());
		}	//for(const auto& current : trackList.find(currentCorrespondence.right)->second)

	for_each(output, [&](vector<double>& current){current.shrink_to_fit();});

	return output;
}	//vector<vector<double> > ComputeReprojectionError(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, TrackT>& trackList, const CorrespondenceListT& correspondences, const vector<CameraC>& cameraList)

/**
 * @brief Computes the statistics for an error vector
 * @param[in] errorList Error vector. Pass by value on purpose
 * @param[in] noiseVariance Observation noise variance
 * @param[in] dof Degrees of freedom for chi^2 distribution
 * @return A tuple: Error statistics; Bhattacharyya coefficient
 * @remarks If \c noiseVariance==0 the Bhattacharyya coefficient is set to 0
 */
auto ZeroReconstructionEvaluationModuleC::ComputeErrorStatistics(vector<double> errorList, double noiseVariance, unsigned int dof) -> ErrorStatisticsT
{
	ErrorStatisticsT stats;

	sort(errorList);	//Sort the errors

	size_t nPoints=errorList.size();
	stats[iCount]=nPoints;

	//Order statistics
	stats[iQ5]=errorList[ floor(0.05*nPoints) ];
	stats[iQ25]=errorList[ floor(0.25*nPoints) ];
	stats[iQ50]=errorList[ floor(0.5*nPoints) ];
	stats[iQ75]=errorList[ floor(0.75*nPoints) ];
	stats[iQ95]=errorList[ floor(0.95*nPoints) ];

	//Mean and variance
	stats[iMean]=accumulate(errorList, 0.0)/nPoints;
	stats[iStd]=sqrt(accumulate(errorList, 0.0, [](double acc, double current){return acc+pow<2>(current);})/nPoints - pow<2>(stats[iMean]) );

	if(noiseVariance==0)
	{
		stats[iBC]=0;
		return stats;
	}

	//Error histogram
	accumulator_set<double, features<accN::tag::density> > densityAcc(accN::tag::density::num_bins=100, accN::tag::density::cache_size=2);
	densityAcc(0);
	densityAcc(2*quantile(chi_squared(2), 0.99));
	for_each(errorList, [&](double current){densityAcc(pow<2>(current)/noiseVariance);});	//Error values are normalised by the noise variance as chi^2 is defined for std=1

	iterator_range<vector<pair<double, double> >::iterator> tmp=accN::density(densityAcc);
	vector<pair<double, double> > empricalPMF(next(tmp.begin()), prev(tmp.end()));	//Excluding the overflow bins

	//Theoretical PMF
	chi_squared chi2(dof);
	size_t nBins=empricalPMF.size();
	vector<double> chi2PMF; chi2PMF.reserve(nBins);
	size_t nBinsm1=nBins-1;
	for(size_t c=0; c<nBinsm1; ++c)
	{
		double prob=cdf(chi2, empricalPMF[c+1].first) - cdf(chi2, empricalPMF[c].first);
		chi2PMF.push_back(prob);
	}	//for(size_t c=1; c<nBins-1; ++c)

	chi2PMF.push_back(cdf(chi2, tmp[nBins+1].first) - cdf(chi2, empricalPMF[nBins-1].first));	//empricalPMF does not have the left boundary of the last bin

	//Bhattacharyya coefficient
	stats[iBC]=inner_product(chi2PMF, empricalPMF, 0.0, plus<double>(), [](double val1, pair<double, double> val2 ){ return sqrt(val1*val2.second);  }  );

	return stats;
}	//ImageErrorStatisticsT ComputeReprojectionErrorStatistics(const vector<double>& errorList)

/**
 * @brief Filters the input data to build a valid input for multiview triangulation
 * @param[in] cameraIds Ids to be included in the output
 * @param[in] cameraList Camera list
 * @param[in] coordinateStack Observations coordinates
 * @param[in] correspondenceStack Observation correspondences
 * @return A tuple holding the data corresponding to the specified indices: Camera matrices; coordinates; correspondences.
 * @pre For each id, there is a valid camera matrix
 * @remarks The function modifies the pair ids in the correspondence stack
 */
auto ZeroReconstructionEvaluationModuleC::FilterData(const set<unsigned int>& cameraIds, const CameraMatrixListT cameraList, const CoordinateStackT& coordinateStack, const CorrespondenceStackT& correspondenceStack) -> tuple<CameraMatrixListT, CoordinateStackT, CorrespondenceStackT>
{
	//Preconditions
	assert(cameraIds.rbegin()<=cameraList.size());

	size_t nFiltered=cameraIds.size();
	CameraMatrixListT filteredCameras; filteredCameras.reserve(nFiltered);
	CoordinateStackT filteredCoordinates; filteredCoordinates.reserve(nFiltered);
	vector<int> mapToFilteredIds(cameraList.size(), -1);
	size_t index=0;
	for(const auto current : cameraIds)
	{
		mapToFilteredIds[current]=index;
		filteredCameras.push_back(cameraList[current]);
		filteredCoordinates.push_back(coordinateStack[current]);
		++index;
	}	//for(const auto current : cameraIds)s

	CorrespondenceStackT filteredCorrespondences;
	for(const auto& current : correspondenceStack)
	{
		int i1=mapToFilteredIds[get<0>(current.first)];
		int i2=mapToFilteredIds[get<1>(current.first)];

		if(i1==-1 || i2==-1)
			continue;

		filteredCorrespondences.emplace(CameraPairIdT(i1, i2), current.second);
	}	//for(const auto& current : correspondenceStack)

	return make_tuple(filteredCameras, filteredCoordinates, filteredCorrespondences);
}	//tuple<CameraMatrixListT, CoordinateStackT, CorrespondenceStackT> FilterData(const set<unsigned int>& cameraIds, const CameraMatrixListT cameraList, const CoordinateStackT& coordinateStack, const CorrespondenceStackT& correspondenceStack)

/**
 * @brief Reconstructs a 3D model of the scene from the specified cameras
 * @param[in] cameraIds	Camera indices
 * @param[in] cameraList Camera matrices
 * @param[in] coordinateStack Observation coordinates
 * @param[in] correspondenceStack Observation correspondences
 * @param[in] parameters Triangulation parameters
 * @return A tuple: Point cloud; 3D-2D correspondences; mean per-coordinate variance
 */
auto ZeroReconstructionEvaluationModuleC::ReconstructModel(const set<unsigned int>& cameraIds, const CameraMatrixListT& cameraList, const CoordinateStackT& coordinateStack, const CorrespondenceStackT& correspondenceStack, const MultiviewTriangulationParametersC& parameters) -> tuple<CoordinateList3DT, CorrespondenceStackT, double>
{
	//Prepare the input data
	CameraMatrixListT filteredCameras;
	CoordinateStackT filteredCoordinates;
	CorrespondenceStackT filteredCorrespondences;
	std::tie(filteredCameras, filteredCoordinates, filteredCorrespondences)=FilterData(cameraIds, cameraList, coordinateStack, correspondenceStack);

	//Triangulate
	typedef MultiviewTriangulationC::covariance_matrix_type PointCovarianceMatrixT;

	vector<Coordinate3DT> pointCloud;
	vector<PointCovarianceMatrixT> covarianceList;
	CorrespondenceStackT inliers;
	MultiviewTriangulationDiagnosticsC diagnostics=MultiviewTriangulationC::Run(pointCloud, covarianceList, inliers, filteredCorrespondences, filteredCameras, filteredCoordinates, parameters);

	tuple<CoordinateList3DT, CorrespondenceStackT, double> output;
	if(!diagnostics.flagSuccess)
		return output;

	//Revert the camera ids
	vector<unsigned int> toOriginalIds(cameraIds.cbegin(), cameraIds.cend());
	CorrespondenceStackT reverted;
	for(auto& current : inliers)
		reverted[CameraPairIdT(toOriginalIds[get<0>(current.first)], toOriginalIds[get<1>(current.first)]) ].swap(current.second);

	//Compute the average covariance
	double avgCov=accumulate(covarianceList, 0.0, [](double acc, const PointCovarianceMatrixT& current){return acc+(current.trace())/3; })/covarianceList.size();

	return make_tuple(pointCloud, reverted, avgCov);
}	//tuple<CoordinateList3DT, CorrespondenceStackT, double> ReconstructModel(const CameraMatrixListT& cameraList, const CoordinateStackT& coordinateStack, const CorrespondenceStackT& correspondenceStack, const MultiviewTriangulationParametersC& parameters)

/**
 * @brief Computes the similarity transformation between the models
 * @param[out] mH Estimated similarity transformation
 * @param[in] correspondences 3D correspondences
 * @param[in] binSize1 Bin size for the first set
 * @param[in] binSize2 Bin size for the second set
 * @param[in] noiseVariance1 Noise variance for the first set
 * @param[in] noiseVariance2 Noise variance for the second set
 * @param[in] seed Random number seed
 * @param[in] parameters Parameters
 * @return Diagnostics object
 */
GeometryEstimatorDiagnosticsC ZeroReconstructionEvaluationModuleC::ComputeSimilarityTransformation(Homography3DT& mH, const CoordinateCorrespondenceList3DT& correspondences, const BinT& binSize1, const BinT& binSize2, double noiseVariance1, double noiseVariance2, unsigned int seed, const GeometryEstimatorParametersC& parameters)
{
	//RANSAC problem

	//TODO to the interface?
	unsigned int loGeneratorRatio=3;
	double inlierRejectionProbability=0.05;

	unsigned int loGeneratorSize=Similarity3DSolverC::GeneratorSize()*loGeneratorRatio;
	double effectiveNoise=0.5*(noiseVariance1+noiseVariance2);

	typedef RANSACSimilarity3DProblemT RANSACProblemT;
	RANSACProblemT ransacProblem1=MakeRANSACSimilarity3DProblem(correspondences, correspondences, effectiveNoise, inlierRejectionProbability, 1, loGeneratorSize, binSize1, binSize2);	//Max algebraic distance is 0, as no initial estimate exists- so MORANSAC is disabled
	RANSACProblemT ransacProblem2=MakeRANSACSimilarity3DProblem(correspondences, correspondences, effectiveNoise, inlierRejectionProbability, 1, loGeneratorSize, binSize1, binSize2);

	//PDL problem
	RANSACProblemT::data_container_type dummyList;
	PDLSimilarity3DEstimationProblemC optimisationProblem=MakePDLSimilarity3DProblem(Homography3DT::Zero(), dummyList, optional<MatrixXd>());

	//Geometry estimation problem
	Similarity3DEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, optional<Homography3DT>());

	Similarity3DEstimatorT::rng_type rng;
	rng.seed(seed);

	vector<size_t> generatorSet;
	vector<size_t> inliers;
	GeometryEstimatorDiagnosticsC diagnostics=Similarity3DEstimatorT::Run(mH, inliers, generatorSet, geometryEstimationProblem, rng, parameters);

	return diagnostics;
}	//GeometryEstimatorDiagnosticsC ComputeSimilarityTransformation(Homography3DT& mH, const CoordinateCorrespondenceList3DT& correspondences, const BinT& binSize1, const BinT& binSize2, double noiseVariance1, double noiseVariance2, unsigned int seed, const GeometryEstimatorParametersC& parameters)s

/**
 * @brief Performs a multiview 3D reconstruction from image observations
 * @param[in] cameraList Cameras
 * @param[in] trackList Image feature tracks
 * @param[in] parameters Triangulation parameters
 * @return A tuple: Point cloud; Point-track correspondences
 * @pre For each image, there is an associated camera
 * @throws invalid_argument If an image does not have a corresponding camera
 */
auto ZeroReconstructionEvaluationModuleC::Reconstruct(const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList, const MultiviewTriangulationParametersC& parameters ) -> tuple<vector<Coordinate3DT>, CorrespondenceListT>
{
	tuple<vector<Coordinate3DT>, CorrespondenceListT> output;

	//No cameras, no triangulation
	if(cameraList.empty())
		return output;

	//Preprocess the data
	CameraMatrixListT mPList;
	CoordinateStackT coordinateStack;
	CorrespondenceStackT pairwiseCorrespondenceStack;
	IdTrackStackT tracks;
	std::tie(mPList, coordinateStack, pairwiseCorrespondenceStack, tracks)=PreprocessData(cameraList, trackList);

	//3D reconstruction
	vector<Coordinate3DT> pointCloud;
	vector<MultiviewTriangulationC::covariance_matrix_type> covarianceList;
	CorrespondenceStackT inliers;
	MultiviewTriangulationDiagnosticsC diagnostics=MultiviewTriangulationC::Run(pointCloud, covarianceList, inliers, pairwiseCorrespondenceStack, mPList, coordinateStack, parameters);

	if(!diagnostics.flagSuccess)
		return output;

	//Point-track correspondences
	get<1>(output)=FindPointTrackCorrespondences(inliers, tracks);
	get<0>(output).swap(pointCloud);

	return output;
}	//tuple<vector<Coordinate3DT>, CorrespondenceListT> Reconstruct(const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList, const MultiviewTriangulationParametersC& parameters )

/**
 * @brief Evaluates the image error
 * @param[in] pointCloud 3D point cloud
 * @param[in] trackList Image observation tracks
 * @param[in] correspondences Point-to-track correspondences, as indices into \c point cloud and \c trackList
 * @param[in] cameraList Cameras
 * @param[in] noiseVariance Variance of the noise on the observation coordinates
 * @return A map: Camera id; [Error stats, reprojection errrors]
 * @pre For each image, there is an associated camera (unenforced)
 * @pre Each element of \c correspondences contains a valid index into \c pointCloud and \c trackList (unenforced)
 * @remarks If the lens distortion operator fails, the corresponding scene point is excluded from the evaluation
 */
auto ZeroReconstructionEvaluationModuleC::EvaluateImageError(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, TrackT>& trackList, const CorrespondenceListT& correspondences, const vector<CameraC>& cameraList, double noiseVariance) -> map<unsigned int, tuple<ErrorStatisticsT, vector<double> > >
{
	//Compute the reprojection error
	vector<vector<double> > errorStack=ComputeReprojectionError(pointCloud, trackList, correspondences, cameraList);

	//Compute the statistics
	size_t nCamera=cameraList.size();
	map<unsigned int, tuple<ErrorStatisticsT, vector<double> > > output;
	for(size_t c=0; c<nCamera; ++c)
	{
		ErrorStatisticsT stats=ComputeErrorStatistics(errorStack[c], noiseVariance, 2);
		output.emplace(c, make_tuple(stats, errorStack[c]) );
	}	//for(size_t c=0; c<nCamera; ++c)

	return output;
}	//void EvaluateImageError(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, TrackT>& trackList, const CorrespondenceListT& correspondences, const vector<CameraC>& cameraList)

/**
 * @brief Evaluates the consistency of reconstructions
 * @param[in] reference Camera ids for the reference structure
 * @param[in] sourceList Camera ids for the structures to be compared against the reference
 * @param[in] cameraList Camera list
 * @param[in] trackList Observation tracks
 * @param[in] triangulationParameters Triangulation parameters
 * @param[in] geometryParameters Similarity matrix estimation parameters
 * @return A map of error statistics: [Id; transfer error; deviation; symmetric transfer errors]
 * @pre \c reference has at least two elements
 * @pre \c sourceList is a vector holding sets with at least two elements
 */
auto ZeroReconstructionEvaluationModuleC::EvaluateConsistency(const set<unsigned int>& reference, const vector<set<unsigned int> >& sourceList, const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList, const MultiviewTriangulationParametersC& triangulationParameters, const GeometryEstimatorParametersC& geometryParameters ) -> map<unsigned int, tuple<ErrorStatisticsT, DeviationT, vector<double>, double > >
{
	//Preconditions
	assert(reference.size()>=2);
	assert(all_of(sourceList.cbegin(), sourceList.cend(), [](const set<unsigned int>& current){return current.size()>=2;}));

	map<unsigned int, tuple<ErrorStatisticsT, DeviationT, vector<double>, double> > output;

	if(sourceList.empty())
		return output;

	//Preprocess the data
	CameraMatrixListT mPList;
	CoordinateStackT coordinateStack;
	CorrespondenceStackT pairwiseCorrespondenceStack;
	IdTrackStackT idTracks;
	std::tie(mPList, coordinateStack, pairwiseCorrespondenceStack, idTracks)=PreprocessData(cameraList, trackList);

	//Reference reconstruction
	CoordinateList3DT referenceStructure;
	CorrespondenceStackT referenceInliers;
	double referenceNoise;
	std::tie(referenceStructure, referenceInliers, referenceNoise)=ReconstructModel(reference, mPList, coordinateStack, pairwiseCorrespondenceStack, triangulationParameters);

	double binDensity=4;	//TODO to the interface?
	optional<BinT> binSize1=ComputeBinSize<BinT>(referenceStructure, binDensity);

	if(!binSize1 || referenceStructure.empty())
		return output;

	//Track to reference map
	CorrespondenceListT referenceTrackCorrespondences=FindPointTrackCorrespondences(referenceInliers, idTracks);
	map<unsigned int, unsigned int> trackToReferenceMap;
	for_each(referenceTrackCorrespondences, [&](const CorrespondenceListT::value_type& current){trackToReferenceMap.emplace(current.right, current.left);} );

	//Evaluate the consistency with the elements of sourceList
	unsigned int index=0;
	for(const auto& current : sourceList)
	{

		//3D reconstruction
		CoordinateList3DT pointCloud;
		CorrespondenceStackT inlierObservations;
		double noise;
		std::tie(pointCloud, inlierObservations, noise)=ReconstructModel(current, mPList, coordinateStack, pairwiseCorrespondenceStack, triangulationParameters);

		if(pointCloud.empty())
			continue;

		CorrespondenceListT sceneTrackCorrespondences=FindPointTrackCorrespondences(inlierObservations, idTracks);

		//3D-3D correspondences
		auto itE=trackToReferenceMap.end();
		CoordinateCorrespondenceList3DT sceneCorrespondences;
		for(const auto& currentCorrespondence : sceneTrackCorrespondences)
		{
			auto it=trackToReferenceMap.find(currentCorrespondence.right);
			if(it!=itE)
				sceneCorrespondences.push_back(CoordinateCorrespondenceList3DT::value_type(referenceStructure[it->second], pointCloud[currentCorrespondence.left]) );
		}	//for(const auto& currentCorrespondence : sceneTrackCorrespondences)

		//Estimate the similarity transformation between the point clouds

		optional<BinT> binSize2=ComputeBinSize<BinT>(pointCloud, binDensity);

		if(!binSize2)
			continue;

		Homography3DT mH;
		GeometryEstimatorDiagnosticsC geDiagnostics=ComputeSimilarityTransformation(mH, sceneCorrespondences, *binSize1, *binSize2, referenceNoise, noise, index, geometryParameters);

		if(!geDiagnostics.flagSuccess)
			continue;

		//Evaluation

		//Eulidean distance. Untransformed points, as the ground-truth transformation is the identity
		vector<double> errorList(sceneCorrespondences.size());
		transform(sceneCorrespondences, errorList.begin(), [&](const CoordinateCorrespondenceList3DT::value_type& current){return (current.left-current.right).norm(); } );
		ErrorStatisticsT errorStat=ComputeErrorStatistics(errorList, referenceNoise+noise, 6);

		//Deviation of the transformation from identity
		double scale;
		Coordinate3DT offset;
		RotationMatrix3DT rotation;
		std::tie(scale, offset, rotation)=DecomposeSimilarity3D(mH);
		DeviationT deviation{scale-1, offset.norm(), RotationMatrixToRotationVector(rotation).norm()};

		output.emplace(index, make_tuple(errorStat, deviation, errorList, referenceNoise+noise) );
		++index;
	}	//for(const auto& current : sourceList)

	return output;
}	//void ZeroReconstructionEvaluationModuleC::EvaluateConsistency(const set<unsigned int>& reference, const vector<set<unsigned int> >& sourceList, const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList, const MultiviewTriangulationParametersC& parameters, )

}	//ZeroN
}	//SeeSawN

