/**
 * @file ZeroReconstructionEvaluationModule.h Public interface for the performance evaluation module for 3D cloth reconstruction for Zero
 * @author Evren Imre
 * @date 9 Dec 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ZERO_RECONSTRUCTION_EVALUATION_MODULE_H_9078122
#define ZERO_RECONSTRUCTION_EVALUATION_MODULE_H_9078122

#include "ZeroReconstructionEvaluationModule.ipp"

namespace SeeSawN
{
namespace ZeroN
{

class ZeroReconstructionEvaluationModuleC;	///< Metrics and tools for the evaluation of the 3D reconstruction accuracy for Zero
}	//ZeroN
}	//SeeSawN

#endif /* ZERO_RECONSTRUCTION_EVALUATION_MODULE_H_9078122 */
