/**
 * @file ZeroReconstructionEvaluationModule.ipp Implementation of the performance evaluation module for 3D cloth reconstruction for Zero
 * @author Evren Imre
 * @date 9 Dec 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ZERO_RECONSTRUCTION_EVALUATION_MODULE_IPP_1790289
#define ZERO_RECONSTRUCTION_EVALUATION_MODULE_IPP_1790289

#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/numeric.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/density.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <Eigen/Dense>
#include <vector>
#include <map>
#include <set>
#include <array>
#include <tuple>
#include <iterator>
#include <cstddef>
#include <utility>
#include <string>
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <functional>
#include <algorithm>
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Geometry/MultiviewTriangulation.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/Projection32.h"
#include "../Geometry/Similarity3DSolver.h"
#include "../Geometry/Rotation.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationComponents/Similarity3DComponents.h"
#include "../Matcher/CorrespondenceFusion.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"

namespace SeeSawN
{
namespace ZeroN
{

namespace accN=boost::accumulators;

using boost::optional;
using boost::for_each;
using boost::transform;
using boost::sort;
using boost::iterator_range;
using boost::accumulate;
using boost::inner_product;
using boost::lexical_cast;
using boost::accumulators::accumulator_set;
using boost::accumulators::features;
using boost::accumulators::density;
using boost::math::chi_squared;
using boost::math::pow;
using Eigen::MatrixXd;
using std::vector;
using std::map;
using std::set;
using std::multiset;
using std::array;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::get;
using std::next;
using std::prev;
using std::advance;
using std::size_t;
using std::pair;
using std::make_pair;
using std::cout;
using std::string;
using std::invalid_argument;
using std::floor;
using std::sqrt;
using std::plus;
using std::all_of;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::GeometryN::MultiviewTriangulationC;
using SeeSawN::GeometryN::MultiviewTriangulationParametersC;
using SeeSawN::GeometryN::MultiviewTriangulationDiagnosticsC;
using SeeSawN::GeometryN::ApplyDistortionCorrection;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::GeometryN::GeometryEstimatorDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimatorParametersC;
using SeeSawN::GeometryN::Similarity3DEstimatorT;
using SeeSawN::GeometryN::Similarity3DEstimatorProblemT;
using SeeSawN::GeometryN::RANSACSimilarity3DProblemT;
using SeeSawN::GeometryN::PDLSimilarity3DEstimationProblemC;
using SeeSawN::GeometryN::MakePDLSimilarity3DProblem;
using SeeSawN::GeometryN::MakeRANSACSimilarity3DProblem;
using SeeSawN::GeometryN::Similarity3DSolverC;
using SeeSawN::GeometryN::RotationMatrixToRotationVector;
using SeeSawN::MatcherN::CorrespondenceFusionC;
using SeeSawN::RANSACN::ComputeBinSize;

/**
 * @brief Metrics and tools for the evaluation of the 3D reconstruction accuracy for Zero
 * @ingroup Algorithm
 * @nosubgrouping
 */
class ZeroReconstructionEvaluationModuleC
{
	public:

		/** @name Error statistics structure */ //@{
		typedef array<double, 9> error_statistics_type;	///< Object keeping the error statistics
		static constexpr unsigned int iCount=0;	///< Index of the point count component
		static constexpr unsigned int iMean=1;	///< Index of the mean reprojection error component
		static constexpr unsigned int iStd=2;	///< Index of the std of the reprojection error
		static constexpr unsigned int iQ5=3;	///< Index of the 5th quantile of the reprojection error
		static constexpr unsigned int iQ25=4;	///< Index of the 25th quantile of the reprojection error
		static constexpr unsigned int iQ50=5;	///< Index of the 50th quantile of the reprojection error
		static constexpr unsigned int iQ75=6;	///< Index of the 75th quantile of the reprojection error
		static constexpr unsigned int iQ95=7;	///< Index of the 95th quantile of the reprojection error
		static constexpr unsigned int iBC=8;	///< Index of the Bhattacharyya coefficient component
		//@}

		/** @name Transformation error structure */ //@{
		typedef array<double,3> deviation_type;	///< Type keeping the transformation error. [Scale-1; norm of the offset; rotation angle in rads]
		static constexpr unsigned int iScale=0;	///< Index of the scale component
		static constexpr unsigned int iOffset=1;	///< Index of the offset component
		static constexpr unsigned int iRotation=2;	///< Index of the rotation component
		//@}
	private:

		typedef error_statistics_type ErrorStatisticsT;
		typedef deviation_type DeviationT;

		typedef map<unsigned int, Coordinate2DT> TrackT;	///< An image observation track. Image id; coordinates

		typedef map<unsigned int, unsigned int> IdTrackT;	///< An image observation track, as observation ids. Image id; observation id
		typedef map<unsigned int, IdTrackT> IdTrackStackT;	///< A stack of \c IdTrackT objects

		typedef CorrespondenceFusionC::pair_id_type CameraPairIdT;	///< Type identifying a camera pair
		typedef CorrespondenceFusionC::correspondence_container_type CorrespondenceContainerT;	///< A correspondence container
		typedef CorrespondenceFusionC::pairwise_view_type<CorrespondenceContainerT> CorrespondenceStackT;	///< A stack of pairwise correspondences

		typedef vector<Coordinate2DT> CoordinateList2DT;	///< A 2D coordinate list
		typedef vector<CoordinateList2DT> CoordinateStackT;	///< A 2D coordinate stack

		typedef vector<Coordinate3DT> CoordinateList3DT;	///< A 3D coordinate stack

		typedef vector<CameraMatrixT> CameraMatrixListT;	///< A list of camera matrices
		typedef vector<LensDistortionC> DistortionListT;	///< A list of lens distortions

		/** @name Implementation details */ //@{
		static tuple<CoordinateStackT, IdTrackStackT, set<unsigned int> > PreprocessTracks(const map<unsigned int, TrackT>& tracks, unsigned int nCameras);	///< Preprocesses the image tracks
		static tuple<CameraMatrixListT, DistortionListT> PreprocessCameras(const vector<CameraC>& cameraList);	///< Preprocesses the cameras
		static tuple<CameraMatrixListT, CoordinateStackT, CorrespondenceStackT, IdTrackStackT> PreprocessData(const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList);	///< Preprocesses the input data

		static CorrespondenceStackT MakePairwiseCorrespondences(const IdTrackStackT& trackList);	///< Builds pairwise correspondence lists from consecutive track points

		static CoordinateStackT UndistortObservations(const CoordinateStackT& distorted, const DistortionListT& distortionList);	///< Undistorts the image observations

		typedef map< pair<unsigned int, unsigned int>, unsigned int> ObservationToScenePointMapT;	///< A map linking the observations to the scene points. [Camera; observation id]; Scene point id
		static ObservationToScenePointMapT MakeObservationToSceneMap(const CorrespondenceStackT& pairwiseCorrespondences);	///< Links each observation to its corresponding scene point
		static CorrespondenceListT FindPointTrackCorrespondences(const CorrespondenceStackT& inliers, const IdTrackStackT& tracks);	///< Finds the corresponding track for each scene point

		static vector<vector<double> > ComputeReprojectionError(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, TrackT>& trackList, const CorrespondenceListT& correspondences, const vector<CameraC>& cameraList);	///< Computes the reprojection error for image observations

		static tuple<CameraMatrixListT, CoordinateStackT, CorrespondenceStackT> FilterData(const set<unsigned int>& cameraIds, const CameraMatrixListT cameraList, const CoordinateStackT& coordinateStack, const CorrespondenceStackT& correspondenceStack);	///< Filters the input data to build a valid input for multiview triangulation
		static tuple<CoordinateList3DT, CorrespondenceStackT, double> ReconstructModel(const set<unsigned int>& cameraIds, const CameraMatrixListT& cameraList, const CoordinateStackT& coordinateStack, const CorrespondenceStackT& correspondenceStack, const MultiviewTriangulationParametersC& parameters);	///< Reconstructs a 3D model of the scene from the specified cameras

		typedef RANSACSimilarity3DProblemT::dimension_type1 BinT;	///< RANSAC bin type for 3D points
		static GeometryEstimatorDiagnosticsC ComputeSimilarityTransformation(Homography3DT& mH, const CoordinateCorrespondenceList3DT& correspondences, const BinT& binSize1, const BinT& binSize2, double noiseVariance1, double noiseVariance2, unsigned int seed, const GeometryEstimatorParametersC& parameters);	///< Computes the similarity transformation between the models
		//@}

	public:

		typedef TrackT track_type;	///< An image track

		/** @name Operations */ //@{
		static tuple<vector<Coordinate3DT>, CorrespondenceListT> Reconstruct(const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList, const MultiviewTriangulationParametersC& parameters );	///< Performs a multiview 3D reconstruction from image observations
		static map<unsigned int, tuple<ErrorStatisticsT, vector<double> > > EvaluateImageError(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, TrackT>& trackList, const CorrespondenceListT& correspondences, const vector<CameraC>& cameraList, double noiseVariance);	///< Evaluates the image error
		static map<unsigned int, tuple<ErrorStatisticsT, DeviationT, vector<double>, double > > EvaluateConsistency(const set<unsigned int>& reference, const vector<set<unsigned int> >& sourceList, const vector<CameraC>& cameraList, const map<unsigned int, TrackT>& trackList, const MultiviewTriangulationParametersC& triangulationParameters, const GeometryEstimatorParametersC& geometryParameters );	///< Evaluates the consistency of reconstructions
		//@}

		/**@name Utilities */	//@{
		static ErrorStatisticsT ComputeErrorStatistics(vector<double> errorList, double noiseVariance, unsigned int dof);	///< Computes the statistics for an error vector
		//@}

};	//class ZeroReconstructionEvaluationModuleC

}	//ZeroN
}	//SeeSawN

#endif /* ZERO_RECONSTRUCTION_EVALUATION_MODULE_IPP_1790289 */
