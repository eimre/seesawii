/**
 * @file AppZeroReconstructionEvaluation.cpp ZERO reconstruction evaluation application
 * @author Evren Imre
 * @date 11 Dec 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "../Application/Application.h"
#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <fstream>
#include <functional>
#include <cmath>
#include <climits>
#include "ZeroReconstructionEvaluationModule.h"
#include "../ApplicationInterface/InterfaceUtility.h"
#include "../Geometry/MultiviewTriangulation.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../Metrics/GeometricError.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../IO/CameraIO.h"
#include "../IO/ArrayIO.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Correspondence.h"

namespace SeeSawN
{
namespace AppZeroReconstructionEvaluationN
{

using namespace ApplicationN;

using boost::str;
using boost::format;
using boost::optional;
using boost::for_each;
using Eigen::ArrayXXd;
using std::ofstream;
using std::bind;
using std::greater;
using std::max;
using std::numeric_limits;
using SeeSawN::ZeroN::ZeroReconstructionEvaluationModuleC;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::GeometryN::MultiviewTriangulationParametersC;
using SeeSawN::GeometryN::GeometryEstimatorParametersC;
using SeeSawN::MetricsN::TransferErrorH32DT;

/**
 * @brief Parameters for \c zeroReconstructionEvaluation
 * @ingroup Application
 */
struct AppZeroReconstructionEvaluationParametersC
{
	unsigned int nThreads;	///< Number of threads available to the application.

	//Reprojection error statistics
	bool flagReprojectionError;	///< If \c true reprojection error analysis is enabled
	double minNoiseVariance;	///< Variance of the coordinate noise for the image observations, assuming perfect blob detection

	//Consistency statistics
	bool flagConsistency;	///< If \c true consistency analysis is enabled
	set<unsigned int> referenceSet;	///< Ids for the reference cameras
	vector<set<unsigned int> > sourceSets;	///< Ids for the source cameras

	unsigned int initialFrame;	///< Id of the first first frame
	unsigned int nFrames;	///< Number of frames in the sequence
	string observationPattern;	///< Filename pattern for reading the image observation files. Should have two printf-style tokens
	string cameraFile;	///< Filename for the cameras

	string outputRoot;	///< Root directory for the output

	MultiviewTriangulationParametersC triangulationParameters;	///< Parameters for the multiview triangulation algorithm
	GeometryEstimatorParametersC geometryEstimatorParameters;		///< Parameters for the geometry estimation pipeline

	AppZeroReconstructionEvaluationParametersC() : nThreads(1), flagReprojectionError(true), minNoiseVariance(0.25/3), flagConsistency(true), initialFrame(0), nFrames(1)
	{}
};	//AppZeroReconstructionEvaluationNParametersC

/**
 * @brief Implementation of \c zeroReconstructionEvaluation
 * @ingroup Application
 */
class  AppZeroReconstructionEvaluationImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed
		AppZeroReconstructionEvaluationParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string,double> logger;	///< Logs various values
		//@}

		/** @name Implementation details */ //@{
		typedef ZeroReconstructionEvaluationModuleC::track_type TrackT;	///< An observation track
		tuple<map<unsigned int, TrackT>, unsigned int > LoadObservations(unsigned int frameId, unsigned int nCameras);	///< Loads the image observations

		void VerifyConsistencyParameters(unsigned int nCamera);	///< Verifies the parameters for consistency analysis

		void SaveLog();	///< Saves the log

		vector<Coordinate3DT> MakeExportPointCloud(const vector<Coordinate3DT>& pointCloud, const CorrespondenceListT& trackCorrespondences, unsigned int sArray);	///< The exported version of the point cloud, with unsuccessful tracks marked
		typedef ZeroReconstructionEvaluationModuleC::error_statistics_type ErrorStatisticsT;	///< Type for the error statistics structure
		typedef ZeroReconstructionEvaluationModuleC::deviation_type DeviationT;	///< Type for the deviation measure from identity transformation
		void SaveOutput(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, tuple<ErrorStatisticsT, vector<double>>>& reprojectionStatistics, const map<unsigned int, tuple<ErrorStatisticsT, DeviationT, vector<double>, double>>& consistencyStatistics, unsigned int frameId);	///< Saves the per frame output
		void SaveAggregate(const vector<list<double>>& reprojectionErrorStack, const vector<list<double>>& symmetricTransferErrorStack , const vector<list<double>>& noiseStack, const vector<array<list<double>,3>>& transformationErrorStack);	///< Computes and saves the aggregate statistics
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppZeroReconstructionEvaluationImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//AppZeroReconstructionEvaluationImplementationC

/**
 * @brief Loads the image observations
 * @param[in] frameId Frame id
 * @param[in] nCameras Number of cameras
 * @return Observations tracks across the images; max observation count per camera
 */
auto AppZeroReconstructionEvaluationImplementationC::LoadObservations(unsigned int frameId, unsigned int nCameras) -> tuple<map<unsigned int, TrackT>, unsigned int>
{
	map<unsigned int, TrackT> output;
	size_t maxObservationCount=0;

	for(size_t c=0; c<nCameras; ++c)
	{
		string filename=str(format(parameters.observationPattern)%(c+1)%frameId);	//Filename
		ArrayXXd coordinates=ArrayIOC<ArrayXXd>::ReadArray(filename);

		size_t nObservations=coordinates.rows();
		maxObservationCount=max(maxObservationCount, nObservations);
		for(size_t c2=0; c2<nObservations; ++c2)
			if(coordinates(c2,0)!=-1)	//No observation
				output[c2].emplace(c, Coordinate2DT(coordinates(c2,0), coordinates(c2,1)));	//Rows correspond to the same point
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(output, maxObservationCount);
}	//void LoadObservations(unsigned int nCameras)

/**
 * @brief Verifies the parameters for consistency analysis
 * @param nCamera Number of cameras
 * @throws invalid_argument If the reference or source sets are invalid
 */
void AppZeroReconstructionEvaluationImplementationC::VerifyConsistencyParameters(unsigned int nCamera)
{
	//Verify the number of elements per set
	if(parameters.referenceSet.size() < 2)
		throw(invalid_argument( string("AppZeroReconstructionEvaluationImplementationC::VerifyConsistencyParameters: Reference set should have at least 2 elements. Value=")+lexical_cast<string>(parameters.referenceSet.size()) ) );

	set<unsigned int> cameraIds(parameters.referenceSet);

	for(const auto& current : parameters.sourceSets)
	{
		if(current.size() < 2)
			throw(invalid_argument( string("AppZeroReconstructionEvaluationImplementationC::VerifyConsistencyParameters: A source set should have at least 2 elements. Value=")+lexical_cast<string>(current.size()) ) );

		cameraIds.insert(current.begin(), current.end());
	}	//for(const auto& current : parameters.sourceSets)

	for(const unsigned int current : cameraIds)
		if(current >= nCamera)
			throw(invalid_argument( string("AppZeroReconstructionEvaluationImplementationC::VerifyConsistencyParameters: Undefined camera. Value=") + lexical_cast<string>(current) ) );
}	//void VerifyConsistencyParameters(unsigned int nCamera)

/**
 * @brief The exported version of the point cloud, with unsuccessful tracks marked
 * @param[in] pointCloud Point cloud
 * @param[in] trackCorrespondences Point-track correspondences
 * @param[in] sArray Size of the output array
 * @return Point cloud for export
 * @remarks The point cloud is ordered wrt tracks, and the tracks without a corresponding 3D point is marked with a point at infinity
 */
vector<Coordinate3DT> AppZeroReconstructionEvaluationImplementationC::MakeExportPointCloud(const vector<Coordinate3DT>& pointCloud, const CorrespondenceListT& trackCorrespondences, unsigned int sArray)
{
	vector<Coordinate3DT> output(sArray, Coordinate3DT(numeric_limits<double>::infinity(), numeric_limits<double>::infinity(), numeric_limits<double>::infinity() ));
	for_each(trackCorrespondences, [&](const CorrespondenceListT::value_type& current){ output[current.right]=pointCloud[current.left]; });
	return output;
}	//vector<Coordinate3DT> MakeExportPointCloud(const vector<Coordinate3DT>& pointCloud, const CorrespondenceListT& trackCorrespondences, unsigned int sArray)

/**
 * @brief Saves the per-frame output
 * @param[in] pointCloud 3D point cloud
 * @param[in] reprojectionStatistics Reprojection error statistics
 * @param[in] consistencyStatistics Consistency statistics
 * @param[in] frameId Frame id
 * @throws runtime_error If file generation fails
 */
void AppZeroReconstructionEvaluationImplementationC::SaveOutput(const vector<Coordinate3DT>& pointCloud, const map<unsigned int, tuple<ErrorStatisticsT, vector<double> > >& reprojectionStatistics, const map<unsigned int, tuple<ErrorStatisticsT, DeviationT, vector<double>, double > >& consistencyStatistics, unsigned int frameId)
{
	//Save the point cloud

	string pointCloudFilename=parameters.outputRoot+lexical_cast<string>(frameId)+".asc";
	ofstream pointCloudFile(pointCloudFilename);

	if(pointCloudFile.fail())
		throw(runtime_error(string("AppZeroReconstructionEvaluationImplementationC::SaveOutput: Cannot open file ")+pointCloudFilename));

	for(const auto& current : pointCloud)
		pointCloudFile<<current.transpose()<<"\n";

	//Save the reprojection error statistics

	if(parameters.flagReprojectionError)
	{
		string filename=parameters.outputRoot+string("reprojection.")+lexical_cast<string>(frameId)+".txt";
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("AppZeroReconstructionEvaluationImplementationC::SaveOutput: Cannot open file ")+filename));

		for(const auto& currentFrame : reprojectionStatistics)
		{
			for(const auto& current : get<0>(currentFrame.second) )
				output<<current<<" ";

			output<<"\n";
		}	//for(const auto& currentFrame : reprojectionStatistics)
	}	//if(parameters.flagReprojectionError)

	if(parameters.flagConsistency)
	{
		string filename=parameters.outputRoot+string("consistency.")+lexical_cast<string>(frameId)+".txt";
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("AppZeroReconstructionEvaluationImplementationC::SaveOutput: Cannot open file ")+filename));

		for(const auto& currentFrame : consistencyStatistics)
		{
			output<<currentFrame.first<<" ";
			for(const auto& current : get<0>(currentFrame.second) )
				output<<current<<" ";

			for(const auto& current : get<1>(currentFrame.second) )
				output<<current<<" ";

			output<<"\n";
		}	//for(const auto& currentFrame : reprojectionStatistics)

	}	//if(parameters.flagConsistency)

}	//void SaveOutput(const vector<ReprojectionErrorStatisticT>& reprojectionStatistics, unsigned int frameId)

/**
 * @brief Computes and saves the aggregate statistics
 * @param reprojectionErrorStack Reprojection errors for each camera
 * @param symmetricTransferErrorStack Symmetric transfer errors for each point cloud
 * @param noiseStack Point cloud noise stacks for each registration
 * @param transformationErrorStack Transformation errors for each point cloud
 */
void AppZeroReconstructionEvaluationImplementationC::SaveAggregate(const vector<list<double>>& reprojectionErrorStack, const vector<list<double>>& symmetricTransferErrorStack , const vector<list<double>>& noiseStack, const vector<array<list<double>,3>>& transformationErrorStack)
{
	//Aggregate reprojection error

	if(parameters.flagReprojectionError)
	{
		string filename=parameters.outputRoot+string("reprojection")+".txt";
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("AppZeroReconstructionEvaluationImplementationC::SaveAggregate: Cannot open file ")+filename));

		size_t nCameras=reprojectionErrorStack.size();

		for(size_t c=0; c<nCameras; ++c)
		{
			vector<double> errorList(reprojectionErrorStack[c].cbegin(), reprojectionErrorStack[c].cend());
			ErrorStatisticsT stats=ZeroReconstructionEvaluationModuleC::ComputeErrorStatistics(errorList, parameters.minNoiseVariance, 2);
			for(auto current : stats)
				output<<current<<" ";
			output<<"\n";
		}
	}	//if(parameters.flagReprojectionError)

	if(parameters.flagConsistency)
	{
		string filename=parameters.outputRoot+string("consistency")+".txt";
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("AppZeroReconstructionEvaluationImplementationC::SaveAggregate: Cannot open file ")+filename));

		size_t nSourceSets=parameters.sourceSets.size();
		for(size_t c=0; c<nSourceSets; ++c)
		{
			if(noiseStack[c].empty())
				continue;

			set<double> sortedNoise(noiseStack[c].cbegin(), noiseStack[c].cend());
			double noise=*next(sortedNoise.begin(), floor(sortedNoise.size())/2 );

			vector<double> errorList(symmetricTransferErrorStack[c].cbegin(), symmetricTransferErrorStack[c].cend());
			ErrorStatisticsT stats=ZeroReconstructionEvaluationModuleC::ComputeErrorStatistics(errorList, noise, 6);

			output<<c<<" ";
			for(auto current : stats)
				output<<current<<" ";

			for(const auto& current : transformationErrorStack[c])
			{
				set<double> sorted(current.cbegin(), current.cend());
				output<< *next(sorted.cbegin(), floor(sorted.size()/2) )<<" ";
			}

			output<<"\n";
		}	//for(size_t c=0; c<nSourceSets; ++c)


	}	//if(parameters.flagConsistency)

}	//void SaveAggregate(const vector<list<double>>& reprojectionErrorStack, const vector<list<double>>& symmetricTransferErrorStack , const array<list<double>,3>& transformationErrorStack)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppZeroReconstructionEvaluationImplementationC::AppZeroReconstructionEvaluationImplementationC()
{
	AppZeroReconstructionEvaluationParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads. A non-positive number attempts to use all available cores")
	("Triangulation.InlierThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.triangulationParameters.inlierTh)), "Maximum reprojection error for an inlier observation, in pixel. >0")
	("ReprojectionErrorAnalysis.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagReprojectionError)), "If true, reprojection error analysis is enabled" )
	("ReprojectionErrorAnalysis.NoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.minNoiseVariance)), "Variance of the noise on the observation coordinates, assuming perfect blob detection. Pixel^2. >0" )
	("ConsistencyAnalysis.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagConsistency)), "If true, consistency analysis is enabled" )
	("Input.ObservationPattern", value<string>(), "Filename pattern for the image observation files including two printf-style tokens- for the camera id and the frame id. Camera ids start from 1")
	("Input.InitialFrame", value<string>()->default_value(lexical_cast<string>(defaultParameters.initialFrame)), "Id of the initial frame")
	("Input.SequenceLength", value<string>()->default_value(lexical_cast<string>(defaultParameters.nFrames)), "Number of frames")
	("Input.CameraFilename", value<string>(), "File holding the camera parameters")
	("Output.Root", value<string>(), "Root directory for the output files")
	;

	//For consistency analysis, reference and source id parameters cannot be set from the command line
}	//AppZeroReconstructionEvaluationImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppZeroReconstructionEvaluationImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppZeroReconstructionEvaluationImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppZeroReconstructionEvaluationParametersC defaultParameters;

	tree.put("xmlcomment", "zeroReconstructionEvaluation configuration file");

	tree.put("General","");
	tree.put("Triangulation","");
	tree.put("ReprojectionErrorAnalysis","");
	tree.put("ConsistencyAnalysis","");
	tree.put("Input","");
	tree.put("Output","");

	tree.put("General.NoThreads", defaultParameters.nThreads);

	tree.put("Triangulation.InlierThreshold", defaultParameters.triangulationParameters.inlierTh);

	tree.put("ReprojectionErrorAnalysis.Enable", defaultParameters.flagReprojectionError);
	tree.put("ReprojectionErrorAnalysis.NoiseVariance", defaultParameters.minNoiseVariance);

	tree.put("ConsistencyAnalysis.Enable", defaultParameters.flagConsistency);
	tree.put("ConsistencyAnalysis.Reference", "0 1");

	tree.put("ConsistencyAnalysis.SourceSets", "");
	tree.put("ConsistencyAnalysis.SourceSets.Set", "1 2");

	tree.put("Input.ObservationPattern", "./%03d/%03d.txt");
	tree.put("Input.InitialFrame", defaultParameters.initialFrame);
	tree.put("Input.SequenceLength", defaultParameters.nFrames);
	tree.put("Input.CameraFile", "./calibration.cam");

	tree.put("Output.Root", "./");

    xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
   	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppZeroReconstructionEvaluationImplementationC::SetParameters(const ptree& tree)
{
	AppZeroReconstructionEvaluationParametersC defaultParameters;

	auto gt0=bind(greater<double>(),_1,0);

	//General
	int nThreads=tree.get<int>("General.NoThreads", defaultParameters.nThreads);
	parameters.nThreads = nThreads>0 ? nThreads : omp_get_num_threads();

	//Triangulation
	parameters.triangulationParameters.inlierTh=ReadParameter(tree, "Triangulation.InlierThreshold", gt0, "is not >0", optional<double>(defaultParameters.triangulationParameters.inlierTh));

	//Reprojection error analysis
	parameters.flagReprojectionError=tree.get<bool>("ReprojectionErrorAnalysis.Enable", defaultParameters.flagReprojectionError);
	parameters.minNoiseVariance=ReadParameter(tree, "ReprojectionErrorAnalysis.NoiseVariance", gt0, "is not >0", optional<double>(defaultParameters.minNoiseVariance));

	//Consistency analysis
	parameters.flagConsistency=tree.get<bool>("ConsistencyAnalysis.Enable", defaultParameters.flagConsistency);

	vector<unsigned int> referenceSet=Tokenise<unsigned int>(tree.get<string>("ConsistencyAnalysis.Reference"), " ");
	parameters.referenceSet=set<unsigned int>(referenceSet.begin(), referenceSet.end());

	ptree sourceBranch=tree.get_child("ConsistencyAnalysis.SourceSets");

	size_t nSets=sourceBranch.count("Set");
	parameters.sourceSets.reserve(nSets);

	for(const auto& current : sourceBranch.get_child(""))
	{
		if(current.first!="Set")
			continue;

		vector<unsigned int> sourceSet=Tokenise<unsigned int>(current.second.get<string>(""), " ");
		parameters.sourceSets.emplace_back(sourceSet.begin(), sourceSet.end());
	}	//for(const auto& current : sourceBranch.get_child(""))

	//Input
	parameters.observationPattern=tree.get<string>("Input.ObservationPattern", "");
	parameters.initialFrame=tree.get<unsigned int>("Input.InitialFrame", defaultParameters.initialFrame);
	parameters.nFrames=tree.get<unsigned int>("Input.SequenceLength", defaultParameters.nFrames);
	parameters.cameraFile=tree.get<string>("Input.CameraFile", "");

	//Output
	parameters.outputRoot=tree.get<string>("Output.Root","");
	if(!IsWriteable(parameters.outputRoot))
		throw(invalid_argument(string("AppZeroReconstructionEvaluationImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputRoot));

	parameters.triangulationParameters.maxDistanceRank=1.01;

	parameters.geometryEstimatorParameters.ransacParameters.parameters1.flagHistory=false;
	parameters.geometryEstimatorParameters.ransacParameters.parameters2.flagHistory=false;

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppZeroReconstructionEvaluationImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	//Load the cameras
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.cameraFile);
	size_t nCameras=cameraList.size();

	if(parameters.flagConsistency)
		VerifyConsistencyParameters(nCameras);

	//Accumulators
	size_t nSourceSets=parameters.sourceSets.size();
	vector<list<double> > reprojectionErrorStack(nCameras);	//Reprojection errors for the entire sequence
	vector<list<double> > symmetricTransferErrorStack(nSourceSets);	//Symmetric transfer errors for the entire sequence
	vector<list<double> > pointCloudNoiseStack(nSourceSets);
	vector<array<list<double>, 3 > > transformationErrorStack(nSourceSets);	//Transformation errors for the entire sequence

	size_t c=0;
	size_t finalFrame=parameters.initialFrame+parameters.nFrames;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) num_threads(parameters.nThreads)
	for(c=parameters.initialFrame; c<finalFrame; ++c)
	{

		//Load the observations
		map<unsigned int, TrackT> tracks;
		unsigned int maxObservationCount;
	#pragma omp critical(AZREI_R1)
		std::tie(tracks, maxObservationCount)=LoadObservations(c, nCameras);

		//Triangulation
		vector<Coordinate3DT> pointCloud;
		CorrespondenceListT pointTrackCorrespondences;
		std::tie(pointCloud, pointTrackCorrespondences)=ZeroReconstructionEvaluationModuleC::Reconstruct(cameraList, tracks, parameters.triangulationParameters);

		//Export version of the point cloud
		vector<Coordinate3DT> exportedPointCloud=MakeExportPointCloud(pointCloud, pointTrackCorrespondences, maxObservationCount);

		//Reprojection error statistics
		map<unsigned int, tuple<ErrorStatisticsT, vector<double> > > reprojectionStatistics;

		if(parameters.flagReprojectionError)
			reprojectionStatistics=ZeroReconstructionEvaluationModuleC::EvaluateImageError(pointCloud, tracks, pointTrackCorrespondences, cameraList, parameters.minNoiseVariance);

		//Deviation statistics
		map<unsigned int, tuple<ErrorStatisticsT, DeviationT, vector<double>, double > > consistencyStatistics;

		if(parameters.flagConsistency)
			consistencyStatistics=ZeroReconstructionEvaluationModuleC::EvaluateConsistency(parameters.referenceSet, parameters.sourceSets, cameraList, tracks, parameters.triangulationParameters, parameters.geometryEstimatorParameters);

		//Save the output
	#pragma omp critical(AZREI_R2)
	{

		//Update the accumulators
		for(const auto& current : reprojectionStatistics)
			reprojectionErrorStack[current.first].insert(reprojectionErrorStack[current.first].end(), get<1>(current.second).begin(), get<1>(current.second).end() );

		for(const auto& current : consistencyStatistics)
		{
			symmetricTransferErrorStack[current.first].insert(symmetricTransferErrorStack[current.first].end(), get<2>(current.second).begin(), get<2>(current.second).end() );

			transformationErrorStack[current.first][0].push_back( get<1>(current.second)[ZeroReconstructionEvaluationModuleC::iScale] );
			transformationErrorStack[current.first][1].push_back( get<1>(current.second)[ZeroReconstructionEvaluationModuleC::iRotation] );
			transformationErrorStack[current.first][2].push_back( get<1>(current.second)[ZeroReconstructionEvaluationModuleC::iOffset] );

			pointCloudNoiseStack[current.first].push_back(get<3>(current.second));
		}

		//Save output
		SaveOutput(exportedPointCloud, reprojectionStatistics, consistencyStatistics, c);
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Frame "<<c<<" processed."<<"\n";
	}
	}	//for(c=parameters.initialFrame; c<finalFrame; ++c)

	SaveAggregate(reprojectionErrorStack, symmetricTransferErrorStack, pointCloudNoiseStack, transformationErrorStack);
	cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Finished!"<<"\n";

	return true;
}	//bool Run()

}	//AppZeroReconstructionEvaluationN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("151211");
    std::string header("zeroReconstructionEvaluation: Evaluates the reconstruction accuracy");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppZeroReconstructionEvaluationN::AppZeroReconstructionEvaluationImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )

