/**
 * @file HistogramMatching.ipp Implementation details for histogram matching
 * @author Evren Imre
 * @date 11 Feb 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HISTOGRAM_MATCHING_IPP_08183921
#define HISTOGRAM_MATCHING_IPP_08183921

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Dense>
#include <algorithm>
#include <type_traits>
#include <cstddef>
#include <tuple>
#include <climits>
#include <cmath>
#include <string>
#include <stdexcept>
#include <map>
#include "../Elements/Correspondence.h"
#include "../Wrappers/BoostRange.h"

namespace SeeSawN
{
namespace SignalProcessingN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::range::for_each;
using boost::counting_range;
using boost::optional;
using boost::lexical_cast;
using Eigen::Matrix;
using std::is_floating_point;
using std::is_arithmetic;
using std::all_of;
using std::size_t;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;
using std::numeric_limits;
using std::fabs;
using std::min;
using std::max;
using std::string;
using std::invalid_argument;
using std::map;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::WrappersN::MakeBinaryZipRange;

/**
 * @brief Parameters for \c HistogramMatcherC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct HistogramMatchingParametersC
{
	bool flagConsistency;	///< If \c true , left-right consistency is enforced
	double maxDistance;		///< Maximum distance on the normalised cumulative histogram between two corresponding bins. [0,1]
	double minFrequency;	///< Minimum frequency value on the normalised histogram to be satisfied by both members of a pair. [0,1]

	HistogramMatchingParametersC() : flagConsistency(false), maxDistance(1), minFrequency(0)
	{}
};	//struct HistogramMatchingParametersC

/**
 * @brief Finds the corresponding bins between two histograms
 * @tparam RealT Floating point type for the normalised histograms
 * @pre \c RealT is a floating point type
 * @remarks Usage notes
 * 	- The algorithm seeks the pairs closest in their normalised cumulative histogram values
 * 	- The original algorithm is not symmetric: Swapping the inputs yields different results
 * 	- The consistency constraint is enforced by validating the L-R matches with R-L. The ambiguity is chosen as the maximum of those of the two correspondences
 * @remarks The algorithm can handle unnormalised histograms as well
 * @remarks http://en.wikipedia.org/wiki/Histogram_matching
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<typename RealT>
class HistogramMatchingC
{
	///@cond CONCEPT_CHECK
	static_assert(is_floating_point<RealT>::value, "HistogramMatchingC : RealT must be a floating point type");
	///@endcond

	private:

		typedef Matrix<RealT, Eigen::Dynamic, 1> VectorT;	///< A floating point vector

		static void ValidateParameters(const HistogramMatchingParametersC& parameters);	///< Validates the parameters
		template<class HistogramT> tuple<VectorT, VectorT> static ProcessHistogram(const HistogramT& histogram);	///< Processes a histogram for use in the algorithm
		static CorrespondenceListT FindCorrespondences(const VectorT& normalised1, const VectorT& cumulative1, const VectorT& normalised2, const VectorT& cumulative2, const HistogramMatchingParametersC& parameters);	///< Finds the correspondences between two normalised histograms
		static CorrespondenceListT EnforceConsistency(const CorrespondenceListT& correspondences12, const CorrespondenceListT& correspondences21);	///< Enforces the consistency constraint

	public:

		template<class HistogramT> static CorrespondenceListT Run(const HistogramT& h1, const HistogramT& h2, const HistogramMatchingParametersC& parameters );	///< Runs the algorithm

};	//class HistogramMatchingC

/**
 * @brief Validates the parameters
 * @param[in] parameters Parameters
 * @throws invalid_argument If any parameters are invalid
 */
template<typename RealT>
void HistogramMatchingC<RealT>::ValidateParameters(const HistogramMatchingParametersC& parameters)
{
	if(parameters.maxDistance<0 || parameters.maxDistance>1)
		throw(invalid_argument(string("HistogramMatchingC::ValidateParameters : parameters.maxDistance must be in [0,1]. Value=")+lexical_cast<string>(parameters.maxDistance)));


	if(parameters.minFrequency<0 || parameters.minFrequency>1)
		throw(invalid_argument(string("HistogramMatchingC::ValidateParameters : parameters.minFrequency must be in [0,1]. Value=")+lexical_cast<string>(parameters.minFrequency)));
}	//void ValidateParameters(const HistogramMatchingParametersC& parameters)

/**
 * @brief Processes a histogram for use in the algorithm
 * @tparam HistogramT A container for a histogram
 * @param[in] histogram Histogram to be processed
 * @return A tuple: Normalised histogram and normalised cumulative histogram
 * @pre \c HistogramT is a forward range of arithmetic values
 * @pre \c histogram does not have any negative values
 */
template<typename RealT>
template<class HistogramT>
auto HistogramMatchingC<RealT>::ProcessHistogram(const HistogramT& histogram) -> tuple<VectorT, VectorT>
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<HistogramT>));
	static_assert(is_arithmetic<typename range_value<HistogramT>::type>::value, "HistogramMatchingC::Run : HistogramT must hold arithmetic elements" );

	assert(all_of(histogram.begin(), histogram.end(), [](typename range_value<HistogramT>::type val){return val>=0;}));

	//Compute the normalised histogram
	size_t nBins=boost::distance(histogram);
	VectorT normalised(nBins);
	size_t c=0;
	for(const auto& current : histogram)
	{
		normalised[c]=current;
		++c;
	}

	normalised/=normalised.sum();

	//Compute the normalised cumulative histogram
	VectorT cumulative(nBins); cumulative[0]=normalised[0];
	for_each(counting_range((size_t)1, nBins), [&](size_t c){cumulative[c]=cumulative[c-1]+normalised[c];});

	return make_tuple(normalised, cumulative);
}	//tuple<VectorT, VectorT> ProcessHistogram(const HistogramT& histogram)

/**
 * @brief Finds the correspondences between two normalised histograms
 * @param[in] normalised1 Normalised histogram for the first operand
 * @param[in] cumulative1 Normalised cumulative histogram for the first operand
 * @param[in] normalised2 Normalised histogram for the second operand
 * @param[in] cumulative2 Normalised cumulative histogram for the second operand
 * @param[in] parameters Algorithm parameters
 * @return A list of corresponding bins
 */
template<class RealT>
CorrespondenceListT HistogramMatchingC<RealT>::FindCorrespondences(const VectorT& normalised1, const VectorT& cumulative1, const VectorT& normalised2, const VectorT& cumulative2, const HistogramMatchingParametersC& parameters)
{
	CorrespondenceListT output;

	//Algorithm: For each bin in the second histogram, find the bin with the closest cumulative histogram value in the first
	//Since cdf is monotnic, this automatically enforces the ordering constraint
	size_t nBins1=cumulative1.size();
	size_t nBins2=cumulative2.size();
	size_t index=0;	//Index of the previous match
	for(size_t c2=0; c2<nBins2; ++c2)
	{
		RealT bestError=numeric_limits<RealT>::infinity();
		RealT secondBestError=numeric_limits<RealT>::infinity();
		bool flagUpdated=false; //Indicates whether a better match is found
		for(size_t c1=index; c1<nBins1; ++c1)	//Starting from the index of the last match imposes the ordering constraint. This is valid, as a cumulative histogram is monotonic
		{
			RealT error=fabs(cumulative2[c2]-cumulative1[c1]);

			if(error<bestError)
			{
				index=c1;
				secondBestError=bestError;
				bestError=error;
				flagUpdated=true;
			}	//if(error<bestError)

			//Since the cumulative histograms are monotonic, the error function is convex. So once the error starts increasing, it is not going to decrease
			if(error>bestError)
			{
				secondBestError=min(error, secondBestError);
				break;
			}	//if(error>bestError)
		}	//for(size_t c1=index; c1<nBins1; ++c1)

		//If a valid match is found, record
		if(flagUpdated && normalised1[index]>=parameters.minFrequency && normalised2[c2]>=parameters.minFrequency && bestError<parameters.maxDistance)
			output.push_back(typename CorrespondenceListT::value_type(index, c2, MatchStrengthC(bestError, bestError/secondBestError)));
	}	//for(size_t c2=0; c2<nBins2; ++c2)

	return output;
}	//CorrespondenceListT FindCorrespondences(const VectorT& normalised1, const VectorT& cumulative1, const VectorT& normalised2, const VectorT& cumulative2)

/**
 * @brief Enforces the consistency constraint
 * @param[in] correspondences12 L-R correspondences
 * @param[in] correspondences21 R-L correspondences
 * @return Correspondences satisfying the consistency constraint
 */
template<class RealT>
CorrespondenceListT HistogramMatchingC<RealT>::EnforceConsistency(const CorrespondenceListT& correspondences12, const CorrespondenceListT& correspondences21)
{
	map<tuple<size_t, size_t>, MatchStrengthC> map12;	//correspondences12 in map form
	for(const auto& current : correspondences12)
		map12.emplace(make_tuple(current.left, current.right), current.info);

	//Seek the elements of correspondences21 in map12
	CorrespondenceListT output;
	auto itE=map12.end();
	for(const auto& current : correspondences21)
	{
		auto it=map12.find(make_tuple(current.right, current.left) );

		if(it==itE)
			continue;

		MatchStrengthC strength(current.info.Similarity(), max(current.info.Ambiguity(), it->second.Ambiguity()));
		output.push_back(typename CorrespondenceListT::value_type(current.right, current.left, strength));
	}	//for(const auto& current : correspondences21)

	return output;
}	//CorrespondenceListT EnforceConsistency(const CorrespondenceListT& correspondences12, const CorrespondenceListT& correspondences21)

/**
 * @brief Runs the algorithm
 * @tparam HistogramT A container for a histogram
 * @param[in] h1 First histogram
 * @param[in] h2 Second histogram
 * @param[in] parameters Algorithm parameters
 * @return Corresponding histogram bins
 */
template<class RealT>
template<class HistogramT>
CorrespondenceListT HistogramMatchingC<RealT>::Run(const HistogramT& h1, const HistogramT& h2, const HistogramMatchingParametersC& parameters)
{
	ValidateParameters(parameters);

	CorrespondenceListT output;

	//Empty?
	if(boost::empty(h1) || boost::empty(h2))
		return output;

	//Compute the normalised histograms
	VectorT normalised1;
	VectorT cumulative1;
	tie(normalised1, cumulative1)=ProcessHistogram(h1);

	VectorT normalised2;
	VectorT cumulative2;
	tie(normalised2, cumulative2)=ProcessHistogram(h2);

	if(!parameters.flagConsistency)
		output=FindCorrespondences(normalised1, cumulative1, normalised2, cumulative2, parameters);
	else
	{
		CorrespondenceListT corr12=FindCorrespondences(normalised1, cumulative1, normalised2, cumulative2, parameters);
		CorrespondenceListT corr21=FindCorrespondences(normalised2, cumulative2, normalised1, cumulative1, parameters);
		output=EnforceConsistency(corr12, corr21);
	}	//if(!parameters.flagConsistency

	return output;
}	//CorrespondenceListT MatchHistogramBins(const HistogramT& h1, const HistogramT& h2, double minFrequency, double maxDistance )

}	//SignalProcessingN
}	//SeeSawN

#endif /* HISTOGRAM_MATCHING_IPP_08183921 */
