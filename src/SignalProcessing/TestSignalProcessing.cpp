/**
 * @file TestSignalProcessing.cpp
 * @author Evren Imre
 * @date 11 Feb 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/


#define BOOST_TEST_MODULE SIGNAL_PROCESSING

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <vector>
#include <iterator>
#include "../Elements/Correspondence.h"
#include "HistogramMatching.h"

namespace SeeSawN
{
namespace UnitTestN
{
namespace TestSignalProcessingN
{

using namespace SeeSawN::SignalProcessingN;

using std::vector;
using std::next;
using SeeSawN::ElementsN::CorrespondenceListT;

BOOST_AUTO_TEST_SUITE(Histogram_Matching)

BOOST_AUTO_TEST_CASE(Histogram_Matching_Operation)
{
	typedef HistogramMatchingC<double> MatcherT;

	vector<double> hLeft{ 0.0088661, 0.1080112, 0.4840725, 0.7981006, 0.4840725, 0.1080112, 0.0088661};
	vector<double> hRight{ 0.0022182, 0.0087731, 0.0270232, 0.0648252, 0.1211094, 0.1762131, 0.1996756, 0.1762131, 0.1211094, 0.0648252, 0.0270232, 0.0087731, 0.0022182};

	//Default parameters: Original histogram matching
	HistogramMatchingParametersC parameters1;
	CorrespondenceListT corr1=MatcherT::Run(hLeft, hRight, parameters1);

	BOOST_CHECK_EQUAL(corr1.size(), 13);
	BOOST_CHECK_EQUAL(next(corr1.begin(),3)->left, 1);
	BOOST_CHECK_EQUAL(next(corr1.begin(),3)->right, 3);
	BOOST_CHECK_CLOSE(next(corr1.begin(),3)->info.Similarity(), 0.044401, 5e-4);
	BOOST_CHECK_CLOSE(next(corr1.begin(),3)->info.Ambiguity(), 0.224662, 5e-4);

	//Quality thresholds

	HistogramMatchingParametersC parameters2;
	parameters2.minFrequency=0.05;
	parameters2.maxDistance=0.2;
	CorrespondenceListT corr2=MatcherT::Run(hLeft, hRight, parameters2);

	BOOST_CHECK_EQUAL(corr2.size(), 7);
	BOOST_CHECK_EQUAL(next(corr2.begin(),2)->left, 2);
	BOOST_CHECK_EQUAL(next(corr2.begin(),2)->right,5);
	BOOST_CHECK_CLOSE(next(corr2.begin(),2)->info.Similarity(), 0.099687, 5e-4);
	BOOST_CHECK_CLOSE(next(corr2.begin(),2)->info.Ambiguity(), 0.332998, 5e-4);

	//Consistency check

	HistogramMatchingParametersC parameters3;
	parameters3.flagConsistency=true;
	CorrespondenceListT corr3=MatcherT::Run(hLeft, hRight, parameters3);

	BOOST_CHECK_EQUAL(corr3.size(), 7);
	BOOST_CHECK_EQUAL(next(corr3.begin(),1)->left, 1);
	BOOST_CHECK_EQUAL(next(corr3.begin(),1)->right, 2);
	BOOST_CHECK_CLOSE(next(corr3.begin(),1)->info.Similarity(), 0.0204241, 5e-4);
	BOOST_CHECK_CLOSE(next(corr3.begin(),1)->info.Ambiguity(), 0.608197, 5e-5);
}	//BOOST_AUTO_TEST_CASE(Histogram_Matching_Operation)

BOOST_AUTO_TEST_SUITE_END() //Histogram_Matching

}	//TestSignalProcessingN
}	//UnitTestN
}	//SeeSawN

