/**
 * @file RSTSRatioRegistrationProblem.h Public interface for \c RSTSRatioRegistrationProblemC
 * @author Evren Imre
 * @date 12 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RSTS_RATIO_REGISTRATION_PROBLEM_H_6109925
#define RSTS_RATIO_REGISTRATION_PROBLEM_H_6109925

#include "RSTSRatioRegistrationProblem.ipp"

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

class RSTSRatioRegistrationProblemC;	///< Random spanning tree problem for ratio registration
}	//RandomSpanningTreeSamplerN
}	//SeeSawN

#endif /* RSTS_RATIO_REGISTRATION_PROBLEM_H_6109925 */
