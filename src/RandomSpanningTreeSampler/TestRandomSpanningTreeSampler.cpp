/**
 * @file TestRandomSpanningTreeSampler.cpp Unit tests for the random spanning tree sampler module
 * @author Evren Imre
 * @date 4 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE OPTIMISATION

#include <boost/test/unit_test.hpp>
#include <boost/dynamic_bitset.hpp>
#include <vector>
#include <utility>
#include <tuple>
#include <stdexcept>
#include "RandomSpanningTreeSampler.h"
#include "RSTSRotationRegistrationProblem.h"
#include "RSTSRatioRegistrationProblem.h"
#include "../Elements/GeometricEntity.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestRandomSpanningTreeSamplerN
{

using namespace RandomSpanningTreeSamplerN;
using boost::dynamic_bitset;
using std::vector;
using std::pair;
using std::make_pair;
using std::tie;
using std::invalid_argument;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::NumericN::ComputeClosestRotationMatrix;;

BOOST_AUTO_TEST_SUITE(RSTS_Problems)

BOOST_AUTO_TEST_CASE(RSTS_Rotation_Registration)
{
	RSTSRotationRegistrationProblemC problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Data
	vector<RotationMatrix3DT> mRList(4);
	mRList[0]=QuaternionT(0.85154,0.0616971,-0.519809,-0.029535).matrix();
	mRList[1]=QuaternionT(0.953864, 0.0283951, -0.298819, -0.00665323).matrix();
	mRList[2]=QuaternionT(0.988633, 0.0457148, -0.143168, -0.00410147).matrix();
	mRList[3]=QuaternionT(0.996071, 0.0144022, 0.0872254, -0.00511926 ).matrix();

	vector<RelativeRotationRegistrationC::observation_type> observations; observations.reserve(6);
	vector<RSTSRotationRegistrationProblemC::edge_type> edgeListR; edgeListR.reserve(6);
	for(size_t c1=0; c1<4; ++c1)
		for(size_t c2=c1+1; c2<4; ++c2)
		{
			RotationMatrix3DT mR=mRList[c2]*mRList[c1].transpose();
			mR+=(1e-1)*RotationMatrix3DT::Random();

			if(c1==2 && c2==3)	//Outlier
				mR+=RotationMatrix3DT::Random();

			mR=ComputeClosestRotationMatrix(mR);
			observations.emplace_back(c1, c2, mR, c1*c2+1);
			edgeListR.emplace_back(c1, c2, c1*c2+1);
		}

	//Methods

	RSTSRotationRegistrationProblemC problem2(observations, 0.25);
	BOOST_CHECK(problem2.IsValid());

	vector<RSTSRotationRegistrationProblemC::edge_type> edgeList=problem2.GetEdgeList();
	BOOST_CHECK_EQUAL(edgeList.size(), 6);
	for(size_t c=0; c<6; ++c)
		BOOST_CHECK(edgeList[c]==edgeListR[c]);

	vector<RSTSRotationRegistrationProblemC::edge_type> spanningTree(3);
	spanningTree[0]=edgeList[0]; spanningTree[1]=edgeList[1]; spanningTree[2]=edgeList[4];

	vector<QuaternionT> model=*problem2.GenerateModel(spanningTree);
	BOOST_CHECK_EQUAL(model.size(), 4);

	dynamic_bitset<> inlierFlagsR(6); inlierFlagsR.set(); inlierFlagsR[5]=false;
	for(size_t c=0; c<6; ++c)
	{
		bool flagInlier;
		double error;
		tie(flagInlier, error)=problem2.EvaluateEdge(model, edgeList[c]);

		BOOST_CHECK(flagInlier==inlierFlagsR[c]);
	}	//for(const auto& current : edgeList)

	//Quaternion constructor
	vector<RelativeRotationRegistrationC::quaternion_observation_type> observationsQ; observationsQ.reserve(6);
	for(const auto& current : observations)
		observationsQ.emplace_back(get<0>(current), get<1>(current), QuaternionT(get<2>(current)), get<3>(current));

	RSTSRotationRegistrationProblemC problem3(observationsQ, 0.25);
	BOOST_CHECK(problem3.IsValid());

	vector<QuaternionT> model3=*problem3.GenerateModel(spanningTree);
	BOOST_CHECK_EQUAL(model3.size(), 4);

	for(size_t c=0; c<6; ++c)
	{
		bool flagInlier;
		double error;
		tie(flagInlier, error)=problem3.EvaluateEdge(model3, edgeList[c]);

		BOOST_CHECK(flagInlier==inlierFlagsR[c]);
	}	//for(const auto& current : edgeList)

	//Utilities
	vector<size_t> inliers4;
	double error4;
	tie(inliers4, error4)=problem3.EvaluateModel(model);
	BOOST_CHECK_CLOSE(error4, 2.1628594959591458, 1e-5);

	vector<size_t> inliers4r{0,1,2,3,4};
	BOOST_CHECK_EQUAL_COLLECTIONS(inliers4.begin(), inliers4.end(), inliers4r.begin(), inliers4r.end());

	vector<size_t> indices5=problem3.RetrieveObservationIndexList(spanningTree);
	vector<size_t> indices5r{0,1,4};
	BOOST_CHECK_EQUAL_COLLECTIONS(indices5.begin(), indices5.end(), indices5r.begin(), indices5r.end());
}	//BOOST_AUTO_TEST_CASE(RSTS_Rotation_Registration)

BOOST_AUTO_TEST_CASE(RSTS_Ratio_Registration)
{
	RSTSRatioRegistrationProblemC problem1;
	BOOST_CHECK(!problem1.IsValid());

	//Data
	vector<double> elements{1, 1.6, 2.5, 4};

	vector<RatioRegistrationC::observation_type> observations; observations.reserve(6);
	vector<RSTSRatioRegistrationProblemC::edge_type> edgeListR; edgeListR.reserve(6);
	for(size_t c1=0; c1<4; ++c1)
		for(size_t c2=c1+1; c2<4; ++c2)
		{
			observations.emplace_back(c1, c2, elements[c2]/elements[c1] + 1e-3*c1, c1*c2+1);
			edgeListR.emplace_back(c1, c2, c1*c2+1);
		}	//for(size_t c2=c1+1; c2<4; ++c2)

	//Methods

	RSTSRatioRegistrationProblemC problem2(observations, 5e-4);
	BOOST_CHECK(problem2.IsValid());

	vector<RSTSRatioRegistrationProblemC::edge_type> edgeList=problem2.GetEdgeList();
	BOOST_CHECK_EQUAL(edgeList.size(), 6);
	for(size_t c=0; c<6; ++c)
		BOOST_CHECK(edgeList[c]==edgeListR[c]);

	vector<RSTSRatioRegistrationProblemC::edge_type> spanningTree(3);
	spanningTree[0]=edgeList[0]; spanningTree[1]=edgeList[1]; spanningTree[2]=edgeList[4];

	vector<double> model=*problem2.GenerateModel(spanningTree);
	BOOST_CHECK_EQUAL(model.size(), 4);

	dynamic_bitset<> inlierFlagsR(6); inlierFlagsR.set(); inlierFlagsR[3]=false; inlierFlagsR[5]=false;
	for(size_t c=0; c<6; ++c)
	{
		bool flagInlier;
		double error;
		tie(flagInlier, error)=problem2.EvaluateEdge(model, edgeList[c]);

		BOOST_CHECK(flagInlier==inlierFlagsR[c]);
	}	//for(const auto& current : edgeList)

	RSTSRatioRegistrationProblemC problem3(observations, 5e-4);
	BOOST_CHECK(problem3.IsValid());

	vector<double> model3=*problem3.GenerateModel(spanningTree);
	BOOST_CHECK_EQUAL(model3.size(), 4);

	for(size_t c=0; c<6; ++c)
	{
		bool flagInlier;
		double error;
		tie(flagInlier, error)=problem3.EvaluateEdge(model3, edgeList[c]);

		BOOST_CHECK(flagInlier==inlierFlagsR[c]);
	}	//for(const auto& current : edgeList)
}	//BOOST_AUTO_TEST_CASE(RSTS_Ratio_Registration)

BOOST_AUTO_TEST_SUITE_END()	//RSTS_Problems

BOOST_AUTO_TEST_SUITE(RSTS)

BOOST_AUTO_TEST_CASE(RSTS_Operation)
{
	typedef RandomSpanningTreeSamplerC<RSTSRotationRegistrationProblemC> EngineT;

	//Data
	vector<RotationMatrix3DT> mRList(4);
	mRList[0]=QuaternionT(0.85154,0.0616971,-0.519809,-0.029535).matrix();
	mRList[1]=QuaternionT(0.953864, 0.0283951, -0.298819, -0.00665323).matrix();
	mRList[2]=QuaternionT(0.988633, 0.0457148, -0.143168, -0.00410147).matrix();
	mRList[3]=QuaternionT(0.996071, 0.0144022, 0.0872254, -0.00511926 ).matrix();

	vector<RelativeRotationRegistrationC::observation_type> observations; observations.reserve(6);
	for(size_t c1=0; c1<4; ++c1)
		for(size_t c2=c1+1; c2<4; ++c2)
		{
			RotationMatrix3DT mR=mRList[c2]*mRList[c1].transpose();
			mR+=(1e-5)*RotationMatrix3DT::Random();

			if(c1==0 && c2==1)	//Outlier
				mR+=RotationMatrix3DT::Random();

			mR=ComputeClosestRotationMatrix(mR);
			observations.emplace_back(c1, c2, mR, c1*c2+c2+1);
		}	//for(size_t c2=c1+1; c2<4; ++c2)

	//Problem
	RSTSRotationRegistrationProblemC problem(observations, 0.15);

	RandomSpanningTreeSamplerParametersC parameters;
	EngineT::result_type result;
	EngineT::rng_type rng;
	RandomSpanningTreeSamplerDiagnosticsC diagnostics=EngineT::Run(result, problem, rng, parameters);

	BOOST_CHECK(diagnostics.flagSuccess);
	BOOST_CHECK_EQUAL(diagnostics.inlierRatio, 5.0/6.0);
	BOOST_CHECK_EQUAL(diagnostics.error, get<EngineT::iModelError>(result));

	//Invalid cases
	RandomSpanningTreeSamplerParametersC invalidParameters;
	invalidParameters.nThreads=0;
	BOOST_CHECK_THROW(EngineT::Run(result, problem, rng, invalidParameters), invalid_argument);

	invalidParameters.nThreads=1; invalidParameters.minConfidence=1.5;
	BOOST_CHECK_THROW(EngineT::Run(result, problem, rng, invalidParameters), invalid_argument);

	invalidParameters.minConfidence=-1;
	BOOST_CHECK_THROW(EngineT::Run(result, problem, rng, invalidParameters), invalid_argument);

	invalidParameters.minConfidence=0.99; invalidParameters.maxIteration=0; invalidParameters.minIteration=1;
	BOOST_CHECK_THROW(EngineT::Run(result, problem, rng, invalidParameters), invalid_argument);

	//Minimal spanning tree
	EngineT::result_type mstResult;
	RandomSpanningTreeSamplerDiagnosticsC mstDiagnostics=EngineT::ComputeMSTSolution(mstResult, problem);

	BOOST_CHECK(mstDiagnostics.flagSuccess);
	BOOST_CHECK_EQUAL(mstDiagnostics.inlierRatio, 5.0/6.0);
	BOOST_CHECK_EQUAL(mstDiagnostics.error, get<EngineT::iModelError>(mstResult));
	BOOST_CHECK_EQUAL(mstDiagnostics.nIteration,1);
}	//BOOST_AUTO_TEST_CASE(RSTS_Operation)

BOOST_AUTO_TEST_SUITE_END()	//RSTS

}	//TestRandomSpanningTreeSamplerN
}	//UnitTestsN
}	//SeeSawN
