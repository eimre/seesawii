/**
 * @file RandomSpanningTreeSampler.ipp Implementation of the random spanning tree sampler
 * @author Evren Imre
 * @date 30 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RANDOM_SPANNING_TREE_SAMPLER_IPP_690126
#define RANDOM_SPANNING_TREE_SAMPLER_IPP_690126

#include <boost/optional.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/random_spanning_tree.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm.hpp>
#include <omp.h>
#include <limits>
#include <cstddef>
#include <tuple>
#include <vector>
#include <set>
#include <map>
#include <stdexcept>
#include <string>
#include <cmath>
#include <iterator>
#include <utility>
#include <random>
#include "../Numeric/Graph.h"

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

using boost::adjacency_matrix;
using boost::edge_weight_t;
using boost::undirectedS;
using boost::no_property;
using boost::property;
using boost::add_edge;
using boost::random_spanning_tree;
using boost::prim_minimum_spanning_tree;
using boost::vertex_index_t;
using boost::vertex_property_type;
using boost::graph_traits;
using boost::associative_property_map;
using boost::predecessor_map;
using boost::weight_map;
using boost::property_map;
using boost::num_vertices;
using boost::vertices;
using boost::source;
using boost::target;
using boost::edge_weight;
using boost::optional;
using boost::lexical_cast;
using boost::range::for_each;
using boost::range::max_element;
using std::numeric_limits;
using std::size_t;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;
using std::vector;
using std::map;
using std::invalid_argument;
using std::string;
using std::set;
using std::log;
using std::min;
using std::max;
using std::advance;
using std::pair;
using std::mt19937_64;
using SeeSawN::NumericN::CountSpanningTrees;

/**
 * @brief Parameter object for \c RandomSpanningTreeSamplerC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct RandomSpanningTreeSamplerParametersC
{
	unsigned int nThreads;	///< Number of threads. >=1

	unsigned int minIteration;	///< Minimum number of iterations. <=maxIteration
	unsigned int maxIteration;	///< Maximum number of iterations

	double minConfidence;	///< Minimum confidence level for an all-inlier solution. (0,1]

	RandomSpanningTreeSamplerParametersC() : nThreads(1), minIteration(0), maxIteration(numeric_limits<unsigned int>::max()), minConfidence(0.99)
	{}
};	//struct RandomSpanningTreeSamplerParametersC

/**
 * @brief Diagnostics object for \c RandomSpanningTreeSamplerC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct RandomSpanningTreeSamplerDiagnosticsC
{
	bool flagSuccess;	///< \c true if the operation terminates successfully

	double inlierRatio;	///< Ratio of the inlier edges to all edges
	double error;	///< Best error
	unsigned int nIteration;	///< Number of iterations

	RandomSpanningTreeSamplerDiagnosticsC() : flagSuccess(false), inlierRatio(0), error(numeric_limits<double>::infinity()), nIteration(0)
	{}
};	//struct RandomSpanningTreeSamplerDiagnosticsC

/**
 * @brief Robust optimisation by random spanning tree sampling
 * @tparam ProblemT Problem type
 * @tparam RNGT Random number generator type
 * @pre RNGT is a model of UniformRandomNumberGenerator concept(unenforced)
 * @remarks Algorithm
 * 	- Generate a random spanning tree
 * 	- Evaluate and identify the inliers
 * 	- Update the confidence
 * 	- Check for termination
 * @remarks Usage notes
 * 	- The algorithm is designed for simple connected graphs. If the vertex indices from problem.GetEdgeList() do not form a consecutive set starting from 0, the operation fails.
 * 	- The implementation uses a matrix representation for graphs, which may be too expensive in terms of memory
 * 	- Computation of success rate requires a determinant operation, an \f$ O(N^3) \f$ operation in the number of vertices. In large graphs, fixing the number of iterations could be preferable
 * 	- In large graphs, convergence in confidence may require too many iterations
 * 	- Generator edges are not necessarily inliers
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT, class RNGT=mt19937_64>
class RandomSpanningTreeSamplerC
{
	private:

		typedef typename ProblemT::model_type ModelT;	 ///< A model explaining the observed edges
		typedef tuple<unsigned int, unsigned int, double> EdgeT;	///< An edge
		typedef vector<EdgeT> EdgeContainerT;	///< A container for edges
		typedef vector<size_t> IndexContainerT;	///< A container for indices

		typedef adjacency_matrix<undirectedS, no_property, property<edge_weight_t, double> > GraphT;	///< Type of the graph
		typedef typename graph_traits<GraphT>::vertex_descriptor VertexDescriptorT;	///< A vertex descriptor
		typedef associative_property_map<map<VertexDescriptorT, VertexDescriptorT> > PredecessorMapT;	///< A predecessor map

		typedef tuple<double, ModelT, EdgeContainerT, EdgeContainerT> ResultT;	///< A potential solution

		/** @name Implementation details */ //@{
		static tuple<size_t, bool> ValidateInput(const ProblemT& problem, const RandomSpanningTreeSamplerParametersC& parameters);	///< Validates the parameters

		static GraphT MakeGraph(const EdgeContainerT& edgeList, size_t nVertex);	///< Makes a graph from an edge list

		static EdgeContainerT SpanningTreeToEdgeContainer(const PredecessorMapT& predecessorMap, const GraphT& graph);	///< Converts a predecessor map to an edge container
		static EdgeContainerT GenerateSample(RNGT& rng, const GraphT& graph, bool flagUniformWeight);	///< Generates a random spanning tree
		static optional<ModelT> GenerateModel(ProblemT& problem, const EdgeContainerT& generator);	///< Generates a model
		static tuple<double, IndexContainerT> EvaluateModel(ProblemT& problem, const ModelT& model, const EdgeContainerT& edgeList);	///< Evaluates a model
		//@}

	public:

		/** @name Edge definition */ //@{
		typedef EdgeT edge_type;	///< An edge
		static constexpr size_t iVertex1=0; 	///< Index of the component for the first vertex
		static constexpr size_t iVertex2=1;	///< Index of the component for the second vertex
		static constexpr size_t iWeight=2;	///< Index of the component for the edge weight
		//@}

		typedef EdgeContainerT edge_container_type;	///< An edge container

		/** @name Solution */ //@{
		typedef ResultT result_type;	///< Result type
		static constexpr unsigned int iModelError=0;	///< Index of the model error
		static constexpr unsigned int iModel=1;	///< Index of the model
		static constexpr unsigned int iInlierList=2;	///< Index of the inlier set
		static constexpr unsigned int iGenerator=3;	///< Index of the generator
		//@}

		typedef RNGT rng_type;	///< Type of the RNG

		/** @name Operations */ //@{
		static RandomSpanningTreeSamplerDiagnosticsC Run(result_type& result, ProblemT& problem, RNGT& rng, const RandomSpanningTreeSamplerParametersC& parameters);	///< Runs the algorithm
		//@}

		/** @name Utility */ //@{
		static RandomSpanningTreeSamplerDiagnosticsC ComputeMSTSolution(result_type& result, ProblemT& problem);	///< Computes the minimum spanning tree solution
		//@}
};	//class RandomSpanningTreeSamplerC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @param[in] problem Problem to be validated
 * @param[in] parameters Parameters to be validated
 * @return A tuple. [No vertices; unweighted graph]
 * @pre \c problem is valid
 * @throw invalid_argument If the preconditions are violated
 */
template<class ProblemT, class RNGT>
tuple<size_t, bool> RandomSpanningTreeSamplerC<ProblemT, RNGT>::ValidateInput(const ProblemT& problem, const RandomSpanningTreeSamplerParametersC& parameters)
{
	if(!problem.IsValid())
		throw(invalid_argument("Invalid problem object"));

	if(parameters.nThreads<1)
		throw(invalid_argument(string("RandomSpanningTreeSamplerC::ValidateParameters: parameters.nThreads must be >=1. Value= ") + lexical_cast<string>(parameters.nThreads)));

	if(parameters.minIteration > parameters.maxIteration)
		throw(invalid_argument(string("RandomSpanningTreeSamplerC::ValidateParameters: parameters.minIteration must be <=maxIteration. Values= ")+ lexical_cast<string>(parameters.minIteration) + " " +lexical_cast<string>(parameters.maxIteration) ));

	if(parameters.minConfidence<=0 || parameters.minConfidence>1)
		throw(invalid_argument(string("RandomSpanningTreeSamplerC::ValidateParameters: parameters.minConfidence must be in (0,1]. Value= ") + lexical_cast<string>(parameters.minConfidence)));

	//Graph analysis
	tuple<size_t, bool> output;

	EdgeContainerT edgeList=problem.GetEdgeList();
	if(edgeList.empty())
		return output;

	set<unsigned int> vertexList;
	bool flagUniformWeight=true;
	double firstWeight=get<iWeight>(edgeList[0]);
	for(const auto& current : edgeList)
	{
		vertexList.insert(get<iVertex1>(current));
		vertexList.insert(get<iVertex2>(current));

		if(get<iWeight>(current)<=0)
			throw(invalid_argument(string("RandomSpanningTreeSamplerC::ValidateParameters: Weights must be positive. Value= ") + lexical_cast<string>(get<iWeight>(current)) ) );

		flagUniformWeight = flagUniformWeight && (get<iWeight>(current)==firstWeight);
	}	//for(const auto& current : edgeList)

	//Consecutiveness test
	if(*vertexList.rbegin()!= vertexList.size()-1)
		throw(invalid_argument("RandomSpanningTreeSamplerC::ValidateParameters: Vertex indices must form a consecutive sequence starting from 0 "));


	return make_tuple(vertexList.size(), flagUniformWeight);
}	//void ValidateParameters(const RandomSpanningTreeSamplerParametersC& parameters)

/**
 * @brief Makes a graph from an edge list
 * @param[in] edgeList Edge list
 * @param[in] nVertex Number of vertices
 * @return Graph corresponding to \c edgeList
 */
template<class ProblemT, class RNGT>
auto RandomSpanningTreeSamplerC<ProblemT, RNGT>::MakeGraph(const EdgeContainerT& edgeList, size_t nVertex) -> GraphT
{
	GraphT output(nVertex);
	for(const auto& current : edgeList)
		add_edge(get<iVertex1>(current), get<iVertex2>(current), get<iWeight>(current), output);

	return output;
}	//GraphT MakeGraph(const EdgeContainerT& edgeList)

/**
 * @brief Converts a spanning tree to an edge container
 * @param[in] spanningTree Spanning tree
 * @param[in] graph Graph
 * @return Edge list corresponding to the map
 */
template<class ProblemT, class RNGT>
auto RandomSpanningTreeSamplerC<ProblemT, RNGT>::SpanningTreeToEdgeContainer(const PredecessorMapT& spanningTree, const GraphT& graph) -> EdgeContainerT
{
	size_t nVertex=num_vertices(graph);
	EdgeContainerT output; output.reserve(nVertex-1);
	typedef typename graph_traits<GraphT>::edge_descriptor EdgeDescriptorT;
	typedef typename graph_traits<GraphT>::vertex_iterator VertexIteratorT;

	pair<VertexIteratorT, VertexIteratorT> itPairV=vertices(graph);
	for(; itPairV.first!=itPairV.second; advance(itPairV.first,1))
	{
		//Skip the root
		VertexDescriptorT predVertex=spanningTree[*itPairV.first];
		if(predVertex==graph_traits<GraphT>::null_vertex() || predVertex==*itPairV.first)
			continue;

		pair<EdgeDescriptorT, bool> currentEdge=boost::edge(predVertex, *itPairV.first, graph);	//The edge is guaranteed to exist
		output.emplace_back(source(currentEdge.first, graph), target(currentEdge.first, graph), boost::get(edge_weight, graph, currentEdge.first) );
	}	//for(; itPairV.first!=itPairV.second; advance(itPairV.first,1))

	return output;
}	//EdgeContainerT PredecessorMapToEdgeContainer(const PredecessorMapT& predecessorMap, const GraphT& graph)

/**
 * @brief Generates a random spanning tree
 * @param[in, out] rng Random number generator
 * @param[in] graph Graph
 * @param[in] flagUniformWeight If \c true unweighted sampler
 * @return Edges included in the spanning tree
 */
template<class ProblemT, class RNGT>
auto RandomSpanningTreeSamplerC<ProblemT, RNGT>::GenerateSample(RNGT& rng, const GraphT& graph, bool flagUniformWeight) -> EdgeContainerT
{
	//Random spanning tree
	map<VertexDescriptorT, VertexDescriptorT> predecessorMapContainer;
	PredecessorMapT spanningTree(predecessorMapContainer);

	if(flagUniformWeight)
		random_spanning_tree(graph, rng, predecessor_map(spanningTree));
	else
		random_spanning_tree(graph, rng, predecessor_map(spanningTree).weight_map( boost::get(edge_weight_t(), graph) ));

	return SpanningTreeToEdgeContainer(spanningTree, graph);
}	//EdgeContainerT GenerateSample(RNGT& rng, const GraphT& graph, bool flagUniformWeight)

/**
 * @brief Generates a model
 * @param[in,out] problem Problem
 * @param[in] generator Generator edges
 * @return Model. Invalid if model generation fails
 */
template<class ProblemT, class RNGT>
auto RandomSpanningTreeSamplerC<ProblemT, RNGT>::GenerateModel(ProblemT& problem, const EdgeContainerT& generator) -> optional<ModelT>
{
	return problem.GenerateModel(generator);
}	//optional<ModelT> GenerateModel(const EdgeContainerT& generator)

/**
 * @brief Evaluates a model
 * @param[in,out] problem Problem
 * @param[in] model Model
 * @param[in] edgeList
 * @return A tuple: error, indices of the inlier edges
 */
template<class ProblemT, class RNGT>
auto RandomSpanningTreeSamplerC<ProblemT, RNGT>::EvaluateModel(ProblemT& problem, const ModelT& model, const EdgeContainerT& edgeList) -> tuple<double, IndexContainerT>
{
	//Iterate over the edges
	size_t nEdge=edgeList.size();
	double error;	//Current error
	bool flagInlier;
	double errorAcc=0;	//Error accumulator
	IndexContainerT inliers; inliers.reserve(edgeList.size());
	for(size_t c=0; c<nEdge; ++c)
	{
		tie(flagInlier, error)=problem.EvaluateEdge(model, edgeList[c]);
		errorAcc+=error;

		if(flagInlier)
			inliers.push_back(c);
	};	//for(const auto& current : edgeList)

	inliers.shrink_to_fit();

	tuple<double, IndexContainerT> output;
	get<0>(output)=errorAcc;
	get<1>(output).swap(inliers);

	return output;
}	//tuple<double, IndexContainerT> EvaluateModel(ProblemT& problem, const ModelT& model)

/**
 * @brief Runs the algorithm
 * @param[in,out] result Result
 * @param[in,out] problem Problem
 * @param[in,out] rng Random number generator
 * @param[in] parameters Parameters
 * @throw invalid_argument If called on a graph that is not connected
 * @return Diagnostics object
 */
template<class ProblemT, class RNGT>
RandomSpanningTreeSamplerDiagnosticsC RandomSpanningTreeSamplerC<ProblemT, RNGT>::Run(result_type& result, ProblemT& problem, RNGT& rng, const RandomSpanningTreeSamplerParametersC& parameters)
{
	//Validation

	RandomSpanningTreeSamplerDiagnosticsC diagnostics;
	diagnostics.flagSuccess=true;

	result=ResultT();

	//Empty graph? Return
	EdgeContainerT edgeList=problem.GetEdgeList();
	if(edgeList.empty())
		return diagnostics;

	size_t nVertex;
	bool flagUniformWeight;
	tie(nVertex, flagUniformWeight)=ValidateInput(problem, parameters);

	//Initialisation
	GraphT graph=MakeGraph(edgeList, nVertex);
	long double nSTree=CountSpanningTrees(graph);
	double eta=log(1-parameters.minConfidence);

	//Not a connected graph
	if(nSTree==0)
		throw(invalid_argument("RandomSpanningTreeSamplerC::Run: Not a connected graph") );

	//State
	unsigned int cIteration=0;	//Iteration counter
	unsigned int maxIteration=parameters.maxIteration;	// Maximum number of iterations
	bool flagBreak=false;	//If true, terminate the iterations

	ResultT bestSolution(numeric_limits<double>::infinity(), ModelT(), EdgeContainerT(), EdgeContainerT());	//Best solution so far
	bool flagSolutionFound=false;

#pragma omp parallel if(parameters.nThreads>1) num_threads(parameters.nThreads)
	do
	{
		EdgeContainerT generator=GenerateSample(rng, graph, flagUniformWeight);
		optional<ModelT> currentModel=GenerateModel(problem, generator);

		IndexContainerT inliers;
		double error;
		bool flagUpdate=false;

		if(currentModel)
		{
			tie(error, inliers)=EvaluateModel(problem, *currentModel, edgeList);
			flagUpdate= (error<get<iModelError>(bestSolution));
		}	//if(currentModel)

		//State update

	#pragma omp critical (RSTS_R1)
	{
		++cIteration;

		if(flagUpdate)
		{
			//Do the inliers form a connected graph
			EdgeContainerT inlierList; inlierList.reserve(inliers.size());
			for_each(inliers, [&](size_t i){ inlierList.push_back(edgeList[i]);});

			//Count the number of spanning trees formed by the inliers
			GraphT inlierGraph=MakeGraph(inlierList, nVertex);
			long double nSTreeInlier=CountSpanningTrees(inlierGraph);

			if(nSTreeInlier!=0)
			{
				//Update the best solution
				bestSolution=ResultT(error, *currentModel, inlierList, generator);

				//Update maxIteration
				maxIteration=min(maxIteration, (unsigned int) (eta/log( max((long double)0.01, 1-nSTreeInlier/nSTree) )) );	//max operator
				flagSolutionFound=true;
			}	//if(tempInlierList!=0)
		}	//	if(flagUpdate)

		//Check termination
		if(cIteration>parameters.minIteration)
			flagBreak = flagBreak || (cIteration>=maxIteration);
	}	//#pragma omp critical (RSTS_R1)

	}while(!flagBreak);


	diagnostics.flagSuccess=flagSolutionFound;

	if(flagSolutionFound)
	{
		diagnostics.error=get<iModelError>(bestSolution);
		diagnostics.inlierRatio=(double)get<iInlierList>(bestSolution).size()/edgeList.size();
		diagnostics.nIteration=cIteration;
	}	//if(flagSolutionFound)

	result=bestSolution;

	return diagnostics;
}	//RandomSpanningTreeSamplerDiagnosticsC Run(result_type& result, ProblemT& problem, RNGT& rng, const RandomSpanningTreeSamplerParametersC& parameters)

/**
 * @brief Computes the minimum spanning tree solution
 * @param[out] result Estimated model
 * @param[in, out] problem Problem
 * @return Diagnostics object
 */
template<class ProblemT, class RNGT>
RandomSpanningTreeSamplerDiagnosticsC RandomSpanningTreeSamplerC<ProblemT, RNGT>::ComputeMSTSolution(result_type& result, ProblemT& problem)
{
	RandomSpanningTreeSamplerDiagnosticsC diagnostics;
	diagnostics.flagSuccess=true;

	result=ResultT();

	//Empty graph? Return
	EdgeContainerT edgeList=problem.GetEdgeList();
	if(edgeList.empty())
		return diagnostics;

	//Validation
	size_t nVertex;
	bool flagUniformWeight;
	tie(nVertex, flagUniformWeight)=ValidateInput(problem, RandomSpanningTreeSamplerParametersC());	//Default parameters are valid

	//Edge weight -> Edge distance
	auto maxWeight= get<iWeight>(*max_element(edgeList, [](const EdgeT& op1, const EdgeT& op2){ return get<iWeight>(op1) < get<iWeight>(op2); } )) + 1;
	for_each(edgeList, [&](EdgeT& value){ get<iWeight>(value)=maxWeight-get<iWeight>(value); } );

	//Minimum spanning tree
	GraphT graph=MakeGraph(edgeList, nVertex);

	typedef typename graph_traits<GraphT>::vertex_descriptor VertexDescriptorT;
	map<VertexDescriptorT, VertexDescriptorT> predecessorMapContainer;
	associative_property_map<map<VertexDescriptorT, VertexDescriptorT> > spanningTree(predecessorMapContainer);

	prim_minimum_spanning_tree(graph, spanningTree);
	EdgeContainerT generator=SpanningTreeToEdgeContainer(spanningTree, graph);

	//Model
	optional<ModelT> model=problem.GenerateModel(generator);

	if(!model)
	{
		diagnostics.flagSuccess=false;
		return diagnostics;
	}	//if(!model)

	//Inliers
	IndexContainerT inliers;
	double error;
	tie(error, inliers)=EvaluateModel(problem, *model, edgeList);

	EdgeContainerT inlierEdges; inlierEdges.reserve(inliers.size());
	for_each(inliers, [&](size_t value){ inlierEdges.push_back(edgeList[value]); } );

	//Output
	result=ResultT(error, *model, inlierEdges, generator);
	diagnostics.error=error;
	diagnostics.inlierRatio=(double)inliers.size()/edgeList.size();
	diagnostics.nIteration=1;

	return diagnostics;
}	//RandomSpanningTreeSamplerDiagnosticsC ComputeMSTSolution(result_type& result, const ProblemT& problem)

}	//RandomSpanningTreeSamplerN
}	//SeeSawN



#endif /* RANDOM_SPANNING_TREE_SAMPLER_IPP_690126 */
