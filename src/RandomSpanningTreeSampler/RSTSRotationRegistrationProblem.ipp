/**
 * @file RSTSRotationRegistrationProblem.ipp Implementation of the RSTS problem for rotation registration
 * @author Evren Imre
 * @date 4 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RSTS_ROTATION_REGISTRATION_PROBLEM_IPP_2095880
#define RSTS_ROTATION_REGISTRATION_PROBLEM_IPP_2095880

#include <boost/optional.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/range/algorithm.hpp>
#include <vector>
#include <tuple>
#include <map>
#include <utility>
#include <limits>
#include <cmath>
#include <stdexcept>
#include "../Elements/GeometricEntity.h"
#include "../Geometry/RelativeRotationRegistration.h"

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

using boost::optional;
using boost::math::constants::pi;
using boost::range::for_each;
using std::vector;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::map;
using std::pair;
using std::make_pair;
using std::get;
using std::swap;
using std::numeric_limits;
using std::min;
using std::invalid_argument;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::GeometryN::RelativeRotationRegistrationC;

/**
 * @brief RSTS problem for rotation registration
 * @remarks A model of \c RandomSpanningTreeSampleProblemConceptC
 * @remarks Fitness error: min(inlierTh, Orientation error) * observation weight. But the inlier decision is made only on the orientation error
 * @ingroup Problem
 * @nosubgrouping
 */
class RSTSRotationRegistrationProblemC
{
	private:

		typedef RelativeRotationRegistrationC::real_type RealT;

		typedef vector<QuaternionT> ModelT;	///< A vector of rotation matrices
		typedef tuple<unsigned int, unsigned int, RealT> EdgeT;	///< A graph edge
		typedef vector<EdgeT> EdgeContainerT;	///< An edge container

		typedef RelativeRotationRegistrationC::observation_type ObservationT;	///< An observation
		typedef RelativeRotationRegistrationC::quaternion_observation_type ObservationQT;	///< A quaternion observation

		typedef pair<unsigned int, unsigned int> IndexPairT;	///< An index pair

		/** @name Configuration */ //@{
		vector<ObservationT> observations;	///< Relative rotation observations
		vector<ObservationQT> observationsQ;	///< Relative rotation observations
		RealT inlierThreshold;	///< Inlier threshold. The angle between an observation and its prediction. [0, pi]
		//@}

		/** @name State */ //@{
		bool flagValid;	///< \c true if the object is initialised
		EdgeContainerT edgeList;	///< Edge list
		map<IndexPairT, pair<size_t, bool> > edgeIndexArray;	///< Maps the edges to observation indices. Edge; [index, transpose?]
		//@}

		/** @name Implementation details */ //@{
		void Initialise();	///< Initialises the object
		pair<size_t, bool> RetrieveObservationIndex(const IndexPairT& edgeIndex) const;	///< Finds the observation corresponding to an edge
		//@}

	public:

		/** @name Edge structure */ //@{
		typedef EdgeT edge_type;	///< A graph edge
		static constexpr size_t iVertex1=0; 	///< Index of the component for the first vertex
		static constexpr size_t iVertex2=1;	///< Index of the component for the second vertex
		static constexpr size_t iWeight=2;	///< Index of the component for the edge weight
		//@}

		RSTSRotationRegistrationProblemC(const vector<ObservationT>& oobservations, double iinlierThreshold);	///< Constructor
		RSTSRotationRegistrationProblemC(const vector<ObservationQT>& oobservations, double iinlierThreshold);	///< Constructor

		/**@name RandomSpanningTreeSampleProblemConceptC interface */ //@{
		typedef ModelT model_type;	///< Type of the geometric model estimated by the problem

		RSTSRotationRegistrationProblemC();	///< Default constructor

		const EdgeContainerT& GetEdgeList() const;	///< Returns the edge list for the graph representation of the relative rotations
		optional<ModelT> GenerateModel(const EdgeContainerT& generator) const;		///< Generates a model from a list of relative rotations
		tuple<bool, double> EvaluateEdge(const ModelT& model, const EdgeT& edge) const;	///< Evaluates the fitness of a relative rotation against an absolute rotation hypothesis

		bool IsValid() const;	///< Returns \c flagValid
		//@}

		/** @name Utilities */ //@{
		tuple<vector<size_t>, RealT> EvaluateModel(const ModelT& model) const;	///< Evaluates the total fitness a model against the observations
		vector<size_t> RetrieveObservationIndexList(const EdgeContainerT& edgeContainer) const;	///< Retrieves the observation indices corresponding to an edge list
		//@}
};	//class RSRSRotationRegistrationProblemC

}	//RandomSpanningTreeSamplerN
}	//SeeSawN




#endif /* RSTS_ROTATION_REGISTRATION_PROBLEM_IPP_2095880 */
