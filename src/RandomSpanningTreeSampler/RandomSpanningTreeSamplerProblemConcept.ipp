/**
 * @file RandomSpanningTreeSamplerProblemConcept.ipp Concept definition for a random spanning tree sampler problem
 * @author Evren Imre
 * @date 30 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RANDOM_SPANNING_TREE_SAMPLER_PROBLEM_CONCEPT_IPP_8032891
#define RANDOM_SPANNING_TREE_SAMPLER_PROBLEM_CONCEPT_IPP_8032891

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <tuple>

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

using boost::CopyConstructible;
using boost::Assignable;
using boost::DefaultConstructible;
using boost::optional;
using std::vector;
using std::tuple;

/**
 * @brief Concept checker for random spanning tree sampler problems
 * @tparam TestT Concept to be tested
 * @ingroup Concept
 */
template <class TestT>
class RandomSpanningTreeSamplerProblemConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((DefaultConstructible<TestT>));

	public:

		BOOST_CONCEPT_USAGE(RandomSpanningTreeSamplerProblemConceptC)
		{
			typedef typename TestT::model_type ModelT;

			TestT test;
			test.IsValid();

			vector< tuple<unsigned int, unsigned int, double> > edgeList=test.GetEdgeList();
			optional<ModelT> model=test.GenerateModel(edgeList);

			tuple<bool double> evaluation=test.EvaluateEdge(model, edgeList[0]); (void)evaluation;
		}	//RandomSpanningTreeSamplerProblemConceptC

	private:
		BOOST_CONCEPT_ASSERT((DefaultConstructible<typename TestT::model_type>));
	///@endcond

};	//class RandomSpanningTreeSamplerProblemConceptC




}	//RandomSpanningTreeSamplerN
}	//SeeSawN




#endif /* RANDOM_SPANNING_TREE_SAMPLER_PROBLEM_CONCEPT_IPP_8032891 */
