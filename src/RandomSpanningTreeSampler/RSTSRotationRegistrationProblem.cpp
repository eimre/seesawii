/**
 * @file RSTSRotationRegistrationProblem.cpp Implementation of the RSTS problem for rotation registration
 * @author Evren Imre
 * @date 4 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RSTSRotationRegistrationProblem.h"

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

/**
 * @brief Constructor
 * @param oobservations Relative rotation observations, rotation matrix variant
 * @param iinlierThreshold Inlier threshold, as the angle between an observation and its prediction
 * @pre \c iinlierThreshold is in [0,pi]
 * @post The object is valid
 */
RSTSRotationRegistrationProblemC::RSTSRotationRegistrationProblemC(const vector<ObservationT>& oobservations, double iinlierThreshold) : observations(oobservations), inlierThreshold(iinlierThreshold), flagValid(true)
{
	//Preconditions
	assert(inlierThreshold>=0 && inlierThreshold<=pi<double>());
	Initialise();
}	//RSTSRotationRegistrationProblemC(const map<IndexPairT, pair<RotationMatrix3DT, double> >& oobservations, double iinlierThreshold)

/**
 * @brief Constructor
 * @param oobservations Relative rotation observations, quaternion variant
 * @param iinlierThreshold Inlier threshold, as the angle between an observation and its prediction
 * @post The object is valid
 */
RSTSRotationRegistrationProblemC::RSTSRotationRegistrationProblemC(const vector<ObservationQT>& oobservations, double iinlierThreshold) : observationsQ(oobservations), inlierThreshold(iinlierThreshold), flagValid(true)
{
	//Preconditions
	assert(inlierThreshold>=0 && inlierThreshold<=pi<double>());
	Initialise();
}	//RSTSRotationRegistrationProblemC(const map<IndexPairT, pair<RotationMatrix3DT, double> >& oobservations, double iinlierThreshold)

/**
 * @brief Initialises the object
 * @pre \c flagValid is true
 * @throws If the observations do not satisfy the conditions imposed by the registration solver
 */
void RSTSRotationRegistrationProblemC::Initialise()
{
	//Preconditions
	assert(flagValid);

	constexpr size_t iIndex1=RelativeRotationRegistrationC::iIndex1;
	constexpr size_t iIndex2=RelativeRotationRegistrationC::iIndex2;
	constexpr size_t iRotation=RelativeRotationRegistrationC::iRotation;
	constexpr size_t iWeight2=RelativeRotationRegistrationC::iWeight;

	//Initialise the observation arrays

	size_t nObservations = observations.empty() ? observationsQ.size() : observations.size();

	if(!observations.empty() && observationsQ.empty())
	{
		observationsQ.reserve(nObservations);
		for(const auto& current : observations)
			observationsQ.emplace_back(get<iIndex1>(current), get<iIndex2>(current), QuaternionT(get<iRotation>(current)), get<iWeight2>(current) );
	}	//if(!observations.empty() && observationsQ.empty())

	if(observations.empty() && !observationsQ.empty())
	{
		observations.reserve(nObservations);
		for(const auto& current : observationsQ)
			observations.emplace_back(get<iIndex1>(current), get<iIndex2>(current), get<iRotation>(current).toRotationMatrix(), get<iWeight2>(current) );
	}	//if(observations.empty() && !observationsQ.empty())

	if(!RelativeRotationRegistrationC::ValidateObservations(observations))
		throw(invalid_argument("RSTSRotationRegistrationProblemC::Initialise: Invalid observation set. RelativeRotationRegistrationC::ValidateObservations failed"));

	//Initialise the helper array
	edgeList.reserve(nObservations);
	for(size_t c=0; c<nObservations; ++c)
	{
		edgeList.emplace_back(get<iIndex1>(observations[c]), get<iIndex2>(observations[c]), get<iWeight2>(observations[c]));
		edgeIndexArray.emplace( IndexPairT(get<iIndex1>(observations[c]), get<iIndex2>(observations[c])), make_pair(c, false) );
		edgeIndexArray.emplace( IndexPairT(get<iIndex2>(observations[c]), get<iIndex1>(observations[c])), make_pair(c, true) );
	}	//for(size_t c=0; c<nObservations; ++c)

}	//void Initialise()

/**
 * @brief Finds the observation corresponding to an edge
 * @param[in] edgeIndex Edge index
 * @return A pair: The corresponding index in \c observations; flag indicating whether the indices appear as swapped in \c observations
 * @pre \c edgeIndex defines a valid edge in \c observations
 * @remarks If the indices are swapped, the rotation measurement should be transposed
 */
pair<size_t, bool> RSTSRotationRegistrationProblemC::RetrieveObservationIndex(const IndexPairT& edgeIndex) const
{
	return edgeIndexArray.find(edgeIndex)->second;
}	//size_t RetrieveObservationIndex(const IndexPairT& edgeIndex)

/**
 * @brief Default constructor
 * @post Invalid object
 */
RSTSRotationRegistrationProblemC::RSTSRotationRegistrationProblemC() : inlierThreshold(numeric_limits<double>::infinity()), flagValid(false)
{}

/**
 * @brief Returns the edge list for the graph representation of the relative rotations
 * @return Edges of the graph representation the observations
 * @pre The object is initialised
 */
auto RSTSRotationRegistrationProblemC::GetEdgeList() const -> const EdgeContainerT&
{
	//Preconditions
	assert(flagValid);
	return edgeList;
}	//EdgeContainerT GetEdgeList()

/**
 * @brief Generates a model from a list of relative rotations
 * @param[in] generator Generator edges
 * @return Model. Invalid if the solver fails
 * @pre The object is initialised
 */
auto RSTSRotationRegistrationProblemC::GenerateModel(const EdgeContainerT& generator) const -> optional<ModelT>
{
	//Preconditions
	assert(flagValid);

	//Observation list
	vector<RelativeRotationRegistrationC::observation_type> relativeRotations; relativeRotations.reserve(generator.size());
	for(const auto& current : generator)
	{
		size_t i1=get<iVertex1>(current);
		size_t i2=get<iVertex2>(current);
		pair<size_t, bool> observationIndex=RetrieveObservationIndex(IndexPairT(i1,i2));
		relativeRotations.emplace_back(i1, i2, get<RelativeRotationRegistrationC::iRotation>(observations[observationIndex.first]), get<iWeight>(current));

		if(observationIndex.second)
			get<RelativeRotationRegistrationC::iRotation>(*relativeRotations.rbegin()).transposeInPlace();
	}	//for(const auto& current : generator)

	//Solver
	vector<RotationMatrix3DT> absoluteRotations=RelativeRotationRegistrationC::Run(relativeRotations, false);

	optional<vector<QuaternionT> > output;
	if(absoluteRotations.empty())
		return output;

	output=vector<QuaternionT>();
	output->reserve(absoluteRotations.size());
	for_each(absoluteRotations, [&](const RotationMatrix3DT& mR){output->emplace_back(mR);} );

	return output;
}	//optional<ModelT> GenerateModel(const EdgeContainerT& generator)

/**
 * @brief Evaluates the fitness of a relative rotation against an absolute rotation hypothesis
 * @param[in] model Absolute rotations
 * @param[in] edge Relative rotation to be evaluated
 * @return  A tuple: [inlier flag; error]
 * @pre The object is initialised
 */
tuple<bool, double> RSTSRotationRegistrationProblemC::EvaluateEdge(const ModelT& model, const EdgeT& edge)  const
{
	//Preconditions
	assert(flagValid);

	IndexPairT index;
	index.first=get<iVertex1>(edge);
	index.second=get<iVertex2>(edge);

	QuaternionT prediction= model[index.second] * model[index.first].conjugate();	//Prediction

	//Observation
	pair<size_t, bool> observationIndex=RetrieveObservationIndex(index);
	QuaternionT observed = get<RelativeRotationRegistrationC::iRotation>(observationsQ[observationIndex.first]);
	if(observationIndex.second)
		observed=observed.conjugate();

	RealT error=RelativeRotationRegistrationC::ComputeAngularDeviation(observed, prediction);

	return make_tuple(error<inlierThreshold, min(error, inlierThreshold)*get<iWeight>(edge));
}	//tuple<bool, double> EvaluateEdge(const ModelT& model, const EdgeT& edge)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool RSTSRotationRegistrationProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Evaluates the total fitness a model against the observations
 * @param[in] model An absolute rotation sequence
 * @return A tuple: Inlier indices; Total fitness error
 * @pre For each observation index there is a corresponding measurement (unenforced)
 */
auto RSTSRotationRegistrationProblemC::EvaluateModel(const ModelT& model) const -> tuple<vector<size_t>, RealT>
{
	size_t nObservations=observationsQ.size();
	RealT errorAcc=0;
	vector<size_t> inliers; inliers.reserve(nObservations);

	size_t i1;
	size_t i2;
	RealT weight;
	QuaternionT observed;
	for(size_t c=0; c<nObservations; ++c)
	{
		tie(i1, i2, observed, weight)=observationsQ[c];

		QuaternionT prediction=model[i2]*model[i1].conjugate();
		RealT error=RelativeRotationRegistrationC::ComputeAngularDeviation(observed, prediction);

		if(error<inlierThreshold)
			inliers.push_back(c);

		errorAcc+= min(error, inlierThreshold)*weight;
	}	//for(const auto& current : observationsQ)
	return make_tuple(inliers, errorAcc);
}	//RealT EvaluateObservations(const ModelT& model)

/**
 * @brief Retrieves the observation indices corresponding to an edge list
 * @param[in] edgeContainer Edge container
 * @return Indices corresponding to the elements of \c edgeContainer
 * @pre \c edgeContainer contains only the edges in that are in \c observations (unenforced). The RSTS engine guarantees that
 */
vector<size_t> RSTSRotationRegistrationProblemC::RetrieveObservationIndexList(const EdgeContainerT& edgeContainer) const
{
	vector<size_t> output; output.reserve(edgeContainer.size());
	for_each(edgeContainer, [&](const EdgeT& current){ output.push_back(RetrieveObservationIndex(IndexPairT(get<iVertex1>(current), get<iVertex2>(current))).first);});

	return output;
}	//vector<size_t> RetrieveObservationIndexList(const EdgeContainerT& edgeContainer)

}	//RandomSpanningTreeSamplerN
}	//SeeSawN
