/**
 * @file RSTSRatioRegistrationProblem.ipp Implementation details for \c RSTSRatioRegistrationProblemC
 * @author Evren Imre
 * @date 12 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef RSTS_RATIO_REGISTRATION_PROBLEM_IPP_1808002
#define RSTS_RATIO_REGISTRATION_PROBLEM_IPP_1808002

#include <boost/optional.hpp>
#include <tuple>
#include <vector>
#include <map>
#include <utility>
#include <cstddef>
#include <math.h>
#include <stdexcept>
#include <limits>
#include "../Numeric/RatioRegistration.h"

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

using boost::optional;
using std::tuple;
using std::get;
using std::make_tuple;
using std::vector;
using std::map;
using std::pair;
using std::make_pair;
using std::size_t;
using std::fabs;
using std::min;
using std::invalid_argument;
using std::numeric_limits;
using SeeSawN::NumericN::RatioRegistrationC;

/**
 * @brief Random spanning tree problem for ratio registration
 * @ingroup Problem
 * @nosubgrouping
 */
class RSTSRatioRegistrationProblemC
{
	private:

		typedef vector<double> ModelT;	///< A model, the element values
		typedef tuple<unsigned int, unsigned int, double> EdgeT;	///< A graph edge
		typedef vector<EdgeT> EdgeContainerT;	///< An edge container

		typedef RatioRegistrationC::observation_type ObservationT;	///< An observation

		typedef pair<unsigned int, unsigned int> IndexPairT;	///< An index pair

		/** @name Configuration */ //@{
		vector<ObservationT> observations;	///< Ratio observations
		double inlierThreshold;	///< Inlier threshold. Relative error between the predicted and the observed ratios
		//@}

		/** @name State */ //@{
		bool flagValid;	///< \c true if the object is initialised
		EdgeContainerT edgeList;	///< Edge list
		map<IndexPairT, pair<size_t, bool> > edgeIndexArray;	///< Maps the edges to observation indices. Edge; [index, transpose?]
		//@}

		/** @name Implementation details */ //@{
		pair<size_t, bool> RetrieveObservationIndex(const IndexPairT& edgeIndex) const;	///< Finds the observation corresponding to an edge
		//@}

	public:

		/** @name Edge structure */ //@{
		typedef EdgeT edge_type;	///< A graph edge
		static constexpr size_t iVertex1=0; 	///< Index of the component for the first vertex
		static constexpr size_t iVertex2=1;	///< Index of the component for the second vertex
		static constexpr size_t iWeight=2;	///< Index of the component for the edge weight
		//@}

		RSTSRatioRegistrationProblemC(const vector<ObservationT>& oobservations, double iinlierThreshold);	///< Constructor

		/**@name RandomSpanningTreeSampleProblemConceptC interface */ //@{
		typedef ModelT model_type;	///< Type of the geometric model estimated by the problem

		RSTSRatioRegistrationProblemC();	///< Default constructor

		const EdgeContainerT& GetEdgeList() const;	///< Returns the edge list for the graph representation of the ratios
		optional<ModelT> GenerateModel(const EdgeContainerT& generator) const;		///< Generates a model from a list of ratios
		tuple<bool, double> EvaluateEdge(const ModelT& model, const EdgeT& edge) const;	///< Evaluates the fitness of a set of ratios against a hypothesis of element values

		bool IsValid() const;	///< Returns \c flagValid
		//@}
};	//class RSTSRatioRegistrationProblemC

}	//RandomSpanningTreeSamplerN
}	//SeeSawN



#endif /* RSTS_RATIO_REGISTRATION_PROBLEM_IPP_1808002 */
