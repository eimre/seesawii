/**
 * @file RSTSRatioRegistrationProblem.cpp Implementation of \c RSTSRatioRegistrationProblemC
 * @author Evren Imre
 * @date 12 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "RSTSRatioRegistrationProblem.h"

namespace SeeSawN
{
namespace RandomSpanningTreeSamplerN
{

/**
 * @brief Constructor
 * @param oobservations Ratio observations
 * @param iinlierThreshold Inlier threshold, relative error between the ratios
 * @pre \c iinlierThreshold>0
 * @post The object is valid
 */
RSTSRatioRegistrationProblemC::RSTSRatioRegistrationProblemC(const vector<ObservationT>& oobservations, double iinlierThreshold) : observations(oobservations), inlierThreshold(iinlierThreshold), flagValid(true)
{
	//Preconditions
	assert(inlierThreshold>=0);

	if(!RatioRegistrationC::ValidateObservations(observations))
		throw(invalid_argument("RSTSRatioRegistrationProblemC::Initialise: Invalid observation set. RatioRegistrationC::ValidateObservations failed"));


	constexpr size_t iIndex1=RatioRegistrationC::iIndex1;
	constexpr size_t iIndex2=RatioRegistrationC::iIndex2;
	constexpr size_t iWeight2=RatioRegistrationC::iWeight;

	//Initialise the helper array
	size_t nObservations = observations.size();
	edgeList.reserve(nObservations);
	for(size_t c=0; c<nObservations; ++c)
	{
		edgeList.emplace_back(get<iIndex1>(observations[c]), get<iIndex2>(observations[c]), get<iWeight2>(observations[c]));
		edgeIndexArray.emplace( IndexPairT(get<iIndex1>(observations[c]), get<iIndex2>(observations[c])), make_pair(c, false) );
		edgeIndexArray.emplace( IndexPairT(get<iIndex2>(observations[c]), get<iIndex1>(observations[c])), make_pair(c, true) );
	}	//for(size_t c=0; c<nObservations; ++c)


}	//RSTSRotationRegistrationProblemC(const map<IndexPairT, pair<RotationMatrix3DT, double> >& oobservations, double iinlierThreshold)

/**
 * @brief Finds the observation corresponding to an edge
 * @param[in] edgeIndex Edge index
 * @return A pair: The corresponding index in \c observations; flag indicating whether the indices appear as swapped in \c observations
 * @pre \c edgeIndex defines a valid edge in \c observations
 * @remarks If the indices are swapped, the ratio measurement should be inverted
 */
pair<size_t, bool> RSTSRatioRegistrationProblemC::RetrieveObservationIndex(const IndexPairT& edgeIndex) const
{
	return edgeIndexArray.find(edgeIndex)->second;
}	//size_t RetrieveObservationIndex(const IndexPairT& edgeIndex)

/**
 * @brief Default constructor
 * @post Invalid object
 */
RSTSRatioRegistrationProblemC::RSTSRatioRegistrationProblemC() : inlierThreshold(numeric_limits<double>::infinity()), flagValid(false)
{}

/**
 * @brief Returns the edge list for the graph representation of the ratios
 * @return Edges of the graph representation the observations
 * @pre The object is initialised
 */
auto RSTSRatioRegistrationProblemC::GetEdgeList() const -> const EdgeContainerT&
{
	//Preconditions
	assert(flagValid);
	return edgeList;
}	//EdgeContainerT GetEdgeList()

/**
 * @brief Generates a model from a list of relative rotations
 * @param[in] generator Generator edges
 * @return Model. Invalid if the solver fails
 * @pre The object is initialised
 */
auto RSTSRatioRegistrationProblemC::GenerateModel(const EdgeContainerT& generator) const -> optional<ModelT>
{
	//Preconditions
	assert(flagValid);

	//Observation list
	vector<RatioRegistrationC::observation_type> ratios; ratios.reserve(generator.size());
	for(const auto& current : generator)
	{
		size_t i1=get<iVertex1>(current);
		size_t i2=get<iVertex2>(current);
		pair<size_t, bool> observationIndex=RetrieveObservationIndex(IndexPairT(i1,i2));
		ratios.emplace_back(i1, i2, get<RatioRegistrationC::iRatio>(observations[observationIndex.first]), get<iWeight>(current));

		if(observationIndex.second)
			get<RatioRegistrationC::iRatio>(*ratios.rbegin())=1.0/get<RatioRegistrationC::iRatio>(*ratios.rbegin());
	}	//for(const auto& current : generator)

	//Solver
	vector<double> solution=RatioRegistrationC::Run(ratios, false);

	optional<vector<double> > output;
	if(!solution.empty())
		output=solution;

	return output;
}	//optional<ModelT> GenerateModel(const EdgeContainerT& generator)

/**
 * @brief Evaluates the fitness of a set of ratios against a hypothesis of element values
 * @param[in] model Absolute rotations
 * @param[in] edge Relative rotation to be evaluated
 * @return  A tuple: [inlier flag; error]
 * @pre The object is initialised
 */
tuple<bool, double> RSTSRatioRegistrationProblemC::EvaluateEdge(const ModelT& model, const EdgeT& edge)  const
{
	//Preconditions
	assert(flagValid);

	IndexPairT index;
	index.first=get<iVertex1>(edge);
	index.second=get<iVertex2>(edge);

	double prediction = model[index.second]/model[index.first];	//Prediction

	//Observation
	pair<size_t, bool> observationIndex=RetrieveObservationIndex(index);
	double observed = get<RatioRegistrationC::iRatio>(observations[observationIndex.first]);
	if(observationIndex.second)
		observed=1.0/observed;

	double error= fabs((prediction-observed)/min( fabs(prediction), fabs(observed) ));

	return make_tuple(error<inlierThreshold, min(error, inlierThreshold)*get<iWeight>(edge));
}	//tuple<bool, double> EvaluateEdge(const ModelT& model, const EdgeT& edge)

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool RSTSRatioRegistrationProblemC::IsValid() const
{
	return flagValid;
}	//bool IsValid()

}	//RandomSpanningTreeSamplerN
}	//SeeSawN
