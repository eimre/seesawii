/**
 * @file ImageFeatureIO.ipp Implementation of ImageFeatureIOC
 * @author Evren Imre
 * @date 18 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_IO_IPP_8421305
#define IMAGE_FEATURE_IO_IPP_8421305

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <iostream>
#include <ios>
#include <type_traits>
#include <iterator>
#include <limits>
#include <tuple>
#include <numeric>
#include <climits>
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Elements/FeatureConcept.h"
#include "../Elements/Camera.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/LensDistortionConcept.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace ION
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::lexical_cast;
using boost::tokenizer;
using boost::char_separator;
using boost::optional;
using Eigen::Matrix;
using std::is_same;
using std::is_convertible;
using std::string;
using std::vector;
using std::runtime_error;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::istream;
using std::stringstream;
using std::distance;
using std::advance;
using std::numeric_limits;
using std::streamsize;
using std::tuple;
using std::tie;
using std::random_shuffle;
using std::iota;
using std::back_inserter;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::DecimateFeatures2D;
using SeeSawN::ElementsN::ComputeBoundingBox2D;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::GeometryN::LensDistortionConceptC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::LensDistortionNoDC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;

/**
 * @brief Base class for image feature IO operations
 * @remarks Common operations
 * @ingroup IO
 * @nosubgrouping
 */
class BaseImageFeatureIOC
{
	private:

		template<class ImageFeatureT> static void Normalise(ImageFeatureT& dummy);	///< Default normalisation- dummy
		static void Normalise(ImageFeatureC& feature);	///< Descriptor normalisation for \c ImageFeatureC

	public:

	/** @name Operations */ //@{
    template<class ImageFeatureT, class LensDistortionT, class TransformationT> static void PreprocessFeatureSet(vector<ImageFeatureT>& featureList, const optional<tuple<typename ImageFeatureC::coordinate_type, typename ImageFeatureC::coordinate_type> >& boundingBox, double gridSpacing, bool flagNormalise, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation );  ///< Applies some common operations to the features
    template<class ImageFeatureT, class TransformationT> static void PreprocessFeatureSet(vector<ImageFeatureT>& featureList, const optional<tuple<typename ImageFeatureC::coordinate_type, typename ImageFeatureC::coordinate_type> >& boundingBox, double gridSpacing, bool flagNormalise, const LensDistortionC& lensDistortion,  const optional<TransformationT>& transformation);	///< Applies some common operations to the features

    template<class ImageFeatureT, class LensDistortionT, class TransformationT> static void PostprocessFeatureSet(vector<ImageFeatureT>& featureList, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation );	///< Common post-processing operations before writing to a file
    template<class ImageFeatureT, class TransformationT> static void PostprocessFeatureSet(vector<ImageFeatureT>& featureList, const LensDistortionC& lensDistortion, const optional<TransformationT>& transformation );	///< Common post-processing operations before writing to a file

    template<class ImageFeatureT, class LensDistortionT, class TransformationT> static void PreprocessFeature(ImageFeatureT& feature, bool flagNormalise, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation);	///< Common preprocessing operations after a read
    template<class ImageFeatureT, class LensDistortionT, class TransformationT> static void PostprocessFeature(ImageFeatureT& feature, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation);	///< Common post-processing operations prior to write
	//@}

};	//class BaseImageFeatureIOC

/**
 * @brief Image feature input/output operations
 * @remarks Format originally used by K.Mikolajczyk's tool at <A> http://www.robots.ox.ac.uk/~vgg/research/affine </A>
 * @remarks The class can discover the size of the RoI in a feature file, and process the file accordingly. 0-sized RoI fields are ok.
 * @ingroup IO
 * @nosubgrouping
 */
class ImageFeatureIOC : public BaseImageFeatureIOC
{
    public:

        /** @name Operations */ //@{
        static vector<ImageFeatureC> ReadFeatureFile(const string& filename, bool flagRandomise=true);   ///< Reads a feature file
        template<class FeatureRangeT> static void WriteFeatureFile(const FeatureRangeT& features, const string& filename);    ///< Writes a feature set to a file
        //@}

        /** @name Utilities */ //@{
        static void WriteSampleFile(const string& filename); ///< Writes a sample file to illustrate the format
        //@}
};  //class ImageFeatureIOC

/**
 * @brief Binary image feature input/output operations
 * @remarks Format originally used by K.Mikolajczyk's tool at <A> http://www.robots.ox.ac.uk/~vgg/research/affine </A>
 * @remarks The class can discover the size of the RoI in a feature file, and process the file accordingly. 0-sized RoI fields are ok.
 * @ingroup IO
 * @nosubgrouping
 */
class BinaryImageFeatureIOC : public BaseImageFeatureIOC
{
	private:
		static BinaryImageFeatureC::descriptor_type ReadDescriptor(vector<int>& blocks, size_t sDescriptor);	///< Reads a descriptor

    public:

        /** @name Operations */ //@{
		static BinaryImageFeatureC ReadFeature(istream& src, size_t sDescriptor, size_t nBlocks, size_t sRoI);	///< Reads a binary image feature from a stream
        static vector<BinaryImageFeatureC> ReadFeatureFile(const string& filename, bool flagRandomise=true);   ///< Reads a feature file
        template<class FeatureRangeT> static void WriteFeatureFile(const FeatureRangeT& features, const string& filename);    ///< Writes a feature set to a file
        //@}

        /** @name Utilities */ //@{
        static void WriteSampleFile(const string& filename); ///< Writes a sample file to illustrate the format
        //@}
};  //class ImageFeatureIOC

/********** IMPLEMENTATION STARTS HERE **********/

/*********** BaseImageFeatureIOC **********/

/**
 * @brief Default normalisation- dummy
 * @param[in] dummy Dummy parameter
 */
template<class ImageFeatureT>
void BaseImageFeatureIOC::Normalise(ImageFeatureT& dummy)
{}

/**
 * @brief Common preprocessing operations after a read
 * @tparam ImageFeatureT An image feature
 * @tparam LensDistortionT A lens distoriton model
 * @tparam TransformationT A transformation
 * @param[in, out] feature Feature to be preprocessed
 * @param[in] flagNormalise If \c true , the descriptor is normalised
 * @param[in] lensDistortion Lens distortion
 * @param[in] transformation Transformation
 * @pre \c ImageFeatureT is a feature, and its coordinate type is convertible to \c Coordinate2DT
 * @pre \c LensDistoritonModelT satisfies LensDistortionConceptC
 * @pre \c TransformationT is an Eigen geometry transformation (unenforced)
 * @remarks Coordinate transformations: Correct the lens distortion, then transform
 */
template<class ImageFeatureT, class LensDistortionT, class TransformationT>
void BaseImageFeatureIOC::PreprocessFeature(ImageFeatureT& feature, bool flagNormalise, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((LensDistortionConceptC<LensDistortionT>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<ImageFeatureT>));
	static_assert(is_convertible<typename ImageFeatureT::coordinate_type, Coordinate2DT>::value, "BaseImageFeatureIOC::PreprocessFeature: ImageFeatureT::coordinate_type must be convertible to Coordinate2DT" );

	typedef typename ImageFeatureT::coordinate_type CoordinateT;

	//Normalise
	if(flagNormalise)
		Normalise(feature);

    //Coordinate transformations

    if(lensDistortion)
    {
    	optional<CoordinateT> transformed=lensDistortion->Correct(feature.Coordinate());
    	if(transformed)
    		feature.Coordinate().swap(*transformed);
    }	//if(lensDistortion)

    if(transformation)
		feature.Coordinate()=CoordinateTransformations2DT::TransformCoordinate(feature.Coordinate(), *transformation);
}	//void PreprocessFeature(ImageFeatureC& feature, bool flagNormalise, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation)

/**
 * @brief Applies some common operations to the features
 * @tparam ImageFeatureT An image feature
 * @tparam LensDistortionT A lens distoriton model
 * @tparam TransformationT A transformation
 * @param[in,out] featureList Features to be processed
 * @param[in] boundingBox Bounding box. If not valid, it is computed inside the function
 * @param[in] gridSpacing Grid spacing for decimation
 * @param[in] flagNormalise \c true if the feature vectors are to be normalised
 * @param[in] lensDistortion Lens distortion object. If not valid, no lens distortion
 * @param[in] transformation Coordinate transformation, for normalisation
 * @pre \c ImageFeatureT is a feature, and its coordinate type is convertible to \c Coordinate2DT
 * @remarks A convenience function bundling some common operations together: Find bounding box, decimate, normalise, correct lens distortion, apply coordinate normalisation
 * @remarks Coordinate transformations: Correct the lens distortion, then transform
 * @remarks In applications involving feature matching, the decimation operation should be used with care. A fixed-grid can introduce errors in tracking, when there is a relative motion between the camera and a feature. General feature matching is bound to be affected, but to a lesser extent.
 */
template<class ImageFeatureT, class LensDistortionT, class TransformationT>
void BaseImageFeatureIOC::PreprocessFeatureSet(vector<ImageFeatureT>& featureList, const optional<tuple<typename ImageFeatureC::coordinate_type, typename ImageFeatureC::coordinate_type> >& boundingBox, double gridSpacing, bool flagNormalise, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<ImageFeatureT>));
	static_assert(is_convertible<typename ImageFeatureT::coordinate_type, Coordinate2DT>::value, "BaseImageFeatureIOC::PreprocessFeatureSet: ImageFeatureT::coordinate_type must be convertible to Coordinate2DT" );

    vector<ImageFeatureT> rawFeatureList; rawFeatureList.swap(featureList);

    typedef typename ImageFeatureT::coordinate_type CoordinateT;

    //Find the bounding box, if necessary

    vector<CoordinateT> coordinateList=MakeCoordinateVector(rawFeatureList);

    CoordinateT ul;
    CoordinateT lr;
    if(!boundingBox)
        tie(ul, lr)=ComputeBoundingBox2D(coordinateList);
    else
        tie(ul, lr)=*boundingBox;

    //Decimate
    vector<size_t> indexList=DecimateFeatures2D(coordinateList, gridSpacing, ul, lr);

    size_t nIndex=indexList.size();
    featureList.resize(nIndex);

    //Output
    for(size_t c=0; c<nIndex; ++c)
    {
    	featureList[c].Swap(rawFeatureList[indexList[c]]);
    	PreprocessFeature(featureList[c], flagNormalise, lensDistortion, transformation);
    }
}   //vector<ImageFeatureC> PreprocessFeatureSet(const string& filename, const optional<tuple<Coordinate2DT, Coordinate2DT> >& boundingBox, double gridSpacing, bool flagNormalise )

/**
 * @brief Applies some common operations to the features
 * @tparam ImageFeatureT An image feature
 * @tparam TransformationT A transformation
 * @param[in,out] featureList Features to be processed
 * @param[in] boundingBox Bounding box. If not valid, it is computed inside the function
 * @param[in] gridSpacing Grid spacing for decimation
 * @param[in] flagNormalise \c true if the feature vectors are to be normalised
 * @param[in] lensDistortion Lens distortion
 * @param[in] transformation Coordinate transformation, for normalisation
 * @return Processed features
 * @remarks A convenience function bundling some common operations together: Read, find bounding box, decimate, normalise, correct lens distortion, apply coordinate normalisation
 * @remarks Coordinate transformations: Correct the lens distortion, then normalise
 * @remarks In applications involving feature matching, the decimation operation should be used with care. A fixed-grid can introduce errors in tracking, when there is a relative motion between the camera and a feature. General feature matching is bound to be affected, but to a lesser extent.
 * @remarks Untested
 */
template<class ImageFeatureT, class TransformationT>
void BaseImageFeatureIOC::PreprocessFeatureSet(vector<ImageFeatureT>& featureList, const optional<tuple<typename ImageFeatureC::coordinate_type, typename ImageFeatureC::coordinate_type> >& boundingBox, double gridSpacing, bool flagNormalise, const LensDistortionC& lensDistortion, const optional<TransformationT>& transformation)
{
	if(lensDistortion.modelCode==LensDistortionCodeT::FP1)
	{
		optional<LensDistortionFP1C> distortion=LensDistortionFP1C(lensDistortion.centre, lensDistortion.kappa);
		PreprocessFeatureSet(featureList, boundingBox, gridSpacing, flagNormalise, distortion, transformation);
		return;
	}	//if(distortionCode==LensDistortionCodeT::FP1)

	if(lensDistortion.modelCode==LensDistortionCodeT::OP1)
	{
		optional<LensDistortionOP1C> distortion=LensDistortionOP1C(lensDistortion.centre, lensDistortion.kappa);
		PreprocessFeatureSet(featureList, boundingBox, gridSpacing, flagNormalise, distortion, transformation);
		return;
	}	//if(distortionCode==LensDistortionCodeT::OP1)

	if(lensDistortion.modelCode==LensDistortionCodeT::DIV)
	{
		optional<LensDistortionDivC> distortion=LensDistortionDivC(lensDistortion.centre, lensDistortion.kappa);
		PreprocessFeatureSet(featureList, boundingBox, gridSpacing, flagNormalise, distortion, transformation);
		return;
	}	//if(distortionCode==LensDistortionCodeT::FP1)

	optional<LensDistortionNoDC> distortion=LensDistortionNoDC(lensDistortion.centre, lensDistortion.kappa);
	PreprocessFeatureSet(featureList, boundingBox, gridSpacing, flagNormalise, distortion, transformation);
}	//vector<ImageFeatureC> PreprocessFeatureSet(const string& filename, const optional<tuple<typename ImageFeatureC::coordinate_type, typename ImageFeatureC::coordinate_type> >& boundingBox, double gridSpacing, bool flagNormalise, LensDistortionCodeT distortionCode, const optional<TransformationT>& transformation)

/**
 * @brief Common post-processing operations prior to write
 * @tparam ImageFeatureT An image feature
 * @tparam LensDistortionT A lens distoriton model
 * @tparam TransformationT A transformation
 * @param[in,out] feature Feature to be processed
 * @param[in] lensDistortion Lens distortion to be applied (to undo a previous correction)
 * @param[in] transformation Inverse transformation, to undo a previous normalisation operation
 * @pre \c ImageFeatureT is a feature, and its coordinate type is convertible to \c Coordinate2DT
 * @pre \c TransformationT is an Eigen geometry transformation (unenforced)
 */
template<class ImageFeatureT, class LensDistortionT, class TransformationT>
void BaseImageFeatureIOC::PostprocessFeature(ImageFeatureT& feature, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<ImageFeatureT>));
	static_assert(is_convertible<typename ImageFeatureT::coordinate_type, Coordinate2DT>::value, "BaseImageFeatureIOC::PostprocessFeature: ImageFeatureT::coordinate_type must be convertible to Coordinate2DT" );

	//Coordinate transformations. In reverse order of PreprocessFeatureSet
	 if(transformation)
		 feature.Coordinate()=CoordinateTransformations2DT::TransformCoordinate(feature.Coordinate(), *transformation);

	if(lensDistortion)
	{
		optional<typename ImageFeatureT::coordinate_type> transformed=lensDistortion->Apply(feature.Coordinate());
		if(transformed)
			feature.Coordinate().swap(*transformed);
	}	//if(lensDistortion)
}	//void PostprocessFeature(ImageFeatureC& feature, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation)

/**
 * @brief Common post-processing operations before writing to a file
 * @tparam ImageFeatureT An image feature
 * @tparam LensDistortionT A lens distortion operator
 * @tparam TransformationT A transformation
 * @param[in,out] featureList Feature list to be post-processed
 * @param[in] lensDistortion Lens distortion to be applied (to undo a previous correction)
 * @param[in] transformation Inverse transformation, to undo a previous normalisation operation
 * @remarks This function inverts various common transformations on the initial feature set, to facilitate the comparison with the original input data
 */
template<class ImageFeatureT, class LensDistortionT, class TransformationT>
void BaseImageFeatureIOC::PostprocessFeatureSet(vector<ImageFeatureT>& featureList, const optional<LensDistortionT>& lensDistortion, const optional<TransformationT>& transformation)
{
	for(auto& current : featureList)
		PostprocessFeature(current, lensDistortion, transformation);
}	//void PostprocessFeatureSet(vector<ImageFeatureC>& featureList, const optional<LensDistortionT>& lensDistortion, const optional<MatrixT>& transformation)

/**
 * @brief Common post-processing operations before writing to a file
 * @tparam LensDistortionT A lens distortion operator
 * @tparam TransformationT A transformation
 * @param[in,out] featureList Feature list to be post-processed
 * @param[in] lensDistortion Lens distortion to be applied (to undo a previous correction)
 * @param[in] transformation Inverse transformation, to undo a previous normalisation operation
 * @remarks This function inverts various common transformations on the initial feature set, to facilitate the comparison with the original input data
 * @remarks Untested
 */
template<class ImageFeatureT, class TransformationT>
void BaseImageFeatureIOC::PostprocessFeatureSet(vector<ImageFeatureT>& featureList, const LensDistortionC& lensDistortion, const optional<TransformationT>& transformation )
{
	if(lensDistortion.modelCode==LensDistortionCodeT::FP1)
	{
		optional<LensDistortionFP1C> distortion=LensDistortionFP1C(lensDistortion.centre, lensDistortion.kappa);
		PostprocessFeatureSet(featureList, distortion, transformation);
		return;
	}

	if(lensDistortion.modelCode==LensDistortionCodeT::OP1)
	{
		optional<LensDistortionOP1C> distortion=LensDistortionOP1C(lensDistortion.centre, lensDistortion.kappa);
		PostprocessFeatureSet(featureList, distortion, transformation);
		return;
	}

	if(lensDistortion.modelCode==LensDistortionCodeT::DIV)
	{
		optional<LensDistortionDivC> distortion=LensDistortionDivC(lensDistortion.centre, lensDistortion.kappa);
		PostprocessFeatureSet(featureList, distortion, transformation);
		return;
	}

	optional<LensDistortionNoDC> distortion=LensDistortionNoDC(lensDistortion.centre, lensDistortion.kappa);
	PostprocessFeatureSet(featureList, distortion, transformation);
}	//void PostprocessFeatureSet(vector<ImageFeatureC>& featureList, LensDistortionCodeT distortionCode, const Coordinate2DT& distortionCentre, double distortionCoefficient, const optional<TransformationT>& transformation )

/*********** ImageFeatureIOC **********/

/**
 * @brief Writes a feature set to a file
 * @tparam FeatureRangeT A feature range
 * @param[in] features Features to be exported
 * @param[in] filename Target file
 * @pre \c FeatureRangeT is a forward range of image features
 */
template<class FeatureRangeT>
void ImageFeatureIOC::WriteFeatureFile(const FeatureRangeT& features, const string& filename)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
    BOOST_CONCEPT_ASSERT((is_same<typename range_value<FeatureRangeT>::type, ImageFeatureC>));

    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("ImageFeatureIOC::WriteFeatureFile: Cannot open file at ") + filename));

    //Header

    size_t sRange=boost::distance(features);

    //If empty, return
    if(sRange==0)
        return;

    size_t sDescriptor=boost::begin(features)->Descriptor().size();    //Descriptor size
    file<<sDescriptor<<"\n"<<sRange<<"\n";

    //Write the features
    for(const auto& current : features)
        file<<current;
}   //void WriteFeatureFile(const FeatureRangeT& features, const string& filename);    ///< Writes a feature set to a file

/*********** BinaryImageFeatureIOC **********/
/**
 * @brief Writes a feature set to a file
 * @tparam FeatureRangeT A feature range
 * @param[in] features Features to be exported
 * @param[in] filename Target file
 * @pre \c FeatureRangeT is a forward range of image features
 * @remarks The descriptor size is the number of bits, not the discrete bytes in a row
 */
template<class FeatureRangeT>
void BinaryImageFeatureIOC::WriteFeatureFile(const FeatureRangeT& features, const string& filename)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
    BOOST_CONCEPT_ASSERT((is_same<typename range_value<FeatureRangeT>::type, BinaryImageFeatureC>));

    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("BinaryImageFeatureIOC::WriteFeatureFile: Cannot open file at ") + filename));

    //Header

    size_t sRange=boost::distance(features);

    //If empty, return
    if(sRange==0)
        return;

    size_t sDescriptor=boost::begin(features)->Descriptor().size();    //Descriptor size
    file<<sDescriptor<<"\n"<<sRange<<"\n";

    for(const auto& current : features)
		file<<current;
}   //void WriteFeatureFile(const FeatureRangeT& features, const string& filename);    ///< Writes a feature set to a file

}   //ION
}	//SeeSawN

//Extern templates
extern template class std::vector<std::size_t>;
extern template class boost::tokenizer< boost::char_separator<char> >;
extern template std::string boost::lexical_cast<std::string, int>(const int&);
extern template double boost::lexical_cast<double, std::string>(const std::string&);
extern template class boost::tokenizer< boost::char_separator<char> >;
#endif /* IMAGE_FEATURE_IO_IPP_8421305 */
