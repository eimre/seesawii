/**
 * @file ImageFeatureIO.h Public interface for ImageFeatureIOC
 * @author Evren Imre
 * @date 18 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ImageFeatureIO.ipp"
#include "../Geometry/LensDistortion.h"

#ifndef IMAGE_FEATURE_IO_H_3132632
#define IMAGE_FEATURE_IO_H_3132632

namespace SeeSawN
{
namespace ION
{

class BaseImageFeatureIOC;	///< Base class for image feature IO operations
class ImageFeatureIOC;	///< Image feature input/output operations
class BinaryImageFeatureIOC;	///< Binary image feature input/output operations


/********** EXTERN TEMPLATES **********/
extern template void ImageFeatureIOC::WriteFeatureFile(const vector<ImageFeatureC>& features, const string& filename);

extern template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<ImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
extern template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<ImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

extern template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<ImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
extern template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<ImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

extern template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<BinaryImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
extern template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<BinaryImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

extern template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<BinaryImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
extern template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<BinaryImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

}   //ION
}	//SeeSawN

#endif /* IMAGE_FEATURE_IO_H_3132632 */
