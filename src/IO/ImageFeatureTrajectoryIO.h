/**
 * @file ImageFeatureTrajectoryIO.h Public interface \c ImageFeatureTrajectoryIOC
 * @author Evren Imre
 * @date 22 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_TRAJECTORY_IO_H_4180912
#define IMAGE_FEATURE_TRAJECTORY_IO_H_4180912

#include "ImageFeatureTrajectoryIO.ipp"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace ION
{

template<typename TimeStampT> class ImageFeatureTrajectoryIOC;	///< Image feature trajectory I/O operations

/********** EXTERN TEMPLATES **********/
extern template class ImageFeatureTrajectoryIOC<unsigned int>;

extern template void ImageFeatureTrajectoryIOC<unsigned int>::WriteTrajectoryFile(const vector<ImageFeatureTrajectoryIOC<unsigned int>::trajectory_type >&, const string&);

using SeeSawN::GeometryN::CoordinateTransformations2DT;
extern template void ImageFeatureTrajectoryIOC<unsigned int>::PostprocessMeasurements(vector<ImageFeatureTrajectoryIOC<unsigned int>::trajectory_type >&, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
extern template void ImageFeatureTrajectoryIOC<unsigned int>::PostprocessMeasurements(vector<ImageFeatureTrajectoryIOC<unsigned int>::trajectory_type >&, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

extern template void ImageFeatureTrajectoryIOC<unsigned int>::PreprocessMeasurements(vector<ImageFeatureTrajectoryIOC<unsigned int>::trajectory_type >&, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
extern template void ImageFeatureTrajectoryIOC<unsigned int>::PreprocessMeasurements(vector<ImageFeatureTrajectoryIOC<unsigned int>::trajectory_type >&, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

}	//ION
}	//SeeSawN


#endif /* IMAGE_FEATURE_TRAJECTORY_IO_H_4180912 */
