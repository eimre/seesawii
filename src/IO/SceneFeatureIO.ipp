/**
 * @file SceneFeatureIO.ipp Implementation of the I/O class for 3D scene features
 * @author Evren Imre
 * @date 25 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_FEATURE_IO_IPP_0693214
#define SCENE_FEATURE_IO_IPP_0693214

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <map>
#include <string>
#include <type_traits>
#include <iostream>
#include <stdexcept>
#include <climits>
#include <algorithm>
#include <cstddef>
#include "../Elements/Feature.h"
#include "../Elements/Camera.h"
#include "../Elements/FeatureConcept.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/CoordinateTransformation.h"
#include "ImageFeatureIO.h"

namespace SeeSawN
{
namespace ION
{

using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::optional;
using std::vector;
using std::map;
using std::string;
using std::is_same;
using std::ofstream;
using std::ifstream;
using std::runtime_error;
using std::cout;
using std::numeric_limits;
using std::size_t;
using std::iota;
using std::random_shuffle;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::LensDistortionNoDC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::ION::ImageFeatureIOC;

/**
 * @brief Base class for scene feature I/O operations
 * @remarks Tested via \c SceneFeatureIOC
 * @ingroup IO
 * @nosubgrouping
 */
class SceneFeatureIOBaseC
{
	public:

	   template<class SceneFeatureT> static void PreprocessDescriptors(vector<SceneFeatureT>& featureList, bool flagNormalise=true); ///< Common pre-processing operations after a feature set is read in
	   template<class SceneFeatureT, class TransformationT> static void PostprocessDescriptors(vector<SceneFeatureT>& featureList, const vector<LensDistortionC>& distortionList, const vector< optional<TransformationT> > & transformationList );	///< Common post-processing operations before writing to a file
	   template<class FeatureRangeT> static void WriteFeatureFile(const FeatureRangeT& features, const string& filename);    ///< Writes a feature set to a file
};	//class SceneFeatureIOBaseC

/**
 * @brief Scene feature I/O operations
 * @remarks Format: 3D coordinates, image ids, list of 2D observations
 * @ingroup IO
 * @nosubgrouping
 */
class SceneFeatureIOC : public SceneFeatureIOBaseC
{
	public:

	   static vector<SceneFeatureC> ReadFeatureFile(const string& filename, bool flagRandomise=true);   ///< Reads a feature file

	   /** @name Utilities */ //@{
	   static void WriteSampleFile(const string& filename); ///< Writes a sample file to illustrate the format
	   //@}
};	//class SceneFeatureIOC

/**
 * @brief Oriented binary scene feature I/O operations
 * @remarks Format: 3D coordinates, image ids, list of 2D observations
 * @ingroup IO
 * @nosubgrouping
 */
class OrientedBinarySceneFeatureIOC : public SceneFeatureIOBaseC
{
	public:

	   static vector<OrientedBinarySceneFeatureC> ReadFeatureFile(const string& filename, bool flagRandomise=true);   ///< Reads a feature file

	   /** @name Utilities */ //@{
	   static void WriteSampleFile(const string& filename); ///< Writes a sample file to illustrate the format
	   //@}
};	//class OrientedBinarySceneFeatureIOC

/********** IMPLEMENTATION STARTS HERE **********/

/********** SceneFeatureIOBaseC **********/

/**
 * @brief Common post-processing operations before writing to a file
 * @tparam SceneFeatureT A scene feature
 * @tparam TransformationT A transformation
 * @param[in,out] featureList Feature list
 * @param[in] distortionList Lens distortions
 * @param[in] transformationList Transformations to be applied to the image features
 * @pre For each camera in \c featureList , there is a distortion and transformation (unenforced)
 * @pre \c SceneFeatureT satisfies \c FeatureConceptC
 */
template<class SceneFeatureT, class TransformationT>
void SceneFeatureIOBaseC::PostprocessDescriptors(vector<SceneFeatureT>& featureList, const vector<LensDistortionC>& distortionList, const vector< optional<TransformationT> > & transformationList )
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<SceneFeatureT>));

	for(auto& currentFeature : featureList)
	{
		for(auto& currentObservation : currentFeature.Descriptor())
		{
			size_t index=currentObservation.first;

			if(distortionList[index].modelCode==LensDistortionCodeT::FP1)
			{
				LensDistortionFP1C lensDistortion(distortionList[index].centre, distortionList[index].kappa);
				ImageFeatureIOC::PostprocessFeature(currentObservation.second, optional<LensDistortionFP1C>(lensDistortion), transformationList[index]);
			}	//if(distortionCode[index]==LensDistortionCodeT::FP1)

			if(distortionList[index].modelCode==LensDistortionCodeT::OP1)
			{
				LensDistortionOP1C lensDistortion(distortionList[index].centre, distortionList[index].kappa);
				ImageFeatureIOC::PostprocessFeature(currentObservation.second, optional<LensDistortionOP1C>(lensDistortion), transformationList[index]);
			}	//if(distortionCode[index]==LensDistortionCodeT::OP1)

			if(distortionList[index].modelCode==LensDistortionCodeT::DIV)
			{
				LensDistortionDivC lensDistortion(distortionList[index].centre, distortionList[index].kappa);
				ImageFeatureIOC::PostprocessFeature(currentObservation.second, optional<LensDistortionDivC>(lensDistortion), transformationList[index]);
			}	//if(distortionCode[index]==LensDistortionCodeT::DIV)

			if(distortionList[index].modelCode==LensDistortionCodeT::NOD)
			{
				LensDistortionNoDC lensDistortion(distortionList[index].centre, distortionList[index].kappa);
				ImageFeatureIOC::PostprocessFeature(currentObservation.second, optional<LensDistortionNoDC>(lensDistortion), transformationList[index]);
			}	//if(distortionList[index].modelCode==LensDistortionCodeT::NOD)

		}	//for(auto& currentObservation : currentFeature.Descriptor())
	}	//for(auto& current : featureList)
}	//void PostprocessFeatureSet(vector<SceneFeatureC>& featureList, const vector<LensDistortionCodeT> distortionCode, const vector<Coordinate2DT>& distortionCentre, const vector<double>& distortionCoefficient, const vector< optional<TransformationT> > & transformation )

/**
 * @brief Common pre-processing operations after a feature set is read in
 * @tparam SceneFeatureT A scene feature
 * @param[in,out] featureList Feature list
 * @param[in] flagNormalise \c true if the image feature descriptors are to be normalised
 * @pre \c SceneFeatureT satisfies \c FeatureConceptC
 */
template<class SceneFeatureT>
void SceneFeatureIOBaseC::PreprocessDescriptors(vector<SceneFeatureT>& featureList,  bool flagNormalise)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<SceneFeatureT>));

	for(auto& currentFeature : featureList)
		if(flagNormalise)
			currentFeature.NormaliseDescriptor();
}	//void ProcessFeatureSet(vector<SceneFeatureC>& featureList, const vector<LensDistortionCodeT>& distortionCode, const vector<Coordinate2DT>& distortionCentre, const vector<double>& distortionCoefficient, const vector< optional<TransformationT> > & transformation)

/**
 * @brief Writes a feature set to a file
 * @tparam FeatureRangeT A range of 3D features
 * @param[in] features 3D features
 * @param[in] filename Filename
 * @pre \c FeatureRangeT is a single pass range of elements satisfying \c FeatureConceptC
 */
template<class FeatureRangeT>
void SceneFeatureIOBaseC::WriteFeatureFile(const FeatureRangeT& features, const string& filename)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<FeatureRangeT>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<typename range_value<FeatureRangeT>::type>));

	ofstream file(filename);

	if(file.fail())
		throw(runtime_error(string("SceneFeatureIOC::WriteFeatureFile: Cannot open file at ") + filename));

	//Header

	size_t sRange=boost::distance(features);

	//If empty, return
	if(sRange==0)
		return;

	size_t sDescriptor=boost::begin(features)->Descriptor().begin()->second.Descriptor().size();    //Descriptor size
	size_t sRoI=boost::begin(features)->Descriptor().begin()->second.RoI().size();	//RoI size

	file<<sDescriptor<<" "<<sRoI<<"\n"<<sRange<<"\n";

    //Write the features
    for(const auto& current : features)
        file<<current;
}	//void WriteFeatureFile(const FeatureRangeT& features, const string& filename)

}	//ION
}	//SeeSawN

#endif /* SCENE_FEATURE_IO_IPP_0693214 */
