/**
 * @file CoordinateCorrespondenceIO.cpp Implementation and instantiations of CoordinateCorrespondenceIOC
 * @author Evren Imre
 * @date 22 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CoordinateCorrespondenceIO.h"
namespace SeeSawN
{
namespace ION
{
/**
 * @brief Writes a sample file
 * @param[in] filename Filename
 * @throws runtime_error If the file cannot be opened for writing
 */
void CoordinateCorrespondenceIOC::WriteSampleFile(const string& filename)
{
    cout<<"cXY file format\n";
    cout<<"#Correspondences\n";
    cout<<"Coordinates of the first point (X); Coordinates of the second point(Y); Similarity; Ambiguity\n";
    cout<<"Example: An image correspondence\n";

    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("ImageCorrespondenceIOC::WriteSampleFile: ")+filename+( "cannot be opened for writing.")));

    file<<"2\n";
    file<<"101 95 234 56 1.2 0.75 \n";
    file<<"11 5 24 6 0.2 0.9 \n";
}   //void WriteSampleFile(const string& filename)

/********** EXPLICIT INSTANTIATIONS **********/
template void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CoordinateCorrespondenceList2DT&, const string&);
template void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CoordinateCorrespondenceList3DT&, const string&);
template void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CoordinateCorrespondenceList32DT&, const string&);

}   //ION
}	//SeeSawN


