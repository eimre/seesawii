/**
 * @file ImageFeatureTrajectoryIO.ipp Implementation of \c ImageFeatureTrajectoryIOC
 * @author Evren Imre
 * @date 22 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_TRAJECTORY_IO_IPP_6091018
#define IMAGE_FEATURE_TRAJECTORY_IO_IPP_6091018

#include <boost/optional.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <type_traits>
#include "../Elements/Feature.h"
#include "../Elements/Camera.h"
#include "../Geometry/LensDistortion.h"
#include "ImageFeatureIO.h"

namespace SeeSawN
{
namespace ION
{

using boost::optional;
using boost::range_value;
using boost::SinglePassRangeConcept;
using std::vector;
using std::string;
using std::ofstream;
using std::ifstream;
using std::runtime_error;
using std::cout;
using std::is_same;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::FeatureTrajectoryT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::LensDistortionNoDC;
using SeeSawN::ION::ImageFeatureIOC;

/**
 * @brief Image feature trajectory I/O operations
 * @tparam TimeStampT Time stamp type
 * @ingroup IO
 * @nosubgrouping
 */
template<typename TimeStampT=unsigned int>
class ImageFeatureTrajectoryIOC
{
	private:

		typedef FeatureTrajectoryT<TimeStampT, ImageFeatureC> TrajectoryT;	///< A trajectory

	public:

		typedef TrajectoryT trajectory_type;	///< A trajectory

	    static vector<TrajectoryT> ReadTrajectoryFile(const string& filename);   ///< Reads a trajectory set
	    template<class TrajectoryRangeT> static void WriteTrajectoryFile(const TrajectoryRangeT& trajectories, const string& filename);    ///< Writes a trajectory set to a file

	    template<class TransformationT> static void PreprocessMeasurements(vector<TrajectoryT>& trajectoryList, bool flagNormalise, const LensDistortionC& distortion, const optional<TransformationT>& transformation); ///< Common pre-processing operations after a trajectory set is read in
	    template<class TransformationT> static void PostprocessMeasurements(vector<TrajectoryT>& trajectoryList, const LensDistortionC& distortion, const optional<TransformationT> & transformation );	///< Common post-processing operations before writing to a file

	    /** @name Utilities */ //@{
	    static void WriteSampleFile(const string& filename); ///< Writes a sample file to illustrate the format
	   //@}
};	//class SceneFeatureIOC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Writes a sample file to illustrate the format
 * @param[in] filename Filename for the sample file
 * @throws runtime_error If the file cannot be opened
 */
template<typename TimeStampT>
void ImageFeatureTrajectoryIOC<TimeStampT>::WriteSampleFile(const string& filename)
{
	ofstream file(filename);

	if(file.fail())
		throw(runtime_error(string("ImageFeatureTrajectoryIOC::WriteSampleFile: Cannot open file at ") + filename));

	cout<<".trj file format:"<<"\n";
	cout<<"Image descriptor size "<<"Image RoI size "<<"\n"<<"Number of trajectories"<<"\n";
	cout<<"Number of observations(1)"<<"\n";
	cout<<"Image Id(1)"<<" "<<"Coordinates(2)"<<" "<<"RoI(3)"<<" "<<"Descriptor(5)"<<"\n";

	file<<5<<" "<<3<<"\n";
	file<<2<<"\n";

	file<<1<<"\n";
	file<<"0 "<<"100 120 "<<"0.1 0.25 0.3 "<<" 1 4 6 1 2"<<"\n";

	file<<3<<"\n";
	file<<"0 "<<"125 120 "<<"0.1 0.25 0.3 "<<" 1 4 6 1 2"<<"\n";
	file<<"1 "<<"127 120 "<<"0.15 0.25 0.21 "<<" 1 2 3 1 6"<<"\n";
	file<<"2 "<<"130 112 "<<"0.1 0.28 0.4 "<<" 2 0 1 3 5"<<"\n";
}	//void WriteSampleFile(const string& filename)

/**
 * @brief Reads a trajectory set
 * @param[in] filename Filename for the input file
 * @return A vector of trajectories
 */
template<typename TimeStampT>
auto ImageFeatureTrajectoryIOC<TimeStampT>::ReadTrajectoryFile(const string& filename) -> vector<TrajectoryT>
{
	ifstream file(filename);

	if(file.fail())
		throw(runtime_error(string("ImageFeatureTrajectoryIOC::ReadTrajectoryFile: Cannot open file at ") + filename));

	int sDescriptor; file>>sDescriptor;

	if(sDescriptor<0)
		throw(runtime_error(string("ImageFeatureTrajectoryIOC::ReadFeatureFile: Descriptor size must be non-negative, not ") + lexical_cast<string>(sDescriptor)));

	int sRoI; file>>sRoI;

	if(sRoI<0)
		throw(runtime_error(string("ImageFeatureTrajectoryIOC::ReadFeatureFile: RoI size must be non-negative, not ") + lexical_cast<string>(sRoI)));

	int nTracks; file>>nTracks;

	if(nTracks<0)
		throw(runtime_error(string("ImageFeatureTrajectoryIOC::ReadFeatureFile: Number of features must be non-negative, not ") + lexical_cast<string>(nTracks)));

	//Tracks
	vector<TrajectoryT> output(nTracks);
	if(nTracks==0)
		return output;

	size_t c=0;
	for(auto& current : output)
	{
		int nMeasurements;
		file>>nMeasurements;

		for(int c2=0; c2<nMeasurements; ++c2)
		{
			ImageFeatureC::coordinate_type coordinate;
			ImageFeatureC::roi_type imageRoI(sRoI);
			ImageFeatureC::descriptor_type imageDescriptor(sDescriptor);

			size_t timeStamp;
			file>>timeStamp;

			file>>coordinate[0];
			file>>coordinate[1];

			for(int cRoI=0; cRoI<sRoI; ++cRoI)
				file>>imageRoI[cRoI];

			for(int cDesc=0; cDesc<sDescriptor; ++cDesc)
				file>>imageDescriptor[cDesc];

			current.emplace(timeStamp, ImageFeatureC(coordinate, imageDescriptor, imageRoI) );
		}	//for(int c2=0; c2<nObservations; ++c2)

		++c;
	}	//for(auto& current : output)

	return output;
}	//vector<TrajectoryT> ReadTrajectoryFile(const string& filename)

/**
 * @brief Writes a feature set to a file
 * @tparam TrajectoryRangeT A range of trajectories
 * @param[in] trajectories Trajectories
 * @param[in] filename Filename
 * @pre \c TrajectoryRangeT is a single pass range of elements of type \c TrajectoryT
 */
template<typename TimeStampT>
template<class TrajectoryRangeT>
void ImageFeatureTrajectoryIOC<TimeStampT>::WriteTrajectoryFile(const TrajectoryRangeT& trajectories, const string& filename)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<TrajectoryRangeT>));
	static_assert(is_same< typename range_value<TrajectoryRangeT>::type, TrajectoryT >::value, "ImageFeatureTrajectoryIOC::WriteFeatureFile : TrajectoryRangeT must have elements of type TrajectoryT");

	ofstream file(filename);

	if(file.fail())
		throw(runtime_error(string("ImageFeatureTrajectoryIOC::WriteTrajectoryFile: Cannot open file at ") + filename));

	//Header

	size_t sRange=boost::distance(trajectories);

	//If empty, return
	if(sRange==0)
		return;

	size_t sDescriptor=boost::begin(trajectories)->begin()->second.Descriptor().size();    //Descriptor size
	size_t sRoI=boost::begin(trajectories)->begin()->second.RoI().size();	//RoI size

	file<<sDescriptor<<" "<<sRoI<<"\n"<<sRange<<"\n";

    //Write the trajectories
    for(const auto& current : trajectories)
    	file<<current;
}	//void WriteFeatureFile(const FeatureRangeT& features, const string& filename)

/**
 * @brief Common pre-processing operations after a trajectory set is read in
 * @param[in,out] trajectoryList Trajectory list
 * @param[in] flagNormalise If \c true , the descriptor vectors are normalised to unit norm
 * @param[in] distortion Lens distortion
 * @param[in] transformation Transformation to be applied to the image features
 */
template<typename TimeStampT>
template<class TransformationT>
void ImageFeatureTrajectoryIOC<TimeStampT>::PreprocessMeasurements(vector<TrajectoryT>& trajectoryList, bool flagNormalise, const LensDistortionC& distortion, const optional<TransformationT>& transformation)
{
	for(auto& currentTrajectory : trajectoryList )
		for(auto& currentMeasurement : currentTrajectory)
		{
			if(distortion.modelCode==LensDistortionCodeT::FP1)
			{
				LensDistortionFP1C lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PreprocessFeature(currentMeasurement.second, flagNormalise, optional<LensDistortionFP1C>(lensDistortion), transformation);
			}	//if(distortionCode[index]==LensDistortionCodeT::FP1)

			if(distortion.modelCode==LensDistortionCodeT::OP1)
			{
				LensDistortionOP1C lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PreprocessFeature(currentMeasurement.second, flagNormalise, optional<LensDistortionOP1C>(lensDistortion), transformation);
			}	//if(distortionCode[index]==LensDistortionCodeT::OP1)

			if(distortion.modelCode==LensDistortionCodeT::DIV)
			{
				LensDistortionDivC lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PreprocessFeature(currentMeasurement.second, flagNormalise, optional<LensDistortionDivC>(lensDistortion), transformation);
			}	//if(distortionCode[index]==LensDistortionCodeT::DIV)

			if(distortion.modelCode==LensDistortionCodeT::NOD)
			{
				LensDistortionNoDC lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PreprocessFeature(currentMeasurement.second, flagNormalise, optional<LensDistortionNoDC>(lensDistortion), transformation);
			}	//if(distortionList[index].modelCode==LensDistortionCodeT::NOD)
		}	//for(auto& current : featureList)

}	//void PreprocessMeasurements(vector<TrajectoryT>& trajectoryList, bool flagNormalise, const LensDistortionC& lensDistortion, const optional<TransformationT>& transformation)

/**
 * @brief Common post-processing operations before writing to a file
 * @param[in,out] trajectoryList Trajectory list
 * @param[in] distortion Lens distortion
 * @param[in] transformation Transformation to be applied to the image features
 */
template<typename TimeStampT>
template<class TransformationT>
void ImageFeatureTrajectoryIOC<TimeStampT>::PostprocessMeasurements(vector<TrajectoryT>& trajectoryList, const LensDistortionC& distortion, const optional<TransformationT> & transformation )
{
	for(auto& currentTrajectory : trajectoryList )
		for(auto& currentMeasurement : currentTrajectory)
		{
			if(distortion.modelCode==LensDistortionCodeT::FP1)
			{
				LensDistortionFP1C lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PostprocessFeature(currentMeasurement.second, optional<LensDistortionFP1C>(lensDistortion), transformation);
			}	//if(distortionCode[index]==LensDistortionCodeT::FP1)

			if(distortion.modelCode==LensDistortionCodeT::OP1)
			{
				LensDistortionOP1C lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PostprocessFeature(currentMeasurement.second, optional<LensDistortionOP1C>(lensDistortion), transformation);
			}	//if(distortionCode[index]==LensDistortionCodeT::OP1)

			if(distortion.modelCode==LensDistortionCodeT::DIV)
			{
				LensDistortionDivC lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PostprocessFeature(currentMeasurement.second, optional<LensDistortionDivC>(lensDistortion), transformation);
			}	//if(distortionCode[index]==LensDistortionCodeT::DIV)

			if(distortion.modelCode==LensDistortionCodeT::NOD)
			{
				LensDistortionNoDC lensDistortion(distortion.centre, distortion.kappa);
				ImageFeatureIOC::PostprocessFeature(currentMeasurement.second, optional<LensDistortionNoDC>(lensDistortion), transformation);
			}	//if(distortionList[index].modelCode==LensDistortionCodeT::NOD)
		}	//for(auto& current : featureList)
}	//void PostprocessMeasurements(vector<SceneFeatureC>& featureList, const vector<LensDistortionCodeT> distortionCode, const vector<Coordinate2DT>& distortionCentre, const vector<double>& distortionCoefficient, const vector< optional<TransformationT> > & transformation )


}	//ION
}	//SeeSawN

#endif /* IMAGE_FEATURE_TRAJECTORY_IO_IPP_6091018 */
