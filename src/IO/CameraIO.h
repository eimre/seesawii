/**
 * @file CameraIO.h Public interface for CameraIOC
 * @author Evren Imre
 * @date 7 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CAMERAIO_H_0921321
#define CAMERAIO_H_0921321

#include "CameraIO.ipp"
namespace SeeSawN
{
namespace ION
{

class CameraIOC;

/********** EXTERN TEMPLATES **********/
extern template void CameraIOC::WriteCameraFile(const vector<CameraC>&, const string&, const string&, bool);
extern template void CameraIOC::WriteUniSFile(const vector<CameraC>&, const string&);

}	//ION
}	//SeeSawN

/********** EXTERN TEMPLATES **********/

extern template double boost::lexical_cast<double, std::string>(const std::string&);
extern template float boost::lexical_cast<float, std::string>(const std::string&);
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template std::string boost::lexical_cast<std::string, float>(const float&);


#endif /* CAMERAIO_H_0921321 */
