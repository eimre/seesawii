/**
 * @file SpecialArrayIO.cpp Implementation of a collection of dedicated array parsers
 * @author Evren Imre
 * @date 2 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SpecialArrayIO.h"
namespace SeeSawN
{
namespace ION
{

/********** EXPLICIT INSTANTIATIONS **********/
template void WriteCovFile(const string&, const vector<Coordinate2DT>&, const vector<Matrix<ValueTypeM<Coordinate2DT>::type,2,2>>& );
template void WriteCovFile(const string&, const vector<Coordinate3DT>&, const vector<Matrix<ValueTypeM<Coordinate3DT>::type,3,3>>& );
//template tuple<vector<Coordinate2DT>, vector<Matrix<ValueTypeM<Coordinate2DT>::type,2,2> > > ReadCovFile<ValueTypeM<Coordinate2DT>::type,2>(const string&);
//template tuple<vector<Coordinate3DT>, vector<Matrix<ValueTypeM<Coordinate3DT>::type,3,3> > > ReadCovFile<ValueTypeM<Coordinate3DT>::type,3>(const string&);

}	//ION
}	//SeeSawN

