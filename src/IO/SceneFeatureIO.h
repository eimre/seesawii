/**
 * @file SceneFeatureIO.h Public interface for the I/O class for 3D scene features
 * @author Evren Imre
 * @date 25 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_FEATURE_IO_H_9021231
#define SCENE_FEATURE_IO_H_9021231

#include "SceneFeatureIO.ipp"
namespace SeeSawN
{
namespace ION
{

class SceneFeatureIOBaseC;	///< Base class for scene feature I/O operations
class SceneFeatureIOC;	///< Scene feature I/O operations
class OrientedBinarySceneFeatureIOC;	///< Oriented binary scene feature I/O operations

}	//ION
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template class std::vector<double>;
extern template std::string boost::lexical_cast<std::string, int>(const int&);
extern template double boost::lexical_cast<double, std::string>(const std::string&);

#endif /* SCENE_FEATURE_IO_H_9021231 */
