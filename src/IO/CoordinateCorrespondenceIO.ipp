/**
 * @file CoordinateCorrespondenceIO.ipp Implementation of CoordinateCorrespondenceIOC
 * @author Evren Imre
 * @date 22 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_CORRESPONDENCE_IO_IPP_8125623
#define IMAGE_CORRESPONDENCE_IO_IPP_8125623

#include <fstream>
#include <string>
#include <stdexcept>
#include <iostream>
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace ION
{

using std::runtime_error;
using std::string;
using std::ofstream;
using std::cout;
using SeeSawN::WrappersN::RowsM;

/**
 * @brief I/O operations for correspondence correspondences
 * @ingroup IO
 */
class CoordinateCorrespondenceIOC
{
	public:

    	static void WriteSampleFile(const string& filename);    ///< Writes a sample file
        template<class CorrespondenceContainerT> static void WriteCorrespondenceFile(const CorrespondenceContainerT& correspondences, const string& filename);  ///< Writes a correspondence range to a file
};	//class CoordinateCorrespondenceIOC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Writes a correspondence range to a file
 * @tparam CorrespondenceContainerT A container of corresponding coordinates
 * @param[in] correspondences Correspondences
 * @param[in] filename Filename
 * @pre \c CorrespondenceContainerT is one of the coordinate correspondence lists in Correspondence.h (unenforced)
 * @throws runtime_error If the file cannot be opened for writing
 */
template<class CorrespondenceContainerT>
void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CorrespondenceContainerT& correspondences, const string& filename)
{
    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("CoordinateCorrespondenceIOC::WriteCorrespondenceFile: ")+filename+( "cannot be opened for writing.")));

    //Number of elements in the list
    file<<correspondences.size();
    file<<"\n";

    constexpr int DIM1=RowsM<typename CorrespondenceContainerT::left_key_type>::value;
    constexpr int DIM2=RowsM<typename CorrespondenceContainerT::right_key_type>::value;

    //Iterate over the correspondences
    for(const auto& current : correspondences)
    {
    	for(size_t c=0; c<DIM1; ++c)
    		file<<current.left[c]<<" ";

    	for(size_t c=0; c<DIM2; ++c)
			file<<current.right[c]<<" ";

    	file<<current.info.Similarity()<<" "<<current.info.Ambiguity()<<"\n";
    }   //for(const auto& current : correspondences.by<SeeSawN::ElementsN::set1> )
}   //void WriteCorrespondenceFile(const CorrespondenceContainerT& correspondences, const string& filename)

}   //ION
}	//SeeSawN

#endif /* IMAGE_CORRESPONDENCE_IO_IPP_8125623 */
