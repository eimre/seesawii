/**
 * @file ArrayIO.ipp Implementation of ArrayIOC
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ARRAY_IO_IPP_5123654
#define ARRAY_IO_IPP_5123654

#include <boost/lexical_cast.hpp>
#include <string>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <iterator>
#include <list>
#include <vector>
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace ION
{

using boost::lexical_cast;
using std::string;
using std::size_t;
using std::ifstream;
using std::ofstream;
using std::ostream;
using std::stringstream;
using std::runtime_error;
using std::list;
using std::vector;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief I/O operations for Eigen dense arrays
 * @tparam ArrayT An array
 * @pre \c ArrayT is an Eigen dense array (unenforced)
 * @ingroup IO
 * @nosubgrouping
 */
template<class ArrayT>
class ArrayIOC
{
    public:
		static ArrayT ReadArray(const string& filename);	///< Reads a file into an array
        static ArrayT ReadArray(const string& filename, size_t nRows, size_t nCols);   ///< Reads a file into an array
        static void WriteArray(const string& filename, const ArrayT& array);   ///< Writes an array into a file
        static void WriteArray(ostream& output, const ArrayT& array);	///< Writes an array to a stream
};  //class ArrayIOC

/********** IMPLEMENTATION DETAILS **********/

/**
 * @brief Reads a file into an array
 * @param[in] filename Filename
 * @param[in] nRows Number of rows
 * @param[in] nCols Number of columns
 * @return An array with the contents of the file
 * @throws runtime_error If cannot read from file
 * @remarks The function assumes row-major organisation in the file
 */
template<class ArrayT>
ArrayT ArrayIOC<ArrayT>::ReadArray(const string& filename, size_t nRows, size_t nCols)
{
    ifstream file(filename);

    if(file.fail())
        throw(runtime_error(string("ArrayIOC::ReadArray : Cannot read from file ")+filename));

    ArrayT output(nRows, nCols);
    for(size_t c1=0; c1<nRows; ++c1)
        for(size_t c2=0; c2<nCols; ++c2)
            file>>output(c1, c2);

    return output;
}   //ArrayT ReadArray(const string& filename, size_t nRows, size_t nCols)

/**
 * @brief Reads a file into an array
 * @param[in] filename Filename
 * @return An array with the contents of the file
 * @throws runtime_error If cannot read from file
 * @throws runtime_error If array cannot be resized to the correct size
 * @remarks The function assumes row-major organisation in the file
 * @remarks Rows with mismatching number of columns are skipped
 */
template<class ArrayT>
ArrayT ArrayIOC<ArrayT>::ReadArray(const string& filename)
{
    ifstream file(filename);

    if(file.fail())
        throw(runtime_error(string("ArrayIOC::ReadArray : Cannot read from file ")+filename));

    //Read the first line to get the number of columns
	stringstream firstLine;
	file.get(*firstLine.rdbuf());    //Read until \n

	typedef typename ValueTypeM<ArrayT>::type ValueT;
	vector<ValueT> firstLineVector=Tokenise<ValueT>(firstLine.str());
	size_t nCol=firstLineVector.size();

	//Since number of rows are unknown, read into a temporary
	list<vector<ValueT> > buffer;
	buffer.push_back(firstLineVector);

	while(!file.eof())
	{
		vector<ValueT> row; row.reserve(nCol);
		size_t c=0;
		for(; c<nCol && !file.eof(); ++c)
		{
			ValueT temp;
			file>>temp;
			row.push_back(temp);
		}

		if(row.size()==nCol)	//Guard against lines with only a carriage return
			buffer.push_back(row);
	}	//while(!file.eof())

	size_t nRows=buffer.size();
	ArrayT output; output.resize(nRows, nCol);
	if((size_t)output.rows()!=nRows || (size_t)output.cols()!=nCol)
		throw(runtime_error(string("ArrayIOC::ReadArray : Array has fixed dimensions. Cannot resize.")));

	size_t cRow=0;
	for(const auto& current : buffer)
	{
		for(size_t cCol=0; cCol<nCol; ++cCol)
			output(cRow, cCol)=current[cCol];

		++cRow;
	}	//for(const auto& current : buffer)

    return output;
}   //ArrayT ReadArray(const string& filename, size_t nRows, size_t nCols)


/**
 * @brief Writes an array into a file
 * @param[in] filename Filename
 * @param[in] array Array
 * @throws runtime_error If cannot open file for writing
 */
template<class ArrayT>
void ArrayIOC<ArrayT>::WriteArray(const string& filename, const ArrayT& array)
{
    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("ArrayIOC::ReadArray : Cannot write to file ")+filename));

    WriteArray(file, array);
}   //void WriteArray(const string& filename, const ArrayT& array)

/**
 * @brief Writes an array to a stream
 * @param[in] output Output stream
 * @param[in] array Array
 */
template<class ArrayT>
void ArrayIOC<ArrayT>::WriteArray(ostream& output, const ArrayT& array)
{
	size_t nRows=array.rows();
	size_t nCols=array.cols();
	for(size_t c1=0; c1<nRows; ++c1, output<<"\n")
		for(size_t c2=0; c2<nCols; ++c2)
			output<<array(c1,c2)<<" ";

}	//void WriteArray(ostream& output, const ArrayT& array)

}   //ION
}	//SeeSawN

#endif /* ARRAY_IO_IPP_5123654 */
