/**
 * @file SpecialArrayIO.h Public interface for a collection of dedicated array parsers
 * @author Evren Imre
 * @date 2 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SPECIAL_ARRAY_IO_H_4128090
#define SPECIAL_ARRAY_IO_H_4128090

#include "SpecialArrayIO.ipp"
namespace SeeSawN
{
namespace ION
{

template<class MeanRangeT, class CovarianceRangeT> void WriteCovFile(const string& filename, const MeanRangeT& meanRange, const CovarianceRangeT& covarianceRange);	///< Writes a range of means and covariance matrices to a cov file
template<typename ValueT, int DIM> tuple<vector<CoordinateVectorT<ValueT, DIM> >, vector<Matrix<ValueT,DIM,DIM> > > ReadCovFile(const string& filename);	///< Reads a cov file

/********** EXTERN TEMPLATES **********/
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
extern template void WriteCovFile(const string&, const vector<Coordinate2DT>&, const vector<Matrix<ValueTypeM<Coordinate2DT>::type,2,2>>& );
extern template void WriteCovFile(const string&, const vector<Coordinate3DT>&, const vector<Matrix<ValueTypeM<Coordinate3DT>::type,3,3>>& );
//extern template tuple<vector<Coordinate2DT>, vector<Matrix<ValueTypeM<Coordinate2DT>::type,2,2> > > ReadCovFile<ValueTypeM<Coordinate2DT>::type,2>(const string&);
//extern template tuple<vector<Coordinate3DT>, vector<Matrix<ValueTypeM<Coordinate3DT>::type,3,3> > > ReadCovFile<ValueTypeM<Coordinate3DT>::type,3>(const string&);

}	//ION
}	//SeeSawN

#endif /* SPECIALARRAYIO_H_ */
