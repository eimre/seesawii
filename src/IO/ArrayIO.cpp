/**
 * @file ArrayIO.cpp Instantiations for ArrayIOC
 * @author Evren Imre
 * @date 28 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ArrayIO.h"

namespace SeeSawN
{
namespace ION
{

/********** EXPLICIT INSTANTIATIONS **********/
template class ArrayIOC<MatrixXd>;
template class ArrayIOC<Matrix3d>;
template class ArrayIOC<VectorXd>;
template class ArrayIOC<Vector3d>;
template class ArrayIOC<MatrixXf>;
template class ArrayIOC<Matrix3f>;
template class ArrayIOC<VectorXf>;
template class ArrayIOC<Vector3f>;
}	//ION
}	//SeeSawN

