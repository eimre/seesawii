/**
 * @file CameraIO.ipp Implementation of CameraIOC
 * @author Evren Imre
 * @date 7 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CAMERA_IO_IPP_8901432
#define CAMERA_IO_IPP_8901432

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <string>
#include <vector>
#include <locale>
#include <type_traits>
#include <iostream>
#include <fstream>
#include <map>
#include <stdexcept>
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Geometry/LensDistortion.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/EigenExtensions.h"
#include "ArrayIO.h"

namespace SeeSawN
{
namespace ION
{

using boost::property_tree::ptree;
using boost::property_tree::write_xml;
using boost::property_tree::read_xml;
using boost::property_tree::xml_writer_settings;
using boost::ForwardRangeConcept;
using boost::range_value;
using boost::lexical_cast;
using boost::optional;
using Eigen::Matrix;
using std::vector;
using std::string;
using std::locale;
using std::is_same;
using std::ostringstream;
using std::map;
using std::ifstream;
using std::ofstream;
using std::runtime_error;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::TranslationToPosition;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::ION::ArrayIOC;

/**
 * @brief I/O operations for cameras
 * @remarks A camera file is denoted by the extension .cam
 * @ingroup IO
 * @nosubgrouping
 */
class CameraIOC
{
    public:

        static void WriteSampleFile(const string& filename);    ///< Writes a sample file

        template<class CameraRangeT> static void WriteCameraFile(const CameraRangeT& cameras, const string& filename, const string& tag=string(""), bool flagQuaternion=true);  ///< Writes a camera range to a file
        static vector<CameraC> ReadCameraFile(const string& filename);	///< Reads a camera file

        template<class CameraRangeT> static void WriteUniSFile(const CameraRangeT& cameras, const string& filename);	///< Writes a camera range to a UniS calibration file
        static vector<CameraC> ReadUniSFile(const string& filename);	///< Reads a UniS calibration file
};  //class CameraIOC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Writes a camera range to a file
 * @tparam CameraRangeT A range of cameras
 * @param[in] cameras Cameras to be written to a file
 * @param[in] filename Filename
 * @param[in] tag Tag describing the file
 * @param[in] flagQuaternion If \c true , orientation is saved as a quaternion. Otherwise, as a rotation matrix
 * @pre \c CameraRangeT is a forward range holding elements of type CameraC
 */
template<class CameraRangeT>
void CameraIOC::WriteCameraFile(const CameraRangeT& cameras, const string& filename, const string& tag, bool flagQuaternion)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CameraRangeT>));
	static_assert(is_same<CameraC, typename range_value<CameraRangeT>::type >::value, "CameraIOC::WriteCameraFile : CameraRangeT must hold elements of type CameraC");

	map<LensDistortionCodeT, string> codeToString{ {LensDistortionCodeT::NOD,"NOD"}, {LensDistortionCodeT::DIV, "DIV"}, {LensDistortionCodeT::FP1, "FP1"}, {LensDistortionCodeT::OP1,"OP1"} };
	ptree tree;

	if(!tag.empty())
		tree.put("xmlcomment", tag);

	for(const auto& current : cameras)
	{
		ptree& node=tree.add("Camera","");

		if(!current.Tag().empty())
			node.put("Tag", current.Tag());

		if(current.Extrinsics())
		{
			if(current.Extrinsics()->position)
			{
				ostringstream posStream;
				ArrayIOC<Matrix<typename CameraC::real_type, 1,3>  >::WriteArray(posStream, current.Extrinsics()->position->transpose());

				string posString=posStream.str(); posString.back()=' ';
				node.put("Pose.Position", posString );
			}	//if(current.Extrinsics()->position)

			if(current.Extrinsics()->orientation)
			{
				ostringstream rotStream;

				if(flagQuaternion)
					rotStream<< current.Extrinsics()->orientation->w() <<" "<< current.Extrinsics()->orientation->x()<<" "<< current.Extrinsics()->orientation->y() <<" "<< current.Extrinsics()->orientation->z()<<" ";
				else
				{
					typedef Matrix<typename CameraC::real_type, 1,9> ColT;
					ColT vR=Reshape<ColT>(current.Extrinsics()->orientation->matrix(), 1, 9);
					ArrayIOC<ColT>::WriteArray(rotStream, vR);
				}	//if(flagQuaternion)

				string rotString=rotStream.str(); rotString.back()=' ';
				node.put("Pose.Orientation", rotString);
			}	//if(current.Extrinsics()->orientation)
		}	//if(current.Extrinsics())

		if(current.Intrinsics())
		{
			if(current.Intrinsics()->focalLength)
				node.put("Intrinsics.FocalLength", lexical_cast<string>(*current.Intrinsics()->focalLength));

			node.put("Intrinsics.AspectRatio", lexical_cast<string>(current.Intrinsics()->aspectRatio));
			node.put("Intrinsics.Skewness", lexical_cast<string>(current.Intrinsics()->skewness));

			if(current.Intrinsics()->principalPoint)
			{
				ostringstream ppStream;
				ppStream<<current.Intrinsics()->principalPoint->transpose();
				node.put("Intrinsics.PrincipalPoint", ppStream.str());
			}	//if(current.Intrinsics()->principalPoint)
		}	//if(current.Intrinsics())

		if(current.Distortion())
		{
			node.put("LensDistortion.Coefficient", lexical_cast<string>(current.Distortion()->kappa));

			ostringstream dcStream;
			dcStream<<current.Distortion()->centre.transpose();
			node.put("LensDistortion.Centre", dcStream.str());

			node.put("LensDistortion.Model", codeToString[current.Distortion()->modelCode]);
		}	//if(current.Distortion())

		if(current.FNumber())
			node.put("Intrinsics.FNumber", *current.FNumber());

		if(current.PixelSize())
			node.put("Intrinsics.PixelSize", *current.PixelSize());

		if(current.FrameBox())
		{
			ostringstream ulStream;
			ulStream<<current.FrameBox()->corner(FrameT::BottomLeft).transpose();
			node.put("FrameBox.UpperLeft", ulStream.str());

			ostringstream lrStream;
			lrStream<<current.FrameBox()->corner(FrameT::TopRight).transpose();
			node.put("FrameBox.LowerRight", lrStream.str());
		}	//if(current.FrameBox())
	}	//for(const auto& current : cameras)

	xml_writer_settings<typename ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void WriteCameraFile(const CameraRangeT& cameras, const string& filename)

/**
 * @brief Writes a camera range to a UniS calibration file
 * @tparam CameraRange A range of cameras
 * @param[in] cameras Cameras to be written to a file
 * @param[in] filename Filename
 * @pre \c CameraRangeT is a forward range holding elements of type CameraC
 */
template<class CameraRangeT>
void CameraIOC::WriteUniSFile(const CameraRangeT& cameras, const string& filename)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CameraRangeT>));
	static_assert(is_same<CameraC, typename range_value<CameraRangeT>::type >::value, "CameraIOC::WriteUniSFile : CameraRangeT must hold elements of type CameraC");

	ofstream output;
	output.open(filename.data());

	if(output.fail())
		throw(runtime_error(string("WriteUniSFile: Cannot open file ") +filename ) );

	size_t nCameras=boost::distance(cameras);

	output<<nCameras<<" "<<1<<"\n";

	output.setf(std::ios::fixed, std::ios::floatfield);
	for(const auto& current : cameras)
	{
		//Unless a camera has all the necessary components, skip
		if(!current.Extrinsics() || !current.Intrinsics() || !current.FrameBox())
			continue;

		Coordinate2DT ul=current.FrameBox()->corner(FrameT::BottomLeft);
		Coordinate2DT lr=current.FrameBox()->corner(FrameT::TopRight);
		output<<(int)ul(1)<<" "<<(int)lr(1)<<" "<<(int)ul(0)<<" "<<(int)lr(0)<<"\n";

		IntrinsicCalibrationMatrixT mK=*current.MakeIntrinsicCalibrationMatrix();
		output<<mK(0,0)<<" "<<mK(1,1)<<" "<<mK(0,2)<<" "<<mK(1,2)<<"\n";

		if(!current.Distortion() || current.Distortion()->modelCode!=LensDistortionCodeT::FP1)
			output<<0<<"\n";
		else
			output<<current.Distortion()->kappa<<"\n";

		RotationMatrix3DT mR=*current.MakeRotationMatrix();
		ArrayIOC<RotationMatrix3DT>::WriteArray(output, mR);

		Coordinate3DT translation= -mR * (*current.Extrinsics()->position);
		output<<translation(0)<<" "<<translation(1)<<" "<<translation(2)<<"\n\n";
	}	//for(const auto& current : cameras)
}	//WriteUniSFilee(const CameraRangeT& cameras, const string& filename)
}	//ION
}	//SeeSawN

#endif /* CAMERA_IO_IPP_8901432 */
