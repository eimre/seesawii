/**
 * @file SpecialArrayIO.ipp Implementation of a collection of dedicated array parsers
 * @author Evren Imre
 * @date 2 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SPECIAL_ARRAY_IO_IPP_5389031
#define SPECIAL_ARRAY_IO_IPP_5389031

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/tuple/tuple.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include <string>
#include <cstddef>
#include <iterator>
#include "ArrayIO.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace ION
{

using boost::range_value;
using boost::range_const_iterator;
using boost::ForwardRangeConcept;
using boost::make_zip_iterator;
using boost::zip_iterator;
using Eigen::Array;
using Eigen::Matrix;
using std::advance;
using std::vector;
using std::tuple;
using std::get;
using std::string;
using std::size_t;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ColsM;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::ElementsN::CoordinateVectorT;

/**
 * @brief  Writes a range of means and covariance matrices to a cov file
 * @tparam MeanRangeT A range of means
 * @tparam CovarianceRangeT A range of covariances
 * @param[in] filename Filename
 * @param[in] meanRange Means
 * @param[in] covarianceRange Covariances
 * @pre \c MeanRangeT and \c CovarianceRangeT are forward ranges
 * @pre \c MeanRangeT is a range of fixed-size Nx1 Eigen dense objects (unenforced)
 * @pre \c CovarianceRangeT is a range of fixed-size NXN Eigen dense objects (unenforced)
 * @pre \c meanRange and \c covarianceRange have the same number of elements
 * @ingroup IO
 */
template<class MeanRangeT, class CovarianceRangeT>
void WriteCovFile(const string& filename, const MeanRangeT& meanRange, const CovarianceRangeT& covarianceRange)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<MeanRangeT>));
	static_assert( RowsM<typename range_value<MeanRangeT>::type>::value >=0, "WriteCovFile: MeanRangeT must hold fixed-size Eigen dense objects of dimensions Nx1.");
	static_assert( ColsM<typename range_value<MeanRangeT>::type>::value ==1, "WriteCovFile: MeanRangeT must hold fixed-size Eigen dense objects of dimensions Nx1.");
	static_assert( RowsM<typename range_value<CovarianceRangeT>::type>::value == RowsM<typename range_value<MeanRangeT>::type>::value, "WriteCovFile: MeanRangeT and CovarianceRangeT must hold fixed-size elements with the same number of rows.");
	static_assert( ColsM<typename range_value<CovarianceRangeT>::type>::value == RowsM<typename range_value<CovarianceRangeT>::type>::value, "WriteCovFile: CovarianceRangeT must hold fixed-size square matrices.");

	assert(boost::distance(meanRange)==boost::distance(covarianceRange));

	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CovarianceRangeT>));

	typedef typename range_value<MeanRangeT>::type MeanT;
	typedef typename ValueTypeM<MeanT>::type ValueT;

	constexpr int DIM=RowsM<MeanT>::value;

	typedef Array<ValueT, Eigen::Dynamic, DIM*(DIM+1)> ArrayT;
	typedef Array<ValueT, 1, DIM*DIM> CovarianceVectorT;

	size_t nElements=boost::distance(meanRange);
	ArrayT outputArray(nElements, DIM*(DIM+1));

	auto itM=boost::const_begin(meanRange);
	auto itC=boost::const_begin(covarianceRange);
	for(size_t c=0; c<nElements; ++c, advance(itM,1), advance(itC,1))
	{
		outputArray.row(c).head(DIM)=*itM;
		outputArray.row(c).tail(DIM*DIM)=Reshape<CovarianceVectorT>(*itC, 1, DIM*DIM);
	}	//for(size_t c=0; c<nElements; ++c, advance(itM,1), advance(itC,1))

	ArrayIOC<ArrayT>::WriteArray(filename, outputArray);
}	//void WriteCovFile(const string& filename, const MeanRangeT& meanRange, const CovarianceRangeT& covarianceRange)

/**
 * @brief Reads a cov file
 * @tparam ValueT  Value type
 * @tparam DIM Dimensionality
 * @param[in] filename Filename
 * @return A tuple: [Means; Covariances]
 * @ingroup IO
 */
template<typename ValueT, int DIM>
tuple<vector<CoordinateVectorT<ValueT, DIM> >, vector<Matrix<ValueT,DIM,DIM> > > ReadCovFile(const string& filename)
{
	typedef Array<ValueT, Eigen::Dynamic, DIM*(DIM+1)> ArrayT;
	typedef CoordinateVectorT<ValueT, DIM> CoordinateT;
	typedef Matrix<ValueT,DIM,DIM> CovarianceT;

	ArrayT raw=ArrayIOC<ArrayT>::ReadArray(filename);	//Read the array

	//Parse
	size_t nElements=raw.rows();	//Number of elements
	vector<CoordinateT> meanList; meanList.reserve(nElements);
	vector<CovarianceT> covarianceList; covarianceList.reserve(nElements);

	for(size_t c=0; c<nElements; ++c)
	{
		meanList.emplace_back(raw.row(c).head(DIM));
		covarianceList.emplace_back( Reshape<CovarianceT>(raw.row(c).tail(DIM*DIM),DIM,DIM));
	}	//for(const auto& current : raw)

	tuple<vector<CoordinateT>, vector<CovarianceT> > output;
	get<0>(output).swap(meanList);
	get<1>(output).swap(covarianceList);

	return output;
}	//tuple<vector<CoordinateVectorT<ValueT, DIM> >, Matrix<ValueT,DIM,DIM> > ReadCovFile(const string& filename)

}	//ION
}	//SeeSawN

#endif /* SPECIAL_ARRAY_IO_IPP_5389031 */
