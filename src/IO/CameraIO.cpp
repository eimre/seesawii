/**
 * @file CameraIO.cpp Implementation and instantiations of CameraIOC
 * @author Evren Imre
 * @date 7 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CameraIO.h"
namespace SeeSawN
{
namespace ION
{

/**
 * @brief Writes a sample file
 * @param[in] filename Filename
 */
void CameraIOC::WriteSampleFile(const string& filename)
{
	ptree tree;

	tree.put("xmlcomment", "Sample cam file");
	tree.put("xmlcomment", "Valid distortion models: FP1, OP1, DIV, NOD");

	tree.put("Camera","");
	tree.put("Camera.Tag", "Camera0");
	tree.put("Camera.Pose","");
	tree.put("Camera.Pose.Position", "1 1.5 2");
	tree.put("Camera.Pose.Orientation", "1 0 0 0");
	tree.put("Camera.Intrinsics", "");
	tree.put("Camera.Intrinsics.FocalLength", "1000");
	tree.put("Camera.Intrinsics.AspectRatio","1");
	tree.put("Camera.Intrinsics.Skewness", "0");
	tree.put("Camera.Intrinsics.PrincipalPoint", "960 540");
	tree.put("Camera.Intrinsics.FNumber", "5.6");
	tree.put("Camera.Intrinsics.PixelSize", "4e-6");
	tree.put("Camera.LensDistortion", "");
	tree.put("Camera.LensDistortion.Coefficient","1e-8");
	tree.put("Camera.LensDistortion.Centre", "960 540");
	tree.put("Camera.LensDistortion.Model", "DIV");
	tree.put("Camera.FrameBox","");
	tree.put("Camera.FrameBox.UpperLeft", "0 0");
	tree.put("Camera.FrameBox.LowerRight", "1919 1079");

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void WriteSampleFile(const string& filename)

/**
 * @brief Reads a camera file
 * @param[in] filename Filename
 * @return A vector containing the cameras in the file
 */
vector<CameraC> CameraIOC::ReadCameraFile(const string& filename)
{
	ptree tree;
	read_xml(filename, tree, boost::property_tree::xml_parser::no_comments | boost::property_tree::xml_parser::trim_whitespace);

	map<string, LensDistortionCodeT> stringToCode{ {"NOD", LensDistortionCodeT::NOD}, {"DIV", LensDistortionCodeT::DIV}, {"FP1", LensDistortionCodeT::FP1}, {"OP1", LensDistortionCodeT::OP1} };

	unsigned int nElements=tree.count("Camera");
	vector<CameraC> output; output.reserve(nElements);
	typedef CameraC::real_type RealT;

	CameraC defaultCamera;

	for(const auto& current : tree.get_child(""))
	{
		if(current.first!="Camera")
			continue;

		string tag=current.second.get<string>("Tag","");

		//Extrinsics
		string position=current.second.get<string>("Pose.Position","");
		string orientation=current.second.get<string>("Pose.Orientation","");

		optional<ExtrinsicC> extrinsics;
		if(!position.empty() || !orientation.empty())
		{
			extrinsics=ExtrinsicC();

			if(!position.empty())
			{
				vector<RealT> tokens=Tokenise<RealT>(position, string(" "));
				if(tokens.size()==3)
					extrinsics->position=Coordinate3DT(tokens[0], tokens[1], tokens[2]);
			}	//if(!position.empty())

			if(!orientation.empty())
			{
				vector<RealT> tokens=Tokenise<RealT>(orientation, string(" "));

				//Quaternion
				if(tokens.size()==4)
					extrinsics->orientation=QuaternionT(tokens[0], tokens[1], tokens[2], tokens[3]);

				//Rotation matrix
				if(tokens.size()==9)
				{
					RotationMatrix3DT mR; mR << tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], tokens[6], tokens[7], tokens[8];
					extrinsics->orientation=QuaternionT(mR);
				}	//if(tokens.size()==9)
			}	//if(!orientation.empty())

		}	//if(!position.empty() || !orientation.empty())

		//Intrinsics

		optional<IntrinsicC> intrinsics;

		string f=current.second.get<string>("Intrinsics.FocalLength","");
		string aspectRatio=current.second.get<string>("Intrinsics.AspectRatio","");
		string skewness=current.second.get<string>("Intrinsics.Skewness","");
		string principalPoint=current.second.get<string>("Intrinsics.PrincipalPoint","");
		if(!f.empty() || !aspectRatio.empty() || !skewness.empty() || !principalPoint.empty())
		{
			intrinsics=IntrinsicC();

			if(!f.empty())
				intrinsics->focalLength=lexical_cast<RealT>(f);

			if(!aspectRatio.empty())
				intrinsics->aspectRatio=lexical_cast<RealT>(aspectRatio);

			if(!skewness.empty())
				intrinsics->skewness=lexical_cast<RealT>(skewness);

			if(!principalPoint.empty())
			{
				vector<RealT> tokens=Tokenise<RealT>(principalPoint, string(" "));
				if(tokens.size()==2)
					intrinsics->principalPoint=Coordinate2DT(tokens[0], tokens[1]);
			}	//if(!principalPoint.empty())
		}	//		if(!f.empty() || !aspectRatio.empty() || !skewness.empty() || !principalPoint.empty())

		optional<double> fNumber=current.second.get_optional<double>("Intrinsics.FNumber");
		optional<double> pixelSize=current.second.get_optional<double>("Intrinsics.PixelSize");

		//Lens distortion

		optional<LensDistortionC> distortion;

		string kappa=current.second.get<string>("LensDistortion.Coefficient","");
		string distortionCentre=current.second.get<string>("LensDistortion.Centre","");
		string distortionModel=current.second.get<string>("LensDistortion.Model","");

		if(!kappa.empty() && !distortionCentre.empty() && !distortionModel.empty())
		{
			distortion=LensDistortionC();

			distortion->kappa=lexical_cast<RealT>(kappa);

			vector<RealT> tokens=Tokenise<RealT>(distortionCentre, string(" "));
			if(tokens.size()==2)
				distortion->centre=Coordinate2DT(tokens[0], tokens[1]);

			distortion->modelCode=stringToCode[distortionModel];
		}	//if(!kappa.empty() || !distortionCentre.empty() || !distortionModel.empty())

		optional<FrameT> frameBox;

		string upperLeft=current.second.get<string>("FrameBox.UpperLeft","");
		string lowerRight=current.second.get<string>("FrameBox.LowerRight","");

		if(!upperLeft.empty() && !lowerRight.empty())
		{
			frameBox=FrameT();

			vector<RealT> tokensUL=Tokenise<RealT>(upperLeft, string(" "));
			vector<RealT> tokensLR=Tokenise<RealT>(lowerRight, string(" "));

			if(tokensUL.size()==2 && tokensLR.size()==2)
				frameBox=FrameT(Coordinate2DT(tokensUL[0], tokensUL[1]), Coordinate2DT(tokensLR[0], tokensLR[1]));
		}	//if(!upperLeft.empty() && lowerRight.empty())

		output.emplace_back(extrinsics, intrinsics, distortion, frameBox, fNumber, pixelSize, tag);
	}	//for(const auto& current : tree.get_child("Camera"))

	return output;
}	//vector<CameraC> ReadCameraFile(const string& filename)

/**
 * @brief Reads a UniS calibration file
 * @param[in] filename Filename
 * @return A vector holding the cameras
 */
vector<CameraC> CameraIOC::ReadUniSFile(const string& filename)
{
	vector<CameraC> output;

	ifstream inputStream ( filename.data() );
	if(inputStream.fail())
		throw(runtime_error( string("CameraIOC::ReadUniSFile : Calibration file not found! ")+filename));

	unsigned int nCameras;
	inputStream>>nCameras;

	if(nCameras==0)
		return output;
	else
		output.reserve(nCameras);

	unsigned int cameraType;
	inputStream>>cameraType;
	if(cameraType!=1)
	throw(runtime_error( string("CameraIOC::ReadUniSFile : Not a UniS camera file! Camera type must be 1")));

    //Read the cameras
    for(unsigned int c=0; c<nCameras; ++c)
    {
        //Image size
    	Coordinate2DT ul;
    	Coordinate2DT lr;

        inputStream>>ul[1];
        inputStream>>lr[1];
        inputStream>>ul[0];
        inputStream>>lr[0];

        FrameT frameBox(ul,lr);

        //Intrinsics
        IntrinsicC intrinsics;
        double focalLength;
        inputStream>>focalLength;
        intrinsics.focalLength=focalLength;

        double rf;
        inputStream>>rf;
        intrinsics.aspectRatio=rf/ (*intrinsics.focalLength);

        intrinsics.skewness=0;

        Coordinate2DT principalPoint;
        inputStream>>principalPoint(0);
        inputStream>>principalPoint(1);
		intrinsics.principalPoint=principalPoint;

		//Distortion
		LensDistortionC distortion;
		inputStream>>distortion.kappa;

        distortion.centre=principalPoint;
		distortion.modelCode=LensDistortionCodeT::FP1;

		//Extrinsics

		ExtrinsicC extrinsics;

		RotationMatrix3DT mR;
		for(size_t c1=0; c1<3; ++c1)
			for(size_t c2=0; c2<3; ++c2)
				inputStream>>mR(c1,c2);

		extrinsics.orientation=QuaternionT(mR);

		Coordinate3DT translation;
        for(size_t c1=0; c1<3; ++c1)
            inputStream>>translation(c1);

        extrinsics.position=TranslationToPosition(translation, mR);

        output.emplace_back(extrinsics, intrinsics, distortion, frameBox, optional<double>(), optional<double>());
    }	//for(unsigned int c=0; c<nCameras; ++c)

	return output;
}	//vector<CameraC> ReadUniSFile(const string& filename)


/********** EXPLICIT INSTANTIATIONS **********/
template void CameraIOC::WriteCameraFile(const vector<CameraC>&, const string&, const string&, bool);
template void CameraIOC::WriteUniSFile(const vector<CameraC>&, const string&);

}	//ION
}	//SeeSawN

