/**
 * @file ArrayIO.h Public interface for ArrayIOC
 * @author Evren Imre
 * @date 4 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ARRAY_IO_H_8312561
#define ARRAY_IO_H_8312561

#include "ArrayIO.ipp"
#include <Eigen/Dense>

namespace SeeSawN
{
namespace ION
{

template<class ArrayT> class ArrayIOC;  ///< I/O operations for Eigen dense arrays

/********** EXTERN TEMPLATES **********/
using Eigen::MatrixXd;
extern template class ArrayIOC<MatrixXd>;

using Eigen::Matrix3d;
extern template class ArrayIOC<Matrix3d>;

using Eigen::VectorXd;
extern template class ArrayIOC<VectorXd>;

using Eigen::Vector3d;
extern template class ArrayIOC<Vector3d>;

using Eigen::MatrixXf;
extern template class ArrayIOC<MatrixXf>;

using Eigen::Matrix3f;
extern template class ArrayIOC<Matrix3f>;

using Eigen::VectorXf;
extern template class ArrayIOC<VectorXf>;

using Eigen::Vector3f;
extern template class ArrayIOC<Vector3f>;
}   //ION
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, std::size_t>(const std::size_t&);

#endif /* ARRAY_IO_H_8312561 */
