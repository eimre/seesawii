/**
 * @file SceneFeatureIO.cpp Implementation of the I/O class for 3D scene features
 * @author Evren Imre
 * @date 25 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SceneFeatureIO.h"
namespace SeeSawN
{
namespace ION
{

/**
 * @brief Writes a sample file to illustrate the format
 * @param[in] filename File name
 */
void SceneFeatureIOC::WriteSampleFile(const string& filename)
{
	 ofstream file(filename);

	if(file.fail())
		throw(runtime_error(string("SceneFeatureIOC::WriteSampleFile: Cannot open file at ") + filename));

	cout<<".ft3 file format:"<<"\n";
	cout<<"Image descriptor size "<<"Image RoI size "<<"\n"<<"Number of features"<<"\n";
	cout<<"Coordinates(3)"<<"\n"<<"Number of observations(1)"<<"\n";
	cout<<"Image Id(1)"<<" "<<"Coordinates(2)"<<" "<<"RoI(3)"<<" "<<"Descriptor(5)"<<"\n";

	ImageFeatureC feature2D1;
	feature2D1.Coordinate()<<120, 100;
	feature2D1.RoI().resize(3); feature2D1.RoI()<<0.25, 0.75, 0.1;
	feature2D1.Descriptor().resize(5); feature2D1.Descriptor()<<100, 25, 68, 11, 90;

	ImageFeatureC feature2D2;
	feature2D2.Coordinate()<<10, 15;
	feature2D2.RoI().resize(3); feature2D2.RoI()<<0.5, 0.755, 0.41;
	feature2D2.Descriptor().resize(5); feature2D2.Descriptor()<<10, 25, 71, 16, 2;

	SceneFeatureC feature3D;
	feature3D.Coordinate()<<0.25, 1.25, 0.75;
	feature3D.RoI().insert(0); feature3D.RoI().insert(1);

	feature3D.Descriptor().emplace(0, feature2D1);
	feature3D.Descriptor().emplace(1, feature2D2);

	file<<"5 3"<<"\n"<<"1"<<"\n"<<feature3D;
}	//void WriteSampleFile(const string& filename)

/**
 * @brief Reads a feature file
 * @param[in] filename File to be read in
 * @param[in] flagRandomise If \c true , the order of the features is randomised.
 * @return A vector of scene features
 * @throws runtime_error If cannot read the file
 * @remarks Randomisation is important to prevent biases in order-dependent algorithms
 */
vector<SceneFeatureC> SceneFeatureIOC::ReadFeatureFile(const string& filename, bool flagRandomise)
{
	ifstream file(filename);
	if(file.fail())
		throw(runtime_error(string("SceneFeatureIOC::ReadFeatureFile: Cannot open file at ") + filename));

	int sDescriptor;
	file>>sDescriptor;

	if(sDescriptor<0)
		throw(runtime_error(string("SceneFeatureIOC::ReadFeatureFile: Descriptor size must be non-negative, not ") + lexical_cast<string>(sDescriptor)));

	int sRoI;
	file>>sRoI;

	if(sRoI<0)
		throw(runtime_error(string("SceneFeatureIOC::ReadFeatureFile: RoI size must be non-negative, not ") + lexical_cast<string>(sRoI)));

	int nFeatures;
	file>>nFeatures;

	if(nFeatures<0)
		throw(runtime_error(string("SceneFeatureIOC::ReadFeatureFile: Number of features must be non-negative, not ") + lexical_cast<string>(nFeatures)));

	//Features

	vector<SceneFeatureC> output(nFeatures);

	if(nFeatures==0)
		return output;

	//Random shuffle

	vector<size_t> indexRange(nFeatures);
    iota(indexRange.begin(), indexRange.end(), 0);

    if(flagRandomise)
    	random_shuffle(indexRange.begin(), indexRange.end());

    for(size_t c=0; c<(size_t)nFeatures; ++c)
    {
    	size_t index=indexRange[c];

    	file>>output[index].Coordinate()[0];
    	file>>output[index].Coordinate()[1];
    	file>>output[index].Coordinate()[2];

    	int nObservations;
    	file>>nObservations;

    	for(int c2=0; c2<nObservations; ++c2)
    	{
    		ImageFeatureC::coordinate_type coordinate;
    		ImageFeatureC::roi_type imageRoI(sRoI);
    		ImageFeatureC::descriptor_type imageDescriptor(sDescriptor);

    		size_t observationIndex;
    		file>>observationIndex;

    		file>>coordinate[0];
    		file>>coordinate[1];

    		for(int cRoI=0; cRoI<sRoI; ++cRoI)
				file>>imageRoI[cRoI];

			for(int cDesc=0; cDesc<sDescriptor; ++cDesc)
				file>>imageDescriptor[cDesc];

			output[index].RoI().insert(observationIndex);
			output[index].Descriptor().emplace(observationIndex, ImageFeatureC(coordinate, imageDescriptor, imageRoI) );
    	}	//for(size_t c2=0; c2<nObservations; ++c2)
    }	//for(size_t c=0; c<nFeatures; ++c)

    return output;
}   //vector<ImageFeatureC> ReadFeatureFile(const string& filename)

/********** OrientedBinarySceneFeatureIOC **********/

/**
 * @brief Writes a sample file to illustrate the format
 * @param[in] filename File name
 */
void OrientedBinarySceneFeatureIOC::WriteSampleFile(const string& filename)
{
	ofstream file(filename);

	if(file.fail())
		throw(runtime_error(string("OrientedBinarySceneFeatureIOC::WriteSampleFile: Cannot open file at ") + filename));

	cout<<".bft3 file format:"<<"\n";
	cout<<"Image descriptor size "<<"Image RoI size "<<"\n"<<"Number of features"<<"\n";
	cout<<"Coordinates(3)"<<"\n"<<"Number of observations(1)"<<"\n";
	cout<<"Image Id(1)"<<" "<<"Coordinates(2)"<<" "<<"RoI(3)"<<" "<<"Descriptor(2)"<<"\n";

	BinaryImageFeatureC feature2D1;
	feature2D1.Coordinate()<<120, 100;
	feature2D1.RoI().resize(3); feature2D1.RoI()<<0.25, 0.75, 0.1;
	feature2D1.Descriptor().append(100);
	feature2D1.Descriptor().append(25);

	BinaryImageFeatureC feature2D2;
	feature2D2.Coordinate()<<10, 15;
	feature2D2.RoI().resize(3); feature2D2.RoI()<<0.5, 0.755, 0.41;
	feature2D2.Descriptor();
	feature2D2.Descriptor().append(71);
	feature2D2.Descriptor().append(16);

	OrientedBinarySceneFeatureC feature3D;
	feature3D.Coordinate()<<0.25, 1.25, 0.75;
	feature3D.RoI().insert(0); feature3D.RoI().insert(1);

	feature3D.Descriptor().emplace(0, feature2D1);
	feature3D.Descriptor().emplace(1, feature2D2);

	file<<"64 3"<<"\n"<<"1"<<"\n"<<feature3D;
}	//void WriteSampleFile(const string& filename)

/**
 * @brief Reads a feature file
 * @param[in] filename File to be read in
 * @param[in] flagRandomise If \c true , the order of the features is randomised.
 * @return A vector of scene features
 * @throws runtime_error If cannot read the file
 * @remarks Randomisation is important to prevent biases in order-dependent algorithms
 */
vector<OrientedBinarySceneFeatureC> OrientedBinarySceneFeatureIOC::ReadFeatureFile(const string& filename, bool flagRandomise)
{
	ifstream file(filename);
	if(file.fail())
		throw(runtime_error(string("OrientedBinarySceneFeatureIOC::ReadFeatureFile: Cannot open file at ") + filename));

	int sDescriptor;
	file>>sDescriptor;

	size_t nBlocks=ceil((float)sDescriptor/CHAR_BIT);	//Number of blocks

	if(sDescriptor<0)
		throw(runtime_error(string("OrientedBinarySceneFeatureIOC::ReadFeatureFile: Descriptor size must be non-negative, not ") + lexical_cast<string>(sDescriptor)));

	int sRoI;
	file>>sRoI;

	if(sRoI<0)
		throw(runtime_error(string("OrientedBinarySceneFeatureIOC::ReadFeatureFile: RoI size must be non-negative, not ") + lexical_cast<string>(sRoI)));

	int nFeatures;
	file>>nFeatures;

	if(nFeatures<0)
		throw(runtime_error(string("OrientedBinarySceneFeatureIOC::ReadFeatureFile: Number of features must be non-negative, not ") + lexical_cast<string>(nFeatures)));

	//Features

	vector<OrientedBinarySceneFeatureC> output(nFeatures);

	if(nFeatures==0)
		return output;

	//Random shuffle

	vector<size_t> indexRange(nFeatures);
    iota(indexRange.begin(), indexRange.end(), 0);

    if(flagRandomise)
    	random_shuffle(indexRange.begin(), indexRange.end());

    for(size_t c=0; c<(size_t)nFeatures; ++c)
    {
    	size_t index=indexRange[c];

    	file>>output[index].Coordinate()[0];
    	file>>output[index].Coordinate()[1];
    	file>>output[index].Coordinate()[2];

    	int nObservations;
    	file>>nObservations;

    	for(int c2=0; c2<nObservations; ++c2)
    	{

    		size_t observationIndex;
    		file>>observationIndex;

			output[index].RoI().insert(observationIndex);
			output[index].Descriptor().emplace(observationIndex, BinaryImageFeatureIOC::ReadFeature(file, sDescriptor, nBlocks, sRoI) );
    	}	//for(size_t c2=0; c2<nObservations; ++c2)
    }	//for(size_t c=0; c<nFeatures; ++c)

    return output;
}   //vector<ImageFeatureC> ReadFeatureFile(const string& filename)
}	//ION
}	//SeeSawN


