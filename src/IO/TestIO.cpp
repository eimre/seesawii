/**
 * @file TestIO.cpp Unit tests for IO
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE IO

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdexcept>
#include "ImageFeatureIO.h"
#include "CoordinateCorrespondenceIO.h"
#include "SceneFeatureIO.h"
#include "ImageFeatureTrajectoryIO.h"
#include "ArrayIO.h"
#include "CameraIO.h"
#include "SpecialArrayIO.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Camera.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestION
{

using namespace SeeSawN::ION;
using Eigen::Matrix3d;
using Eigen::MatrixXd;
using Eigen::Vector3d;
using Eigen::Vector2d;
using Eigen::Matrix2d;
using std::vector;
using std::string;
using std::cout;
using std::getenv;
using std::ostringstream;
using std::runtime_error;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::ImageFeatureTrajectoryT;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::CoordinateTransformations2DT;

BOOST_AUTO_TEST_SUITE(Image_IO)

BOOST_AUTO_TEST_CASE(Image_Feature_IO)
{
    //Write a test file
	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Image_Feature_IO_1.ft2";
		ImageFeatureIOC::WriteSampleFile(filename);

		//Read the file
		vector<ImageFeatureC> features=ImageFeatureIOC::ReadFeatureFile(filename);
		BOOST_CHECK_EQUAL(features.size(), (unsigned int)1);

		ImageFeatureC feature;
		feature.Coordinate()<<120, 100;
		feature.RoI().resize(3); feature.RoI()<<0.25, 0.75, 0.1;
		feature.Descriptor().resize(5); feature.Descriptor()<<100, 25, 68, 11, 90;

		BOOST_CHECK(feature.Coordinate()==features[0].Coordinate());
		BOOST_CHECK(feature.RoI()==features[0].RoI());
		BOOST_CHECK(feature.Descriptor()==features[0].Descriptor());

		//Write

		ImageFeatureC feature2(feature);
		feature2.Coordinate()[1]=250;
		feature2.RoI()[1]=250;
		feature2.Descriptor()[1]=250;
		features.push_back(feature2);

		string filename2=path+"TestIO_Image_Feature_IO_2.ft2";
		ImageFeatureIOC::WriteFeatureFile(features, filename2);

		//Read again
		vector<ImageFeatureC> features2=ImageFeatureIOC::ReadFeatureFile(filename2);
		BOOST_CHECK_EQUAL(features2.size(), (unsigned int)2);

		BOOST_CHECK(feature.Coordinate()==features2[0].Coordinate());
		BOOST_CHECK(feature.RoI()==features2[0].RoI());
		BOOST_CHECK(feature.Descriptor()==features2[0].Descriptor());

		BOOST_CHECK(feature2.Coordinate()==features2[1].Coordinate());
		BOOST_CHECK(feature2.RoI()==features2[1].RoI());
		BOOST_CHECK(feature2.Descriptor()==features2[1].Descriptor());

		//Preprocess
		typedef ImageFeatureC::coordinate_type CoordinateT;
		optional<tuple<CoordinateT, CoordinateT> > boundingBox;
		optional<LensDistortionFP1C> lensDistortion;
		optional<CoordinateTransformations2DT::projective_transform_type> mT;
		vector<ImageFeatureC> features3(features2);
		ImageFeatureIOC::PreprocessFeatureSet(features3, boundingBox, 0, true, lensDistortion, mT);

		BOOST_CHECK_EQUAL(features3.size(), (size_t)2);
		BOOST_CHECK(features3[0].Descriptor().norm()==1);
		BOOST_CHECK(features3[0].Coordinate()==features2[0].Coordinate());
		BOOST_CHECK(features3[1].Coordinate()==features2[1].Coordinate());
		BOOST_CHECK(features3[0].RoI()==features2[0].RoI());
		BOOST_CHECK(features3[1].RoI()==features2[1].RoI());

		lensDistortion=LensDistortionFP1C(CoordinateT(0, 100), -1e-5);
		mT=CoordinateTransformations2DT::projective_transform_type();
		mT->setIdentity(); mT->matrix()(0,0)=2; mT->matrix()(1,1)=2;
		vector<ImageFeatureC> features3b(features2);;
		ImageFeatureIOC::PreprocessFeatureSet(features3b, boundingBox, 0, true, lensDistortion, mT);

		BOOST_CHECK(features3b[0].Coordinate().isApprox(CoordinateT(240.288, 200), 1e-3));

		vector<ImageFeatureC> features3c(features2);
		ImageFeatureIOC::PreprocessFeature(features3c[0], true, lensDistortion, mT);
		BOOST_CHECK(features3c[0].Coordinate().isApprox(CoordinateT(240.288, 200), 1e-3));

		//Postprocess
		vector<ImageFeatureC> features4(features3b);
		*mT = mT->inverse();
		ImageFeatureIOC::PostprocessFeatureSet(features4, lensDistortion, mT);

		BOOST_CHECK(features4[0].Coordinate().isApprox(features2[0].Coordinate(), 1e-8));

		vector<ImageFeatureC> features5(features3b);
		ImageFeatureIOC::PostprocessFeature(features5[0], lensDistortion, mT);
		BOOST_CHECK(features4[0].Coordinate()==features5[0].Coordinate());
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Image_IO::Image_Feature_IO : Path variable not found. Try again with ctest\n";

}//BOOST_AUTO_TEST_CASE(Image_Feature_IO)

BOOST_AUTO_TEST_CASE(Binary_Image_Feature_IO)
{
    //Write a test file
	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Binary_Image_Feature_IO_1.bft2";
		BinaryImageFeatureIOC::WriteSampleFile(filename);

		//Read the file
		vector<BinaryImageFeatureC> features=BinaryImageFeatureIOC::ReadFeatureFile(filename);
		BOOST_CHECK_EQUAL(features.size(), (unsigned int)1);

		BinaryImageFeatureC feature;
		feature.Coordinate()<<120, 100;
		feature.RoI().resize(3); feature.RoI()<<0.25, 0.75, 0.1;
		feature.Descriptor()=BinaryImageFeatureC::descriptor_type(10, false); feature.Descriptor()[0]=true; feature.Descriptor()[4]=true; feature.Descriptor()[8]=true;

		BOOST_CHECK(feature.Coordinate()==features[0].Coordinate());
		BOOST_CHECK(feature.RoI()==features[0].RoI());
		BOOST_CHECK(feature.Descriptor()==features[0].Descriptor());
		//Write

		BinaryImageFeatureC feature2(feature);
		feature2.Coordinate()[1]=250;
		feature2.RoI()[1]=250;
		feature2.Descriptor()[1]=true;
		features.push_back(feature2);

		string filename2=path+"TestIO_Binary_Image_Feature_IO_1.bft2";
		BinaryImageFeatureIOC::WriteFeatureFile(features, filename2);

		//Read again
		vector<BinaryImageFeatureC> features2=BinaryImageFeatureIOC::ReadFeatureFile(filename2);
		BOOST_CHECK_EQUAL(features2.size(), (unsigned int)2);

		BOOST_CHECK(feature.Coordinate()==features2[0].Coordinate());
		BOOST_CHECK(feature.RoI()==features2[0].RoI());
		BOOST_CHECK(feature.Descriptor()==features2[0].Descriptor());

		BOOST_CHECK(feature2.Coordinate()==features2[1].Coordinate());
		BOOST_CHECK(feature2.RoI()==features2[1].RoI());
		BOOST_CHECK(feature2.Descriptor()==features2[1].Descriptor());
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Image_IO::Binary_Image_Feature_IO : Path variable not found. Try again with ctest\n";

}	//BOOST_AUTO_TEST_CASE(Binary_Image_Feature_IO)

BOOST_AUTO_TEST_SUITE_END() //Image_IO

BOOST_AUTO_TEST_SUITE(Correspondence_IO)

BOOST_AUTO_TEST_CASE(Coordinate_Correspondence_IO)
{
	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		//Write a test file
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Coordinate_Correspondence_IO_1.c22";
		CoordinateCorrespondenceIOC::WriteSampleFile(filename);

		//Index->Coordinate conversion

		CorrespondenceListT correspondences;
		typedef CorrespondenceListT::value_type CorrespondenceT;

		MatchStrengthC strength(0.25, 0.9);
		correspondences.push_back(CorrespondenceT(0, 1, strength));

		vector<Coordinate2DT> coordinates1(1, Coordinate2DT(5,2));

		vector<Coordinate2DT> coordinates2; coordinates2.reserve(2);
		coordinates2.emplace_back(Coordinate2DT(1,4));
		coordinates2.emplace_back(Coordinate2DT(1,2));

		CoordinateCorrespondenceList2DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(correspondences, coordinates1, coordinates2);

		BOOST_CHECK( coordinateCorrespondences.begin()->left == (*coordinates1.begin()) );
		BOOST_CHECK(coordinateCorrespondences.begin()->right == (*coordinates2.rbegin()) );
		BOOST_CHECK_EQUAL(coordinateCorrespondences.begin()->info.Similarity(), strength.Similarity());
		BOOST_CHECK_EQUAL(coordinateCorrespondences.begin()->info.Ambiguity(), strength.Ambiguity() );

		//Write a correspondence file
		string filename2=path+"TestIO_Coordinate_Correspondence_IO_2.c22";
		CoordinateCorrespondenceIOC::WriteCorrespondenceFile(coordinateCorrespondences, filename2);
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Correspondence_IO::Coordinate_Correspondence_IO : Path variable not found. Try again with ctest\n";
	    //TODO After a read function is implemented, do read->write->read tests
}   //BOOST_AUTO_TEST_CASE(Coordinate_Correspondence_IO)

BOOST_AUTO_TEST_SUITE_END()	//Correspondence_IO

BOOST_AUTO_TEST_SUITE(Scene_IO)

BOOST_AUTO_TEST_CASE(Scene_IO_Tests)
{
	ImageFeatureC feature2D1;
	feature2D1.Coordinate()<<120, 100;
	feature2D1.RoI().resize(3); feature2D1.RoI()<<0.25, 0.75, 0.1;
	feature2D1.Descriptor().resize(5); feature2D1.Descriptor()<<100, 25, 68, 11, 90;

	ImageFeatureC feature2D2;
	feature2D2.Coordinate()<<10, 15;
	feature2D2.RoI().resize(3); feature2D2.RoI()<<0.5, 0.755, 0.41;
	feature2D2.Descriptor().resize(5); feature2D2.Descriptor()<<10, 25, 71, 16, 2;

	SceneFeatureC feature3D;
	feature3D.Coordinate()<<0.25, 1.25, 0.75;
	feature3D.RoI().insert(0); feature3D.RoI().insert(1);

	feature3D.Descriptor().emplace(0, feature2D1);
	feature3D.Descriptor().emplace(1, feature2D2);

	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		//Write a test file
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Scene_Feature_IO_1.ft3";
		SceneFeatureIOC::WriteSampleFile(filename);

		//Read the file back
		vector<SceneFeatureC> features1=SceneFeatureIOC::ReadFeatureFile(filename, true);

		BOOST_CHECK_EQUAL(features1.size(),1);
		BOOST_CHECK(feature3D.Coordinate()==features1[0].Coordinate());
		BOOST_CHECK_EQUAL_COLLECTIONS(feature3D.RoI().begin(), feature3D.RoI().end(), features1[0].RoI().begin(), features1[0].RoI().end());

		for(const auto& current : feature3D.Descriptor())
		{
			BOOST_CHECK(current.second.Coordinate()==features1[0].Descriptor()[current.first].Coordinate());
			BOOST_CHECK(current.second.RoI()==features1[0].Descriptor()[current.first].RoI());
			BOOST_CHECK(current.second.Descriptor()==features1[0].Descriptor()[current.first].Descriptor());
		};	//for(const auto& current : feature3D.Descriptor())

		//Write again, and read back

		string filename2=path+"TestIO_Scene_Feature_IO_2.ft3";
		features1.push_back(feature3D);
		SceneFeatureIOC::WriteFeatureFile(features1, filename2);
		vector<SceneFeatureC> features2=SceneFeatureIOC::ReadFeatureFile(filename2, false);

		BOOST_CHECK_EQUAL(features2.size(),2);
		for(size_t c=0; c<2; ++c)
		{
			BOOST_CHECK(feature3D.Coordinate()==features2[c].Coordinate());
			BOOST_CHECK_EQUAL_COLLECTIONS(feature3D.RoI().begin(), feature3D.RoI().end(), features2[c].RoI().begin(), features2[c].RoI().end());

			for(const auto& current : feature3D.Descriptor())
			{
				BOOST_CHECK(current.second.Coordinate()==features2[c].Descriptor()[current.first].Coordinate());
				BOOST_CHECK(current.second.RoI()==features2[c].Descriptor()[current.first].RoI());
				BOOST_CHECK(current.second.Descriptor()==features2[c].Descriptor()[current.first].Descriptor());
			};	//for(const auto& current : feature3D.Descriptor())
		}	//for(size_t c=0; c<2; ++c)

		//Postprocess

		LensDistortionFP1C lensDistortion(Coordinate2DT(0, 100), -1e-5);
		CoordinateTransformations2DT::projective_transform_type mT;
		mT.setIdentity(); mT.matrix()(0,0)=2; mT.matrix()(1,1)=2;

		vector<LensDistortionC> distortionList(2);
		distortionList[0].modelCode=LensDistortionCodeT::FP1; distortionList[1].modelCode=LensDistortionCodeT::NOD;
		distortionList[0].centre=Coordinate2DT(0,100); distortionList[1].centre=Coordinate2DT(0,0);
		distortionList[0].kappa=-1e-5; distortionList[1].kappa=0;

		vector<optional<CoordinateTransformations2DT::projective_transform_type> > transformation(2);
		transformation[0]=mT;

		vector<SceneFeatureC> features3(features2);
		SceneFeatureIOC::PostprocessDescriptors(features3, distortionList, transformation);

		BOOST_CHECK(features3[0].Descriptor()[0].Coordinate().isApprox( * (lensDistortion.Apply(Coordinate2DT(240,200)))));
		BOOST_CHECK(features3[0].Descriptor()[1].Coordinate()==features2[0].Descriptor()[1].Coordinate());

		//Process
		vector<SceneFeatureC> features4(features2);
		SceneFeatureIOC::PreprocessDescriptors(features4, true);
		BOOST_CHECK_CLOSE(features4[0].Descriptor()[0].Descriptor().norm(), 1, 1e-8);
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Scene_IO::Scene_Feature_IO : Path variable not found. Try again with ctest\n";

}	//BOOST_AUTO_TEST_CASE(Scene_IO_Tests)

BOOST_AUTO_TEST_CASE(Oriented_Binary_Scene_IO_Tests)
{
	BinaryImageFeatureC feature2D1;
	feature2D1.Coordinate()<<120, 100;
	feature2D1.RoI().resize(3); feature2D1.RoI()<<0.25, 0.75, 0.1;
	feature2D1.Descriptor().append(100);
	feature2D1.Descriptor().append(25);

	BinaryImageFeatureC feature2D2;
	feature2D2.Coordinate()<<10, 15;
	feature2D2.RoI().resize(3); feature2D2.RoI()<<0.5, 0.755, 0.41;
	feature2D2.Descriptor();
	feature2D2.Descriptor().append(71);
	feature2D2.Descriptor().append(16);

	OrientedBinarySceneFeatureC feature3D;
	feature3D.Coordinate()<<0.25, 1.25, 0.75;
	feature3D.RoI().insert(0); feature3D.RoI().insert(1);

	feature3D.Descriptor().emplace(0, feature2D1);
	feature3D.Descriptor().emplace(1, feature2D2);

	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		//Write a test file
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Oriented_Binary_Scene_Feature_IO_1.bft3";
		OrientedBinarySceneFeatureIOC::WriteSampleFile(filename);

		//Read the file back
		vector<OrientedBinarySceneFeatureC> features1=OrientedBinarySceneFeatureIOC::ReadFeatureFile(filename, true);

		BOOST_CHECK_EQUAL(features1.size(),1);
		BOOST_CHECK(feature3D.Coordinate()==features1[0].Coordinate());
		BOOST_CHECK_EQUAL_COLLECTIONS(feature3D.RoI().begin(), feature3D.RoI().end(), features1[0].RoI().begin(), features1[0].RoI().end());

		for(const auto& current : feature3D.Descriptor())
		{
			BOOST_CHECK(current.second.Coordinate()==features1[0].Descriptor()[current.first].Coordinate());
			BOOST_CHECK(current.second.RoI()==features1[0].Descriptor()[current.first].RoI());
			BOOST_CHECK(current.second.Descriptor()==features1[0].Descriptor()[current.first].Descriptor());
		};	//for(const auto& current : feature3D.Descriptor())

		//Write again, and read back

		string filename2=path+"TestIO_Oriented_Binary_Scene_Feature_IO_2.ft3";
		features1.push_back(feature3D);
		SceneFeatureIOC::WriteFeatureFile(features1, filename2);
		vector<OrientedBinarySceneFeatureC> features2=OrientedBinarySceneFeatureIOC::ReadFeatureFile(filename2, false);

		BOOST_CHECK_EQUAL(features2.size(),2);
		for(size_t c=0; c<2; ++c)
		{
			BOOST_CHECK(feature3D.Coordinate()==features2[c].Coordinate());
			BOOST_CHECK_EQUAL_COLLECTIONS(feature3D.RoI().begin(), feature3D.RoI().end(), features2[c].RoI().begin(), features2[c].RoI().end());

			for(const auto& current : feature3D.Descriptor())
			{
				BOOST_CHECK(current.second.Coordinate()==features2[c].Descriptor()[current.first].Coordinate());
				BOOST_CHECK(current.second.RoI()==features2[c].Descriptor()[current.first].RoI());
				BOOST_CHECK(current.second.Descriptor()==features2[c].Descriptor()[current.first].Descriptor());
			};	//for(const auto& current : feature3D.Descriptor())
		}	//for(size_t c=0; c<2; ++c)

	}
	else
		cout<<"Scene_IO::Oriented_Binary_Scene_IO_Tests : Path variable not found. Try again with ctest\n";


}	//BOOST_AUTO_TEST_CASE(Oriented_Binary_Scene_IO_Tests)

BOOST_AUTO_TEST_SUITE_END() //Scene_IO

BOOST_AUTO_TEST_SUITE(Trajectory_IO)

BOOST_AUTO_TEST_CASE(Trajectory_IO_Tests)
{
	typedef ImageFeatureTrajectoryT::key_type TimeStampT;
	typedef ImageFeatureTrajectoryIOC<TimeStampT> ImageFeatureTrajectoryIOT;
	typedef ImageFeatureTrajectoryIOT::trajectory_type TrajectoryT;
	typedef TrajectoryT::key_type TimeStampT;

	vector<ImageFeatureC> features(4);

	features[0].Coordinate()<<100, 120;
	features[0].RoI().resize(3); features[0].RoI()<<0.1, 0.25, 0.3;
	features[0].Descriptor().resize(5); features[0].Descriptor()<<1, 4, 6, 1, 2;

	features[1].Coordinate()<<125, 120;
	features[1].RoI().resize(3); features[1].RoI()<<0.1, 0.25, 0.3;
	features[1].Descriptor().resize(5); features[1].Descriptor()<<1, 4, 6, 1, 2;

	features[2].Coordinate()<<127, 120;
	features[2].RoI().resize(3); features[2].RoI()<<0.15, 0.25, 0.21;
	features[2].Descriptor().resize(5); features[2].Descriptor()<<1, 2, 3, 1, 6;

	features[3].Coordinate()<<130, 112;
	features[3].RoI().resize(3); features[3].RoI()<<0.1, 0.28, 0.4;
	features[3].Descriptor().resize(5); features[3].Descriptor()<<2,0,1,3,5;

	vector<TrajectoryT> trajectoryList1(2);
	trajectoryList1[0].emplace(0, features[0]);

	trajectoryList1[1].emplace(0, features[1]);
	trajectoryList1[1].emplace(1, features[2]);
	trajectoryList1[1].emplace(2, features[3]);

	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		//Write a test file
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Image_Feature_Trajectory_IO_1.trj";
		ImageFeatureTrajectoryIOT::WriteSampleFile(filename);

		//Read the file back
		vector<TrajectoryT> trajectoryList2=ImageFeatureTrajectoryIOT::ReadTrajectoryFile(filename);

		BOOST_CHECK_EQUAL(trajectoryList2.size(),2);
		size_t c=0;
		for(const auto& current : trajectoryList2)
		{
			BOOST_CHECK_EQUAL(current.begin()->first, trajectoryList1[c].begin()->first);
			BOOST_CHECK(current.begin()->second.Coordinate()==trajectoryList1[c].begin()->second.Coordinate());
			BOOST_CHECK(current.begin()->second.RoI()==trajectoryList1[c].begin()->second.RoI());
			BOOST_CHECK(current.begin()->second.Descriptor()==trajectoryList1[c].begin()->second.Descriptor());
			++c;
		};	//for(const auto& current : feature3D.Descriptor())

		//Write again and read back

		string filename2=path+"TestIO_Image_Feature_Trajectory_IO_2.trj";
		ImageFeatureTrajectoryIOT::WriteTrajectoryFile(trajectoryList2, filename2);
		vector<ImageFeatureTrajectoryT> trajectoryList3=ImageFeatureTrajectoryIOT::ReadTrajectoryFile(filename2);

		BOOST_CHECK_EQUAL(trajectoryList2.size(),2);
		size_t c2=0;
		for(const auto& current : trajectoryList2)
		{
			BOOST_CHECK_EQUAL(current.begin()->first, trajectoryList3[c2].begin()->first);
			BOOST_CHECK(current.begin()->second.Coordinate()==trajectoryList3[c2].begin()->second.Coordinate());
			BOOST_CHECK(current.begin()->second.RoI()==trajectoryList3[c2].begin()->second.RoI());
			BOOST_CHECK(current.begin()->second.Descriptor()==trajectoryList3[c2].begin()->second.Descriptor());
			++c2;
		};	//for(const auto& current : feature3D.Descriptor())

		//Preprocess

		LensDistortionC lensDistortion;
		lensDistortion.centre=Coordinate2DT(0,100);
		lensDistortion.kappa=-1e-5;
		lensDistortion.modelCode=LensDistortionCodeT::FP1;

		optional<CoordinateTransformations2DT::projective_transform_type> mT;
		mT->setIdentity(); mT->matrix()(0,0)=2; mT->matrix()(1,1)=2;

		vector<TrajectoryT> trajectoryList4(trajectoryList1);
		ImageFeatureTrajectoryIOT::PreprocessMeasurements(trajectoryList4, true, lensDistortion, mT);

		BOOST_CHECK(trajectoryList4[0].begin()->second.Coordinate().isApprox(Coordinate2DT(100.102, 120.02), 1e-3));
		BOOST_CHECK_CLOSE(trajectoryList4[0].begin()->second.Descriptor().norm(),1,1e-5);

		//Postprocess
		vector<TrajectoryT> trajectoryList5(trajectoryList4);
		ImageFeatureTrajectoryIOT::PostprocessMeasurements(trajectoryList5, lensDistortion, mT);

		BOOST_CHECK(trajectoryList5[0].begin()->second.Coordinate().isApprox(Coordinate2DT(100, 120), 1e-3));
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
}	//BOOST_AUTO_TEST_CASE(Trajectory_IO_Tests)

BOOST_AUTO_TEST_SUITE_END() //Trajectory_IO

BOOST_AUTO_TEST_SUITE(Camera_IO)

BOOST_AUTO_TEST_CASE(Cam_IO)
{
	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
		{
		//Write a test file
		string path(getenv("SEESAW_TEMP_PATH"));

		string filename=path+"TestIO_Camera_IO_1.cam";
		CameraIOC::WriteSampleFile(filename);

		//Read again
		vector<CameraC> cameraList=CameraIOC::ReadCameraFile(filename);

		ExtrinsicC deltaExtrinsics;
		deltaExtrinsics.position=Coordinate3DT(0, 0.25, 0);
		CameraC camera2=CameraC::ApplyPerturbation(cameraList[0], deltaExtrinsics);

		cameraList.push_back(camera2);

		//Write
		CameraIOC::WriteCameraFile(cameraList, path+"TestIO_Camera_IO_2.cam", "Test file", true);
		CameraIOC::WriteCameraFile(cameraList, path+"TestIO_Camera_IO_3.cam", "Test file", false);

		//Read again, and compare the camera matrices
		vector<CameraC> cameraList2=CameraIOC::ReadCameraFile(path+"TestIO_Camera_IO_2.cam");
		BOOST_CHECK(cameraList[0].MakeCameraMatrix()->isApprox(*cameraList2[0].MakeCameraMatrix(), 1e-6));
		BOOST_CHECK(cameraList[1].MakeCameraMatrix()->isApprox(*cameraList2[1].MakeCameraMatrix(), 1e-6));

		vector<CameraC> cameraList3=CameraIOC::ReadCameraFile(path+"TestIO_Camera_IO_3.cam");
		BOOST_CHECK(cameraList[0].MakeCameraMatrix()->isApprox(*cameraList3[0].MakeCameraMatrix(), 1e-6));
		BOOST_CHECK(cameraList[1].MakeCameraMatrix()->isApprox(*cameraList3[1].MakeCameraMatrix(), 1e-6));
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Camera_IO::Camera_IO_Operation : Path variable not found. Try again with ctest\n";

}	//BOOST_AUTO_TEST_CASE(Camera_IO_Operation)

BOOST_AUTO_TEST_CASE(UniS_IO)
{
	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		//Read a 2-camera file
		string path(getenv("SEESAW_TEMP_PATH"));
		vector<CameraC> cameraList=CameraIOC::ReadCameraFile(path+"TestIO_Camera_IO_2.cam");

		cameraList[0].Distortion()->modelCode=LensDistortionCodeT::FP1;
		cameraList[1].Distortion()->modelCode=LensDistortionCodeT::FP1;

		//Write as a UniS file
		CameraIOC::WriteUniSFile(cameraList, path+"TestIO_Camera_IO_2.cal");

		//Read and verify
		vector<CameraC> cameraList2=CameraIOC::ReadUniSFile(path+"TestIO_Camera_IO_2.cal");
		BOOST_CHECK(cameraList[0].MakeCameraMatrix()->isApprox(*cameraList2[0].MakeCameraMatrix(), 1e-6));
		BOOST_CHECK(cameraList[1].MakeCameraMatrix()->isApprox(*cameraList2[1].MakeCameraMatrix(), 1e-6));
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Camera_IO::UniS_IO : Path variable not found. Try again with ctest\n";
}	//BOOST_AUTO_TEST_CASE(UniS_IO)

BOOST_AUTO_TEST_SUITE_END()	//Camera_IO

BOOST_AUTO_TEST_SUITE(General_Purpose_IO)

BOOST_AUTO_TEST_CASE(Array_IO)
{
	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		Matrix3d m;
		m<<1, 0.4, 3.343, 2.3, 1, 4.9, 5, 4, 2;

		string path(getenv("SEESAW_TEMP_PATH"));
		string filename=path+"TestIO_Array_IO_matrix.txt";
		ArrayIOC<Matrix3d>::WriteArray(filename, m);

		Matrix3d m2=ArrayIOC<Matrix3d>::ReadArray(filename, 3, 3);

		BOOST_CHECK(m==m2);

		Vector3d v; v<<1,2,3;
		string strv("1 \n2 \n3 \n");
		ostringstream out;
		ArrayIOC<Vector3d>::WriteArray(out, v);

		string strvo=out.str();
		BOOST_CHECK_EQUAL_COLLECTIONS(strvo.begin(), strvo.end(), strv.begin(), strv.end());

		//ReadArray, generic variant

		MatrixXd m3=ArrayIOC<MatrixXd>::ReadArray(filename);
		BOOST_CHECK(m==m3);

		BOOST_CHECK_THROW(ArrayIOC<Vector3d>::ReadArray(filename), runtime_error);
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"General_Purpose_IO::Array_IO : Path variable not found. Try again with ctest\n";

}   //BOOST_AUTO_TEST_CASE(Array_IO)

BOOST_AUTO_TEST_SUITE_END() //General_Purpose_IO

BOOST_AUTO_TEST_SUITE(Parsing_Array_IO)

BOOST_AUTO_TEST_CASE(Special_Array_IO)
{
	vector<Vector2d> coordinates(2);
	vector<Matrix2d> covariances(2);
	for(size_t c=0; c<2; ++c)
	{
		coordinates[c]=Vector2d::Random();
		covariances[c]=Matrix2d::Random();
	}	//for(size_t c=0; c<2; ++c)

	if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	{
		string path(getenv("SEESAW_TEMP_PATH"));
		string filename=path+"TestIO_Special_Array_IO.cov";

		WriteCovFile(filename, coordinates, covariances);

		vector<Vector2d> coordinates2;
		vector<Matrix2d> covariances2;
		tie(coordinates2, covariances2)=ReadCovFile<double,2>(filename);

		for(size_t c=0; c<2; ++c)
		{
			BOOST_CHECK(coordinates[c].isApprox(coordinates2[c], 1e-6));
			BOOST_CHECK(covariances[c].isApprox(covariances2[c], 1e-6));
		}	//for(size_t c=0; c<2; ++c)
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Parsing_Array_IO::Special_Array_IO : Path variable not found. Try again with ctest\n";

}	//BOOST_AUTO_TEST_CASE(Special_Array_IO)

BOOST_AUTO_TEST_SUITE_END()	//Parsing_Array_IO

}   //TestION
}   //UnitTestsN
}	//SeeSawN


