/**
 * @file CoordinateCorrespondenceIO.h Public interface for CoordinateCorrespondenceIOC
 * @author Evren Imre
 * @date 22 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_CORRESPONDENCE_IO_H_2132562
#define IMAGE_CORRESPONDENCE_IO_H_2132562

#include "CoordinateCorrespondenceIO.ipp"
#include "../Elements/Correspondence.h"

namespace SeeSawN
{
namespace ION
{

class CoordinateCorrespondenceIOC;    ///< I/O operations for correspondence correspondences

/********** EXTERN TEMPLATES **********/
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
extern template void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CoordinateCorrespondenceList2DT&, const string&);

using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
extern template void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CoordinateCorrespondenceList3DT&, const string&);

using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
extern template void CoordinateCorrespondenceIOC::WriteCorrespondenceFile(const CoordinateCorrespondenceList32DT&, const string&);


}   //ION
}	//SeeSawN

#endif /* IMAGE_CORRESPONDENCE_IO_H_2132562 */
