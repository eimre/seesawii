/**
 * @file ImageFeatureIO.cpp Implementation of ImageFeatureIOC
 * @author Evren Imre
 * @date 18 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ImageFeatureIO.h"

namespace SeeSawN
{
namespace ION
{

/*********** BaseImageFeatureIOC **********/

/**
 * @brief Descriptor normalisation for \c ImageFeatureC
 * @param[in] feature Feature to be normalised
 */
void BaseImageFeatureIOC::Normalise(ImageFeatureC& feature)
{
	feature.Descriptor().normalize();
}	//void Normalise(ImageFeatureC& feature)

/*********** ImageFeatureIOC **********/

/**
 * @brief Writes a sample file to illustrate the format
 * @param[in] filename Output filename
 * @throws runtime_error If the file cannot be opened
 */
void ImageFeatureIOC::WriteSampleFile(const string& filename)
{
    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("ImageFeatureIOC::WriteSampleFile: Cannot open file at ") + filename));

    cout<<".ft2 file format:"<<"\n"<<"Descriptor size"<<"\n"<<"Number of features"<<"\n"<<"Coordinates(2)"<<" "<<"RoI(3)"<<" "<<"Descriptor(5)"<<"\n";

    ImageFeatureC feature;
    feature.Coordinate()<<120, 100;
    feature.RoI().resize(3); feature.RoI()<<0.25, 0.75, 0.1;
    feature.Descriptor().resize(5); feature.Descriptor()<<100, 25, 68, 11, 90;

    file<<5<<"\n"<<1<<"\n"<<feature;
}   //void WriteSampleFile(const string& filename)

/**
 * @brief Reads a feature file
 * @param[in] filename File to be read in
 * @param[in] flagRandomise If \c true , the order of the features is randomised.
 * @return A vector of image features
 * @remarks Randomisation is important to prevent biases in order-dependent algorithms
 */
vector<ImageFeatureC> ImageFeatureIOC::ReadFeatureFile(const string& filename, bool flagRandomise)
{
    ifstream file(filename);
    if(file.fail())
        throw(runtime_error(string("ImageFeatureIOC::ReadFeatureFile: Cannot open file at ") + filename));

    //Header

    int sDescriptor;
    file>>sDescriptor;

    if(sDescriptor<0)
        throw(runtime_error(string("ImageFeatureIOC::ReadFeatureFile: Descriptor size must be non-negative, not ") + lexical_cast<string>(sDescriptor)));

    int nFeatures;
    file>>nFeatures;

    if(nFeatures<0)
        throw(runtime_error(string("ImageFeatureIOC::ReadFeatureFile: Number of features must be non-negative, not ") + lexical_cast<string>(nFeatures)));

    //Features

    vector<ImageFeatureC> output;
    if(nFeatures==0)
        return output;

    //Determine the layout of a feature: unknown RoI size

    //Seek to the next line
    file.ignore(numeric_limits<streamsize>::max(), '\n');

    //Read the first feature
    stringstream firstLine;
    file.get(*firstLine.rdbuf());    //Read until \n

    //Tokenize, and count the tokens
    char_separator<char> separator(" ");
    string tokenizerInput=firstLine.str();  //Tokenizer stores a reference to the string, so temporaries do not work
    tokenizer< char_separator<char> > tokens(tokenizerInput, separator);

    size_t nTokens=distance(tokens.begin(), tokens.end());
    int sRoI=nTokens-sDescriptor-2;  //2 for the coordinates, sDescriptor for the descriptor

    if(sRoI<0)
        throw(runtime_error(string("ImageFeatureIOC::ReadFeatureFile: Each line must contain at least sDescriptor+2 elements, not ") + lexical_cast<string>(nTokens)));

    //Allocate memory
    ImageFeatureC masterFeature;
    masterFeature.Descriptor().resize(sDescriptor);
    masterFeature.RoI().resize(sRoI);
    output=vector<ImageFeatureC>(nFeatures, masterFeature);

    //Make the first feature
    firstLine>>output[0].Coordinate()[0];
    firstLine>>output[0].Coordinate()[1];

    typedef typename ImageFeatureC::coordinate_type CoordinateT;
    typedef typename ImageFeatureC::descriptor_type DescriptorT;
    typedef typename ImageFeatureC::roi_type RoIT;

    RoIT& roi=output[0].RoI();
    for(int cRoI=0; cRoI<sRoI; ++cRoI)
        firstLine>>roi[cRoI];

    DescriptorT& descriptor=output[0].Descriptor();
    for(int cDesc=0; cDesc<sDescriptor; ++cDesc)
        firstLine>>descriptor[cDesc];

    //Now, read the remaining features
    vector<size_t> indexRange(nFeatures-1);	//0th feature is already extracted
    iota(indexRange.begin(), indexRange.end(), 1);

    if(flagRandomise)
    	random_shuffle(indexRange.begin(), indexRange.end());

    for(int c=1; c<nFeatures; ++c)
    {
    	size_t index=indexRange[c-1];
        CoordinateT& coordinate=output[index].Coordinate();
        RoIT& roi=output[index].RoI();
        DescriptorT& descriptor=output[index].Descriptor();

        file>>coordinate[0];
        file>>coordinate[1];

        for(int cRoI=0; cRoI<sRoI; ++cRoI)
            file>>roi[cRoI];

        for(int cDesc=0; cDesc<sDescriptor; ++cDesc)
            file>>descriptor[cDesc];
    }   //for(int c=1; c<nFeatures; ++c)

    return output;
}   //vector<ImageFeatureC> ReadFeatureFile(const string& filename)

/*********** BinaryImageFeatureIOC **********/

/**
 * @brief Writes a sample file to illustrate the format
 * @param[in] filename Output filename
 * @throws runtime_error If the file cannot be opened
 */
void BinaryImageFeatureIOC::WriteSampleFile(const string& filename)
{
    ofstream file(filename);

    if(file.fail())
        throw(runtime_error(string("BinaryImageFeatureIOC::WriteSampleFile: Cannot open file at ") + filename));

    cout<<".bft2 file format:"<<"\n"<<"Descriptor size"<<"\n"<<"Number of features"<<"\n"<<"Coordinates(2)"<<" "<<"RoI(3)"<<" "<<"Descriptor(10 bits, 2 block) "<<"\n";

    BinaryImageFeatureC feature;
    feature.Coordinate()<<120, 100;
    feature.RoI().resize(3); feature.RoI()<<0.25, 0.75, 0.1;
    feature.Descriptor()=BinaryImageFeatureC::descriptor_type(10, false); feature.Descriptor()[0]=true; feature.Descriptor()[4]=true; feature.Descriptor()[8]=true;

    file<<10<<"\n"<<1<<"\n"<<feature;
}   //void WriteSampleFile(const string& filename)

/**
 * @brief Reads a descriptor
 * @param[in, out] blocks Descriptor, in blocks. Consumed in the operation
 * @param[in] sDescriptor Descriptor size, in bits
 * @return Descriptor bitset
 * @pre \c floor(sDescriptor/CHAR_BIT) <= \c blocks.size() <= ceil(sDescriptor/CHAR_BIT)
 * @post \c blocks is modified
 */
BinaryImageFeatureC::descriptor_type BinaryImageFeatureIOC::ReadDescriptor(vector<int>& blocks, size_t sDescriptor)
{
	//Preconditions
	assert(floor(sDescriptor/CHAR_BIT) <= blocks.size());
	assert(blocks.size() <= ceil(sDescriptor/CHAR_BIT));

	BinaryImageFeatureC::descriptor_type output(sDescriptor, false);
	if(sDescriptor==0)
		return output;

	int nBlocks=blocks.size();
	size_t iDesc=0;
	for(int c=nBlocks-1; iDesc<sDescriptor; --c)
		for(size_t c2=0; (c2<CHAR_BIT) && (iDesc<sDescriptor); ++c2, ++iDesc, blocks[c]>>=1)
			output[iDesc] = blocks[c] & 1;

	return output;
}	//BinaryImageFeatureC::descriptor_type ReadDescriptor(ifstream& file, size_t sDescriptor, size_t nExcess)

/**
 * @brief Reads a binary image feature from a stream
 * @param[in,out] src Input stream
 * @param[in] sDescriptor Size of the descriptor
 * @param[in] nBlocks Number of blocks
 * @param[in] sRoI Size of the RoI
 * @return A binary image feature
 * @remarks Tested under \c ReadFeatureFile
 */
BinaryImageFeatureC BinaryImageFeatureIOC::ReadFeature(istream& src, size_t sDescriptor, size_t nBlocks, size_t sRoI)
{
	BinaryImageFeatureC output;

	typedef typename BinaryImageFeatureC::coordinate_type CoordinateT;
	typedef typename BinaryImageFeatureC::roi_type RoIT;

	CoordinateT& coordinate=output.Coordinate();
	RoIT& roi=output.RoI();
	roi.resize(sRoI);

	src>>coordinate[0];
	src>>coordinate[1];

    for(size_t cRoI=0; cRoI<sRoI; ++cRoI)
        src>>roi[cRoI];

    vector<int> blocks(nBlocks);
    for(size_t c=0; c<nBlocks; ++c)
        src>>blocks[c];

    output.Descriptor()=ReadDescriptor(blocks, sDescriptor);

	return output;
}	//BinaryImageFeatureC ReadFeature(stringstream& src, size_t sDescriptor, size_t sRoI)

/**
 * @brief Reads a feature file
 * @param[in] filename File to be read in
 * @param[in] flagRandomise If \c true , the order of the features is randomised.
 * @return A vector of image features
 * @remarks Randomisation is important to prevent biases in order-dependent algorithms
 */
vector<BinaryImageFeatureC> BinaryImageFeatureIOC::ReadFeatureFile(const string& filename, bool flagRandomise)
{
	ifstream file(filename);
	if(file.fail())
		throw(runtime_error(string("BinaryImageFeatureIOC::ReadFeatureFile: Cannot open file at ") + filename));

	//Header

	int sDescriptor;
	file>>sDescriptor;

	if(sDescriptor<0)
		throw(runtime_error(string("BinaryImageFeatureIOC::ReadFeatureFile: Descriptor size must be non-negative, not ") + lexical_cast<string>(sDescriptor)));

	int nFeatures;
	file>>nFeatures;

	if(nFeatures<0)
		throw(runtime_error(string("BinaryImageFeatureIOC::ReadFeatureFile: Number of features must be non-negative, not ") + lexical_cast<string>(nFeatures)));

    //Features

    vector<BinaryImageFeatureC> output;
    if(nFeatures==0)
        return output;

    //Determine the layout of a feature: unknown RoI size

    //Seek to the next line
    file.ignore(numeric_limits<streamsize>::max(), '\n');

    //Read the first feature
    stringstream firstLine;
    file.get(*firstLine.rdbuf());    //Read until \n

    //Tokenize, and count the tokens
	char_separator<char> separator(" ");
	string tokenizerInput=firstLine.str();  //Tokenizer stores a reference to the string, so temporaries do not work
	tokenizer< char_separator<char> > tokens(tokenizerInput, separator);

	size_t nTokens=distance(tokens.begin(), tokens.end());

	size_t nBlocks=ceil((float)sDescriptor/CHAR_BIT);	//Number of blocks
    int sRoI=nTokens-nBlocks-2;  //2 for the coordinates, sDescriptor for the descriptor

    if(sRoI<0)
        throw(runtime_error(string("BinaryImageFeatureIOC::ReadFeatureFile: Each line must contain at least ceil(sDescriptor/CHAR_BIT)+2 elements, not ") + lexical_cast<string>(nTokens)));

    //Allocate memory
	BinaryImageFeatureC masterFeature;
	masterFeature.RoI().resize(sRoI);
	output=vector<BinaryImageFeatureC>(nFeatures, masterFeature);

	//Make the first feature
	output[0]=ReadFeature(firstLine, sDescriptor, nBlocks, sRoI);

    //Now, read the remaining features
    vector<size_t> indexRange(nFeatures-1);	//0th feature is already extracted
    iota(indexRange.begin(), indexRange.end(), 1);

    if(flagRandomise)
    	random_shuffle(indexRange.begin(), indexRange.end());

    for(int c=1; c<nFeatures; ++c)
    	output[indexRange[c-1]]=ReadFeature(file, sDescriptor, nBlocks, sRoI);

    return output;
}	//vector<BinaryImageFeatureC> ReadFeatureFile(const string& filename)


/********** EXPLICIT INSTANTIATIONS **********/
template void ImageFeatureIOC::WriteFeatureFile(const vector<ImageFeatureC>&, const string&);

template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<ImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<ImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<ImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<ImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<BinaryImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
template void BaseImageFeatureIOC::PostprocessFeatureSet(vector<BinaryImageFeatureC>&, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);

template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<BinaryImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::projective_transform_type>&);
template void BaseImageFeatureIOC::PreprocessFeatureSet(vector<BinaryImageFeatureC>&, const optional< tuple<ImageFeatureC::coordinate_type, ImageFeatureC::coordinate_type> >&, double, bool, const LensDistortionC&, const optional<CoordinateTransformations2DT::affine_transform_type>&);


}   //ION
}	//SeeSawN
