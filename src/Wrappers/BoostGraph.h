/**
 * @file BoostGraph.h Public interface for wrappers for common Boost.Graph operations
 * @author Evren Imre
 * @date 28 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BOOST_GRAPH_H_3218924
#define BOOST_GRAPH_H_3218924

#include "BoostGraph.ipp"

namespace SeeSawN
{
namespace WrappersN
{
template <class IndexRangeT> vector<list< typename range_value<IndexRangeT>::type > > FindConnectedComponents(const IndexRangeT& indexRange, bool flagFindLargest);	///< Finds the connected components in an index range

/********** EXTERN TEMPLATES **********/
extern template vector<list<pair<unsigned int, unsigned int>> > FindConnectedComponents(const vector<pair<unsigned int, unsigned int>>&, bool);
extern template vector<list<pair<size_t, size_t>> > FindConnectedComponents(const vector<pair<size_t, size_t>>&, bool);

}	//Wrappers
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::list<unsigned int>;
extern template class std::map<unsigned int, unsigned int>;

#endif /* BOOST_GRAPH_H_3218924 */
