/**
 * @file BoostTokenizer.ipp Implementation of Boost.Tokenizer extensions
 * @author Evren Imre
 * @date 7 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_TOKENIZER_IPP_6921323
#define BOOST_TOKENIZER_IPP_6921323

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <vector>
#include <string>
#include <cstddef>
#include <iterator>

namespace SeeSawN
{
namespace WrappersN
{

using boost::lexical_cast;
using boost::tokenizer;
using boost::char_separator;
using std::vector;
using std::string;
using std::size_t;
using std::distance;

/**
 * @brief Tokenises a string and converts the tokens to a specified type
 * @tparam ValueT Target type
 * @param[in] src Source string
 * @param[in] delimiters Delimiter characters
 * @return Vector of tokens
 * @remarks Type conversion utilises \c lexical_cast
 * @ingroup Utility
 */
template<typename ValueT>
vector<ValueT> Tokenise(const string& src, const string& delimiters=" ")
{
	char_separator<char> separators (delimiters.data());
	tokenizer<char_separator<char> > parser(src, separators);

	size_t nToken=distance(parser.begin(), parser.end());
	vector<ValueT> output; output.reserve(nToken);

	for(const auto& current : parser)
		output.emplace_back(lexical_cast<ValueT>(current));

	return output;
}	//vector<ValueT> Tokenize(const string& src, const string& delimiters)

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_TOKENIZER_IPP_6921323 */
