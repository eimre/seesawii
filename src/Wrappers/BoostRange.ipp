/**
 * @file BoostRange.ipp Extensions and wrappers for Boost.Range
 * @author Evren Imre
 * @date 7 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_RANGE_IPP_6309032
#define BOOST_RANGE_IPP_6309032

#include <boost/concept_check.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/iterator/permutation_iterator.hpp>
#include <type_traits>

#include <iostream>
namespace SeeSawN
{
namespace WrappersN
{

using boost::ForwardRangeConcept;
using boost::RandomAccessRangeConcept;
using boost::make_iterator_range;
using boost::iterator_range;
using boost::zip_iterator;
using boost::make_zip_iterator;
using boost::permutation_iterator;
using boost::make_permutation_iterator;
using boost::range_iterator;
using boost::range_const_iterator;
using boost::range_value;
using std::is_integral;

/**
 * @brief Makes a binary zip iterator range
 * @tparam Range1T First range
 * @tparam Range2T Second range
 * @param[in] range1 First range
 * @param[in] range2 Second range
 * @returns An iterator range of zip iterators
 * @pre \c Range1T and \c Range2T are forward ranges
 * @pre \c range1 and \c range2 have the same length
 * @ingroup Utility
 */
template<class Range1T, class Range2T>
iterator_range<zip_iterator<boost::tuple<typename range_iterator<Range1T>::type, typename range_iterator<Range2T>::type> > > MakeBinaryZipRange(Range1T& range1, Range2T& range2)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<Range1T>));
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<Range2T>));
	assert(boost::distance(range1)==boost::distance(range2));

	auto itB=make_zip_iterator(boost::make_tuple(boost::begin(range1), boost::begin(range2)));
	auto itE=make_zip_iterator(boost::make_tuple(boost::end(range1), boost::end(range2)));

	return make_iterator_range(itB, itE);
}	//iterator_range<zip_iterator<boost::tuple<range_iterator<Range1T>::type, range_iterator<Range2T>::type> > > MakeBinaryZipRange(const Range1T& range1, const Range2T& range2);

/**
 * @brief Makes a binary zip iterator range, with constant iterators
 * @tparam Range1T A range
 * @tparam Range2T A range
 * @param[in] range1 First range
 * @param[in] range2 Second range
 * @returns An iterator range of constant zip iterators
 * @pre \c Range1T and \c Range2T are forward ranges
 * @pre \c range1 and \c range2 have the same length
 * @ingroup Utility
 */
template<class Range1T, class Range2T>
iterator_range<zip_iterator<boost::tuple<typename range_const_iterator<Range1T>::type, typename range_const_iterator<Range2T>::type> > > MakeBinaryZipRange(const Range1T& range1, const Range2T& range2)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<Range1T>));
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<Range2T>));
	assert(boost::distance(range1)==boost::distance(range2));

	auto itB=make_zip_iterator(boost::make_tuple(boost::const_begin(range1), boost::const_begin(range2)));
	auto itE=make_zip_iterator(boost::make_tuple(boost::const_end(range1), boost::const_end(range2)));
	return make_iterator_range(itB, itE);
}	//iterator_range<zip_iterator<boost::tuple<range_const_iterator<Range1T>::type, range_const_iterator<Range2T>::type> > > MakeConstantBinaryZipRange(const Range1T& range1, const Range2T& range2)

/**
 * @brief Makes a permutation range
 * @tparam ElementRangeT An element range
 * @tparam IndexRangeT An index range
 * @param[in] elementRange Element range
 * @param[in] indexRange Index range
 * @return An iterator range of permutation iterators
 * @pre \c ElementRangeT is a random access range
 * @pre \c IndexRangeT is a random access range of integral values
 */
template<class ElementRangeT, class IndexRangeT>
iterator_range<permutation_iterator<typename range_iterator<ElementRangeT>::type, typename range_iterator<IndexRangeT>::type> > MakePermutationRange(ElementRangeT& elementRange, IndexRangeT& indexRange)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<ElementRangeT>));
	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<IndexRangeT>));
	static_assert(is_integral<typename range_value<IndexRangeT>::type>::value, "MakePermutationRange: IndexRangeT must hold elements of integral type");

	auto itB=make_permutation_iterator(boost::begin(elementRange), boost::begin(indexRange));
	auto itE=make_permutation_iterator(boost::end(elementRange), boost::end(indexRange));
	return make_iterator_range(itB, itE);
}	//iterator_range<permutation_iterator<range_iterator<ElementRangeT>::type, range_iterator<IndexRangeT>::type> > MakePermutationRange(const ElementRangeT& elementRange, const IndexRangeT& indexRange)

/**
 * @brief Makes a permutation range, with constant iterators
 * @tparam ElementRangeT An element range
 * @tparam IndexRangeT An index range
 * @param[in] elementRange Element range
 * @param[in] indexRange Index range
 * @return An iterator range of constant permutation iterators
 * @pre \c ElementRangeT is a random access range
 * @pre \c IndexRangeT is a random access range of integral values
 */
template<class ElementRangeT, class IndexRangeT>
iterator_range<permutation_iterator<typename range_const_iterator<ElementRangeT>::type, typename range_const_iterator<IndexRangeT>::type> > MakePermutationRange(const ElementRangeT& elementRange, const IndexRangeT& indexRange)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<ElementRangeT>));
	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<IndexRangeT>));
	static_assert(is_integral<typename range_value<IndexRangeT>::type>::value, "MakeConstPermutationRange: IndexRangeT must hold elements of integral type");

	auto itB=make_permutation_iterator(boost::const_begin(elementRange), boost::const_begin(indexRange));
	auto itE=make_permutation_iterator(boost::const_end(elementRange), boost::const_end(indexRange));
	return make_iterator_range(itB, itE);
}	//iterator_range<permutation_iterator<range_const_iterator<ElementRangeT>::type, range_const_iterator<IndexRangeT>::type> > MakeConstPermutationRange(const ElementRangeT& elementRange, const IndexRangeT& indexRange)

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_RANGE_IPP_6309032 */
