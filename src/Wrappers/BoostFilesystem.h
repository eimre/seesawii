/**
 * @file BoostFilesystem.h Public interface for Boost.Filesystem extensions
 * @author Evren Imre
 * @date 9 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_FILESYSTEM_H_6793212
#define BOOST_FILESYSTEM_H_6793212

#include "BoostFilesystem.ipp"
namespace SeeSawN
{
namespace WrappersN
{

bool IsWriteable(const string& pathString, bool flagFilename=false);	///< Checks whether a path is a valid location for writing

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_FILESYSTEM_H_6793212 */
