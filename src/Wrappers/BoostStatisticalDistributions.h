/**
 * @file BoostStatisticalDistributions.h Public interface for Boost.Statistical Distributions wrappers
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_STATISTICAL_DISTRIBUTIONS_H_5643256
#define BOOST_STATISTICAL_DISTRIBUTIONS_H_5643256

#include "BoostStatisticalDistributions.ipp"
#include <boost/math/distributions/cauchy.hpp>

namespace SeeSawN
{
namespace WrappersN
{

template<class DensityT> class PDFWrapperC;

}   //WrappersN
}	//SeeSawN

#endif /* BOOST_STATISTICAL_DISTRIBUTIONS_H_5643256 */
