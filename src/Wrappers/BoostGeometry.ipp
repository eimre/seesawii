/**
 * @file BoostGeometry.ipp Implementation details for the wrapper functions for Boost.Geometry
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_GEOMETRY_IPP_5120941
#define BOOST_GEOMETRY_IPP_5120941

#include <boost/geometry/geometry.hpp>
#include "EigenMetafunction.h"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace WrappersN
{

using boost::geometry::model::point;
using boost::geometry::cs::cartesian;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_GEOMETRY_IPP_5120941 */
