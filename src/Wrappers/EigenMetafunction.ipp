/**
 * @file EigenMetafunction.ipp Implementation of various Eigen metafunctions
 * @author Evren Imre
 * @date 29 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef EIGEN_METAFUNCTION_IPP_1383654
#define EIGEN_METAFUNCTION_IPP_1383654

#include <type_traits>

namespace SeeSawN
{
namespace WrappersN
{

using std::remove_pointer;

/**
 * @brief Extracts the value type from an Eigen matrix or array
 * @tparam ObjectT An eigen matrix or array
 * @pre \c ObjectT has the member \c data() returning a pointer to the underlying data array (unenforced)
 * @ingroup Utility
 */
template<class MatrixT>
struct ValueTypeM
{
    typedef typename remove_pointer<decltype( (new MatrixT)->data() )>::type type;	///< Value type
};  //struct ExtractValueTypeM

/**
 * @brief A metafunction returning the number of compile-time rows of an Eigen dense expression
 * @tparam DenseT An Eigen dense expression
 * @pre \c DenseT is an Eigen dense expression (unenforced)
 * @warning An Eigen vector is an Nx1 (and not 1xN) matrix
 * @ingroup Utility
 */
template<class DenseExpressionT>
struct RowsM
{
    constexpr static int value=DenseExpressionT::RowsAtCompileTime;	///< Number of rows at compile type
};  //struct RowsM

/**
 * @brief A metafunction returning the number of compile-time columns of an Eigen dense expression
 * @tparam DenseT An Eigen dense expression
 * @pre \c DenseT is an Eigen dense expression (unenforced)
 * @warning An Eigen vector is an Nx1 (and not a 1xN) matrix
 * @ingroup Utility
 */
template<class DenseExpressionT>
struct ColsM
{
    constexpr static int value=DenseExpressionT::ColsAtCompileTime;	///< Number of columns at compile time
};  //struct RowsM


}   //WrappersN
}	//SeeSawN

#endif /* EIGEN_METAFUNCTION_IPP_1383654 */
