/**
 * @file TestWrappers.cpp Unit tests for Wrappers
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE WRAPPERS

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/math/distributions/cauchy.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/geometry.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <utility>
#include <vector>
#include <tuple>
#include <utility>
#include <string>
#include <iostream>
#include "BoostStatisticalDistributions.h"
#include "BoostBimap.h"
#include "BoostDynamicBitset.h"
#include "EigenExtensions.h"
#include "BoostTokenizer.h"
#include "BoostFilesystem.h"
#include "BoostRange.h"
#include "BoostGeometry.h"
#include "BoostGraph.h"
#include "../Geometry/Projection32.h"
#include "../Elements/GeometricEntity.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestWrappersN
{

using namespace SeeSawN::WrappersN;
using boost::math::pdf;
using boost::math::cauchy_distribution;
using boost::bimap;
using boost::bimaps::with_info;
using boost::geometry::equals;
using boost::to_string;
using boost::equal;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::Matrix3d;
using Eigen::Vector3d;
using Eigen::Matrix;
using std::make_pair;
using std::string;
using std::cout;
using std::get;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::ElementsN::FrameT;

BOOST_AUTO_TEST_SUITE(Boost_Statistical_Distributions)

BOOST_AUTO_TEST_CASE(PDFWrappers)
{
    typedef PDFWrapperC< cauchy_distribution<> > PDFWrapperT;

    //Default constructor
    PDFWrapperT cauchy1;
    BOOST_CHECK_CLOSE(cauchy1(0.25), pdf(cauchy_distribution<double>(), 0.25), 1e-16 );

    cauchy_distribution<double> density2(0.25, 2);
    PDFWrapperT cauchy2(density2);
    BOOST_CHECK_CLOSE(cauchy2(0.25), pdf(density2, 0.25), 1e-16 );
}   //BOOST_AUTO_TEST_CASE(PDFWrappers)

BOOST_AUTO_TEST_SUITE_END() //Boost_Statistical_Distributions

BOOST_AUTO_TEST_SUITE(Boost_Bimap)

BOOST_AUTO_TEST_CASE(Bimap_To_Vector)
{
    typedef bimap<list_of<size_t>, list_of<size_t>, left_based> BimapT;
    BimapT bm;
    bm.left.push_back(make_pair(1,4));
    bm.left.push_back(make_pair(2,1));

    tuple<vector<size_t>, vector<size_t> > output=BimapToVector<size_t, size_t>(bm);

    BOOST_CHECK_EQUAL(get<0>(output)[0], (size_t)1);
    BOOST_CHECK_EQUAL(get<0>(output)[1], (size_t)2);
    BOOST_CHECK_EQUAL(get<1>(output)[0], (size_t)4);
    BOOST_CHECK_EQUAL(get<1>(output)[1], (size_t)1);
    BOOST_CHECK_EQUAL(get<0>(output).size(), (size_t)2);
    BOOST_CHECK_EQUAL(get<1>(output).size(), (size_t)2);

    //with info

    typedef bimap<list_of<size_t>, list_of<size_t>, left_based, with_info<double> > BimapIT;
    BimapIT bmI;
    bmI.push_back(BimapIT::value_type(1,4,0.1));
    bmI.push_back(BimapIT::value_type(2,1,0.2));
    tuple<vector<size_t>, vector<size_t>, vector<double> > outputI=BimapToVector<size_t, size_t, double>(bmI);

    BOOST_CHECK_EQUAL(get<0>(outputI)[0], (size_t)1);
    BOOST_CHECK_EQUAL(get<0>(outputI)[1], (size_t)2);
    BOOST_CHECK_EQUAL(get<1>(outputI)[0], (size_t)4);
    BOOST_CHECK_EQUAL(get<1>(outputI)[1], (size_t)1);
    BOOST_CHECK_EQUAL(get<2>(outputI)[0], 0.1);
	BOOST_CHECK_EQUAL(get<2>(outputI)[1], 0.2);
    BOOST_CHECK_EQUAL(get<0>(outputI).size(), (size_t)2);
    BOOST_CHECK_EQUAL(get<1>(outputI).size(), (size_t)2);
}   //BOOST_AUTO_TEST_CASE(Bimap_To_Vector)

BOOST_AUTO_TEST_CASE(Vector_To_Bimap)
{
	vector<size_t> leftVector{1,2};
	vector<size_t> rightVector{4,1};
	vector<double> infoVector{0.1, 0.2};

	typedef bimap<list_of<size_t>, list_of<size_t>, left_based > BimapT;
	BimapT bimap1=VectorToBimap<BimapT>(leftVector, rightVector);
	BOOST_CHECK_EQUAL(bimap1.begin()->left, 1);
	BOOST_CHECK_EQUAL(bimap1.begin()->right, 4);
	BOOST_CHECK_EQUAL(bimap1.size(),2);

	typedef bimap<list_of<size_t>, list_of<size_t>, left_based, with_info<double> > BimapIT;
	BimapIT bimap2=VectorToBimap<BimapIT>(leftVector, rightVector, infoVector);
	BOOST_CHECK_EQUAL(bimap2.begin()->left, 1);
	BOOST_CHECK_EQUAL(bimap2.begin()->right, 4);
	BOOST_CHECK_EQUAL(bimap2.begin()->info, 0.1);
	BOOST_CHECK_EQUAL(bimap2.size(),2);

}	//BOOST_AUTO_TEST_CASE(Vector_To_Bimap)

BOOST_AUTO_TEST_CASE(Filter_Bimap)
{
	vector<size_t> index{0, 2, 3};

	typedef bimap<list_of<size_t>, list_of<size_t>, left_based> BimapT;
	BimapT bm;
	bm.left.push_back(make_pair(1,4));
	bm.left.push_back(make_pair(2,1));
	bm.left.push_back(make_pair(3,2));

	BimapT filtered=FilterBimap(bm, index);

	BOOST_CHECK_EQUAL(filtered.size(), (size_t)2);
	BOOST_CHECK_EQUAL(filtered.begin()->left, 1);
	BOOST_CHECK_EQUAL(filtered.begin()->right, 4);
	BOOST_CHECK_EQUAL(filtered.rbegin()->left, 3);
	BOOST_CHECK_EQUAL(filtered.rbegin()->right, 2);

	//SplitBimap
	BimapT split1;
	BimapT split2;
	std::tie(split1, split2)=SplitBimap(bm, index);

	BOOST_CHECK_EQUAL(split1.size(), (size_t)2);
	BOOST_CHECK_EQUAL(split2.size(), (size_t)1);
	BOOST_CHECK( *split1.begin() == *filtered.begin());
	BOOST_CHECK( *split1.rbegin() == *filtered.rbegin());
	BOOST_CHECK_EQUAL( split2.begin()->left, 2);
	BOOST_CHECK_EQUAL( split2.begin()->right, 1);
}	//BOOST_AUTO_TEST_CASE(Filter_Bimap)

BOOST_AUTO_TEST_SUITE_END()  //Boost_Bimap

BOOST_AUTO_TEST_SUITE(Boost_Range)

BOOST_AUTO_TEST_CASE(Range_Wrappers)
{
	vector<double> rangeA{0,1,2,3,4,5,6};
	vector<double> rangeB{6,5,4,3,2,1};

	vector<size_t> indexRangeA{0,2,4};
	vector<size_t> indexRangeB{1,3,5};

	//Zip range
	auto zipRange1=MakeBinaryZipRange(indexRangeA, indexRangeB);
	BOOST_CHECK_EQUAL(zipRange1.begin()->get<0>(),0);
	BOOST_CHECK_EQUAL(zipRange1.begin()->get<1>(),1);
	BOOST_CHECK_EQUAL(zipRange1.size(), 3);

	//Permutation range
	auto permutationRange2=MakePermutationRange(rangeA, indexRangeB);
	BOOST_CHECK_EQUAL(*permutationRange2.begin(), 1);
	BOOST_CHECK_EQUAL(permutationRange2.size(), 3);
}	//BOOST_AUTO_TEST_CASE(Range_Wrappers)

BOOST_AUTO_TEST_SUITE_END()  //Boost_Range

BOOST_AUTO_TEST_SUITE(Boost_Tokenizer)

BOOST_AUTO_TEST_CASE(Tokenizer)
{
	string src("1 2 3");
	vector<double> v=Tokenise<double>(src, string(" ") );
	vector<double> vr{1,2,3};
	BOOST_CHECK_EQUAL_COLLECTIONS(v.begin(), v.end(), vr.begin(), vr.end());

	vector<double> v2=Tokenise<double>(src);
	BOOST_CHECK_EQUAL_COLLECTIONS(v2.begin(), v2.end(), vr.begin(), vr.end());
}	//BOOST_AUTO_TEST_CASE(Tokenizer)

BOOST_AUTO_TEST_SUITE_END()  //Boost_Tokenizer

BOOST_AUTO_TEST_SUITE(Boost_Filesystem)

BOOST_AUTO_TEST_CASE(Path_Check)
{
	//Write a test file
	if(getenv("SEESAW_TEST_DIR")!=nullptr)
	{
		string pathname(getenv("SEESAW_TEST_DIR"));

		BOOST_CHECK(IsWriteable(pathname));
		BOOST_CHECK(IsWriteable(pathname, false));

		string pathname2=pathname+"INVALID";
		BOOST_CHECK(!IsWriteable(pathname2));

		string filename=pathname+"/tst.txt";
		BOOST_CHECK(IsWriteable(filename, true));
		BOOST_CHECK(!IsWriteable(filename, false));
	}	//if(getenv("SEESAW_TEMP_PATH")!=nullptr)
	else
		cout<<"Boost_Filesystem::Path_Check : Path variable not found. Try again with ctest\n";

}	//BOOST_AUTO_TEST_CASE(Path_Check)

BOOST_AUTO_TEST_SUITE_END()  //Boost_Filesystem

BOOST_AUTO_TEST_SUITE(Boost_Geometry)

BOOST_AUTO_TEST_CASE(Boost_Geometry_Functions)
{

	//MakePoint
	typedef point< ValueTypeM<Coordinate2DT>::type, 2, cartesian> Point2DT;
	Point2DT p2D=MakePoint(Coordinate2DT(0,1));
	BOOST_CHECK(equals(p2D,Point2DT(0,1)));

	typedef point< ValueTypeM<Coordinate2DT>::type, 3, cartesian> Point3DT;
	Point3DT p3D=MakePoint(Coordinate3DT(0,1,2));
	BOOST_CHECK(equals(p3D,Point3DT(0,1,2)));

	//MakeCoordinate
	BOOST_CHECK(Coordinate2DT(0,1)==MakeCoordinate(p2D));
	BOOST_CHECK(Coordinate3DT(0,1,2)==MakeCoordinate(p3D));

	//Lexical ordering
	BOOST_CHECK(p2D<Point2DT(0,2));
	BOOST_CHECK(p2D<Point2DT(1,0));
	BOOST_CHECK(!(p2D<Point2DT(0,0)));

	BOOST_CHECK(p3D<Point3DT(0,2,0));
	BOOST_CHECK(p3D<Point3DT(1,0,0));
	BOOST_CHECK(!(p3D<Point3DT(0,0,1)));
}	//BOOST_AUTO_TEST_CASE(Boost_Geometry_Functions)

BOOST_AUTO_TEST_SUITE_END()  //Boost_Geometry

BOOST_AUTO_TEST_SUITE(Boost_Dynamic_Bitset)

BOOST_AUTO_TEST_CASE(Boost_Dynamic_Bitset_Functions)
{
	typedef dynamic_bitset<> BitsetT;

	set<unsigned int> trueBits{0,2,4};
	BitsetT output=MakeBitset< BitsetT >(trueBits, 5);

	BOOST_CHECK(output.to_ulong()==21);

	//short to ulong
	vector<unsigned int> inputRange2{64,12311};
	dynamic_bitset<unsigned long> output2=MakeBitset<dynamic_bitset<unsigned long> >(inputRange2);
	dynamic_bitset<unsigned int> outputR2(inputRange2.begin(), inputRange2.end());

	string outputString2;
	to_string(output2, outputString2);

	string outputStringR2;
	to_string(outputR2, outputStringR2);

	BOOST_CHECK(!outputString2.compare(outputStringR2));

	//ulong to short

	vector<unsigned long> inputRange3{64,12311};
	dynamic_bitset<unsigned short> output3=MakeBitset<dynamic_bitset<unsigned short> >(inputRange3);
	dynamic_bitset<unsigned long> outputR3(inputRange3.begin(), inputRange3.end());

	string outputString3;
	to_string(output3, outputString3);

	string outputStringR3;
	to_string(outputR3, outputStringR3);

	BOOST_CHECK(!outputString3.compare(outputStringR3));

	//ulong to ulong
	dynamic_bitset<unsigned long> output4=MakeBitset<dynamic_bitset<unsigned long> >(inputRange3);
	BOOST_CHECK(output4==outputR3);
}	//BOOST_AUTO_TEST_CASE(Boost_Dynamic_Bitset_Functions)

BOOST_AUTO_TEST_SUITE_END()	//Boost_Dynamic_Bitset

BOOST_AUTO_TEST_SUITE(Boost_Graph)

BOOST_AUTO_TEST_CASE(Find_Connected_Components)
{
	//Find connected components

	vector<pair<size_t, size_t> > src{ {2,3}, {2,1}, {1,3}, {4,5}, {4,6}, {9,9} };
	vector<list<pair<size_t, size_t> > > components=FindConnectedComponents(src);

	vector<list<pair<size_t, size_t> > > gtComponents={ list<pair<size_t, size_t>>{{2,3}, {2,1}, {1,3}},
															list<pair<size_t, size_t>>{{4,5}, {4,6}},
															list<pair<size_t, size_t>>{ {9,9} }  };

	BOOST_CHECK_EQUAL(components.size(), 3);
	for(size_t c=0; c<3; ++c)
		BOOST_CHECK(equal(components[c], gtComponents[c]) );

	//Find the largest component
	vector<list<pair<size_t, size_t> > > largestComponent=FindConnectedComponents(src, true);
	BOOST_CHECK_EQUAL(largestComponent.size(), 1);
	BOOST_CHECK(equal(largestComponent[0], gtComponents[0]) );
}	//FindConnectedComponents

BOOST_AUTO_TEST_SUITE_END()	//Boost_Graph

BOOST_AUTO_TEST_SUITE(Eigen_Extensions)

BOOST_AUTO_TEST_CASE(Eigen_Matrix)
{
	//Reshape

	VectorXd v1(6); v1<<1, 2, 3, 4, 5, 6;
	Matrix<double, 2, 3> reshaped1=Reshape<Matrix<double,2,3> >(v1, 2, 3);

	Matrix<double, 2, 3> ref1; ref1<<1, 2, 3, 4, 5, 6;
	BOOST_CHECK(reshaped1==ref1);

	Matrix<double, 2, 4> m2; m2<<1,2,3,4,5,6,7,8;
	MatrixXd reshaped2=Reshape<MatrixXd>(m2,4,2);

	MatrixXd ref2(4,2); ref2<<1,2,3,4,5,6,7,8;
	BOOST_CHECK(reshaped2==ref2);

	VectorXd reshaped3=Reshape<MatrixXd>(reshaped2, 8, 1);
	VectorXd ref3(8); ref3<<1,2,3,4,5,6,7,8;
	BOOST_CHECK(ref3==reshaped3);

	BOOST_CHECK(reshaped3==Reshape<VectorXd>(reshaped3,8,1));

	//MakeCrossProductMatrix

	Vector3d vt1(1,2,3);
	Vector3d vt2(4,5,6);

	Matrix3d mTx=MakeCrossProductMatrix(vt1);
	BOOST_CHECK( (mTx*vt2).eval().isApprox( vt1.cross(vt2), 1e-6) );

	//EigenLexicalCompareC

	EigenLexicalCompareC eigenLess;
	BOOST_CHECK(eigenLess(vt1, vt2));
	BOOST_CHECK(!eigenLess(vt2, vt1));
	BOOST_CHECK(!eigenLess(vt1, vt1));
	BOOST_CHECK(eigenLess(v1, ref3));

}	//BOOST_AUTO_TEST_CASE(Eigen_Matrix)

BOOST_AUTO_TEST_CASE(Eigen_Geometry)
{
	Quaternion<double> q(1, 0, 1, 2);
	Matrix<double, 4, 1> v=QuaternionToVector(q);

	BOOST_CHECK( (v==Matrix<double, 4, 1>(1,0,1,2)) );

}	//BOOST_AUTO_TEST_CASE(Eigen_Geometry)

BOOST_AUTO_TEST_SUITE_END()  //Eigen_Extensions


}   //TestWrappersN
}   //UnitTestsN
}	//SeeSawN


