/**
 * @file BoostStatisticalDistributions.ipp Implementation of  Boost.Statistical Distributions wrappers
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_STATISTICAL_DISTRIBUTIONS_IPP_9752596
#define BOOST_STATISTICAL_DISTRIBUTIONS_IPP_9752596

#include <stdexcept>

namespace SeeSawN
{
namespace WrappersN
{

using std::logic_error;

/**
 * @brief Evaluates a PDF at a given value
 * @pre \c DensityT is a Boost.Statistical Distributions distribution (unenforced)
 * @remarks Adaptable unary function interface for Boost.Statistical Distributions library elements
 * @remarks The class is a model of adaptable unary function concept and InvertibleUnaryFunctionConceptC. However, the generic implementation assumes a non-invertible PDF.
 * @ingroup Utility
 * @nosubgrouping
 */
template<class DensityT>
class PDFWrapperC
{
    private:

        typedef typename DensityT::value_type ValueT;  ///< Value type

        /** @name Configuration */ //@{
        DensityT density; ///< PDF to be evaluated
        //@}

    public:

        /** @name Constructors */ //@{
        PDFWrapperC();  ///< Default constructor
        explicit PDFWrapperC(const DensityT& ddensity);  ///< Constructor
        //@}

        /** @name Adaptable unary function interface */ //@{
        ValueT operator()(ValueT x) const; ///< Evaluates the pdf at the given value
        typedef ValueT argument_type;
        typedef ValueT result_type;
        //@}

        /** @name Invertible unary function interface */ //@{
        static ValueT Invert(ValueT x);	///< Dummy function for satisfying InvertibleUnaryFunctionConceptC
        //@}
};  //class PDFWrapperC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post \c density is constructed with the default parameters
 */
template<class DensityT>
PDFWrapperC<DensityT>::PDFWrapperC()
{}

/**
 * @brief Constructor
 * @param[in] ddensity PDF
 */
template<class DensityT>
PDFWrapperC<DensityT>::PDFWrapperC(const DensityT& ddensity) : density(ddensity)
{}

/**
 * @brief Evaluates the pdf at the given value
 * @param[in] x Value
 * @return pdf(x)
 */
template<class DensityT>
auto PDFWrapperC<DensityT>::operator()(ValueT x) const ->ValueT
{
    return pdf(density, x);
}   //value_type operator()(value_type x)

/**
 * @brief Dummy function for satisfying InvertibleUnaryFunctionConceptC
 * @param[in] x Input
 * @return Always 0
 * @throws Always
 * @remarks Not tested
 * @warning This function is not meant to be called. It is there only to satisfy InvertibleUnaryFunctionConceptC
 */
template<class DensityT>
auto PDFWrapperC<DensityT>::Invert(ValueT x) -> ValueT
{
	throw(logic_error("PDFWrapperC::Invert : The generic implementation assumes a non-invertible PDF."));
	return 0;
}	//auto Invert(ValueT x) -> ValueT

}   //WrappersN
}	//SeeSawN

#endif /* BOOST_STATISTICAL_DISTRIBUTIONS_IPP_9752596 */
