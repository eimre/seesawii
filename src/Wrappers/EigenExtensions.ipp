/**
 * @file EigenExtensions.ipp Implementations for various extensions to Eigen
 * @author Evren Imre
 * @date 11 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef EIGEN_EXTENSIONS_IPP_5871232
#define EIGEN_EXTENSIONS_IPP_5871232

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <cstddef>
#include <cassert>
#include <cmath>
#include <type_traits>
#include <limits>
#include "EigenMetafunction.h"

namespace SeeSawN
{
namespace WrappersN
{

using Eigen::Matrix;
using Eigen::Quaternion;
using std::size_t;
using std::min;
using std::is_arithmetic;
using std::numeric_limits;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Lexical comparison for two dense Eigen objects
 * @remarks Implements \c less functor for Eigen
 */
struct EigenLexicalCompareC
{
	template<class DenseT> bool operator()(const DenseT& op1, const DenseT& op2) const;	///< Compares two dense eigen objects
};	//struct EigenLexicalCompareC

/********** IMPLEMENTATION STARTS HERE **********/

/********** EigenLexicalCompareC **********/

/**
 * @brief Compares two dense eigen objects
 * @tparam DenseT An Eigen dense object
 * @param[in] op1 First operand
 * @param[in] op2 Second operand
 * @return \c true if \c op1 is smaller than \c op2
 * @pre \c DenseT supports the Eigen dense object interface
 */
template<class DenseT>
bool EigenLexicalCompareC::operator()(const DenseT& op1, const DenseT& op2) const
{
	size_t nElements1=op1.size();
	size_t nElements2=op2.size();

	//If not equal, shorter array is smaller
	if(nElements1!=nElements2 || (nElements1==0 && nElements2==0) )
		return nElements1<nElements2;

	//Equality may not be recognised by an ordinary comparison, due to numerical errors
	typename ValueTypeM<DenseT>::type zero=1e6*numeric_limits< typename ValueTypeM<DenseT>::type >::epsilon();

	//Equality check with zero vectors
	if(op1.isApproxToConstant(0, zero) || op2.isApproxToConstant(0, zero))
	{
		if(!op1.isMuchSmallerThan(op2, zero) && !op2.isMuchSmallerThan(op1))
			return false;
	}
	else
		if(op1.isApprox(op2, zero))	//Fuzzy equality check
			return false;

	return *op1.data() < *op2.data();	//If equality is ruled out, comparing the first element is sufficient
}	//bool operator()(const DenseT& op1, const DenseT& op2) const

/********** FREE FUNCTIONS **********/

/**
 * @brief Reshapes a matrix
 * @tparam MatrixST Source matrix type
 * @tparam MatrixRT Destination matrix type
 * @param[in] src Matrix to be reshaped
 * @param[in] nRow New number of rows
 * @param[in] nCol New number of columns
 * @return Reshaped matrix
 * @pre \c MatrixST is an Eigen dense object (unenforced)
 * @pre \c MatrixRT is an Eigen dense object (unenforced)
 * @pre \c nRowxnCol=src.size()
 * @remarks Row-major convention. This results in inefficient scanning, and slower performance in large matrices
 * @ingroup Utility
 */
template<class MatrixRT, class MatrixST>
MatrixRT Reshape(const MatrixST& src, size_t nRow, size_t nCol)
{
	//Preconditions
	assert(nRow*nCol==src.size());

	MatrixRT output(nRow, nCol);

	size_t nColsO=src.cols();
	size_t c3=0;
	size_t c4=0;
	for(size_t c1=0; c1<nRow; ++c1)
		for(size_t c2=0; c2<nCol; ++c2, ++c4)
		{
			if(c4==nColsO)
			{
				c4=0;
				++c3;
			}	//if(c3==nColsO)

			output(c1,c2)=src(c3,c4);
		}	//for(size_t c2=0; c2<nRow; ++c2, ++c3)

	return output;
}	//MatrixRT Reshape(const MatrixST& src)

/**
 * @brief Matrix performing the cross-product operation with a 3D vector
 * @tparam ValueT Type of the cells
 * @param[in] coordinate lhs operator of the cross product
 * @return Matrix whose lhs multiplication with a vector is equivalent to the cross-product operation
 * @pre \c ValueT is an arithmetic type
 * @ingroup Utility
 */
template<typename ValueT>
Matrix<ValueT, 3, 3> MakeCrossProductMatrix(const Matrix<ValueT,3,1>& coordinate)
{
	//Preconditions
	static_assert(is_arithmetic<ValueT>::value, "MakeCrossProductMatrix: ValueT must be an arithmetic value.");

	Matrix<ValueT, 3, 3> output;
	output(0,0)=0; 			   output(0,1)=-coordinate(2); output(0,2)= coordinate(1);
	output(1,0)= coordinate(2); output(1,1)=0;			 ; output(1,2)=-coordinate(0);
	output(2,0)=-coordinate(1); output(2,1)=coordinate(0); output(2,2)=0;

	return output;
}	//Matrix<ValueT, 3, 3> MakeCrossProductMatrix(const Matrix<ValueT,3,1>& coordinate)

/**
 * @brief Converts a quaternion to a vector
 * @tparam ValueT Value type
 * @param[in] q Quaternion
 * @return Vector corresponding to the quaternion
 * @ingroup Utility
 */
template<typename ValueT>
Matrix<ValueT, 4, 1> QuaternionToVector(const Quaternion<ValueT>& q)
{
	return Matrix<ValueT,4,1>(q.w(), q.x(), q.y(), q.z());
}	//Matrix<ValueT, 4, 1> QuaternionToVector(const Quaternion<ValueT>& q)


}	//WrapperN
}	//SeeSawN

#endif /* EIGEN_EXTENSIONS_IPP_5871232 */
