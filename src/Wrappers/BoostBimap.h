/**
 * @file BoostBimap.h
 * @author Evren Imre
 * @date 27 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_BIMAP_H_3132565
#define BOOST_BIMAP_H_3132565

#include "BoostBimap.ipp"
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <list>

namespace SeeSawN
{
namespace WrappersN
{

template<class Data1T, class Data2T, class BimapT> tuple<vector<Data1T>, vector<Data2T> > BimapToVector(const BimapT& input);   ///< Copies the left and right maps into vectors
template<class Data1T, class Data2T, class InfoT, class BimapT> tuple<vector<Data1T>, vector<Data2T>, vector<InfoT> > BimapToVector(const BimapT& input);   ///< Copies the left and right maps into vectors
template<class BimapT, class DataRange1T, class DataRange2T> BimapT VectorToBimap(const DataRange1T& range1, const DataRange2T& range2);	///< Makes a bimap from vectors
template<class BimapT, class DataRange1T, class DataRange2T, class InfoRangeT> BimapT VectorToBimap(const DataRange1T& range1, const DataRange2T& range2, const InfoRangeT& rangeI);	///< Makes a bimap from vectors

template<class BimapT, class IndexRangeT> BimapT FilterBimap(const BimapT& input, const IndexRangeT& indexRange);	///< Filters a bimap by removing the entries missing in the index range
template<class BimapT, class IndexRangeT> tuple<BimapT, BimapT> SplitBimap(const BimapT& input, const IndexRangeT& indexRange);	///< Splits a bimap into two, where one contains the elements contained by, and the other, missing in the index range


/********** EXTERN TEMPLATES **********/

using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
extern tuple<vector<size_t>, vector<size_t> > BimapToVector(const bimap<list_of<size_t>, list_of<size_t>, left_based>& );

using std::list;
extern template bimap<list_of<size_t>, list_of<size_t>, left_based> FilterBimap(const bimap<list_of<size_t>, list_of<size_t>, left_based>&, const list<size_t>& );

}   //WrappersN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template class std::tuple<std::vector<std::size_t>, std::vector<std::size_t> >;
#endif /* BOOST_BIMAP_H_3132565 */
