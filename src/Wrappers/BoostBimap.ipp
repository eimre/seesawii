/**
 * @file BoostBimap.ipp
 * @author Evren Imre
 * @date 27 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOSTBIMAP_IPP_9132532
#define BOOSTBIMAP_IPP_9132532

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/tuple/tuple.hpp>
#include <vector>
#include <set>
#include <tuple>
#include <cstddef>
#include <type_traits>
#include <iterator>
#include <cmath>

namespace SeeSawN
{
namespace WrappersN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::range_const_iterator;
using boost::make_zip_iterator;
using boost::zip_iterator;
using std::is_unsigned;
using std::is_integral;
using std::vector;
using std::set;
using std::tuple;
using std::get;
using std::size_t;
using std::advance;
using std::distance;
using std::is_convertible;
using std::min;

/**
 * @brief Copies the left and right maps into vectors
 * @tparam Data1T First data type
 * @tparam Data2T Second data type
 * @tparam BimapT A bimap
 * @param[in] input Input bimap
 * @return A tuple of two vectors, corresponding to the left and right collections
 * @pre \c BimapT is a Boost bimap (unenforced)
 * @pre \c BimapT::left_key_type is convertible to \c Data1T
 * @pre \c BimapT::left_value_type is convertible to \c Data2T
 * @ingroup Utility
 */
template<class Data1T, class Data2T, class BimapT>
tuple<vector<Data1T>, vector<Data2T> > BimapToVector(const BimapT& input)
{
	//Preconditions
	static_assert(is_convertible<typename BimapT::left_key_type, Data1T>::value, "BimapToVector : Incompatible left data type");
	static_assert(is_convertible<typename BimapT::right_key_type, Data2T>::value, "BimapToVector : Incompatible right data type");

    tuple<vector<Data1T>, vector<Data2T> > output;

    size_t nElement=boost::distance(input);
    get<0>(output).reserve(nElement);
    get<1>(output).reserve(nElement);
    for(const auto& current : input)
    {
        get<0>(output).push_back(current.left);
        get<1>(output).push_back(current.right);
    }   //for(const auto& current : correspondences)

    return output;
}   //tuple<vector<Data1T>, vector<Data2T> > BimapToVector(const BimapT& input)

/**
 * @brief Copies the left and right maps into vectors
 * @tparam Data1T First data type
 * @tparam Data2T Second data type
 * @tparam InfoT Info type
 * @tparam BimapT A bimap
 * @param[in] input Input bimap
 * @return A tuple of two vectors, corresponding to the left and right collections
 * @pre \c BimapT is a Boost bimap (unenforced)
 * @pre \c BimapT::left_key_type is convertible to \c Data1T
 * @pre \c BimapT::left_value_type is convertible to \c Data2T
 * @pre \c BimapT::info_type is convertible to \c InfoT
 * @ingroup Utility
 */
template<class Data1T, class Data2T, class InfoT, class BimapT>
tuple<vector<Data1T>, vector<Data2T>, vector<InfoT> > BimapToVector(const BimapT& input)
{
	//Preconditions
	static_assert(is_convertible<typename BimapT::left_key_type, Data1T>::value, "BimapToVector : Incompatible left data type");
	static_assert(is_convertible<typename BimapT::right_key_type, Data2T>::value, "BimapToVector : Incompatible right data type");
	static_assert(is_convertible<typename BimapT::info_type, InfoT>::value, "BimapToVector : Incompatible info type");

    tuple<vector<Data1T>, vector<Data2T>, vector<InfoT> > output;

    size_t nElement=boost::distance(input);
    get<0>(output).reserve(nElement);
    get<1>(output).reserve(nElement);
    get<2>(output).reserve(nElement);
    for(const auto& current : input)
    {
        get<0>(output).push_back(current.left);
        get<1>(output).push_back(current.right);
        get<2>(output).push_back(current.info);
    }   //for(const auto& current : correspondences)

    return output;
}	//tuple<vector<Data1T>, vector<Data2T>, vector<InfoT> > BimapToVector(const BimapT& input)

/**
 * @brief Makes a bimap from vectors
 * @tparam BimapT Output bimap type
 * @tparam DataRange1T Type of the container with the left-side data
 * @tparam DataRange2T Type of the container with the right-side data
 * @param[in] range1 Left data range
 * @param[in] range2 Right data range
 * @return A bimap
 * @pre \c BimapT is a bimap (unenforced)
 * @pre \c DataRange1T is a forward range with elements convertible to \c BimapT::left_key_type
 * @pre \c DataRange2T is a forward range with elements convertible to \c BimapT::right_key_type
 * @remarks The function returns as soon as one of the ranges is exhausted
 * @ingroup Utility
 */
template<class BimapT, class DataRange1T, class DataRange2T>
BimapT VectorToBimap(const DataRange1T& range1, const DataRange2T& range2)
{
	//Preconditions
	static_assert(is_convertible< typename boost::range_value<DataRange1T>::type, typename BimapT::left_key_type>::value, "VectorToBimap : Incompatible left data type" );
	static_assert(is_convertible< typename boost::range_value<DataRange2T>::type, typename BimapT::right_key_type>::value, "VectorToBimap : Incompatible right data type" );
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<DataRange1T>));
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<DataRange2T>));

	BimapT output;

	size_t nElements= min(distance(boost::const_begin(range1), boost::const_end(range1)), distance(boost::const_begin(range2), boost::const_end(range2)));
	auto it1=boost::const_begin(range1);
	auto it2=boost::const_begin(range2);
	for(size_t c=0; c<nElements; ++c, advance(it1,1), advance(it2,1))
		output.insert(output.end(), typename BimapT::value_type(*it1, *it2) );

	return output;
}	//BimapT VectorToBimap(const DataRange1T& range1, const DataRange2T& range2)

/**
 * @brief Makes a bimap from vectors
 * @tparam BimapT Output bimap type
 * @tparam DataRange1T Type of the container with the left-side data
 * @tparam DataRange2T Type of the container with the right-side data
 * @tparam InfoRangeT Type of the container with the info component
 * @param[in] range1 Left data range
 * @param[in] range2 Right data range
 * @param[in] rangeI Info data range
 * @return A bimap
 * @pre \c BimapT is a bimap (unenforced)
 * @pre \c DataRange1T is a forward range with elements convertible to \c BimapT::left_key_type
 * @pre \c DataRange2T is a forward range with elements convertible to \c BimapT::right_key_type
 * @pre \c InfoRangeT is a forward range with elements convertible to \c BimapT::info_type
 * @remarks The function returns as soon as one of the ranges is exhausted
 * @ingroup Utility
 */
template<class BimapT, class DataRange1T, class DataRange2T, class InfoRangeT>
BimapT VectorToBimap(const DataRange1T& range1, const DataRange2T& range2, const InfoRangeT& rangeI)
{
	//Preconditions
	static_assert(is_convertible< typename boost::range_value<DataRange1T>::type, typename BimapT::left_key_type>::value, "VectorToBimap : Incompatible left data type" );
	static_assert(is_convertible< typename boost::range_value<DataRange2T>::type, typename BimapT::right_key_type>::value, "VectorToBimap : Incompatible right data type" );
	static_assert(is_convertible< typename boost::range_value<InfoRangeT>::type, typename BimapT::info_type>::value, "VectorToBimap : Incompatible right data type" );
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<DataRange1T>));
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<DataRange2T>));
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InfoRangeT>));

	BimapT output;

	size_t nElements= min(distance(boost::const_begin(rangeI), boost::const_end(rangeI)),min(distance(boost::const_begin(range1), boost::const_end(range1)), distance(boost::const_begin(range2), boost::const_end(range2))));

	auto it1=boost::const_begin(range1);
	auto it2=boost::const_begin(range2);
	auto itI=boost::const_begin(rangeI);
	for(size_t c=0; c<nElements; ++c, advance(it1,1), advance(it2,1), advance(itI,1))
		output.insert(output.end(), typename BimapT::value_type(*it1, *it2, *itI) );

	return output;
}	//BimapT VectorToBimap(const DataRange1T& range1, const DataRange2T& range2, const InfoRangeT& rangeI)

/**
 * @brief Filters a bimap by removing the entries missing in the index range
 * @tparam BimapT A bimap
 * @tparam IndexRangeT A range of indices
 * @param[in] input Bimap to be filtered
 * @param[in] indexRange Indices of the elements to be retained
 * @returns Filtered bimap
 * @pre \c IndexRangeT is a forward range of unsigned integrals
 * @pre \c BimapT is a Boost bimap (unenforced)
 * @remarks If \c indexRange has elements larger than \c input.size() , they are ignored
 * @ingroup Utility
 */
template<class BimapT, class IndexRangeT>
auto FilterBimap(const BimapT& input, const IndexRangeT& indexRange) ->BimapT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<IndexRangeT>));
	static_assert(is_unsigned<typename range_value<IndexRangeT>::type>::value, "FilterBimap: IndexRangeT::value_type must be unsigned");
	static_assert(is_integral<typename range_value<IndexRangeT>::type>::value, "FilterBimap: IndexRangeT::value_type must be integral");

	BimapT filtered;

	if(boost::empty(indexRange) || input.empty())
		return filtered;

	//Sort the range
	set<size_t> sortedIndex(boost::begin(indexRange), boost::end(indexRange));

	//Clip the indices outside of the range
	size_t sInput=input.size();
	auto itIndex=sortedIndex.begin();
	auto itIndexEnd=sortedIndex.lower_bound(sInput);

	auto itInput=input.begin(); advance(itInput, *itIndex);
	size_t iPrev;
	do
	{
		filtered.insert(filtered.end(), *itInput);

		iPrev=*itIndex;
		advance(itIndex,1);
		if(itIndex==itIndexEnd)
			break;
		else
			advance(itInput, *itIndex - iPrev);

	}while(true);

	return filtered;
}	//void FilterBimap(BimapT& input, const IndexRangeT& indexRange)

/**
 * @brief Splits a bimap into two, where one contains the elements contained by, and the other, missing in the index range
 * @tparam BimapT A bimap
 * @tparam IndexRangeT A range of indices
 * @param[in] input Bimap to be filtered
 * @param[in] indexRange Indices of the elements to be retained
 * @returns A tuple of bimaps. [including; excluding]
 * @pre \c IndexRangeT is a forward range of unsigned integrals
 * @pre \c BimapT is a Boost bimap (unenforced)
 * @remarks If \c indexRange has elements larger than \c input.size() , they are ignored
 * @ingroup Utility
 */
template<class BimapT, class IndexRangeT>
tuple<BimapT, BimapT> SplitBimap(const BimapT& input, const IndexRangeT& indexRange)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<IndexRangeT>));
	static_assert(is_unsigned<typename range_value<IndexRangeT>::type>::value, "SplitBimap: IndexRangeT::value_type must be unsigned");
	static_assert(is_integral<typename range_value<IndexRangeT>::type>::value, "SplitBimap: IndexRangeT::value_type must be integral");

	tuple<BimapT, BimapT> output;

	if(boost::empty(indexRange) || input.empty())
	{
		get<1>(output)=input;
		return output;
	}	//if(boost::empty(indexRange))

	//Sort the range
	set<size_t> sortedIndex(boost::begin(indexRange), boost::end(indexRange));

	//Split
	auto itIndex=sortedIndex.cbegin();
	auto itIndexE=sortedIndex.end();
	size_t c=0;
	for(const auto& current : input)
	{
		if(c==*itIndex && itIndex!=itIndexE)
		{
			get<0>(output).insert( get<0>(output).end(), current);
			advance(itIndex,1);
		}
		else
			get<1>(output).insert( get<1>(output).end(), current);

		++c;
	}	//for(const auto& current : input)

	return output;
}	//tuple<BimapT, BimapT> SplitBimap(const BimapT& input, const IndexRangeT& indexRange)


}   //WrappersN
}	//SeeSawN

#endif /* BOOSTBIMAP_IPP_9132532 */
