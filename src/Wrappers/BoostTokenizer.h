/**
 * @file BoostTokenizer.h Public interface for Boost.Tokenizer extensions
 * @author Evren Imre
 * @date 7 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_TOKENIZER_H_7945312
#define BOOST_TOKENIZER_H_7945312

#include "BoostTokenizer.ipp"
namespace SeeSawN
{
namespace WrappersN
{

template<typename ValueT> vector<ValueT> Tokenise(const string& src, const string& delimiters);	///< Tokenises a string and converts the tokens to a specified type

/********** EXTERN TEMPLATES **********/
extern template vector<double> Tokenise(const string& src, const string& delimiters);
extern template vector<float> Tokenise(const string& src, const string& delimiters);
}	//WrappersN
}	//SeeSawN

#endif /* BOOST_TOKENIZER_H_7945312 */
