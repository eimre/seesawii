/**
 * @file BoostGeometry.cpp Implementations for the wrapper functions for Boost.Geometry
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_GEOMETRY_CPP_1688020
#define BOOST_GEOMETRY_CPP_1688020

#include "BoostGeometry.h"
namespace SeeSawN
{
namespace WrappersN
{

using boost::geometry::get;	//When in the header, confused with std::get

/**
 * @brief Converts a 2D coordinate to a Boost.Geometry point
 * @param[in] source Source
 * @return A 2D Boost.Geometry point
 * @ingroup Utility
 */
point< ValueTypeM<Coordinate2DT>::type, 2, cartesian> MakePoint(const Coordinate2DT source)
{
	return point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>(source[0], source[1]);
}	//point< ValueTypeM<Coordinate2DT>::type, 2, cartesian> MakePoint(const Coordinate2DT source)

/**
 * @brief Converts a 3D coordinate to a Boost.Geometry point
 * @param[in] source Source
 * @return A 3D Boost.Geometry point
 * @ingroup Utility
 */
point< ValueTypeM<Coordinate3DT>::type, 3, cartesian> MakePoint(const Coordinate3DT source)
{
	return point< ValueTypeM<Coordinate2DT>::type, 3, cartesian>(source[0], source[1], source[2]);
}	//point< ValueTypeM<Coordinate3DT>::type, 3, cartesian> MakePoint(const Coordinate3DT source)

/**
 * @brief Converts a 2D Boost.Geometry point to a coordinate
 * @param[in] point Point to be converted
 * @return 2D coordinate corresponding to \c point
 * @ingroup Utility
 */
Coordinate2DT MakeCoordinate(const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& point)
{
	return Coordinate2DT(get<0>(point), get<1>(point));
}	//Coordinate2DT MakeCoordinate(const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& point)

/**
 * @brief Converts a 3D Boost.Geometry point to a coordinate
 * @param[in] point Point to be converted
 * @return 3D coordinate corresponding to \c point
 * @ingroup Utility
 */
Coordinate3DT MakeCoordinate(const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& point)
{
	return Coordinate3DT(get<0>(point), get<1>(point), get<2>(point));
}	//Coordinate3DT MakeCoordinate(const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& point)

/**
 * @brief "less than" operator for 2D cartesian points
 * @param[in] x First operand
 * @param[in] y Second operand
 * @return \c true if \c x<y
 * @remarks Lexical ordering
 * @ingroup Utility
 */
bool operator < (const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& x, const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& y)
{

	return get<0>(x)==get<0>(y) ? get<1>(x)<get<1>(y) : get<0>(x)<get<0>(y);
}	//bool operator < (const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& x, const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& y)

/**
 * @brief "less than" operator for 3D cartesian points
 * @param[in] x First operand
 * @param[in] y Second operand
 * @return \c true if \c x<y
 * @remarks Lexical ordering
 * @ingroup Utility
 */
bool operator < (const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& x, const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& y)
{
	return get<0>(x)==get<0>(y) ? (get<1>(x)==get<1>(y) ? get<2>(x)<get<2>(y) : get<1>(x)<get<1>(y)) : get<0>(x) < get<0>(y);
}	//bool operator < (const point< ValueTypeM<Coordinate2DT>::type, 3, cartesian>& x, const point< ValueTypeM<Coordinate2DT>::type, 3, cartesian>& y)

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_GEOMETRY_CPP_1688020 */
