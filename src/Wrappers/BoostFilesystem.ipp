/**
 * @file BoostFilesystem.ipp Implementation details of Boost.Filesystem extensions
 * @author Evren Imre
 * @date 9 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_FILESYSTEM_IPP_0754303
#define BOOST_FILESYSTEM_IPP_0754303

#include <boost/filesystem.hpp>
#include <string>

namespace SeeSawN
{
namespace WrappersN
{

using boost::filesystem::exists;
using boost::filesystem::path;
using boost::filesystem::perms;
using boost::filesystem::file_status;
using std::string;

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_FILESYSTEM_IPP_0754303 */
