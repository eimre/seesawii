/**
 * @file BoostFilesystem.cpp Implementation and instantiations of Boost.Filesystem extensions
 * @author Evren Imre
 * @date 9 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "BoostFilesystem.h"
namespace SeeSawN
{
namespace WrappersN
{

/**
 * @brief Checks whether a path is a valid location for writing
 * @param[in] pathString Path name
 * @param[in] flagFilename If \c true, pathString includes the file name as well
 * @return \c false if the path does not exist, or does not grant write permissions
 * @ingroup Utility
 */
bool IsWriteable(const string& pathString, bool flagFilename)
{
	if(pathString.empty())
		return false;

	path testPath(pathString);

	if(flagFilename)
		testPath.remove_filename();

	bool flagExist=exists(testPath);

	if(!flagExist)
		return false;

	file_status fileStatus=boost::filesystem::status(testPath);
	perms permFlags=fileStatus.permissions();

	perms writeFlags= boost::filesystem::owner_write | boost::filesystem::group_write | boost::filesystem::others_write;

	 if( (permFlags & writeFlags) == boost::filesystem::no_perms)
		 return false;

	 return true;
}	//bool IsWriteable(const string& pathString, bool flagFilename)

}	//WrappersN
}	//SeeSawN

