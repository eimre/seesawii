/**
 * @file BoostDynamicBitset.ipp Implementation of various wrappers for Boost.Dynamic Bitset
 * @author Evren Imre
 * @date 30 Jun 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_DYNAMIC_BITSET_IPP_9602092
#define BOOST_DYNAMIC_BITSET_IPP_9602092

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <cstddef>
#include <type_traits>

namespace SeeSawN
{
namespace WrappersN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::dynamic_bitset;
using std::size_t;
using std::is_integral;
using std::is_unsigned;
using std::is_same;

/**
 * @brief Makes a bitset from an index range
 * @tparam BitsetT Bitset type
 * @tparam IndexRangeT Index range type
 * @param trueBits Indices of the bits that are set to 1
 * @param size Size of the bitset
 * @return Bitset corresponding to \c trueBits
 * @pre \c IndexRangeT is a forward range of unsigned integral values
 * @pre \c BitsetT support the \c dynamic_bitset interface (unenforced)
 * @pre \c size > the maximum element of \c trueBits (unenforced)
 * @ingroup Utility
 */
template<class BitsetT, class IndexRangeT>
BitsetT MakeBitset(const IndexRangeT& trueBits, size_t size)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<IndexRangeT>));
	static_assert(is_integral<typename range_value<IndexRangeT>::type >::value, "MakeBitset: IndexRangeT must hold integer values.");
	static_assert(is_unsigned<typename range_value<IndexRangeT>::type >::value, "MakeBitset: IndexRangeT must hold unsigned values.");

	BitsetT output(size);
	for(auto c : trueBits)
		output[c]=1;

	return output;
}	//BitsetT MakeBitset(const IndexRangeT& trueBits, size_t size)

/**
 * @brief Makes a bitset from a range of blocks
 * @tparam BitsetT A bitset
 * @tparam BlockRangeT A block range
 * @param[in] src A range of blocks
 * @return The bitset corresponding to \c src
 * @pre \c BlockRangeT is a forward range holding unsigned integral elements
 * @pre \c BitsetT support the \c dynamic_bitset interface (unenforced)
 * @remarks The values in \c src are correctly sliced/merged to match the block type of \c BitsetT
 * @warning The first element of \c src is the least significant block!
 */
template<class BitsetT, class BlockRangeT>
BitsetT MakeBitset(const BlockRangeT& src)
{
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<BlockRangeT>));
	static_assert(is_integral<typename range_value<BlockRangeT>::type >::value, "MakeBitset: BlockRangeT must hold integer values.");
	static_assert(is_unsigned<typename range_value<BlockRangeT>::type >::value, "MakeBitset: BlockRangeT must hold unsigned values.");

	typedef typename BitsetT::block_type DestinationT;
	typedef typename range_value<BlockRangeT>::type SourceT;

	//If the types match, use the existing constructor
	if(is_same<SourceT, DestinationT>::value)
		return BitsetT(boost::begin(src), boost::end(src));

	//Different sizes
	dynamic_bitset<SourceT> tmp(boost::begin(src), boost::end(src));	//intermediate type
	size_t nBits=tmp.size();
	BitsetT output(nBits);
	for(size_t c=0; c<nBits; ++c)
		output[c]=tmp[c];

	return output;
}

}	//WrappersN
}	//SeeSawN

#endif /* BOOST_DYNAMIC_BITSET_IPP_9602092 */
