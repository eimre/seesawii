/**
 * @file EigenExtensions.cpp Instantiations for various Eigen extensions
 * @author Evren Imre
 * @date 11 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "EigenExtensions.h"
namespace SeeSawN
{
namespace WrappersN
{

/********** EXPLICIT INSTANTIATIONS **********/

template bool EigenLexicalCompareC::operator()(const Vector3d&, const Vector3d&) const;

template MatrixXd Reshape(const VectorXd&, size_t, size_t);
template VectorXd Reshape(const MatrixXd&, size_t, size_t);
template Matrix3d Reshape(const VectorXd&, size_t, size_t);
template VectorXd Reshape(const Matrix3d&, size_t, size_t);

template Matrix3d MakeCrossProductMatrix(const Vector3d&);

template Vector4d QuaternionToVector(const Quaterniond&);
}	//WrapperN
}	//SeeSawN

