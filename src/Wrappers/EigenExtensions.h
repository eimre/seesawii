/**
 * @file EigenExtensions.h Public interface for various Eigen extensions
 * @author Evren Imre
 * @date 11 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef EIGEN_EXTENSIONS_H_4070123
#define EIGEN_EXTENSIONS_H_4070123

#include "EigenExtensions.ipp"

namespace SeeSawN
{
namespace WrappersN
{

struct EigenLexicalCompareC;	//Lexical comparison for two dense Eigen objects

/********** FREE FUNCTIONS **********/

template<class MatrixRT, class MatrixST> MatrixRT Reshape(const MatrixST& src, size_t nRow, size_t nCol);	///< Reshapes a matrix

template<typename ValueT> Matrix<ValueT, 3, 3> MakeCrossProductMatrix(const Matrix<ValueT,3,1>& coordinate);	///< Matrix performing the cross-product operation with a 3D vector

template<typename ValueT> Matrix<ValueT, 4, 1> QuaternionToVector(const Quaternion<ValueT>& q);	///< Converts a quaternion to a vector

/********** EXTERN TEMPLATES **********/

using Eigen::Vector3d;
extern template bool EigenLexicalCompareC::operator()(const Vector3d&, const Vector3d&) const;

using Eigen::MatrixXd;
using Eigen::VectorXd;
extern template MatrixXd Reshape(const VectorXd&, size_t, size_t);
extern template VectorXd Reshape(const MatrixXd&, size_t, size_t);

using Eigen::Matrix3d;
extern template Matrix3d Reshape(const VectorXd&, size_t, size_t);
extern template VectorXd Reshape(const Matrix3d&, size_t, size_t);

using Eigen::Vector3d;
extern template Matrix3d MakeCrossProductMatrix(const Vector3d&);

using Eigen::Vector4d;
using Eigen::Quaterniond;
extern template Vector4d QuaternionToVector(const Quaterniond&);
}	//WrapperN
}	//SeeSawN

#endif /* EIGENEXTENSIONS_H_ */
