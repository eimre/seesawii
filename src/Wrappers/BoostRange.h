/**
 * @file BoostRange.h Extensions and wrappers for Boost.Range
 * @author Evren Imre
 * @date 7 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_RANGE_H_4131533
#define BOOST_RANGE_H_4131533

#include "BoostRange.ipp"
namespace SeeSawN
{
namespace WrappersN
{

template<class Range1T, class Range2T> iterator_range<zip_iterator<boost::tuple<typename range_iterator<Range1T>::type, typename range_iterator<Range2T>::type> > > MakeBinaryZipRange(Range1T& range1, Range2T& range2);	///< Makes a binary zip iterator range
template<class Range1T, class Range2T> iterator_range<zip_iterator<boost::tuple<typename range_const_iterator<Range1T>::type, typename range_const_iterator<Range2T>::type> > > MakeBinaryZipRange(const Range1T& range1, const Range2T& range2);	///< Makes a binary zip iterator range, with constant iterators

template<class ElementRangeT, class IndexRangeT> iterator_range<permutation_iterator<typename range_iterator<ElementRangeT>::type, typename range_iterator<IndexRangeT>::type> > MakePermutationRange(ElementRangeT& elementRange, IndexRangeT& indexRange);	///< Makes a permutation range
template<class ElementRangeT, class IndexRangeT> iterator_range<permutation_iterator<typename range_const_iterator<ElementRangeT>::type, typename range_const_iterator<IndexRangeT>::type> > MakePermutationRange(const ElementRangeT& elementRange, const IndexRangeT& indexRange);	///< Makes a permutation range, with constant iterators

}	//WrappersN
}	//SeeSawN

#endif /* BOOSTRANGE_H_ */
