/**
 * @file BoostGeometry.h Public interface for the wrapper functions for Boost.Geometry
 * @author Evren Imre
 * @date 5 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BOOST_GEOMETRY_H_0379213
#define BOOST_GEOMETRY_H_0379213

#include "BoostGeometry.ipp"
namespace SeeSawN
{
namespace WrappersN
{

point< ValueTypeM<Coordinate2DT>::type, 2, cartesian> MakePoint(const Coordinate2DT source);		///< Converts a 2D coordinate to a Boost.Geometry point
point< ValueTypeM<Coordinate3DT>::type, 3, cartesian> MakePoint(const Coordinate3DT source);		///< Converts a 3D coordinate to a Boost.Geometry point

Coordinate2DT MakeCoordinate(const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& point);	///< Converts a 2D Boost.Geometry point to a coordinate
Coordinate3DT MakeCoordinate(const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& point);	///< Converts a 3D Boost.Geometry point to a coordinate


//Lexical cordering
bool operator < (const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& x, const point< ValueTypeM<Coordinate2DT>::type, 2, cartesian>& y);	///< "less than" operator for 2D cartesian points
bool operator < (const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& x, const point< ValueTypeM<Coordinate3DT>::type, 3, cartesian>& y);	///< "less than" operator for 3D cartesian points

}	//WrappersN
}	//SeeSawN

#endif /* BOOSTGEOMETRY_H_ */
