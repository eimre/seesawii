/**
 * @file BoostGraph.ipp Implementation of wrappers for common Boost.Graph operations
 * @author Evren Imre
 * @date 28 Apr 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BOOST_GRAPH_IPP_0891242
#define BOOST_GRAPH_IPP_0891242

#include <boost/functional/hash.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/property_map/vector_property_map.hpp>
#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/algorithm.hpp>
#include <vector>
#include <list>
#include <map>
#include <type_traits>
#include <utility>
#include <iterator>

namespace SeeSawN
{
namespace WrappersN
{

using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::add_edge;
using boost::adjacency_matrix;
using boost::undirectedS;
using boost::connected_components;
using boost::vector_property_map;
using boost::range::max_element;
using boost::range::for_each;
using std::vector;
using std::list;
using std::pair;
using std::map;
using std::is_convertible;
using std::is_integral;
using std::is_unsigned;
using std::distance;

/**
 * @brief Finds the connected components in an index range
 * @tparam IndexRangeT A range of edges
 * @param[in] indexRange Index range
 * @param[in] flagFindLargest If \c true only the largest component is returned
 * @return A vector, where each element is the list of indices associated with a particular component
 * @pre \c IndexRangeT is a single pass range
 * @pre Elements of \c IndexRangeT is convertible to <tt> pair(unsigned int, unsigned int) </tt>, and hold unsigned integral values
 */
template <class IndexRangeT>
vector<list<typename range_value<IndexRangeT>::type> > FindConnectedComponents(const IndexRangeT& indexRange, bool flagFindLargest=false)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<IndexRangeT>));
	static_assert(is_convertible<pair<unsigned int, unsigned int>, typename range_value<IndexRangeT>::type >::value, "FindConnectedComponents : indexRange must hold values convertible to pair<unsigned int, unsigned int> " );
	static_assert(is_integral<typename range_value<IndexRangeT>::type::first_type>::value, "FindConnectedComponents : indexRange must hold integral values" );
	static_assert(is_integral<typename range_value<IndexRangeT>::type::second_type>::value, "FindConnectedComponents : indexRange must hold integral values" );
	static_assert(is_unsigned<typename range_value<IndexRangeT>::type::first_type>::value, "FindConnectedComponents : indexRange must hold unsigned values" );
	static_assert(is_unsigned<typename range_value<IndexRangeT>::type::second_type>::value, "FindConnectedComponents : indexRange must hold unsigned values" );

	typedef typename range_value<IndexRangeT>::type EdgeT;
	vector<list<EdgeT> > output;

	if(boost::empty(indexRange))
		return output;

	//Map the input indices to a consecutive set
	map<unsigned int, unsigned int> sourceToVertex;
	for(const auto& current : indexRange)
	{
		sourceToVertex.emplace(current.first, sourceToVertex.size());
		sourceToVertex.emplace(current.second, sourceToVertex.size());
	}	//for(const auto& current : indexRange)

	//Graph
	size_t nVertex=sourceToVertex.size();
	adjacency_matrix<undirectedS> graph(nVertex);
	for(const auto& current : indexRange)
		add_edge(sourceToVertex[current.first], sourceToVertex[current.second], graph);

	//Connected components
	vector_property_map<size_t> vertexToComponentMap(nVertex);
	size_t nComponents=connected_components(graph, vertexToComponentMap);

	//Output
	if(nComponents==1 || !flagFindLargest)
	{
		output.resize(nComponents);
		for(const auto& current : indexRange)
			output[ vertexToComponentMap[ sourceToVertex[current.first] ] ].push_back(current);
	}	//if(nComponents==1 || !flagFindLargest)
	else
	{
		//Find the largest component
		vector<unsigned int> acc(nComponents,0);
		for(size_t c=0; c<nComponents; ++c)
			++acc[vertexToComponentMap[c]];

		auto it=max_element(acc);
		size_t iLargest=distance(acc.begin(), it);

		output.resize(1);
		for(const auto& current : indexRange)
			if( vertexToComponentMap[ sourceToVertex[current.first] ] == iLargest)
				output[0].push_back(current);
	}	//if(nComponents==1 || !flagFindLargest)

	return output;
}	//vector<list<unsigned int> > FindConnectedComponents(const IndexRangeT& indexRange)
}	//WrappersN
}	//SeeSawN



#endif /* BOOST_GRAPH_IPP_0891242 */
