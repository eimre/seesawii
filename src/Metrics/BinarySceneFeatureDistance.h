/**
 * @file BinarySceneFeatureDistance.h Public interface for the distance metrics for the binary scene features
 * @author Evren Imre
 * @date 4 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BINARY_SCENE_FEATURE_DISTANCE_H_0012809
#define BINARY_SCENE_FEATURE_DISTANCE_H_0012809

#include "BinarySceneFeatureDistance.ipp"
#include "Distance.h"

namespace SeeSawN
{
namespace MetricsN
{

template<class DistanceT> class BinarySceneImageFeatureDistanceC;	///< Measures the distance between a scene and an image feature
template<class DistanceT> class BinarySceneFeatureDistanceC;	///< Measures the distance between two scene features

/********** EXTERN TEMPLATES **********/
using SeeSawN::MetricsN::HammingDistanceBIDT;
extern template class BinarySceneImageFeatureDistanceC<HammingDistanceBIDT>;
typedef BinarySceneImageFeatureDistanceC<HammingDistanceBIDT> HammingBinarySceneImageFeatureDistanceT;

extern template class BinarySceneFeatureDistanceC<HammingDistanceBIDT>;
typedef BinarySceneFeatureDistanceC<HammingDistanceBIDT> HammingBinarySceneFeatureDistanceT;

}	//MetricsN
}	//SeeSawN




#endif /* BINARYSCENEFEATUREDISTANCE_HBINARY_SCENE_FEATURE_DISTANCE_H_0012809_ */
