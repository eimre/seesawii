/**
 * @file MatchAmbiguityMetric.ipp Implementation of various match ambiguity metrics
 * @author Evren Imre
 * @date 20 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MATCH_AMBIGUITY_METRIC_IPP_6923123
#define MATCH_AMBIGUITY_METRIC_IPP_6923123

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <cmath>
#include <climits>
#include <numeric>
#include <iterator>
#include <vector>
#include <queue>
#include <functional>
#include "InvertibleBinarySimilarityConcept.h"
#include "../Elements/Correspondence.h"


namespace SeeSawN
{
namespace MetricsN
{

using boost::math::pow;
using boost::optional;
using Eigen::Array;
using std::min;
using std::max;
using std::numeric_limits;
using std::next;
using std::prev;
using std::vector;
using std::priority_queue;
using std::less;
using SeeSawN::MetricsN::InvertibleBinarySimilarityConceptC;

/**
 * @brief Computes the Rayleigh meta-recognition metric for a set of correspondences
 * @tparam SimilarityMetricT A similarity metric
 * @pre \c SimilarityMetricT is the similarity metric with which the similarity scores are calculated (unenforced)
 * @pre \c SimilarityMetricT is a model of InvertibleBinarySimilarityConceptC
 * @remarks "SWIGS : A Swift Guided Sampling Method, " V. Fragoso, M. Turk, CVPR 2013
 * @remarks The original paper is for one-sided matching (i.e. if the feature sets are swapped, the result could be different). It is adapted to the nearest-N neighbourhood symmetric matching as follows
 * - If a feature occurs in more than one pair, the ambiguity for each occurrence is equal to that of the pair with the best similarity. This effectively means that all other occurrences are treated as outliers.
 * - In a pair, each member is likely to have a different ambiguity score (as they compete against different candidates). The ambiguity score of a pair is the maximum of the ambiguity scores of individual members.
 * - Guided matching: Any correspondence failing the constraint is assumed to be impossible, and not included in the tail
 * @remarks The distance is computed as the inverse of the feature similarity score. The computed ambiguity overwrites  \c strength.ambiguity (see Correspondence.h)
 * @remarks The original paper computes the confidence through a Rayleigh nonlinearity. The implementation computes the ambiguity as -ln(confidence).
 * @remarks Relation to the ratio test: the metric is the squared ratio of the best distance to the average of the other best-N candidates. So the effect of the second-best candidate is reduced, and a match with a high ratio ambiguity may have a low Rayleigh ambiguity
 * @remarks Usage notes
 * 	- This class is meant to be called from within a feature matching problem- hence the slightly awkward interface
 * 	- Parameter range is [0,1]. Recommended threshold is 0.51 (-ln(0.6), R1)
 * 	- Guided matching: A poor correspondence may end up with a very low ambiguity, if the constraint removes the other candidates.  When this happens, ambiguity ceases to be a good indicator of quality.
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class SimilarityMetricT>
class RayleighMetaRecognitionMetricC
{
	//SimilarityMetricT is needed only to convert a similarity score to a distance
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((InvertibleBinarySimilarityConceptC<SimilarityMetricT, typename SimilarityMetricT::result_type, typename SimilarityMetricT::first_argument_type, typename SimilarityMetricT::second_argument_type>));
	///@endcond

	private:

		typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT;	///< A boolean array
		typedef typename SimilarityMetricT::result_type RealT;	///< A floating point type
		typedef Array<RealT, Eigen::Dynamic, Eigen::Dynamic> RealArrayT;	///< A real array

		constexpr static float rPop=0.005;	///< Percentage of the members to be included in the tail sample. R1

		/** @name Implementation details */ //@{
		typedef priority_queue<RealT, vector<RealT>, less<RealT> >  TailT;
		static inline void Insert(TailT& tail, size_t maxSize, RealT value);	///< Inserts an element into a tail
		static inline RealT ComputeMR(TailT& tail);	///< Computes the meta-recognition score for a tail
		//@}
	public:

		template<class CorrespondenceRangeT> void operator()(CorrespondenceRangeT& correspondences, const SimilarityMetricT& similarityMetric, const RealArrayT& similarityArray, const BoolArrayT& validityMask, const optional<BoolArrayT>& constraintMask, unsigned int nThreads=1, unsigned int maxCardinality=10) const;	///< Computes the Rayleigh-MR metric

};	//class RayleighMetaRecognitionMetricC

/**
 * @brief Inserts an element into a tail
 * @param[in, out] tail Buffer to be updated
 * @param[in] maxSize Maximum size of the buffer
 * @param[in] value Value to be inserted
 */
template<class SimilarityMetricT>
void RayleighMetaRecognitionMetricC<SimilarityMetricT>::Insert(TailT& tail, size_t maxSize, RealT value)
{
	if(tail.size()==maxSize && tail.top()>value)
		tail.pop();

	if(tail.size()<maxSize)
		tail.push(value);
}	//void Insert(BufferT& buffer, size_t maxSize, RealT similarity)

/**
 * @brief Computes the meta-recognition score for a tail
 * @param[in] tail Tail
 * @return MR score
 */
template<class SimilarityMetricT>
auto RayleighMetaRecognitionMetricC<SimilarityMetricT>::ComputeMR(TailT& tail) -> RealT
{
	size_t sTail=tail.size();

	//If just 1 member, no ambiguity
	if(sTail==1)
		return 0;

	//Compute sigma2
	size_t sTailm1=sTail-1;
	RealT acc=0;
	for(size_t c=0; c<sTailm1; ++c, tail.pop())	//Leaves the minimum element in the buffer
		acc+=pow<2>(tail.top());

	RealT sigma2=acc/sTailm1;	//TODO Try unbiased estimate

	return pow<2>(tail.top())/sigma2;
}	//RealT ComputeMR(BufferT& buffer, const vector<RealT>& tail, const SimilarityMetricT& similarityMetric)

/**
 * @brief Computes the Rayleigh-MR metric
 * @tparam CorrespondenceRangeT An index correspondence range
 * @param[in, out] correspondences Correspondences. In the output \c strength.Ambiguity() is overwritten by the Rayleigh-MR metric
 * @param[in] similarityMetric Similarity metric with which \c similarityArray is built. Required for converting similarity to distance
 * @param[in] similarityArray Similarity scores between all candidates
 * @param[in] validityMask A valid entry in \c similarityArray is indicated by \c true
 * @param[in] constraintMask If a valid mask is provided, the metric is computed only over the candidates satisfying the constraint
 * @param[in] nThreads Number of threads
 * @param[in] maxCardinality Maximum cardinality of the set from which \f$ \sigma \f$ is computed
 * @pre \c CorrespondenceRangeT is an index correspondence range as described in Correspondence.h (unenforced)
 * @remarks \c validityMask may have \c false entries, when a constraint prevents the computation of the corresponding similarity score
 * @warning Unlike the paper, the implementation computes a penalty value. A lower value indicates a more reliable correspondence
 */
template<class SimilarityMetricT>
template<class CorrespondenceRangeT>
void RayleighMetaRecognitionMetricC<SimilarityMetricT>::operator()(CorrespondenceRangeT& correspondences, const SimilarityMetricT& similarityMetric, const RealArrayT& similarityArray, const BoolArrayT& validityMask, const optional<BoolArrayT>& constraintMask, unsigned int nThreads, unsigned int maxCardinality) const
{
	nThreads=min(nThreads, (unsigned int)omp_get_max_threads());

	unsigned int sSet1=similarityArray.rows();
	unsigned int sSample1=min(maxCardinality, (unsigned int)max(1.0f, sSet1*rPop))+1;

	unsigned int sSet2=similarityArray.cols();
	unsigned int sSample2=min(maxCardinality, (unsigned int)max(1.0f, sSet2*rPop))+1;

	//Correspondence masks
	vector<bool> correspondenceMask1(sSet1, false);
	vector<bool> correspondenceMask2(sSet2, false);
	for(const auto& current : correspondences)
	{
		correspondenceMask1[current.left]=true;
		correspondenceMask2[current.right]=true;
	}	//for(const auto& current : correspondences)

	//Iterate over the similarity scores, and build the left tails
	vector<TailT> tails1(sSet1);
	vector<TailT> tails2(sSet2);
	RealT dissimilarity;
	size_t c2=0;
#pragma omp parallel for if(nThreads>1) schedule(static) private(c2, dissimilarity) num_threads(nThreads)
	for(c2=0; c2<sSet2; ++c2)	//TODO Assumes row-major! Should be able to work with column-major with equal efficiency
	{
		TailT& pTail2=tails2[c2];
		for(size_t c1=0; c1<sSet1; ++c1)
			if( validityMask(c1,c2) && (!constraintMask || (*constraintMask)(c1,c2)))	//The score is computed, and the pair satisfies the constraint, if exists
			{
				dissimilarity=similarityMetric.Invert(similarityArray(c1,c2));

				if(correspondenceMask1[c1])
			#pragma omp critical(MAM_1)
					Insert(tails1[c1], sSample1, dissimilarity);

				//Not critical, only one thread is working on this
				if(correspondenceMask2[c2])
					Insert(pTail2, sSample2, dissimilarity);
			}	//if( validityMask(c1,c2) && (!constraintMask || (*constraintMask)(c1,c2)) )
	}	//for(size_t c2=0; c2<sSet2; ++c2)

	//Compute the Rayleigh meta-recognition metric, left-based
	vector<RealT> metric1(sSet1, numeric_limits<RealT>::infinity());
	for(size_t c=0; c<sSet1; ++c)
		if(!tails1[c].empty())
			metric1[c]=ComputeMR(tails1[c]);

	//Compute the Rayleigh meta-recognition metric, right-based
	vector<RealT> metric2(sSet2, numeric_limits<RealT>::infinity());
	for(size_t c=0; c<sSet2; ++c)
		if(!tails2[c].empty() )
			metric2[c]=ComputeMR(tails2[c]);

	//Output
	for(auto& current : correspondences)
		current.info.Ambiguity()=max( metric1[current.left], metric2[current.right] );
}	//void operator()(CorrespondenceRangeT& correspondences, const RealArrayT& similarityArray, const BoolArrayT& validityMask)

}	//MetricsN
}	//SeeSawN

#endif /* MATCH_AMBIGUITY_METRIC_IPP_6923123 */
