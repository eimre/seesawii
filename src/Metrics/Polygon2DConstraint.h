/**
 * @file Polygon2DConstraint.h Public interface for \c Polygon2DConstraint
 * @author Evren Imre
 * @date 18 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POLYGON2D_CONSTRAINT_H_0801289
#define POLYGON2D_CONSTRAINT_H_0801289

#include <vector>
#include "Polygon2DConstraint.ipp"
namespace SeeSawN
{
namespace MetricsN
{

class Polygon2DConstraintC;	///< Checks whether a point is within a 2D polygon


/********** EXTERN TEMPLATES **********/
using std::vector;
extern template Polygon2DConstraintC::Polygon2DConstraintC(const vector<Coordinate2DT>&);

}	//MetricN
}	//SeeSawN

#endif /* POLYGON2D_CONSTRAINT_H_0801289 */
