/**
 * @file Similarity.h Public interface of the similarity metrics
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SIMILARITY_H_9135645
#define SIMILARITY_H_9135645

#include "Similarity.ipp"
#include "Distance.h"
#include "SceneFeatureDistance.h"
#include "BinarySceneFeatureDistance.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace MetricsN
{

template<class DistanceT, class MapT> class DistanceToSimilarityConverterC; ///< Converts a distance value to a similarity value

/********** EXTERN TEMPLATES **********/

using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
typedef DistanceToSimilarityConverterC<EuclideanDistanceIDT, ReciprocalC<typename EuclideanDistanceIDT::result_type> > InverseEuclideanIDConverterT;    ///< Inverse Euclidean distance, dynamic vectors
extern template class DistanceToSimilarityConverterC<EuclideanDistanceIDT, ReciprocalC<typename EuclideanDistanceIDT::result_type> >;

using SeeSawN::MetricsN::ChiSquaredDistanceIDT;
typedef DistanceToSimilarityConverterC<ChiSquaredDistanceIDT, ReciprocalC<typename ChiSquaredDistanceIDT::result_type> > InverseChiSquaredIDConverterT;   ///< Inverse Chi-squared distance
extern template class DistanceToSimilarityConverterC<ChiSquaredDistanceIDT, ReciprocalC<typename ChiSquaredDistanceIDT::result_type> >;

using SeeSawN::MetricsN::HammingDistanceBIDT;
typedef DistanceToSimilarityConverterC<HammingDistanceBIDT, ReciprocalC<float> > InverseHammingBIDConverterT;    ///< Inverse Hamming distance
extern template class DistanceToSimilarityConverterC<HammingDistanceBIDT, ReciprocalC<float> >;	//Memory allocation costs a lot, and single-precision is sufficient

using SeeSawN::MetricsN::EuclideanSceneFeatureDistanceT;
typedef DistanceToSimilarityConverterC<EuclideanSceneFeatureDistanceT, ReciprocalC<EuclideanSceneFeatureDistanceT::result_type> > InverseEuclideanSceneFeatureDistanceConverterT;	///< Inverse distance between two scene features
extern template class DistanceToSimilarityConverterC<EuclideanSceneFeatureDistanceT, ReciprocalC<EuclideanSceneFeatureDistanceT::result_type> >;

using SeeSawN::MetricsN::EuclideanSceneImageFeatureDistanceT;
typedef DistanceToSimilarityConverterC<EuclideanSceneImageFeatureDistanceT, ReciprocalC<EuclideanSceneImageFeatureDistanceT::result_type> > InverseEuclideanSceneImageFeatureDistanceConverterT;	///< Inverse distance between a scene and an image features
extern template class DistanceToSimilarityConverterC<EuclideanSceneImageFeatureDistanceT, ReciprocalC<EuclideanSceneImageFeatureDistanceT::result_type> >;

using SeeSawN::MetricsN::HammingBinarySceneFeatureDistanceT;
typedef DistanceToSimilarityConverterC<HammingBinarySceneFeatureDistanceT, ReciprocalC<HammingBinarySceneFeatureDistanceT::result_type> > InverseHammingSceneFeatureDistanceConverterT;	///< Inverse distance between two scene features
extern template class DistanceToSimilarityConverterC<HammingBinarySceneFeatureDistanceT, ReciprocalC<HammingBinarySceneFeatureDistanceT::result_type> >;

using SeeSawN::MetricsN::HammingBinarySceneImageFeatureDistanceT;
typedef DistanceToSimilarityConverterC<HammingBinarySceneImageFeatureDistanceT, ReciprocalC<HammingBinarySceneImageFeatureDistanceT::result_type> > InverseHammingSceneImageFeatureDistanceConverterT;	///< Inverse distance between a scene and an image features
extern template class DistanceToSimilarityConverterC<HammingBinarySceneImageFeatureDistanceT, ReciprocalC<HammingBinarySceneImageFeatureDistanceT::result_type> >;


}   //MetricsN
}	//SeeSawN

#endif /* SIMILARITY_H_ */
