/**
 * @file CheiralityConstraint.cpp Implementation of the cheirality constraint
 * @author Evren Imre
 * @date 6 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CheiralityConstraint.h"
namespace SeeSawN
{
namespace MetricsN
{

/**
 * @brief Constructor
 * @post Object invalid
 */
CheiralityConstraintC::CheiralityConstraintC() : flagValid(false), signDetM(0)
{}

/**
 * @brief Constructor
 * @param[in] mmP Camera matrix
 * @post A valid object
 * @throws invalid_argument If \c mmP has an all-zero 3rd row
 */
CheiralityConstraintC::CheiralityConstraintC(const CameraMatrixT& mmP) : flagValid(true), signDetM(0)
{
	//Preconditions
	if(mmP.row(2).norm() < numeric_limits< ValueTypeM<CameraMatrixT>::type >::epsilon()  )
		throw(invalid_argument("CheiralityConstraintC::CheiralityConstraintC : Invalid principal vector. The third row of the camera matrix must have a non-zero norm"));

	vP3=mmP.row(2)/mmP.row(2).segment(0,3).norm();

	double detM= (mmP.block(0,0,3,3).normalized()).determinant();
	if(fabs(detM)<1e3*numeric_limits< ValueTypeM<CameraMatrixT>::type >::epsilon() )
		signDetM=0;
	else
		signDetM=sign(detM);
}	//CheiralityConstraintC(const CameraMatrixT& mmP)

/**
 * @brief Signed distance from the image plane
 * @param[in] point A 3D point
 * @return Signed distance from the principal plane of the camera
 * @pre \c flagValid=true
 * @pre \c signDetM!=0
 * @throws invalid_argument if \c signDetM==0
 * @remarks "Multiple View Geometry in Computer Vision,", R. Hartley, A. Zisserman, pp. 162, Eq. 6.15
 * @remarks Not the same as the distance to the camera centre!
 */
double CheiralityConstraintC::Distance(const Coordinate3DT& point) const
{
	//Preconditions
	assert(flagValid);

	if(signDetM==0)
		throw(invalid_argument("CheiralityConstraintC::Distance : Cannot be called with a camera matrix with a non-invertible first 3x3 submatrix.") );

	return signDetM*(vP3.dot(point.homogeneous() ) );
}	//double Distance(const Coordinate3DT& point) const

/**
 * @brief Absolute distance from the image plane
 * @param[in] point A 3D point
 * @return Absolute distance from the image plane
 */
double CheiralityConstraintC::AbsoluteDistance(const Coordinate3DT& point) const
{
	return fabs(vP3.dot(point.homogeneous() ));
}	//double AbsoluteDistance(const Coordinate3DT& point) const

/**
 * @brief Verifies the cheirality constraint
 * @param[in] point A 3D point
 * @return \c true if the point is in front of the camera
 */
bool CheiralityConstraintC::operator()(const Coordinate3DT& point) const
{
	return Distance(point)>0;
}	//bool operator()(const Coordinate3DT& point) const

}	//MetricsN
}	//SeeSawN

