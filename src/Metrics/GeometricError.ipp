/**
 * @file GeometricError.ipp Implementation of various geometric errors
 * @author Evren Imre
 * @date 31 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_ERROR_IPP_2121562
#define GEOMETRIC_ERROR_IPP_2121562

#include <boost/math/special_functions/pow.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <type_traits>
#include "../Wrappers/EigenMetafunction.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Numeric/LinearAlgebraJacobian.h"
#include "../Numeric/Complexity.h"

#ifdef NDEBUG
#include <Eigen/LU>
#endif

namespace SeeSawN
{
namespace MetricsN
{

using boost::optional;
using boost::math::chi_squared_distribution;
using boost::math::quantile;
using boost::math::complement;
using boost::math::pow;
using Eigen::Matrix;
using std::vector;
using std::logic_error;
using std::sqrt;
using std::fabs;
using std::hypot;
using std::is_same;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::ColsM;
using SeeSawN::GeometryN::CoordinateTransformationsT;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::HCoordinate2DT;
using SeeSawN::ElementsN::CoordinateVectorT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::NumericN::JacobiandAidA;
using SeeSawN::NumericN::ComplexityEstimatorC;

//TODO How to estimate a threshold that is not affected by the scale term in H?
namespace DetailN
{
/**@brief Arity-2 tag
 * @ingroup Tag
 * @internal
 * */
struct Arity2Tag{};

/**@brief Arity-4 tag
 * @ingroup Tag
 * @internal
 * */
struct Arity4Tag{};
}   //namespace DetailN


/**
 * @brief One-sided transfer error for homogeneous transformations
 * @tparam MatrixT A matrix
 * @pre \c MatrixT is a fixed-size Eigen dense matrix
 * @remarks \f$ \| \mathbf{x_2}-\mathbf{Hx_1}  \| \f$
 * @remarks When the error function is to be evaluated for all pairs in two sets, in order to eliminate the redundant projections::
 *          - Pre-compute the projections (Project)
 *          - Compute the error by using the projected points (ComputeFromProjected)
 * @remarks "Multiple View Geometry in Computer Vision, " Hartley and Zisserman, 2nd Ed., 2003, pp. 94
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class MatrixT>
class TransferErrorC
{
    static_assert(RowsM<MatrixT>::value!=Eigen::Dynamic, "TransferErrorC : MatrixT must be a fixed-size matrix");
    static_assert(ColsM<MatrixT>::value!=Eigen::Dynamic, "TransferErrorC : MatrixT must be a fixed-size matrix");

    private:

        typedef typename ValueTypeM<MatrixT>::type RealT;  ///< Floating point type
        typedef CoordinateVectorT<RealT, ColsM<MatrixT>::value-1> CoordinateST;   ///< A coordinate in the source domain
        typedef CoordinateVectorT<RealT, RowsM<MatrixT>::value-1> CoordinateDT;   ///< A coordinate in the destination domain

        typedef CoordinateTransformationsT<CoordinateST> CoordinateTransformationsST;   ///< Coordinate transformations for the source domain
        typedef CoordinateTransformationsT<CoordinateDT> CoordinateTransformationsDT;   ///< Coordinate transformations for the destination domain

        typedef Matrix<RealT, Eigen::Dynamic, 1> ColumnVectorXT;	///< A dynamic column vector
        typedef Matrix<RealT, 1, Eigen::Dynamic> RowVectorXT;	///< A dynamic row vector

        /** @name Configuration */ //@{
        MatrixT mH; ///< Homography
        bool flagValid; ///< \c true if mH is initialised
        static constexpr unsigned int nDOF=RowsM<CoordinateDT>::value;	///< DoF for the associated Chi-squared distribution
        float cost;	///< Computational cost of the operation
        //@}

    public:

        /**@name Constructors */ //@{
        TransferErrorC();   ///< Default constructor
        TransferErrorC(const MatrixT& mmH); ///< Constructor
        //@}

        /** @name Adaptable binary function interface */ //@{
        typedef RealT result_type;
        typedef CoordinateST first_argument_type;
        typedef CoordinateDT second_argument_type;
        RealT operator()(const CoordinateST& x1, const CoordinateDT& x2) const; ///< Computes the distance
        //@}

        /** @name Advanced interface */ //@{
        typedef CoordinateDT projected1_type;  ///< Projection of CoordinateST in the second domain. Dummy
        typedef CoordinateST projected2_type; ///< Projection of CoordinateDT in the first domain
        typedef MatrixT projector_type; ///< Projector type

        typedef DetailN::Arity2Tag arity_tag; ///< Arity of ComputeFromProjected
        static RealT ComputeFromProjected(const projected1_type& Hx1, const CoordinateDT& x2); ///< Computes the distance when \f$ \mathbf{Hx_1} \f$ is available
        template<class CoordinateRangeT> vector<projected1_type> Project(const CoordinateRangeT& x1Range) const;  ///< Projects a range of coordinates via mH
        template<class CoordinateRangeT> vector<projected2_type> InverseProject(const CoordinateRangeT& x2Range) const;  ///< Projects a range of coordinates via mH^-1 . Dummy

        RealT operator()(const projector_type& projector, const CoordinateST& x1, const CoordinateDT& x2) const;	///< Computes the distance wrt a projector

        typedef RowVectorXT jacobian_type;	///< Jacobian vector
        optional<RowVectorXT> ComputeJacobiandP(const CoordinateST& x1, const CoordinateDT& x2) const;	///< Computes the Jacobian of the error wrt/projector

        static RealT ComputeOutlierThreshold(RealT pReject, RealT noiseVariance=1);	///< Computes the outlier rejection threshold for a specified inlier rejection probability and noise variance
        double Cost() const;	///< Computational cost of the operation
        //@}

        /** @name Accessors */ //@{
        const MatrixT& GetProjector() const;    ///< Returns a constant reference to the projection operator
        void SetProjector(const MatrixT& mmH);  ///< Sets the projection operator
        //@}
};  //class TransferErrorC

/**
 * @brief Symmetric transfer error for homogeneous transformations
 * @tparam MatrixT A matrix
 * @pre \c MatrixT is a fixed-size square Eigen dense matrix
 * @remarks \f$ \sqrt {\| \mathbf{x_2}-\mathbf{Hx_1} \|^2 + \| \mathbf{x_1}-\mathbf{Hx_2} \|^2 } \f$
 * @remarks When the error function is to be evaluated for all pairs in two sets, in order to eliminate the redundant projections::
 *          - Pre-compute the projections (Project)
 *          - Compute the error by using the projected points (ComputeFromProjected)
 * @remarks "Multiple View Geometry in Computer Vision, " Hartley and Zisserman, 2nd Ed., 2003, pp. 94
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class MatrixT>
class SymmetricTransferErrorC
{
    static_assert(RowsM<MatrixT>::value!=Eigen::Dynamic, "SymmetricTransferErrorC : MatrixT must be a fixed-size matrix");
    static_assert(ColsM<MatrixT>::value!=Eigen::Dynamic, "SymmetricTransferErrorC : MatrixT must be a fixed-size matrix");
    static_assert(RowsM<MatrixT>::value==ColsM<MatrixT>::value, "SymmetricTransferErrorC : MatrixT must be a square matrix");

    private:

        typedef typename ValueTypeM<MatrixT>::type RealT;  ///< Floating point type
        typedef CoordinateVectorT<RealT, ColsM<MatrixT>::value-1> CoordinateST;   ///< A coordinate in the source domain
		typedef CoordinateVectorT<RealT, RowsM<MatrixT>::value-1> CoordinateDT;   ///< A coordinate in the destination domain

        typedef CoordinateTransformationsT<CoordinateST> CoordinateTransformationsST;   ///< Coordinate transformations for the source domain
        typedef CoordinateTransformationsT<CoordinateDT> CoordinateTransformationsDT;   ///< Coordinate transformations for the destination domain

        /** @name Configuration */ //@{
        MatrixT mH; ///< Homography
        MatrixT mHi; ///< Inverse homography
        bool flagValid; ///< \c true if mH is initialised
        static constexpr unsigned int nDOF=RowsM<CoordinateDT>::value + RowsM<CoordinateST>::value;	///< DoF for the associated Chi-squared distribution
        float cost;	///< Computational cost of the operation
        //@}

        typedef Matrix<RealT,-1, 1> ColumnVectorXT;	///< A dynamic column vector
        typedef Matrix<RealT, 1,-1> RowVectorXT;	///< A dynamic row vector

    public:

        /** @name Constructors */ //@{
        SymmetricTransferErrorC();   ///< Default constructor
        SymmetricTransferErrorC(const MatrixT& mmH); ///< Constructor
        //@}

        /** @name Adaptable binary function interface */ //@{
        typedef RealT result_type;
        typedef CoordinateST first_argument_type;
        typedef CoordinateDT second_argument_type;
        RealT operator()(const CoordinateST& x1, const CoordinateDT& x2) const; ///< Computes the distance
        //@}

        /** @name Advanced interface */ //@{
        typedef CoordinateDT projected1_type;  ///< Projection of CoordinateST in the second domain
        typedef CoordinateST projected2_type; ///< Projection of CoordinateDT in the first domain
        typedef MatrixT projector_type; ///< Projector type

        typedef DetailN::Arity4Tag arity_tag; ///< Arity of ComputeFromProjected
        static RealT ComputeFromProjected(const CoordinateST& x1, const projected1_type& Hx1, const CoordinateDT& x2, const projected2_type& Hix2); ///< Computes the distance when \f$ \mathbf{Hx_1} \f$ and \f$ \mathbf{Hx_2} \f$ are available
        template<class CoordinateRangeT> vector<projected1_type> Project(const CoordinateRangeT& x1Range) const;  ///< Projects a range of coordinates via mH
        template<class CoordinateRangeT> vector<projected2_type> InverseProject(const CoordinateRangeT& x2Range) const;  ///< Projects a range of coordinates via mH^-1


        RealT operator()(const projector_type& projector, const CoordinateST& x1, const CoordinateDT& x2) const;	///< Computes the distance wrt a projector

        typedef RowVectorXT jacobian_type;	///< Jacobian vector
        optional<RowVectorXT> ComputeJacobiandP(const CoordinateST& x1, const CoordinateDT& x2) const;	///< Computes the Jacobian of the error wrt/projector

        static RealT ComputeOutlierThreshold(RealT pReject, RealT noiseVariance=1);	///< Computes the outlier rejection threshold for a specified inlier rejection probability and noise variance
        double Cost() const;	///< Computational cost of the operation
        //@}

        /** @name Accessors */ //@{
        const MatrixT& GetProjector() const;    ///< Returns a constant reference to mH
        void SetProjector(const MatrixT& mmH);  ///< Sets mH
        //@}
};  //class SymmetricTransferErrorC

/**
 * @brief Sampson error for epipolar geometry
 * @remarks \f$ \frac{\mathbf{x_2^T F x_1} }{ (\mathbf{Fx_1})_1^2 + (\mathbf{Fx_1})_2^2 + (\mathbf{F^Tx_2})_1^2 + (\mathbf{F^Tx_2})_2^2 } \f$
 * @remarks When the error function is to be evaluated for all pairs in two sets, in order to eliminate the redundant projections::
 *          - Pre-compute the projections (Project)
 *          - Compute the error by using the projected points (ComputeFromProjected)
 * @remarks F indicates the relative calibration of the target camera with respect to the reference camera
 * @remarks "Multiple View Geometry in Computer Vision, " Hartley and Zisserman, 2nd Ed., 2003, pp. 287
 * @warning Undefined at the epipoles
 * @ingroup Metrics
 * @nosubgrouping
 */
class EpipolarSampsonErrorC
{
    private:

        typedef typename ValueTypeM<Coordinate2DT>::type RealT;  ///< Floating point type

        /** @name Configuration */ //@{
        typedef typename CoordinateTransformations2DT::projective_transform_type TransformT;
        TransformT mF;  ///< Fundamental or essential matrix
        TransformT mFt; ///< Transpose of mF
        bool flagValid; ///< \c true if mH is initialised
        static constexpr unsigned int nDOF=1;	///< DoF for the associated Chi-squared distribution
        float cost;	///< Computational cost of the operation
        //@}

        typedef Matrix<RealT,Eigen::Dynamic, 1> ColumnVectorXT;	///< A dynamic column vector
		typedef Matrix<RealT, 1, Eigen::Dynamic> RowVectorXT;	///< A dynamic row vector

    public:

		/** @name Constructors */ //@{
        EpipolarSampsonErrorC();   ///< Default constructor
        EpipolarSampsonErrorC(const EpipolarMatrixT& mmF); ///< Constructor
        //@}

        /** @name Adaptable binary function interface */ //@{
        typedef RealT result_type;
        typedef Coordinate2DT first_argument_type;
        typedef Coordinate2DT second_argument_type;
        RealT operator()(const Coordinate2DT& x1, const Coordinate2DT& x2) const; ///< Computes the distance
        //@}

        /** @name Advanced interface */ //@{
       typedef HCoordinate2DT projected1_type;  ///< Projection of CoordinateST in the second domain
       typedef HCoordinate2DT projected2_type; ///< Projection of CoordinateDT in the first domain
       typedef EpipolarMatrixT projector_type; ///< Projector type

       typedef DetailN::Arity4Tag arity_tag; ///< Arity of ComputeFromProjected
       static RealT ComputeFromProjected(const Coordinate2DT& x1, const HCoordinate2DT& Fx1, const Coordinate2DT& x2, const HCoordinate2DT& Ftx2); ///< Computes the distance when the epipolar lines are available
       template<class CoordinateRangeT> vector<HCoordinate2DT> Project(const CoordinateRangeT& x1Range) const;  ///< Projects a range of coordinates via mF
       template<class CoordinateRangeT> vector<HCoordinate2DT> InverseProject(const CoordinateRangeT& x2Range) const;  ///< Projects a range of coordinates via mF^T

       RealT operator()(const projector_type& projector, const Coordinate2DT& x1, const Coordinate2DT& x2) const;	///< Computes the distance wrt a projector

       typedef RowVectorXT jacobian_type;	///< Jacobian vector
	   optional<RowVectorXT> ComputeJacobiandP(const Coordinate2DT& x1, const Coordinate2DT& x2) const;	///< Computes the Jacobian of the error wrt/projector
	   optional<RowVectorXT> ComputeJacobiandx1x2(const Coordinate2DT& x1, const Coordinate2DT& x2) const;	///< Computes the Jacobian of the error wrt/Coordinates

	   static RealT ComputeOutlierThreshold(RealT pReject, RealT noiseVariance=1);	///< Computes the outlier rejection threshold for a specified inlier rejection probability and noise variance
	   double Cost() const;	///< Computational cost of the operation
	   //@}

	   /** @name Accessors */ //@{
       const EpipolarMatrixT& GetProjector() const;    ///< Returns a constant reference to mF,matrix()
       void SetProjector(const EpipolarMatrixT& mmF);  ///< Sets mF
       //@}
};  //class EpipolarSampsonErrorC

/********** IMPLEMENTATION STARTS HERE **********/

/********** TransferErrorC **********/

/**
 * @brief Default constructor
 * @post An invalid object
 */
template<class MatrixT>
TransferErrorC<MatrixT>::TransferErrorC() : flagValid(false)
{
	unsigned int m=RowsM<MatrixT>::value;
	unsigned int n=ColsM<MatrixT>::value;
	bool flagDouble=is_same<RealT, double>::value;
	cost=ComplexityEstimatorC::MatrixVectorMultiplication(m, n, flagDouble) + ComplexityEstimatorC::VectorAdd(m, flagDouble) + ComplexityEstimatorC::L2Norm(m, flagDouble);
}	//TransferErrorC()

/**
 * @brief Constructor
 * @param[in] mmH Homography transferring the points to the second domain
 * @post A valid object
 */
template<class MatrixT>
TransferErrorC<MatrixT>::TransferErrorC(const MatrixT& mmH) : TransferErrorC()
{
	mH=mmH;
	flagValid=true;

	bool flagDouble=is_same<RealT,double>::value;
	size_t m=mH.rows();
	size_t n=mH.cols();
	cost=ComplexityEstimatorC::MatrixVectorMultiplication(m,n,flagDouble) + ComplexityEstimatorC::L2Norm(m,flagDouble);
}	//TransferErrorC(const MatrixT& mmH)

/**
 * @brief Computes the distance
 * @param[in] x1 Point in the source domain
 * @param[in] x2 Point in the destination domain
 * @return Transfer error
 * @pre \c flagValid=true
 */
template<class MatrixT>
auto TransferErrorC<MatrixT>::operator()(const CoordinateST& x1, const CoordinateDT& x2) const -> RealT
{
    //Preconditions
    assert(flagValid);
    return ComputeFromProjected(((mH*x1.homogeneous())).eval().hnormalized(), x2);
}   //RealT operator()(const Coordinate1T& operand1, const Coordinate2T& operand2)

/**
 * @brief Computes the distance when \f$ \mathbf{Hx_1} \f$ is available
 * @param[in] Hx1 Projection of a point in the first set to the destination domain
 * @param[in] x2  A point in the second set
 * @return Transfer error
 */
template<class MatrixT>
auto TransferErrorC<MatrixT>::ComputeFromProjected(const projected1_type& Hx1, const CoordinateDT& x2) -> RealT
{
    return (x2-Hx1).norm();
}   //RealT ComputeFromProjected(const Coordinate2T& x2, const Coordinate1T& Hx1)

/**
 * @brief Projects a range of coordinates via mH
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] x1Range A list of coordinates in the source domain
 * @return Projection of x1Range to the destination domain
 * @pre \c flagValid=true
 */
template<class MatrixT>
template<class CoordinateRangeT>
auto TransferErrorC<MatrixT>::Project(const CoordinateRangeT& x1Range) const -> vector<projected1_type>
{
    //Preconditions
    assert(flagValid);
    return CoordinateTransformationsDT::NormaliseHomogeneous(CoordinateTransformationsST::HomogeneousProject(x1Range, mH));
}   //vector<Coordinate2T> Project(const CoordinateRangeT& x1Range)

/**
 * @brief Projects a range of coordinates via mH^-1 . Dummy
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] x2Range A list of coordinates in the destination domain
 * @return An uninitialised vector
 * @throws logic_error Always
 */
template<class MatrixT>
template<class CoordinateRangeT>
auto TransferErrorC<MatrixT>::InverseProject(const CoordinateRangeT& x2Range) const -> vector<projected2_type>
{
    throw(logic_error("TransferErrorC::InverseProject : Undefined operation for one-sided transfer error"));
    return vector<projected2_type>();
}   //vector<Coordinate2T> InverseProject(const CoordinateRangeT& x1Range)

/**
 * @brief Computes the distance
 * @param[in] projector Homography
 * @param[in] x1 Point in the source domain
 * @param[in] x2 Point in the destination domain
 * @return Transfer error
 */
template<class MatrixT>
auto TransferErrorC<MatrixT>::operator()(const projector_type& projector, const CoordinateST& x1, const CoordinateDT& x2) const -> RealT
{
	return ComputeFromProjected(((projector*x1.homogeneous())).eval().hnormalized(), x2);
}	//auto operator()(const projector_type& projector, const CoordinateST& x1, const CoordinateDT& x2) const -> RealT

/**
 * @brief Computes the Jacobian of the error wrt/projector
 * @param[in] x1 First coordinate
 * @param[in] x2 Second coordinate
 * @return Error. Invalid if the Jacobian computation fails
 */
template<class MatrixT>
auto TransferErrorC<MatrixT>::ComputeJacobiandP(const CoordinateST& x1, const CoordinateDT& x2) const -> optional<RowVectorXT>
{
	typename CoordinateTransformationsDT::homogeneous_coordinate_type vPh=mH*(x1.homogeneous());	//Project x1, homogeneous
	CoordinateDT vP= vPh.hnormalized();	//Project to cartesian
	CoordinateDT vError=vP-x2;	//Prediction error vector

	//Jacobian
	RealT error=vError.norm();	//Prediction error
	if(error==0)
		return optional<RowVectorXT>();

	size_t nCol=mH.cols();
	RowVectorXT vJ(mH.size());

	size_t dimD=RowsM<MatrixT>::value-1;
	size_t dimS=nCol-1;
	for(size_t c=0; c<dimD; ++c)
	{
		vJ.segment(c*nCol, dimS)=vError(c)*x1;
		vJ(c*nCol+dimS)=vError(c);
	}	//for(size_t c=0; c<dimD; ++c)

	RealT f2=-vError.dot(vP);
	vJ.segment(dimD*nCol, dimS)=f2*x1;
	vJ(vJ.size()-1)=f2;

	RealT f1=error*vPh(dimD);
	vJ*=(RealT(1.0)/f1);

	return vJ;
}	//MatrixT ComputeJacobiandM(const CoordinateST& x1, const CoordinateDT& x2)

/**
 * @brief Computes the outlier rejection threshold for a specified inlier rejection probability and noise variance
 * @param[in] pReject Probability of rejection of an inlier
 * @param[in] noiseVariance Variance of the noise on the individual coordinates
 * @return Outlier threshold, in units of the destination domain (i.e. not its square!)
 * @warning The function assumes that the noise is only in the destination domain. If the source domain is corrupted as well, \c noiseVariance should be adjusted accordingly
 */
template<class MatrixT>
auto TransferErrorC<MatrixT>::ComputeOutlierThreshold(RealT pReject, RealT noiseVariance) -> RealT
{
	return sqrt(quantile(complement(chi_squared_distribution<RealT>(nDOF),pReject))*noiseVariance);
}	//auto ComputeThreshold(RealT pReject, RealT noiseVariance) -> RealT

/**
 * @brief Returns a constant reference to mH
 * @return A reference to mH
 * @pre \c flagValid=true
 */
template<class MatrixT>
auto TransferErrorC<MatrixT>::GetProjector() const -> const MatrixT&
{
    //Preconditions
    assert(flagValid);
    return mH;
}   //MatrixT& Projector()

/**
 * @brief Sets mH
 * @param[in] mmH New mH
 * @post mH is modified
 */
template<class MatrixT>
void TransferErrorC<MatrixT>::SetProjector(const MatrixT& mmH)
{
    flagValid=true;
    mH=mmH;
}   //MatrixT& Projector()

/**
 * @brief Computational cost of the operation
 * @return \c cost
 * @remarks No unit tests, as the costs are volatile
 */
template<class MatrixT>
double TransferErrorC<MatrixT>::Cost() const
{
	return cost;
}	//float Cost()

/********** SymmetricTransferErrorC **********/

/**
 * @brief Default constructor
 * @post An invalid object
 */
template<class MatrixT>
SymmetricTransferErrorC<MatrixT>::SymmetricTransferErrorC() : flagValid(false)
{
	unsigned int n=ColsM<MatrixT>::value;
	bool flagDouble=is_same<RealT, double>::value;

	//Worst-case cost: includes inversion as well
	cost= 2*(ComplexityEstimatorC::MatrixVectorMultiplication(n, n, flagDouble) + ComplexityEstimatorC::VectorAdd(n, flagDouble) + ComplexityEstimatorC::L2Norm(n, flagDouble)) + ComplexityEstimatorC::Inverse(n, flagDouble);
}	//SymmetricTransferErrorC()

/**
 * @brief Constructor
 * @param[in] mmH Homography transferring the points to the second domain
 * @pre \c mmH is invertible (unenforced)
 * @post A valid object
 */
template<class MatrixT>
SymmetricTransferErrorC<MatrixT>::SymmetricTransferErrorC(const MatrixT& mmH) : SymmetricTransferErrorC()
{
    SetProjector(mmH);
}   //SymmetricTransferErrorC(const MatrixT& mmH) : mH(mmH)

/**
 * @brief Computes the distance
 * @param[in] x1 Point in the source domain
 * @param[in] x2 Point in the destination domain
 * @return Symmetric transfer error
 * @pre \c flagValid=true
 */
template<class MatrixT>
auto SymmetricTransferErrorC<MatrixT>::operator()(const CoordinateST& x1, const CoordinateDT& x2) const -> RealT
{
    //Preconditions
    assert(flagValid);
    return ComputeFromProjected(x1, (mH*x1.homogeneous()).eval().hnormalized(), x2, (mHi*x2.homogeneous()).eval().hnormalized() );
}   //RealT operator()(const Coordinate1T& operand1, const Coordinate2T& operand2)

/**
 * @brief Computes the distance when \f$ \mathbf{Hx_1} \f$ is availabl
 * @param[in] x1 A point in the first set
 * @param[in] Hx1 Projection of \c x1 to the destination domain
 * @param[in] x2  A point in the second set
 * @param[in] Hix2 Projection of \c x2 to the source domain
 * @return Symmetric transfer error
 */
template<class MatrixT>
auto SymmetricTransferErrorC<MatrixT>::ComputeFromProjected(const CoordinateST& x1, const projected1_type& Hx1, const CoordinateDT& x2, const projected2_type& Hix2) -> RealT
{
    return sqrt( (x2-Hx1).squaredNorm() + (x1-Hix2).squaredNorm());
}   //RealT ComputeFromProjected(const CoordinateST& x1, const projected1_type& Hx1, const CoordinateDT& x2, const projected2_type& Hix2)

/**
 * @brief Projects a range of coordinates via mH
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] x1Range A list of coordinates in the source domain
 * @return Projection of x1Range to the destination domain
 * @pre \c flagValid=true
 */
template<class MatrixT>
template<class CoordinateRangeT>
auto SymmetricTransferErrorC<MatrixT>::Project(const CoordinateRangeT& x1Range) const -> vector<projected1_type>
{
    //Preconditions
    assert(flagValid);
    return CoordinateTransformationsDT::NormaliseHomogeneous(CoordinateTransformationsST::HomogeneousProject(x1Range, mH));
}   //vector<Coordinate2T> Project(const CoordinateRangeT& x1Range)

/**
 * @brief Projects a range of coordinates via mH^-1
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] x2Range A list of coordinates in the destination domain
 * @return Projection of x2Range to the source domain
 * @pre \c flagValid=true
 */
template<class MatrixT>
template<class CoordinateRangeT>
auto SymmetricTransferErrorC<MatrixT>::InverseProject(const CoordinateRangeT& x2Range) const -> vector<projected2_type>
{
    //Preconditions
    assert(flagValid);
    return CoordinateTransformationsST::NormaliseHomogeneous(CoordinateTransformationsDT::HomogeneousProject(x2Range, mHi));
}   //vector<Coordinate2T> InverseProject(const CoordinateRangeT& x1Range)

/**
 * @brief Computes the distance
 * @param[in] projector Homography
 * @param[in] x1 Point in the source domain
 * @param[in] x2 Point in the destination domain
 * @return Symmetric transfer error
 * @pre \c projector is invertible (unenforced)
 * @remarks Inefficient for multiple pairs
 */
template<class MatrixT>
auto SymmetricTransferErrorC<MatrixT>::operator()(const projector_type& projector, const CoordinateST& x1, const CoordinateDT& x2) const -> RealT
{
    //Preconditions
    assert(flagValid);

    projector_type inverted=projector.inverse();
    return ComputeFromProjected(x1, (projector*x1.homogeneous()).eval().hnormalized(), x2, (inverted*x2.homogeneous()).eval().hnormalized() );
}   //RealT operator()(const Coordinate1T& operand1, const Coordinate2T& operand2)

/**
 * @brief Computes the Jacobian of the error wrt/projector
 * @param[in] x1 First coordinate
 * @param[in] x2 Second coordinate
 * @return Error. Invalid if the Jacobian computation fails
 */
template<class MatrixT>
auto SymmetricTransferErrorC<MatrixT>::ComputeJacobiandP(const CoordinateST& x1, const CoordinateDT& x2) const -> optional<RowVectorXT>
{
	//dT(A,x,y) dA
	TransferErrorC<MatrixT> transferError(mH);
	RealT error1=transferError(x1,x2);
	optional<RowVectorXT> vJ1=transferError.ComputeJacobiandP(x1,x2);

	if(!vJ1)
		return vJ1;

	//dT(A^-1, y, x) d A^-1
	transferError.SetProjector(mHi);
	RealT error2=transferError(x2,x1);
	optional<RowVectorXT> vJ2=transferError.ComputeJacobiandP(x2,x1);

	if(!vJ2)
		return vJ2;

	//dA^-1 dA
	typedef Matrix<RealT, -1, -1> MatrixJT;
	MatrixJT dAidA=JacobiandAidA<MatrixJT>(mH);

	//dT(A^-1, y, x) dA= dT(A^-1, y, x) dA^-1 * dA^-1 dA
	RowVectorXT dT2dA= (*vJ2)*dAidA;

	RealT error=hypot(error1, error2);

	return RowVectorXT( (error1/error)* (*vJ1) + (error2/error)*dT2dA );
}	//MatrixT ComputeJacobiandM(const CoordinateST& x1, const CoordinateDT& x2)

/**
 * @brief Computes the outlier rejection threshold for a specified inlier rejection probability and noise variance
 * @param[in] pReject Probability of rejection of an inlier
 * @param[in] noiseVariance Variance of the noise on the individual coordinates
 * @return Outlier threshold, in units of the destination domain (i.e. not its square!)
 * @warning The function assumes that the noise is only in the destination domain. If the source domain is corrupted as well, \c noiseVariance should be adjusted accordingly
 */
template<class MatrixT>
auto SymmetricTransferErrorC<MatrixT>::ComputeOutlierThreshold(RealT pReject, RealT noiseVariance) -> RealT
{
	return sqrt(quantile(complement(chi_squared_distribution<RealT>(nDOF),pReject))*noiseVariance);
}	//auto ComputeThreshold(RealT pReject, RealT noiseVariance) -> RealT

/**
 * @brief Returns a constant reference to mH
 * @return A reference to mH
 * @pre \c flagValid=true
 */
template<class MatrixT>
auto SymmetricTransferErrorC<MatrixT>::GetProjector() const -> const MatrixT&
{
    //Preconditions
    assert(flagValid);
    return mH;
}   //MatrixT& Projector()

/**
 * @brief Sets mH
 * @param[in] mmH New mH
 * @pre \c mmH is invertible
 * @post \c mH and \c mHi are modified
 */
template<class MatrixT>
void SymmetricTransferErrorC<MatrixT>::SetProjector(const MatrixT& mmH)
{
    //Preconditions
    assert(mmH.determinant()!=0);

    flagValid=true;
    mH=mmH;
    mHi=mmH.inverse();
}   //MatrixT& Projector()

/**
 * @brief Computational cost of the operation
 * @return \c cost
 * @remarks No unit tests, as the costs are volatile
 */
template<class MatrixT>
double SymmetricTransferErrorC<MatrixT>::Cost() const
{
	return cost;
}	//float Cost()

/********** EpipolarSampsonErrorC **********/

/**
 * @brief Projects a range of coordinates via mF
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] x1Range A list of coordinates in the reference image
 * @return Corresponding epipolar lines in the target image
 * @pre \c flagValid=true
 */
template<class CoordinateRangeT>
auto EpipolarSampsonErrorC::Project(const CoordinateRangeT& x1Range) const -> vector<HCoordinate2DT>
{
    //Preconditions
    assert(flagValid);
    return CoordinateTransformations2DT::ApplyProjectiveTransformation(x1Range, mF);
}   //vector<Coordinate2T> Project(const CoordinateRangeT& x1Range)

/**
 * @brief Projects a range of coordinates via mF^T
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] x2Range A list of coordinates in the target image
 * @return Corresponding epipolar lines in the reference image
 * @pre \c flagValid=true
 */
template<class CoordinateRangeT>
auto EpipolarSampsonErrorC::InverseProject(const CoordinateRangeT& x2Range) const -> vector<HCoordinate2DT>
{
    //Preconditions
    assert(flagValid);
    return CoordinateTransformations2DT::ApplyProjectiveTransformation(x2Range, mFt);
}   //vector<Coordinate2T> InverseProject(const CoordinateRangeT& x2Range)

}   //MetricsN
}	//SeeSawN

#endif /* GEOMETRIC_ERROR_IPP_2121562 */
