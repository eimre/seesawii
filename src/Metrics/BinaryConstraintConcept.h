/*
 * @file BinaryConstraintConcept.h Public interface for BinaryConstraintConceptC
 * @author Evren Imre
 * @date 30 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BINARY_CONSTRAINT_CONCEPT_H_5131562
#define BINARY_CONSTRAINT_CONCEPT_H_5131562

#include "BinaryConstraintConcept.ipp"

namespace SeeSawN
{
namespace MetricsN
{
template<class TestT, class Range1T, class Range2T> class BinaryConstraintConceptC;   ///< Pairwise constraint concept
}   //MetricsN
}	//SeeSawN

#endif /* BINARY_CONSTRAINT_CONCEPT_H_5131562 */
