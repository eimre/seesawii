/**
 * @file GeometricConstraint.h Public interface for GeometricConstraintC
 * @author Evren Imre
 * @date 31 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_CONSTRAINT_H_5212565
#define GEOMETRIC_CONSTRAINT_H_5212565

#include "GeometricConstraint.ipp"
#include <vector>
#include "GeometricError.h"
namespace SeeSawN
{
namespace MetricsN
{

template<class ErrorT> class GeometricConstraintC;  ///< Verifies geometric error constraints

/********** EXTERN TEMPLATES **********/
using SeeSawN::MetricsN::TransferErrorH32DT;
typedef GeometricConstraintC<TransferErrorH32DT> ReprojectionErrorConstraintT;
extern template class GeometricConstraintC<TransferErrorH32DT>;

using std::vector;
extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<TransferErrorH32DT>::BatchConstraintEvaluation(const vector<typename TransferErrorH32DT::first_argument_type>&, const vector<typename TransferErrorH32DT::second_argument_type>&) const;

using SeeSawN::MetricsN::SymmetricTransferErrorH2DT;
typedef GeometricConstraintC<SymmetricTransferErrorH2DT> SymmetricTransferErrorConstraint2DT;
extern template class GeometricConstraintC<SymmetricTransferErrorH2DT>;
extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<SymmetricTransferErrorH2DT>::BatchConstraintEvaluation(const vector<typename SymmetricTransferErrorH2DT::first_argument_type>&, const vector<typename SymmetricTransferErrorH2DT::second_argument_type>&) const;

using SeeSawN::MetricsN::SymmetricTransferErrorH3DT;
typedef GeometricConstraintC<SymmetricTransferErrorH3DT> SymmetricTransferErrorConstraint3DT;
extern template class GeometricConstraintC<SymmetricTransferErrorH3DT>;
extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<SymmetricTransferErrorH3DT>::BatchConstraintEvaluation(const vector<typename SymmetricTransferErrorH3DT::first_argument_type>&, const vector<typename SymmetricTransferErrorH3DT::second_argument_type>&) const;

using SeeSawN::MetricsN::EpipolarSampsonErrorC;
typedef GeometricConstraintC<EpipolarSampsonErrorC> EpipolarSampsonConstraintT;
extern template class GeometricConstraintC<EpipolarSampsonErrorC>;
extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<EpipolarSampsonErrorC>::BatchConstraintEvaluation(const vector<typename EpipolarSampsonErrorC::first_argument_type>&, const vector<typename EpipolarSampsonErrorC::second_argument_type>&) const;


}   //MetricsN
}	//SeeSawN

#endif /* GEOMETRIC_CONSTRAINT_H_5212565 */
