/**
 * @file SceneFeatureDistance.h Public interface for scene feature distance metrics
 * @author Evren Imre
 * @date 28 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_FEATURE_DISTANCE_H_0790832
#define SCENE_FEATURE_DISTANCE_H_0790832

#include "SceneFeatureDistance.ipp"
#include "Distance.h"
namespace SeeSawN
{
namespace MetricsN
{

template<class DistanceT> class SceneImageFeatureDistanceC;	///< Measures the distance between a scene and an image feature
template<class DistanceT> class SceneFeatureDistanceC;	///< Measures the distance between two scene features

/********** EXTERN TEMPLATES **********/
using SeeSawN::MetricsN::EuclideanDistanceIDT;
extern template class SceneImageFeatureDistanceC<EuclideanDistanceIDT>;
typedef SceneImageFeatureDistanceC<EuclideanDistanceIDT> EuclideanSceneImageFeatureDistanceT;

extern template class SceneFeatureDistanceC<EuclideanDistanceIDT>;
typedef SceneFeatureDistanceC<EuclideanDistanceIDT> EuclideanSceneFeatureDistanceT;


}	//MetricsN
}	//SeeSawN

#endif /* SCENEFEATUREDISTANCE_H_ */
