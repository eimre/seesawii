/*
 * @file TestMetrics.cpp Unit tests for Metrics
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/


#define BOOST_TEST_MODULE METRICS

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include "Distance.h"
#include "Similarity.h"
#include "Constraint.h"
#include "GeometricError.h"
#include "GeometricConstraint.h"
#include "MatchAmbiguityMetric.h"
#include "RobustLossFunction.h"
#include "CheiralityConstraint.h"
#include "SceneFeatureDistance.h"
#include "Polygon2DConstraint.h"
#include "BinarySceneFeatureDistance.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/BoostDynamicBitset.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestMetricsN
{

using namespace SeeSawN::MetricsN;

using boost::optional;
using Eigen::Array;
using Eigen::RowVectorXd;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ImageDescriptorT;
using SeeSawN::ElementsN::BinaryImageDescriptorT;
using SeeSawN::WrappersN::MakeBitset;
using std::vector;
using std::make_tuple;
using std::tie;

BOOST_AUTO_TEST_SUITE(Distance_Metrics)

BOOST_AUTO_TEST_CASE(Simple_Metrics)
{
    typedef typename EuclideanDistanceIDT::first_argument_type Argument1T;
    typedef typename EuclideanDistanceIDT::second_argument_type Argument2T;

    Argument1T v1(2); v1<<1,2;
    Argument2T v2(2); v2<<4,-1;

    //Euclidean distance
    EuclideanDistanceIDT euclid1;
    BOOST_CHECK_CLOSE(euclid1(v1,v2), (v1-v2).norm(), 1e-16);

    //X2 distance

    Argument1T h1(5); h1<<5,0,2,4,1; h1.normalize();
    Argument2T h2(5); h2<<1,0,5,2,4; h2.normalize();

    ChiSquaredDistanceIDT chi2;
    BOOST_CHECK_CLOSE(chi2(h1, h2), 0.687908, 5e-3);

    //Hamming distance
    vector<unsigned int>indexSet1{1, 2, 4, 8};
    BinaryImageDescriptorT bitset1=MakeBitset<BinaryImageDescriptorT>(indexSet1, 12);

    vector<unsigned int>indexSet2{2, 3, 4, 5, 9, 10, 11};
	BinaryImageDescriptorT bitset2=MakeBitset<BinaryImageDescriptorT>(indexSet2, 12);

	HammingDistanceBIDT hamming;
	BOOST_CHECK_EQUAL(hamming(bitset1, bitset2), (bitset1^bitset2).count());
}   //BOOST_AUTO_TEST_CASE(Simple_Metrics)

BOOST_AUTO_TEST_CASE(Scene_Feature_Distance)
{
	//Data
	vector<ImageDescriptorT> imageDescriptors(4, ImageDescriptorT(4));
	imageDescriptors[0]<<0, 2, 1, 1;
	imageDescriptors[1]<<3, 2, 4, 1;
	imageDescriptors[2]<<1, 6, 1, 5;
	imageDescriptors[3]<<1, 4, 2, 1;

	vector<ImageFeatureC> imageFeatures(4);
	for(size_t c=0; c<4; ++c)
		imageFeatures[c]=ImageFeatureC(Coordinate2DT::Random(), imageDescriptors[c].normalized(), ImageFeatureC::roi_type::Random(1));

	//Scene features
	typedef SceneFeatureC::descriptor_type SceneDescriptorT;
	SceneDescriptorT trajectory1{{0, imageFeatures[0]}, {1, imageFeatures[1]}};
	SceneDescriptorT trajectory2{{2, imageFeatures[2]}, {3, imageFeatures[3]}};
	SceneDescriptorT trajectory3{{0, imageFeatures[0]}, {3, imageFeatures[3]}};

	//SceneImageFeatureDistanceC
	SceneImageFeatureDistanceC<EuclideanDistanceIDT> distanceSI;

	BOOST_CHECK_SMALL(distanceSI(trajectory1, imageDescriptors[0].normalized()), (float)1e-6);
	BOOST_CHECK_CLOSE(distanceSI(trajectory2, imageDescriptors[0].normalized()), 0.29180, (float)1e-3);

	//SceneFeatureDistanceC
	SceneFeatureDistanceC<EuclideanDistanceIDT> distanceSS;
	BOOST_CHECK_SMALL(distanceSS(trajectory1, trajectory3), (float)1e-6);
	BOOST_CHECK_CLOSE(distanceSS(trajectory1, trajectory2), 0.29180, (float)1e-3);
}	//BOOST_AUTO_TEST_CASE(Scene_Feature_Distance)

BOOST_AUTO_TEST_CASE(Binary_Scene_Feature_Distance)
{
	vector<BinaryImageDescriptorT> imageDescriptors(4);
    imageDescriptors[0]=MakeBitset<BinaryImageDescriptorT>(vector<unsigned int>{1, 2, 4, 8}, 12);
    imageDescriptors[1]=MakeBitset<BinaryImageDescriptorT>(vector<unsigned int>{2, 6}, 12);
    imageDescriptors[2]=MakeBitset<BinaryImageDescriptorT>(vector<unsigned int>{1, 9,10}, 12);
    imageDescriptors[3]=MakeBitset<BinaryImageDescriptorT>(vector<unsigned int>{1, 2, 3, 6, 7}, 12);

    vector<BinaryImageFeatureC> imageFeatures(4);
	for(size_t c=0; c<4; ++c)
		imageFeatures[c]=BinaryImageFeatureC(Coordinate2DT::Random(), imageDescriptors[c], BinaryImageFeatureC::roi_type::Random(1));

	//Scene features
	typedef OrientedBinarySceneFeatureC::descriptor_type SceneDescriptorT;
	SceneDescriptorT trajectory1{{0, imageFeatures[0]}, {1, imageFeatures[1]}};
	SceneDescriptorT trajectory2{{2, imageFeatures[2]}, {3, imageFeatures[3]}};
	SceneDescriptorT trajectory3{{0, imageFeatures[0]}, {3, imageFeatures[3]}};

	//SceneImageFeatureDistanceC
	BinarySceneImageFeatureDistanceC<HammingDistanceBIDT> distanceSI;

	BOOST_CHECK_EQUAL(distanceSI(trajectory1, imageDescriptors[0]), 0);
	BOOST_CHECK_EQUAL(distanceSI(trajectory2, imageDescriptors[0]), 5);

	//SceneFeatureDistanceC
	BinarySceneFeatureDistanceC<HammingDistanceBIDT> distanceSS;
	BOOST_CHECK_EQUAL(distanceSS(trajectory1, trajectory3), 0);
	BOOST_CHECK_EQUAL(distanceSS(trajectory1, trajectory2), 3);
}	//BOOST_AUTO_TEST_CASE(Binary_Scene_Feature_Distance)

BOOST_AUTO_TEST_SUITE_END() //Distance_Metrics

BOOST_AUTO_TEST_SUITE(Similarity_Metrics)

BOOST_AUTO_TEST_CASE(Simple_Metrics)
{
    typedef typename EuclideanDistanceIDT::first_argument_type Argument1T;
    typedef typename EuclideanDistanceIDT::second_argument_type Argument2T;

    Argument1T v1(2); v1<<1,2;
    Argument2T v2(2); v2<<4,-1;

    EuclideanDistanceIDT euclid1;
    typedef ReciprocalC<typename ValueTypeM<Argument1T>::type > MapperT;
    MapperT reciprocal;
    DistanceToSimilarityConverterC<EuclideanDistanceIDT, MapperT> e2s(euclid1, reciprocal);

    BOOST_CHECK_CLOSE(e2s(v1,v2), reciprocal( euclid1(v1,v2)), 1e-16);
    BOOST_CHECK_CLOSE(e2s.Invert(e2s(v1,v2)), euclid1(v1,v2), 1e-16);
}   //BOOST_AUTO_TEST_CASE(Simple_Metrics)

BOOST_AUTO_TEST_SUITE_END() //Similarity_Metrics

BOOST_AUTO_TEST_SUITE(Geometric_Error)

BOOST_AUTO_TEST_CASE(Transfer_Error)
{
    typedef TransferErrorH32DT::result_type RealT;

    typedef TransferErrorH32DT::projector_type MatrixT;
    MatrixT mH;
    mH=MatrixT::Zero();

    typedef TransferErrorH32DT::first_argument_type Coordinate1T;
    vector<Coordinate1T> coordinates1(2);
    coordinates1[0]<<2, 1, 1;
    coordinates1[1]<<3, -1, 2;

    typedef TransferErrorH32DT::second_argument_type Coordinate2T;
    vector<Coordinate2T> coordinates2(2);
    coordinates2[0]<<1.15, 0.75;
    coordinates2[1]<<-2, 4;

    TransferErrorH32DT evaluator(mH);
    mH<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1;
    evaluator.SetProjector(mH);
    BOOST_CHECK(mH==evaluator.GetProjector());

    RealT error1=evaluator(coordinates1[0], coordinates2[0]);
    RealT error2=evaluator(coordinates1[1], coordinates2[1]);

    typedef TransferErrorH32DT::projected1_type Projected1T;
    vector<Projected1T> projected1=evaluator.Project(coordinates1);

    BOOST_CHECK_CLOSE(error1, 0.29155, 1e-3);
    BOOST_CHECK_CLOSE(error1, evaluator.ComputeFromProjected(projected1[0], coordinates2[0]), 1e-4);
    BOOST_CHECK_CLOSE(error2, evaluator.ComputeFromProjected(projected1[1], coordinates2[1]), 1e-4);

    TransferErrorH32DT evaluator2(mH);
    BOOST_CHECK_EQUAL(evaluator2(mH, coordinates1[0], coordinates2[0]), error1);

    //Jacobian
    MatrixT mP; mP<<890.648,-183.749,1714.6,4453.79,180.218,-1766.68,-3.83591,3734.81,0.980423,-0.196894,0.0017298,4.89515;
    Coordinate1T x1; x1<<0.031848, 0.16923, 0.447124;
    Coordinate2T x2; x2<<1010.2, 601.75;
    TransferErrorH32DT evaluator3(mP);
    optional<TransferErrorH32DT::jacobian_type> vJ=evaluator3.ComputeJacobiandP(x1, x2);

    TransferErrorH32DT::jacobian_type vJr(12); vJr<<0.00315111,0.016744,0.0442394,0.0989422,0.00569401,0.0302562,0.0799401,0.178787,-7.36196,-39.1191,-103.357,-231.159;
    BOOST_CHECK(vJ);
    BOOST_CHECK(vJ->isApprox(vJr, 1e-3));

    //Outlier threshold
    BOOST_CHECK_CLOSE(TransferErrorH32DT::ComputeOutlierThreshold(0.05, 1), sqrt(5.991), 1e-2);
}   //BOOST_AUTO_TEST_CASE(Transfer_Error)

BOOST_AUTO_TEST_CASE(Symmetric_Transfer_Error)
{
    typedef SymmetricTransferErrorH2DT::result_type RealT;

    typedef SymmetricTransferErrorH2DT::projector_type MatrixT;
    MatrixT mH;
    mH<<2, 0, 1, 0, 3, -1, 0, 0, 1;

    typedef SymmetricTransferErrorH2DT::first_argument_type Coordinate1T;
    vector<Coordinate1T> coordinates1(2);
    coordinates1[0]<<2, 1;
    coordinates1[1]<<3, -1;

    typedef SymmetricTransferErrorH2DT::second_argument_type Coordinate2T;
    vector<Coordinate2T> coordinates2(2);
    coordinates2[0]<<0, 3;
    coordinates2[1]<<-2, 4;

    SymmetricTransferErrorH2DT evaluator(mH);
    mH(0,0)=4;
    evaluator.SetProjector(mH);
    BOOST_CHECK(mH==evaluator.GetProjector());

    RealT error1=evaluator(coordinates1[0], coordinates2[0]);
    RealT error2=evaluator(coordinates1[1], coordinates2[1]);

    typedef SymmetricTransferErrorH2DT::projected1_type Projected1T;
    vector<Projected1T> projected1=evaluator.Project(coordinates1);

    typedef SymmetricTransferErrorH2DT::projected2_type Projected2T;
    vector<Projected2T> projected2=evaluator.InverseProject(coordinates2);

    BOOST_CHECK_CLOSE(error1, 9.3366, 1e-3);
    BOOST_CHECK_CLOSE(error1, evaluator.ComputeFromProjected(coordinates1[0], projected1[0], coordinates2[0], projected2[0]), 1e-4);
    BOOST_CHECK_CLOSE(error2, evaluator.ComputeFromProjected(coordinates1[1], projected1[1], coordinates2[1], projected2[1]), 1e-4);

    SymmetricTransferErrorH2DT evaluator2;
    BOOST_CHECK_EQUAL(evaluator2(mH, coordinates1[0], coordinates2[0]), error1);

    //Jacobian
    MatrixT mH3; mH3<<0.801406,0.12776,-0.584317,-0.123494,0.991214,0.0473526,0.585233,0.0342115,0.810142;
    Coordinate1T x1; x1<<1032.46,695.364;
    Coordinate2T x2; x2<<1010.2,601.75;

    SymmetricTransferErrorH2DT evaluator3(mH3);
    optional<SymmetricTransferErrorH2DT::jacobian_type> vJ=evaluator3.ComputeJacobiandP(x1,x2);

    SymmetricTransferErrorH2DT::jacobian_type vJr(9); vJr<<-0.659702,-0.348223,-0.23532,-1.08911,-0.89384,0.390548,0.040643,-0.560874,1.43687;
    BOOST_CHECK(vJ);
    BOOST_CHECK(vJ->isApprox(vJr, 1e-3));

    //Outlier threshold
	BOOST_CHECK_CLOSE(SymmetricTransferErrorH2DT::ComputeOutlierThreshold(0.05, 1), sqrt(9.488), 1e-2);
}   //BOOST_AUTO_TEST_CASE(Symmetric_Transfer_Error)

BOOST_AUTO_TEST_CASE(Epipolar_Sampson_Error)
{
    typedef EpipolarSampsonErrorC::result_type RealT;
    typedef EpipolarSampsonErrorC::projector_type MatrixT;
    MatrixT mF; mF<<0.941524, -0.108497, 0.274965, -0.144663, 0.023072, -0.021805,  0.013674, 0.066155, 0.220270;

    typedef EpipolarSampsonErrorC::first_argument_type Coordinate1T;
    vector<Coordinate1T> coordinates1(2);
    coordinates1[0]<<2, 1;
    coordinates1[1]<<3, -1;

    typedef EpipolarSampsonErrorC::second_argument_type Coordinate2T;
    vector<Coordinate2T> coordinates2(2);
    coordinates2[0]<<0, 3;
    coordinates2[1]<<-2, 4;

    EpipolarSampsonErrorC evaluator(MatrixT::Identity());
    evaluator.SetProjector(mF);
    BOOST_CHECK(mF==evaluator.GetProjector());

    RealT error1=evaluator(coordinates1[0], coordinates2[0]);
    RealT error2=evaluator(coordinates1[1], coordinates2[1]);

    typedef EpipolarSampsonErrorC::projected1_type Projected1T;
    vector<Projected1T> projected1=evaluator.Project(coordinates1);

    typedef EpipolarSampsonErrorC::projected2_type Projected2T;
    vector<Projected2T> projected2=evaluator.InverseProject(coordinates2);

    BOOST_CHECK_CLOSE(error1, 0.260085, 1e-3);
    BOOST_CHECK_CLOSE(error1, evaluator.ComputeFromProjected(coordinates1[0], projected1[0], coordinates2[0], projected2[0]), 1e-4);
    BOOST_CHECK_CLOSE(error2, evaluator.ComputeFromProjected(coordinates1[1], projected1[1], coordinates2[1], projected2[1]), 1e-4);

    EpipolarSampsonErrorC evaluator2;
    BOOST_CHECK_EQUAL(evaluator2(mF, coordinates1[0], coordinates2[0]), error1);

    //Outlier threshold
   	BOOST_CHECK_CLOSE(EpipolarSampsonErrorC::ComputeOutlierThreshold(0.05, 1), sqrt(3.841), 1e-2);

   	//Jacobian
   	EpipolarMatrixT mFj; mFj<< 3.74882e-08,2.85887e-06,-0.000385163,3.70286e-06,-1.46293e-08,0.0136915,-0.000730566,-0.0201989,0.999702;
    vector<Coordinate2T> coordinates3(2);
    coordinates3[0]<<1334.86,37.939;
    coordinates3[1]<<756.252,37.3691;
    EpipolarSampsonErrorC evaluator3(mFj);

    EpipolarSampsonErrorC::jacobian_type mJr(1,9); mJr<<-3.89058e+07,-936420,-29150,-2.23174e+06,-55061.6,-1672.08,-51450.6,-1238.38,-38.549;
    optional<EpipolarSampsonErrorC::jacobian_type> mJ=evaluator3.ComputeJacobiandP(coordinates3[0], coordinates3[1]);
    BOOST_CHECK(mJ);
    BOOST_CHECK(mJ->isApprox(mJr, 1e-3));

    //Jacobian wrt/coordinates
    RowVectorXd mJxr(4); mJxr<<0.0208781,0.695337, 0.00937801,-0.71829;
    optional<RowVectorXd> mJx=evaluator3.ComputeJacobiandx1x2(coordinates3[0], coordinates3[1]);
    BOOST_CHECK(mJx);
    BOOST_CHECK(mJx->isApprox(mJxr,1e-5));
}   //BOOST_AUTO_TEST_CASE(Epipolar_Sampson_Error)

BOOST_AUTO_TEST_SUITE_END()   //Geometric_Error

BOOST_AUTO_TEST_SUITE(Constraints)

BOOST_AUTO_TEST_CASE(Dummy_Constraint)
{
	typedef DummyConstraintC<double, double> ConstraintT;

	ConstraintT constraint;
	BOOST_CHECK(constraint(0,0));

	vector<double> set1{0,2};
	vector<double> set2{0,2,3};
	Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArray=constraint.BatchConstraintEvaluation(set1, set2);
	BOOST_CHECK(constraintArray.all());

	bool flagTrue;
	double distanceZero;
	tie(flagTrue, distanceZero)=constraint.Enforce(1,2);
	BOOST_CHECK(flagTrue);
	BOOST_CHECK(distanceZero==0);
}	//BOOST_AUTO_TEST_CASE(Dummy_Constraint)

BOOST_AUTO_TEST_CASE(Distance_Constraint)
{
    typedef DistanceConstraintC<EuclideanDistance2DT> DistanceT;
    DistanceT distanceConstraint(EuclideanDistance2DT(), 1);

    typedef typename EuclideanDistance2DT::first_argument_type Argument1T;
    typedef typename EuclideanDistance2DT::second_argument_type Argument2T;

    Argument1T v1(0, 1);
    Argument2T v2(0.5, 0.5);
    BOOST_CHECK_EQUAL(distanceConstraint(v1, v2), true );

    Argument2T v3(0, 9);
    BOOST_CHECK_EQUAL(distanceConstraint(v1, v3), false);

    vector<Argument1T> set1{v1, v2};
    vector<Argument1T> set2{v3, v1};
    Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArray=distanceConstraint.BatchConstraintEvaluation(set1, set2);

    Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArrayr(2,2); constraintArrayr<<false, true, false, true;
    BOOST_CHECK( (constraintArray==constraintArrayr).all() );

    bool flagPass4;
    DistanceT::distance_type dist4;
    tie(flagPass4, dist4)=distanceConstraint.Enforce(v1,v2);
    BOOST_CHECK(flagPass4);
    BOOST_CHECK_CLOSE((double)dist4, sqrt(0.5), 2.5e-6);
}   //BOOST_AUTO_TEST_CASE(Distance_Constraint)

BOOST_AUTO_TEST_CASE(Geometric_Constraint_Arity2)
{
    typedef TransferErrorH32DT::projector_type MatrixT;
    MatrixT mH;
    mH<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1;
    TransferErrorH32DT error(mH);

    typedef TransferErrorH32DT::first_argument_type Coordinate1T;
    vector<Coordinate1T> coordinates1(2);
    coordinates1[0]<<2, 1, 1;
    coordinates1[1]<<3, -1, 2;

    typedef TransferErrorH32DT::second_argument_type Coordinate2T;
    vector<Coordinate2T> coordinates2(2);
    coordinates2[0]<<1.15, 0.75;
    coordinates2[1]<<-2, 4;

    GeometricConstraintC<TransferErrorH32DT> constraint(error, 1);

    BOOST_CHECK_EQUAL(constraint(coordinates1[0], coordinates2[0]), true );
    BOOST_CHECK_EQUAL(constraint(coordinates1[0], coordinates2[1]), false );

    Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArray=constraint.BatchConstraintEvaluation(coordinates1, coordinates2);
    Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArrayr(2,2); constraintArrayr<<true, false, false, false;
    BOOST_CHECK( (constraintArray==constraintArrayr).all() );

    BOOST_CHECK(constraint.Enforce(coordinates1[0], coordinates2[1])==make_tuple(false, error(coordinates1[0], coordinates2[1])));
    BOOST_CHECK(constraint.Enforce(mH, coordinates1[0], coordinates2[1])==make_tuple(false, error(mH, coordinates1[0], coordinates2[1])));

    BOOST_CHECK(constraint(mH, coordinates1[0], coordinates2[0]));
    BOOST_CHECK(!constraint(mH, coordinates1[0], coordinates2[1]));

    BOOST_CHECK(constraint.GetErrorMetric()(coordinates1[0], coordinates2[1])==error(coordinates1[0], coordinates2[1]));

    MatrixT mH2;  mH2<<1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1;
    constraint.SetProjector(mH2);
    BOOST_CHECK(constraint.GetErrorMetric()(coordinates1[0], coordinates2[1])==error(mH2, coordinates1[0], coordinates2[1]));
}   //BOOST_AUTO_TEST_CASE(Geometric_Constraint_Arity2)

BOOST_AUTO_TEST_CASE(Geometric_Constraint_Arity4)
{
    typedef EpipolarSampsonErrorC::projector_type MatrixT;
    MatrixT mF; mF<<0.941524, -0.108497, 0.274965, -0.144663, 0.023072, -0.021805,  0.013674, 0.066155, 0.220270;
    EpipolarSampsonErrorC error(mF);

    typedef EpipolarSampsonErrorC::first_argument_type Coordinate1T;
    vector<Coordinate1T> coordinates1(2);
    coordinates1[0]<<2, 1;
    coordinates1[1]<<3, -1;

    typedef EpipolarSampsonErrorC::second_argument_type Coordinate2T;
    vector<Coordinate2T> coordinates2(2);
    coordinates2[0]<<0, 3;
    coordinates2[1]<<-2, 4;

    GeometricConstraintC<EpipolarSampsonErrorC> constraint(error, 0.3);
    BOOST_CHECK_EQUAL(constraint(coordinates1[0], coordinates2[0]), true );
    BOOST_CHECK_EQUAL(constraint(coordinates1[0], coordinates2[1]), false );

    Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArray=constraint.BatchConstraintEvaluation(coordinates1, coordinates2);
    Array<bool, Eigen::Dynamic, Eigen::Dynamic> constraintArrayr(2,2); constraintArrayr<<true, false, false, false;
    BOOST_CHECK( (constraintArray==constraintArrayr).all() );
}   //BOOST_AUTO_TEST_CASE(Geometric_Constraint_Arity4)

BOOST_AUTO_TEST_SUITE_END() //Constraints

BOOST_AUTO_TEST_SUITE(Match_Ambiguity_Metric)

BOOST_AUTO_TEST_CASE(Rayleigh_MR)
{

    typedef ReciprocalC<EuclideanDistanceIDT::result_type> MapperT;
    typedef InverseEuclideanIDConverterT SimilarityMetricT;
    MapperT distanceToSimilarityMap;
    SimilarityMetricT similarityMetric(EuclideanDistanceIDT(), distanceToSimilarityMap);

	Array<EuclideanDistanceIDT::result_type, Eigen::Dynamic, Eigen::Dynamic> similarityCache(2,2); similarityCache<<1, 0.25, 0.5, 3;
	Array<bool, Eigen::Dynamic, Eigen::Dynamic> similarityValidityArray(2,2); similarityValidityArray<<true, true, true, false;
	optional<Array<bool, Eigen::Dynamic, Eigen::Dynamic> > constraintCache1;

	CorrespondenceListT correspondences1;
	correspondences1.push_back(CorrespondenceListT::value_type(0, 0, MatchStrengthC(1, 0) ));
	correspondences1.push_back(CorrespondenceListT::value_type(0, 1, MatchStrengthC(0.25, 0) ));
	correspondences1.push_back(CorrespondenceListT::value_type(1, 0, MatchStrengthC(0.5, 0) ));

	RayleighMetaRecognitionMetricC<SimilarityMetricT> rayleighMR;
	rayleighMR(correspondences1, similarityMetric, similarityCache, similarityValidityArray, constraintCache1, 1, 3);
	BOOST_CHECK_EQUAL(correspondences1.begin()->info.Ambiguity(), 0.25);
	BOOST_CHECK_EQUAL(next(correspondences1.begin(),1)->info.Ambiguity(), 0.0625);
	BOOST_CHECK_EQUAL(next(correspondences1.begin(),2)->info.Ambiguity(), 0.25);
}	//BOOST_AUTO_TEST_CASE(Rayleigh_MR)

BOOST_AUTO_TEST_SUITE_END()	//Match_Ambiguity_Metric

BOOST_AUTO_TEST_SUITE(Robust_Loss_Functions)

BOOST_AUTO_TEST_CASE(Pseudo_Huber)
{
	PseudoHuberLossC<double> huber;
	BOOST_CHECK_SMALL(huber(0), 1e-6);
	BOOST_CHECK_CLOSE(huber(0.5), 2*(sqrt(1.25)-1), 1e-6);
	BOOST_CHECK_CLOSE(huber.ComputeFitness(0.5), 0.5, 1e-6);
	BOOST_CHECK_EQUAL(huber.ComputeFitness(0.5), huber.ConvertToFitness(huber(0.5)));

	huber.Configure(4, 0.75);

	PseudoHuberLossC<double> huber2(4,0.75);
	BOOST_CHECK_EQUAL(huber2(0.25), huber(0.25));
	BOOST_CHECK_EQUAL(huber2.ComputeFitness(0.25), huber.ComputeFitness(0.25));
	BOOST_CHECK_EQUAL(huber2.ConvertToFitness(0.25), huber.ConvertToFitness(0.25));
}	//BOOST_AUTO_TEST_CASE(Pseudo_Huber)

BOOST_AUTO_TEST_SUITE_END()	//BOOST_AUTO_TEST_CASE(Pseudo_Huber)

BOOST_AUTO_TEST_SUITE(Cheirality_Constraint)

BOOST_AUTO_TEST_CASE(Cheirality_Test)
{
	CameraMatrixT mP; mP.setZero(); mP.block(0,0,3,3).setIdentity();

	CheiralityConstraintC tester(mP);

	Coordinate3DT x1(2, 2, 1);
	BOOST_CHECK(tester(x1));
	BOOST_CHECK_EQUAL(1, tester.Distance(x1));

	Coordinate3DT x2(-2, -2, -1);
	BOOST_CHECK(!tester(x2));
	BOOST_CHECK_EQUAL(-1, tester.Distance(x2));

	BOOST_CHECK_EQUAL(1, tester.AbsoluteDistance(x1));
	BOOST_CHECK_EQUAL(tester.AbsoluteDistance(x2), tester.AbsoluteDistance(x1));

	//Invalid camera matrix
	CameraMatrixT mP3(mP); mP3(1,1)=0;
	CheiralityConstraintC tester3(mP3);
	BOOST_CHECK_EQUAL(1, tester3.AbsoluteDistance(x1));
	BOOST_CHECK_THROW(tester3.Distance(x1), invalid_argument);
}	//BOOST_AUTO_TEST_CASE(Constraint)

BOOST_AUTO_TEST_SUITE_END()	//BOOST_AUTO_TEST_SUITE(Cheirality_Constraint)

BOOST_AUTO_TEST_SUITE(Polygon_Constraint)

BOOST_AUTO_TEST_CASE(Operation)
{
	Polygon2DConstraintC constraint;
	BOOST_CHECK(!constraint.IsValid());

	vector<Coordinate2DT> vertexList{ Coordinate2DT(0,0), Coordinate2DT(3,3), Coordinate2DT(3,0) };
	Polygon2DConstraintC constraint2(vertexList);
	BOOST_CHECK(constraint2.IsValid());

	BOOST_CHECK(constraint2(Coordinate2DT(0.25, 0.15)));
	BOOST_CHECK(!constraint2(Coordinate2DT(4,5)));
	BOOST_CHECK(constraint2(Coordinate2DT(0,0)));

	//Empty polygon
	vector<Coordinate2DT> vertexList3;
	Polygon2DConstraintC constraint3(vertexList3);
	BOOST_CHECK(!constraint3(Coordinate2DT(0,0)));
}	//BOOST_AUTO_TEST_CASE(Operation)

BOOST_AUTO_TEST_SUITE_END()	//Polygon_Constraint


}   //TestMetricsN
}   //UnitTestsN
}	//SeeSawN


