/*
 * @file BinaryConstraintConcept.ipp Implementation of BinaryConstraintConceptC
 * @author Evren Imre
 * @date 30 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef BINARY_CONSTRAINT_CONCEPT_IPP_3759256
#define BINARY_CONSTRAINT_CONCEPT_IPP_3759256

#include <tuple>
#include <vector>
#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryPredicateConcept;
using boost::Assignable;
using boost::CopyConstructible;
using boost::DefaultConstructible;
using std::tie;
using std::vector;

/**
 * @brief Pairwise constraint concept
 * @tparam TestT Type to be tested
 * @tparam Argument1T An argument
 * @tparam Argument2T An argument
 * @ingroup Concept
 */
template<class TestT, class Argument1T, class Argument2T>
class BinaryConstraintConceptC
{
    ///@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((DefaultConstructible<TestT>));
    BOOST_CONCEPT_ASSERT((Assignable<TestT>));
    BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
    BOOST_CONCEPT_ASSERT((AdaptableBinaryPredicateConcept<TestT, typename TestT::first_argument_type, typename TestT::second_argument_type >));

    private:

        Argument1T* arg1;
        Argument2T* arg2;

    public:

        BOOST_CONCEPT_USAGE(BinaryConstraintConceptC)
        {
            TestT tested;
            vector<Argument1T> range1;
            vector<Argument2T> range2;
            auto tmp=tested.BatchConstraintEvaluation(range1, range2);

            bool flagInlier;
            typename TestT::distance_type error;
            tie(flagInlier, error)=tested.Enforce(*arg1, *arg2);
        }   //BOOST_CONCEPT_USAGE(BinaryConstraintConceptC)
    ///@endcond
};  //class BinaryConstraintConceptC

}   //MetricsN
}	//SeeSawN

#endif /* BINARY_CONSTRAINT_CONCEPT_IPP_3759256 */
