/**
 * @file GeometricError.h Public interaface for various geometric errors
 * @author Evren Imre
 * @date 31 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_ERROR_H_9565324
#define GEOMETRIC_ERROR_H_9565324

#include "GeometricError.ipp"

namespace SeeSawN
{
namespace MetricsN
{

template<class MatrixT> class TransferErrorC;   ///< One-sided transfer error for homogeneous transformations
template<class MatrixT> class SymmetricTransferErrorC;   ///< Symmetric transfer error for homogeneous transformations
class EpipolarSampsonErrorC;    ///< Sampson error for epipolar geometry

namespace DetailN
{
    struct Arity2Tag;   ///< Arity-2 tag
    struct Arity4Tag;   ///< Arity-4 tag
}   //DetailN

/********** EXTERN TEMPLATES **********/

using SeeSawN::ElementsN::CameraMatrixT;
typedef TransferErrorC< CameraMatrixT  > TransferErrorH32DT;  ///< Reprojection error for a projective camera
extern template class TransferErrorC< CameraMatrixT >;
extern template vector<typename TransferErrorH32DT::projected1_type> TransferErrorH32DT::Project(const vector<typename TransferErrorH32DT::first_argument_type>&) const;

using SeeSawN::ElementsN::Homography2DT;
typedef TransferErrorC<Homography2DT> TransferErrorH2DT;	///< 2D transfer error, homogeneous transformations
extern template class TransferErrorC< Homography2DT >;
extern template vector<typename TransferErrorH2DT::projected1_type> TransferErrorH2DT::Project(const vector<typename TransferErrorH2DT::first_argument_type>&) const;

typedef SymmetricTransferErrorC<Homography2DT> SymmetricTransferErrorH2DT;  ///< 2D symmetric transfer error, homogeneous transformations
extern template class SymmetricTransferErrorC<Homography2DT>;
extern template vector<typename SymmetricTransferErrorH2DT::projected1_type> SymmetricTransferErrorH2DT::Project(const vector<typename SymmetricTransferErrorH2DT::first_argument_type>&) const;
extern template vector<typename SymmetricTransferErrorH2DT::projected2_type> SymmetricTransferErrorH2DT::InverseProject(const vector<typename SymmetricTransferErrorH2DT::second_argument_type>&) const;

using SeeSawN::ElementsN::Homography3DT;
typedef SymmetricTransferErrorC<Homography3DT> SymmetricTransferErrorH3DT;  ///< 3D symmetric transfer error, homogeneous transformations
extern template class SymmetricTransferErrorC<Homography3DT>;
extern template vector<typename SymmetricTransferErrorH3DT::projected1_type> SymmetricTransferErrorH3DT::Project(const vector<typename SymmetricTransferErrorH3DT::first_argument_type>&) const;
extern template vector<typename SymmetricTransferErrorH3DT::projected2_type> SymmetricTransferErrorH3DT::InverseProject(const vector<typename SymmetricTransferErrorH3DT::second_argument_type>&) const;

extern template vector<typename EpipolarSampsonErrorC::projected1_type> EpipolarSampsonErrorC::Project(const vector<typename EpipolarSampsonErrorC::first_argument_type>&) const;
extern template vector<typename EpipolarSampsonErrorC::projected2_type> EpipolarSampsonErrorC::InverseProject(const vector<typename EpipolarSampsonErrorC::second_argument_type>&) const;

}   //MetricsN
}	//SeeSawN

#endif /* GEOMETRICERROR_H_ */
