/**
 * @file GeometricErrorConcept.ipp Implementation of GeometricErrorConceptC
 * @author Evren Imre
 * @date 3 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_ERROR_CONCEPT_IPP_9132152
#define GEOMETRIC_ERROR_CONCEPT_IPP_9132152

#include <boost/concept_check.hpp>
#include <vector>
#include <type_traits>
#include "GeometricError.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::CopyConstructible;
using boost::Assignable;
using std::vector;
using std::is_floating_point;

/**
 * @brief Concept checker for geometric error concept
 * @tparam TestT Class to be checked
 * @ingroup Concept
 */
template<class TestT>
class GeometricErrorConceptC
{
    ///@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<TestT, typename TestT::result_type, typename TestT::first_argument_type, typename TestT::second_argument_type> ));
    BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

    private:

        typename TestT::result_type ComputeFromProjected(SeeSawN::MetricsN::DetailN::Arity2Tag)
        {
            TestT tested;
            return tested.ComputeFromProjected(typename TestT::projected1_type(), typename TestT::second_argument_type());
        }

        typename TestT::result_type ComputeFromProjected(SeeSawN::MetricsN::DetailN::Arity4Tag)
        {
            TestT tested;
            return tested.ComputeFromProjected(typename TestT::first_argument_type(), typename TestT::projected1_type(), typename TestT::second_argument_type(), typename TestT::projected2_type());
        }

        typename TestT::first_argument_type* arg1;
        typename TestT::second_argument_type* arg2;

    public:

        BOOST_CONCEPT_USAGE(GeometricErrorConceptC)
        {
        	typedef typename TestT::projector_type ProjectorT;
            typedef typename TestT::projected1_type Projected1T;
            typedef typename TestT::projected2_type Projected2T;
            typedef typename TestT::arity_tag ArityTag;

            typedef typename TestT::result_type ResultT;
            static_assert(is_floating_point<ResultT>::value, "GeometricErrorConceptC : TestT::result_type must be a floating point type");

            TestT tested;
            vector<Projected1T> prj1=tested.Project(vector<typename TestT::first_argument_type>());
            vector<Projected2T> prj2=tested.InverseProject(vector<typename TestT::second_argument_type>());
            ResultT d=ComputeFromProjected(ArityTag()); (void)d;

            ProjectorT prj=tested.GetProjector();
            tested.SetProjector(prj);

            ResultT error=tested(prj, *arg1, *arg2); (void)error;

            optional<typename TestT::jacobian_type> vJ=tested.ComputeJacobiandP(*arg1,*arg2); (void)vJ;

            ResultT threshold=tested.ComputeOutlierThreshold(0.05,4); (void)threshold;

            float cost=tested.Cost(); (void)cost;
        }   //BOOST_CONCEPT_USAGE(GeometricErrorConceptC)
    ///@endcond
};  //class GeometricErrorConceptC

}   //MetricsN
}	//SeeSawN

#endif /* GEOMETRIC_ERROR_CONCEPT_IPP_9132152 */
