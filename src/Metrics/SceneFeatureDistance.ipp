/**
 * @file SceneFeatureDistance.ipp Implementation for scene feature distance metrics
 * @author Evren Imre
 * @date 28 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_FEATURE_DISTANCE_IPP_9653243
#define SCENE_FEATURE_DISTANCE_IPP_9653243

#include <boost/concept_check.hpp>
#include <climits>
#include "../Elements/Feature.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::DefaultConstructibleConcept;
using std::numeric_limits;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::SceneFeatureC;

/**
 * @brief Measures the distance between a scene and an image feature
 * @tparam DistanceT Distance functor for two image feature descriptors
 * @pre \c DistanceT is default constructible
 * @pre \c DistanceT is a model of adaptable binary function concept
 * @remarks Minimum distance between the image descriptor and the collection of image descriptor associated with the scene feature
 * @remarks An instance of adaptable binary function concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class DistanceT>
class SceneImageFeatureDistanceC
{
	private:

		///@cond CONCEPT_CHECK
		BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<DistanceT>));
		BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DistanceT, typename DistanceT::result_type, typename ImageFeatureC::descriptor_type, typename ImageFeatureC::descriptor_type>));
		///@endcond

		DistanceT distanceI;	///< Image feature distance metric

	public:

		/** @name Adaptable binary function concept interface */ ///@{
		typedef typename SceneFeatureC::descriptor_type first_argument_type;
		typedef typename ImageFeatureC::descriptor_type second_argument_type;
		typedef typename DistanceT::result_type result_type;
		result_type operator()(const first_argument_type& sceneDescriptor, const second_argument_type& imageDescriptor) const;	///< Returns the distance between a scene and an image descriptor
		///@}


};	//class SceneImageFeatureDistanceC

/**
 * @brief Measures the distance between two scene features
 * @tparam DistanceT Distance functor for two image feature descriptors
 * @pre \c DistanceT is default constructible
 * @pre \c DistanceT is a model of adaptable binary function concept
 * @remarks Minimum distance between the collections of the image descriptors associated with the scene features
 * @remarks An instance of adaptable binary function concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class DistanceT>
class SceneFeatureDistanceC
{
	private:

		///@cond CONCEPT_CHECK
		BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<DistanceT>));
		BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DistanceT, typename DistanceT::result_type, typename ImageFeatureC::descriptor_type, typename ImageFeatureC::descriptor_type>));
		///@endcond

		SceneImageFeatureDistanceC<DistanceT> distanceSI;	///< Scene-image feature distance metric

	public:

		/** @name Adaptable binary function concept interface */ ///@{
		typedef typename SceneFeatureC::descriptor_type first_argument_type;
		typedef typename SceneFeatureC::descriptor_type second_argument_type;
		typedef typename DistanceT::result_type result_type;
		result_type operator()(const first_argument_type& descriptor1, const second_argument_type& descriptor2) const;	///< Returns the distance between two scene descriptors
		///@}

};	//class SceneImageFeatureDistanceC

/********** IMPLEMENTATION STARTS HERE **********/

/********** SceneImageFeatureDistanceC **********/

/**
 * @brief Returns the distance between a scene and an image descriptor
 * @param[in] sceneDescriptor Scene descriptor
 * @param[in] imageDescriptor Image descriptor
 * @return Distance
 */
template<class DistanceT>
auto SceneImageFeatureDistanceC<DistanceT>::operator()(const first_argument_type& sceneDescriptor, const second_argument_type& imageDescriptor) const -> result_type
{
	result_type minDistance=numeric_limits<result_type>::infinity();
	for(const auto& current : sceneDescriptor)
	{
		result_type dist=distanceI( current.second.Descriptor(), imageDescriptor );
		if(dist<minDistance)
			minDistance=dist;
	}	//for(const auto& current : sceneDescriptor)

	return minDistance;
}	// result_type  operator()(const first_argument_type& sceneDescriptor, const second_argument_type& imageDescriptor) const

/********** SceneFeatureDistanceC **********/

/**
 * @brief Returns the distance between two scene descriptors
 * @param[in] descriptor1 First scene descriptor
 * @param[in] descriptor2 Second scene descriptor
 * @return Distance
 */
template<class DistanceT>
auto SceneFeatureDistanceC<DistanceT>::operator()(const first_argument_type& descriptor1, const second_argument_type& descriptor2) const -> result_type
{
	result_type minDistance=numeric_limits<result_type>::infinity();
	for(const auto& current : descriptor2)
	{
		result_type dist=distanceSI(descriptor1, current.second.Descriptor());
		if(dist<minDistance)
			minDistance=dist;
	}	//for(const auto& current : descriptor1)

	return minDistance;
}	//result_type operator()(const first_argument_type& descriptor1, const second_argument_type& descriptor2) const

}	//MetricsN
}	//SeeSawN

#endif /* SCENE_FEATURE_DISTANCE_IPP_9653243 */
