/**
 * @file Constraint.ipp Implementation of various constraints
 * @author Evren Imre
 * @date 21 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CONSTRAINT_IPP_4688778
#define CONSTRAINT_IPP_4688778

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <type_traits>
#include <cstddef>
#include <omp.h>
#include <iterator>
#include <tuple>
#include "Distance.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::LessThanComparableConcept;
using boost::RandomAccessRangeConcept;
using boost::ForwardRangeConcept;
using boost::SinglePassRangeConcept;
using boost::range_value;
using Eigen::Array;
using std::is_same;
using std::size_t;
using std::advance;
using std::tuple;
using std::make_tuple;

/**
 * @brief Checks whether the distance between two objects is below a threshold
 * @tparam DistanceT A distance type
 * @pre \c DistanceT is a model of AdaptableBinaryFunctionConcept
 * @remarks A model of adaptable binary predicate concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class DistanceT>
class DistanceConstraintC
{
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DistanceT, typename DistanceT::result_type, typename DistanceT::first_argument_type, typename DistanceT::second_argument_type>));
    BOOST_CONCEPT_ASSERT((LessThanComparableConcept<typename DistanceT::result_type>));
    //@endcond

    private:

        /** @name Configuration */ //@{
        DistanceT distanceMetric;   ///< Distance metric
        typename DistanceT::result_type threshold;    ///< Distance threshold
        unsigned int nThreads; ///< Number of threads
        bool flagValid; ///< \c true if the object is initialised
        //@}

        typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT; ///< A boolean array
        typedef Array<bool, Eigen::Dynamic, 1> BoolVectorT; ///< A boolean vector

        /** @name Implementation details */ //@{
        template<class InputRange1T> void ConstraintInnerLoop(BoolArrayT& output, const InputRange1T& range1, const typename DistanceT::first_argument_type& item2, size_t index2) const; ///< Inner loop for batch constraint evaluation, for single threaded operation
        template<class InputRange1T> void ConstraintInnerLoop(BoolVectorT& outputVector, const InputRange1T& range1, const typename DistanceT::first_argument_type& item2) const; ///< Inner loop for batch constraint evaluation, for multithreaded operation
        //@}

    public:

        /** @name Constructors */ //@{
		DistanceConstraintC();  ///< Default constructor
		DistanceConstraintC(const DistanceT& ddistanceMetric, typename DistanceT::result_type tthreshold, unsigned int nnThreads=1);   ///< Constructor
		//@}

        /**@name Adaptable binary predicate interface */ //@{
        typedef typename DistanceT::first_argument_type first_argument_type;
        typedef typename DistanceT::second_argument_type second_argument_type;
        typedef bool result_type;
        bool operator()(const first_argument_type& item1, const second_argument_type& item2) const;   ///< Checks whether the operands satisfy a distance constraint
        //@}

        /**@name BinaryConstraintConceptC interface */ //@{
        typedef typename DistanceT::result_type distance_type;	///< Type of the distance between two objects
        template<class InputRange1T, class InputRange2T> BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const;  ///< Evaluates the constraint on all pairs in the sets
        tuple<bool, distance_type> Enforce(const first_argument_type& item1, const second_argument_type& item2) const;	///< Verifies the constraint, and returns the distance
        //@}
};	//class DistanceConstraintC

/**
 * @brief Dummy constraint
 * @tparam Argument1T Type of the first argument
 * @tparam Argument2T Type of the second argument
 * @remarks Always successful
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class Argument1T, class Argument2T>
class DummyConstraintC
{
	private:
		typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT; ///< A boolean array

	public:

		/** @name Constructors */ //@{
		DummyConstraintC();  ///< Default constructor
		//@}

		/**@name Adaptable binary predicate interface */ //@{
		typedef Argument1T first_argument_type;
		typedef Argument2T second_argument_type;
		typedef bool result_type;
		bool operator()(const first_argument_type& item1, const second_argument_type& item2) const;   ///< Checks whether the operands satisfy a distance constraint
		//@}

		/**@name BinaryConstraintConceptC interface */ //@{
		typedef double distance_type;	///< Type of the distance between two objects
		template<class InputRange1T, class InputRange2T> BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const;  ///< Evaluates the constraint on all pairs in the sets
		tuple<bool, distance_type> Enforce(const first_argument_type& item1, const second_argument_type& item2) const;	///< Verifies the constraint, and returns the distance
		//@}
};	//class DummyConstraintC

/********** IMPLMENTATION STARTS HERE **********/

/********** DistanceConstraintC **********/
/**
 * @brief Inner loop for batch constraint evaluation, for single threaded operation
 * @tparam InputRange1T An input range
 * @param[in, out] output Output array
 * @param[in] range1 First input range
 * @param[in] item2 An element in the second range
 * @param[in] index2 Index of \c item2 in the second range
 * @pre \c InputRange1T is a forward range of first_argument_type
 */
template<class DistanceT>
template<class InputRange1T>
void DistanceConstraintC<DistanceT>::ConstraintInnerLoop(BoolArrayT& output, const InputRange1T& range1, const typename DistanceT::first_argument_type& item2, size_t index2) const
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
    static_assert(is_same< typename range_value<InputRange1T>::type, first_argument_type >::value, "DistanceConstraintC::ConstraintInnerLoop: InputRange1T must hold elements of type first_argument_type");

    size_t c1=0;
    for(const auto& item1 : range1)
    {
        output(c1, index2)=distanceMetric(item1, item2)<=threshold;
        ++c1;
    }   //for(const auto& item1 : range1)
}   //void ConstraintInnerLoopST(BoolArrayT& output, const InputRange1T& range1, const typename DistanceT::first_argument_type& item2, size_t index2)

/**
 * @brief Inner loop for batch constraint evaluation, for multithreaded operation
 * @tparam InputRange1T An input range
 * @param[out] outputVector Preallocated output buffer
 * @param[in] range1 First input range
 * @param[in] item2 An element in the second range
 * @pre \c InputRange1T is a forward range of first_argument_type
 */
template<class DistanceT>
template<class InputRange1T>
void DistanceConstraintC<DistanceT>::ConstraintInnerLoop(BoolVectorT& outputVector, const InputRange1T& range1, const typename DistanceT::first_argument_type& item2) const
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
    static_assert(is_same< typename range_value<InputRange1T>::type, first_argument_type >::value, "DistanceConstraintC::ConstraintInnerLoop: InputRange1T must hold elements of type first_argument_type");

    size_t c1=0;
    for(const auto& item1 : range1)
    {
        outputVector(c1)=distanceMetric(item1, item2)<=threshold;
        ++c1;
    }   //for(const auto& item1 : range1)
}   //void ConstraintInnerLoopST(BoolVetorT& outputVector, const InputRange1T& range1, const typename DistanceT::first_argument_type& item2)


/**
 * @brief Default constructor
 * @post An invalid object is created
 */
template<class DistanceT>
DistanceConstraintC<DistanceT>::DistanceConstraintC() : nThreads(1), flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] ddistanceMetric Distance metric
 * @param[in] tthreshold Distance threshold
 * @param[in] nnThreads Number of threads for multithreaded operation
 * @post A valid object is created
 */
template<class DistanceT>
DistanceConstraintC<DistanceT>::DistanceConstraintC(const DistanceT& ddistanceMetric, typename DistanceT::result_type tthreshold, unsigned int nnThreads) : distanceMetric(ddistanceMetric), threshold(tthreshold), nThreads(nnThreads), flagValid(true)
{}

/**
 * @brief Checks whether the operands satisfy a distance constraint
 * @param[in] item1 First item
 * @param[in] item2 Second item
 * @return \c true if the constraint is satisfied
 * @pre \c flagValid=true
 */
template<class DistanceT>
bool DistanceConstraintC<DistanceT>::operator()(const first_argument_type& item1, const second_argument_type& item2) const
{
    //Preconditions
    assert(flagValid);
    return distanceMetric(item1, item2)<=threshold;
}   //bool operator()(const typename DistanceT::first_argument_type& item1, const typename DistanceT::second_argument_type& item2)

/**
 * @brief Evaluates the constraint on all pairs in the sets
 * @tparam InputRange1T Range holding the items in the first set
 * @tparam InputRange2T Range holding the items in the second set
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @return A boolean array indicating the constraint conformance of all pairs in the sets
 * @pre \c InputRange1T is a forward range holding elements of type first_argument_type
 * @pre \c InputRange2T is a random access range holding elements of type second_argument_type
 * @pre \c flagValid=true
 */
template<class DistanceT>
template<class InputRange1T, class InputRange2T>
auto DistanceConstraintC<DistanceT>::BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const -> BoolArrayT
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<InputRange2T>));
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
    static_assert(is_same< typename range_value<InputRange1T>::type, first_argument_type >::value, "DistanceConstraintC::BatchConstraintEvaluation: InputRange1T must hold elements of type first_argument_type");
    static_assert(is_same< typename range_value<InputRange2T>::type, second_argument_type >::value, "DistanceConstraintC::BatchConstraintEvaluation: InputRange2T must hold elements of type second_argument_type");
    assert(flagValid);

    size_t sSet1=boost::distance(set1);
    size_t sSet2=boost::distance(set2);

    BoolArrayT output(sSet1, sSet2);

    size_t c2=0;
    BoolVectorT buffer(sSet1);
#pragma omp parallel for schedule(static) if(nThreads>1) private(c2) firstprivate(buffer) num_threads(nThreads)
    for(c2=0; c2<sSet2; ++c2)
    {
        if(nThreads==1)
            ConstraintInnerLoop(output, set1, set2[c2], c2);
        else
        {
            ConstraintInnerLoop(buffer, set1, set2[c2]);
    	#pragma omp critical(DC_BCE)
            output.col(c2)=buffer;

        }   // if(!flagMultithreaded)
    }   //for(c2=0; c2<sSet2; ++c2)
    return output;
}   //auto BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) -> BoolArrayT

/**
 * @brief Verifies the constraint, and returns the distance
 * @param[in] item1 First item
 * @param[in] item2 Second item
 * @return A tuple: [Constraint test; distance]
 */
template<class DistanceT>
auto DistanceConstraintC<DistanceT>::Enforce(const first_argument_type& item1, const second_argument_type& item2) const -> tuple<bool, distance_type>
{
	//Preconditions
	assert(flagValid);
	distance_type value=distanceMetric(item1, item2);

	return make_tuple(value<threshold, value);
}	//tuple<bool, distance_type> Enforce(const first_argument_type& item1, const second_argument_type& item2) const

/********** DummyConstraintC **********/

template<class Argument1T, class Argument2T>
DummyConstraintC<Argument1T, Argument2T>::DummyConstraintC()
{}

/**
 * @brief Checks whether the operands satisfy a distance constraint
 * @param[in] item1 First item
 * @param[in] item2 Second item
 * @return Always true
 */
template<class Argument1T, class Argument2T>
bool DummyConstraintC<Argument1T, Argument2T>::operator()(const first_argument_type& item1, const second_argument_type& item2) const
{
	return true;
}	//bool operator()(const first_argument_type& item1, const second_argument_type& item2) const

/**
 * @brief Evaluates the constraint on all pairs in the sets
 * @tparam InputRange1T A range
 * @tparam InputRange2T A range
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @return A \c true array
 * @pre \c InputRange1T is a single pass range
 * @pre \c InputRange2T is a single pass range
 */
template<class Argument1T, class Argument2T>
template<class InputRange1T, class InputRange2T>
auto DummyConstraintC<Argument1T, Argument2T>::BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const -> BoolArrayT
{
    size_t sSet1=boost::distance(set1);
    size_t sSet2=boost::distance(set2);
    return BoolArrayT(sSet1, sSet2).setConstant(true);
}	//BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const

/**
 * @brief Verifies the constraint, and returns the distance
 * @param[in] item1 First item
 * @param[in] item2 Second item
 * @return A tuple: [true,0]
 */
template<class Argument1T, class Argument2T>
auto DummyConstraintC<Argument1T, Argument2T>::Enforce(const first_argument_type& item1, const second_argument_type& item2) const -> tuple<bool, distance_type>
{
	return make_tuple(true, 0);
}	//tuple<bool, distance_type> Enforce(const first_argument_type& item1, const second_argument_type& item2) const

}   //MetricsN
}	//SeeSawN


#endif /* CONSTRAINT_IPP_4688778 */
