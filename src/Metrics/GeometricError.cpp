/**
 * @file GeometricError.cpp Implementation and instantiations of various geometric errors
 * @author Evren Imre
 * @date 31 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "GeometricError.h"
namespace SeeSawN
{
namespace MetricsN
{

/**
 * @brief Default constructor
 * @post An invalid object
 */
EpipolarSampsonErrorC::EpipolarSampsonErrorC() : flagValid(false)
{
	bool flagDouble=is_same<RealT, double>::value;
	cost= 2*ComplexityEstimatorC::MatrixVectorMultiplication(3, 3, flagDouble) + ComplexityEstimatorC::InnerProduct(3, flagDouble) + ComplexityEstimatorC::L2Norm(4, flagDouble);
}	//EpipolarSampsonErrorC()

/**
 * @brief Constructor
 * @param[in] mmF A fundamental matrix
 * @post A valid object
 */
EpipolarSampsonErrorC::EpipolarSampsonErrorC(const EpipolarMatrixT& mmF) : EpipolarSampsonErrorC()
{
    SetProjector(mmF);
}   //EpipolarSampsonErrorC(const MatrixT& mmF)

/**
 * @brief Computes the distance
 * @param[in] x1 A point in the reference image
 * @param[in] x2 A point in the target image
 * @return Sampson distance
 * @pre \c flagValid=true
 */
auto EpipolarSampsonErrorC::operator()(const Coordinate2DT& x1, const Coordinate2DT& x2) const -> RealT
{
    //Preconditions
    assert(flagValid);
    HCoordinate2DT l1=mF*x1.homogeneous();
    HCoordinate2DT l2=mFt*x2.homogeneous();
    return ComputeFromProjected(x1, l1, x2, l2);
}   //RealT operator()(const Coordinate2DT& x1, const Coordinate2DT& x2) const

/**
 * @brief Computes the distance when the epipolar lines are available
 * @param[in] x1 A point in the reference image
 * @param[in] Fx1 Epipolar line in the target image corresponding to \c x1
 * @param[in] x2 A point in the target image
 * @param[in] Ftx2 Epipolar line in the referance image corresponding to \c x2
 * @return Sampson distance
 * @pre Neither \c x1 nor \c x2 is an epipole
 */
auto EpipolarSampsonErrorC::ComputeFromProjected(const Coordinate2DT& x1, const HCoordinate2DT& Fx1, const Coordinate2DT& x2, const HCoordinate2DT& Ftx2) -> RealT
{
    //Preconditions
    assert( !(Fx1.isZero() || Ftx2.isZero()) );
    return fabs(Ftx2.dot(x1.homogeneous())) /sqrt(Fx1.head(2).squaredNorm()+Ftx2.head(2).squaredNorm());
}   //RealT ComputeFromProjected(const Coordinate2DT& x1, const HCoordinate2DT& Fx1, const Coordinate2DT& x2, const HCoordinate2DT& Ftx2) const

/**
 * @brief Computes the distance
 * @param[in] projector Essential or fundamental matrix
 * @param[in] x1 A point in the reference image
 * @param[in] x2 A point in the target image
 * @return Sampson distance
 * @remarks Not efficient for evaluating multiple pairs
 */
auto EpipolarSampsonErrorC::operator()(const projector_type& projector, const Coordinate2DT& x1, const Coordinate2DT& x2) const -> RealT
{
	HCoordinate2DT l1=projector*x1.homogeneous();
	HCoordinate2DT l2=projector.transpose()*x2.homogeneous();
    return ComputeFromProjected(x1, l1, x2, l2);
}   //RealT operator()(const Coordinate2DT& x1, const Coordinate2DT& x2) const

/**
 * @brief Returns a constant reference to mF,matrix()
 * @return A reference to mF.matrix()
 * @pre \c flagValid=true
 */
auto EpipolarSampsonErrorC::GetProjector() const -> const EpipolarMatrixT&
{
    //Preconditions
    assert(flagValid);
    return mF.matrix();
}   //const MatrixT& GetProjector() const

/**
 * @brief Sets mF
 * @param[in] mmF New fundamental matrix
 * @post \c mF and \c mFt are modified
 */
void EpipolarSampsonErrorC::SetProjector(const EpipolarMatrixT& mmF)
{
    flagValid=true;
    mF.matrix()=mmF;
    mFt.matrix()=mmF.transpose();
}   //void SetProjector(const MatrixT& mmF)

/**
 * @brief Computes the Jacobian of the error wrt/projector
 * @param[in] x1 First coordinate
 * @param[in] x2 Second coordinate
 * @return Jacobian matrix. Invalid if the Jacobian computation fails
 */
auto EpipolarSampsonErrorC::ComputeJacobiandP(const Coordinate2DT& x1, const Coordinate2DT& x2) const -> optional<RowVectorXT>
{
	HCoordinate2DT l1=mF*x1.homogeneous();
	HCoordinate2DT l2=mFt*x2.homogeneous();
	RealT yFx=l2.dot(x1.homogeneous());

	RealT C1=pow<2>(yFx);
	RealT C2=pow<2>(l1[0]) + pow<2>(l1[1]) + pow<2>(l2[0]) + pow<2>(l2[1]);

	//Numerator

	Matrix<result_type, 1, 9> dC1dF;
	dC1dF.segment(0,3)=x2[0]*x1.homogeneous().transpose();
	dC1dF.segment(3,3)=x2[1]*x1.homogeneous().transpose();
	dC1dF.segment(6,3)=x1.homogeneous().transpose();

	//Special case: 0/0 resolved via L'Hospital
	if(C1==0)
		return optional<RowVectorXT>(2*sqrt(C2)*dC1dF);

	//Sampson error not defined at the epipoles
	if(C2==0)
		return optional<RowVectorXT>();

	dC1dF*=yFx;

	//Denominator

	Matrix<result_type, 1, 9> dq1dF; dq1dF.setZero();
	dq1dF.segment(0,3)=l1[0]*x1.homogeneous().transpose();
	dq1dF.segment(3,3)=l1[1]*x1.homogeneous().transpose();

	Matrix<result_type, 1, 9> dq2dF; dq2dF.setZero();
	dq2dF.segment(0,2)=x2[0]*l2.transpose();
	dq2dF.segment(3,2)=x2[1]*l2.transpose();
	dq2dF.segment(6,2)=l2.transpose();

	Matrix<result_type, 1, 9> dC2dF = dq1dF + dq2dF;

	return optional<RowVectorXT>(sqrt(1.0/(C1*C2)) * (dC1dF - (C1/C2)*dC2dF));
}	//ComputeJacobiandP(const CoordinateST& x1, const CoordinateDT& x2) -> optional<RowVectorXT>

/**
 * @brief Computes the Jacobian of the error wrt/Coordinates
 * @param[in] x1 First coordinate
 * @param[in] x2 Second coordinate
 * @return Jacobian matrix. Invalid if the Jacobian computation fails
 */
auto EpipolarSampsonErrorC::ComputeJacobiandx1x2(const Coordinate2DT& x1, const Coordinate2DT& x2) const -> optional<RowVectorXT>
{
	HCoordinate2DT l1=mF*x1.homogeneous();
	HCoordinate2DT l2=mFt*x2.homogeneous();
	RealT yFx=l2.dot(x1.homogeneous());

	RealT C1=pow<2>(yFx);
	RealT C2=pow<2>(l1[0]) + pow<2>(l1[1]) + pow<2>(l2[0]) + pow<2>(l2[1]);

	//Sampson error not defined at the epipoles
	if(C2==0)
		return optional<RowVectorXT>();

	RealT error=C1/C2;
	Matrix<result_type, 1, 4> dedxy;
	dedxy.segment(0,2)=(yFx*l2.segment(0,2) - error*mF.matrix().block(0,0,2,2).transpose()*l1.segment(0,2)).transpose();
	dedxy.segment(2,2)=(yFx*l1.segment(0,2) - error*mF.matrix().block(0,0,2,2)*l2.segment(0,2)).transpose();

	return optional<RowVectorXT>(sqrt(1.0/(C1*C2))*dedxy);
}	//optional<RowVectorXT> ComputeJacobiandx1x2(const Coordinate2DT& x1, const Coordinate2DT& x2) const

/**
 * @brief Computes the outlier rejection threshold for a specified inlier rejection probability and noise variance
 * @param[in] pReject Probability of rejection of an inlier
 * @param[in] noiseVariance Variance of the noise on the individual coordinates
 * @return Outlier threshold, in units of the destination domain (i.e. not its square!)
 */
auto EpipolarSampsonErrorC::ComputeOutlierThreshold(RealT pReject, RealT noiseVariance) -> RealT
{
	return sqrt(quantile(complement(chi_squared_distribution<RealT>(nDOF),pReject))*noiseVariance);
}	//auto ComputeThreshold(RealT pReject, RealT noiseVariance) -> RealT

/**
 * @brief Computational cost of the operation
 * @return \c cost
 * @remarks No unit tests, as the costs are volatile
 */
double EpipolarSampsonErrorC::Cost() const
{
	return cost;
}	//float Cost()

/********** EXPLICIT INSTANTIATIONS **********/

template class TransferErrorC<CameraMatrixT>;
template vector<typename TransferErrorH32DT::projected1_type> TransferErrorH32DT::Project(const vector<typename TransferErrorH32DT::first_argument_type>&) const;

template class TransferErrorC< Homography2DT >;
template vector<typename TransferErrorH2DT::projected1_type> TransferErrorH2DT::Project(const vector<typename TransferErrorH2DT::first_argument_type>&) const;

template class SymmetricTransferErrorC<Homography2DT>;
template vector<typename SymmetricTransferErrorH2DT::projected1_type> SymmetricTransferErrorH2DT::Project(const vector<typename SymmetricTransferErrorH2DT::first_argument_type>&) const;
template vector<typename SymmetricTransferErrorH2DT::projected2_type> SymmetricTransferErrorH2DT::InverseProject(const vector<typename SymmetricTransferErrorH2DT::second_argument_type>&) const;

template class SymmetricTransferErrorC<Homography3DT>;
template vector<typename SymmetricTransferErrorH3DT::projected1_type> SymmetricTransferErrorH3DT::Project(const vector<typename SymmetricTransferErrorH3DT::first_argument_type>&) const;
template vector<typename SymmetricTransferErrorH3DT::projected2_type> SymmetricTransferErrorH3DT::InverseProject(const vector<typename SymmetricTransferErrorH3DT::second_argument_type>&) const;

template vector<typename EpipolarSampsonErrorC::projected1_type> EpipolarSampsonErrorC::Project(const vector<typename EpipolarSampsonErrorC::first_argument_type>&) const;
template vector<typename EpipolarSampsonErrorC::projected2_type> EpipolarSampsonErrorC::InverseProject(const vector<typename EpipolarSampsonErrorC::second_argument_type>&) const;

}   //MetricsN
}	//SeeSawN


