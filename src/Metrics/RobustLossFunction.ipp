/**
 * @file RobustLossFunction.ipp Implementation of robust loss functions
 * @author Evren Imre
 * @date 20 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROBUST_LOSS_FUNCTION_IPP_9102312
#define ROBUST_LOSS_FUNCTION_IPP_9102312

#include <boost/math/special_functions/pow.hpp>
#include <type_traits>
#include <cmath>
#include <climits>

namespace SeeSawN
{
namespace MetricsN
{

using boost::math::pow;
using std::is_floating_point;
using std::sqrt;
using std::exp;
using std::log;
using std::min;
using std::max;
using std::numeric_limits;

/**
 * @brief Pseudo-Huber loss function
 * @tparam ValueT Floating point type
 * @pre \c ValueT is a floating point type
 * @remarks Multiple View Geometry, R. Hartley, A. Zissermann, 2nd Ed, 2003, A6.8, pp.619
 * @remarks Loss: \f$ C(\delta) = 2b^2 (\sqrt{1+ (\frac{\delta}{b})^2}-1) \f$, clipped at \c cutoff
 * @remarks Fitness: \f$ F(\delta) = \exp({-C(\delta)/\sigma^2}) \f$, clipped at the cutoff value of the loss
 * @warning The returned quantity is of the square-error type, as the crossover value has the unit of error^2
 * @ingroup Metrics
 * @nosubgrouping
 */
template<typename ValueT>
class PseudoHuberLossC
{
	static_assert(is_floating_point<ValueT>::value, "PseudoHuberLossC: ValueT must be a floating point type");

	private:

		/** @name Configuration */ //@{
		ValueT crossover;	///< Inlier-outlier crossover point
		ValueT cutoff;	///< Any error beyond this value incurs a constant loss

		ValueT crossover2;	///< Square of \c crossover
		ValueT sigma2;	///< Scale factor for the fitness function
		ValueT maxLoss;	///< Loss value beyond \c cutoff
		ValueT minFitness;	///< Fitness value beyond cutoff
		//@}

	public:

		/**@name Constructors */ //@{
		PseudoHuberLossC(ValueT ccrossover=1, ValueT ccutoff=1, ValueT scale=0.5);	///< Constructor
		//@}

		/**@name Adaptable unary function interface */ //@{
		typedef ValueT argument_type;	///< Argument type
		typedef ValueT result_type;	///< Result type
		result_type operator()(argument_type error) const;	///< Computes the loss corresponding to the error
		//@}

		void Configure(ValueT ccrossover=1, ValueT ccutoff=1, ValueT scale=0.5);	///< Sets the crossover and the scale parameters for the function

		result_type ConvertToFitness(argument_type loss) const;	///< Converts a loss value to a fitness value
		result_type ComputeFitness(argument_type error) const;	///< Computes the fitness score corresponding to an error value
};	//class PseudoHuberC

/********** IMPLEMENTATION STARTS HERE **********/

/********** PseudoHuberC **********/

/**
 * @brief Constructor
 * @param[in] ccrossover Crossover point from the inlier to the outlier regime
 * @param[in] ccutoff Any error beyond this value incurs a constant loss
 * @param[in] scale Fitness value for the loss corresponding to an error of \c crossover/2
 * @pre \c 0<scale<1
 */
template<class ValueT>
PseudoHuberLossC<ValueT>::PseudoHuberLossC(ValueT ccrossover, ValueT ccutoff, ValueT scale)
{
	Configure(ccrossover, ccutoff, scale);
}	//PseudoHuberLossC(ValueT ccrossover, ValueT sscale)

/**
 * @brief Sets the crossover and the scale parameters for the function
 * @param[in] ccrossover Crossover point from the inlier to the outlier regime
 * @param[in] ccutoff Any error beyond this value incurs a constant loss
 * @param[in] scale Fitness value for the loss corresponding to an error of \c crossover/2
 * @pre \c 0<scale<1
 */
template<class ValueT>
void PseudoHuberLossC<ValueT>::Configure(ValueT ccrossover, ValueT ccutoff, ValueT scale)
{
	//Preconditions
	assert(scale>0 && scale<1);

	cutoff=ccutoff;
	crossover=ccrossover;
	crossover2=2*pow<2>(crossover);
	sigma2=-crossover2*(sqrt(1.25)-1)/log(scale);	//ComputeFitness(crossover/2)=scale

	maxLoss=numeric_limits<ValueT>::infinity();
	maxLoss=this->operator()(cutoff);

	minFitness=-numeric_limits<ValueT>::infinity();
	minFitness=ConvertToFitness(maxLoss);
}	//void Configure(ValueT ccrossover, ValueT sscale)

/**
 * @brief Computes the loss corresponding to the error
 * @param[in] error Error
 * @return Loss
 */
template<class ValueT>
auto PseudoHuberLossC<ValueT>::operator()(argument_type error) const -> result_type
{
	return min(maxLoss, crossover2*(sqrt(1+ pow<2>(error/crossover))-1));
}	//result_type operator()(argument_type error)

/**
 * @brief Converts a loss value to a fitness value
 * @param[in] loss Loss value
 * @return Fitness value
 * @warning \c loss is not the error, but the corresponding pseudo-Huber loss
 */
template<class ValueT>
auto PseudoHuberLossC<ValueT>::ConvertToFitness(argument_type loss) const -> result_type
{
	return max(minFitness, exp(-loss/sigma2));
}	//result_type ComputeFitness(argument_type error) const

/**
 * @brief Computes the fitness score corresponding to an error value
 * @param[in] error Error
 * @return Fitness value
 */
template<class ValueT>
auto PseudoHuberLossC<ValueT>::ComputeFitness(argument_type error) const -> result_type
{
	return ConvertToFitness(this->operator()(error));
}	//result_type ComputeFitness(argument_type error) const


}	//MetricsN
}	//SeeSawN

#endif /* ROBUST_LOSS_FUNCTION_IPP_9102312 */
