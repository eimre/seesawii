/**
 * @file GeometricConstraint.cpp Instantiations of GeometricConstraintC
 * @author Evren Imre
 * @date 31 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "GeometricConstraint.h"
namespace SeeSawN
{
namespace MetricsN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class GeometricConstraintC<TransferErrorH32DT>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<TransferErrorH32DT>::BatchConstraintEvaluation(const vector<typename TransferErrorH32DT::first_argument_type>&, const vector<typename TransferErrorH32DT::second_argument_type>&) const;

template class GeometricConstraintC<SymmetricTransferErrorH2DT>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<SymmetricTransferErrorH2DT>::BatchConstraintEvaluation(const vector<typename SymmetricTransferErrorH2DT::first_argument_type>&, const vector<typename SymmetricTransferErrorH2DT::second_argument_type>&) const;

template class GeometricConstraintC<SymmetricTransferErrorH3DT>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<SymmetricTransferErrorH3DT>::BatchConstraintEvaluation(const vector<typename SymmetricTransferErrorH3DT::first_argument_type>&, const vector<typename SymmetricTransferErrorH3DT::second_argument_type>&) const;

template class GeometricConstraintC<EpipolarSampsonErrorC>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  GeometricConstraintC<EpipolarSampsonErrorC>::BatchConstraintEvaluation(const vector<typename EpipolarSampsonErrorC::first_argument_type>&, const vector<typename EpipolarSampsonErrorC::second_argument_type>&) const;

}   //MetricsN
}	//SeeSawN


