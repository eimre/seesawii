/**
 * @file RobustLossFunctionConcept.h Public interface for RobustLossFunctionConceptC
 * @author Evren Imre
 * @date 20 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROBUST_LOSS_FUNCTION_CONCEPT_H_6902137
#define ROBUST_LOSS_FUNCTION_CONCEPT_H_6902137

#include "RobustLossFunctionConcept.ipp"
namespace SeeSawN
{
namespace MetricsN
{

template<class TestT> class RobustLossFunctionConceptC;	///< Concept checker for the robust loss function concept
}	//MetricsN
}	//SeeSawN

#endif /* ROBUST_LOSS_FUNCTION_CONCEPT_H_6902137 */
