/**
 * @file MatchAmbiguityMetric.h Public interface for various match ambiguity metrics
 * @author Evren Imre
 * @date 20 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MATCH_AMBIGUITY_METRIC_H_0909212
#define MATCH_AMBIGUITY_METRIC_H_0909212

#include "MatchAmbiguityMetric.ipp"
#include "Similarity.h"
namespace SeeSawN
{
namespace MetricsN
{

template<class SimilarityMetricT> class RayleighMetaRecognitionMetricC;	///< Computes the Rayleigh meta-recognition metric for a set of correspondences

/********** EXTERN TEMPLATES **********/
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
extern template class RayleighMetaRecognitionMetricC<InverseEuclideanIDConverterT>;

}	//MetricsN
}	//SeeSawN

#endif /* MATCH_AMBIGUITY_METRIC_H_0909212 */
