/**
 * @file CheiralityConstraint.ipp Implementation details for the cheirality constraint
 * @author Evren Imre
 * @date 6 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CHEIRALITY_CONSTRAINT_IPP_7012902
#define CHEIRALITY_CONSTRAINT_IPP_7012902

#include <boost/math/special_functions/sign.hpp>
#include <stdexcept>
#include <limits>
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenExtensions.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::math::sign;
using std::invalid_argument;
using std::numeric_limits;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::HCoordinate3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Cheirality test
 * @remarks A point is visible if it is in front of the principal plane (which is parallel to the image plane, and contains the camera centre)
 * @warning The cheirality test is only applicable to cameras with an invertible 3x3 first submatrix. Matrices produced by \c CameraFromFundamental do not qualify
 * @ingroup Metrics
 * @nosubgrouping
 */
class CheiralityConstraintC
{
	private:

		bool flagValid;	///< \c true if the object is initialised
		int signDetM;	///< Sign of the determinant of the first 3x3 submatrix of the camera matrix
		HCoordinate3DT vP3;	///< Scaled 3rd row of the camera matrix

	public:

		/** @name Adaptable unary predicate interface */ //@{
		typedef Coordinate3DT argument_type;
		typedef bool result_type;

		bool operator()(const Coordinate3DT& point) const;	///< Verifies the cheirality constraint
		//@}

		CheiralityConstraintC();	///< Default constructor
		CheiralityConstraintC(const CameraMatrixT& mmP);	///< Constructor
		double Distance(const Coordinate3DT& point) const;	///< Signed distance from the image plane
		double AbsoluteDistance(const Coordinate3DT& point) const;	///< Absolute distance from the image plane
};	//class CheiralityConstraintC

}	//MetricsN
}	//SeeSawN

#endif /* CHEIRALITYCONSTRAINT_IPP_ */
