/**
 * @file Distance.h Public interface for the distance metrics
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DISTANCE_H_5976132
#define DISTANCE_H_5976132

#include "Distance.ipp"
#include "../Elements/Coordinate.h"
#include "../Elements/Descriptor.h"

namespace SeeSawN
{
namespace MetricsN
{

template<class VectorT> class EuclideanDistanceC;    ///< Euclidean distance between two vectors
template<class VectorT> class StableChiSquaredDistanceC;  ///< Chi-squared distance between two histograms. Stabilised against division-by-0
template<class BitsetT> class HammingDistanceC;	///< Hamming distance between two bitsets

/********** EXTERN TEMPLATES **********/
using SeeSawN::ElementsN::Coordinate2DT;
typedef EuclideanDistanceC<Coordinate2DT> EuclideanDistance2DT;   ///< Euclidean distance for 2D coordinates
extern template class EuclideanDistanceC<Coordinate2DT>;

using SeeSawN::ElementsN::Coordinate3DT;
typedef EuclideanDistanceC<Coordinate3DT> EuclideanDistance3DT;   ///< Euclidean distance for 3D coordinates
extern template class EuclideanDistanceC<Coordinate3DT>;

using SeeSawN::ElementsN::ImageDescriptorT;
typedef EuclideanDistanceC<ImageDescriptorT> EuclideanDistanceIDT;   ///< Euclidean distance for image descriptors
extern template class EuclideanDistanceC<ImageDescriptorT>;

typedef StableChiSquaredDistanceC<ImageDescriptorT> ChiSquaredDistanceIDT;   ///< Stable Chi-squared distance for image desciptors
extern template class StableChiSquaredDistanceC<ImageDescriptorT>;

using SeeSawN::ElementsN::BinaryImageDescriptorT;
typedef HammingDistanceC<BinaryImageDescriptorT> HammingDistanceBIDT;	///< Hamming distance for binary image descriptors
extern template class HammingDistanceC<BinaryImageDescriptorT>;

}   //MetricsN
}	//SeeSawN

#endif /* DISTANCE_H_5976132 */
