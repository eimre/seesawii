/**
 * @file Polygon2DConstraint.ipp Implementation of \c Polygon2DConstraintC
 * @author Evren Imre
 * @date 18 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef POLYGON2D_CONSTRAINT_IPP_1809201
#define POLYGON2D_CONSTRAINT_IPP_1809201

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/concept_check.hpp>
#include <type_traits>
#include "../Elements/Coordinate.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::geometry::model::d2::point_xy;
using boost::geometry::model::polygon;
using boost::geometry::covered_by;
using boost::geometry::append;
using boost::ForwardRangeConcept;
using boost::range_value;
using std::is_same;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Checks whether a point is within a 2D polygon
 * @warning A line segment does not cover the points on it! Could be an implementation issue with \c covered_by
 * @nosubgrouping
 * @ingroup Metrics
 */
class Polygon2DConstraintC
{
	private:

		typedef ValueTypeM<Coordinate2DT>::type RealT;	///< Floating point type

		typedef point_xy<RealT> PointT;	///< Geometry point type
		typedef polygon<PointT> PolygonT;	///< Polygon type

		/** @name Configuration */ //@{
		bool flagValid;	///< \c true if the object is valid
		PolygonT region;	///< Polygon defining the admissible region
		//@}

	public:

		/** @name Constructors */ //@{
		Polygon2DConstraintC();	///< Default constructor
		template<class CoordinateRangeT> Polygon2DConstraintC(const CoordinateRangeT& vertexList);	///< Constructor
		//@}

		/** @name Adaptable unary predicate interface */ //@{
		typedef Coordinate2DT argument_type;	///< Type of the argument
		typedef bool result_type;	///< Result type

		bool operator()(const Coordinate2DT& item) const;	///< Checks whether the operand is within the polygon
		//@}

		bool IsValid();	///< Returns \c flagValid

};	//class Polygon2DConstraintC

/**
 * @brief Constructor
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] vertexList Vertices defining the polygon
 * @pre \c CoordinateRangeT is a forward range of elements of type \c Coordinate2DT
 * @post A valid object
 */
template<class CoordinateRangeT>
Polygon2DConstraintC::Polygon2DConstraintC(const CoordinateRangeT& vertexList) : flagValid(true)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
	static_assert(is_same<typename range_value<CoordinateRangeT>::type, Coordinate2DT>::value, "Polygon2DConstraintC::Polygon2DConstraintC : CoordinateRangeT must hold elements of type Coordinate2DT");

	Coordinate2DT closingPoint;	//The first and the last vertex of a polygon
	if(boost::distance(vertexList)!=0)
		closingPoint=*boost::begin(vertexList);
	else
		return;

	for(const auto& current : vertexList)
		append(region, PointT(current[0], current[1]));

	append(region, PointT(closingPoint[0], closingPoint[1]));
}	//Polygon2DConstraintC(const CoordinateRangeT& vertexList) : flagValid(true)

}	//MetricN
}	//SeeSawN

#endif /* POLYGON2D_CONSTRAINT_IPP_1809201 */
