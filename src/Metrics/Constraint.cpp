/**
 * @file Constraint.cpp Instantiations of various constraints
 * @author Evren Imre
 * @date 21 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Constraint.h"

namespace SeeSawN
{
namespace MetricsN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class DistanceConstraintC<EuclideanDistance2DT>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  DistanceConstraintC<EuclideanDistance2DT>::BatchConstraintEvaluation(const vector<typename EuclideanDistance2DT::first_argument_type>&, const vector<typename EuclideanDistance2DT::second_argument_type>&) const;

template class DistanceConstraintC<EuclideanDistance3DT>;
template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  DistanceConstraintC<EuclideanDistance3DT>::BatchConstraintEvaluation(const vector<typename EuclideanDistance3DT::first_argument_type>&, const vector<typename EuclideanDistance3DT::second_argument_type>&) const;
}   //MetricsN
}	//SeeSawN


