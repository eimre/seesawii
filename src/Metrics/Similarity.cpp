/**
 * @file Similarity.cpp Instantiation of the similarity metrics
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Similarity.h"
namespace SeeSawN
{
namespace MetricsN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class DistanceToSimilarityConverterC<EuclideanDistanceIDT, ReciprocalC<typename EuclideanDistanceIDT::result_type> >;
template class DistanceToSimilarityConverterC<ChiSquaredDistanceIDT, ReciprocalC<typename ChiSquaredDistanceIDT::result_type> >;
template class DistanceToSimilarityConverterC<HammingDistanceBIDT, ReciprocalC<float> >;
template class DistanceToSimilarityConverterC<EuclideanSceneFeatureDistanceT, ReciprocalC<EuclideanSceneFeatureDistanceT::result_type> >;
template class DistanceToSimilarityConverterC<EuclideanSceneImageFeatureDistanceT, ReciprocalC<EuclideanSceneImageFeatureDistanceT::result_type> >;
template class DistanceToSimilarityConverterC<HammingBinarySceneImageFeatureDistanceT, ReciprocalC<HammingBinarySceneImageFeatureDistanceT::result_type> >;
template class DistanceToSimilarityConverterC<HammingBinarySceneFeatureDistanceT, ReciprocalC<HammingBinarySceneFeatureDistanceT::result_type> >;

}   //MetricsN
}	//SeeSawN


