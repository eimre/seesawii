/**
 * @file BinarySceneFeatureDistance.ipp Implementation of the distance metrics for the binary scene features
 * @author Evren Imre
 * @date 4 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BINARY_SCENE_FEATURE_DISTANCE_IPP_7712980
#define BINARY_SCENE_FEATURE_DISTANCE_IPP_7712980

#include <boost/concept_check.hpp>
#include <boost/range/algorithm.hpp>
#include <climits>
#include <cmath>
#include "../Elements/Feature.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::DefaultConstructibleConcept;
using boost::for_each;
using std::numeric_limits;
using std::min;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;

/**
 * @brief Measures the distance between a scene and an image feature
 * @tparam DistanceT Distance functor for two image feature descriptors
 * @pre \c DistanceT is default constructible
 * @pre \c DistanceT is a model of adaptable binary function concept
 * @remarks Minimum distance between the image descriptor and the collection of image descriptor associated with the scene feature
 * @remarks An instance of adaptable binary function concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class DistanceT>
class BinarySceneImageFeatureDistanceC
{
	private:

		///@cond CONCEPT_CHECK
		BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<DistanceT>));
		BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DistanceT, typename DistanceT::result_type, typename BinaryImageFeatureC::descriptor_type, typename BinaryImageFeatureC::descriptor_type>));
		///@endcond

		DistanceT distanceI;	///< Image feature distance metric

	public:

		/** @name Adaptable binary function concept interface */ ///@{
		typedef typename OrientedBinarySceneFeatureC::descriptor_type first_argument_type;
		typedef typename BinaryImageFeatureC::descriptor_type second_argument_type;
		typedef typename DistanceT::result_type result_type;
		result_type operator()(const first_argument_type& sceneDescriptor, const second_argument_type& imageDescriptor) const;	///< Returns the distance between a scene and an image descriptor
		///@}


};	//class BinarySceneImageFeatureDistanceC

/**
 * @brief Measures the distance between two scene features
 * @tparam DistanceT Distance functor for two image feature descriptors
 * @pre \c DistanceT is default constructible
 * @pre \c DistanceT is a model of adaptable binary function concept
 * @remarks Minimum distance between the collections of the image descriptors associated with the scene features
 * @remarks An instance of adaptable binary function concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class DistanceT>
class BinarySceneFeatureDistanceC
{
	private:

		///@cond CONCEPT_CHECK
		BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<DistanceT>));
		BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DistanceT, typename DistanceT::result_type, typename BinaryImageFeatureC::descriptor_type, typename BinaryImageFeatureC::descriptor_type>));
		///@endcond

		BinarySceneImageFeatureDistanceC<DistanceT> distanceSI;	///< Scene-image feature distance metric

	public:

		/** @name Adaptable binary function concept interface */ ///@{
		typedef typename OrientedBinarySceneFeatureC::descriptor_type first_argument_type;
		typedef typename OrientedBinarySceneFeatureC::descriptor_type second_argument_type;
		typedef typename DistanceT::result_type result_type;
		result_type operator()(const first_argument_type& descriptor1, const second_argument_type& descriptor2) const;	///< Returns the distance between two scene descriptors
		///@}
};	//class BinarySceneFeatureDistanceC

/********** IMPLEMENTATION STARTS HERE **********/

/********** BinarySceneImageFeatureDistanceC **********/
/**
 * @brief Returns the distance between a scene and an image descriptor
 * @param[in] sceneDescriptor Scene descriptor
 * @param[in] imageDescriptor Image descriptor
 * @return Distance
 */
template<class DistanceT>
auto BinarySceneImageFeatureDistanceC<DistanceT>::operator()(const first_argument_type& sceneDescriptor, const second_argument_type& imageDescriptor) const -> result_type
{
	result_type minDistance=numeric_limits<result_type>::max();
	for_each(sceneDescriptor, [&](const typename first_argument_type::value_type& current){minDistance=min(minDistance,distanceI( current.second.Descriptor(), imageDescriptor));});
	return minDistance;
}	// result_type  operator()(const first_argument_type& sceneDescriptor, const second_argument_type& imageDescriptor) const

/********** BinarySceneFeatureDistanceC **********/
/**
 * @brief Returns the distance between two scene descriptors
 * @param[in] descriptor1 First scene descriptor
 * @param[in] descriptor2 Second scene descriptor
 * @return Distance
 */
template<class DistanceT>
auto BinarySceneFeatureDistanceC<DistanceT>::operator()(const first_argument_type& descriptor1, const second_argument_type& descriptor2) const -> result_type
{
	result_type minDistance=numeric_limits<result_type>::max();
	for_each(descriptor2, [&](const typename second_argument_type::value_type& current){minDistance=min(minDistance,distanceSI( descriptor1, current.second.Descriptor()));});
	return minDistance;
}	//result_type operator()(const first_argument_type& descriptor1, const second_argument_type& descriptor2) const

}	//MetricsN
}	//SeeSawN



#endif /* BINARY_SCENE_FEATURE_DISTANCE_IPP_7712980 */
