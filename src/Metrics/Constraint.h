/**
 * @file Constraint.h Public interface for various constraints
 * @author Evren Imre
 * @date 21 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CONSTRAINT_H_8081232
#define CONSTRAINT_H_8081232

#include "Constraint.ipp"
#include <vector>

namespace SeeSawN
{
namespace MetricsN
{

template<class Argument1T, class Argument2T> class DummyConstraintC;	///< Dummy constraint
template<class DistanceT> class DistanceConstraintC;    ///< Checks whether the distance between two objects is below a threshold

/********** EXTERN TEMPLATES **********/
using SeeSawN::MetricsN::EuclideanDistance2DT;
using std::vector;
typedef DistanceConstraintC<EuclideanDistance2DT> EuclideanDistanceConstraint2DT;   ///< 2D Euclidean distance constraint
extern template class DistanceConstraintC<EuclideanDistance2DT>;
extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  DistanceConstraintC<EuclideanDistance2DT>::BatchConstraintEvaluation(const vector<typename EuclideanDistance2DT::first_argument_type>&, const vector<typename EuclideanDistance2DT::second_argument_type>&) const;

using SeeSawN::MetricsN::EuclideanDistance3DT;
typedef DistanceConstraintC<EuclideanDistance3DT> EuclideanDistanceConstraint3DT;   ///< 3D Euclidean distance constraint
extern template class DistanceConstraintC<EuclideanDistance3DT>;
extern template Array<bool, Eigen::Dynamic, Eigen::Dynamic>  DistanceConstraintC<EuclideanDistance3DT>::BatchConstraintEvaluation(const vector<typename EuclideanDistance3DT::first_argument_type>&, const vector<typename EuclideanDistance3DT::second_argument_type>&) const;


}   //MetricsN
}	//SeeSawN

#endif /* CONSTRAINT_H_8081232 */
