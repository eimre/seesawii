/**
 * @file InvertibleBinarySimilarityConcept.ipp Implementation of InvertibleBinarySimilarityConceptC
 * @author Evren Imre
 * @date 18 Sep 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INVERTIBLE_BINARY_SIMILARITY_CONCEPT_IPP_7909213
#define INVERTIBLE_BINARY_SIMILARITY_CONCEPT_IPP_7909213

#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryFunctionConcept;

/**
 * @brief Invertible binary similarity concept checker
 * @tparam TestT Class to be tested
 * @tparam ResultT Result type
 * @tparam Operand1T First operand
 * @tparam Operand2T Second operand
 * @remarks A similarity metric between two operands. Can invert a similarity score to get a distance.
 * @ingroup Concept
 */
template<class TestT, typename ResultT, class Operand1T, class Operand2T>
class InvertibleBinarySimilarityConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<TestT, ResultT, Operand1T, Operand2T>));

	public:
		BOOST_CONCEPT_USAGE(InvertibleBinarySimilarityConceptC)
		{
			TestT tested;
			ResultT distance=tested.Invert(ResultT()); (void)distance;

			typename TestT::map_type inverter; (void)inverter;
			typename TestT::distance_type dist; (void)dist;
		}	//BOOST_CONCEPT_USAGE(InvertibleBinarySimilarityConceptC)
	///@endcond

};	//class InvertibleBinarySimilarityConceptC
}	//MetricsN
}	//SeeSawN

#endif /* INVERTIBLE_BINARY_SIMILARITY_CONCEPT_IPP_7909213 */
