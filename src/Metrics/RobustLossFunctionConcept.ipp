/**
 * @file RobustLossFunctionConcept.ipp
 * @author Evren Imre
 * @date 20 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROBUST_LOSS_FUNCTION_CONCEPT_IPP_7012316
#define ROBUST_LOSS_FUNCTION_CONCEPT_IPP_7012316

#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace MetricsN
{

using boost::CopyConstructible;
using boost::Assignable;
using boost::UnaryFunctionConcept;

/**
 * @brief Concept checker for the robust loss function concept
 * @tparam TestT Concept to be tested
 * @ingroup Concept
 */
template<class TestT>
class RobustLossFunctionConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));
	BOOST_CONCEPT_ASSERT((UnaryFunctionConcept<TestT, typename TestT::argument_type, typename TestT::result_type>));

	public:

		BOOST_CONCEPT_USAGE(RobustLossFunctionConceptC)
		{
			TestT tested0;

			TestT tested(1.0, 1.0, 0.5);
			tested.Configure(2.0, 3.5, 0.25);

			typename TestT::result_type t1=tested.ComputeFitness(1.0); (void)t1;
			typename TestT::result_type t2=tested(1.0);
			typename TestT::result_type t3=tested.ConvertToFitness(t2); (void)t3;
		}	//BOOST_CONCEPT_USAGE(RobustLossFunctionConceptC)

	///@endcond
};	//class RobustLossFunctionConceptC

}	//MetricsN
}	//SeeSawN

#endif /* ROBUST_LOSS_FUNCTION_CONCEPT_IPP_7012316 */
