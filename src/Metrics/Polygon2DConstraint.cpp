/**
 * @file Polygon2DConstraint.cpp Implementation of \c Polygon2DConstraintC
 * @author Evren Imre
 * @date 18 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Polygon2DConstraint.h"
namespace SeeSawN
{
namespace MetricsN
{

/**
 * @brief Default constructor
 * @post An invalid object
 */
Polygon2DConstraintC::Polygon2DConstraintC() : flagValid(false)
{}

/**
 * @brief Checks whether the operand is within the polygon
 * @param[in] item Point to be checked
 * @return \c false if \c item is not within the polygon
 */
bool Polygon2DConstraintC::operator()(const Coordinate2DT& item) const
{
	return covered_by(PointT(item[0], item[1]), region);
}	//bool operator()(const Coordinate2DT& item) const

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool Polygon2DConstraintC::IsValid()
{
	return flagValid;
}	//bool IsValid()

/********** EXPLICIT INSTANTIATIONS **********/
template Polygon2DConstraintC::Polygon2DConstraintC(const vector<Coordinate2DT>&);
}	//MetricN
}	//SeeSawN

