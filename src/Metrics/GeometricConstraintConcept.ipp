/**
 * @file GeometricConstraintConcept.ipp Implementation of GeometricConstraintConceptC
 * @author Evren Imre
 * @date 18 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_CONSTRAINT_CONCEPT_IPP_1239093
#define GEOMETRIC_CONSTRAINT_CONCEPT_IPP_1239093

#include <boost/concept_check.hpp>
#include <tuple>
#include <type_traits>
#include "BinaryConstraintConcept.ipp"

namespace SeeSawN
{
namespace MetricsN
{

using boost::CopyConstructible;
using boost::Assignable;
using std::tie;
using std::is_floating_point;
using SeeSawN::MetricsN::BinaryConstraintConceptC;

/**
 * @brief Geometric constraint concept checker
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class GeometricConstraintConceptC
{
	//@cond CONCEPT_CHECK

	BOOST_CONCEPT_ASSERT((BinaryConstraintConceptC<TestT, typename TestT::first_argument_type, typename TestT::second_argument_type>));
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

	private:

		typename TestT::projector_type *pProjector;
		typename TestT::first_argument_type *arg1;
		typename TestT::second_argument_type *arg2;

	public:

		BOOST_CONCEPT_USAGE(GeometricConstraintConceptC)
		{
			static_assert(is_floating_point<typename TestT::distance_type>::value, "GeometricConstraintConceptC : TestT::distance_type must be a floating point type");

			TestT tested;

			bool flagConstraint;
			typename TestT::distance_type dist;
			tie(flagConstraint, dist)=tested.Enforce(*pProjector, *arg1, *arg2);

			tested.SetProjector(*pProjector);

            typedef typename TestT::error_type ErrorT;
            ErrorT error=tested.GetErrorMetric(); (void)error;
		}	//BOOST_CONCEPT_USAGE(GeometricConstraintConceptC)
	//@endcond
};

}	//MetricsN
}	//SeeSawN

#endif /* GEOMETRIC_CONSTRAINT_CONCEPT_IPP_1239093 */
