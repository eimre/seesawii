/**
 * @file Distance.ipp Implementation of the distance metrics
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DISTANCE_IPP_8135621
#define DISTANCE_IPP_8135621

#include <cassert>
#include <limits>
#include <cmath>
#include <cstddef>
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace MetricsN
{

using std::numeric_limits;
using std::sqrt;
using std::size_t;
using SeeSawN::WrappersN::ValueTypeM;

/**
 * @brief Euclidean distance between two vectors
 * @tparam VectorT A vector
 * @pre \c VectorT is an Eigen dense matrix (unenforced)
 * @remarks A model of adaptable binary function concept
 * @remarks The maximum distance for normalised vectors is sqrt(2). Numerical simulations show that the mean distance between random normalised vectors is sqrt(2)/2, regardless of the size.
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class VectorT>
class EuclideanDistanceC
{
    public:

        /**@name Adaptable binary function interface */ //@{
        typedef VectorT first_argument_type;
        typedef VectorT second_argument_type;
        typedef typename ValueTypeM<VectorT>::type result_type;
        result_type operator()(const VectorT& v1, const VectorT& v2) const;    ///< Computes the Euclidean distance between two vectors
        //@}
};	//class EuclideanDistanceC

/**
 * @brief Chi-squared distance between two histograms. Stabilised against division-by-0
 * @tparam VectorT A vector
 * @pre \c VectorT is an Eigen dense matrix(unenforced)
 * @remarks The intended use is for normalised histograms, where \c stabTerm will be negligible. When \c stabTerm is comparable to the norm of the histogram (e.g. integer bins), the result might be inaccurate
 * @remarks The operator returns the square-root of the distance, for consistency with the other norm-type distances.
 * @remarks The maximum distance for normalised vectors is ~(size/2)^0.25 (two histograms, [1 0 1 0...]/ and [0 1 0 1...]. Normalise and compute the distance). Numerical simulations show that the mean distance between random normalised vectors is 0.47*maxDistance
 * @remarks A model of adaptable binary function concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class VectorT>
class StableChiSquaredDistanceC
{
    public:
        /**@name Adaptable binary function interface */ //@{
        typedef VectorT first_argument_type;
        typedef VectorT second_argument_type;
        typedef typename ValueTypeM<VectorT>::type result_type;
        result_type operator()(const VectorT& v1, const VectorT& v2) const;    ///< Computes the Euclidean distance between two vectors
        //@}

    private:
        static constexpr result_type stabTerm=numeric_limits<result_type>::epsilon(); ///< Stabilisation term to prevent a division by 0
};  //StableChiSquaredDistanceC

/**
 * @brief Hamming distance between two bitsets
 * @tparam BitsetT A bitset
 * @pre \c BitsetT is a boost bitset (unenforced)
 * @remarks A model of adaptable binary function concept
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class BitsetT>
class HammingDistanceC
{
    public:

        /**@name Adaptable binary function interface */ //@{
        typedef BitsetT first_argument_type;
        typedef BitsetT second_argument_type;
        typedef double result_type;	//For the inverse distance mapper
        result_type operator()(const BitsetT& op1, const BitsetT& op2) const;    ///< Computes the Hamming distance between two bitsets
        //@}
};	//class EuclideanDistanceC

/********** IMPLEMENTATION STARTS HERE **********/

/********** EuclideanDistanceC **********/

/**
 * @brief Computes the Euclidean distance between two vectors
 * @param[in] v1 First vector
 * @param[in] v2 Second vector
 * @return Euclidean distance
 * @pre \c v1 and \c v2 have the same size
 */
template<class VectorT>
auto EuclideanDistanceC<VectorT>::operator()(const VectorT& v1, const VectorT& v2) const-> result_type
{
    //Preconditions
    assert(v1.size()==v2.size());

    return (v1-v2).norm();
}   //auto EuclideanDistanceC<VectorT>::operator()(const VectorT& v1, const VectorT& v2)->return_type

/********** StableChiSquaredDistanceC **********/

/** @brief Computes the Euclidean distance between two vectors
* @param[in] v1 First vector
* @param[in] v2 Second vector
* @return Euclidean distance
* @pre \c v1 and \c v2 have the same size
* @pre \c v1 and \c v2 have no negative entries (unenforced)
* @remarks Reference: "The Quadratic-Chi Histogram Distance Family", O. Pele, M. Werman
*/
template<class VectorT>
auto StableChiSquaredDistanceC<VectorT>::operator()(const VectorT& v1, const VectorT& v2) const-> result_type
{
    //Preconditions
    assert(v1.size()==v2.size());

#ifdef NDEBUG
    // diff' * (diff/(sum+stabTerm))
    return sqrt( 0.5 * ( (v1-v2).dot( ( (v1-v2).array() / ( (v1+v2).array() + stabTerm) ).matrix() ) ) );  //Eq 4. Modifications: square-root and stabilisation term
#else //For some reason, debug mode does not like stabTerm
    return sqrt( 0.5 * ( (v1-v2).dot( ( (v1-v2).array() / ( (v1+v2).array() + numeric_limits<result_type>::epsilon()) ).matrix() ) ) );  //Eq 4. Modifications: square-root and stabilisation term
#endif
}   //auto StableChiSquaredDistanceC<VectorT>::operator()(const VectorT& v1, const VectorT& v2)->return_type

/********** HammingDistanceC **********/

/**
 * @brief Computes the Hamming distance between two bitsets
 * @param[in] op1 First bitset
 * @param[in] op2 Second bitset
 * @return Hamming distance
 * @pre \c op1 and \c op2 have the same size
 */
template<class BitsetT>
auto HammingDistanceC<BitsetT>::operator()(const BitsetT& op1, const BitsetT& op2) const-> result_type
{
    //Preconditions
    assert(op1.size()==op2.size());

//    op1^=op2;
//    return op1.count();
    return (double)BitsetT::HammingDistance(op1, op2);
}   //auto EuclideanDistanceC<VectorT>::operator()(const VectorT& v1, const VectorT& v2)->return_type


}   //MetricsN
}	//SeeSawN



#endif /* DISTANCE_IPP_8135621 */
