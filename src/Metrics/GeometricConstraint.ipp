/**
 * @file GeometricConstraint.ipp Implementation of GeometricConstraintC
 * @author Evren Imre
 * @date 31 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_CONSTRAINT_IPP_9123545
#define GEOMETRIC_CONSTRAINT_IPP_9123545

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/geometry/geometry.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <cstddef>
#include <type_traits>
#include <vector>
#include <tuple>
#include <stdexcept>
#include <map>
#include <cmath>
#include "GeometricError.h"
#include "GeometricErrorConcept.h"
#include "../Elements/FeatureUtility.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::RandomAccessRangeConcept;
using boost::ForwardRangeConcept;
using boost::geometry::model::d2::point_xy;
using boost::geometry::index::rtree;
using boost::geometry::index::dynamic_rstar;
using boost::geometry::index::nearest;
using boost::range::for_each;
using boost::range_value;
using Eigen::Array;
using std::size_t;
using std::is_same;
using std::vector;
using std::tuple;
using std::make_tuple;
using std::max;
using std::logic_error;
using std::enable_if;
using std::map;
using std::integral_constant;
using SeeSawN::MetricsN::DetailN::Arity2Tag;
using SeeSawN::MetricsN::DetailN::Arity4Tag;
using SeeSawN::MetricsN::GeometricErrorConceptC;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::MetricsN::TransferErrorH2DT;
using SeeSawN::ElementsN::DetailN::LexicalCompare2DC;
using SeeSawN::WrappersN::ValueTypeM;


/**
 * @brief Verifies geometric error constraints
 * @tparam ErrorT A geometric error
 * @pre \c ErrorT is a model of GeometricErrorConceptC
 * @remarks Approximate neighbourhood implementation
 * 	- Can be activated only if the constraint involves the evaluation of the transfer error in a 2D space (e.g. \c TransferErrorH32DT and TransferErrorH2DT)
 * 	- R-tree nearest neighbour association on both sets
 * 	- Exact if the max-distance neighbourhoods are disjoint
 * 	- Approximate if an item can be associated with multiple items in the other set
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class ErrorT>
class GeometricConstraintC
{
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((GeometricErrorConceptC<ErrorT>));
    //@endcond

    private:

        typedef typename ErrorT::result_type RealT; ///< Floating point type
        typedef typename ErrorT::first_argument_type Coordinate1T;  ///< Coordinate type for the first set
        typedef typename ErrorT::second_argument_type Coordinate2T; ///< Coordinate type for the second set

        /** @name Configuration */ //@{
        ErrorT errorMetric; ///< Error metric
        unsigned int nThreads; ///< Number of threads, for multithreaded operation
        RealT threshold;    ///< Error threshold

        static constexpr bool flagEuclidean2D = is_same<ErrorT, TransferErrorH32DT>::value || is_same<ErrorT, TransferErrorH2DT>::value;	///< Indicates whether the problem involves a constraint on the 2D Euclidean distance
        bool flagApproximateNeighbourhoods;	///< \c true if fast approximate constraint evaluation via R-trees is active

        bool flagValid; ///< \c true if the object is initialised
        //@}

        typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT; ///< A boolean array
        typedef Array<bool, Eigen::Dynamic, 1> BoolVectorT; ///< A boolean vector


        typedef typename ErrorT::projected1_type Projected1T;    ///< A projection of first_argument_type in the domain of the second set
        typedef typename ErrorT::projected2_type Projected2T;    ///< A projection of second_argument_type in the domain of the first set

       /** @name Implementation details */ //@{
        template<class InputRange1T, class InputRange2T> BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2, Arity2Tag* dummy) const;  ///< Evaluates the constraint on all pairs in the set
        template<class InputRange1T, class InputRange2T> BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2, Arity4Tag* dummy) const;  ///< Evaluates the constraint on all pairs in the set

        template<class DummyT> void ConstraintInnerLoop(BoolArrayT& output, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2, size_t index2) const; ///< Inner loop for batch constraint evaluation, for single threaded operation
        template<class DummyT> void ConstraintInnerLoop(BoolVectorT& outputVector, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2) const; ///< Inner loop for batch constraint evaluation, for multithreaded operation
        template<class InputRange1T> void ConstraintInnerLoop(BoolArrayT& output, const InputRange1T& range1, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2, const Projected2T& projected2, size_t index2) const; ///< Inner loop for batch constraint evaluation, for single threaded operation
        template<class InputRange1T> void ConstraintInnerLoop(BoolVectorT& outputVector, const InputRange1T& range1, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2, const Projected2T& projected2) const; ///< Inner loop for batch constraint evaluation, for multithreaded operation

        template<class TargetRangeT, class SourceRangeT> void PerformNNAssociation(BoolArrayT& output, const TargetRangeT& target, const SourceRangeT& source, bool flagTranspose, integral_constant<bool,true> dummy) const;	///< Associates the elements of \c source with their nearest neighbours in \c target, and evaluates the constraint
        template<class TargetRangeT, class SourceRangeT> void PerformNNAssociation(BoolArrayT& output,const TargetRangeT& target, const SourceRangeT& source, bool flagTranspose, integral_constant<bool,false> dummy) const;	///< Base version of \c PerformNNAssociation , does nothing
        template<class InputRange1T, class InputRange2T> BoolArrayT ApproximateBatchConstraintEvaluationA2(const InputRange1T& set1, const InputRange2T& set2) const; ///< Approximate batch constraint evaluation. Skips some pairs
        template<class InputRange1T, class InputRange2T> BoolArrayT ExactBatchConstraintEvaluationA2(const InputRange1T& set1, const InputRange2T& set2) const;  ///< Evaluates the constraint on all pairs in the set

       //@}

    public:

        /** @name Constructors */ //@{
        GeometricConstraintC(); ///< Default constructor
        GeometricConstraintC(const ErrorT& eerrorMetric, typename ErrorT::result_type tthreshold, unsigned int nnThreads=1, bool fflagApproximateNN=false);    ///< Constructor
        //@}

        /** @name Adaptable binary predicate interface */ //@{
        typedef typename ErrorT::first_argument_type first_argument_type;
        typedef typename ErrorT::second_argument_type second_argument_type;
        typedef bool result_type;
        bool operator()(const first_argument_type& operand1, const second_argument_type& operand2) const;
        //@}

        /**@name BinaryConstraintConceptC interface */ //@{
        typedef RealT distance_type;	///< Type of the distance between two objects
        template<class InputRange1T, class InputRange2T> BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const;  ///< Evaluates the constraint on all pairs in the set
        tuple<bool, RealT> Enforce(const first_argument_type& item1, const second_argument_type& item2) const;	///< Verifies the constraint, and returns the distance
        //@}

        /**@name GeometricConstraintConceptC interface */ //@{

        typedef ErrorT error_type;	///< Geometric error underlying the constraint
        const ErrorT& GetErrorMetric() const;	///< Returns a constant reference to \c errorMetric

        typedef typename ErrorT::projector_type projector_type;	///< Geometric entity defining the constraint
        bool operator()(const projector_type& projector, const first_argument_type& operand1, const second_argument_type& operand2) const;
        tuple<bool, RealT> Enforce(const projector_type& projector, const first_argument_type& item1, const second_argument_type& item2) const;	///< Verifies the constraint, and returns the distance

        void SetProjector(const projector_type& projector);	///< Sets the projector for the error metric
        //@}
};  //class GeometricConstraintC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Inner loop for batch constraint evaluation, for single threaded operation
 * @tparam DummyT Dummy template parameter to prevent unwanted instantiations of ConstraintInnerLoop
 * @param[out] output Output array
 * @param[in] projectedRange1 Set1 projected to the domain of the second set
 * @param[in] item2 An item in the second set
 * @param[in] index2 Index of \c item2
 */
template<class ErrorT>
template<class DummyT>
void GeometricConstraintC<ErrorT>::ConstraintInnerLoop(BoolArrayT& output, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2, size_t index2) const
{
    size_t c1=0;
    for(const auto& item1 : projectedRange1)
    {
        output(c1, index2)=errorMetric.ComputeFromProjected(item1, item2)<=threshold;
        ++c1;
    }   //for(const auto& item1 : range1)
}   //void ConstraintInnerLoop(BoolArrayT& output, const ProjectedRange1T& range1, const Coordinate2T& item2, size_t index2)

/**
 * @brief Inner loop for batch constraint evaluation, for multithreaded operation
 * @tparam DummyT Dummy template parameter to prevent unwanted instantiations of ConstraintInnerLoop
 * @param[out] outputVector Output
 * @param[in] projectedRange1 Set1 projected to the domain of the second set
 * @param[in] item2 An item in the second set
 */
template<class ErrorT>
template<class DummyT>
void GeometricConstraintC<ErrorT>::ConstraintInnerLoop(BoolVectorT& outputVector, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2) const
{
    size_t c1=0;
    for(const auto& item1 : projectedRange1)
    {
        outputVector(c1)=errorMetric.ComputeFromProjected(item1, item2)<=threshold;
        ++c1;
    }   //for(const auto& item1 : range1)
}   //void ConstraintInnerLoop(BoolVectorT& outputVector, const ProjectedRange1T& range1, const Coordinate2T& item2)

/**
 * @brief Inner loop for batch constraint evaluation, for single threaded operation
 * @param[out] output Output
 * @param[in] range1 First set
 * @param[in] projectedRange1 Projection of the first set into the domain of the second set
 * @param[in] item2 An item in the second set
 * @param[in] projected2 Projection of \c item2 into the domain of the first set
 * @param[in] index2 Index of \c item2
 */
template<class ErrorT>
template<class InputRange1T>
void GeometricConstraintC<ErrorT>::ConstraintInnerLoop(BoolArrayT& output, const InputRange1T& range1, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2, const Projected2T& projected2, size_t index2) const
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
    static_assert(is_same< typename range_value<InputRange1T>::type, Coordinate1T >::value, "GeometricConstraintC::ConstraintInnerLoop: InputRange1T must hold elements of type Coordinate1T");

    size_t c1=0;
    for(const auto& item1 : range1)
    {
        output(c1, index2)=errorMetric.ComputeFromProjected(item1, projectedRange1[c1], item2, projected2)<=threshold;
        ++c1;
    }   //for(const auto& item1 : range1)

}   //void ConstraintInnerLoop(BoolArrayT& output, const InputRange1T range1, const ProjectedRange1T& projectedRange1, const Coordinate2T& item2, const Projected2T& projected2, size_t index2)

/**
 * @brief Inner loop for batch constraint evaluation, for multithreaded operation
 * @param[out] outputVector Output
 * @param[in] range1 First set
 * @param[in] projectedRange1 Projection of the first set into the domain of the second set
 * @param[in] item2 An item in the second set
 * @param[in] projected2 Projection of \c item2 into the domain of the first set
 */
template<class ErrorT>
template<class InputRange1T>
void GeometricConstraintC<ErrorT>::ConstraintInnerLoop(BoolVectorT& outputVector, const InputRange1T& range1, const vector<Projected1T>& projectedRange1, const Coordinate2T& item2, const Projected2T& projected2) const
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
    static_assert(is_same< typename range_value<InputRange1T>::type, Coordinate1T >::value, "GeometricConstraintC::ConstraintInnerLoop: InputRange1T must hold elements of type Coordinate1T");

    size_t c1=0;
    for(const auto& item1 : range1)
    {
        outputVector(c1)=errorMetric.ComputeFromProjected(item1, projectedRange1[c1], item2, projected2)<=threshold;
        ++c1;
    }   //for(const auto& item1 : range1)
}   //void ConstraintInnerLoop(BoolVectorT& outputVector, const InputRange1T range1, const ProjectedRange1T& projectedRange1, const Coordinate2T& item2, const Projected2T& projected2)

/**
 * @brief Associates the elements of \c source with their nearest neighbours in \c target, and evaluates the constraint
 * @tparam TargetRangeT Target range
 * @tparam SourceRangeT Source range
 * @param[in,out] output Output array
 * @param[in] target Target set
 * @param[in] source Source set
 * @param[in] flagTranspose If \c true , \c target corresponds to the columns of the output array
 * @param[in] dummy Dummy parameter for overload resolution
 * @return A boolean array indicating the pairs satisfying the constraint
 * @pre \c TargetRangeT is a forward range holding elements of type \c Projected1T
 * @pre \c SourceRangeT is a random access range holding elements of type \c Projected1T
 */
template<class ErrorT>
template<class TargetRangeT, class SourceRangeT>
void GeometricConstraintC<ErrorT>::PerformNNAssociation(BoolArrayT& output, const TargetRangeT& target, const SourceRangeT& source, bool flagTranspose, integral_constant<bool,true> dummy) const
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<TargetRangeT>));
	static_assert(is_same< typename range_value<TargetRangeT>::type, Projected1T >::value, "GeometricConstraintC::PerformNNAssociation: TargetRangeT must hold elements of type Projected1T");

	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<SourceRangeT>));
	static_assert(is_same< typename range_value<SourceRangeT>::type, Projected1T >::value, "GeometricConstraintC::PerformNNAssociation: SourceRangeT must hold elements of type Projected1T");

	size_t sSetT=boost::distance(target);
	size_t sSetS=boost::distance(source);

	//Build an r-tree
	typedef point_xy<typename ValueTypeM<Projected1T>::type> PointT;    //A 2D point
	vector<PointT> pointSetT; pointSetT.reserve(sSetT);
	for_each(target, [&](const Projected1T current){pointSetT.emplace_back(current[0], current[1]);});
	rtree<PointT, dynamic_rstar> tree(pointSetT, dynamic_rstar( max((size_t)4,pointSetT.size()/32)));

	//Index map
	map<PointT, size_t, LexicalCompare2DC<PointT> > indexMap;
	for(size_t c=0; c<sSetT; ++c)
		indexMap.emplace(pointSetT[c],c);

	double threshold2=pow<2>(threshold);

	//Get the nearest target point for each source point
	//TODO Explicitly assumes that the matrix is column-major. Should be able to deal with row-major matrices equally efficiently
	size_t c2=0;
#pragma omp parallel for if(nThreads>1) schedule(static) private(c2) num_threads(nThreads)
	for(c2=0; c2<sSetS; ++c2)
	{
		PointT nearestPoint;
		tree.query(nearest(PointT(source[c2][0], source[c2][1]),1), &nearestPoint ); //Query with composite predicate (i.e. chaining the max distance constraint) is a lot slower

		//If it satisfies the error constraint, fetch the index of the target point
		if( pow<2>(source[c2][0]-nearestPoint.x()) + pow<2>(source[c2][1]-nearestPoint.y()) < threshold2 )
		{
			size_t i=indexMap[nearestPoint];
			bool flagUpdate = ! (flagTranspose ? output(c2,i) : output(i,c2));

			if(flagUpdate)
			{
			#pragma omp critical(GC_PNNA1)
				if(flagTranspose)
					output(c2,i)=true;
				else
					output(i,c2)=true;
			}	//if(flagUpdate)
		}	//if( (source[c2]-sourcePoint).squaredNorm()<threshold2 )
	}	//for(c2=0; c2<sSetS; ++c2)
}	//BoolArrayT PerformNNAssociation(const TargetRangeT& target, const SourceRangeT& source)

/**
 * @brief Base version of \c PerformNNAssociation , does nothing
 * @tparam TargetRangeT Target range
 * @tparam SourceRangeT Source range
 * @param[in,out] output Output array
 * @param[in] target Target set
 * @param[in] source Source set
 * @param[in] flagTranspose If \c true , \c target corresponds to the columns of the output array
 * @param[in] dummy Dummy parameter for overload resolution
 * @return A boolean array indicating the pairs satisfying the constraint
 * @throws logic_error Always
 */
template<class ErrorT>
template<class TargetRangeT, class SourceRangeT>
void GeometricConstraintC<ErrorT>::PerformNNAssociation(BoolArrayT& output, const TargetRangeT& target, const SourceRangeT& source, bool flagTranspose, integral_constant<bool,false> dummy) const
{
	throw(logic_error("GeometricConstraintC<ErrorT>::PerformNNAssociation: This function should not be called in normal operation"));

}	//BoolArrayT PerformNNAssociation(const TargetRangeT& target, const SourceRangeT& source)

/**
 * @brief Approximate batch constraint evaluation. Skips some pairs
 * @tparam InputRange1T Range holding the items in the first set
 * @tparam InputRange2T Range holding the items in the second set
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @return A boolean array indicating the constraint conformance of all pairs in the sets
 * @pre \c InputRange1T is a forward range holding elements of type first_argument_type
 * @pre \c InputRange2T is a random access range holding elements of type second_argument_type
 */
template<class ErrorT>
template<class InputRange1T, class InputRange2T>
auto GeometricConstraintC<ErrorT>::ApproximateBatchConstraintEvaluationA2(const InputRange1T& set1, const InputRange2T& set2) const -> BoolArrayT
{
	//Project
	vector<Projected1T> projectedSet1=errorMetric.Project(set1);

	size_t sSet1=boost::distance(set1);
	size_t sSet2=boost::distance(set2);
	BoolArrayT output(sSet1, sSet2); output.setConstant(false);

	PerformNNAssociation(output, projectedSet1, set2, false, integral_constant<bool,flagEuclidean2D>());	//Set 1 as target
	PerformNNAssociation(output, set2, projectedSet1, true, integral_constant<bool,flagEuclidean2D>());	//Set 2 as target

	return output;
}	//BoolArrayT ApproximateBatchConstraintEvaluationA2(const InputRange1T& set1, const InputRange2T& set2, Arity2Tag* dummy) const

/**
 * @brief Evaluates the constraint on all pairs in the sets
 * @tparam InputRange1T Range holding the items in the first set
 * @tparam InputRange2T Range holding the items in the second set
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @return A boolean array indicating the constraint conformance of all pairs in the sets
 * @pre \c InputRange1T is a forward range holding elements of type first_argument_type
 * @pre \c InputRange2T is a random access range holding elements of type second_argument_type
 */
template<class ErrorT>
template<class InputRange1T, class InputRange2T>
auto GeometricConstraintC<ErrorT>::ExactBatchConstraintEvaluationA2(const InputRange1T& set1, const InputRange2T& set2) const -> BoolArrayT
{
	 //Preconditions
	BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<InputRange2T>));
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
	static_assert(is_same< typename range_value<InputRange1T>::type, first_argument_type >::value, "GeometricConstraintC::ExactBatchConstraintEvaluationA2: InputRange1T must hold elements of type first_argument_type");
	static_assert(is_same< typename range_value<InputRange2T>::type, second_argument_type >::value, "GeometricConstraintC::ExactBatchConstraintEvaluationA2: InputRange2T must hold elements of type second_argument_type");

	//Projection cache
	vector<Projected1T> projectedSet1=errorMetric.Project(set1);

	size_t sSet1=boost::distance(set1);
	size_t sSet2=boost::distance(set2);

	BoolArrayT output(sSet1, sSet2);

	size_t c2=0;
	BoolVectorT buffer(sSet1);

	//TODO Explicitly assumes that the matrix is column-major. Should be able to deal with row-major matrices equally efficiently
#pragma omp parallel for if(nThreads>1) schedule(static) private(c2) firstprivate(buffer) num_threads(nThreads)
	for(c2=0; c2<sSet2; ++c2)
	{
		//Compute the error by using the cached projections
		if(nThreads==1)
			ConstraintInnerLoop<Arity2Tag>(output, projectedSet1, set2[c2], c2);
		else
		{
			ConstraintInnerLoop<Arity2Tag>(buffer, projectedSet1, set2[c2]);
		#pragma omp critical(GC_BCE_2)
			output.col(c2)=buffer;
		}   // if(!flagMultithreaded)
	}   //for(c2=0; c2<sSet2; ++c2)

	return output;
}	//BoolArrayT ExactBatchConstraintEvaluationA2(const InputRange1T& set1, const InputRange2T& set2) const

/**
 * @brief Evaluates the constraint on all pairs in the sets
 * @tparam InputRange1T Range holding the items in the first set
 * @tparam InputRange2T Range holding the items in the second set
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @param[in] dummy Dummy parameter for overload selection
 * @return A boolean array indicating the constraint conformance of all pairs in the sets
 * @pre \c InputRange1T is a forward range holding elements of type first_argument_type
 * @pre \c InputRange2T is a random access range holding elements of type second_argument_type
 */
template<class ErrorT>
template<class InputRange1T, class InputRange2T>
auto GeometricConstraintC<ErrorT>::BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2, Arity2Tag* dummy) const -> BoolArrayT
{
	if(flagApproximateNeighbourhoods)
		return ApproximateBatchConstraintEvaluationA2(set1, set2);
	else
		return ExactBatchConstraintEvaluationA2(set1, set2);

}   //BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2)

/**
 * @brief Evaluates the constraint on all pairs in the sets
 * @tparam InputRange1T Range holding the items in the first set
 * @tparam InputRange2T Range holding the items in the second set
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @param[in] dummy Dummy parameter for overload selection
 * @return A boolean array indicating the constraint conformance of all pairs in the sets
 * @pre \c InputRange1T is a forward range holding elements of type first_argument_type
 * @pre \c InputRange2T is a forward range holding elements of type second_argument_type
 */
template<class ErrorT>
template<class InputRange1T, class InputRange2T>
auto GeometricConstraintC<ErrorT>::BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2, Arity4Tag* dummy) const -> BoolArrayT
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange2T>));
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<InputRange1T>));
    static_assert(is_same< typename range_value<InputRange1T>::type, first_argument_type >::value, "GeometricConstraintC::BatchConstraintEvaluation: InputRange1T must hold elements of type first_argument_type");
    static_assert(is_same< typename range_value<InputRange2T>::type, second_argument_type >::value, "GeometricConstraintC::BatchConstraintEvaluation: InputRange2T must hold elements of type second_argument_type");

    //Cache the projections
    vector<Projected1T> projectedSet1=errorMetric.Project(set1);
    vector<Projected2T> projectedSet2=errorMetric.InverseProject(set2);

    size_t sSet1=boost::distance(set1);
    size_t sSet2=boost::distance(set2);

    BoolArrayT output(sSet1, sSet2);

    size_t c2=0;
    BoolVectorT buffer(sSet1);
	//TODO Explicitly assumes that the matrix is column-major. Should be able to deal with row-major matrices equally efficiently
#pragma omp parallel for  if(nThreads>1) schedule(static) private(c2) firstprivate(buffer) num_threads(nThreads)
    for(c2=0; c2<sSet2; ++c2)
    {
        //Compute the error using the pre-projected points
        if(nThreads==1)
            ConstraintInnerLoop(output, set1, projectedSet1, set2[c2], projectedSet2[c2], c2);
        else
        {
            ConstraintInnerLoop(buffer, set1, projectedSet1, set2[c2], projectedSet2[c2]);
    	#pragma omp critical(GC_BCE_4)
            output.col(c2)=buffer;
        }   // if(!flagMultithreaded)
    }   //for(c2=0; c2<sSet2; ++c2)

    return output;
}   //BoolArrayT BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2)

/**
 * @brief Default constructor
 * @post An invalid object
 */
template<class ErrorT>
GeometricConstraintC<ErrorT>::GeometricConstraintC() : nThreads(1), threshold(0), flagApproximateNeighbourhoods(false), flagValid(false)
{}

/**
 * @brief Constructor
 * @param[in] eerrorMetric Error metric
 * @param[in] tthreshold Error threshold. Unit: that of the target domain (e.g., pixel, m...)
 * @param[in] nnThreads Number of threads, for multithreaded operation
 * @param[in] fflagApproximateNeighbourhoods If \c true , fast approximate neighbourhoods by r-tree. Automatically set to \c false if \c flagEuclidean2D is false
 * @post A valid object
 */
template<class ErrorT>
GeometricConstraintC<ErrorT>::GeometricConstraintC(const ErrorT& eerrorMetric, typename ErrorT::result_type tthreshold, unsigned int nnThreads, bool fflagApproximateNeighbourhoods) : errorMetric(eerrorMetric), nThreads(nnThreads), threshold(tthreshold), flagApproximateNeighbourhoods(flagEuclidean2D&&fflagApproximateNeighbourhoods), flagValid(true)
{}

/**
 * @brief Checks whether the operands satisfy a distance constraint
 * @param[in] operand1 First operand
 * @param[in] operand2 Second operand
 * @return \c true if the constraint is satisfied
 * @pre \c flagValid=true
 */
template<class ErrorT>
bool GeometricConstraintC<ErrorT>::operator()(const first_argument_type& operand1, const second_argument_type& operand2) const
{
    //Preconditions
    assert(flagValid);
    return errorMetric(operand1, operand2)<=threshold;
}   //bool operator()(const first_argument_type& operand1, const second_argument_type& operand2)

/**
 * @brief Checks whether the operands satisfy a distance constraint
 * @param projector Algebraic entity defining the constraint
 * @param[in] operand1 First operand
 * @param[in] operand2 Second operand
 * @return \c true if the constraint is satisfied
 * @pre \c flagValid=true
 */
template<class ErrorT>
bool GeometricConstraintC<ErrorT>::operator()(const projector_type& projector, const first_argument_type& operand1, const second_argument_type& operand2) const
{
    //Preconditions
    assert(flagValid);
    return errorMetric(projector, operand1, operand2)<=threshold;
}   //bool operator()(const first_argument_type& operand1, const second_argument_type& operand2)

/**
 * @brief Evaluates the constraint on all pairs in the sets
 * @tparam InputRange1T Range holding the items in the first set
 * @tparam InputRange2T Range holding the items in the second set
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @return A boolean array indicating the constraint conformance of all pairs in the sets
 * @pre \c flagValid=true
 */
template<class ErrorT>
template<class InputRange1T, class InputRange2T>
auto GeometricConstraintC<ErrorT>::BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) const -> BoolArrayT
{
    //Preconditions
    assert(flagValid);
    typename ErrorT::arity_tag* dummy=nullptr;
    return BatchConstraintEvaluation(set1, set2, dummy);
}   //BatchConstraintEvaluation(const InputRange1T& set1, const InputRange2T& set2) -> BoolArrayT

/**
 * @brief Verifies the constraint, and returns the distance
 * @param[in] item1 First item
 * @param[in] item2 Second item
 * @return A tuple: [Constraint test; distance]
 * @pre \c flagValid=true
 */
template<class ErrorT>
auto GeometricConstraintC<ErrorT>::Enforce(const first_argument_type& item1, const second_argument_type& item2) const -> tuple<bool, distance_type>
{
	//Preconditions
	assert(flagValid);
	RealT value=errorMetric(item1, item2);
	return make_tuple(value<threshold, value);
}	//tuple<bool, distance_type> Enforce(const first_argument_type& item1, const second_argument_type& item2) const

/**
 * @brief Verifies the constraint, and returns the distance
 * @param[in] projector Algebraic entity defining the constraint
 * @param[in] item1 First item
 * @param[in] item2 Second item
 * @return A tuple: [Constraint test; distance]
 */
template<class ErrorT>
auto GeometricConstraintC<ErrorT>::Enforce(const projector_type& projector, const first_argument_type& item1, const second_argument_type& item2) const -> tuple<bool, RealT>
{
	//Preconditions
	assert(flagValid);
	RealT value=errorMetric(projector, item1, item2);
	return make_tuple(value<threshold, value);
}	//auto Enforce(const projector_type& projector, const first_argument_type& item1, const second_argument_type& item2) -> tuple<bool, RealT>

/**
 * @brief Sets the projector for the error metric
 * @param[in] projector Projector
 */
template<class ErrorT>
void GeometricConstraintC<ErrorT>::SetProjector(const projector_type& projector)
{
	errorMetric.SetProjector(projector);
}	//void SetProjector(const projector_type& projector)

/**
 * @brief Returns a constant reference to \c errorMetric
 * @return A constant reference to \c errorMetric
 * @pre \c flagValid=true
 */
template<class ErrorT>
auto GeometricConstraintC<ErrorT>::GetErrorMetric() const -> const ErrorT&
{
	assert(flagValid);
	return errorMetric;
}	//const ErrorT& GetErrorMetric()

}   //MetricsN
}	//SeeSawN

#endif /* GEOMETRIC_CONSTRAINT_IPP_9123545 */
