/**
 * @file Similarity.ipp Implementation of the similarity metrics
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SIMILARITY_IPP_3153548
#define SIMILARITY_IPP_3153548

#include <boost/concept_check.hpp>
#include "../Numeric/InvertibleUnaryFunctionConcept.h"

namespace SeeSawN
{
namespace MetricsN
{

using boost::AdaptableBinaryFunctionConcept;
using boost::DefaultConstructibleConcept;
using SeeSawN::NumericN::InvertibleUnaryFunctionConceptC;

/**
 * @brief Converts a distance value to a similarity value
 * @pre \c DistanceT is a default constructible binary functor
 * @pre \c MapT is a default constructible unary function that can accept the output of \c DistanceT
 * @remarks A model of InvertibleBinarySimilarityConceptC
 * @ingroup Metrics
 * @nosubgrouping
 */
template<class DistanceT, class MapT>
class DistanceToSimilarityConverterC
{
    //Preconditions
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<DistanceT, typename DistanceT::result_type, typename DistanceT::first_argument_type, typename DistanceT::second_argument_type >));
    BOOST_CONCEPT_ASSERT((InvertibleUnaryFunctionConceptC<MapT, typename DistanceT::result_type, typename DistanceT::result_type>));
    BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<DistanceT>));
    BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<MapT>));
    //@endcond
    private:

        /** @name Configuration */ //@{
        DistanceT distanceMetric;   ///< Distance metric
        MapT distanceToSimilarityMap;   ///< Maps a distance value to a similarity value
        //@}

    public:

        /** @name Constructors */ //@{
        DistanceToSimilarityConverterC();   ///< Default constructor
        DistanceToSimilarityConverterC(const DistanceT& ddistanceMetric, const MapT& ddistanceToSimilarityMap);    ///< Constructor
        //@}

        /**@name InvertibleBinarySimilarityConceptC interface*/ //@{
        typedef typename MapT::result_type result_type;
        typedef typename DistanceT::first_argument_type first_argument_type;
        typedef typename DistanceT::second_argument_type second_argument_type;
        typedef MapT map_type;
        typedef DistanceT distance_type;
        result_type operator()(const first_argument_type& item1, const second_argument_type& item2 ) const;    ///< Converts a distance value to a similarity value
        result_type Invert(result_type similarity) const;	///< Converts a similarity score to a distance
        //@}
};  //class DistanceToSimilarityConverterC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post \c distanceMetric and \c distanceToSimilarityMap are constructed by their default constructors
 */
template<class DistanceT, class MapT>
DistanceToSimilarityConverterC<DistanceT, MapT>::DistanceToSimilarityConverterC()
{}   //DistanceToSimilarityConverterC()

/**
 * @brief Constructor
 * @param[in] ddistanceMetric Distance metric
 * @param[in] ddistanceToSimilarityMap Conversion map
 * @post The object is initialised by the specified arguments
 */
template<class DistanceT, class MapT>
DistanceToSimilarityConverterC<DistanceT, MapT>::DistanceToSimilarityConverterC(const DistanceT& ddistanceMetric, const MapT& ddistanceToSimilarityMap) : distanceMetric(ddistanceMetric), distanceToSimilarityMap(ddistanceToSimilarityMap)
{}

/**
 * @brief Converts a distance value to a similarity value
 * @param[in] item1 First operand
 * @param[in] item2 Second operand
 * @return Similarity score
 */
template<class DistanceT, class MapT>
auto DistanceToSimilarityConverterC<DistanceT, MapT>::operator()(const first_argument_type& item1, const second_argument_type& item2 ) const -> result_type
{
    return distanceToSimilarityMap(distanceMetric(item1, item2));
}   //auto operator()(const first_argument_type& item1, const second_argument_type& item2 ) -> result_type

/**
 * @brief Converts a similarity score to a distance
 * @param[in] similarity A similarity score
 * @return The corresponding distance
 */
template<class DistanceT, class MapT>
auto DistanceToSimilarityConverterC<DistanceT, MapT>::Invert(result_type similarity) const -> result_type
{
	return distanceToSimilarityMap.Invert(similarity);
}	//auto Invert(result_type similarity) -> result_type
}   //MetricsN
}	//SeeSawN

#endif /* SIMILARITY_IPP_3153548 */
