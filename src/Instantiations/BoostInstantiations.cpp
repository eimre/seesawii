/**
 * @file BoostInstantiations.cpp Instantiation of common Boost templates
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/math/distributions/cauchy.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/tokenizer.hpp>
#include <string>
#include <cstddef>

//Boost.Optional
template class boost::optional<double>;
template class boost::optional<float>;
template class boost::optional<std::string>;

//Boost.LexicalCast
template std::string boost::lexical_cast<std::string, double>(const double&);
template std::string boost::lexical_cast<std::string, float>(const float&);
template std::string boost::lexical_cast<std::string, int>(const int&);
template std::string boost::lexical_cast<std::string, std::size_t>(const std::size_t&);
template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
template double boost::lexical_cast<double, std::string>(const std::string&);
template float boost::lexical_cast<float, std::string>(const std::string&);

//Boost.Geometry
template class boost::geometry::model::d2::point_xy<double>;
template class boost::geometry::model::d2::point_xy<float>;

//Boost.Tokenizer
template class boost::tokenizer< boost::char_separator<char> >;


