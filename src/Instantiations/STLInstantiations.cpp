/**
 * @file STLInstantiations.cpp Instantiations of common STL templates
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <cstddef>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <array>
#include <functional>
#include <tuple>
#include <complex>

template class std::vector<double>;
template class std::vector<float>;
template class std::vector<unsigned int>;
template class std::vector<std::size_t>;

template class std::list<double>;
template class std::list<float>;
template class std::list<std::size_t>;

template class std::set<double>;
template class std::set<float>;
template class std::set<int>;
template class std::set<unsigned int>;
template class std::set<std::size_t>;

template class std::multiset<double>;
template class std::multiset<float>;
template class std::multiset<std::size_t>;
template class std::multiset<unsigned int>;

template class std::map<double, std::size_t>;
template class std::map<std::size_t, double>;
template class std::map<float, std::size_t>;
template class std::map<std::size_t, float>;
template class std::map<std::size_t, unsigned int>;
template class std::map<unsigned int, std::size_t, std::greater<unsigned int> >;
template class std::map<unsigned int, unsigned int>;
template class std::map<std::string, double>;
template class std::map<std::string, std::string>;

template class std::multimap<double, std::size_t>;
template class std::multimap<double, std::size_t, std::greater<double> >;
template class std::multimap<double, unsigned int, std::greater<double> >;
template class std::multimap<float, std::size_t>;
template class std::multimap<float, std::size_t, std::greater<float> >;

template class std::array<double,2>;
template class std::array<double,3>;
template class std::array<double,4>;
template class std::array<std::complex<double>, 2>;
template class std::array<std::complex<double>, 3>;
template class std::array<float,2>;
template class std::array<float,3>;
template class std::array<float,4>;
template class std::array<std::complex<float>, 2>;
template class std::array<std::complex<float>, 3>;

template class std::tuple<std::vector<unsigned int>, std::vector<unsigned int> >;
template class std::tuple<std::vector<std::size_t>, std::vector<std::size_t> >;
template class std::tuple<unsigned int, unsigned int>;
template class std::tuple<std::size_t, std::size_t>;
template class std::tuple<double, double>;
