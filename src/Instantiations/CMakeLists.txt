add_library(seesaw_stl STATIC STLInstantiations.cpp)
set_property(TARGET seesaw_stl PROPERTY ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_LIBRARY_DIR})

add_library(seesaw_boost STATIC BoostInstantiations.cpp)
set_property(TARGET seesaw_boost PROPERTY ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_LIBRARY_DIR})

add_library(seesaw_eigen STATIC EigenInstantiations.cpp)
set_property(TARGET seesaw_eigen PROPERTY ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_LIBRARY_DIR})

install(TARGETS seesaw_stl
				seesaw_boost
				seesaw_eigen
		DESTINATION lib)