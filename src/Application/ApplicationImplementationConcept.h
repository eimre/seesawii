/*
 * @file ApplicationImplementationConcept.h Public interface for ApplicationImplementationConceptC
 * @author Evren Imre
 * @date 19 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef APPLICATION_IMPLEMENTATION_CONCEPT_H_8231613
#define APPLICATION_IMPLEMENTATION_CONCEPT_H_8231613

#include "ApplicationImplementationConcept.ipp"
namespace SeeSawN
{
namespace ApplicationN
{
template<class TestT> class ApplicationImplementationConceptC;  ///< Application implementation concept

}   //ApplicationN
}	//SeeSawN

#endif /* APPLICATION_IMPLEMENTATION_CONCEPT_H_8231613 */
