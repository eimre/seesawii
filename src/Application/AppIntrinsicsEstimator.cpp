/**
 * @file AppIntrinsicsEstimator.cpp Application for the estimation of the intrinsic parameters
 * @author Evren Imre
 * @date 3 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Application.h"
#include "../ApplicationInterface/InterfaceUtility.h"
#include <boost/range/algorithm.hpp>
#include <random>
#include <functional>
#include <fstream>
#include "../GeometryEstimationPipeline/LinearAutocalibrationPipeline.h"
#include "../IO/CameraIO.h"
#include "../IO/ArrayIO.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../ApplicationInterface/InterfaceLinearAutocalibrationPipeline.h"
#include "../ApplicationInterface/InterfaceUtility.h"

namespace SeeSawN
{
namespace AppIntrinsicsEstimatorN
{

using namespace ApplicationN;
using boost::range::transform;
using std::mt19937_64;
using std::ofstream;
using std::bind;
using std::greater;
using std::round;
using std::move;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineC;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineParametersC;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineDiagnosticsC;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::IsWriteable;

/**
 * @brief Parameters for \c intrinsicsEstimator
 * @ingroup Application
 */
struct AppIntrinsicsEstimatorParametersC
{
	string inputFilename;	///< Filename for the input

	string outputPath;	///< Path for the output
	string cameraFilename;	///< Filename for the estimated camera matrices
	string homographyFilename;	///< Filename for the rectifying homography
	string logFilename;	///< Filename for the application log
	bool flagVerbose;	///< If \c true , verbose output

	int seed;	///< Random number seed. If <0, default

	bool flagUniform;	///< Uniform intrinsic calibration matrix for all cameras
	optional<Coordinate2DT> principalPoint;	///< Principal point. If invalid, automatically determined by the algorithm
	LinearAutocalibrationPipelineParametersC pipelineParameters;	///< Parameters for the LAC pipeline

	AppIntrinsicsEstimatorParametersC() : flagVerbose(true), seed(0), flagUniform(false)
	{}
};	//struct AppIntrinsicsEstimatorParametersC

/**
 * @brief Implementation of \c intrinsicsEstimator
 * @remarks Usage notes
 * 	- This tool works best to refine an existing metric calibration, as the automatic estimation of the initial estimates in LAC is not really accurate
 * 	- The ids of the successfully calibrated cameras as indicated as tags
 * @ingroup Application
 */
//FIXME The user should be able to provide initial estimates
class AppIntrinsicsEstimatorImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed
		AppIntrinsicsEstimatorParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string,double> logger;	///< Logs various values
		//@}

		/** @name Implementation details */ //@{
		tuple<vector<CameraMatrixT>, vector<CameraC>, vector<IntrinsicC> > LoadData();	///< Loads the input data
		void SaveLog(const LinearAutocalibrationPipelineDiagnosticsC& diagnostics);	///< Saves the log
		void SaveOutput(const Homography3DT& mH, const vector<CameraC>& cameraList, const map<size_t, IntrinsicCalibrationMatrixT>& inliers);	///< Saves the output
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppIntrinsicsEstimatorImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//class AppIntrinsicsEstimatorImplementationC

/**
 * @brief Loads the input data
 * @return A tuple: Camera matrices, cameras and initial estimates for the intrinsic parameters
 * @throws invalid_argument If there are too few cameras, or no valid camera matrix exists for any of the entries
 */
tuple<vector<CameraMatrixT>, vector<CameraC>, vector<IntrinsicC> > AppIntrinsicsEstimatorImplementationC::LoadData()
{
	tuple<vector<CameraMatrixT>, vector<IntrinsicC> > output;

	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.inputFilename);
	size_t nCameras=cameraList.size();

	if(nCameras<parameters.pipelineParameters.sGenerator)
		throw(invalid_argument(string("AppIntrinsicsEstimatorImplementationC::LoadData : The camera list must have at least ") + lexical_cast<string>(parameters.pipelineParameters.sGenerator) + string(". Value= ") + lexical_cast<string>(nCameras)) );

	//Initial estimates
	IntrinsicC defaultIntrinsic;	//The xml keeps even the projective cameras in decomposed form. So, the initial estimate will be derived within LAC
	defaultIntrinsic.principalPoint=parameters.principalPoint;	//If invalid, LAC pipeline initialises the principal point from the decomposition of the camera matrix
	vector<IntrinsicC> intrinsics(nCameras, defaultIntrinsic);

	//Camera matrices
	vector<CameraMatrixT> mPList; mPList.reserve(nCameras);

	for(size_t c=0; c<nCameras; ++c)
	{
		optional<CameraMatrixT> mP=cameraList[c].MakeCameraMatrix();

		if(!mP)
			throw(invalid_argument(string("AppIntrinsicsEstimatorImplementationC::LoadData : Failed to compute the camera matrix for the entry ") + lexical_cast<string>(c) ));

		mPList.push_back(*mP);
	}	//for(size_t c=0; c<nCameras; ++c)

	return make_tuple(move(mPList), move(cameraList), move(intrinsics));
}	//tuple<vector<CameraMatrixT>, vector<IntrinsicC> > LoadData()

/**
 * @brief Saves the log
 * @param[in] diagnostics Diagnostics
 */
void AppIntrinsicsEstimatorImplementationC::SaveLog(const LinearAutocalibrationPipelineDiagnosticsC& diagnostics)
{
	if(parameters.logFilename.empty())
		return;

	ofstream logfile(parameters.outputPath+parameters.logFilename);
	if(logfile.fail())
		throw(runtime_error(string("AppIntrinsicsEstimatorImplementationC::SaveOutput: Cannot open file ")+parameters.outputPath+parameters.logFilename));

	string timeString= to_simple_string(second_clock::universal_time());
	logfile<<"IntrinsicsEstimator log file created on "<<timeString<<"\n";

	logfile<<"# Cameras: "<<logger["NoCameras"]<<" \n";
	logfile<<"# Calibrated: "<<round(logger["NoCameras"]*diagnostics.ransacDiagnostics.inlierRatio)<<"\n";

	if(diagnostics.flagSuccess)
		logfile<<"Error: "<<diagnostics.error<<"\n";

	logfile<<"# RANSAC iterations: "<<diagnostics.ransacDiagnostics.nIterations<<"\n";
	logfile<<"Time Elapsed: "<<logger["TimeElapsed"]<<" s\n";

}	//void SaveLog(const LinearAutocalibrationPipelineDiagnosticsC& diagnostics)

/**
 * @brief Saves the output
 * @param[in] mH Rectifying homography
 * @param[in] cameraList Projective cameras
 * @param[in] inliers Intrinsic calibration matrices
 */
void AppIntrinsicsEstimatorImplementationC::SaveOutput(const Homography3DT& mH, const vector<CameraC>& cameraList, const map<size_t, IntrinsicCalibrationMatrixT>& inliers)
{
	//Save the homography
	if(!parameters.homographyFilename.empty())
		ArrayIOC<Homography3DT>::WriteArray(parameters.outputPath+parameters.homographyFilename, mH);

	//Save the cameras: Overwrite the intrinsic calibration parameters of the original camera list, and delete the extrinsics
	vector<CameraC> recovered; recovered.reserve(inliers.size());
	for(const auto& current : inliers)
	{
		CameraC newCamera(cameraList[current.first]);

		newCamera.Extrinsics()=ExtrinsicC();

		newCamera.Intrinsics()->focalLength=current.second(0,0);
		newCamera.Intrinsics()->aspectRatio=current.second(1,1)/current.second(0,0);
		newCamera.Intrinsics()->skewness = current.second(0,1);
		newCamera.Intrinsics()->principalPoint=current.second.col(2).head(2);

		newCamera.Tag()=lexical_cast<string>(current.first);

		recovered.push_back(newCamera);
	}	//for(const auto& current : cameraList)

	CameraIOC::WriteCameraFile(recovered, parameters.outputPath+parameters.cameraFilename, "Inlier cameras only. Original camera ids are indicated as tags.", true);
}	//void SaveOutput(const Homography3DT& mH, const map<size_t, CameraMatrixT>& cameraList)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppIntrinsicsEstimatorImplementationC::AppIntrinsicsEstimatorImplementationC()
{
	AppIntrinsicsEstimatorParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.seed)), "Seed for the random number generator. If <0, rng default")

	("Problem.FlagUniformIntrinsics", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagUniform)), "True if all cameras have identical intrinsics")
	("Problem.PrincipalPoint", value<string>(), "Initial estimate for the principal point. If omitted, derived from the camera decomposition")

	("Estimator.Main.InlierThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.inlierTh)), "Inlier threshold. >0")
	("Estimator.Main.NoWeights", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.nLACIteration)), "The number of different weights for which a minimal linear autocalibration problem is solved")

	("Estimator.RANSAC.Main.GeneratorSize", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.sGenerator)), "Size of the minimal generator. >=4")
	("Estimator.RANSAC.Main.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.nThreads)), "Number of cores available to RANSAC. ")
	("Estimator.RANSAC.Main.MinConfidence", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.minConfidence)), "Minimum probability that RANSAC finds an acceptable solution. (0,1]")
	("Estimator.RANSAC.Main.MinIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.minIteration)), "Minimum number of iterations. >=0")
	("Estimator.RANSAC.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.maxIteration)), "Maximum number of iterations. >=minIteration")
	("Estimator.RANSAC.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("Estimator.RANSAC.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.minError)), "If the error for the solution is below this value, the process is terminated")
	("Estimator.RANSAC.Main.MaxError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.maxError)), "If the error for the solution is above this value, it is rejected. >=minError")
	("Estimator.RANSAC.Main.TopN", value<string>(), "Maximum rank of the result. If specified, overrides other termination conditions.[0,1]")

	("Estimator.RANSAC.LORANSAC.GeneratorSize", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.sLOGenerator)), "Size of the generator for LO-RANSAC. >=4")
	("Estimator.RANSAC.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.nLOIterations)), "Number of local optimisation iterations. >=0")
	("Estimator.RANSAC.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("Estimator.RANSAC.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")

	("Estimator.RANSAC.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("Estimator.RANSAC.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("Estimator.RANSAC.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("Input.CameraFile", value<string>(), "Camera filename")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.RectifyingHomography", value<string>(), "File containing the rectifying homography")
	("Output.RectifiedCameras", value<string>(), "File containing the rectified cameras")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output")
	;
}	//AppIntrinsicsEstimatorImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppIntrinsicsEstimatorImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppIntrinsicsEstimatorImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppIntrinsicsEstimatorParametersC defaultParameters;

	tree.put("xmlcomment", "intrinsicsEstimator configuration file");

	tree.put("General","");
	tree.put("Problem","");
	tree.put("Estimator","");
	tree.put("Input","");
	tree.put("Output","");

	tree.put("General.Seed", defaultParameters.seed);

	tree.put("Problem.FlagUniformIntrinsics", defaultParameters.flagUniform);
	tree.put("Problem.PrincipalPoint", "959.5, 539.5");

	tree.put("Input.CameraFile", "projective.cam");

	tree.put("Output.Root", "/");
	tree.put("Output.RectifyingHomography", "mH.txt");
	tree.put("Output.RectifiedCameras", "rectified.cam");
	tree.put("Output.Log", "intrinsicsEstimator.log");
	tree.put("Output.Verbose", defaultParameters.flagVerbose);

	tree.put_child("Estimator", InterfaceLinearAutocalibrationPipelineC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel));

    xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
   	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppIntrinsicsEstimatorImplementationC::SetParameters(const ptree& tree)
{
	AppIntrinsicsEstimatorParametersC defaultParameters;

	//General
	parameters.seed=tree.get<bool>("General.Seed", defaultParameters.seed);

	//Problem
	parameters.flagUniform=tree.get<bool>("Problem.FlagUniformIntrinsics", defaultParameters.flagUniform);

	if(tree.get_optional<double>("Problem.PrincipalPoint"))
	{
		vector<double> principalPoint=ReadParameterArray<double>(tree, "Problem.PrincipalPoint", 2, [](double){return true;}, "", " ");
		parameters.principalPoint=Coordinate2DT(principalPoint[0], principalPoint[1]);
	}	//if(tree.get_optional<double>("Problem.principalPoint"))

	//Estimator
	if(tree.get_child_optional("Estimator"))
		parameters.pipelineParameters=InterfaceLinearAutocalibrationPipelineC::MakeParameterObject(tree.get_child("Estimator"));

	//Input
	parameters.inputFilename=tree.get<string>("Input.CameraFile", "");

	//Output

	parameters.outputPath=tree.get<string>("Output.Root","");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppIntrinsicsEstimatorImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.homographyFilename=tree.get<string>("Output.RectifyingHomography","");
	parameters.cameraFilename=tree.get<string>("Output.RectifiedCameras", "");
	parameters.logFilename=tree.get<string>("Output.Log", "");

	parameters.flagVerbose=tree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppIntrinsicsEstimatorImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the camera file..\n";

	//Load the data
	vector<CameraMatrixT> mPList;
	vector<CameraC> cameraList;
	vector<IntrinsicC> initialEstimates;
	tie(mPList, cameraList, initialEstimates)=LoadData();

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<initialEstimates.size()<<" cameras. Running the autocalibration pipeline...\n";

	//RNG
	mt19937_64 rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	//Linear autocalibration
	Homography3DT mH;
	map<size_t, IntrinsicCalibrationMatrixT> inliers;
	LinearAutocalibrationPipelineDiagnosticsC diagnostics=LinearAutocalibrationPipelineC::Run(mH, inliers, rng, mPList, initialEstimates, parameters.pipelineParameters);

	if(parameters.flagVerbose)
	{
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: ";

		if(diagnostics.flagSuccess)
			cout<< "Calibrated "<<inliers.size()<<" cameras... Saving the output...\n";
		else
			cout<<" Operation failed. Terminating...\n";
	}	//if(parameters.flagVerbose)

	logger["NoCameras"]=mPList.size();
	logger["TimeElapsed"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	//Output
	if(diagnostics.flagSuccess)
		SaveOutput(mH, cameraList, inliers);

	SaveLog(diagnostics);

	return true;
}	//bool Run()
}	//AppIntrinsicsEstimatorN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("150303");
    std::string header("intrinsicsEstimator: Estimates the intrinsics parameters for a set of projective cameras");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppIntrinsicsEstimatorN::AppIntrinsicsEstimatorImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
