/*
 * @file ApplicationImplementationConcept.ipp Implementation of ApplicationImplementationConceptC
 * @author Evren Imre
 * @date 19 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef APPLICATION_IMPLEMENTATION_CONCEPT_IPP_6132643
#define APPLICATION_IMPLEMENTATION_CONCEPT_IPP_6132643

#include <boost/concept_check.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <string>

namespace SeeSawN
{
namespace ApplicationN
{

using boost::program_options::options_description;
using boost::property_tree::ptree;
using std::string;

/**
 * @brief Application implementation concept
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class ApplicationImplementationConceptC
{
	///@cond CONCEPT_CHECK
    public:
        BOOST_CONCEPT_USAGE(ApplicationImplementationConceptC)
        {
            TestT tested;
            options_description cmdOp=tested.GetCommandLineOptions(); (void)cmdOp;

            ptree tree;
            bool flagSet=tested.SetParameters(tree); (void)flagSet;

            string filename;
            int detailLevel;
            TestT::GenerateConfigFile(filename, detailLevel);

            bool flagRun=tested.Run(); (void)flagRun;
        }   //BOOST_CONCEPT_USAGE(ApplicationImplementationConceptC)
	///@endcond
};  //class ApplicationImplementationConceptC

}   //ApplicationN
}	//SeeSawN

#endif /* APPLICATION_IMPLEMENTATION_CONCEPT_IPP_6132643 */
