/**
 * @file AppSparseModelBuilder.cpp Implementation of sparseModelBuilder
 * @author Evren Imre
 * @date 18 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/format.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include "Application.h"
#include "../GeometryEstimationPipeline/Sparse3DReconstructionPipeline.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../IO/CameraIO.h"
#include "../IO/ImageFeatureIO.h"
#include "../IO/ArrayIO.h"
#include "../IO/SpecialArrayIO.h"
#include "../IO/SceneFeatureIO.h"
#include "../Elements/Camera.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/MultiviewTriangulation.h"
#include "../Geometry/LensDistortion.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace AppSparseModelBuilderN
{

using namespace ApplicationN;

using boost::format;
using boost::str;
using Eigen::RowVector3d;
using Eigen::Matrix;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineParametersC;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineC;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineDiagnosticsC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::MultiviewTriangulationC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ION::WriteCovFile;
using SeeSawN::ION::SceneFeatureIOC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::NumericN::ReciprocalC;

/**
 * @brief Parameters for sparseModelBuilder
 * @ingroup Application
 */
struct AppSparseModelBuilderParametersC
{
	unsigned int nThreads;	///< Number of threads available to the application

	string featureFilePattern;	///< Filename pattern for the feature files
	double spacing;	///< Grid spacing
	string cameraFile;	///< Camera filename

	string outputPath;	///< Output path
	string logFile;	///< Filename for the log file
	string modelFile;	///< Filename for the 3D model
	string covarianceFile;	///< Name of the file for 3D model with covariance
	string modelFeatureFile;	///< Filename for the 3D model file
	bool flagVerbose;	///< Verbose output

	Sparse3DReconstructionPipelineParametersC sparse3DParameters;	///< Parameters for the sparse 3D reconstruction module

	AppSparseModelBuilderParametersC() : nThreads(1), spacing(0), flagVerbose(false)
	{}
};	//struct AppSparseModelBuilderParametersC

/**
 * @brief Implementation of sparseModelBuilder
 * @ingroup Application
 */
class AppSparseModelBuilderImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		AppSparseModelBuilderParametersC parameters;  ///< Application parameters
		//@}

		/**@name Implementation details */ //@{
		tuple<vector<CameraMatrixT>, vector<LensDistortionC> > LoadCameras(const string& filename) const;	///< Loads the camera file
		vector<vector<ImageFeatureC> > LoadFeatures(const string& filename, const vector<LensDistortionC>& distortionList);	///< Loads the image features

		void SaveLog(const Sparse3DReconstructionPipelineDiagnosticsC& diagnostics, double runtime);	///< Saves the log
		void SaveOutput(const vector<Coordinate3DT>& pointCloud, const vector<MultiviewTriangulationC::covariance_matrix_type>& covarianceList, vector<SceneFeatureC>& sceneFeatures, const vector<LensDistortionC>& lensDistortion);	///< Saves the output
		//@}

	public:

    	/**@name ApplicationImplementationConceptC interface */ //@{
		AppSparseModelBuilderImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
    	//@}
};	//class AppSparseModelBuilderImplementationC

/**
 * @brief Loads the camera file
 * @param[in] filename Camera filename
 * @return A tuple of vectors: Camera matrices and distortion objects
 * @throws runtime_error If no valid camera matrix any of the cameras present in the camera file
 */
tuple<vector<CameraMatrixT>, vector<LensDistortionC> > AppSparseModelBuilderImplementationC::LoadCameras(const string& filename) const
{
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(filename);
	size_t nCamera=cameraList.size();
	vector<CameraMatrixT> cameraMatrixList(nCamera);
	vector<LensDistortionC> distortionList(nCamera);
	for(size_t c=0; c<nCamera; ++c)
	{
		//Camera matrix

		optional<CameraMatrixT> mP=cameraList[c].MakeCameraMatrix();

		if(!mP)
			throw(runtime_error(string("AppSparseModelBuilderImplementationC::LoadCameras : Invalid camera matrix for the camera ")+lexical_cast<string>(c)+string(", with the tag ")+cameraList[c].Tag()));

		cameraMatrixList[c].swap(*mP);

		//Lens distortion

		optional<LensDistortionC> distortion=cameraList[c].Distortion();
		LensDistortionC dummy; dummy.modelCode=LensDistortionCodeT::NOD;
		distortionList[c] = distortion ? *distortion : dummy;
	}	//for(size_t c=0; c<nCamera; ++c)

	return make_tuple(cameraMatrixList, distortionList);
}	//vector<CameraMatrixT> LoadCameras(const string& filename);	///< Loads the camera files

/**
 * @brief Loads the image features
 * @param[in] filename Filename pattern
 * @param[in] distortionList Lens distortion list
 * @return A vector of feature files
 */
vector< vector<ImageFeatureC> > AppSparseModelBuilderImplementationC::LoadFeatures(const string& filename, const vector<LensDistortionC>& distortionList)
{
	size_t nCamera=distortionList.size();
	vector<vector<ImageFeatureC> > output(nCamera);

	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) num_threads(parameters.nThreads)
	for(c=0; c<nCamera; ++c)
	{
		string featureFile=str(format(filename)%c);
		vector<ImageFeatureC> featureList=ImageFeatureIOC::ReadFeatureFile(featureFile, true);

		optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
		optional<CoordinateTransformations2DT::affine_transform_type> normaliser;
		ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, parameters.spacing, true, distortionList[c], normaliser);

	#pragma omp critical (ASMBI_LF)
		{
			cout<<"Loaded "<<featureList.size()<<" features from Set "<<c<<"\n";
			output[c].swap(featureList);
		}
	}	//for(size_t c=0; c<nCamera; ++c)

	return output;
}	//vector<ImageFeatureC> LoadFeatures(const string& filename, const vector<LensDistortionC>& distortionList)

/**
 * @brief Saves the log
 * @param[in] diagnostics Diagnostics
 * @param[in] runtime Runtime
 * @throws runtime_error If the write operation fails
 */
void AppSparseModelBuilderImplementationC::SaveLog(const Sparse3DReconstructionPipelineDiagnosticsC& diagnostics, double runtime)
{
	if(parameters.logFile.empty())
		return;

	string filename=parameters.outputPath+parameters.logFile;
	ofstream logfile(filename);

	if(logfile.fail())
		throw(runtime_error(string("AppSparseModelBuilderImplementationC::SaveLog : Failed to open the file for writing at ")+filename));

    string timeString= to_simple_string(second_clock::universal_time());
    logfile<<"sparseModelBuilder log file created on "<<timeString<<"\n";

    logfile<<"Number of camera pairs: "<<diagnostics.matcherDiagnostics.nSourcePairs<<"\n";
    logfile<<"Number of observed correspondences: "<<diagnostics.matcherDiagnostics.nObserved<<"\n";
    logfile<<"Number of inferred correspondences: "<<diagnostics.matcherDiagnostics.nImplied<<"\n";
    logfile<<"Number of pairwise 3D observations: "<<diagnostics.triangulationDiagnositics.nMeasurements<<"\n";
    logfile<<"Number of original 3D points: "<<diagnostics.triangulationDiagnositics.nOriginal3D<<"\n";
    logfile<<"Number of filtered 3D points: "<<diagnostics.triangulationDiagnositics.nFiltered3D<<"\n";

    logfile<<"Run time: "<<runtime<<"s";
}	//void SaveLog(const Sparse3DReconstructionPipelineDiagnosticsC& diagnostics, double runtime)

/**
 * @brief Saves the output
 * @param[in] pointCloud 3D point cloud
 * @param[in] covarianceList Covariance list
 * @param[in, out] sceneFeatures Scene features. At the output, all 2D coordinates are distorted
 * @param[in] lensDistortion Lens distortion parameters
 * @throws runtime_error If the write operation fails
 */
void AppSparseModelBuilderImplementationC::SaveOutput(const vector<Coordinate3DT>& pointCloud, const vector<MultiviewTriangulationC::covariance_matrix_type>& covarianceList, vector<SceneFeatureC>& sceneFeatures, const vector<LensDistortionC>& lensDistortion)
{
	if(!parameters.modelFile.empty())
	{
		string filename=parameters.outputPath+"/"+parameters.modelFile;
		ofstream modelFile(filename);

		if(modelFile.fail())
			throw(runtime_error(string("AppSparseModelBuilderImplementationC::SaveOutput : Failed to open the file for writing at ")+filename));

		typedef SeeSawN::ION::ArrayIOC<RowVector3d> ArrayIOT;

		for(const auto& current : pointCloud)
			ArrayIOT::WriteArray(modelFile, current.transpose());
	}	//if(!parameters.modelFile.empty())

	if(!parameters.covarianceFile.empty())
	{
		string filename=parameters.outputPath+"/"+parameters.covarianceFile;
		WriteCovFile(filename, pointCloud, covarianceList);
	}	//if(!parameters.covarianceFile.empty())

	if(!parameters.modelFeatureFile.empty())
	{
		//Apply the lens distortion back
		size_t nCamera=lensDistortion.size();
		vector<optional<CoordinateTransformations2DT::projective_transform_type> > dummy(nCamera);
		SceneFeatureIOC::PostprocessDescriptors(sceneFeatures, lensDistortion, dummy);

		string filename=parameters.outputPath+"/"+parameters.modelFeatureFile;
		SceneFeatureIOC::WriteFeatureFile(sceneFeatures, filename);
	}	//if(!parameters.modelFeatureFile.empty())
}	//void SaveOutput(const vector<Coordinate3DT>& pointCloud, const vector<MultiviewTriangulationC::covariance_matrix_type>& covarianceList, const vector<SceneFeatureC>& sceneFeatures)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppSparseModelBuilderImplementationC::AppSparseModelBuilderImplementationC()
{
	AppSparseModelBuilderParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("Environment.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")

	("General.Noise", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.noiseVariance)), "Variance of the noise on the image coordinates, due to the detector. pixel^2. >0")
	("General.pReject", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.pRejection)), "Probability of rejection for an inlier. Determines the inlier threshold. [0,1]")

	("Matcher.FeatureMatcher.MaxRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the similarity score of the best outsider to that of the best insider. See the documentation of NearestNeighbourMatcherC. [0,1]")
	("Matcher.FeatureMatcher.MaxFeatureDistance", value<string>()->default_value("0.5"), "Maximum distance between the normalised descriptor vectors of two corresponding points, as a percentage of the maximum possible distance.[0 1]")

	("Matcher.BucketFilter.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.flagBucketFilter)), "If true, the bucket filter is enabled. Useful for eliminating outliers. 0 or 1")
	("Matcher.BucketFilter.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity between two buckets for a valid bucket match. [0,1]")
	("Matcher.BucketFilter.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.minQuorum)), "If no buckets in a pair has this many votes, the pair is exempted due to insufficient evidence for rejection (i.e., automatic success).>=0")
	("Matcher.BucketFilter.BucketDensity", value<string>()->default_value(lexical_cast<string>(1.0/defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.bucketDimensions[0])), "Number of buckets, per unit standard deviation. >0")

	("Matcher.Fusion.MinimumClusterSize", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.matcherParameters.minCardinality)), "Minimum number of views in which a valid 3D point is observed. >=2")
	("Matcher.Fusion.FlagSequentialOperation", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.matcherParameters.flagSequentialOperation)), "If true, the matching problems are solved sequentially. Recommended for large datasets and/or low-memory environments.")

	("Triangulation.FastTriangulation", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.triangulationParameters.flagFastTriangulation)), "If true, fast triangulation is employed.")
	("Triangulation.MaxDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.triangulationParameters.maxDistanceRank)), "Maximum percentile point for an admissible scene point, when ranked wrt distance to the median of the point cloud. (0,1]")
	("Triangulation.MaxTrace", value<string>()->default_value(lexical_cast<string>(defaultParameters.sparse3DParameters.triangulationParameters.maxCovarianceTrace)), "Maximum value of the trace of the covariance for an admissible scene point. >0")
	("Triangulation.Alpha", value<string>()->default_value("0.866"), "Sample spread parameter for SUT. Lower values a better for highly nonlinear transformations, however, too small values may lead to ill-conditioned covariance matrices. (0,1]. If not defined/invalid, automatic.")

	("Input.Feature.Filename", value<string>(), "Feature filename, with a single printf-like token (e.g. feature%02d.ft2)")
	("Input.Feature.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.spacing)), "Affects the decimation factor of the initial set. 0 means no decimation. See the documentation of DecimateFeatures2D. >=0")
	("Input.CameraFile", value<string>(), "Camera filename")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Model", value<string>()->default_value(""), "3D coordinates. asc")
	("Output.Covariances", value<string>()->default_value(""), "3D coordinates with covariance. cov3")
	("Output.FeatureList", value<string>()->default_value(""), "3D feature list")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output")
	;
}	//AppSparseModelBuilderImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppSparseModelBuilderImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppSparseModelBuilderImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	AppSparseModelBuilderParametersC defaultParameters;

    ptree tree; //Options tree

    tree.put("xmlcomment", "sparseModelBuilder configuration file");

    tree.put("Environment","");
    tree.put("Environment.NoThreads",defaultParameters.nThreads);

    tree.put("General", "");

    tree.put("Matcher","");

    tree.put("Matcher.FeatureMatcher","");

    tree.put("Matcher.BucketFilter","");

    tree.put("Matcher.Fusion","");

    tree.put("Triangulation","");

    tree.put("Input", "");

    tree.put("Input.Feature","");
    tree.put("Input.Feature.Filename","feature%02d");

    tree.put("Input.CameraFile", "camera.cal");

    tree.put("Output","");
	tree.put("Output.Root","/home/");
	tree.put("Output.Model","pointCloud.asc");
	tree.put("Output.Covariances","covariances.cov3");
	tree.put("Output.Log","sparseModelBuilder.log");
	tree.put("Output.FeatureList","pointCloud.ft3");
	tree.put("Output.Verbose", defaultParameters.flagVerbose);

    if(detailLevel>1)
    {
    	tree.put("General.Noise", defaultParameters.sparse3DParameters.noiseVariance);
		tree.put("General.pReject", defaultParameters.sparse3DParameters.pRejection);

		tree.put("Matcher.FeatureMatcher.MaxRatio", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.nnParameters.ratioTestThreshold);
		tree.put("Matcher.FeatureMatcher.MaxFeatureDistance", "0.5");

		tree.put("Matcher.BucketFilter.Enable", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.flagBucketFilter);
		tree.put("Matcher.BucketFilter.MinSimilarity", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.bucketSimilarityThreshold);
		tree.put("Matcher.BucketFilter.MinQuorum", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.minQuorum);
		tree.put("Matcher.BucketFilter.BucketDensity", 1.0/defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.bucketDimensions[0]);

		tree.put("Matcher.Fusion.MinimumClusterSize", defaultParameters.sparse3DParameters.matcherParameters.minCardinality);
		tree.put("Matcher.Fusion.FlagSequentialOperation", defaultParameters.sparse3DParameters.matcherParameters.flagSequentialOperation);

		tree.put("Triangulation.FastTriangulation", defaultParameters.sparse3DParameters.triangulationParameters.flagFastTriangulation);
		tree.put("Triangulation.MaxDistance", defaultParameters.sparse3DParameters.triangulationParameters.maxDistanceRank);
		tree.put("Triangulation.MaxTrace", defaultParameters.sparse3DParameters.triangulationParameters.maxCovarianceTrace);
		tree.put("Triangulation.Alpha", "0.866");

		tree.put("Input.Feature.GridSpacing", defaultParameters.spacing);
    }	//if(!flagBasic)

    xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppSparseModelBuilderImplementationC::SetParameters(const ptree& tree)
{
	AppSparseModelBuilderParametersC defaultParameters;

	//Environment
	parameters.nThreads=min(omp_get_max_threads(), max(1, tree.get<int>("Environment.NoThreads", defaultParameters.nThreads)));
	parameters.sparse3DParameters.matcherParameters.nThreads=parameters.nThreads;

	//General
	double noise=tree.get<double>("General.Noise", defaultParameters.sparse3DParameters.noiseVariance);
	if(noise<=0)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters: General.Noise must be a positive value. Value=")+lexical_cast<string>(noise) ) );
	else
		parameters.sparse3DParameters.noiseVariance=noise;

	//Matching

	double maxRatio=tree.get<double>("Matcher.FeatureMatcher.MaxRatio", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.nnParameters.ratioTestThreshold);
	if(maxRatio<0 || maxRatio>1)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters: Matcher.FeatureMatcher.MaxRatio must be in [0,1]. Value=")+lexical_cast<string>(maxRatio) ) );
	else
		parameters.sparse3DParameters.matcherParameters.matcherParameters.nnParameters.ratioTestThreshold=maxRatio;

	double maxFeatureDistance=tree.get<double>("Matcher.FeatureMatcher.MaxFeatureDistance", 0.5);
	if(maxFeatureDistance<0 || maxFeatureDistance>1)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters: Matcher.FeatureMatcher.MaxFeatureDistance must be in [0,1]. Value=")+lexical_cast<string>(maxFeatureDistance) ) );
	else
		parameters.sparse3DParameters.matcherParameters.matcherParameters.nnParameters.similarityTestThreshold=maxFeatureDistance*sqrt(2);	//Max distance between two unit-norm vectors is sqrt(2)

	bool flagBucketFilter=tree.get<bool>("Matcher.BucketFilter.Enable", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.flagBucketFilter);
	parameters.sparse3DParameters.matcherParameters.matcherParameters.flagBucketFilter=flagBucketFilter;

	if(flagBucketFilter)
	{
		double minBucketSimilarity=tree.get<double>("Matcher.BucketFilter.MinSimilarity", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.bucketSimilarityThreshold);
		if(minBucketSimilarity<0 || minBucketSimilarity>1)
			throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters: Matcher.BucketFilter.MinSimilarity is not in [0,1]. Value=")+lexical_cast<string>(minBucketSimilarity)));
		else
			parameters.sparse3DParameters.matcherParameters.matcherParameters.bucketSimilarityThreshold=minBucketSimilarity;

		unsigned int minQuorum=tree.get<unsigned int> ("Matcher.BucketFilter.MinQuorum", defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.minQuorum);
		parameters.sparse3DParameters.matcherParameters.matcherParameters.minQuorum=minQuorum;

		unsigned int bucketDensity=tree.get<unsigned int>("Matcher.BucketFilter.BucketDensity", 1.0/defaultParameters.sparse3DParameters.matcherParameters.matcherParameters.bucketDimensions[0]);
		if(bucketDensity<=0)
			throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters: Matcher.BucketFilter.BucketDensity is not positive. Value=")+lexical_cast<string>(bucketDensity)));

		parameters.sparse3DParameters.matcherParameters.matcherParameters.bucketDimensions.fill(1.0/bucketDensity);
	}	//if(flagBucketFilter)

	unsigned int minimumClusterSize=tree.get<unsigned int>("Matcher.Fusion.MinimumClusterSize", defaultParameters.sparse3DParameters.matcherParameters.minCardinality);
	parameters.sparse3DParameters.matcherParameters.minCardinality=minimumClusterSize;

	parameters.sparse3DParameters.matcherParameters.flagSequentialOperation=tree.get<bool>("Matcher.Fusion.FlagSequentialOperation", defaultParameters.sparse3DParameters.matcherParameters.flagSequentialOperation);

	//Triangulation

	bool flagFastTriangulation=tree.get<bool>("Triangulation.FastTriangulation", defaultParameters.sparse3DParameters.triangulationParameters.flagFastTriangulation);
	parameters.sparse3DParameters.triangulationParameters.flagFastTriangulation=flagFastTriangulation;

	double maxDistanceToMedian=tree.get<double>("Triangulation.MaxDistance", defaultParameters.sparse3DParameters.triangulationParameters.maxDistanceRank);
	if(maxDistanceToMedian<=0 || maxDistanceToMedian>1)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters : Triangulation.MaxDistance must be (0,1]. Value=")+lexical_cast<string>(maxDistanceToMedian)));
	parameters.sparse3DParameters.triangulationParameters.maxDistanceRank=maxDistanceToMedian;

	double maxTrace=tree.get<double>("Triangulation.MaxTrace", defaultParameters.sparse3DParameters.triangulationParameters.maxCovarianceTrace);
	if(maxTrace<=0)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters : Triangulation.MaxDistance must be positive. Value=")+lexical_cast<string>(maxTrace)));
	parameters.sparse3DParameters.triangulationParameters.maxCovarianceTrace=maxTrace;

	double alpha=tree.get<double>("Triangulation.Alpha", 0.866);
	if(alpha<=0 && alpha>1)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters : Triangulation.Alpha must be (0,1]. Value=")+lexical_cast<string>(alpha)));

	//Input

	parameters.featureFilePattern=tree.get<string>("Input.Feature.Filename","");

	double spacing=tree.get<double>("Input.Feature.GridSpacing",defaultParameters.spacing);
	if(spacing<0)
		throw(invalid_argument(string("AppSparseModelBuilderImplementationC::SetParameters: FeatureSet1.Grid must be nonnegative. Value=")+lexical_cast<string>(spacing)));
	parameters.spacing=spacing;

	parameters.cameraFile=tree.get<string>("Input.CameraFile","");

	//Output

	parameters.outputPath=tree.get<string>("Output.Root","");

	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("AppSparseModelBuilderImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.logFile=tree.get<string>("Output.Log","");
	parameters.modelFile=tree.get<string>("Output.Model","");
	parameters.covarianceFile=tree.get<string>("Output.Covariances","");
	parameters.modelFeatureFile=tree.get<string>("Output.FeatureList","");
	parameters.flagVerbose=tree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);
	parameters.sparse3DParameters.flagVerbose=parameters.flagVerbose;
	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppSparseModelBuilderImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the cameras
	vector<CameraMatrixT> cameraList;
	vector<LensDistortionC> distortionList;
	tie(cameraList, distortionList)=LoadCameras(parameters.cameraFile);

	size_t nCamera=cameraList.size();
	cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<nCamera<<" cameras.\n";

	//Load the feature files
	vector<vector<ImageFeatureC> > featureStack=LoadFeatures(parameters.featureFilePattern, distortionList);
	cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Building the model...\n";

	//Build the model
	vector<Coordinate3DT> pointCloud;
	vector<MultiviewTriangulationC::covariance_matrix_type> covarianceList;
	vector<SceneFeatureC> sceneFeatures;

	ReciprocalC<EuclideanDistanceIDT::result_type> inverter;
	InverseEuclideanIDConverterT similarityMetric(EuclideanDistanceIDT(), inverter);

	Sparse3DReconstructionPipelineDiagnosticsC diagnostics=Sparse3DReconstructionPipelineC::Run(pointCloud, covarianceList, sceneFeatures, cameraList, featureStack, similarityMetric, parameters.sparse3DParameters);
	cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Found "<<pointCloud.size()<<" 3D features. Saving the results...\n";

	//Output
	double runtime=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;
	SaveLog(diagnostics, runtime);
	SaveOutput(pointCloud, covarianceList, sceneFeatures, distortionList);

	return true;
}	//bool Run()

}	//AppSparseModelBuilderN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("140218");
    std::string header("sparseModelBuilder: Sparse 3D model reconstruction from 2D images acquired by known cameras");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppSparseModelBuilderN::AppSparseModelBuilderImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )

