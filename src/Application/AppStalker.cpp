/**
 * @file AppStalker.cpp Implementation of Stalker, the image feature tracking application
 * @author Evren Imre
 * @date 18 May 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <omp.h>
#include "Application.h"
#include "../Tracker/ImageFeatureTrackingPipeline.h"
#include "../Tracker/ImageFeatureTrackingProblem.h"
#include "../Elements/Feature.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../IO/ImageFeatureIO.h"
#include "../IO/ImageFeatureTrajectoryIO.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/Homography2DSolver.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/BoostFilesystem.h"

namespace SeeSawN
{
namespace AppStalker22N
{

using namespace ApplicationN;
using boost::optional;
using boost::str;
using boost::format;
using boost::math::pow;
using boost::math::chi_squared;
using boost::math::cdf;
using boost::math::quantile;
using SeeSawN::TrackerN::ImageFeatureTrackingPipelineDiagnosticsC;
using SeeSawN::TrackerN::ImageFeatureTrackingPipelineParametersC;
using SeeSawN::TrackerN::ImageFeatureTrackingPipelineC;
using SeeSawN::TrackerN::ImageFeatureTrackingProblemC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::ImageFeatureTrajectoryT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::ImageFeatureTrajectoryIOC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::Homography2DSolverC;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::IsWriteable;

/**
 * @brief Parameter object for StalkerImplementationC
 * @ingroup Application
 */
struct StalkerParametersC
{
	unsigned int nThreads;	///< Number of threads available to the application

	double imageNoiseVariance;	///< Variance of the noise on the image coordinates. Measurement noise
	array<double,2> processNoise;	///< Uncertainty of the velocity. Process noise
	double initialUncertaintyFactor;	///< Scales the process noise

	double minDynamicMeasurementRatio;	///< Minimum ratio dynamic measurements to the total trajectory length for a dynamic track
	ImageFeatureTrackingPipelineParametersC pipelineParameters;	///< Parameters of the tracking pipeline

	//Input
	unsigned int firstFrame;	///< Index of the first frame
	unsigned int lastFrame;	///< Index of the last frame
	string featureFilePattern;	///< Filename pattern for the feature files
	LensDistortionC distortion;	///< Lens distoriton parameters
	optional<FrameT> boundingBox;	///< Bounding box

	//Output
	string outputPath;	///< Output path
	string logFile;		///< Filename for the log file
	string trajectoryFile;	///< Filename for the trajectory file
	bool flagVerbose;	///< If \c true , verbose output

	StalkerParametersC() : nThreads(1), imageNoiseVariance(4), processNoise{25,25}, initialUncertaintyFactor(10), minDynamicMeasurementRatio(0.5), firstFrame(0), lastFrame(0), flagVerbose(true)
	{}
};	//StalkerParametersC


/**
 * @brief Implementation of Stalker
 * @ingroup Application
 * @nosubgrouping
 */
class StalkerImplementationC
{
	private:

    	auto_ptr<options_description> commandLineOptions; ///< Command line options
                                                      	  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

    	/** @name Configuration */ //@{
    	StalkerParametersC parameters;  ///< Application parameters
    	//@}

    	/**@name State */ //@{
    	map<string, double> logger;	///< Logs various values
    	//@}

    	/** @name Implementation details */ ///@{
    	vector<ImageFeatureC> LoadFeatures(unsigned int index);	///< Loads the image features
    	void SaveLog(const vector<ImageFeatureTrajectoryT>& trajectoryList);	///< Saves the log

    	///@}
	public:

		/**@name ApplicationImplementationConceptC interface */ ///@{
		StalkerImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		///@}

};	//StalkerImplementationC

/**
 * @brief Loads the image features
 * @param[in] index Index
 * @return A vector of image features
 */
vector<ImageFeatureC> StalkerImplementationC::LoadFeatures(unsigned int index)
{
	string filename=str(format(parameters.featureFilePattern)%index);

	//Read the features
	vector<ImageFeatureC> featureList=ImageFeatureIOC::ReadFeatureFile(filename, true);

	//Bounding box
	optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
	if(!parameters.boundingBox)
		boundingBox=make_tuple( parameters.boundingBox->corner(FrameT::BottomLeft), parameters.boundingBox->corner(FrameT::TopRight));

	//Preprocess
	optional<CoordinateTransformations2DT::affine_transform_type>  dummy;
	ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, 0, true, parameters.distortion, dummy);	//Decimation might discard the features the tracker is looking for

	return featureList;
}	//vector<ImageFeatureC> LoadFeatures(unsigned int index)

/**
 * @brief Saves the log
 * @param[in] trajectoryList Trajectories
 */
void StalkerImplementationC::SaveLog(const vector<ImageFeatureTrajectoryT>& trajectoryList)
{
	if(parameters.logFile.empty())
		return;

	string filename = parameters.outputPath+parameters.logFile;
	ofstream logfile(filename);

	string timeString= to_simple_string(second_clock::universal_time());
	logfile<<"Stalker log file created on "<<timeString<<"\n";

	logfile<<"Number of frames: "<<parameters.lastFrame-parameters.firstFrame+1<<"\n";
	logfile<<"Number of tracks: "<<logger["NumTracks"]<<"\n";

	//Statistics
	size_t nTracks=trajectoryList.size();
	size_t nMeasurementAcc=0;
	size_t lengthAcc=0;

	for(auto const& current : trajectoryList)
	{
		nMeasurementAcc+=current.size();
		lengthAcc+=current.rbegin()->first - current.begin()->first;
	}	//for(auto const& current : trajectoryList)

	double meanMeasurementCount=(double)nMeasurementAcc/nTracks;
	double meanTrackLength=(double)lengthAcc/nTracks;	//Underestimate: does not take into account the consecutive miss segment that lead to the removal

	logfile<<"Average track length: "<<meanTrackLength<<"\n";
	logfile<<"Average measurement per track: "<<meanMeasurementCount<<"\n";

	logfile<<"Execution time: "<<logger["Runtime"]<<"s";
}	//void SaveLog(const ImageFeatureTrackingPipelineDiagnosticsC& diagnostics)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
StalkerImplementationC::StalkerImplementationC()
{
	StalkerParametersC defaultParameters;
	defaultParameters.pipelineParameters.homographyParameters.maxIteration=2;

	chi_squared chi2(4);
	double defaulMaxPerturbation=sqrt(quantile(chi2,defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio));


	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("Environment.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")
	("Environment.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.seed)), "Seed for the random number generator. If <0, rng default")

	("General.Noise", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.noiseVariance)), "Variance of the noise on the image coordinates, due to the detector. pixel^2. >0")
	("General.StaticCamera", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "If true, the camera is assumed to be static.")
	("General.TrackFiltering", value<string>()->default_value("All"), "Indicates which tracks are to be retained. Valid values: All, Static, Dynamic.")
	("General.LensDistortion.Type", value<string>()->default_value("NOD"), "Lens distortion type. Valid values: NOD, DIV, FP1, OP1")
	("General.LensDistortion.Centre", value<string>()->default_value(lexical_cast<string>(defaultParameters.distortion.centre[0])+string(" ")+ lexical_cast<string>(defaultParameters.distortion.centre[1]) ), "Centre of distortion.")
	("General.LensDistortion.Coefficient", value<string>()->default_value(lexical_cast<string>(defaultParameters.distortion.kappa)), "Lens distortion coefficient.")

	("Tracker.ProcessNoise", value<string>()->default_value( lexical_cast<string>(defaultParameters.processNoise[0])+string(" ")+lexical_cast<string>(defaultParameters.processNoise[1])), "Frame-to-frame variation of the velocity. Horizontal and vertical. pixel^2. The steady-state search region radius is approximately sqrt( (process noise + image noise) )*mahalonobis) . >0")
	("Tracker.InitialUncertaintyFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.initialUncertaintyFactor)), "Multiplier to the process noise to determine the velocity uncertainty of a track at initiation. Should be large in the case of fast motion.>0")
	("Tracker.MinNeighbourDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.minNeighbourDistance)), "Minimum distance between a new track and its closest neighbour. pixel. >=0")
	("Tracker.MaxMissRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.maxMissRatio)), "Maximum ratio of misses to the track length. [0,1]")
	("Tracker.MaxConsecutiveMiss", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.maxConsecutiveMiss)), "Maximum consecutive misses. >0")
	("Tracker.GracePeriod", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.gracePeriod)), "A track cannot be retired during the grace period >=0")
	("Tracker.MinLength", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.minLength)), "Tracks shorter than this value are not stored upon retirement. >=0")
	("Tracker.StaticDescriptor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.flagStaticDescriptor)), "If true, the descriptor for the track is that of the first measurement. Less chance of drift, but fewer and shorter tracks. If false, the descriptor for the track is that of the last measurement.")
	("Tracker.MaxMahalanobisDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.trackerParameters.maxMahalanobisDistance)), "Maximum Mahalonobis distance for a track-measurement association. >0")
	("Tracker.MinDynamicMeasurementRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.minDynamicMeasurementRatio)), "Minimum ratio of measurements generated by moving objects to the total number of measurements in a dynamic track. Has no effect if track filtering is inactive. [0,1]")

	("Tracker.Matcher.FeatureMatcher.MaxRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold) ), "Maximum ratio of the similarity score of the best outsider to that of the best insider. See the documentation of NearestNeighbourMatcherC. [0,1]")
	("Tracker.Matcher.FeatureMatcher.MaxFeatureDistance", value<string>()->default_value("0.5"), "Maximum distance between the normalised descriptor vectors of two corresponding points, as a percentage of the maximum possible distance.[0 1]")

	("TrackFilter.HomographyPrediction.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.flagPrediction)), "If true, the homography estimation is initialised by the estimate for the previous frame. Accelerates the procedure when the camera has periods of steady, predictable motion.")
	("TrackFilter.HomographyPrediction.PredictionErrorNoise", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.predictionErrorNoise)), "Variance of the additive coordinate noise to compensate for the homography prediction error, in pixel^2. >=0")
	("TrackFilter.HomographyPrediction.PredictionFailureThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.predictionFailureTh)), "If the ratio of inlier counts between two successive frame pairs drops below this value, the homography is recalculated without any initial estimate. [0,1]")

	("TrackFilter.GuidedMatching.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.maxIteration)), "Maximum number of iterations. >0")
	("TrackFilter.GuidedMatching.MinRelativeDifference", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.minRelativeDifference)), "If the relative difference between the fitness scores of two successive iterations is below this value, terminate. >0")
	("TrackFilter.GuidedMatching.BinDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.binDensity1)), "Number of feature bins per standard deviation. Used in spatial quantisation. >0")
	("TrackFilter.GuidedMatching.pValue", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.pRejection)), "Probability of rejection for an inlier. Determines the inlier threshold. [0,1]")
	("TrackFilter.GuidedMatching.MaxPerturbation", value<string>()->default_value(lexical_cast<string>(defaulMaxPerturbation)), "Maximum Euclidean distance between the true and the measured coordinates of a generator. Determines the proportion of eligible inliers for RANSAC, i.e. number of iterations. A scale factor to the noise standard deviation. >0")

	("TrackFilter.GuidedMatching.Decimation.MaxObservationDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.maxObservationDensity)), "Number of features per bin. Determines the number of observations. >0")
	("TrackFilter.GuidedMatching.Decimation.MinObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.minObservationRatio)), "Minimum ratio of the number of observations to the size of the generator. Determines the minimum size of the observation set. >1")
	("TrackFilter.GuidedMatching.Decimation.ValidationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.validationRatio)), "Ratio of the number of validators to the size of the generator. >1")
	("TrackFilter.GuidedMatching.Decimation.MaxAmbiguity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.maxAmbiguity)), "Maximum value of Matcher.FeatureMatcher.MaxRatio in a decimated set. Used only in the first pass, if there is no initial estimate. [0,1]")

	("TrackFilter.Matcher.FeatureMatcher.MaxRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the similarity score of the best outsider to that of the best insider. See the documentation of NearestNeighbourMatcherC. [0,1]")
	("TrackFilter.Matcher.FeatureMatcher.MaxFeatureDistance", value<string>()->default_value("0.5"), "Maximum distance between the normalised descriptor vectors of two corresponding points, as a percentage of the maximum possible distance.[0 1]")

	("TrackFilter.Matcher.BucketFilter.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.flagBucketFilter)), "If true, the bucket filter is enabled. Useful for eliminating outliers. 0 or 1")
	("TrackFilter.Matcher.BucketFilter.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity between two buckets for a valid bucket match. [0,1]")
	("TrackFilter.Matcher.BucketFilter.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.minQuorum)), "If no buckets in a pair has this many votes, the pair is exempted due to insufficient evidence for rejection (i.e., automatic success).>=0")
	("TrackFilter.Matcher.BucketFilter.BucketDensity", value<string>()->default_value(lexical_cast<string>(1.0/defaultParameters.pipelineParameters.matcherParameters.bucketDimensions[0])), "Number of buckets, per unit standard deviation. >0")

	("TrackFilter.GeometryEstimator.TSRANSAC.Confidence", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.minConfidence)), "Probability that the solution estimated by RANSAC is 'good'. Affects the number of iterations. [0,1]")

	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration)), "Maximum number of iterations. >0")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations)), "Number of LO iterations. >=0")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport)), "If the ratio of the number of inlier observations to the size of the generator set is below this value, LO-RANSAC does not trigger. Prevents LO-RANSAC from triggering at poor hypotheses. >=1")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.loGeneratorSize1/Homography2DSolverC::GeneratorSize())), "Ratio of the cardinality of the LO generator to that of the minimal generator. >=1 ")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate)), "Rate of expansion of the observation set, as a percentage of the total number of observations. Any value >1 disables PROSAC. >0")

	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations)), "Number of LO iterations. >=0")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport)), "If the ratio of the number of inlier observations to the size of the generator set is below this value, LO-RANSAC does not trigger. Prevents LO-RANSAC from triggering at poor hypotheses. >=1")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.loGeneratorSize2/Homography2DSolverC::GeneratorSize())), "Ratio of the cardinality of the LO generator to that of the minimal generator. >=1 ")
	("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate)), "Rate of expansion of the observation set, as a percentage of the total number of observations. Any value >1 disables PROSAC. >0")

	("TrackFilter.GeometryEstimator.Optimisation.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.pdlParameters.maxIteration)), "Maximum number of nonlinear least squares iterations. >=0")

	("Input.FirstFrame", value<string>(), "Index of the first frame.")
	("Input.LastFrame", value<string>(), "Index of the last frame.")
	("Input.FilenamePattern", value<string>(), "Pattern for the filenames.")
	("Input.RoI", value<string>(), "Region-of-interest in pixels. If not specified, bounding box of the features. [left upper right lower]")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Trajectories", value<string>(), "Feature trajectories, as a .trj file")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output")
	;
}	//StalkerImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& StalkerImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void StalkerImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	StalkerParametersC defaultParameters;
	defaultParameters.pipelineParameters.homographyParameters.maxIteration=2;

	chi_squared chi2(4);
	double defaulMaxPerturbation=sqrt(quantile(chi2, defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio));

	ptree tree; //Options tree

	tree.put("xmlcomment", "stalker configuration file");

	tree.put("Environment","");
	tree.put("Environment.NoThreads", defaultParameters.nThreads);
	tree.put("Environment.Seed", defaultParameters.pipelineParameters.seed);

	tree.put("General","");
	tree.put("General.LensDistortion","");
    tree.put("General.Noise", defaultParameters.pipelineParameters.noiseVariance);
    tree.put("General.StaticCamera", defaultParameters.pipelineParameters.flagStaticCamera);
    tree.put("General.TrackFiltering","All");

	tree.put("Tracker", "");
	tree.put("Tracker.ProcessNoise", lexical_cast<string>(defaultParameters.processNoise[0]) + string(" ") + lexical_cast<string>(defaultParameters.processNoise[1]) );
	tree.put("Tracker.InitialUncertaintyFactor", defaultParameters.initialUncertaintyFactor);
	tree.put("Tracker.MinNeighbourDistance", defaultParameters.pipelineParameters.trackerParameters.minNeighbourDistance);
	tree.put("Tracker.MaxMissRatio", defaultParameters.pipelineParameters.trackerParameters.maxMissRatio);
	tree.put("Tracker.MaxConsecutiveMiss",defaultParameters.pipelineParameters.trackerParameters.maxConsecutiveMiss);
	tree.put("Tracker.GracePeriod",defaultParameters.pipelineParameters.trackerParameters.gracePeriod);
	tree.put("Tracker.MinLength",defaultParameters.pipelineParameters.trackerParameters.minLength);
	tree.put("Tracker.StaticDescriptor",defaultParameters.pipelineParameters.trackerParameters.flagStaticDescriptor);
	tree.put("Tracker.MaxMahalanobisDistance",defaultParameters.pipelineParameters.trackerParameters.maxMahalanobisDistance);

	tree.put("Tracker.Matcher","");
	tree.put("Tracker.Matcher.FeatureMatcher","");

	tree.put("TrackFilter","");

	tree.put("TrackFilter.HomographyPrediction", "");
	tree.put("TrackFilter.HomographyPrediction.Enable", defaultParameters.pipelineParameters.flagPrediction);
	tree.put("TrackFilter.HomographyPrediction.PredictionErrorNoise", defaultParameters.pipelineParameters.predictionErrorNoise);

	tree.put("TrackFilter.GuidedMatching","");
	tree.put("TrackFilter.GuidedMatching.MaxIteration", defaultParameters.pipelineParameters.homographyParameters.maxIteration);

	tree.put("TrackFilter.Matcher","");

	tree.put("TrackFilter.Matcher.FeatureMatcher", "");

	tree.put("TrackFilter.Matcher.BucketFilter","");

	tree.put("TrackFilter.GeometryEstimator", "");

	tree.put("TrackFilter.GeometryEstimator.TSRANSAC", "");

	tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1","");
	tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration);

	tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC","");


	tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2","");

	tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC","");

	tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC","");

	tree.put("TrackFilter.GeometryEstimator.Optimisation", "");

	tree.put("Input","");
	tree.put("Input.FirstFrame", defaultParameters.firstFrame);
	tree.put("Input.LastFrame", defaultParameters.lastFrame);
	tree.put("Input.FilenamePattern","%03d.ft2");

	tree.put("Output","");
	tree.put("Output.Root","/");
	tree.put("Output.Trajectories","trajectories.trj");
	tree.put("Output.Log","stalker.log");
	tree.put("Output.Verbose", defaultParameters.flagVerbose);

	if(detailLevel>1)
	{

		tree.put("General.LensDistortion.Type", "NOD");
		tree.put("General.LensDistortion.Centre", lexical_cast<string>(defaultParameters.distortion.centre[0])+string(" ")+lexical_cast<string>(defaultParameters.distortion.centre[1]) );
		tree.put("General.LensDistortion.Coefficient", defaultParameters.distortion.kappa);

		tree.put("Tracker.MinDynamicMeasurementRatio",  defaultParameters.minDynamicMeasurementRatio);

		tree.put("Tracker.Matcher.FeatureMatcher.MaxRatio", defaultParameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold);
		tree.put("Tracker.Matcher.FeatureMatcher.MaxFeatureDistance", "0.5");

		tree.put("TrackFilter.HomographyPrediction.PredictionFailureThreshold", defaultParameters.pipelineParameters.predictionFailureTh);

		tree.put("TrackFilter.GuidedMatching.MinRelativeDifference",defaultParameters.pipelineParameters.homographyParameters.minRelativeDifference);
		tree.put("TrackFilter.GuidedMatching.BinDensity", defaultParameters.pipelineParameters.homographyParameters.binDensity1);
		tree.put("TrackFilter.GuidedMatching.pValue", defaultParameters.pipelineParameters.pRejection);
		tree.put("TrackFilter.GuidedMatching.MaxPerturbation", defaulMaxPerturbation);

		tree.put("TrackFilter.GuidedMatching.Decimation","");
		tree.put("TrackFilter.GuidedMatching.Decimation.MaxObservationDensity",defaultParameters.pipelineParameters.homographyParameters.maxObservationDensity);
		tree.put("TrackFilter.GuidedMatching.Decimation.MinObservationRatio",defaultParameters.pipelineParameters.homographyParameters.minObservationRatio);
		tree.put("TrackFilter.GuidedMatching.Decimation.ValidationRatio",defaultParameters.pipelineParameters.homographyParameters.validationRatio);
		tree.put("TrackFilter.GuidedMatching.Decimation.MaxAmbiguity",defaultParameters.pipelineParameters.homographyParameters.maxAmbiguity);

		tree.put("TrackFilter.Matcher.FeatureMatcher.MaxRatio", defaultParameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold);
		tree.put("TrackFilter.Matcher.FeatureMatcher.MaxFeatureDistance", "0.5");

		tree.put("TrackFilter.Matcher.BucketFilter.Enable", defaultParameters.pipelineParameters.matcherParameters.flagBucketFilter);
		tree.put("TrackFilter.Matcher.BucketFilter.MinSimilarity", defaultParameters.pipelineParameters.matcherParameters.bucketSimilarityThreshold);
		tree.put("TrackFilter.Matcher.BucketFilter.MinQuorum", defaultParameters.pipelineParameters.matcherParameters.minQuorum);
		tree.put("TrackFilter.Matcher.BucketFilter.BucketDensity", 1.0/defaultParameters.pipelineParameters.matcherParameters.bucketDimensions[0]);

		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.Confidence", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.minConfidence);

		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate",defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate);

		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations",defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations);
		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio",defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport);
		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio",defaultParameters.pipelineParameters.loGeneratorSize1/Homography2DSolverC::GeneratorSize());

		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations",defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations);
		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio",defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport);
		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio",defaultParameters.pipelineParameters.loGeneratorSize2/Homography2DSolverC::GeneratorSize());

		tree.put("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate);

		tree.put("TrackFilter.GeometryEstimator.Optimisation.MaxIteration", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.pdlParameters.maxIteration);

		tree.put("Input.RoI","0 0 1919 1079");
	}	//if(!flagBasic)

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);

}	//void GenerateConfigFile(const string& filename)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool StalkerImplementationC::SetParameters(const ptree& tree)
{
	StalkerParametersC defaultParameters;
	defaultParameters.pipelineParameters.homographyParameters.maxIteration=2;

	//Environment

	unsigned int nThreads=min(omp_get_max_threads(), max(1, tree.get<int>("Environment.NoThreads", defaultParameters.nThreads)));
	parameters.nThreads=nThreads;
	parameters.pipelineParameters.nThreads=nThreads;

	int seed=tree.get<int>("Environment.Seed", defaultParameters.pipelineParameters.seed);
	parameters.pipelineParameters.seed=seed;

	//General

	double noise=tree.get<double>("General.Noise", defaultParameters.pipelineParameters.noiseVariance);
	if(noise<=0)
		throw(invalid_argument(string("StalkerImplementationC::SetParameters: General.Noise must be a positive value. Value=")+lexical_cast<string>(noise) ) );

	parameters.pipelineParameters.noiseVariance=noise;
	parameters.imageNoiseVariance=noise;

	string filterType=tree.get<string>("General.TrackFiltering","All");
	map<string, unsigned int> validFilterTypes={ {"All",3}, {"Dynamic",2}, {"Static", 1}};
	auto itFT=validFilterTypes.find(filterType);
	if(itFT==validFilterTypes.end())
		throw(invalid_argument("StalkerImplementationC::SetParameters: Invalid track filter type. Value="+filterType));
	else
	{
		parameters.pipelineParameters.flagTrackStatic= (itFT->second==1 || itFT->second==3);
		parameters.pipelineParameters.flagTrackDynamic= (itFT->second==2 || itFT->second==3);;
	}	//if(itFT==validFilterTypes.end())

	bool flagStatic=tree.get<bool>("General.StaticCamera",defaultParameters.pipelineParameters.flagStaticCamera);
	parameters.pipelineParameters.flagStaticCamera=flagStatic;

	//General.LensDistoriton

	string distortionType=tree.get<string>("General.LensDistortion.Type","NOD");
	map<string, LensDistortionCodeT> validDistortionTypes={{"NOD", LensDistortionCodeT::NOD}, {"FP1", LensDistortionCodeT::FP1}, {"OP1", LensDistortionCodeT::OP1}, {"DIV", LensDistortionCodeT::DIV}};
	auto itDT=validDistortionTypes.find(distortionType);
	if(itDT==validDistortionTypes.end())
		throw(invalid_argument("StalkerImplementationC::SetParameters: Invalid lens distortion type. Value="+distortionType));

	string sDC=tree.get<string>("General.LensDistortion.Centre", lexical_cast<string>(defaultParameters.distortion.centre[0]) + string(" ") + lexical_cast<string>(defaultParameters.distortion.centre[1]) );
	vector<double> distortionCentre=Tokenise<double>(sDC, " ");
	if(distortionCentre.size()!=2)
		throw(invalid_argument("StalkerImplementationC::SetParameters: General.LensDistortion.Centre must have two elements. Value="+sDC));

	double distortionCoefficient=tree.get<double>("General.LensDistortion.Coefficient", defaultParameters.distortion.kappa);

	parameters.distortion.modelCode=itDT->second;
	parameters.distortion.centre=Coordinate2DT(distortionCentre[0], distortionCentre[1]);
	parameters.distortion.kappa=distortionCoefficient;


	//Tracker

	string sPN=tree.get<string>("Tracker.ProcessNoise", lexical_cast<string>(defaultParameters.processNoise[0]) + string(" ") + lexical_cast<string>(defaultParameters.processNoise[1]) );
	vector<double> processNoise=Tokenise<double>(sPN, " ");
	if(processNoise.size()!=2)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.ProcessNoise must have two elements. Value="+sPN));

	if(processNoise[0]<=0 || processNoise[1]<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.ProcessNoise must have positive elements. Value="+sPN));

	parameters.processNoise[0]=processNoise[0];
	parameters.processNoise[1]=processNoise[1];

	double initialUncertaintyFactor=tree.get<double>("Tracker.InitialUncertaintyFactor", defaultParameters.initialUncertaintyFactor);
	if(initialUncertaintyFactor<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.InitialUncertaintyFactor must be positive. Value="+lexical_cast<string>(initialUncertaintyFactor)));

	parameters.initialUncertaintyFactor=initialUncertaintyFactor;

	double minNeighbourDistance=tree.get<double>("Tracker.MinNeighbourDistance", defaultParameters.pipelineParameters.trackerParameters.minNeighbourDistance);
	if(minNeighbourDistance<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.MinNeighbourDistance must be positive. Value="+lexical_cast<string>(minNeighbourDistance)));

	parameters.pipelineParameters.trackerParameters.minNeighbourDistance=minNeighbourDistance;

	double maxMissRatio=tree.get<double>("Tracker.MaxMissRatio", defaultParameters.pipelineParameters.trackerParameters.maxMissRatio);
	if(maxMissRatio<0 || maxMissRatio>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.MaxMissRatio must be in [0,1]. Value="+lexical_cast<string>(maxMissRatio)));

	parameters.pipelineParameters.trackerParameters.maxMissRatio=maxMissRatio;

	double maxConsecutiveMiss=tree.get<double>("Tracker.MaxConsecutiveMiss", defaultParameters.pipelineParameters.trackerParameters.maxConsecutiveMiss);
	if(maxConsecutiveMiss<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.MaxConsecutiveMiss must be positive. Value="+lexical_cast<string>(maxConsecutiveMiss)));

	parameters.pipelineParameters.trackerParameters.maxConsecutiveMiss=maxConsecutiveMiss;

	unsigned int gracePeriod=tree.get<unsigned int>("Tracker.GracePeriod", defaultParameters.pipelineParameters.trackerParameters.gracePeriod);
	parameters.pipelineParameters.trackerParameters.gracePeriod=gracePeriod;

	unsigned int minTrackLength=tree.get<unsigned int>("Tracker.MinLength", defaultParameters.pipelineParameters.trackerParameters.minLength);
	parameters.pipelineParameters.trackerParameters.minLength=minTrackLength;

	bool flagStaticDescriptor=tree.get<bool>("Tracker.StaticDescriptor", defaultParameters.pipelineParameters.trackerParameters.flagStaticDescriptor);
	parameters.pipelineParameters.trackerParameters.flagStaticDescriptor=flagStaticDescriptor;

	double maxMahalanobisDistance=tree.get<double>("Tracker.MaxMahalanobisDistance", defaultParameters.pipelineParameters.trackerParameters.maxMahalanobisDistance);
	if(maxMahalanobisDistance<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.MaxMahalanobisDistance must be positive. Value="+lexical_cast<string>(maxMahalanobisDistance)));

	parameters.pipelineParameters.trackerParameters.maxMahalanobisDistance=maxMahalanobisDistance;

	double minDynamicMeasurementRatio=tree.get<double>("Tracker.MinDynamicMeasurementRatio", defaultParameters.minDynamicMeasurementRatio);
	if(minDynamicMeasurementRatio<0 || minDynamicMeasurementRatio>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.MinDynamicMeasurementRatio must be in [0,1]. Value="+lexical_cast<string>(minDynamicMeasurementRatio)));

	parameters.minDynamicMeasurementRatio=minDynamicMeasurementRatio;

	//Tracker.Matcher

	double trackerMaxRatio=tree.get<double>("Tracker.Matcher.FeatureMatcher.MaxRatio", defaultParameters.pipelineParameters.trackerParameters.matcherParameters.nnParameters.ratioTestThreshold);
	if(trackerMaxRatio<0 || trackerMaxRatio>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.Matcher.FeatureMatcher.MaxRatio must be in [0,1]. Value="+lexical_cast<string>(trackerMaxRatio)));

	parameters.pipelineParameters.trackerParameters.matcherParameters.nnParameters.ratioTestThreshold=trackerMaxRatio;

	double trackerMaxFeatureDistance=tree.get<double>("Tracker.Matcher.FeatureMatcher.MaxFeatureDistance", 0.5);
	if(trackerMaxFeatureDistance<0 || trackerMaxFeatureDistance>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: Tracker.Matcher.FeatureMatcher.MaxFeatureDistance must be in [0,1]. Value="+lexical_cast<string>(trackerMaxFeatureDistance)));

	parameters.pipelineParameters.trackerParameters.matcherParameters.nnParameters.similarityTestThreshold=trackerMaxFeatureDistance*sqrt(2);	//The application normalises the descriptors upon loading. So, the max distance is sqrt(2)

	//TrackFilter

	//TrackFilter.HomographyPrediction

	parameters.pipelineParameters.flagPrediction=tree.get<bool>("TrackFilter.HomographyPrediction.Enable", defaultParameters.pipelineParameters.flagPrediction);

	double predictionErrorNoise=tree.get<double>("TrackFilter.HomographyPrediction.PredictionErrorNoise", defaultParameters.pipelineParameters.predictionErrorNoise);
	if(predictionErrorNoise<0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.HomographyPrediction.PredictionErrorNoise must be non-negative. Value="+lexical_cast<string>(predictionErrorNoise)));

	parameters.pipelineParameters.predictionErrorNoise=predictionErrorNoise;

	double predictionFailureThreshold=tree.get<double>("TrackFilter.HomographyPrediction.PredictionFailureThreshold", defaultParameters.pipelineParameters.predictionFailureTh);
	if(predictionFailureThreshold<0 || predictionFailureThreshold>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.HomographyPrediction.PredictionFailureThreshold must be [0,1]. Value="+lexical_cast<string>(predictionFailureThreshold)));

	parameters.pipelineParameters.predictionFailureTh=predictionFailureThreshold;

	//TrackFilter.GuidedMatching

	unsigned int gmMaxIteration=tree.get<unsigned int>("TrackFilter.GuidedMatching.MaxIteration", defaultParameters.pipelineParameters.homographyParameters.maxIteration);
	parameters.pipelineParameters.homographyParameters.maxIteration=gmMaxIteration;

	double gmMinRelativeDifference=tree.get<double>("TrackFilter.GuidedMatching.MinRelativeDifference", defaultParameters.pipelineParameters.homographyParameters.minRelativeDifference);
	if(gmMinRelativeDifference<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.MinRelativeDifference must be positive. Value="+lexical_cast<string>(gmMinRelativeDifference)) );

	parameters.pipelineParameters.homographyParameters.minRelativeDifference=gmMinRelativeDifference;

	double gmBinDensity=tree.get<double>("TrackFilter.GuidedMatching.BinDensity", defaultParameters.pipelineParameters.homographyParameters.binDensity1);
	if(gmBinDensity<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.BinDensity must be positive. Value="+lexical_cast<string>(gmBinDensity)) );

	parameters.pipelineParameters.homographyParameters.binDensity1=gmBinDensity;
	parameters.pipelineParameters.homographyParameters.binDensity2=gmBinDensity;

	double pValue=tree.get<double>("TrackFilter.GuidedMatching.pValue", defaultParameters.pipelineParameters.pRejection);
	if(pValue<0 || pValue>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.pValue must be in [0,1]. Value="+lexical_cast<string>(pValue)) );

	parameters.pipelineParameters.pRejection=pValue;

	chi_squared chi2(4);
	double defaulMaxPerturbation=sqrt(quantile(chi2,defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio));

	double gmMaxPerturbation=tree.get<double>("TrackFilter.GuidedMatching.MaxPerturbation", defaulMaxPerturbation);
	if(gmMaxPerturbation<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.MaxPerturbation must be positive. Value="+lexical_cast<string>(gmMaxPerturbation)));

	double eligibilityRatio=cdf(chi2, pow<2>(gmMaxPerturbation));	//maxPerturbation is a scale factor!

	//TrackFilter.GuidedMatching.Decimation

	double maxObservationDensity=tree.get<double>("TrackFilter.GuidedMatching.Decimation.MaxObservationDensity", defaultParameters.pipelineParameters.homographyParameters.maxObservationDensity);
	if(maxObservationDensity<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.Decimation.MaxObservationDensity must be positive. Value="+lexical_cast<string>(maxObservationDensity)) );

	parameters.pipelineParameters.homographyParameters.maxObservationDensity=maxObservationDensity;

	double minObservationRatio=tree.get<double>("TrackFilter.GuidedMatching.Decimation.MinObservationRatio", defaultParameters.pipelineParameters.homographyParameters.minObservationRatio);
	if(minObservationRatio<=1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.Decimation.MinObservationRatio must be >1. Value="+lexical_cast<string>(minObservationRatio)) );

	parameters.pipelineParameters.homographyParameters.minObservationRatio=minObservationRatio;

	double validationRatio=tree.get<double>("TrackFilter.GuidedMatching.Decimation.ValidationRatio", defaultParameters.pipelineParameters.homographyParameters.validationRatio);
	if(validationRatio<=1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.Decimation.ValidationRatio must be >1. Value="+lexical_cast<string>(validationRatio)) );

	parameters.pipelineParameters.homographyParameters.validationRatio=validationRatio;

	double maxAmbiguity=tree.get<double>("TrackFilter.GuidedMatching.Decimation.MaxAmbiguity", defaultParameters.pipelineParameters.homographyParameters.maxAmbiguity);
	if(maxAmbiguity<0 || maxAmbiguity>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GuidedMatching.Decimation.MaxAmbiguity must be in [0,1]. Value="+lexical_cast<string>(maxAmbiguity)) );

	parameters.pipelineParameters.homographyParameters.maxAmbiguity=maxAmbiguity;

	parameters.pipelineParameters.homographyParameters.maxObservationRatio=pow<2>(2*3*gmBinDensity)*maxObservationDensity;	//Approximate number of bins x density. Functionally, infinite

	//TrackFilter.Matcher.FeatureMatcher

	double filterMaxRatio=tree.get<double>("TrackFilter.Matcher.FeatureMatcher.MaxRatio", defaultParameters.pipelineParameters.homographyParameters.matcherParameters.nnParameters.ratioTestThreshold);
	if(filterMaxRatio<0 || filterMaxRatio>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.Matcher.FeatureMatcher.MaxRatio must be in [0,1]. Value="+lexical_cast<string>(filterMaxRatio)));

	parameters.pipelineParameters.homographyParameters.matcherParameters.nnParameters.ratioTestThreshold=filterMaxRatio;

	double filterMaxFeatureDistance=tree.get<double>("TrackFilter.Matcher.FeatureMatcher.MaxFeatureDistance", 0.5);
	if(filterMaxFeatureDistance<0 || filterMaxFeatureDistance>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.Matcher.FeatureMatcher.MaxFeatureDistance must be in [0,1]. Value="+lexical_cast<string>(filterMaxFeatureDistance)));

	parameters.pipelineParameters.homographyParameters.matcherParameters.nnParameters.similarityTestThreshold=filterMaxFeatureDistance*sqrt(2);	//The application normalises the descriptors upon loading. So, the max distance is sqrt(2)

	//TrackFilter.Matcher.BucketFilter

	bool flagBucketFilter=tree.get<bool>("TrackFilter.Matcher.BucketFilter.Enable", defaultParameters.pipelineParameters.homographyParameters.matcherParameters.flagBucketFilter);
	parameters.pipelineParameters.homographyParameters.matcherParameters.flagBucketFilter=flagBucketFilter;

	if(flagBucketFilter)
	{
		double bfMinSimilarity=tree.get<double>("TrackFilter.Matcher.BucketFilter.MinSimilarity", defaultParameters.pipelineParameters.homographyParameters.matcherParameters.bucketSimilarityThreshold);
		if(bfMinSimilarity<0 || bfMinSimilarity>1)
			throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.Matcher.BucketFilter.MinSimilarity must be in [0,1]. Value="+lexical_cast<string>(bfMinSimilarity)));

		parameters.pipelineParameters.homographyParameters.matcherParameters.bucketSimilarityThreshold=bfMinSimilarity;

		unsigned int minQuorum=tree.get<unsigned int>("TrackFilter.Matcher.BucketFilter.MinQuorum", defaultParameters.pipelineParameters.homographyParameters.matcherParameters.minQuorum);
		parameters.pipelineParameters.homographyParameters.matcherParameters.minQuorum=minQuorum;

		double bucketDensity=tree.get<double>("TrackFilter.Matcher.BucketFilter.BucketDensity", 1.0/defaultParameters.pipelineParameters.homographyParameters.matcherParameters.bucketDimensions[0]);
		if(bucketDensity<=0)
			throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.Matcher.BucketFilter.BucketDensity must be positive. Value="+lexical_cast<string>(bucketDensity)));

		parameters.pipelineParameters.homographyParameters.matcherParameters.bucketDimensions.fill(1.0/bucketDensity);
	}	//if(flagBucketFilter)

	//TrackFilter.GeometryEstimator.TSRANSAC

	double confidence=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.Confidence", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.minConfidence);
	if(confidence<0 || confidence>1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.Confidence must be in [0,1]. Value="+lexical_cast<string>(confidence)));

	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.minConfidence=confidence;
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=eligibilityRatio;

	//TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1

	unsigned int rs1MaxIteration=tree.get<unsigned int>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration);
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration=rs1MaxIteration;

	unsigned int loNoIterations1=tree.get<unsigned int>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.nLOIterations);
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.nLOIterations=loNoIterations1;

	double rs1MinObservationSupport=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.minObservationSupport);
	if(rs1MinObservationSupport<1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio must be >1 Value="+lexical_cast<string>(rs1MinObservationSupport)));

	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.minObservationSupport=rs1MinObservationSupport;

	double rs1GeneratorRatio=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio", defaultParameters.pipelineParameters.loGeneratorSize1/Homography2DSolverC::GeneratorSize());
	if(rs1GeneratorRatio<1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio must be >1. Value="+lexical_cast<string>(rs1GeneratorRatio)));

	parameters.pipelineParameters.loGeneratorSize1=rs1GeneratorRatio * Homography2DSolverC::GeneratorSize();

	double rs1GrowthRate=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate);
	if(rs1GrowthRate<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate must be a positive value. Value="+lexical_cast<string>(rs1GrowthRate)));


	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagPROSAC=rs1GrowthRate<=1;
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate=rs1GrowthRate;

	//TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2

	double rs2GrowthRate=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate);
	if(rs2GrowthRate<=0)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate must be a positive value. Value="+lexical_cast<string>(rs2GrowthRate)));

	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagPROSAC=rs2GrowthRate<=1;
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate=rs2GrowthRate;

	unsigned int loNoIterations2=tree.get<unsigned int>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations);
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations=loNoIterations2;

	double rs2MinObservationSupport=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport);
	if(rs2MinObservationSupport<1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio must be >1 Value="+lexical_cast<string>(rs2MinObservationSupport)));

	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport=rs2MinObservationSupport;

	double rs2GeneratorRatio=tree.get<double>("TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio", defaultParameters.pipelineParameters.loGeneratorSize2/Homography2DSolverC::GeneratorSize());
	if(rs2GeneratorRatio<1)
		throw(invalid_argument("StalkerImplementationC::SetParameters: TrackFilter.GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio must be >1. Value="+lexical_cast<string>(rs2GeneratorRatio)));

	parameters.pipelineParameters.loGeneratorSize2=rs2GeneratorRatio * Homography2DSolverC::GeneratorSize();

	//TrackFilter.GeometryEstimator.Optimisation
	unsigned int pdlMaxIteration=tree.get<unsigned int>("TrackFilter.GeometryEstimator.Optimisation.MaxIteration", defaultParameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.pdlParameters.maxIteration);
	parameters.pipelineParameters.homographyParameters.geometryEstimatorParameters.pdlParameters.maxIteration=pdlMaxIteration;

	//Input

	parameters.firstFrame=tree.get<unsigned int>("Input.FirstFrame");
	parameters.lastFrame=tree.get<unsigned int>("Input.LastFrame");
	parameters.featureFilePattern=tree.get<string>("Input.FilenamePattern");

	optional<string> roi=tree.get_optional<string>("Input.RoI");
	char_separator<char> separator(" ");
	if(roi)
	{
		vector<ValueTypeM<Coordinate2DT>::type> tokens=Tokenise<ValueTypeM<Coordinate2DT>::type>(*roi, string(" "));

		if(tokens.size()==4)
			parameters.boundingBox=FrameT(Coordinate2DT(tokens[0], tokens[1]), Coordinate2DT(tokens[2], tokens[3]));
	}	//if(roi)

	//Output

	bool flagVerbose=tree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);
	parameters.flagVerbose=flagVerbose;

	parameters.outputPath=tree.get<string>("Output.Root","");

	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("StalkerImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.logFile=tree.get<string>("Output.Log","");

	parameters.trajectoryFile=tree.get<string>("Output.Trajectories");

	return true;
}	//bool SetParameters(const ptree& tree)


/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool StalkerImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Initialising the tracker...\n";
	}	//if(parameters.flagVerbose)

	//Initialise the tracker

	typedef ImageFeatureTrackingProblemC::kalman_problem_type::state_covariance_type StateCovarianceT;
	StateCovarianceT mQ=StateCovarianceT::Zero(); mQ(2,2)=parameters.processNoise[0]; mQ(3,3)=parameters.processNoise[1];
	StateCovarianceT mQi=parameters.initialUncertaintyFactor*mQ;

	typedef ImageFeatureTrackingProblemC::kalman_problem_type::innovation_covariance_type MeasurementCovarianceT;
	MeasurementCovarianceT mR=parameters.imageNoiseVariance * MeasurementCovarianceT::Identity();

	ImageFeatureTrackingProblemC trackingProblem(mQ, mQi, mR);

	ImageFeatureTrackingPipelineC tracker(parameters.pipelineParameters);

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Tracker initialised. Initiating the tracking loop...\n";

	//Tracking loop
	for(size_t c=parameters.firstFrame; c<=parameters.lastFrame; ++c)
	{
		//TODO Is it possible to load the next feature set, while processing the current one?
		vector<ImageFeatureC> featureList=LoadFeatures(c);	//Load the features

		ImageFeatureTrackingPipelineDiagnosticsC diagnostics=tracker.Update(trackingProblem, featureList, c);	//Feed to the tracker

		if(parameters.flagVerbose)
		{
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Frame "<<c<<" processed.";
			cout<< " #Active: "<<diagnostics.trackerDiagnostics.nActiveTracks<<" #Inactive:"<<diagnostics.trackerDiagnostics.nInactiveTracks<<"\n";
		}	//if(parameters.flagVerbose)
	}	//for(size_t c=0; c<parameters.nFrames; ++c)

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Saving the output...\n";

	//Get the trajectories

	unsigned int minLength=parameters.pipelineParameters.trackerParameters.minLength;
	double maxMissRatio=parameters.pipelineParameters.trackerParameters.maxMissRatio + (double)1/minLength;
	unsigned int minMeasurement=(1-maxMissRatio)*minLength;

	double minSuccess=parameters.minDynamicMeasurementRatio;
	if(parameters.pipelineParameters.flagTrackStatic && !parameters.pipelineParameters.flagTrackDynamic)
		minSuccess=1-minSuccess;

	vector<ImageFeatureTrajectoryT> trajectoryList=tracker.GetTrajectories(minSuccess, parameters.firstFrame, parameters.lastFrame, minMeasurement, minLength, maxMissRatio);

	//Postprocess
	optional<CoordinateTransformations2DT::affine_transform_type>  dummy;
	ImageFeatureTrajectoryIOC<ImageFeatureTrackingProblemC::time_stamp_type>::PostprocessMeasurements(trajectoryList, parameters.distortion, dummy);

	logger["NumTracks"]=trajectoryList.size();
	logger["Runtime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	//Save

	SaveLog(trajectoryList);

	if(!parameters.trajectoryFile.empty())
		ImageFeatureTrajectoryIOC<ImageFeatureTrackingProblemC::time_stamp_type>::WriteTrajectoryFile(trajectoryList, parameters.outputPath+parameters.trajectoryFile);

	return true;
}	//bool Run()

}	//AppStalker22N
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("140518");
    std::string header("stalker: Image feature tracker");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppStalker22N::StalkerImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
