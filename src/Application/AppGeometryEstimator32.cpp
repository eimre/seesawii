/**
 * @file AppGeometryEstimator32.cpp Implementation of the 3D-2D registration application
 * @author Evren Imre
 * @date 23 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <cmath>
#include <functional>
#include <fstream>
#include "Application.h"
#include "../ApplicationInterface/InterfaceGeometryEstimationPipeline.h"
#include "../ApplicationInterface/InterfaceTwoStageRANSAC.h"
#include "../ApplicationInterface/InterfaceRANSAC.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Correspondence.h"
#include "../Elements/FeatureUtility.h"
#include "../IO/CameraIO.h"
#include "../IO/SceneFeatureIO.h"
#include "../IO/ImageFeatureIO.h"
#include "../IO/SpecialArrayIO.h"
#include "../IO/ArrayIO.h"
#include "../IO/CoordinateCorrespondenceIO.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationComponents/P2PComponents.h"
#include "../GeometryEstimationComponents/P2PfComponents.h"
#include "../GeometryEstimationComponents/P3PComponents.h"
#include "../GeometryEstimationComponents/P4PComponents.h"
#include "../GeometryEstimationComponents/P6PComponents.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/BoostFilesystem.h"

namespace SeeSawN
{
namespace AppGeometryEstimator32N
{

using namespace ApplicationN;
using boost::optional;
using boost::math::pow;
using boost::math::chi_squared;
using boost::math::quantile;
using Eigen::MatrixXd;
using Eigen::Matrix3d;
using Eigen::Matrix4d;
using std::bind;
using std::greater;
using std::ofstream;
using std::min;
using SeeSawN::ApplicationN::InterfaceGeometryEstimationPipelineC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::ApplyFeatureCoordinateTransformation;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::SceneFeatureIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::ReadCovFile;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ION::CoordinateCorrespondenceIOC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::CoordinateTransformations3DT;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::MakeRANSACP2PProblem;
using SeeSawN::GeometryN::MakePDLP2PProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineP2PProblem;
using SeeSawN::GeometryN::MakeRANSACP2PfProblem;
using SeeSawN::GeometryN::MakePDLP2PfProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineP2PfProblem;
using SeeSawN::GeometryN::MakeRANSACP3PProblem;
using SeeSawN::GeometryN::MakePDLP3PProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineP3PProblem;
using SeeSawN::GeometryN::MakeRANSACP4PProblem;
using SeeSawN::GeometryN::MakePDLP4PProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineP4PProblem;
using SeeSawN::GeometryN::MakeRANSACP6PProblem;
using SeeSawN::GeometryN::MakePDLP6PProblem;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineP6PProblem;
using SeeSawN::GeometryN::RANSACP2PProblemT;
using SeeSawN::GeometryN::PDLP2PProblemT;
using SeeSawN::GeometryN::P2PEstimatorProblemT;
using SeeSawN::GeometryN::P2PPipelineProblemT;
using SeeSawN::GeometryN::P2PPipelineT;
using SeeSawN::GeometryN::RANSACP2PfProblemT;
using SeeSawN::GeometryN::PDLP2PfProblemT;
using SeeSawN::GeometryN::P2PfEstimatorProblemT;
using SeeSawN::GeometryN::P2PfPipelineProblemT;
using SeeSawN::GeometryN::P2PfPipelineT;
using SeeSawN::GeometryN::RANSACP3PProblemT;
using SeeSawN::GeometryN::PDLP3PProblemT;
using SeeSawN::GeometryN::P3PEstimatorProblemT;
using SeeSawN::GeometryN::P3PPipelineProblemT;
using SeeSawN::GeometryN::P3PPipelineT;
using SeeSawN::GeometryN::RANSACP4PProblemT;
using SeeSawN::GeometryN::PDLP4PProblemT;
using SeeSawN::GeometryN::P4PEstimatorProblemT;
using SeeSawN::GeometryN::P4PPipelineProblemT;
using SeeSawN::GeometryN::P4PPipelineT;
using SeeSawN::GeometryN::RANSACP6PProblemT;
using SeeSawN::GeometryN::PDLP6PProblemT;
using SeeSawN::GeometryN::P6PEstimatorProblemT;
using SeeSawN::GeometryN::P6PPipelineProblemT;
using SeeSawN::GeometryN::P6PPipelineT;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::WrappersN::IsWriteable;

//TODO Optional fixed 3D structure covariance, like geometryEstimator33. We do not always have access to a .cov file
/**
 * @brief Parameter object for GeometryEstimator32ImplementationC
 * @ingroup Application
 */
struct GeometryEstimator32ParametersC
{
	enum class Geometry32ProblemT{AUTO, P2P, P2PF, P3P, P4P, P6P};	//Registration problem types. If AUTO, the applications automatically chooses a solver, to estimate the missing calibration parameters in the input camera file

	//General
	int seed;	///< RNG seed

	//Problem
	Geometry32ProblemT problemType;	///< Type of the problem
	double noiseVariance;	///< Image noise variance, in pixel^2. >0
	bool flagInitialEstimate;	///< True if the algorithm attempts to derive an initial estimate from the input camera file
	double initialEstimateNoiseVariance;	///< Additive noise due to the initial estimate, in multiples of the image noise variance

	//Pipeline
	GeometryEstimationPipelineParametersC pipelineParameters;	///< Geometry estimation pipeline parameters
	double inlierRejectionProbability;	///< Probability of the rejection of an inlier
	double loGeneratorRatio1;	///< Size of the LO generators, as a multiple of the minimal generator, stage 1
	double loGeneratorRatio2;	///< Size of the LO generators, as a multiple of the minimal generator, stage 2
	double moMaxAlgebraicDistance;	///< Maximum algebraic model distance for an admissible association between a hypothesis and an existing cluster (MORANSAC)

	//Input
	string sceneFile;	///< Filename for the 3D model
	string sceneCovarianceFile;	///< Filename for the covariance of the scene points
	string featureFile;	///< Filename for the image features
	string cameraFile;	///< Filename for the camera file
	double gridSpacing;	///< Minimum distance between two neighbouring image features
	optional<FrameT> roi;	///< Region of interest on the image

	//Output
	string outputPath;	///< Output path
	string cameraEstimateFile;	///< Filename for the estimated camera
	string covarianceFile;	///< Covariance estimate
	string correspondenceFile;	///< Filename for the correspondences
	string sceneInlierFile;	///< Filename for the inlier scene features
	string imageInlierFile;	///< Filename for the inlier image features
	string logFile;	///< Filename for the log
	bool flagVerbose;   ///< Verbosity flag

	GeometryEstimator32ParametersC() : seed(0), problemType(Geometry32ProblemT::AUTO), noiseVariance(4), flagInitialEstimate(false), initialEstimateNoiseVariance(4), inlierRejectionProbability(0.05), loGeneratorRatio1(3), loGeneratorRatio2(3),  moMaxAlgebraicDistance(1), gridSpacing(0), flagVerbose(true)
	{}
};	//struct GeometryEstimator32ParametersC

/**
 * @brief Implementation of GeometryEstimtor32
 * @ingroup Application
 * @nosubgrouping
 */
class GeometryEstimator32ImplementationC
{
	private:

    	typedef GeometryEstimator32ParametersC::Geometry32ProblemT ProblemIdentifierT;	///< Problem identifier tags

    	/** @name Configuration */ //@{
    	auto_ptr<options_description> commandLineOptions; ///< Command line options
  														  	  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

    	GeometryEstimator32ParametersC parameters;  ///< Application parameters
    	//@}

    	/**@name State *///@{
    	map<string, double> logger;	///< Collects various statistics for the log
    	//@}

    	/** @name Implementation details */ //@{
    	static ptree PruneParameterTree(const ptree& src);	///< Removes the redundant parameters from the tree

    	bool VerifyProblem(ProblemIdentifierT problemTag, const CameraC& camera) const;	///< Verifies whether all necessary camera parameters are available for a specified problem
    	tuple<ProblemIdentifierT, optional<CameraMatrixT> > ProcessCamera(CameraC& camera) const;	///< Processes the input camera

    	typedef CoordinateTransformations2DT::affine_transform_type AffineTransform2T;	///< A 2D affine transformation
    	typedef CoordinateTransformations3DT::projective_transform_type ProjectiveTransform3T;	///< A 3D projective transformation

    	tuple<vector<ImageFeatureC>, optional<AffineTransform2T>, LensDistortionC > LoadImageFeatures(CameraC camera) const;	///< Loads the image features
    	tuple<vector<SceneFeatureC>, vector<Matrix3d>, optional<ProjectiveTransform3T> >  LoadSceneFeatures(const CameraC& camera) const;	///< Loads the scene features

    	typedef RANSACP2PProblemT::dimension_type1 dimension_type1;
    	typedef RANSACP2PProblemT::dimension_type2 dimension_type2;
		tuple<dimension_type1, dimension_type2> ComputeRANSACBinSize(const vector<SceneFeatureC>& sceneModel, const vector<ImageFeatureC>& featureList) const;	///< Computes the bin sizes for RANSAC

    	GeometryEstimationPipelineDiagnosticsC ComputeP2P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const;	///< Runs the P2P pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeP2Pf(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures, const AffineTransform2T& normaliser) const;	///< Runs the P2Pf pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeP3P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const;	///< Runs the P3P pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeP4P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures, const AffineTransform2T& normaliser) const;	///< Runs the P4P pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeP6P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const;	///< Runs the P6P pipeline
    	void SaveOutput(vector<SceneFeatureC>& sceneModel, vector<ImageFeatureC>& imageFeatures, const CameraC& camera, const CameraMatrixT& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics, const optional<ProjectiveTransform3T>& shifter, const optional<AffineTransform2T>& normaliser, const LensDistortionC& distortion);	///< Saves the output
    	//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		GeometryEstimator32ImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}

};	//class GeometryEstimator32ImplementationC

/**
 * @brief Removes the redundant parameters from the tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree GeometryEstimator32ImplementationC::PruneParameterTree(const ptree& src)
{
	ptree output(src);

    if(src.get_child_optional("EstimationPipeline.Main.NoThreads"))
    	output.get_child("EstimationPipeline.Main").erase("NoThreads");

    if(src.get_child_optional("EstimationPipeline.Main.FlagVerbose"))
    	output.get_child("EstimationPipeline.Main").erase("FlagVerbose");

    return output;
}	//void PruneParameterTree(ptree& parameters)

/**
 * @brief Verifies whether all necessary camera parameters are available for a specified problem
 * @param[in] problemTag Problem identifier
 * @param[in] camera Camera
 * @return \c false if \c camera does not have the necessary information for the problem
 */
bool GeometryEstimator32ImplementationC::VerifyProblem(ProblemIdentifierT problemTag, const CameraC& camera) const
{
	//P2P? Camera centre and intrinsic parameters
	if(problemTag==ProblemIdentifierT::P2P)
		return camera.Extrinsics() && camera.Extrinsics()->position && camera.MakeIntrinsicCalibrationMatrix();

	//P3P? Intrinsic calibration parameters
	if(problemTag==ProblemIdentifierT::P3P)
		return (bool)camera.MakeIntrinsicCalibrationMatrix();

	//P2Pf? Position and principal point
	if(problemTag==ProblemIdentifierT::P2PF)
		return camera.Extrinsics() && camera.Intrinsics() && camera.Extrinsics()->position && camera.Intrinsics()->principalPoint;

	//P4P? Principal point
	if(problemTag==ProblemIdentifierT::P4P)
		return camera.Intrinsics() && camera.Intrinsics()->principalPoint;

	//Default: P6P
	return true;
}	//bool VerifyProblem(ProblemIdentifierT problemTag, const CameraC& camera)

/**
 * @brief Processes the input camera
 * @param[in,out] camera Camera
 * @return A tuple: A problem identifier and a camera matrix. If an initial cannot be computed (or is not requested), the camera matrix is invalid
 * @throws runtime_error If the frame box is undefined
 */
auto GeometryEstimator32ImplementationC::ProcessCamera(CameraC& camera) const -> tuple<ProblemIdentifierT, optional<CameraMatrixT> >
{
	//Identify the available parameters
	bool flagFrame=(bool)camera.FrameBox();
	bool flagPosition=camera.Extrinsics() && camera.Extrinsics()->position;
	bool flagFocal=camera.Intrinsics() && camera.Intrinsics()->focalLength;

	optional<CameraMatrixT> initialEstimate;	// Initial estimate
	ProblemIdentifierT problemTag=parameters.problemType;	//Automatically detected problem

	//Image dimensions are necessary for normalisation and a number of other tasks
	if(!flagFrame)
		throw(runtime_error("GeometryEstimator32ImplementationC::ProcessCamera: FrameBox is not defined."));

	if(!camera.Intrinsics())
		camera.Intrinsics()=IntrinsicC();	//Default intrinsic calibration parameters

	if(!camera.Intrinsics()->principalPoint)
		camera.Intrinsics()->principalPoint = camera.FrameBox()->center();

	//Determine the problem type
	if(parameters.problemType==ProblemIdentifierT::AUTO)
	{
		problemTag=ProblemIdentifierT::P6P;

		if(flagPosition)
			problemTag = flagFocal ? ProblemIdentifierT::P2P : ProblemIdentifierT::P2PF;

		if(!flagPosition)
			problemTag = flagFocal ? ProblemIdentifierT::P3P : ProblemIdentifierT::P4P;
	}	//if(parameters.problemType==ProblemIdentifierT::AUTO)

	//Initial estimate
	if(parameters.flagInitialEstimate)
		initialEstimate=camera.MakeCameraMatrix();	//No special case for known intrinsics or camera centre. This is handled later

	return make_tuple(problemTag, initialEstimate);
}	//tie<ProblemIdentifierT, optional<CameraMatrixT> > ProcessCamera(const CameraC& camera)

/**
 * @brief Loads the image features
 * @param[in] camera Camera
 * @return Tuple: A vector of image features; normalising transformation (if necessary), lens distortion
 * @remarks This is called after ProcessCamera. So the camera is already validated for the designated problem type
 */
auto GeometryEstimator32ImplementationC::LoadImageFeatures(CameraC camera) const -> tuple<vector<ImageFeatureC>, optional<AffineTransform2T>, LensDistortionC >
{
	//Lens distortion
	optional<LensDistortionC> distortion=camera.Distortion();

	if(!distortion)
		distortion=LensDistortionC();	//NOD

	//P2P, P2Pf, P3P and P4P require normalised image coordinates
	optional<AffineTransform2T> normaliser;
	bool flagNormalise=parameters.problemType==ProblemIdentifierT::P2P || parameters.problemType==ProblemIdentifierT::P2PF || parameters.problemType==ProblemIdentifierT::P3P || parameters.problemType==ProblemIdentifierT::P4P;
	if(flagNormalise)
	{
		//Intrinsics
		if((parameters.problemType==ProblemIdentifierT::P2PF || parameters.problemType==ProblemIdentifierT::P4P) && !camera.Intrinsics()->focalLength)
			camera.Intrinsics()->focalLength=camera.FrameBox()->diagonal().norm();	//~"Normal lens" for digital stills. FoV: 53 degrees

		normaliser=CoordinateTransformations2DT::affine_transform_type();
		optional<IntrinsicCalibrationMatrixT> mK=camera.MakeIntrinsicCalibrationMatrix();

		//Solvers employing a rotation estimation component cannot be used with non-isotropic scaling
		double avgFocal= ((*mK)(0,0) + (*mK)(1,1))*0.5;
		(*mK)(0,0)=avgFocal;
		(*mK)(1,1)=avgFocal;

		normaliser->matrix()=mK->inverse();
	}	//if(parameters.problemType==ProblemIdentifierT::P2P || parameters.problemType==ProblemIdentifierT::P2PF || parameters.problemType==ProblemIdentifierT::P3P)

	tuple<vector<ImageFeatureC>, optional<AffineTransform2T>, LensDistortionC > output;
	get<1>(output)=normaliser;
	get<2>(output)=*distortion;

	//RoI
	optional<tuple<Coordinate2DT, Coordinate2DT> > roi;

	if(parameters.roi)
		roi=make_tuple(parameters.roi->corner(FrameT::BottomLeft), parameters.roi->corner(FrameT::TopRight) );
	else
		roi=make_tuple(camera.FrameBox()->corner(FrameT::BottomLeft), camera.FrameBox()->corner(FrameT::TopRight) );	//camera is validated, so there is a valid frame box

	get<0>(output)=ImageFeatureIOC::ReadFeatureFile(parameters.featureFile, true);	//Read
	ImageFeatureIOC::PreprocessFeatureSet(get<0>(output), *roi, parameters.gridSpacing, true, *distortion, normaliser); //Preprocess

	return output;
}	//vector<ImageFeatureC> LoadImageFeatures(const CameraC& camera) const

/**
 * @brief Loads the scene features
 * @param[in] camera Camera
 * @return A tuple: vector of scene features, vector of covariance matrices, shift applied to the scene points (invalid if not required)
 * @throws runtime_error If the scene model and the covariance list do not have the same number of elements
 */
auto GeometryEstimator32ImplementationC::LoadSceneFeatures(const CameraC& camera) const -> tuple<vector<SceneFeatureC>, vector<Matrix3d>, optional<ProjectiveTransform3T> >
{
	tuple<vector<SceneFeatureC>, vector<Matrix3d>, optional<ProjectiveTransform3T> > output;

	get<0>(output)=SceneFeatureIOC::ReadFeatureFile(parameters.sceneFile, true);

	vector<Coordinate3DT> dummy;	//Ignored
	std::tie(dummy, get<1>(output))=ReadCovFile<ValueTypeM<Coordinate3DT>::type, RowsM<Coordinate3DT>::value>(parameters.sceneCovarianceFile);

	if(get<0>(output).size()!=get<1>(output).size())
		throw(runtime_error(string("GeometryEstimator32ImplementationC::LoadSceneFeatures() : The scene model and the covariance list must have the same number of elements. Values= ")+lexical_cast<string>(get<0>(output).size())+"/"+lexical_cast<string>(get<1>(output).size())));

	SceneFeatureIOC::PreprocessDescriptors(get<0>(output), true);

	//Shift the origin to the camera centre
	//This is called after the validation of the problem type. So, it is guaranteed to have a camera centre
	if(parameters.problemType==ProblemIdentifierT::P2P || parameters.problemType==ProblemIdentifierT::P2PF)
	{
		ProjectiveTransform3T::MatrixType mT=ProjectiveTransform3T::MatrixType::Identity();
		mT.col(3).segment(0,3)=- (*camera.Extrinsics()->position);

		get<2>(output)=ProjectiveTransform3T();
		get<2>(output)->matrix()=mT;

		ApplyFeatureCoordinateTransformation(get<0>(output), mT);
	}	//if(parameters.problemType==ProblemIdentifierT::P2P || parameters.problemType==ProblemIdentifierT::P2PF)

	return output;
}	//vector<SceneFeatureC> LoadSceneFeatures() const

/**
 * @brief Computes the bin sizes for RANSAC
 * @param[in] sceneModel Scene model
 * @param[in] featureList Image features
 * @pre \c sceneModel is nonempty
 * @pre \c featureList is nonempty
 * @return A pair of bins. [First set; Second set]
 */
auto GeometryEstimator32ImplementationC::ComputeRANSACBinSize(const vector<SceneFeatureC>& sceneModel, const vector<ImageFeatureC>& featureList) const -> tuple<dimension_type1, dimension_type2>
{
	//Preconditions
	assert(!sceneModel.empty());
	assert(!featureList.empty());

	vector<Coordinate3DT> coordinateList1=MakeCoordinateVector(sceneModel);
	dimension_type1 binSize1=*ComputeBinSize<Coordinate3DT>(coordinateList1, parameters.pipelineParameters.binDensity1);

	vector<Coordinate2DT> coordinateList2=MakeCoordinateVector(featureList);
	dimension_type2 binSize2=*ComputeBinSize<Coordinate2DT>(coordinateList2, parameters.pipelineParameters.binDensity2);

	return make_tuple(binSize1, binSize2);
}	//tuple<dimension_type1, dimension_type2> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const

/**
 * @brief Runs the P2P pipeline
 * @param[out] model Estimated model
 * @param[out] covariance Estimate covariance. Invalid if not requested
 * @param[out] correspondences Inliers
 * @param[in] initialEstimate Initial estimate
 * @param[in] sceneModel 3D scene model
 * @param[in] sceneCovariance Covariance of the individual scene points
 * @param[in] imageFeatures 2D image features
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator32ImplementationC::ComputeP2P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const
{
	unsigned int loGeneratorSize1 = P2PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2 = P2PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio2);

	dimension_type1 binSize1;
	dimension_type2 binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(sceneModel, imageFeatures);

	RANSACP2PProblemT::data_container_type dummyList;
	RANSACP2PProblemT ransacProblem1=MakeRANSACP2PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACP2PProblemT ransacProblem2=MakeRANSACP2PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLP2PProblemT optimisationProblem=MakePDLP2PProblem(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	P2PEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);
	P2PPipelineProblemT problem=MakeGeometryEstimationPipelineP2PProblem(sceneModel, imageFeatures, geometryEstimationProblem, sceneCovariance, parameters.noiseVariance, parameters.inlierRejectionProbability, initialEstimate, parameters.noiseVariance*(1+parameters.initialEstimateNoiseVariance), parameters.inlierRejectionProbability, parameters.pipelineParameters.nThreads);

	P2PPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return P2PPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeP2P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<ImageFeatureC>& imageFeatures)

/**
 * @brief Runs the P2Pf pipeline
 * @param[out] model Estimated model
 * @param[out] covariance Estimate covariance. Invalid if not requested
 * @param[out] correspondences Inliers
 * @param[in] initialEstimate Initial estimate
 * @param[in] sceneModel 3D scene model
 * @param[in] sceneCovariance Covariance of the individual scene points
 * @param[in] imageFeatures 2D image features
 * @param[in] normaliser Normalising transformation applied to the image points
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator32ImplementationC::ComputeP2Pf(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures, const AffineTransform2T& normaliser) const
{
	unsigned int loGeneratorSize1 = P2PfPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2 = P2PfPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio2);

	dimension_type1 binSize1;
	dimension_type2 binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(sceneModel, imageFeatures);

	RANSACP2PfProblemT::data_container_type dummyList;
	RANSACP2PfProblemT ransacProblem1=MakeRANSACP2PfProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACP2PfProblemT ransacProblem2=MakeRANSACP2PfProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLP2PfProblemT optimisationProblem=MakePDLP2PfProblem(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	P2PfEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);
	P2PfPipelineProblemT problem=MakeGeometryEstimationPipelineP2PfProblem(sceneModel, imageFeatures, geometryEstimationProblem, sceneCovariance, parameters.noiseVariance, parameters.inlierRejectionProbability, initialEstimate, parameters.noiseVariance*(1+parameters.initialEstimateNoiseVariance), parameters.inlierRejectionProbability, parameters.pipelineParameters.nThreads);

	P2PfPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	CameraMatrixT normalisedModel;
	optional<MatrixXd> normalisedCovariance;
	GeometryEstimationPipelineDiagnosticsC diagnostics=P2PfPipelineT::Run(normalisedModel, normalisedCovariance, correspondences, problem, rng, parameters.pipelineParameters);

	//Denormalise
	AffineTransform2T denormaliser= normaliser.inverse();
	model = denormaliser.matrix() * normalisedModel;
	if(normalisedCovariance)
	{
		Matrix4d mS=Matrix4d::Identity(); mS(0,0)= 0.5*(denormaliser.matrix()(0,0)+denormaliser.matrix()(1,1));
		covariance = mS* (*normalisedCovariance) *mS.transpose();
	}	//if(normalisedCovariance)

	return diagnostics;
}	//GeometryEstimationPipelineDiagnosticsC ComputeP2Pf(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const

/**
 * @brief Runs the P3P pipeline
 * @param[out] model Estimated model
 * @param[out] covariance Estimate covariance. Invalid if not requested
 * @param[out] correspondences Inliers
 * @param[in] initialEstimate Initial estimate
 * @param[in] sceneModel 3D scene model
 * @param[in] sceneCovariance Covariance of the individual scene points
 * @param[in] imageFeatures 2D image features
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator32ImplementationC::ComputeP3P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const
{
	unsigned int loGeneratorSize1 = P3PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2 = P3PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio2);

	dimension_type1 binSize1;
	dimension_type2 binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(sceneModel, imageFeatures);

	RANSACP3PProblemT::data_container_type dummyList;
	RANSACP3PProblemT ransacProblem1=MakeRANSACP3PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACP3PProblemT ransacProblem2=MakeRANSACP3PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLP3PProblemT optimisationProblem=MakePDLP3PProblem(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	P3PEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);
	P3PPipelineProblemT problem=MakeGeometryEstimationPipelineP3PProblem(sceneModel, imageFeatures, geometryEstimationProblem, sceneCovariance, parameters.noiseVariance, parameters.inlierRejectionProbability, initialEstimate, parameters.noiseVariance*(1+parameters.initialEstimateNoiseVariance), parameters.inlierRejectionProbability, parameters.pipelineParameters.nThreads);

	P3PPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return P3PPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeP3P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const

/**
 * @brief Runs the P4P pipeline
 * @param[out] model Estimated model
 * @param[out] covariance Estimate covariance. Invalid if not requested
 * @param[out] correspondences Inliers
 * @param[in] initialEstimate Initial estimate
 * @param[in] sceneModel 3D scene model
 * @param[in] sceneCovariance Covariance of the individual scene points
 * @param[in] imageFeatures 2D image features
 * @param[in] normaliser Normalising transformation applied to the image points
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator32ImplementationC::ComputeP4P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures, const AffineTransform2T& normaliser) const
{
	unsigned int loGeneratorSize1 = P4PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2 = P4PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio2);

	dimension_type1 binSize1;
	dimension_type2 binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(sceneModel, imageFeatures);

	RANSACP4PProblemT::data_container_type dummyList;
	RANSACP4PProblemT ransacProblem1=MakeRANSACP4PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACP4PProblemT ransacProblem2=MakeRANSACP4PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLP4PProblemT optimisationProblem=MakePDLP4PProblem(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	P4PEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);
	P4PPipelineProblemT problem=MakeGeometryEstimationPipelineP4PProblem(sceneModel, imageFeatures, geometryEstimationProblem, sceneCovariance, parameters.noiseVariance, parameters.inlierRejectionProbability, initialEstimate, parameters.noiseVariance*(1+parameters.initialEstimateNoiseVariance), parameters.inlierRejectionProbability, parameters.pipelineParameters.nThreads);

	P4PPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	CameraMatrixT normalisedModel;
	optional<MatrixXd> normalisedCovariance;
	GeometryEstimationPipelineDiagnosticsC diagnostics=P4PPipelineT::Run(normalisedModel, normalisedCovariance, correspondences, problem, rng, parameters.pipelineParameters);

	//Denormalise
	AffineTransform2T denormaliser= normaliser.inverse();
	model = denormaliser.matrix() * normalisedModel;
	if(normalisedCovariance)
	{
		MatrixXd mS(7,7); mS.setIdentity(); mS(0,0)= 0.5*(denormaliser.matrix()(0,0)+denormaliser.matrix()(1,1));
		covariance = mS* (*normalisedCovariance) *mS.transpose();
	}	//if(normalisedCovariance)

	return diagnostics;
}	//GeometryEstimationPipelineDiagnosticsC ComputeP3P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const

/**
 * @brief Runs the P6P pipeline
 * @param[out] model Estimated model
 * @param[out] covariance Estimate covariance. Invalid if not requested
 * @param[out] correspondences Inliers
 * @param[in] initialEstimate Initial estimate
 * @param[in] sceneModel 3D scene model
 * @param[in] sceneCovariance Covariance of the individual scene points
 * @param[in] imageFeatures 2D image features
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator32ImplementationC::ComputeP6P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const
{
	unsigned int loGeneratorSize1 = P6PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2 = P6PPipelineProblemT::GetMinimalGeneratorSize()*max(1.0, parameters.loGeneratorRatio2);

	dimension_type1 binSize1;
	dimension_type2 binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(sceneModel, imageFeatures);

	RANSACP6PProblemT::data_container_type dummyList;
	RANSACP6PProblemT ransacProblem1=MakeRANSACP6PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACP6PProblemT ransacProblem2=MakeRANSACP6PProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLP6PProblemT optimisationProblem=MakePDLP6PProblem(CameraMatrixT::Zero(), dummyList, optional<MatrixXd>());

	P6PEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);
	P6PPipelineProblemT problem=MakeGeometryEstimationPipelineP6PProblem(sceneModel, imageFeatures, geometryEstimationProblem, sceneCovariance, parameters.noiseVariance, parameters.inlierRejectionProbability, initialEstimate, parameters.noiseVariance*(1+parameters.initialEstimateNoiseVariance), parameters.inlierRejectionProbability, parameters.pipelineParameters.nThreads);

	P6PPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return P6PPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeP6P(CameraMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<CameraMatrixT>& initialEstimate, const vector<SceneFeatureC>& sceneModel, const vector<Matrix3d>& sceneCovariance, const vector<ImageFeatureC>& imageFeatures) const

/**
 * @brief Saves the output
 * @param[in] sceneModel Scene model
 * @param[in] imageFeatures Image features
 * @param[in] camera Availabe camera parameters
 * @param[in] model Estimated camera matrix
 * @param[in] covariance Estimate covariance
 * @param[in] inliers Inlier list
 * @param[in] diagnostics Diagnostices
 * @param[in] shifter Transformation to shift the origin to the camera centre
 * @param[in] normaliser Normalising transformation
 * @param[in] distortion Lens distortion
 * @throw runtime_error If a write operation fails
 */
void GeometryEstimator32ImplementationC::SaveOutput(vector<SceneFeatureC>& sceneModel, vector<ImageFeatureC>& imageFeatures, const CameraC& camera, const CameraMatrixT& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics, const optional<ProjectiveTransform3T>& shifter, const optional<AffineTransform2T>& normaliser, const LensDistortionC& distortion)
{
	//Camera
	if(diagnostics.flagSuccess && !parameters.cameraEstimateFile.empty())
	{
		//Overwrite with the estimated camera parameters
		CameraC estimated(model);
		CameraC tmp(camera);

		if(parameters.problemType==ProblemIdentifierT::P2P || parameters.problemType==ProblemIdentifierT::P2PF)
			tmp.Extrinsics()->orientation=estimated.Extrinsics()->orientation;

		if(parameters.problemType==ProblemIdentifierT::P3P || parameters.problemType==ProblemIdentifierT::P4P)
			tmp.Extrinsics()=estimated.Extrinsics();

		if(parameters.problemType==ProblemIdentifierT::P2PF || parameters.problemType==ProblemIdentifierT::P4P)
			tmp.Intrinsics()->focalLength=estimated.Intrinsics()->focalLength;

		if(parameters.problemType==ProblemIdentifierT::P6P)
		{
			tmp.Intrinsics()=estimated.Intrinsics();
			tmp.Extrinsics()=estimated.Extrinsics();
		}	//if(parameters.problemType==ProblemIdentifierT::P6P)

		CameraIOC::WriteCameraFile(vector<CameraC>(1,tmp), parameters.outputPath+parameters.cameraEstimateFile, string(""), true);
	}	//if(diagnostics.flagSuccess && !parameters.cameraEstimateFile.empty())

	//Covariance
	if(diagnostics.flagSuccess && covariance && !parameters.covarianceFile.empty())
	{
		ofstream covFile(parameters.outputPath+parameters.covarianceFile);
		if(covFile.fail())
			throw(runtime_error(string("GeometryEstimator32ImplementationC::SaveOutput: Cannot open ")+parameters.outputPath+parameters.covarianceFile));

		//Header

		if(parameters.problemType==ProblemIdentifierT::P2P)
			covFile<<"# Orientation (3)"<<"\n";

		if(parameters.problemType==ProblemIdentifierT::P2PF)
			covFile<<"# Focal length (1); Orientation (3)"<<"\n";

		if(parameters.problemType==ProblemIdentifierT::P3P)
			covFile<<"# Position(3); Orientation (3)"<<"\n";

		if(parameters.problemType==ProblemIdentifierT::P4P)
			covFile<<"#Focal length(1); # Position(3); Orientation (3)"<<"\n";

		if(parameters.problemType==ProblemIdentifierT::P6P)
			covFile<<"# Focal length(1); Aspect ratio(1); Skewness(1); Principal point(2); Position(3); Orientation (3)"<<"\n";

		ArrayIOC<MatrixXd>::WriteArray(covFile, *covariance);
	}	//if(diagnostics.flagSuccess && covariance && !parameters.covarianceFile.empty())

	//Correspondences

	//Shift the scene model back to the world origin
	if(shifter)
	{
		ProjectiveTransform3T::MatrixType deshifter=shifter->matrix().inverse();
		ApplyFeatureCoordinateTransformation(sceneModel, deshifter);
	}	//if(shifter)

	//Denormalise the image features

	optional<AffineTransform2T> denormaliser;
	if(normaliser)
		denormaliser=normaliser->inverse();

	ImageFeatureIOC::PostprocessFeatureSet(imageFeatures, distortion, denormaliser);

	if(!parameters.correspondenceFile.empty())
	{
		vector<Coordinate3DT> coordinates1=MakeCoordinateVector(sceneModel);
		vector<Coordinate2DT> coordinates2=MakeCoordinateVector(imageFeatures);
		CoordinateCorrespondenceList32DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList32DT>(inliers, coordinates1, coordinates2);
		CoordinateCorrespondenceIOC::WriteCorrespondenceFile(coordinateCorrespondences, parameters.outputPath+parameters.correspondenceFile);
	}	//if(!parameters.correspondenceFile.empty())

	if(!parameters.sceneInlierFile.empty() || !parameters.imageInlierFile.empty())
	{
		vector<SceneFeatureC> inlier3D(sceneModel);
		vector<ImageFeatureC> inlier2D(imageFeatures);
		MakeFeatureCorrespondenceList(inlier3D, inlier2D, inliers);

		if(!parameters.sceneInlierFile.empty())
			SceneFeatureIOC::WriteFeatureFile(inlier3D, parameters.outputPath+parameters.sceneInlierFile);

		if(!parameters.imageInlierFile.empty())
			ImageFeatureIOC::WriteFeatureFile(inlier2D, parameters.outputPath+parameters.imageInlierFile);
	}	//	if(!parameters.correspondingFeatureFile1.empty() || !parameters.correspondingFeatureFile2.empty())

	//Log
	if(!parameters.logFile.empty())
	{
		ofstream logfile(parameters.outputPath+parameters.logFile);

		if(logfile.fail())
			throw(runtime_error(string("GeometryEstimator32ImplementationC::SaveOutput: Cannot open ")+parameters.outputPath+parameters.logFile));

		string timeString= to_simple_string(second_clock::universal_time());
		logfile<<"GeometryEstimator32 log file created on "<<timeString<<"\n";
		logfile<<"Number of scene features: "<<sceneModel.size()<<"\n";
		logfile<<"Number of image features: "<<imageFeatures.size()<<"\n";
		logfile<<"Number of correspondences:"<<inliers.size()<<"\n";

		if(diagnostics.flagSuccess)
		{
			logfile<<"Number of guided matching iterations: "<<diagnostics.nIteration<<"\n";
			logfile<<"Error: "<<diagnostics.error<<"\n";
		}	//if(diagnostics.flagSuccess)

		logfile<<"Execution time: "<<logger["Runtime"]<<"s";
	}	//if(!parameters.logFile.empty())
}	//void SaveOutput(vector<SceneFeatureC>& sceneModel, vector<ImageFeatureC>& imageFeatures, const CameraMatrixT& estimatedCamera, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
GeometryEstimator32ImplementationC::GeometryEstimator32ImplementationC()
{
	GeometryEstimator32ParametersC defaultParameters;

	chi_squared chi2(2);
	double defaultPerturbationFactor=sqrt(quantile(chi2, defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio));

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.nThreads)), "Number of threads available to the application. >0. Capped at the maximum number of available threads")
	("General.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.seed)), "Seed for the random number generator. If <0, rng default")

	("Problem.Type", value<string>(), "Problem type. Valid values: P2P (Nodal), P2Pf(Nodal+focal), P3P (Pose), P4P(Pose+focal), P6P(projective). If not defined, automatic detection")
	("Problem.NoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.noiseVariance)), "Variance of the noise on the image coordinates, in pixel^2. Affects the inlier threshold. >0")
	("Problem.FlagInitialEstimate", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagInitialEstimate)), "If true, the algorithm attempts to build an initial estimate from the input camera file")
	("Problem.InitialEstimateNoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.initialEstimateNoiseVariance)),"Variance of the additive noise due to errors in the intial estimate, in multiples of the image noise variance. >=0")

	("EstimationPipeline.Main.InlierRejectionProbability", value<string>()->default_value(lexical_cast<string>(defaultParameters.inlierRejectionProbability)), "The probability that an inlier correspondence is incorrectly rejected. Affects the inlier threshold. [0,1]")
	("EstimationPipeline.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.maxIteration)), "Maximum number of guided matching iterations. >0")
	("EstimationPipeline.Main.MinRelativeDifference", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.minRelativeDifference)), "If the relative error between two successive iterations is below this value, the procedure is terminated. >0")
	("EstimationPipeline.Main.RefreshTreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.refreshThreshold)), "If the relative improvement in the number of inliers between the inliers to the current set and that of the reference is above this value, the reference set is updated. >=1")
	("EstimationPipeline.Main.FlagCovariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.flagCovariance)), "If true, the algorithm calculates the covariance for the estimated parameters")

	("EstimationPipeline.Decimation.BinDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.binDensity1)),"Number of bins per standard deviation. Affects the spatial quantisation level. >0")
	("EstimationPipeline.Decimation.MaxObservationDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.maxObservationDensity)),"Observation set: Maximum number of correspondences per bin. >0")
	("EstimationPipeline.Decimation.MinObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.minObservationRatio)),"Observation set: Minimum number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Decimation.MaxObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.maxObservationRatio)),"Observation set: Maximum number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Decimation.ValidationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.validationRatio)),"Validation set: Number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Decimation.MaxAmbiguity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.maxAmbiguity)),"Maximum ambiguity for a correspondence in the observation or the validation set. [0,1]")

	("EstimationPipeline.Matching.kNN.MinRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the best similarity score to the second best, for an admissible correspondence. Quality-quantity trade-off.[0,1]")
	("EstimationPipeline.Matching.kNN.NeighbourhoodCardinality", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12)), "The maximum number of candidates that can be associated with a feature. k>1 yields ambiguous matches. >=1")
	("EstimationPipeline.Matching.kNN.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold)), "Minimum similarity for an admissible correspondence. >=0")
	("EstimationPipeline.Matching.kNN.FlagConsistencyTest", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.flagConsistencyTest)), "If true, each feature in a valid correspondence is among the best k candidates for the other")

	("EstimationPipeline.Matching.SpatialConsistency.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.flagBucketFilter)), "If true, the algorithm enforces a spatial consistency constraint between neighbouring correspondences")
	("EstimationPipeline.Matching.SpatialConsistency.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity for two spatial bins to be considered associated, relative to the maximum similarity. [0,1]")
	("EstimationPipeline.Matching.SpatialConsistency.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.minQuorum)), "If the number correspondences in a bin is below this value, it is held exempt from the spatial consistency check. >=0")
	("EstimationPipeline.Matching.SpatialConsistency.BucketDensity", value<string>()->default_value(lexical_cast<string>(1.0/defaultParameters.pipelineParameters.matcherParameters.bucketDimensions[0])), "Number of bins per standard deviation. >0")

	("EstimationPipeline.GeometryEstimation.RANSAC.Main.MinConfidence", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.minConfidence)), "Minimum probability that RANSAC finds an acceptable solution. (0,1]")
	("EstimationPipeline.GeometryEstimation.RANSAC.Main.PerturbationFactor", value<string>()->default_value(lexical_cast<string>(defaultPerturbationFactor)), "Maximum admissible distance between the true and the estimated position of an image feature, in multiples of the noise standard deviation. Affects the number of iterations. >0")

	("EstimationPipeline.GeometryEstimation.Stage1.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration)), "Maximum number of iterations. >=minIteration")
	("EstimationPipeline.GeometryEstimation.Stage1.Main.MinIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration)), "Minimum number of iterations. >=0")
	("EstimationPipeline.GeometryEstimation.Stage1.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("EstimationPipeline.GeometryEstimation.Stage1.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minError)), "If the error for the solution is below this value, the process is terminated")
	("EstimationPipeline.GeometryEstimation.Stage1.Main.MaxError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxError)), "If the error for the solution is above this value, it is rejected. >=minError")

	("EstimationPipeline.GeometryEstimation.Stage1.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations)), "Number of local optimisation iterations. >=0")
	("EstimationPipeline.GeometryEstimation.Stage1.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("EstimationPipeline.GeometryEstimation.Stage1.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")
	("EstimationPipeline.GeometryEstimation.Stage1.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.loGeneratorRatio1)), "Generator size for the LO iterations, in multiples of the minimal generator. Ignored if the solver cannot handle nonminimal sets. >=1")

	("EstimationPipeline.GeometryEstimation.Stage1.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagPROSAC)), "If true, PROSAC is enabled")
	("EstimationPipeline.GeometryEstimation.Stage1.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("EstimationPipeline.GeometryEstimation.Stage1.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("EstimationPipeline.GeometryEstimation.Stage1.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("EstimationPipeline.GeometryEstimation.Stage1.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("EstimationPipeline.GeometryEstimation.Stage2.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("EstimationPipeline.GeometryEstimation.Stage2.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minError)), "If the error for the solution is below this value, the process is terminated")

	("EstimationPipeline.GeometryEstimation.Stage2.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations)), "Number of local optimisation iterations. >=0")
	("EstimationPipeline.GeometryEstimation.Stage2.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("EstimationPipeline.GeometryEstimation.Stage2.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")
	("EstimationPipeline.GeometryEstimation.Stage2.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.loGeneratorRatio2)), "Generator size for the LO iterations, in multiples of the minimal generator. Ignored if the solver cannot handle nonminimal sets. >=1")

	("EstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MaxNoCluster", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxSolution)), "Maximum number of solutions. >=1")
	("EstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MinCoverage", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minCoverage)), "Minimum percentage validation support a model should achieve to start a new cluster. [0,1]")
	("EstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MinOverlapIndex", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minModelSimilarity)), "Minimum overlap index between the inlier sets of two models, for a valid association. >=0")
	("EstimationPipeline.GeometryEstimation.Stage2.MORANSAC.ParsimonyFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.parsimonyFactor)), "A new cluster is created only if the new total inlier count relative to the current is above this value . >=1")
	("EstimationPipeline.GeometryEstimation.Stage2.MORANSAC.DiversityFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.diversityFactor)), "A MORANSAC action is rejected if the new total inlier count relative to the current is below this value. >=0")
	("EstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MaxAlgebraicDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.moMaxAlgebraicDistance)), "Number of local optimisation iterations. >=0")

	("EstimationPipeline.GeometryEstimation.Stage2.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagPROSAC)), "If true, PROSAC is enabled")
	("EstimationPipeline.GeometryEstimation.Stage2.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("EstimationPipeline.GeometryEstimation.Stage2.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("EstimationPipeline.GeometryEstimation.Stage2.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("EstimationPipeline.GeometryEstimation.Stage2.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("EstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration)), "Maximum number of iterations. >=0")
	("EstimationPipeline.GeometryEstimation.Refinement.Main.Epsilon1", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.epsilon1)), "Gradient convergence criterion, as a function of the magnitude of the gradient vector. >0")
	("EstimationPipeline.GeometryEstimation.Refinement.Main.Epsilon2", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.epsilon2)), "Solution convergence criterion, as the relative distance between two successive updates. >0")

	("EstimationPipeline.GeometryEstimation.Refinement.TrustRegion.Delta0", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.delta0)), "Initial size of the trust region. >0")
	("EstimationPipeline.GeometryEstimation.Refinement.TrustRegion.LambdaShrink", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.lambdaShrink)), "Shrinkage rate for the trust region. (0,1)")
	("EstimationPipeline.GeometryEstimation.Refinement.TrustRegion.LambdaGrowth", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.lambdaGrowth)), "Growth rate for the trust region. >1")

	("EstimationPipeline.CovarianceEstimation.Alpha", value<string>()->default_value("1"), "Controls the sample spread for SUT. If omitted, calculated automatically. (0,1]")
	("EstimationPipeline.CovarianceEstimation.Beta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.sutParameters.beta)), "Compansates for errors in the high order moments. >=0")
	("EstimationPipeline.CovarianceEstimation.Kappa", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.sutParameters.kappa)), "Controls the sample spread for SUT.")

	("Input.SceneModel", value<string>(), "3D scene model, as a .ft3 file")
	("Input.SceneModelCovariance", value<string>(), "Covariance of the individual scene points, as a .cov3 file")
	("Input.Camera", value<string>(), "Camera file for the known calibration parameters, lens distortion and initial estimate")
	("Input.ImageFeatures", value<string>(), "2D image features, as a .ft2 file")
	("Input.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.gridSpacing)), "Minimum distance between two image features, in pixels. >=0")
	("Input.RegionOfInterest", value<string>()->default_value("0 0 1919 1079"), "Any image features outside of this window are ignored. [Upper-left; lower-right]. If omitted, all features are included")

	("Output.Root", value<string>()->default_value(""), "Root directory for the output files. Should be terminated with a slash")
	("Output.Camera", value<string>()->default_value(""), "Estimated camera")
	("Output.Covariance", value<string>()->default_value(""), "Estimate covariance")
	("Output.Correspondences", value<string>()->default_value(""), "Scene-image correspondences, as a .c32 file")
	("Output.SceneInliers", value<string>()->default_value(""), "Inlier scene features, as a .ft3 file")
	("Output.ImageInliers", value<string>()->default_value(""), "Inlier image features, as a .ft2 file")
	("Output.Log", value<string>()->default_value(""), "Log")
	("Output.FlagVerbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbosity flag")
	;
}	//GeometryEstimator32ImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& GeometryEstimator32ImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void GeometryEstimator32ImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
    ptree tree; //Options tree

    GeometryEstimator32ParametersC defaultParameters;

    tree.put("xmlcomment", "geometryEstimator32 configuration file");

    //General
    tree.put("General","");
    tree.put("General.NoThreads", defaultParameters.pipelineParameters.nThreads);
    tree.put("General.Seed",defaultParameters.seed);

    //Problem
    tree.put("Problem","");
    tree.put("Problem.Type", "P6P");
    tree.put("Problem.NoiseVariance", defaultParameters.noiseVariance);

    //Estimation pipeline

    ptree treeEstimationPipeline=InterfaceGeometryEstimationPipelineC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel);	//Main parameter list
    InterfaceTwoStageRANSACC::InsertGeometryProblemParameters(treeEstimationPipeline, "GeometryEstimation.RANSAC", detailLevel);

    //Add extra parameters
    if(detailLevel>1)
    {
        treeEstimationPipeline.put("Main.InlierRejectionProbability", defaultParameters.inlierRejectionProbability);
        treeEstimationPipeline.put("CovarianceEstimation.Alpha", 1);	//Just to expose the parameter.
    }

    if(!(detailLevel>1))
        if(treeEstimationPipeline.get_child_optional("GeometryEstimation.RANSAC.Stage2.MORANSAC"))
            treeEstimationPipeline.get_child("GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");

    tree.put_child("EstimationPipeline", treeEstimationPipeline);

    //Input
    tree.put("Input","");
    tree.put("Input.SceneModel", "model.ft3");
    tree.put("Input.SceneModelCovariance", "modelCovariance.cov3");
    tree.put("Input.Camera","camera.cam");
    tree.put("Input.ImageFeatures","features.ft2");

    if(detailLevel>1)
    {
        tree.put("Input.GridSpacing",defaultParameters.gridSpacing);
        tree.put("Input.RegionOfInterest", "0 0 1919 1079");
    }

    //Output
    tree.put("Output","");
    tree.put("Output.Root","Output/");
    tree.put("Output.Camera", "estimated.cam");
    tree.put("Output.Covariance", "covariance.txt");
    tree.put("Output.Correspondences", "correspondences.c32");
    tree.put("Output.SceneInliers", "inliers3.ft3");
    tree.put("Output.ImageInliers", "inliers2.ft2");
    tree.put("Output.Log", "log.log");
    tree.put("Output.FlagVerbose", defaultParameters.flagVerbose);

    if(detailLevel<=1)
    {
    	tree.put("Problem.FlagInitialEstimate", defaultParameters.flagInitialEstimate);
    	tree.put("Problem.InitialEstimateNoiseVariance", defaultParameters.initialEstimateNoiseVariance);
    }	//if(!flagBasic)

    tree=PruneParameterTree(tree);

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool GeometryEstimator32ImplementationC::SetParameters(const ptree& tree)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto o01=[](const double& val){return val>0 && val<1;};

	ptree lTree=PruneParameterTree(tree);
	GeometryEstimator32ParametersC defaultParameters;

	//Estimation Pipeline
	parameters.pipelineParameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(lTree.get_child("EstimationPipeline"));

	unsigned int nThreads = min((double)omp_get_max_threads(), ReadParameter(lTree, "General.NoThreads", gt0, "is not >0", optional<double>(defaultParameters.pipelineParameters.nThreads)));
	parameters.pipelineParameters.nThreads=nThreads;
	parameters.pipelineParameters.flagVerbose=lTree.get<bool>("Output.FlagVerbose", defaultParameters.flagVerbose);

	parameters.inlierRejectionProbability=ReadParameter(lTree, "EstimationPipeline.Main.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(defaultParameters.inlierRejectionProbability));

	if(lTree.get_child_optional("EstimationPipeline.GeometryEstimation.RANSAC"))
	{
		map<string, string> ransacExtra=InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(lTree.get_child("EstimationPipeline.GeometryEstimation.RANSAC"));

		auto itE=ransacExtra.end();

		auto it0=ransacExtra.find("Stage1.LORANSAC.GeneratorRatio");
		if(it0!=itE)
			parameters.loGeneratorRatio1=lexical_cast<double>(it0->second);

		auto it1=ransacExtra.find("Stage2.LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatio2=lexical_cast<double>(it1->second);

		auto it2=ransacExtra.find("Stage2.MORANSAC.MaxAlgebraicDistance");
		if(it2!=itE)
			parameters.moMaxAlgebraicDistance=lexical_cast<double>(it2->second);

		auto it3=ransacExtra.find("Main.EligibilityRatio");
		if(it3!=itE)
			parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=lexical_cast<double>(it3->second);
	}	//if(lTree.get_child_optional("EstimationPipeline.GeometryEstimation.RANSAC.Stage2"))

	//General
	parameters.seed=lTree.get<double>("General.Seed", defaultParameters.seed);

	//Problem

	if(lTree.get_optional<string>("Problem.Type"))
	{
		map<string, ProblemIdentifierT> problemTypeSelector{{"P2P", ProblemIdentifierT::P2P}, {"P2Pf", ProblemIdentifierT::P2PF}, {"P3P", ProblemIdentifierT::P3P}, {"P4P", ProblemIdentifierT::P4P}, {"P6P", ProblemIdentifierT::P6P}};
		auto itProblemType=problemTypeSelector.find(lTree.get<string>("Problem.Type",""));

		if(itProblemType==problemTypeSelector.end())
			throw(invalid_argument(string("GeometryEstimator32ImplementationC::SetParameters : Invalid problem type. Value=") + itProblemType->first) );

		parameters.problemType=itProblemType->second;
	}
	else
		parameters.problemType=ProblemIdentifierT::AUTO;

	parameters.noiseVariance=ReadParameter(lTree, "Problem.NoiseVariance", gt0, "is not >0", optional<double>(defaultParameters.noiseVariance));
	parameters.flagInitialEstimate=lTree.get<bool>("Problem.FlagInitialEstimate", defaultParameters.flagInitialEstimate);
	parameters.initialEstimateNoiseVariance=ReadParameter(lTree, "Problem.InitialEstimateNoiseVariance", geq0, "is not >=0", optional<double>(defaultParameters.initialEstimateNoiseVariance));

	//Input
	parameters.sceneFile=lTree.get<string>("Input.SceneModel");
	parameters.sceneCovarianceFile=lTree.get<string>("Input.SceneModelCovariance");
	parameters.featureFile=lTree.get<string>("Input.ImageFeatures");
	parameters.cameraFile=lTree.get<string>("Input.Camera","");
	parameters.gridSpacing=ReadParameter(lTree, "Input.GridSpacing", geq0, "is not >=0", optional<double>(defaultParameters.gridSpacing));

	if(lTree.get_child_optional("Input.RegionOfInterest"))
	{
		vector<double> boundingBox=ReadParameterArray<double>(lTree, "Input.RegionOfInterest", 4, [](const double&){return true;}, "", string(" "));
		parameters.roi=FrameT(Coordinate2DT(boundingBox[0], boundingBox[1]), Coordinate2DT(boundingBox[2], boundingBox[3]));
	}

	//Output
	parameters.outputPath=lTree.get<string>("Output.Root","");

	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("GeometryEstimator32ImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.cameraEstimateFile=lTree.get<string>("Output.Camera","");
	parameters.covarianceFile=lTree.get<string>("Output.Covariance","");
	parameters.correspondenceFile=lTree.get<string>("Output.Correspondences","");
	parameters.sceneInlierFile=lTree.get<string>("Output.SceneInliers","");
	parameters.imageInlierFile=lTree.get<string>("Output.ImageInliers","");
	parameters.logFile=lTree.get<string>("Output.Log","");
	parameters.flagVerbose=lTree.get<bool>("Output.FlagVerbose", defaultParameters.flagVerbose);

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool GeometryEstimator32ImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.pipelineParameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the camera, and determine the problem type
	vector<CameraC> tmpCamera=CameraIOC::ReadCameraFile(parameters.cameraFile);	//Known camera parameters
	if(tmpCamera.size()>1)
		throw(runtime_error(string("GeometryEstimator32ImplementationC::Run : ")+parameters.cameraFile+" must contain a single camera. Value=" + lexical_cast<string>(tmpCamera.size())));

	CameraC camera=tmpCamera[0];

	//Problem type
	map<ProblemIdentifierT, string> problemTypeToString{ {ProblemIdentifierT::P2P, "P2P"},  {ProblemIdentifierT::P2PF, "P2Pf"}, {ProblemIdentifierT::P3P, "P3P"}, {ProblemIdentifierT::P4P, "P4P"}, {ProblemIdentifierT::P6P, "P6P"}};
	optional<CameraMatrixT> initialEstimate;
	std::tie(parameters.problemType, initialEstimate)=ProcessCamera(camera);

	if(parameters.problemType!=ProblemIdentifierT::AUTO)
		if(!VerifyProblem(parameters.problemType, camera))
			throw(runtime_error( string("GeometryEstimator32ImplementationC::Run : Camera file is missing some parameters required by the problem. Value=") + problemTypeToString[parameters.problemType]));

	if(parameters.flagVerbose)
	{
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Problem type: "<<problemTypeToString[parameters.problemType]<<"\n";
		cout<<"              Initial estimate: "<<(initialEstimate ? "Yes" : "No")<<"\n";
	}	//if(parameters.flagVerbose)

	//Load the scene model

	vector<SceneFeatureC> sceneModel;
	vector<Matrix3d> sceneCovariance;
	optional<ProjectiveTransform3T> shifter;	//Transformation to shift the origin to the camera centre, for P2P and P2Pf
	std::tie(sceneModel, sceneCovariance, shifter)=LoadSceneFeatures(camera);

	if(sceneModel.empty())
		throw(runtime_error("GeometryEstimator32ImplementationC::Run : Empty scene model encountered. Terminating...\n"));

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Loaded a scene model with "<<sceneModel.size()<<" vertices.\n";

	//Load the image features

	vector<ImageFeatureC> imageFeatures;
	optional<AffineTransform2T> normaliser;
	LensDistortionC distortion;
	std::tie(imageFeatures, normaliser, distortion)=LoadImageFeatures(camera);

	if(imageFeatures.empty())
		throw(runtime_error("GeometryEstimator32ImplementationC::Run : Empty image feature list encountered. Terminating...\n"));

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Loaded "<<imageFeatures.size()<<" image features. Running the estimation pipeline...\n";

	//Normalise the image noise
	if(normaliser)
		parameters.noiseVariance*=pow<2>((0.5*(normaliser->matrix()(0,0)+normaliser->matrix()(1,1))));

	//Normalise the initial estimate
	if(normaliser && initialEstimate)
	{
		initialEstimate = normaliser->matrix() * (*initialEstimate);

		if(shifter)
			(*initialEstimate) *= shifter->matrix().inverse();	//shifter is valid only in the case of P2P and P2Pf
	}

	//Estimation pipeline

	CameraMatrixT model;
	optional<MatrixXd> covariance;
	CorrespondenceListT correspondences;
	GeometryEstimationPipelineDiagnosticsC diagnostics;

	if(parameters.problemType==ProblemIdentifierT::P2P)
		diagnostics=ComputeP2P(model, covariance, correspondences, initialEstimate, sceneModel, sceneCovariance, imageFeatures);

	if(parameters.problemType==ProblemIdentifierT::P2PF)
		diagnostics=ComputeP2Pf(model, covariance, correspondences, initialEstimate, sceneModel, sceneCovariance, imageFeatures, *normaliser);

	if(parameters.problemType==ProblemIdentifierT::P3P)
		diagnostics=ComputeP3P(model, covariance, correspondences, initialEstimate, sceneModel, sceneCovariance, imageFeatures);

	if(parameters.problemType==ProblemIdentifierT::P4P)
		diagnostics=ComputeP4P(model, covariance, correspondences, initialEstimate, sceneModel, sceneCovariance, imageFeatures, *normaliser);

	if(parameters.problemType==ProblemIdentifierT::P6P)
		diagnostics=ComputeP6P(model, covariance, correspondences, initialEstimate, sceneModel, sceneCovariance, imageFeatures);

	if(parameters.flagVerbose)
	{
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s:";

		if(!diagnostics.flagSuccess)
			cout<<" Geometry estimator failed!\n";
		else
		{
			cout<<"#Correspondences: "<<correspondences.size()<<" Error:"<<diagnostics.error<<"\n";
			cout<<" Result:\n"<<model<<"\n";
		}	//if(!diagnostics.flagSuccess)

	}	//if(parameters.flagVerbose)

	logger["Runtime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	//Output
	SaveOutput(sceneModel, imageFeatures, camera, model, covariance, correspondences, diagnostics, shifter, normaliser, distortion);

	return true;
}	//bool Run()


}	//AppGeometryEstimator32N
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("140401");
    std::string header("geometryEstimator32: 3D-2D registration");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppGeometryEstimator32N::GeometryEstimator32ImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
