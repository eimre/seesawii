/**
 * @file AppCalibrationVerification.cpp Implementation of \c calibrationVerification
 * @author Evren Imre
 * @date 3 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include <boost/format.hpp>
#include <boost/logic/tribool.hpp>
#include <omp.h>
#include <cmath>
#include <functional>
#include <fstream>
#include "../IO/CameraIO.h"
#include "../IO/ImageFeatureIO.h"
#include "../Elements/Camera.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Numeric/NumericFunctor.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../GeometryEstimationPipeline/CalibrationVerificationPipeline.h"
#include "../ApplicationInterface/InterfaceCalibrationVerificationPipeline.h"
#include "../ApplicationInterface/InterfaceUtility.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/BoostFilesystem.h"
#include "Application.h"

namespace SeeSawN
{
namespace AppCalibrationVerificationN
{

using namespace ApplicationN;

using boost::format;
using boost::str;
using boost::logic::tribool;
using boost::logic::indeterminate;
using std::bind;
using std::greater;
using std::ofstream;
using std::min;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::CalibrationVerificationPipelineC;
using SeeSawN::GeometryN::CalibrationVerificationPipelineDiagnosticsC;
using SeeSawN::GeometryN::CalibrationVerificationPipelineParametersC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::ApplicationN::ReadParameter;

/**
 * @brief Parameters for \c calibrationVerification
 * @ingroup Application
 */
struct AppCalibrationVerificationParametersC
{
	//General
	unsigned int nThreads;	///< Number of threads available to the application

	//Problem
	set<size_t> indexCorrect;	///< Indices of the cameras known to be correct

	//Pipeline
	CalibrationVerificationPipelineParametersC pipelineParameters;	///< Parameters for the calibration verification pipeline

	//Input
	string featureFilePattern;	///< Filename pattern for the feature files
	string cameraFile;	///< Camera filename
	double gridSpacing;	///< Minimum distance two neighbouring image features

	//Output
	string outputPath;	///< Output path
	string logFile;	///< Filename for the log file
	string resultFile;	///< Filename for the result
	bool flagVerbose;   ///< Verbosity flag

	/** @name Default constructor */
	AppCalibrationVerificationParametersC() : nThreads(1), gridSpacing(0), flagVerbose(true)
	{}
};	//AppCalibrationVerificationParametersC

/**
 * @brief Implementation of \c calibrationVerification
 * @ingroup Application
 */
class AppCalibrationVerificationImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed
		AppCalibrationVerificationParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string,double> logger;	///< Logs various values
		//@}

		/** @name Implementation details */ //@{
		static ptree PruneParameterTree(const ptree& src);	///< Removes the redundant parameters from a parameter tree

		tuple<vector<CameraMatrixT>, vector<LensDistortionC> > LoadCameras(const string& filename) const;	///< Loads the camera file
		vector<vector<ImageFeatureC> > LoadFeatures(const string& filename, const vector<LensDistortionC>& distortionList);	///< Loads the image features

		void SaveOutput(const vector<CalibrationVerificationPipelineC::result_type>& labels, const CalibrationVerificationPipelineDiagnosticsC& diagnostics);	///< Saves the output
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppCalibrationVerificationImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}

};	//AppCalibrationVerificationImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Removes the redundant parameters from the tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree AppCalibrationVerificationImplementationC::PruneParameterTree(const ptree& src)
{
	ptree output(src);

    if(src.get_child_optional("CalibrationVerification.Main.NoThreads"))
    	output.get_child("CalibrationVerification.Main").erase("NoThreads");

    if(src.get_child_optional("CalibrationVerification.Main.FlagVerbose"))
    	output.get_child("CalibrationVerification.Main").erase("FlagVerbose");

    return output;
}	//void PruneParameterTree(ptree& parameters)


/**
 * @brief Loads the camera file
 * @param[in] filename Camera filename
 * @return A tuple of vectors: Camera matrices and distortion objects
 * @throws runtime_error If no valid camera matrix any of the cameras present in the camera file
 */
tuple<vector<CameraMatrixT>, vector<LensDistortionC> > AppCalibrationVerificationImplementationC::LoadCameras(const string& filename) const
{
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(filename);
	size_t nCamera=cameraList.size();
	vector<CameraMatrixT> cameraMatrixList(nCamera);
	vector<LensDistortionC> distortionList(nCamera);
	for(size_t c=0; c<nCamera; ++c)
	{
		//Camera matrix

		optional<CameraMatrixT> mP=cameraList[c].MakeCameraMatrix();

		if(!mP)
			throw(runtime_error(string("AppCalibrationVerificationImplementationC::LoadCameras : Invalid camera matrix for the camera ")+lexical_cast<string>(c)+string(", with the tag ")+cameraList[c].Tag()));

		cameraMatrixList[c].swap(*mP);

		//Lens distortion

		optional<LensDistortionC> distortion=cameraList[c].Distortion();
		LensDistortionC dummy; dummy.modelCode=LensDistortionCodeT::NOD;

		distortionList[c] = distortion ? *distortion : dummy;
	}	//for(size_t c=0; c<nCamera; ++c)

	return make_tuple(cameraMatrixList, distortionList);
}	//vector<CameraMatrixT> LoadCameras(const string& filename);	///< Loads the camera files

/**
 * @brief Loads the image features
 * @param[in] filename Filename pattern
 * @param[in] distortionList Lens distortion list
 * @return A vector of feature files
 */
vector< vector<ImageFeatureC> > AppCalibrationVerificationImplementationC::LoadFeatures(const string& filename, const vector<LensDistortionC>& distortionList)
{
	size_t nCamera=distortionList.size();
	vector<vector<ImageFeatureC> > output(nCamera);

	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) num_threads(parameters.nThreads)
	for(c=0; c<nCamera; ++c)
	{
		string featureFile=str(format(filename)%c);
		vector<ImageFeatureC> featureList=ImageFeatureIOC::ReadFeatureFile(featureFile, true);

		optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
		optional<CoordinateTransformations2DT::affine_transform_type> normaliser;
		ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, parameters.gridSpacing, true, distortionList[c], normaliser);

	#pragma omp critical (ACVI_LF)
		{
			cout<<"Loaded "<<featureList.size()<<" features from Set "<<c<<"\n";
			output[c].swap(featureList);
		}
	}	//for(size_t c=0; c<nCamera; ++c)

	return output;
}	//vector<ImageFeatureC> LoadFeatures(const string& filename, const vector<LensDistortionC>& distortionList)

/**
 * @brief Saves the output
 * @param[in] labels Camera labels
 * @param[in] diagnostics diagnostics
 */
void AppCalibrationVerificationImplementationC::SaveOutput(const vector<CalibrationVerificationPipelineC::result_type>& labels, const CalibrationVerificationPipelineDiagnosticsC& diagnostics)
{
	const size_t iLabel=CalibrationVerificationPipelineC::iStatus;
	const size_t iConfidence=CalibrationVerificationPipelineC::iConfidence;

	if(!parameters.resultFile.empty())
	{
		ofstream result(parameters.outputPath+parameters.resultFile);
		if(result.fail())
			throw(runtime_error(string("AppCalibrationVerificationImplementationC::SaveOutput: Cannot open file ")+parameters.outputPath+parameters.resultFile));

		result<<"# Camera status; Confidence ([0,1])"<<"\n";
		size_t nCamera=labels.size();
		if(diagnostics.flagSuccess)
			for(size_t c=0; c<nCamera; ++c)
				result<<"Camera "<<c<<": "<< ( (get<iLabel>(labels[c])==true) ? "Correct " : ( (get<iLabel>(labels[c])==false) ? "Perturbed ":"Undetermined") ) <<" "<<get<iConfidence>(labels[c])<<"\n";
	}	//if(!parameters.resultFile.empty())

	if(!parameters.logFile.empty())
	{
		ofstream logfile(parameters.outputPath+parameters.logFile);
		if(logfile.fail())
			throw(runtime_error(string("AppCalibrationVerificationImplementationC::SaveOutput: Cannot open file ")+parameters.outputPath+parameters.logFile));

		string timeString= to_simple_string(second_clock::universal_time());
		logfile<<"CalibrationVerification log file created on "<<timeString<<"\n";

		if(diagnostics.flagSuccess)
		{
			logfile<<"Number of valid camera pairs: "<<diagnostics.nPairs<<"\n";
			logfile<<"Number of consistent camera pairs: "<<diagnostics.nConsistent<<"\n";
			logfile<<"Score: "<<diagnostics.bestScore<<"\n";
		}	//if(diagnostics.flagSuccess)

		logfile<<"Time Elapsed: "<<logger["TimeElapsed"];
	}	//if(!parameters.logFile.empty())

}	//void SaveOutput(const vector<tribool>& labels, const CalibrationVerificationPipelineDiagnosticsC& diagnostics)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppCalibrationVerificationImplementationC::AppCalibrationVerificationImplementationC()
{
	AppCalibrationVerificationParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")

	("CalibrationVerification.Main.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.seed)), "Seed for the random number generator. If <0, rng default")
	("CalibrationVerification.Main.UnperturbedCameras", value<string>()->default_value(""), "Indices of the cameras whose calibrations are known to be correct")

	("CalibrationVerification.Problem.NoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.noiseVariance)), "Variance of the noise on the image coordinates, in pixel^2. Affects the inlier threshold. >0")
	("CalibrationVerification.Problem.InitialEstimateNoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.initialEstimateNoiseVariance)),"Variance of the additive noise due to errors in the intial estimate, in multiples of the image noise variance. >=0")

	("CalibrationVerification.Decision.PairwiseDecisionThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.pairwiseDecisionTh)), "If the ratio of the total loss for the predicted fundamental matrix to that for the estimated is above this value, the pair contains at least one perturbed camera. >1")
	("CalibrationVerification.Decision.MinInlierRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.minInlierRatio)), "Minimum inlier ratio for a valid pair. [0,1]")
	("CalibrationVerification.Decision.MinSupport", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.minSupport)), "Minimum support size for a valid pair. >=0")
	("CalibrationVerification.Decision.MinPairwiseRelation", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.minPairwiseRelation)), "Minimum number of pairwise relations to determine whether a camera is perturbed. >=0")
	("CalibrationVerification.Decision.MaxPerturbed", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.maxPerturbed)), "Maximum number of perturbed cameras. >=0")

	("CalibrationVerification.GeometryEstimationPipeline.Main.InlierRejectionProbability", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.inlierRejectionProbability)), "The probability that an inlier correspondence is incorrectly rejected. Affects the inlier threshold. [0,1]")
	("CalibrationVerification.GeometryEstimationPipeline.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.maxIteration)), "Maximum number of guided matching iterations. >0")
	("CalibrationVerification.GeometryEstimationPipeline.Main.MinRelativeDifference", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.minRelativeDifference)), "If the relative error between two successive iterations is below this value, the procedure is terminated. >0")
	("CalibrationVerification.GeometryEstimationPipeline.Main.RefreshTreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.refreshThreshold)), "If the relative improvement in the number of inliers between the inliers to the current set and that of the reference is above this value, the reference set is updated. >=1")
	("CalibrationVerification.GeometryEstimationPipeline.Main.FlagCovariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.flagCovariance)), "If true, the algorithm calculates the covariance for the estimated parameters")

	("CalibrationVerification.GeometryEstimationPipeline.Decimation.BinDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.binDensity1)),"Number of bins per standard deviation. Affects the spatial quantisation level. >0")
	("CalibrationVerification.GeometryEstimationPipeline.Decimation.MaxObservationDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.maxObservationDensity)),"Observation set: Maximum number of correspondences per bin. >0")
	("CalibrationVerification.GeometryEstimationPipeline.Decimation.MinObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.minObservationRatio)),"Observation set: Minimum number of correspondences, in multiples of the minimal generator. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.Decimation.MaxObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.maxObservationRatio)),"Observation set: Maximum number of correspondences, in multiples of the minimal generator. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.Decimation.ValidationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.validationRatio)),"Validation set: Number of correspondences, in multiples of the minimal generator. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.Decimation.MaxAmbiguity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.maxAmbiguity)),"Maximum ambiguity for a correspondence in the observation or the validation set. [0,1]")

	("CalibrationVerification.GeometryEstimationPipeline.Matching.kNN.MinRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the best similarity score to the second best, for an admissible correspondence. Quality-quantity trade-off.[0,1]")
	("CalibrationVerification.GeometryEstimationPipeline.Matching.kNN.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.nnParameters.similarityTestThreshold)), "Minimum similarity for an admissible correspondence. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.Matching.kNN.FlagConsistencyTest", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.nnParameters.flagConsistencyTest)), "If true, each feature in a valid correspondence is among the best k candidates for the other")

	("EstimationPipeline.Matching.SpatialConsistency.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.flagBucketFilter)), "If true, the algorithm enforces a spatial consistency constraint between neighbouring correspondences")
	("EstimationPipeline.Matching.SpatialConsistency.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity for two spatial bins to be considered associated, relative to the maximum similarity. [0,1]")
	("EstimationPipeline.Matching.SpatialConsistency.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.minQuorum)), "If the number correspondences in a bin is below this value, it is held exempt from the spatial consistency check. >=0")
	("EstimationPipeline.Matching.SpatialConsistency.BucketDensity", value<string>()->default_value(lexical_cast<string>(1.0/defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.matcherParameters.bucketDimensions[0])), "Number of bins per standard deviation. >0")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.MinConfidence", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.minConfidence)), "Minimum probability that RANSAC finds an acceptable solution. (0,1]")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio)), "Maximum admissible distance between the true and the estimated position of an image feature correspondence, in multiples of the noise standard deviation. Affects the number of iterations. >0")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration)), "Maximum number of iterations. >=minIteration")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MinIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration)), "Minimum number of iterations. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minError)), "If the error for the solution is below this value, the process is terminated")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MaxError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxError)), "If the error for the solution is above this value, it is rejected. >=minError")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.nLOIterations)), "Number of local optimisation iterations. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.loGeneratorRatio1)), "Generator size for the LO iterations, in multiples of the minimal generator. Ignored if the solver cannot handle nonminimal sets. >=1")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagPROSAC)), "If true, PROSAC is enabled")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage1.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minError)), "If the error for the solution is below this value, the process is terminated")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations)), "Number of local optimisation iterations. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.loGeneratorRatio2)), "Generator size for the LO iterations, in multiples of the minimal generator. Ignored if the solver cannot handle nonminimal sets. >=1")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MaxNoCluster", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxSolution)), "Maximum number of solutions. >=1")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MinCoverage", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minCoverage)), "Minimum percentage validation support a model should achieve to start a new cluster. [0,1]")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MinOverlapIndex", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minModelSimilarity)), "Minimum overlap index between the inlier sets of two models, for a valid association. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.ParsimonyFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.parsimonyFactor)), "A new cluster is created only if the new total inlier count relative to the current is above this value . >=1")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.DiversityFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.diversityFactor)), "A MORANSAC action is rejected if the new total inlier count relative to the current is below this value. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MaxAlgebraicDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.moMaxAlgebraicDistance)), "Number of local optimisation iterations. >=0")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagPROSAC)), "If true, PROSAC is enabled")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Stage2.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration)), "Maximum number of iterations. >=0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.Epsilon1", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.pdlParameters.epsilon1)), "Gradient convergence criterion, as a function of the magnitude of the gradient vector. >0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.Epsilon2", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.pdlParameters.epsilon2)), "Solution convergence criterion, as the relative distance between two successive updates. >0")

	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Refinement.TrustRegion.Delta0", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.pdlParameters.delta0)), "Initial size of the trust region. >0")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Refinement.TrustRegion.LambdaShrink", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.pdlParameters.lambdaShrink)), "Shrinkage rate for the trust region. (0,1)")
	("CalibrationVerification.GeometryEstimationPipeline.GeometryEstimation.Refinement.TrustRegion.LambdaGrowth", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.pdlParameters.lambdaGrowth)), "Growth rate for the trust region. >1")

	("Input.FeatureFilename", value<string>(), "Feature filename, with a single printf-like token (e.g. feature%02d.ft2)")
	("Input.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.gridSpacing)), "Minimum distance between two image features, in pixels. >=0")
	("Input.CameraFile", value<string>(), "Camera filename")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Result", value<string>()->default_value(""), "Result file")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output")
	;
}	//AppSparseModelBuilderImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppCalibrationVerificationImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppCalibrationVerificationImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
    ptree tree; //Options tree
	AppCalibrationVerificationParametersC defaultParameters;

    tree.put("xmlcomment", "calibrationVerification configuration file");

    tree.put("General","");
    tree.put("General.NoThreads", defaultParameters.nThreads);

    tree.put("CalibrationVerification","");

    tree.put("Input","");
    tree.put("Input.FeatureFilename","%03d.ft2");

    if(detailLevel>1)
        tree.put("Input.GridSpacing",defaultParameters.gridSpacing);

    tree.put("Input.CameraFile","cameras.cam");

    tree.put("Output","");
    tree.put("Output.Root", "/home");
    tree.put("Output.Result","result.txt");
    tree.put("Output.Log","log.log");
    tree.put("Output.Verbose", defaultParameters.flagVerbose);

    ptree cvTree=InterfaceCalibrationVerificationPipelineC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel);

    if(detailLevel<=1)
        if(cvTree.get_child_optional("GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC"))
            cvTree.get_child("GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");

    cvTree.put("Main.UnperturbedCameras", "");

    tree.put_child("CalibrationVerification", cvTree);

    tree=PruneParameterTree(tree);

    xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
   	write_xml(filename, tree, locale(), settings);
}	// void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppCalibrationVerificationImplementationC::SetParameters(const ptree& tree)
{
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);

	ptree lTree=PruneParameterTree(tree);
	AppCalibrationVerificationParametersC defaultParameters;

	parameters.nThreads=ReadParameter(lTree, "General.NoThreads", geq1, "is not >=1", optional<double>(defaultParameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	if(lTree.get_child_optional("CalibrationVerification"))
		parameters.pipelineParameters=InterfaceCalibrationVerificationPipelineC::MakeParameterObject(lTree.get_child("CalibrationVerification"));

	parameters.pipelineParameters.nThreads=parameters.nThreads;
	vector<unsigned int> indexCorrect=Tokenise<unsigned int>(lTree.get<string>("CalibrationVerification.Main.UnperturbedCameras",""), " ");
	parameters.indexCorrect.insert(indexCorrect.begin(), indexCorrect.end());

	parameters.featureFilePattern=lTree.get<string>("Input.FeatureFilename");
	parameters.gridSpacing=ReadParameter(lTree, "Input.GridSpacing", geq0, "is not >=0", optional<double>(defaultParameters.gridSpacing));
	parameters.cameraFile=lTree.get<string>("Input.CameraFile");

	parameters.outputPath=lTree.get<string>("Output.Root");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppCalibrationVerificationImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.logFile=lTree.get<string>("Output.Log","");
	parameters.resultFile=lTree.get<string>("Output.Result","");
	parameters.flagVerbose=lTree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);
	parameters.pipelineParameters.flagVerbose=parameters.flagVerbose;

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppCalibrationVerificationImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the cameras
	vector<CameraMatrixT> cameraList;
	vector<LensDistortionC> distortionList;
	tie(cameraList, distortionList)=LoadCameras(parameters.cameraFile);

	size_t nCamera=cameraList.size();

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<nCamera<<" cameras.\n";

	//Load the feature files
	vector<vector<ImageFeatureC> > featureStack=LoadFeatures(parameters.featureFilePattern, distortionList);

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Running the calibration verification pipeline...\n";

	//Run the pipeline

	ReciprocalC<EuclideanDistanceIDT::result_type> inverter;
	InverseEuclideanIDConverterT similarityMetric(EuclideanDistanceIDT(), inverter);

	vector<CalibrationVerificationPipelineC::result_type> labels;
	CalibrationVerificationPipelineDiagnosticsC diagnostics=CalibrationVerificationPipelineC::Run(labels, cameraList, featureStack, parameters.indexCorrect, similarityMetric, parameters.pipelineParameters);

	if(parameters.flagVerbose)
	{
		if(diagnostics.flagSuccess)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Operation successful. Saving the output...\n";
		else
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Operation failed. Terminating...\n";
	}	//if(parameters.flagVerbose)

	//Output

	logger["TimeElapsed"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;
	SaveOutput(labels, diagnostics);

	if(!diagnostics.flagSuccess)
		return false;


	if(parameters.flagVerbose)
		for(size_t c=0; c<nCamera; ++c)
		{
			tribool currentLabel;
			double currentConfidence;
			tie(currentLabel, currentConfidence)=labels[c];

			cout<<"Camera "<<c<<": "<< ( (currentLabel==true) ? "Correct " : ( (currentLabel==false) ? "Perturbed ":"Undetermined") ) <<" "<<currentConfidence<<"\n";
		}

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Terminating...\n";

	return true;
}	//bool Run()

}	//AppCalibrationVerificationN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("141003");
    std::string header("calibrationVerification: Verification of the validity of the calibration of a multicamera setup");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppCalibrationVerificationN::AppCalibrationVerificationImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
