/**
 * @file AppPfM.cpp Implementation of \c pfm , the panorama-from-motion application
 * @author Evren Imre
 * @date 18 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/optional.hpp>
#include <boost/format.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <map>
#include <functional>
#include "Application.h"
#include "../ApplicationInterface/InterfacePfMPipeline.h"
#include "../GeometryEstimationPipeline/PfMPipeline.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../IO/CameraIO.h"
#include "../IO/ImageFeatureIO.h"
#include "../Elements/Camera.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace AppPfMN
{

using namespace SeeSawN::ApplicationN;

using boost::optional;
using boost::str;
using boost::format;
using Eigen::RowVector3d;
using std::greater;
using std::greater_equal;
using std::bind;
using std::map;
using SeeSawN::ApplicationN::InterfacePfMPipelineC;
using SeeSawN::GeometryN::PfMPipelineC;
using SeeSawN::GeometryN::PfMPipelineParametersC;
using SeeSawN::GeometryN::PfMPipelineDiagnosticsC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::BinaryImageFeatureIOC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::ComposeCameraMatrix;


/**
 * @brief Parameters for \c pfm
 * @ingroup Application
 */
struct AppPfMParametersC
{
	//General
	unsigned int nThreads;	///< Number of threads available to the application
	unsigned int seed;	 ///< Random number seed. For default, 0

	//Problem
	double noiseVariance;	///< Variance of the noise on the image coordinates, >0

	//Pipeline
	PfMPipelineParametersC pipelineParameters;	///< PfM pipeline parameters

	//Input
	string featureFilePattern;	///< Filename pattern for the feature files
	string cameraFile;	///< Camera filename
	double gridSpacing;	///< Minimum distance two neighbouring image features

	//Output
	string outputPath;	///< Output path
	string calibrationFile;	///< Filename for the estimated cameras
	string panoramaFile;	///< Filename for the panorama
	string logFile;	///< Filename for the log file
	bool flagVerbose;   ///< Verbosity flag

	AppPfMParametersC() : nThreads(1), seed(0), noiseVariance(1), gridSpacing(0), flagVerbose(true)
	{}
};	//AppPfMParametersC

/**
 * @brief Implementation of \c pfm
 * @ingroup Application
 */
class AppPfMImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed
		AppPfMParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string,double> logger;	///< Logs various values
		//@}

		/** @name Implementation details */ //@{
		static ptree PrunePfMPipeline(const ptree& src);	///< Removes the redundant PfM parameters
		vector<vector<ImageFeatureC>> LoadFeatures(const vector<CameraC>& cameraList);	///< Loads the features

		void SaveOutput(const map<size_t, CameraMatrixT>& estimatedCameras, const vector<Coordinate3DT>& panorama, const vector<CameraC>& originalCameras);	///< Saves the output
		void SaveLog(const PfMPipelineDiagnosticsC& diagnostics);	///< Saves the log
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppPfMImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//AppPfMImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Saves the output
 * @param[in] estimatedCameras Estimated cameras
 * @param[in] panorama Panorama
 * @param[in] originalCameras Original cameras
 */
void AppPfMImplementationC::SaveOutput(const map<size_t, CameraMatrixT>& estimatedCameras, const vector<Coordinate3DT>& panorama, const vector<CameraC>& originalCameras)
{
	if(!parameters.cameraFile.empty())
	{
		//Camera map for the original cameras
		size_t nOriginal=originalCameras.size();
		map<size_t, CameraC> mapOriginalCameras;
		for(size_t c=0; c<nOriginal; ++c)
			mapOriginalCameras[c]=originalCameras[c];

		//Camera matrix to camera conversion
		size_t nEstimated=estimatedCameras.size();
		size_t index=0;
		vector<CameraC> output(nEstimated);
		for(const auto& current : estimatedCameras)
		{
			output[index]=mapOriginalCameras[current.first];

			Coordinate3DT vC;
			RotationMatrix3DT mR;
			IntrinsicCalibrationMatrixT mK;
			tie(mK, vC, mR)=DecomposeCamera(current.second);
			output[index].SetIntrinsics(mK);
			output[index].SetExtrinsics(ComposeCameraMatrix(vC, mR));
			output[index].Tag()=lexical_cast<string>(current.first);

			++index;
		}	//for(const auto& current : cameras)

		CameraIOC::WriteCameraFile(output, parameters.outputPath+"/"+parameters.calibrationFile);
	}	//if(!parameters.cameraFile.empty())

	if(!parameters.panoramaFile.empty())
	{
		string filename=parameters.outputPath+"/"+parameters.panoramaFile;
		ofstream panoramaFile(filename);

		if(panoramaFile.fail())
			throw(runtime_error(string("AppPfM::SaveOutput : Failed to open the file for writing at ")+filename));

		typedef SeeSawN::ION::ArrayIOC<RowVector3d> ArrayIOT;

		for(const auto& current : panorama)
			ArrayIOT::WriteArray(panoramaFile, current.transpose());
	}	//if(!parameters.panoramaFile.empty())
}	//void SaveOutput(const map<size_t, CameraMatrixT>& cameras)

/**
 * @brief Saves the log
 * @param[in] diagnostics Diagnostics
 */
void AppPfMImplementationC::SaveLog(const PfMPipelineDiagnosticsC& diagnostics)
{
	if(parameters.logFile.empty())
		return;

	string filename=parameters.outputPath+"/"+parameters.logFile;
	ofstream logFile(filename);

	if(logFile.fail())
		throw(runtime_error(string("AppPfM::SaveLog : Failed to open the file for writing at ")+filename));

	string timeString= to_simple_string(second_clock::universal_time());
	logFile<<"pfm log file created on "<<timeString<<"\n";

	logFile<<"Number of cameras: "<<logger["NCameras"]<<"\n";
	logFile<<"Number of registered cameras: "<<diagnostics.nRegistered<<"\n";
	logFile<<"Number of points on the unit sphere: "<<diagnostics.sPointCloud<<"\n";
	logFile<<"Number of inlier edges on the vision graph: "<<diagnostics.nInlierEdge<<"\n";
	logFile<<"Execution time: "<<logger["ExecutionTime"]<<"s\n";
}	//void SaveLog(const PfMPipelineDiagnosticsC& diagnostics)

/**
 * @brief Removes the redundant PfM parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree AppPfMImplementationC::PrunePfMPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PrunePfMPipeline(const ptree& src)

/**
 * @brief Loads the features
 * @param[in] cameraList Cameras
 * @return A vector of image feature vectors
 * @remarks The features are undistorted, but not normalised
 */
vector<vector<ImageFeatureC>> AppPfMImplementationC::LoadFeatures(const vector<CameraC>& cameraList)
{
	size_t nCamera=cameraList.size();
	vector<vector<ImageFeatureC> > output(nCamera);

	size_t c=0;
	LensDistortionC noDistortion; noDistortion.modelCode=LensDistortionCodeT::NOD;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) num_threads(parameters.nThreads)
	for(c=0; c<nCamera; ++c)
	{
		string featureFile=str(format(parameters.featureFilePattern)%c);
		vector<ImageFeatureC> featureList=ImageFeatureIOC::ReadFeatureFile(featureFile, true);

		//Lens distortion
		LensDistortionC distortion = cameraList[c].Distortion() ? *cameraList[c].Distortion() : noDistortion;

		optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
		optional<CoordinateTransformations2DT::affine_transform_type> normaliser;
		ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, parameters.gridSpacing, true, distortion, normaliser);

	#pragma omp critical (AMSfM_LF)
		{
			cout<<"Loaded "<<featureList.size()<<" features from Set "<<c<<"\n";
			output[c].swap(featureList);
		}
	}	//for(; c<nCamera; ++c)

	return output;
}	//vector<vector<ImageFeatureC>> LoadFeatures(const vector<CameraC>& cameraList);

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppPfMImplementationC::AppPfMImplementationC()
{
	AppPfMParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the application. >0" )
	("General.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.seed)), "RNG seed. 0 for rng default" )

	("Input.FeatureFilename", value<string>(), "Feature filename, with a single printf-like token (e.g. feature%02d.ft2)")
	("Input.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.gridSpacing)), "Minimum distance between two image features, in pixels. >=0")
	("Input.CameraFile", value<string>(), "Camera filename")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Calibration", value<string>(), "Estimated camera calibration parameters")
	("Output.Panorama", value<string>(), "Estimated panorama")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output");
}	//AppMultiviewSfMImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppPfMImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppPfMImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppPfMParametersC defaultParameters;

	tree.put("xmlcomment", "pfm configuration file");

	tree.put("General","");
	tree.put("Pipeline","");
	tree.put("Input","");
	tree.put("Output","");

	tree.put("General.NoThreads", defaultParameters.nThreads);
	tree.put("General.Seed", defaultParameters.seed);

	tree.put("Input","");
	tree.put("Input.FeatureFilename","%03d.ft2");
	tree.put("Input.CameraFile","cameras.cam");

	if(detailLevel>1)
        tree.put("Input.GridSpacing",defaultParameters.gridSpacing);

	tree.put("Output.Root", "/home/");
	tree.put("Output.Calibration","calibration.cal");
	tree.put("Output.Panorama","panorama.asc");
	tree.put("Output.Log","log.log");
	tree.put("Output.Verbose", defaultParameters.flagVerbose);

	tree.put_child("Pipeline", PrunePfMPipeline(InterfacePfMPipelineC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel)));

	if(detailLevel<=1)
	{
	    if(tree.get_child_optional("Pipeline.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC") )
	        tree.get_child("Pipeline.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");

	    if(tree.get_child_optional("Pipeline.EstimateRefinement.RANSAC.Stage2.MORANSAC") )
	        tree.get_child("Pipeline.EstimateRefinement.RANSAC.Stage2").erase("MORANSAC");
	}
	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppPfMImplementationC::SetParameters(const ptree& tree)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);

	ptree lTree=tree;
	AppPfMParametersC defaultParameters;

	//General
	parameters.nThreads=ReadParameter(lTree, "General.NoThreads", gt0, "is not >0", optional<double>(defaultParameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	parameters.seed=lTree.get<unsigned int>("General.Seed", parameters.seed);

	//Pipeline
	parameters.pipelineParameters=InterfacePfMPipelineC::MakeParameterObject(PrunePfMPipeline(lTree.get_child("Pipeline")));
	parameters.pipelineParameters.nThreads=parameters.nThreads;

	//Input
	parameters.featureFilePattern=lTree.get<string>("Input.FeatureFilename");
	parameters.gridSpacing=ReadParameter(lTree, "Input.GridSpacing", geq0, "is not >=0", optional<double>(defaultParameters.gridSpacing));
	parameters.cameraFile=lTree.get<string>("Input.CameraFile");

	//Output
	parameters.outputPath=lTree.get<string>("Output.Root");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppPfMImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.calibrationFile=lTree.get<string>("Output.Calibration","");
	parameters.panoramaFile=lTree.get<string>("Output.Panorama","");
	parameters.logFile=lTree.get<string>("Output.Log","");
	parameters.flagVerbose=lTree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);

	return true;
}	//bool AppIntrinsicsEstimatorImplementationC::SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppPfMImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the cameras
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.cameraFile);
	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<cameraList.size()<<" cameras.\n";

	//Load the feature files
	vector<vector<ImageFeatureC> > featureStack=LoadFeatures(cameraList);
	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Loaded the image features. Running the PfM pipeline...\n";

	//RNG
	PfMPipelineC::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	map<size_t, CameraMatrixT> result;
	vector<Coordinate3DT> panorama;
	PfMPipelineDiagnosticsC diagnostics=PfMPipelineC::Run(result, panorama, rng, featureStack, cameraList, parameters.pipelineParameters);

	logger["NCameras"]=featureStack.size();
	logger["ExecutionTime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Finished.\n";

	SaveOutput(result, panorama, cameraList);
	SaveLog(diagnostics);

	return true;
}	//bool Run()
}	//namespace AppPfMN
}	//namespace SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("160518");
    std::string header("pfm: Calibration of a nodal camera sequence with binary image features");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppPfMN::AppPfMImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
