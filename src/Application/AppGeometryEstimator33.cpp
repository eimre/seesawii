/**
 * @file AppGeometryEstimator33.cpp Implementation of the 3D pairwise geometry estimator
 * @author Evren Imre
 * @date 1 Apr 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/optional.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include "Application.h"
#include "../GeometryEstimationComponents/Homography3DComponents.h"
#include "../GeometryEstimationComponents/Rotation3DComponents.h"
#include "../GeometryEstimationComponents/Similarity3DComponents.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../IO/ArrayIO.h"
#include "../IO/SpecialArrayIO.h"
#include "../IO/SceneFeatureIO.h"
#include "../IO/CoordinateCorrespondenceIO.h"


//DOCUMENTME
//Usage notes:
//Available point covariances may lead to a very high inlier threshold, reducing the accuracy of the estimated models
//Homography3D seems to benefit from longer PDL runs
namespace SeeSawN
{
namespace AppGeometryEstimator33N
{

using namespace ApplicationN;

using boost::optional;
using boost::math::pow;
using boost::math::chi_squared;
using boost::math::cdf;
using Eigen::MatrixXd;
using Eigen::Matrix4d;
using Eigen::Vector3d;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineHomography3DProblem;
using SeeSawN::GeometryN::Homography3DPipelineProblemT;
using SeeSawN::GeometryN::Homography3DPipelineT;
using SeeSawN::GeometryN::Homography3DEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACHomography3DProblem;
using SeeSawN::GeometryN::RANSACHomography3DProblemT;
using SeeSawN::GeometryN::MakePDLHomography3DProblem;
using SeeSawN::GeometryN::PDLHomography3DProblemT;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineRotation3DProblem;
using SeeSawN::GeometryN::Rotation3DPipelineProblemT;
using SeeSawN::GeometryN::Rotation3DPipelineT;
using SeeSawN::GeometryN::Rotation3DEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACRotation3DProblem;
using SeeSawN::GeometryN::RANSACRotation3DProblemT;
using SeeSawN::GeometryN::MakePDLRotation3DProblem;
using SeeSawN::GeometryN::PDLRotation3DProblemT;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineSimilarity3DProblem;
using SeeSawN::GeometryN::Similarity3DPipelineProblemT;
using SeeSawN::GeometryN::Similarity3DPipelineT;
using SeeSawN::GeometryN::Similarity3DEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACSimilarity3DProblem;
using SeeSawN::GeometryN::RANSACSimilarity3DProblemT;
using SeeSawN::GeometryN::MakePDLSimilarity3DProblem;
using SeeSawN::GeometryN::PDLSimilarity3DProblemT;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::CoordinateStatistics3DT;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::VolumeT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::MakeFeatureCorrespondenceList;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
using SeeSawN::ElementsN::FindContained;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ION::ReadCovFile;
using SeeSawN::ION::SceneFeatureIOC;
using SeeSawN::ION::CoordinateCorrespondenceIOC;

/**
 * @brief Parameter object for GeometryEstimator33ImplementationC
 * @ingroup Application
 */
struct GeometryEstimator33ParametersC
{
	enum class Geometry33ProblemT{HMG3, ROT3, SIM3};	//3D geometry problem types: Homography, rotation , similarity transformation

	unsigned int nThreads;  ///< Number of threads available to the program
	int seed;	///< RNG seed
	Geometry33ProblemT problemType;	///< Type of the problem
	string initialEstimateFile;	///< Filename for the initial estimate
	double initialEstimateError;	///< Additional variance of the noise on the scene coordinates, due to the errors in the initial estimate. A scale factor to the noise variance
	bool flagInitialEstimate;	///< \c true if an initial estimate exists

	double binDensity;	///< Number of bins per standard deviation, for spatial quantisation

	double pValue;	///< Probability of rejecting an inlier

	double maxPerturbation;	///< Maximum Euclidean distance between the true and the measured coordinates of a correspondence. A scale factor to the noise std

	double maxAlgebraicDistance;	///< If the algebraic distance between two models is below this value, they are deemed similar. Percentage of the maximum distance.
	double loGeneratorRatio1;	///< Ratio of the cardinality of the LO generator to that of the minimal generator, first stage
	double loGeneratorRatio2;	///< Ratio of the cardinality of the LO generator to that of the minimal generator, second stage

	string featureFile1;   ///< Filename for the first feature file
	optional<double> noiseVariance1;	///< Variance of the noise on the scene coordinates of the first set
	string inputCovarianceFile1;	///< Filename for the coordinate covariances of the first set
	optional<VolumeT> boundingBox1;	///< Bounding box for the first point cloud

	string featureFile2;   ///< Filename for the second feature file
	optional<double> noiseVariance2;	///< Variance of the noise on the scene coordinates of the second set
	string inputCovarianceFile2;	///< Filename for the coordinate covariances of the second set
	optional<VolumeT> boundingBox2;	///< Bounding box for the first point cloud

	string outputPath;	///< Output path
	string modelFile;	///< Geometric model
	string covarianceFile;	///< Covariance of the estimated model
	string correspondenceFile;	///< Correspondences
	string correspondingFeatureFile1;	///< Corresponding features in the first set
	string correspondingFeatureFile2;	///< Corresponding features in the second set
	string logFile;	///< Log
	bool flagVerbose;   ///< Verbosity flag

	GeometryEstimationPipelineParametersC pipelineParameters;	///< Parameters for the geometry estimation pipeline
};	//struct GeometryEstimator33ParametersC

/**
 * @brief Implementation of GeometryEstimtor33
 * @ingroup Application
 * @nosubgrouping
 */
class GeometryEstimator33ImplementationC
{
	private:

    	auto_ptr<options_description> commandLineOptions; ///< Command line options
                                                      	  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

    	/** @name Configuration */ //@{
    	GeometryEstimator33ParametersC parameters;  ///< Application parameters
    	//@}

    	typedef SceneFeatureC FeatureT;	///< Feature type
		typedef FeatureT::coordinate_type CoordinateT;	///< Coordinate type
		typedef Homography3DPipelineProblemT::covariance_type1 CoordinateCovarianceT;	///< Coordinate covariance type

		typedef GeometryEstimator33ParametersC::Geometry33ProblemT Geometry33ProblemT;

		/** @name State */ //@{
		map<string, double> logger;	///< Collects various statistics for the log
		//@}

    	/** @name Implementation details */ //@{
    	tuple<vector<FeatureT>, optional<vector<CoordinateCovarianceT> > > LoadFeatures(unsigned int setId) const;	///< Loads and preprocesses the features
    	void InitialiseCovariance(optional<vector<CoordinateCovarianceT> >& covarianceList, optional<double>& noiseVariance);	///< Initialises the covariance parameters

    	typedef RANSACHomography3DProblemT::dimension_type1 dimension_type;
    	tuple<dimension_type, dimension_type> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const;	///< Computes the bin sizes for RANSAC

    	GeometryEstimationPipelineDiagnosticsC ComputeHomography3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2);	///< Runs the 3D homography estimation pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeRotation3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2);	///< Runs the 3D rotation estimation pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeSimilarity3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2);	///< Runs the 3D similarity transformation estimation pipeline

    	void SaveOutput(vector<FeatureT>& featureList1, vector<FeatureT>& featureList2, const Matrix4d& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics);	///< Saves the output
    	//@}

	public:

    	/**@name ApplicationImplementationConceptC interface */ //@{
    	GeometryEstimator33ImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
    	//@}
};	//class GeometryEstimator33ImplementationC

/**
 * @brief Loads and preprocesses the features
 * @param[in] setId Set id. 1 or 2
 * @return A tuple: [Features, coordinate covariances]
 * @throws If the number of covariance matrices does not match that of the features
 */
auto GeometryEstimator33ImplementationC::LoadFeatures(unsigned int setId) const -> tuple<vector<FeatureT>, optional<vector<CoordinateCovarianceT> > >
{
	string featureFilename = (setId==1) ? parameters.featureFile1 : parameters.featureFile2;
	string covarianceFilename = (setId==1) ? parameters.inputCovarianceFile1 : parameters.inputCovarianceFile2;
	optional<VolumeT> boundingBox = (setId==1) ? parameters.boundingBox1 : parameters.boundingBox2;	//Volume-of-interest

	tuple<vector<FeatureT>, optional<vector<CoordinateCovarianceT> > > output;

	//Load the features and the covariances
	vector<FeatureT> featureList;
	vector<CoordinateCovarianceT> covarianceList;
	bool flagCov=false;

#pragma omp critical (GE33_LF1)
	{
	featureList=SceneFeatureIOC::ReadFeatureFile(featureFilename);
	if(!covarianceFilename.empty())
	{
		vector<CoordinateT> dummy;	//Ignored
		tie(dummy, covarianceList)=ReadCovFile<ValueTypeM<CoordinateT>::type,3>(covarianceFilename);

		if(featureList.size()!=covarianceList.size())
			throw(invalid_argument("GeometryEstimator33ImplementationC::LoadFeatures : The number of entries in the feature and the covariance files do not match. Values: "+lexical_cast<string>(featureList.size())+"/"+lexical_cast<string>(covarianceList.size())));

		get<1>(output)=vector<CoordinateCovarianceT>();
		flagCov=true;
	}	//if(!parameters.covarianceFile.empty())
	}

	SceneFeatureIOC::PreprocessDescriptors(featureList,true);
	//No bounding box
	if(!boundingBox)
	{
		get<0>(output).swap(featureList);

		if(flagCov)
		{
			get<1>(output)=vector<CoordinateCovarianceT>();
			get<1>(output)->swap(covarianceList);
		}
		return output;
	}	//if(!boundingBox)

	//Impose the bounding box constraint
	vector<size_t> indexIn=FindContained(featureList, *boundingBox);
	size_t nIn=indexIn.size();

	get<0>(output).reserve(nIn);
	if(flagCov) get<1>(output)->reserve(nIn);

	for(size_t c=0; c<nIn; ++c)
	{
		get<0>(output).push_back(featureList[c]);

		if(flagCov)
			get<1>(output)->push_back(covarianceList[c]);
	}	//for(size_t c=0; c<nIn; ++c)

	return output;
}	//tuple<vector<FeatureT>, vector<CoordinateCovarianceT>, optional<Matrix4d> > LoadFeatures(unsigned int setId) const

/**
 * @brief Initialises the covariance parameters
 * @param[in,out] covarianceList Covariance list. Invalid if no covariance file is specified.
 * @param[in,out] noiseVariance Noise covariance
 * @remarks Operation
 * 	- If \c noiseVariance is uninitialised, it is estimated from \c covarianceList , as first computing the median for each axis, and then assigning the max
 * 	- If \c covarianceList is uninitialised, it is estimated from \c noiseVariance . The output is a 1-element list.
 * 	- If neither of the parameters are uninitialised, throw an exception
 * 	@throws invalid_argument If covariance initialisation fails
 */
void GeometryEstimator33ImplementationC::InitialiseCovariance(optional<vector<CoordinateCovarianceT> >& covarianceList, optional<double>& noiseVariance)
{
	if(!noiseVariance && (!covarianceList || covarianceList->empty() ) )
		throw(invalid_argument("GeometryEstimator33ImplementationC::InitialiseCovariance : Noise variance must be specified."));

	if(noiseVariance && !covarianceList)
	{
		covarianceList=vector<CoordinateCovarianceT>(1);
		(*covarianceList)[0]=CoordinateCovarianceT::Identity()* (*noiseVariance);
	}	//if(noiseCovariance && !covarianceList)

	if(!noiseVariance && covarianceList)
	{
		//Compute the median for each dimension
		vector<set<ValueTypeM<CoordinateT>::type > > medianCalculator(3);
		for(const auto& current : *covarianceList)
		{
			medianCalculator[0].insert(current(0,0));
			medianCalculator[1].insert(current(1,1));
			medianCalculator[2].insert(current(2,2));
		}

		size_t i= covarianceList->size()/2;
		Vector3d buffer( *next(medianCalculator[0].begin(),i), *next(medianCalculator[1].begin(),i), *next(medianCalculator[2].begin(),i) );

		noiseVariance=buffer.maxCoeff();
	}	//if(!noiseCovariance && covarianceList)
}	//void InitialiseCovariance(const optional<Matrix4d>& normaliser, optional<vector<CoordinateCovarianceT> >& covarianceList, optional<double>& noiseCovariance)

/**
 * @brief Computes the bin sizes for RANSAC
 * @param[in] featureList1 First feature list
 * @param[in] featureList2 Second feature list
 * @pre \c featureList1 is nonempty
 * @pre \c featureList2 is nonempty
 * @return A pair of bins. [First set; Second set]
 */
auto GeometryEstimator33ImplementationC::ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const -> tuple<dimension_type, dimension_type>
{
	//Preconditions
	assert(!featureList1.empty());
	assert(!featureList2.empty());

	vector<CoordinateT> coordinateList1=MakeCoordinateVector(featureList1);
	dimension_type binSize1=*ComputeBinSize<CoordinateT>(coordinateList1, parameters.binDensity);

	vector<CoordinateT> coordinateList2=MakeCoordinateVector(featureList2);
	dimension_type binSize2=*ComputeBinSize<CoordinateT>(coordinateList2, parameters.binDensity);

	return make_tuple(binSize1, binSize2);
}	//tuple<dimension_type1, dimension_type2> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const

/**
 * @brief Saves the output
 * @param[in, out] featureList1 Feature list for the first image
 * @param[in, out] featureList2 Feature list for the second image
 * @param[in] model Estimated model
 * @param[in] covariance Model covariance. Invalid, if not estimated
 * @param[in] inliers Inlier indices
 * @param[in] diagnostics Geometry estimation pipeline diagnostics
 */
void GeometryEstimator33ImplementationC::SaveOutput(vector<FeatureT>& featureList1, vector<FeatureT>& featureList2, const Matrix4d& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics)
{
	//Model
	if(diagnostics.flagSuccess && !parameters.modelFile.empty())
		ArrayIOC<Matrix4d>::WriteArray(parameters.outputPath+parameters.modelFile, model);

	//Covariance
	if(diagnostics.flagSuccess && covariance && !parameters.covarianceFile.empty())
		ArrayIOC<MatrixXd>::WriteArray(parameters.outputPath+parameters.covarianceFile, *covariance);

	//Log
	if(!parameters.logFile.empty())
	{
		string filename = parameters.outputPath+parameters.logFile;
		ofstream logfile(filename);

		string timeString= to_simple_string(second_clock::universal_time());
		logfile<<"GeometryEstimator33 log file created on "<<timeString<<"\n";
		logfile<<"Number of features in the first set: "<<featureList1.size()<<"\n";
		logfile<<"Number of features in the second set: "<<featureList2.size()<<"\n";
		logfile<<"Number of correspondences:"<<inliers.size()<<"\n";
		logfile<<"Effective noise variance (m^2): "<<logger["EffectiveNoiseVariance"]<<"\n";

		if(diagnostics.flagSuccess)
		{
			logfile<<"Number of guided matching iterations: "<<diagnostics.nIteration<<"\n";
			logfile<<"Error: "<<diagnostics.error<<"\n";
		}	//if(diagnostics.flagSuccess)

		logfile<<"Run time: "<<logger["Runtime"]<<"s";
	}	//if(!parameters.logFile.empty())

	 //Correspondences
	if(!parameters.correspondenceFile.empty())
	{
		vector<CoordinateT> coordinates1=MakeCoordinateVector(featureList1);
		vector<CoordinateT> coordinates2=MakeCoordinateVector(featureList2);
	    CoordinateCorrespondenceList3DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList3DT>(inliers, coordinates1, coordinates2);
	    CoordinateCorrespondenceIOC::WriteCorrespondenceFile(coordinateCorrespondences, parameters.outputPath+parameters.correspondenceFile);
	}	//if(!parameters.correspondenceFile.empty())

	if(!parameters.correspondingFeatureFile1.empty() || !parameters.correspondingFeatureFile2.empty())
	{
		MakeFeatureCorrespondenceList(featureList1, featureList2, inliers);

		if(!parameters.correspondingFeatureFile1.empty())
    		SceneFeatureIOC::WriteFeatureFile(featureList1, parameters.outputPath+parameters.correspondingFeatureFile1);

		if(!parameters.correspondingFeatureFile2.empty())
			SceneFeatureIOC::WriteFeatureFile(featureList2, parameters.outputPath+parameters.correspondingFeatureFile2);
	}	//	if(!parameters.correspondingFeatureFile1.empty() || !parameters.correspondingFeatureFile2.empty())

}	//void SaveOutput(vector<FeatureT>& featureList1, vector<FeatureT>& featureList2, const Matrix4d& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics) const

/**
 * @brief Runs the 3D homography estimation pipeline
 * @param[out] model Estimated essential matrix
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Features in the first set
 * @param[in] featureList2 Features in the second set
 * @param[in] covarianceList1 Coordinate covariances for the first set
 * @param[in] covarianceList2 Coordinate covariances for the second set
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator33ImplementationC::ComputeHomography3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2)
{
	unsigned int loGeneratorSize1= Homography3DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2= Homography3DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);

	double effectiveNoiseVariance=0.5*(*parameters.noiseVariance1+*parameters.noiseVariance2);
	double effectiveIntialEstimateVariance= (1+parameters.initialEstimateError)*effectiveNoiseVariance;

	logger["EffectiveNoiseVariance"]=effectiveNoiseVariance;

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACHomography3DProblemT::data_container_type dummyList;
	RANSACHomography3DProblemT ransacProblem1=MakeRANSACHomography3DProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACHomography3DProblemT ransacProblem2=MakeRANSACHomography3DProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLHomography3DProblemT optimisationProblem=MakePDLHomography3DProblem(Matrix4d::Zero(), dummyList, optional<MatrixXd>());

	Homography3DEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	Homography3DPipelineProblemT problem=MakeGeometryEstimationPipelineHomography3DProblem(featureList1, featureList2, geometryEstimationProblem, covarianceList1, covarianceList2, effectiveNoiseVariance, parameters.pValue, initialEstimate, effectiveIntialEstimateVariance, parameters.pValue, parameters.nThreads);

	Homography3DPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return Homography3DPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeHomography3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2)

/**
 * @brief Runs the 3D rotation estimation pipeline
 * @param[out] model Estimated essential matrix
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Features in the first set
 * @param[in] featureList2 Features in the second set
 * @param[in] covarianceList1 Coordinate covariances for the first set
 * @param[in] covarianceList2 Coordinate covariances for the second set
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator33ImplementationC::ComputeRotation3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2)
{
	unsigned int loGeneratorSize1= Rotation3DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2= Rotation3DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);

	double effectiveNoiseVariance=0.5*(*parameters.noiseVariance1+*parameters.noiseVariance2);
	double effectiveIntialEstimateVariance= (1+parameters.initialEstimateError)*effectiveNoiseVariance;

	logger["EffectiveNoiseVariance"]=effectiveNoiseVariance;

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACRotation3DProblemT::data_container_type dummyList;
	RANSACRotation3DProblemT ransacProblem1=MakeRANSACRotation3DProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACRotation3DProblemT ransacProblem2=MakeRANSACRotation3DProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLRotation3DProblemT optimisationProblem=MakePDLRotation3DProblem(Matrix4d::Zero(), dummyList, optional<MatrixXd>());

	Rotation3DEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	Rotation3DPipelineProblemT problem=MakeGeometryEstimationPipelineRotation3DProblem(featureList1, featureList2, geometryEstimationProblem, covarianceList1, covarianceList2, effectiveNoiseVariance, parameters.pValue, initialEstimate, effectiveIntialEstimateVariance, parameters.pValue, parameters.nThreads);

	Rotation3DPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return Rotation3DPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeRotation3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2)

/**
 * @brief Runs the 3D similarity transformation estimation pipeline
 * @param[out] model Estimated essential matrix
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Features in the first set
 * @param[in] featureList2 Features in the second set
 * @param[in] covarianceList1 Coordinate covariances for the first set
 * @param[in] covarianceList2 Coordinate covariances for the second set
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator33ImplementationC::ComputeSimilarity3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2)
{
	unsigned int loGeneratorSize1= Similarity3DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2= Similarity3DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);

	double effectiveNoiseVariance=0.5*(*parameters.noiseVariance1+*parameters.noiseVariance2);
	double effectiveIntialEstimateVariance= (1+parameters.initialEstimateError)*effectiveNoiseVariance;

	logger["EffectiveNoiseVariance"]=effectiveNoiseVariance;

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACSimilarity3DProblemT::data_container_type dummyList;
	RANSACSimilarity3DProblemT ransacProblem1=MakeRANSACSimilarity3DProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACSimilarity3DProblemT ransacProblem2=MakeRANSACSimilarity3DProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLSimilarity3DProblemT optimisationProblem=MakePDLSimilarity3DProblem(Matrix4d::Zero(), dummyList, optional<MatrixXd>());

	Similarity3DEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	Similarity3DPipelineProblemT problem=MakeGeometryEstimationPipelineSimilarity3DProblem(featureList1, featureList2, geometryEstimationProblem, covarianceList1, covarianceList2, effectiveNoiseVariance, parameters.pValue, initialEstimate, effectiveIntialEstimateVariance, parameters.pValue, parameters.nThreads);

	Similarity3DPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return Similarity3DPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeSimilarity3D(Homography3DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography3DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const vector<CoordinateCovarianceT>& covarianceList1, const vector<CoordinateCovarianceT>& covarianceList2)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
GeometryEstimator33ImplementationC::GeometryEstimator33ImplementationC()
{
	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("Environment.NoThreads", value<string>()->default_value("1"), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")
	("Environment.Seed", value<string>()->default_value("0"), "Seed for the random number generator. If <0, rng default")

	("General.GeometryType", value<string>()->default_value(""), "Geometric relation between the feature sets. Homography, Rotation and Similarity")
	("General.InitialEstimateFile", value<string>()->default_value(""), "A file containing a 4x4 matrix, corresponding to an initial estimate of the geometry. If no initial estimate available, empty")
	("General.InitialEstimateNoise", value<string>()->default_value("0"), "Variance of the noise on the scene coordinates, due to the errors in the initial estimate. Cumulative with the detector noise, but applied only in the first guided matching iteration. A scale factor to the noise variance. >=0")

	("GuidedMatching.MaxIteration", value<string>()->default_value("10"), "Maximum number of iterations. >0")
	("GuidedMatching.MinRelativeDifference", value<string>()->default_value("0.01"), "If the relative difference between the fitness scores of two successive iterations is below this value, terminate. >0")
	("GuidedMatching.BinDensity", value<string>()->default_value("4"), "Number of feature bins per standard deviation. Used in spatial quantisation. >0")
	("GuidedMatching.pValue", value<string>()->default_value("0.05"), "Probability of rejection for an inlier. Determines the inlier threshold. [0,1]")
	("GuidedMatching.MaxPerturbation", value<string>()->default_value("1"), "Maximum distance between the true and the measured coordinates of a feature correspondence. Determines the proportion of eligible inliers for RANSAC, i.e. number of iterations. A scale factor to the noise std. >0")

	("GuidedMatching.Decimation.MaxObservationDensity", value<string>()->default_value("5"), "Number of features per bin. Determines the number of observations. >0")
	("GuidedMatching.Decimation.MinObservationRatio", value<string>()->default_value("10"), "Minimum ratio of the number of observations to the size of the generator. Determines the minimum size of the observation set. >1")
	("GuidedMatching.Decimation.ValidationRatio", value<string>()->default_value("10"), "Ratio of the number of validators to the size of the generator. >1")
	("GuidedMatching.Decimation.MaxAmbiguity", value<string>()->default_value("0.85"), "Maximum value of Matcher.FeatureMatcher.MaxRatio in a decimated set. Used only in the first pass, if there is no initial estimate. [0,1]")

	("Matcher.FeatureMatcher.MaxRatio", value<string>()->default_value("0.8"), "Maximum ratio of the similarity score of the best outsider to that of the best insider. See the documentation of NearestNeighbourMatcherC. [0,1]")
	("Matcher.FeatureMatcher.MaxFeatureDistance", value<string>()->default_value("0.5"), "Maximum distance between the normalised descriptor vectors of two corresponding points, as a percentage of the maximum possible distance.[0 1]")
	("Matcher.FeatureMatcher.NeighbourhoodCardinality", value<string>()->default_value("1"), "k in k-NN: Each feature can be associated with up to k of its nearest neighbours. >0")

	("Matcher.BucketFilter.Enable", value<string>()->default_value("1"), "If true, the bucket filter is enabled. Useful for eliminating outliers. 0 or 1")
	("Matcher.BucketFilter.MinSimilarity", value<string>()->default_value("0.5"), "Minimum similarity between two buckets for a valid bucket match. [0,1]")
	("Matcher.BucketFilter.MinQuorum", value<string>()->default_value("3"), "If no buckets in a pair has this many votes, the pair is exempted due to insufficient evidence for rejection (i.e., automatic success).>=0")
	("Matcher.BucketFilter.BucketDensity", value<string>()->default_value("10"), "Number of buckets, per unit standard deviation. >0")

	("GeometryEstimator.TSRANSAC.Confidence", value<string>()->default_value("0.99"), "Probability that the solution estimated by RANSAC is 'good'. Affects the number of iterations. [0,1]")

	("GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", value<string>()->default_value("10000"), "Maximum number of iterations. >0")
	("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations", value<string>()->default_value("10"), "Number of LO iterations. >=0")
	("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value("4"), "If the ratio of the number of inlier observations to the size of the generator set is below this value, LO-RANSAC does not trigger. Prevents LO-RANSAC from triggering at poor hypotheses. >=1")
	("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio", value<string>()->default_value("2"), "Ratio of the cardinality of the LO generator to that of the minimal generator. >=1 ")
	("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate", value<string>()->default_value("0.01"), "Rate of expansion of the observation set, as a percentage of the total number of observations. Any value >1 disables PROSAC. >0")

	("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations", value<string>()->default_value("10"), "Number of LO iterations. >=0")
	("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value("4"), "If the ratio of the number of inlier observations to the size of the generator set is below this value, LO-RANSAC does not trigger. Prevents LO-RANSAC from triggering at poor hypotheses. >=1")
	("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio", value<string>()->default_value("2"), "Ratio of the cardinality of the LO generator to that of the minimal generator. >=1 ")
	("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", value<string>()->default_value("0.01"), "Rate of expansion of the observation set, as a percentage of the total number of observations. Any value >1 disables PROSAC. >0")
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution", value<string>()->default_value("1"), "Number of solutions RANSAC returns")
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage", value<string>()->default_value("1"), "Minimum percentage of validators that are inliers to a model. Suppresses poor solutions. The first model is exempt. [0,1]")
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusion", value<string>()->default_value("0.75"), "Given the inlier validator sets A and B, if |intersection(A,B)|/min(|A|,|B|) is above this value, the corresponding models are deemed similar. [0,1]" )
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance", value<string>()->default_value("0.1"), "If the algebraic distance between two models below this value, they are deemed similar. Percentage of the maximum distance. [0,1]" )
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor", value<string>()->default_value("1.05"), "Lower values encourage MO-RANSAC to generate more solutions. >=1" )
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor", value<string>()->default_value("0.95"), "Lower values encourage MO-RANSAC to accept updates that reduce the diversity of the solution set. >=0" )

	("GeometryEstimator.Optimisation.MaxIteration", value<string>()->default_value("100"), "Maximum number of nonlinear least squares iterations. >=0")

	("GeometryEstimator.Covariance.Enable", value<string>()->default_value("1"), "If false, covariance estimation is disabled")
	("GeometryEstimator.Covariance.Alpha", value<string>()->default_value("-1"), "Sample spread parameter for SUT. Lower values a better for highly nonlinear transformations, however, too small values may lead to ill-conditioned covariance matrices. (0,1]. If not defined/invalid, automatic.")

	("FeatureSet1.FeatureFile", value<string>(), ".ft3 file containing the first feature set")
	("FeatureSet1.RoI", value<string>(), "Volume-of-interest in the first point cloud, in m. If not specified, bounding box of the features. [xMin yMin zMin xMax yMax zMax]")
	("FeatureSet1.Noise", value<string>(), "Variance of the noise on the scene coordinates, due to the detector. Used for determining the inlier threshold. If unspecified, estimated from FeatureSet2.CovarianceFile. m^2. >0")
	("FeatureSet1.CovarianceFile", value<string>(), ".cov3 file containing the per-point covariance of the noise on the scene coordinates, due to detector. Used for SUT. If unspecified, FeatureSet1.Noise is used instead.")

	("FeatureSet2.FeatureFile", value<string>(), ".ft3 file containing the second feature set")
	("FeatureSet2.RoI", value<string>(), "Volume-of-interest in the first point cloud, in m. If not specified, bounding box of the features. [xMin yMin zMin xMax yMax zMax]")
	("FeatureSet2.Noise", value<string>(), "Variance of the noise on the scene coordinates, due to the detector. Used for determining the inlier threshold. If unspecified, estimated from FeatureSet2.CovarianceFile. m^2. >0")
	("FeatureSet2.CovarianceFile", value<string>(), ".cov3 file containing the per-point covariance of the noise on the scene coordinates, due to detector. Used for SUT. If unspecified, FeatureSet2.Noise is used instead. ")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Model", value<string>(), "Estimated model, a 4x4 matrix")
	("Output.Covariance", value<string>(), "Covariance matrix for the estimated model")
	("Output.Correspondences", value<string>(), "Correspondences as a .c33 file")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.FeatureList1", value<string>()->default_value(""), "Corresponding features from the first set")
	("Output.FeatureList2", value<string>()->default_value(""), "Corresponding features from the second set")
	("Output.Verbose", value<string>()->default_value("1"), "Verbose output")
	;
}	//GeometryEstimator33ImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& GeometryEstimator33ImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void GeometryEstimator33ImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
    ptree tree; //Options tree

    tree.put("xmlcomment", "geometryEstimator33 configuration file");

    tree.put("Environment", "");
    tree.put("Environment.NoThreads", "1");
    tree.put("Environment.Seed", "0");

    tree.put("General", "");
	tree.put("General.GeometryType", "Homography");

	tree.put("GuidedMatching", "");
	tree.put("GuidedMatching.MaxPerturbation", "1");

	tree.put("GuidedMatching.Decimation", "");

	tree.put("Matcher", "");

	tree.put("Matcher.FeatureMatcher","");
	tree.put("Matcher.FeatureMatcher.NeighbourhoodCardinality", "1");

	tree.put("GeometryEstimator","");

	tree.put("GeometryEstimator.TSRANSAC","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1","");
	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration","10000");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC","");
	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations","10");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC","");
	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations","10");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC","");

	tree.put("GeometryEstimator.Optimisation","");
	tree.put("GeometryEstimator.Optimisation.MaxIteration","100");

	tree.put("ScaledUnscentedTransformation","");

	tree.put("FeatureSet1","");
	tree.put("FeatureSet1.FeatureFile","/home/f1.ft3");
	tree.put("FeatureSet1.Noise","1e-4");

	tree.put("FeatureSet2","");
	tree.put("FeatureSet2.FeatureFile","/home/f2.ft3");
	tree.put("FeatureSet2.Noise","1e-4");

	tree.put("Output","");
	tree.put("Output.Root","/home/");
	tree.put("Output.Model","mH.txt");
	tree.put("Output.Covariance","mCov.txt");
	tree.put("Output.Correspondences","corr.c33");
	tree.put("Output.Log","GeometryEstimator33.log");
	tree.put("Output.FeatureList1","o1.ft3");
	tree.put("Output.FeatureList2","o2.ft3");
	tree.put("Output.Verbose","1");

	//Advanced interface
	if(detailLevel>1)
	{
		tree.put("General.InitialEstimateFile", "/home/mH");
		tree.put("General.InitialEstimateNoise", "0");

	    tree.put("GuidedMatching.MaxIteration", "10");
		tree.put("GuidedMatching.MinRelativeDifference", "0.01");
		tree.put("GuidedMatching.BinDensity", "4");
		tree.put("GuidedMatching.pValue", "0.05");

		tree.put("GuidedMatching.Decimation.MaxObservationDensity", "5");
		tree.put("GuidedMatching.Decimation.MinObservationRatio", "10");
		tree.put("GuidedMatching.Decimation.ValidationRatio", "10");
		tree.put("GuidedMatching.Decimation.MaxAmbiguity", "0.85");

		tree.put("Matcher.FeatureMatcher.MaxRatio", "0.8");
		tree.put("Matcher.FeatureMatcher.MaxFeatureDistance", "0.5");

		tree.put("Matcher.BucketFilter","");
		tree.put("Matcher.BucketFilter.Enable","1");
		tree.put("Matcher.BucketFilter.MinSimilarity","0.5");
		tree.put("Matcher.BucketFilter.MinQuorum","3");
		tree.put("Matcher.BucketFilter.BucketDensity","10");

		tree.put("GeometryEstimator.TSRANSAC.Confidence","0.99");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio","4");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio","2");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate","0.01");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio","4");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio","2");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate","0.01");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution","1");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage","1");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusion","0.75");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance","0.1");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor", "1.05");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor", "0.95");

		tree.put("GeometryEstimator.Covariance.Enable", "1");
		tree.put("GeometryEstimator.Covariance.Alpha", "-1");

		tree.put("FeatureSet1.RoI","0 0 0 1 1 1");
		tree.put("FeatureSet1.CovarianceFile","/home/f1.cov3");

		tree.put("FeatureSet2.RoI","0 0 0 1 1 1");
		tree.put("FeatureSet2.CovarianceFile","/home/f1.cov3");
	}	//if(!flagBasic)

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool GeometryEstimator33ImplementationC::SetParameters(const ptree& tree)
{
	//Environment
	parameters.nThreads=min(omp_get_max_threads(), max(1, tree.get<int>("Environment.NoThreads", 1)));
	parameters.seed=tree.get<int>("Environment.Seed", -1);

	parameters.pipelineParameters.nThreads=parameters.nThreads;
		//General

	string geometryType=tree.get<string>("General.GeometryType", "Homography");
	map<string, Geometry33ProblemT> validGeometryTypes{ {"Homography", Geometry33ProblemT::HMG3}, {"Rotation", Geometry33ProblemT::ROT3}, {"Similarity", Geometry33ProblemT::SIM3} };
	auto itGT=validGeometryTypes.find(geometryType);
	if(itGT==validGeometryTypes.end())
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Invalid problem type. Value=")+geometryType));
	else
		parameters.problemType=itGT->second;

	parameters.initialEstimateFile=tree.get<string>("General.InitialEstimateFile","");
	parameters.flagInitialEstimate= !parameters.initialEstimateFile.empty();

	double initialEstimateNoise=tree.get<double>("General.InitialEstimateNoise", 0);
	if(initialEstimateNoise<0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: General.InitialEstimateNoise must be a non-negative value. Value=")+lexical_cast<string>(initialEstimateNoise) ) );
	else
		parameters.initialEstimateError=initialEstimateNoise;

	//Guided matching

	double maxPerturbation=tree.get<double>("GuidedMatching.MaxPerturbation",1);
	if(maxPerturbation<=0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.MaxPerturbation must be a positive value. Value=")+lexical_cast<string>(maxPerturbation) ) );
	else
		parameters.maxPerturbation=maxPerturbation;

	unsigned int maxGMIteration=tree.get<unsigned int>("GuidedMatching.MaxIteration",10);
	parameters.pipelineParameters.maxIteration=maxGMIteration;

	double minRelativeDifference=tree.get<double>("GuidedMatching.MinRelativeDifference",0.01);
	if(minRelativeDifference<0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.minRelativeDifference must be a nonnegative value. Value=")+lexical_cast<string>(minRelativeDifference) ) );
	else
		parameters.pipelineParameters.minRelativeDifference=minRelativeDifference;

	unsigned int binDensity=tree.get<unsigned int>("GuidedMatching.BinDensity",4);
	if(binDensity==0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.BinDensity must be a positive value. Value=")+lexical_cast<string>(binDensity) ) );
	else
	{
		parameters.binDensity=binDensity;
		parameters.pipelineParameters.binDensity1=binDensity;
		parameters.pipelineParameters.binDensity2=binDensity;
	}	//if(binDensity==0)

	double pValue=tree.get<double>("GuidedMatching.pValue", 0.05);
	if(pValue<=0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.pValue must be a positive value. Value=")+lexical_cast<string>(pValue) ) );
	else
		parameters.pValue=pValue;

	//GuidedMatching.Decimation

	unsigned int maxObservationDensity=tree.get<unsigned int>("GuidedMatching.Decimation.MaxObservationDensity", 5);
	if(maxObservationDensity<1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Decimation.MaxObservationDensity must be >1. Value=")+lexical_cast<string>(maxObservationDensity) ) );
	else
		parameters.pipelineParameters.maxObservationDensity=maxObservationDensity;

	double minObservationRatio=tree.get<double>("GuidedMatching.Decimation.MinObservationRatio",1);
	if(minObservationRatio<1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.Decimation.MinObservationRatio must be >=1. Value=")+lexical_cast<string>(minObservationRatio) ) );
	else
		parameters.pipelineParameters.minObservationRatio=minObservationRatio;

	double validationRatio=tree.get<double>("GuidedMatching.Decimation.ValidationRatio", 10);
	if(validationRatio<=1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.Decimation.ValidationRatio must be >1. Value=")+lexical_cast<string>(validationRatio) ) );
	else
		parameters.pipelineParameters.validationRatio=validationRatio;

	parameters.pipelineParameters.maxObservationRatio= pow<3>(2*3*binDensity)*maxObservationDensity;	//Approximate number of bins x density. Functionally, infinite

	double maxAmbiguity=tree.get<double>("GuidedMatching.Decimation.MaxAmbiguity", 0.85);
	if(maxAmbiguity<0 || maxAmbiguity>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GuidedMatching.Decimation.MaxAmbiguity must be in [0,1]. Value=")+lexical_cast<string>(maxAmbiguity)));
	else
		parameters.pipelineParameters.maxAmbiguity=maxAmbiguity;

	//Matcher.FeatureMatcher

	parameters.pipelineParameters.matcherParameters.flagStatic=false;

	double maxRatio=tree.get<double>("Matcher.FeatureMatcher.MaxRatio", 0.8);
	if(maxRatio<0 || maxRatio>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Matcher.FeatureMatcher.MaxRatio must be in [0,1]. Value=")+lexical_cast<string>(maxRatio) ) );
	else
		parameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold=maxRatio;

	double maxFeatureDistance=tree.get<double>("Matcher.FeatureMatcher.MaxFeatureDistance", 0.5);
	if(maxFeatureDistance<0 || maxFeatureDistance>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Matcher.FeatureMatcher.MaxFeatureDistance must be in [0,1]. Value=")+lexical_cast<string>(maxFeatureDistance) ) );
	else
		parameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold=maxFeatureDistance*sqrt(2);	//Max distance between two unit-norm vectors is sqrt(2)

	unsigned int neighbourhoodCardinality=tree.get<unsigned int>("Matcher.FeatureMatcher.NeighbourhoodCardinality", 1);
	if(neighbourhoodCardinality==0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Matcher.FeatureMatcher.NeighbourhoodCardinality must be positive. Value=")+lexical_cast<string>(neighbourhoodCardinality) ) );
	else
	{
		parameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12=neighbourhoodCardinality;
		parameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21=neighbourhoodCardinality;
	}	//if(neighbourhoodCardinality==0)

	parameters.pipelineParameters.matcherParameters.nnParameters.flagConsistencyTest=true;

	bool flagBucketFilter=tree.get<bool>("Matcher.BucketFilter.Enable", true);
	parameters.pipelineParameters.matcherParameters.flagBucketFilter=flagBucketFilter;

	if(flagBucketFilter)
	{
		double minBucketSimilarity=tree.get<double>("Matcher.BucketFilter.MinSimilarity", 0.5);
		if(minBucketSimilarity<0 || minBucketSimilarity>1)
			throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Matcher.BucketFilter.MinSimilarity is not in [0,1]. Value=")+lexical_cast<string>(minBucketSimilarity)));
		else
			parameters.pipelineParameters.matcherParameters.bucketSimilarityThreshold=minBucketSimilarity;

		unsigned int minQuorum=tree.get<unsigned int> ("Matcher.BucketFilter.MinQuorum", 3);
		parameters.pipelineParameters.matcherParameters.minQuorum=minQuorum;

		unsigned int bucketDensity=tree.get<unsigned int>("Matcher.BucketFilter.BucketDensity", 10);
		if(bucketDensity<=0)
			throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: Matcher.BucketFilter.BucketDensity is not positive. Value=")+lexical_cast<string>(bucketDensity)));

		parameters.pipelineParameters.matcherParameters.bucketDimensions.fill(1.0/bucketDensity);
	}	//if(flagBucketFilter)

	//GeometryEstimator.TSRANSAC

	double confidence=tree.get<double>("GeometryEstimator.TSRANSAC.Confidence", 0.99);
	if(confidence<0 || confidence>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.Confidence is not in [0,1]. Value=")+lexical_cast<string>(confidence)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.minConfidence=confidence;

	unsigned int maxIterationRS1=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", 10000);
	if(maxIterationRS1==0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration must be positive. Value=")+lexical_cast<string>(maxIterationRS1)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration=maxIterationRS1;

	double growthRateRS1=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate", 0.01);
	if(growthRateRS1<=0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate must be positive. Value=")+lexical_cast<string>(growthRateRS1)));
	else
	{
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagPROSAC= (growthRateRS1<=1);

		if(growthRateRS1<=1)
			parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate=growthRateRS1;
	}	//if(growthRateRS1<=0)

	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.eligibilityRatio=1;
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxErrorRatio=1.05;

	unsigned int nLOIteration1=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations", 10);
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.nLOIterations=nLOIteration1;

	double loMinObservationSupport1=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio",4);
	if(loMinObservationSupport1<0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio must be positive. Value=")+lexical_cast<string>(loMinObservationSupport1)));
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minObservationSupport=loMinObservationSupport1;

	double loGeneratorRatio1=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio", 2);
	if(loGeneratorRatio1<1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio must be >=1. Value=")+lexical_cast<string>(loGeneratorRatio1)));
	parameters.loGeneratorRatio1=loGeneratorRatio1;

	unsigned int nLOIteration2=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations", 10);
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations=nLOIteration2;

	double loMinObservationSupport2=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio",4);
	if(loMinObservationSupport2<0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio must be positive. Value=")+lexical_cast<string>(loMinObservationSupport2)));
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport=loMinObservationSupport2;

	double loGeneratorRatio2=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio", 2);
	if(loGeneratorRatio2<1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio must be >=1. Value=")+lexical_cast<string>(loGeneratorRatio2)));
	parameters.loGeneratorRatio2=loGeneratorRatio2;

	double growthRateRS2=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", 0.01);
	if(growthRateRS2<=0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate must be positive. Value=")+lexical_cast<string>(growthRateRS2)));
	else
	{
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagPROSAC=(growthRateRS2<=1);

		if(growthRateRS2<=1)
			parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate=growthRateRS2;
	}	//if(growthRateRS2<=0)

	unsigned int nSolution=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution", 1);
	if(nSolution==0)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution must be positive. Value=")+lexical_cast<string>(nSolution)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxSolution=nSolution;

	double minCoverage=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage", 1);
	if(minCoverage<0 || minCoverage>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage must be in [0,1]. Value=")+lexical_cast<string>(minCoverage)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minCoverage=minCoverage;

	double minInclusion=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusion", 1);
	if(minInclusion<0 || minInclusion>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusuion must be in [0,1]. Value=")+lexical_cast<string>(minInclusion)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minModelSimilarity=minInclusion;

	double maxAlgebraicDistance=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance", 1);
	if(maxAlgebraicDistance<0 || maxAlgebraicDistance>1)
		throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance must be in [0,1]. Value=")+lexical_cast<string>(maxAlgebraicDistance)));
	else
		parameters.maxAlgebraicDistance=maxAlgebraicDistance;

	double parsimonyFactor=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor", 1.05);
	if(parsimonyFactor<1)
		throw(invalid_argument( string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor must be >=1. Value: ")  + lexical_cast<string>(parsimonyFactor) ) );
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.parsimonyFactor=parsimonyFactor;

	double diversityFactor=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor", 0.95);
	if(diversityFactor<0)
		throw(invalid_argument( string("GeometryEstimator33ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor must be >=0. Value: ")  + lexical_cast<string>(diversityFactor) ) );
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.diversityFactor=diversityFactor;

	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.eligibilityRatio=1;
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxErrorRatio=1.05;

	chi_squared chi2(6);
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=cdf(chi2, pow<2>(parameters.maxPerturbation));	//maxPerturbation is a scale factor!

	//GeometryEstimator.Optimisation
	unsigned int maxLSIteration=tree.get<unsigned int>("GeometryEstimator.Optimisation.MaxIteration", 100);
	parameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration=maxLSIteration;
	parameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.flagCovariance=false;

	//GeometryEstimator.Covariance
	parameters.pipelineParameters.flagCovariance=tree.get<bool>("GeometryEstimator.Covariance.Enable", true);

	double alpha=tree.get<double>("GeometryEstimator.Covariance.Alpha", -1);

	if(! (alpha<=0 || alpha>1) )
		parameters.pipelineParameters.sutParameters.alpha=alpha;

	//FeatureSet1
	parameters.featureFile1=tree.get<string>("FeatureSet1.FeatureFile","");

	optional<string> roi1=tree.get_optional<string>("FeatureSet1.RoI");
	char_separator<char> separator(" ");
	if(roi1)
	{
		vector<ValueTypeM<Coordinate3DT>::type> tokens=Tokenise<ValueTypeM<Coordinate3DT>::type>(*roi1, string(" "));
		if(tokens.size()==6)
			parameters.boundingBox1=VolumeT(Coordinate3DT(tokens[0], tokens[1], tokens[2]), Coordinate3DT(tokens[3], tokens[4], tokens[5]));
	}	//if(!roi1)

	parameters.inputCovarianceFile1=tree.get<string>("FeatureSet1.CovarianceFile","");

	optional<double> noise1=tree.get_optional<double>("FeatureSet1.Noise");
	if(*noise1)
	{
		if(*noise1<=0)
			throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: FeatureSet1.Noise must be a positive value. Value=")+lexical_cast<string>(*noise1) ) );
		else
			parameters.noiseVariance1=noise1;
	}	//if(*noise1)

	//FeatureSet2
	parameters.featureFile2=tree.get<string>("FeatureSet2.FeatureFile","");

	optional<string> roi2=tree.get_optional<string>("FeatureSet2.RoI");
	if(roi2)
	{
		vector<ValueTypeM<Coordinate3DT>::type> tokens=Tokenise<ValueTypeM<Coordinate3DT>::type>(*roi2, string(" "));
		if(tokens.size()==6)
			parameters.boundingBox2=VolumeT(Coordinate3DT(tokens[0], tokens[1], tokens[2]), Coordinate3DT(tokens[3], tokens[4], tokens[5]));
	}	//if(!roi1)

	parameters.inputCovarianceFile2=tree.get<string>("FeatureSet2.CovarianceFile","");

	optional<double> noise2=tree.get_optional<double>("FeatureSet2.Noise");
	if(*noise2)
	{
		if(*noise2<=0)
			throw(invalid_argument(string("GeometryEstimator33ImplementationC::SetParameters: FeatureSet2.Noise must be a positive value. Value=")+lexical_cast<string>(*noise1) ) );
		else
			parameters.noiseVariance2=noise2;
	}	//if(*noise1)

	//Output
	parameters.outputPath=tree.get<string>("Output.Root","");

	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("GeometryEstimator33ImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.modelFile=tree.get<string>("Output.Model","");
	parameters.covarianceFile=tree.get<string>("Output.Covariance","");
	parameters.correspondenceFile=tree.get<string>("Output.Correspondences","");
	parameters.logFile=tree.get<string>("Output.Log","");
	parameters.correspondingFeatureFile1=tree.get<string>("Output.FeatureList1","");
	parameters.correspondingFeatureFile2=tree.get<string>("Output.FeatureList2","");
	parameters.flagVerbose=tree.get<bool>("Output.Verbose",true);

	parameters.pipelineParameters.flagVerbose=parameters.flagVerbose;

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool GeometryEstimator33ImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the initial estimate
	optional<Matrix4d> initialEstimate;
	if(parameters.flagInitialEstimate)
		initialEstimate=ArrayIOC<Matrix4d>::ReadArray(parameters.initialEstimateFile, 4, 4);

	if(parameters.flagVerbose)
	{
		map<Geometry33ProblemT, string> codeToProblem{ {Geometry33ProblemT::HMG3, "Homography"}, {Geometry33ProblemT::ROT3, "Rotation"}, {Geometry33ProblemT::SIM3, "Similarity"}};
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Problem type: "<<codeToProblem[parameters.problemType]<<"\n";
		cout<<"            Initial estimate: "<<(initialEstimate ? "Yes" : "No")<<"\n";
	}	//if(parameters.flagVerbose)

	//Load the feature files
    vector<FeatureT> featureList1;
    optional<vector<CoordinateCovarianceT> > covarianceList1;

	vector<FeatureT> featureList2;
	optional<vector<CoordinateCovarianceT> > covarianceList2;

#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)
{
	#pragma omp section
	{
		std::tie(featureList1, covarianceList1)=LoadFeatures(1);
		InitialiseCovariance(covarianceList1, parameters.noiseVariance1);
		if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Set 1 has "<<featureList1.size()<<" features \n";
	}

	#pragma omp section
    {
    	std::tie(featureList2, covarianceList2)=LoadFeatures(2);
    	InitialiseCovariance(covarianceList2, parameters.noiseVariance2);
    	if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Set 2 has "<<featureList2.size()<<" features. Performing guided matching...\n";
    }	//#pragma omp section
}	//#pragma omp parallel sections if(parameters.flagMultithreaded)

	if(parameters.flagVerbose && (featureList1.empty() || featureList2.empty()))
	{
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Empty feature set encountered. Terminating...\n";
		return true;
	}	//if(parameters.flagVerbose && (featureList1.empty() || featureList2.empty()))

	Matrix4d model;
	optional<MatrixXd> covariance;
	CorrespondenceListT correspondences;
	GeometryEstimationPipelineDiagnosticsC diagnostics;

	//Homography estimator
	if(parameters.problemType==Geometry33ProblemT::HMG3)
		diagnostics=ComputeHomography3D(model, covariance, correspondences, initialEstimate, featureList1, featureList2, *covarianceList1, *covarianceList2);

	//Rotation estimator
	if(parameters.problemType==Geometry33ProblemT::ROT3)
		diagnostics=ComputeRotation3D(model, covariance, correspondences, initialEstimate, featureList1, featureList2, *covarianceList1, *covarianceList2);

	//Similarity transformation estimator
	if(parameters.problemType==Geometry33ProblemT::SIM3)
		diagnostics=ComputeSimilarity3D(model, covariance, correspondences, initialEstimate, featureList1, featureList2, *covarianceList1, *covarianceList2);

	if(parameters.flagVerbose)
	{
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s:";

		if(!diagnostics.flagSuccess)
			cout<<" Geometry estimator failed!\n";
		else
		{
			cout<<"#Correspondences: "<<correspondences.size()<<" Error:"<<diagnostics.error<<"\n";
			cout<<" Result:\n"<<model<<"\n";
		}	//if(!diagnostics.flagSuccess)

	}	//if(parameters.flagVerbose)

	logger["Runtime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

    SaveOutput(featureList1, featureList2, model, covariance, correspondences, diagnostics);

	return true;
}	//bool Run()

}	//AppGeometryEstimator33N
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("140401");
    std::string header("geometryEstimator33: 3D geometry estimation");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppGeometryEstimator33N::GeometryEstimator33ImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
