/**
 * @file AppMultimodelGeometryEstimator22.cpp Implementation of \c multimodelGeometryEstimator22
 * @author Evren Imre
 * @date 20 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <omp.h>
#include <Eigen/Dense>
#include <fstream>
#include <functional>
#include "Application.h"
#include "../GeometryEstimationPipeline/MultimodelGeometryEstimationPipeline.h"
#include "../GeometryEstimationComponents/EssentialComponents.h"
#include "../GeometryEstimationComponents/Fundamental7Components.h"
#include "../GeometryEstimationComponents/Homography2DComponents.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Elements/Camera.h"
#include "../IO/CameraIO.h"
#include "../IO/ImageFeatureIO.h"
#include "../IO/ArrayIO.h"
#include "../IO/CoordinateCorrespondenceIO.h"
#include "../ApplicationInterface/InterfaceRANSAC.h"
#include "../ApplicationInterface/InterfaceMultimodelGeometryEstimationPipeline.h"
#include "../Wrappers/BoostFilesystem.h"

namespace SeeSawN
{
namespace AppMultimodelGeometryEstimator22N
{

using namespace ApplicationN;

using boost::math::pow;
using boost::math::chi_squared;
using boost::math::quantile;
using Eigen::MatrixXd;
using Eigen::Matrix3d;
using std::ofstream;
using std::greater;
using std::greater_equal;
using std::bind;
using SeeSawN::GeometryN::MultimodelGeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::MultimodelGeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::MultimodelGeometryEstimationPipelineC;
using SeeSawN::GeometryN::MakeRANSACHomography2DProblem;
using SeeSawN::GeometryN::MakePDLHomography2DProblem;
using SeeSawN::GeometryN::Homography2DPipelineProblemT;
using SeeSawN::GeometryN::Homography2DEstimatorProblemT;
using SeeSawN::GeometryN::RANSACHomography2DProblemT;
using SeeSawN::GeometryN::PDLHomography2DProblemT;
using SeeSawN::GeometryN::MakeRANSACFundamental7Problem;
using SeeSawN::GeometryN::MakePDLFundamentalProblem;
using SeeSawN::GeometryN::Fundamental7PipelineProblemT;
using SeeSawN::GeometryN::Fundamental7EstimatorProblemT;
using SeeSawN::GeometryN::RANSACFundamental7ProblemT;
using SeeSawN::GeometryN::PDLFundamentalProblemT;
using SeeSawN::GeometryN::MakeRANSACEssentialProblem;
using SeeSawN::GeometryN::MakePDLEssentialProblem;
using SeeSawN::GeometryN::EssentialPipelineProblemT;
using SeeSawN::GeometryN::EssentialEstimatorProblemT;
using SeeSawN::GeometryN::RANSACEssentialProblemT;
using SeeSawN::GeometryN::PDLEssentialProblemT;
using SeeSawN::GeometryN::MakeRANSACFundamental8Problem;
using SeeSawN::GeometryN::RANSACFundamental8ProblemT;
using SeeSawN::GeometryN::Fundamental8PipelineProblemT;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ION::CoordinateCorrespondenceIOC;
using SeeSawN::WrappersN::IsWriteable;

/**
 * @brief Parameters for \c multimodelGeometryEstimator22
 * @ingroup Application
 */
struct AppMultimodelGeometryEstimator22ParametersC
{
	enum class Geometry22ProblemT{AUTO, HMG2, FND, ESS};	//2D geometry problem types: Homography, fundamental matrix, essential matrix

	unsigned int nThreads;	///< Number of threads available to the application
	int seed;	///< RNG seed

	Geometry22ProblemT problemType;	///< Type of the problem
	double noiseVariance;	///< Image noise variance, in pixel^2. >0

	MultimodelGeometryEstimationPipelineParametersC pipelineParameters;	///< Parameters for the multimodel geometry estimation engine
	double inlierRejectionProbability;	///< Probability of the rejection of an inlier
	double loGeneratorRatioI;	///< Size of the LO generators, as a multiple of the minimal generator, for sequential RANSAC
	double moMaxAlgebraicDistanceI;	///< Maximum algebraic model distance for an admissible association between a hypothesis and an existing cluster (MORANSAC), for sequential RANSAC
	double loGeneratorRatioR;	///< Size of the LO generators, as a multiple of the minimal generator, for two-stage RANSAC
	double moMaxAlgebraicDistanceR;	///< Maximum algebraic model distance for an admissible association between a hypothesis and an existing cluster (MORANSAC), for two-stage RANSAC

	//Input
	string cameraFile1;	///< Camera file for the first feature set
	string featureFile1;	///< File holding the features in the first set

	string cameraFile2;	///< Camera file for the second feature set
	string featureFile2;	///< File holding the features in the second set

	double gridSpacing;	///< Minimum distance between two neighbouring image features
	optional<FrameT> roi;	///< Region of interest on the image

	//Output
	string outputPath;	///< Output path
	string modelRoot;	///< Root for the model file
	string covarianceRoot;	///< Root for the covariance file
	string correspondenceRoot;	///< Root for the correspondence file
	string logFile;	///< Filename for the log
	bool flagVerbose;	///< If \c true, verbose operation

	AppMultimodelGeometryEstimator22ParametersC() : nThreads(1), seed(-1), problemType(Geometry22ProblemT::AUTO), noiseVariance(1), inlierRejectionProbability(0.05), loGeneratorRatioI(2), moMaxAlgebraicDistanceI(1), loGeneratorRatioR(2), moMaxAlgebraicDistanceR(1), gridSpacing(0), flagVerbose(true)
	{}
};	//AppMultimodelGeometryEstimator22ParametersC

/**
 * @brief Implementation of \c multicameraSynchronisation
 * @remarks Usage notes
 * 	- AUTO does not pick HMG2
 * 	- One-sided fundamental matrix is not included, as each model will potentially have a different focal length estimate. If becomes necessary in future, it could be implemented via multiple iterations of the multimodel geometry estimation pipeline
 * @ingroup Application
 */
class AppMultimodelGeometryEstimator22ImplementationC
{
	private:

   		typedef ImageFeatureC FeatureT;	///< Feature type
		typedef FeatureT::coordinate_type CoordinateT;	///< Coordinate type

		typedef AppMultimodelGeometryEstimator22ParametersC::Geometry22ProblemT ProblemIdentifierT;

		typedef MultimodelGeometryEstimationPipelineC<Homography2DPipelineProblemT>::solution_type SolutionT;	///< Type of a model and the associated entities
		static constexpr unsigned int iModel=MultimodelGeometryEstimationPipelineC<Homography2DPipelineProblemT>::iModel;	///< Index for the model component
		static constexpr unsigned int iCovariance=MultimodelGeometryEstimationPipelineC<Homography2DPipelineProblemT>::iCovariance;	///< Index for the covariance component
		static constexpr unsigned int iInlierList=MultimodelGeometryEstimationPipelineC<Homography2DPipelineProblemT>::iInlierList;	///< Index for the inlier list component

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed
		AppMultimodelGeometryEstimator22ParametersC parameters;  ///< Application parameters
		map<ProblemIdentifierT, string> problemTypeToString{ {ProblemIdentifierT::HMG2, "Homography"}, {ProblemIdentifierT::FND, "Fundamental"}, {ProblemIdentifierT::ESS, "Essential"} };
		//@}

		/** @name State */ //@{
		map<string, double> logger;	///< Logs various values
		//@}

		/** @name Implementation details */ //@{

		static ptree PruneParameterTree(const ptree& src);	///< Removes the redundant parameters from the tree

		CameraC LoadCamera(const string& cameraFile) const;	///< Loads a camera file
		ProblemIdentifierT InferProblemType(const CameraC& camera1, const CameraC& camera2) const;	///< Automatically identifes the problem type
		bool VerifyProblem(CameraC& camera1, CameraC& camera2) const;	///< Verifies whether the camera files hold the necessary information for a specified problem type

		typedef CoordinateTransformations2DT::affine_transform_type AffineTransform2T;	///< A 2D affine transformation
		tuple<vector<FeatureT>, optional<AffineTransform2T>, LensDistortionC, optional<AffineTransform2T> > LoadImageFeatures(const string& featureFile, const CameraC& camera) const;	///< Loads the image features

		typedef RANSACHomography2DProblemT::dimension_type1 dimension_type;
		tuple<dimension_type, dimension_type> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const;	///< Computes the bin sizes for RANSAC

		void SaveOutput(const vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2 ) const;	///< Saves the output
		void SaveLog(const MultimodelGeometryEstimationPipelineDiagnosticsC& diagnostics);	///< Saves the log

    	MultimodelGeometryEstimationPipelineDiagnosticsC ComputeHomography(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2);	///< Runs the homography estimation pipeline
    	MultimodelGeometryEstimationPipelineDiagnosticsC ComputeFundamentalMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2);	///< Runs the fundamental matrix estimation pipeline
    	MultimodelGeometryEstimationPipelineDiagnosticsC ComputeEssentialMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2);	///< Runs the essential matrix estimation pipeline
    	MultimodelGeometryEstimationPipelineDiagnosticsC ComputeOneSidedFundamentalMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2);	///< Runs the one-sided fundamental matrix estimation pipeline, with known first camera
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppMultimodelGeometryEstimator22ImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}


};	//class AppMultimodelGeometryEstimator22ImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Loads a camera file
 * @param[in] cameraFile Filename for the camera file
 * @return The camera parameters
 * @throw runtime_error If the file does not contain exactly one camera
 */
CameraC AppMultimodelGeometryEstimator22ImplementationC::LoadCamera(const string& cameraFile) const
{
	vector<CameraC> tmp=CameraIOC::ReadCameraFile(cameraFile);

	if(tmp.size()!=1)
		throw(runtime_error(string("AppMultimodelGeometryEstimator22ImplementationC::LoadCamera : The file is empty or contains multiple cameras." )));

	if(!tmp[0].FrameBox())
		throw(runtime_error(string("AppMultimodelGeometryEstimator22ImplementationC::LoadCamera : FrameBox is not defined." )));

	return tmp[0];
}	//CameraC LoadCamera(const string& cameraFile)

/**
 * @brief Automatically identifes the problem type
 * @param[in] camera1 First camera
 * @param[in] camera2 Second camera
 * @return Problem type
 */
auto AppMultimodelGeometryEstimator22ImplementationC::InferProblemType(const CameraC& camera1, const CameraC& camera2) const -> ProblemIdentifierT
{
	bool flagK1=(bool)camera1.MakeIntrinsicCalibrationMatrix();
	bool flagK2=(bool)camera2.MakeIntrinsicCalibrationMatrix();

	if(flagK1 && flagK2)
		return ProblemIdentifierT::ESS;

	return ProblemIdentifierT::FND;
}	//Geometry22ProblemT InferProblemType(const CameraC& camera1, const CameraC& camera2)

/**
 * @brief Verifies whether the camera files hold the necessary information for a specified problem type
 * @param[in, out] camera1 First camera. At the output, intrinsics are invalid if not required by the problem
 * @param[in, out] camera2 Second camera. At the output, intrinsics are invalid if not required by the problem
 * @return True if the cameras have the necessary information
 */
bool AppMultimodelGeometryEstimator22ImplementationC::VerifyProblem(CameraC& camera1, CameraC& camera2) const
{
	bool flagK1=(bool)camera1.MakeIntrinsicCalibrationMatrix();
	bool flagK2=(bool)camera2.MakeIntrinsicCalibrationMatrix();

	if(parameters.problemType==ProblemIdentifierT::ESS)
		return flagK1 && flagK2;

	if(flagK1)
		camera1.Intrinsics()=optional<IntrinsicC>();

	if(flagK2)
		camera2.Intrinsics()=optional<IntrinsicC>();

	return true;	//FND or HMG2
}	//bool VerifyProblem(const CameraC& camera1, const CameraC& camera2)

/**
 * @brief Loads the image features
 * @param[in] featureFile File holding the feature set
 * @param[in] camera Camera
 * @throws runtime_error If the feature list is empty
 * @return A tuple: image features, normalising transformation, lens distortion, denormaliser
 */
auto AppMultimodelGeometryEstimator22ImplementationC::LoadImageFeatures(const string& featureFile, const CameraC& camera) const -> tuple<vector<FeatureT>, optional<AffineTransform2T>, LensDistortionC, optional<AffineTransform2T> >
{
	tuple<vector<FeatureT>, optional<AffineTransform2T>, LensDistortionC, optional<AffineTransform2T> > output;

	//Lens distortion
	get<2>(output) = (bool) camera.Distortion() ? *camera.Distortion() : LensDistortionC();	//Default: NOD

	//Intrinsics
	optional<IntrinsicCalibrationMatrixT> mK=camera.MakeIntrinsicCalibrationMatrix();
	if(mK)
	{
		get<1>(output)->matrix()=mK->inverse();
		get<3>(output)->matrix()=*mK;
	}	//if(mK)

	//RoI
	optional<tuple<Coordinate2DT, Coordinate2DT> > roi;

	if(parameters.roi)
		roi=make_tuple(parameters.roi->corner(FrameT::BottomLeft), parameters.roi->corner(FrameT::TopRight) );
	else
		roi=make_tuple(camera.FrameBox()->corner(FrameT::BottomLeft), camera.FrameBox()->corner(FrameT::TopRight) );	//camera is validated, so there is a valid frame box

	//Features
	get<0>(output)=ImageFeatureIOC::ReadFeatureFile(featureFile, true);	//Read

	if(get<0>(output).empty())
		throw(runtime_error("AppMultimodelGeometryEstimator22ImplementationC::LoadImageFeatures : Empty image feature list encountered. Terminating...\n"));

	ImageFeatureIOC::PreprocessFeatureSet(get<0>(output), *roi, parameters.gridSpacing, true, get<2>(output), get<1>(output)); //Preprocess

	return output;
}	//tuple<vector<ImageFeatureC>, optional<AffineTransform2T>, LensDistortionC > LoadImageFeatures(CameraC camera) const

/**
 * @brief Computes the bin sizes for RANSAC
 * @param[in] featureList1 First feature list
 * @param[in] featureList2 Second feature list
 * @pre \c featureList1 is nonempty
 * @pre \c featureList2 is nonempty
 * @return A pair of bins. [First set; Second set]
 */
auto AppMultimodelGeometryEstimator22ImplementationC::ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const -> tuple<dimension_type, dimension_type>
{
	//Preconditions
	assert(!featureList1.empty());
	assert(!featureList2.empty());

	vector<CoordinateT> coordinateList1=MakeCoordinateVector(featureList1);
	dimension_type binSize1=*ComputeBinSize<CoordinateT>(coordinateList1, parameters.pipelineParameters.binDensity1);

	vector<CoordinateT> coordinateList2=MakeCoordinateVector(featureList2);
	dimension_type binSize2=*ComputeBinSize<CoordinateT>(coordinateList2, parameters.pipelineParameters.binDensity2);

	return make_tuple(binSize1, binSize2);
}	//tuple<dimension_type1, dimension_type2> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const

/**
 * @brief Runs the homography estimation pipeline
 * @param[out] solutions Solutions
 * @param[in] featureList1 Features in the first set
 * @param[in] featureList2 Features in the second set
 * @return Diagnostics object
 */
MultimodelGeometryEstimationPipelineDiagnosticsC AppMultimodelGeometryEstimator22ImplementationC::ComputeHomography(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2)
{
	unsigned int sGenerator=Homography2DPipelineProblemT::GetMinimalGeneratorSize();
	unsigned int loGeneratorSizeR= sGenerator * max(1.0, parameters.loGeneratorRatioR);
	unsigned int loGeneratorSizeI= sGenerator * max(1.0, parameters.loGeneratorRatioI);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACHomography2DProblemT::data_container_type dummyList;
	RANSACHomography2DProblemT ransacProblemI=MakeRANSACHomography2DProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceI, loGeneratorSizeI, binSize1, binSize2);	//RANSAC problem for the identification stage
	RANSACHomography2DProblemT ransacProblemR=MakeRANSACHomography2DProblem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceR, loGeneratorSizeR, binSize1, binSize2);	//RANSAC problem for the refinement stage
	PDLHomography2DProblemT optimisationProblem=MakePDLHomography2DProblem(Homography2DT::Zero(), dummyList, optional<MatrixXd>());
	Homography2DEstimatorProblemT geometryEstimationProblem(ransacProblemR, ransacProblemR, optimisationProblem, optional<Homography2DT>());
	Homography2DPipelineProblemT geometryPipelineProblem=MakeGeometryEstimationPipelineHomography2DProblem(featureList1, featureList2, geometryEstimationProblem, parameters.noiseVariance, parameters.inlierRejectionProbability, optional<Homography2DT>(), 0, parameters.inlierRejectionProbability, parameters.nThreads);

	typedef MultimodelGeometryEstimationPipelineC<Homography2DPipelineProblemT> GeometryPipelineT;
	GeometryPipelineT::problem_type problem=make_tuple(ransacProblemI, geometryPipelineProblem);

	GeometryPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return GeometryPipelineT::Run(solutions, problem, rng, parameters.pipelineParameters);
}	//MultimodelGeometryEstimationPipelineDiagnosticsC ComputeHomography(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2)

/**
 * @brief Runs the fundamental matrix estimation pipeline
 * @param[out] solutions Solutions
 * @param[in] featureList1 Features in the first set
 * @param[in] featureList2 Features in the second set
 * @return Diagnostics object
 */
MultimodelGeometryEstimationPipelineDiagnosticsC AppMultimodelGeometryEstimator22ImplementationC::ComputeFundamentalMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2)
{
	unsigned int sGenerator=Fundamental7PipelineProblemT::GetMinimalGeneratorSize();
	unsigned int loGeneratorSizeR= sGenerator * max(1.0, parameters.loGeneratorRatioR);
	unsigned int loGeneratorSizeI= sGenerator * max(1.0, parameters.loGeneratorRatioI);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACFundamental7ProblemT::data_container_type dummyList;
	RANSACFundamental7ProblemT ransacProblemI=MakeRANSACFundamental7Problem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceI, loGeneratorSizeI, binSize1, binSize2);
	RANSACFundamental7ProblemT ransacProblemR=MakeRANSACFundamental7Problem(dummyList, dummyList, parameters.noiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceR, loGeneratorSizeR, binSize1, binSize2);
	PDLFundamentalProblemT optimisationProblem=MakePDLFundamentalProblem(EpipolarMatrixT::Zero(), dummyList, optional<MatrixXd>());
	Fundamental7EstimatorProblemT geometryEstimationProblem(ransacProblemR, ransacProblemR, optimisationProblem, optional<EpipolarMatrixT>());
	Fundamental7PipelineProblemT geometryPipelineProblem=MakeGeometryEstimationPipelineFundamental7Problem(featureList1, featureList2, geometryEstimationProblem, parameters.noiseVariance, parameters.inlierRejectionProbability, optional<Homography2DT>(), 0, parameters.inlierRejectionProbability, parameters.nThreads);

	typedef MultimodelGeometryEstimationPipelineC<Fundamental7PipelineProblemT> GeometryPipelineT;
	GeometryPipelineT::problem_type problem=make_tuple(ransacProblemI, geometryPipelineProblem);

	GeometryPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return GeometryPipelineT::Run(solutions, problem, rng, parameters.pipelineParameters);
}	//MultimodelGeometryEstimationPipelineDiagnosticsC ComputeFundamentalMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2)

/**
 * @brief Runs the essential matrix estimation pipeline
 * @param[out] solutions Solutions
 * @param[in] featureList1 Features in the first set
 * @param[in] featureList2 Features in the second set
 * @param[in] mK1 Intrinsic calibration parameters for the first camera
 * @param[in] mK2 Intrinsic calibration parameters for the second camera
 * @return Diagnostics object
 */
MultimodelGeometryEstimationPipelineDiagnosticsC AppMultimodelGeometryEstimator22ImplementationC::ComputeEssentialMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2)
{
	unsigned int sGenerator=EssentialPipelineProblemT::GetMinimalGeneratorSize();
	unsigned int loGeneratorSizeR1= Fundamental8PipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatioR);
	unsigned int loGeneratorSizeR2= sGenerator * max(1.0, parameters.loGeneratorRatioR);
	unsigned int loGeneratorSizeI= sGenerator * max(1.0, parameters.loGeneratorRatioI);

	double scale=(mK1(0,0)+mK2(0,0))*0.5;	//Scale factor for the inlier thresholds
	double effectiveNoiseVariance=parameters.noiseVariance/pow<2>(scale);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACEssentialProblemT::data_container_type dummyList;
	RANSACEssentialProblemT ransacProblemI=MakeRANSACEssentialProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceI, loGeneratorSizeI, binSize1, binSize2);
	RANSACFundamental8ProblemT ransacProblemR1=MakeRANSACFundamental8Problem(dummyList, dummyList, effectiveNoiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceR, loGeneratorSizeR1, binSize1, binSize2);
	RANSACEssentialProblemT ransacProblemR2=MakeRANSACEssentialProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.inlierRejectionProbability, parameters.moMaxAlgebraicDistanceR, loGeneratorSizeR2, binSize1, binSize2);
	PDLEssentialProblemT optimisationProblem=MakePDLEssentialProblem(EpipolarMatrixT::Zero(), dummyList, optional<MatrixXd>());
	EssentialEstimatorProblemT geometryEstimationProblem(ransacProblemR1, ransacProblemR2, optimisationProblem, optional<EpipolarMatrixT>());
	EssentialPipelineProblemT geometryPipelineProblem=MakeGeometryEstimationPipelineEssentialProblem(featureList1, featureList2, geometryEstimationProblem, effectiveNoiseVariance, parameters.inlierRejectionProbability, optional<EpipolarMatrixT>(), 0, parameters.inlierRejectionProbability, parameters.nThreads);

	typedef MultimodelGeometryEstimationPipelineC<EssentialPipelineProblemT> GeometryPipelineT;
	GeometryPipelineT::problem_type problem=make_tuple(ransacProblemI, geometryPipelineProblem);

	GeometryPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return GeometryPipelineT::Run(solutions, problem, rng, parameters.pipelineParameters);
}	//MultimodelGeometryEstimationPipelineDiagnosticsC ComputeEssentialMatrix(vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppMultimodelGeometryEstimator22ImplementationC::AppMultimodelGeometryEstimator22ImplementationC()
{
	AppMultimodelGeometryEstimator22ParametersC defaultParameters;

	chi_squared chi2(2);
	double defaultPerturbationFactorR=sqrt(quantile(chi2, defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio));

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.nThreads)), "Number of threads available to the application. >0. Capped at the maximum number of available threads")
	("General.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.seed)), "Seed for the random number generator. If <0, rng default")

	("Problem.Type", value<string>()->default_value("Fundamental"), "Problem type. Valid values: Fundamental, Essential, Homography.. If not defined, automatic detection")
	("Problem.NoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.noiseVariance)), "Variance of the noise on the image coordinates, in pixel^2. Affects the inlier threshold. >0")

	("EstimationPipeline.Main.InlierRejectionProbability", value<string>()->default_value(lexical_cast<string>(defaultParameters.inlierRejectionProbability)), "Probability of rejecting an inlier. Determines the inlier threshold. (0,1)" )
	("EstimationPipeline.Main.MinInlierRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.minInlierRatio)), "Minimum inlier ratio for a valid model discovered by sequential RANSAC. Also a termination condition. >0")
	("EstimationPipeline.Main.AssignmentType", value<string>()->default_value(lexical_cast<string>("HARD")), "Assignment strategy. SOFT for multiple memberships, HARD for 1:1. UNIQUE rejects any correspondences that are inliers to multiple models.")

	("EstimationPipeline.Identification.Decimator.MaxObservationDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.maxObservationDensity)), "Observation set: Maximum number of correspondences per bin. >0")
	("EstimationPipeline.Identification.Decimator.ObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.observationRatio)), "Observation set: Observation set: Minimum number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Identification.Decimator.ValidatorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.validatorRatio)), "Validation set: Number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Identification.Decimator.BinDensity1", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.binDensity1)), "Number of bins per standard deviation for the first set. Affects the spatial quantisation level. >0")
	("EstimationPipeline.Identification.Decimator.BinDensity2", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.binDensity2)), "Number of bins per standard deviation for the second set. Affects the spatial quantisation level. >0")

	("EstimationPipeline.Identification.Matching.kNN.MinRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the best similarity score to the second best, for an admissible correspondence. Quality-quantity trade-off.[0,1]")
	("EstimationPipeline.Identification.Matching.kNN.NeighbourhoodCardinality", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12)), "The maximum number of candidates that can be associated with a feature. k>1 yields ambiguous matches. >=1")
	("EstimationPipeline.Identification.Matching.kNN.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold)), "Minimum similarity for an admissible correspondence. >=0")
	("EstimationPipeline.Identification.Matching.kNN.FlagConsistencyTest", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.nnParameters.flagConsistencyTest)), "If true, each feature in a valid correspondence is among the best k candidates for the other")

	("EstimationPipeline.Identification.Matching.SpatialConsistency.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.flagBucketFilter)), "If true, the algorithm enforces a spatial consistency constraint between neighbouring correspondences")
	("EstimationPipeline.Identification.Matching.SpatialConsistency.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity for two spatial bins to be considered associated, relative to the maximum similarity. [0,1]")
	("EstimationPipeline.Identification.Matching.SpatialConsistency.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.matcherParameters.minQuorum)), "If the number correspondences in a bin is below this value, it is held exempt from the spatial consistency check. >=0")
	("EstimationPipeline.Identification.Matching.SpatialConsistency.BucketDensity", value<string>()->default_value(lexical_cast<string>(1.0/defaultParameters.pipelineParameters.matcherParameters.bucketDimensions[0])), "Number of bins per standard deviation. >0")

	("EstimationPipeline.Identification.SequentialRANSAC.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.maxIteration)), "Maximum number of sequential RANSAC iterations. >0")

	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.Main.MinConfidence", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.minConfidence)), "Minimum probability that RANSAC finds an acceptable solution. (0,1]")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.maxIteration)), "Maximum number of iterations. >=minIteration")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.Main.MinIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.minIteration)), "Minimum number of iterations. >=0")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.minError)), "If the error for the solution is below this value, the process is terminated")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.Main.MaxError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.maxError)), "If the error for the solution is above this value, it is rejected. >=minError")

	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.nLOIterations)), "Number of local optimisation iterations. >=0")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.loGeneratorRatioI)), "Generator size for the LO iterations, in multiples of the minimal generator. Ignored if the solver cannot handle nonminimal sets. >=1")

	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.flagPROSAC)), "If true, PROSAC is enabled")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.MORANSAC.MaxNoCluster", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.maxSolution)), "Maximum number of solutions. >=1")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.MORANSAC.MinCoverage", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.minCoverage)), "Minimum percentage validation support a model should achieve to start a new cluster. [0,1]")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.MORANSAC.MinOverlapIndex", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.minModelSimilarity)), "Minimum overlap index between the inlier sets of two models, for a valid association. >=0")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.MORANSAC.ParsimonyFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.parsimonyFactor)), "A new cluster is created only if the new total inlier count relative to the current is above this value . >=1")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.MORANSAC.DiversityFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.diversityFactor)), "A MORANSAC action is rejected if the new total inlier count relative to the current is below this value. >=0")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.MORANSAC.MaxAlgebraicDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.moMaxAlgebraicDistanceI)), "Number of local optimisation iterations. >=0")

	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.SPRT.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.flagWaldSAC)), "If true SPRT is enabled")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("EstimationPipeline.Identification.SequentialRANSAC.RANSAC.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.ransacParameters.ransacParameters.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.maxIteration)), "Maximum number of guided matching iterations. >0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Main.MinRelativeDifference", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.minRelativeDifference)), "If the relative error between two successive iterations is below this value, the procedure is terminated. >0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Main.RefreshTreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.refreshThreshold)), "If the relative improvement in the number of inliers between the inliers to the current set and that of the reference is above this value, the reference set is updated. >=1")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Main.FlagCovariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.flagCovariance)), "If true, the algorithm calculates the covariance for the estimated parameters")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Decimation.BinDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.binDensity1)),"Number of bins per standard deviation. Affects the spatial quantisation level. >0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Decimation.MaxObservationDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.maxObservationDensity)),"Observation set: Maximum number of correspondences per bin. >0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Decimation.MinObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.minObservationRatio)),"Observation set: Minimum number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Decimation.MaxObservationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.maxObservationRatio)),"Observation set: Maximum number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Decimation.ValidationRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.validationRatio)),"Validation set: Number of correspondences, in multiples of the minimal generator. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Decimation.MaxAmbiguity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.maxAmbiguity)),"Maximum ambiguity for a correspondence in the observation or the validation set. [0,1]")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.kNN.MinRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the best similarity score to the second best, for an admissible correspondence. Quality-quantity trade-off.[0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.nnParameters.neighbourhoodSize12)), "The maximum number of candidates that can be associated with a feature. k>1 yields ambiguous matches. >=1")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.kNN.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.nnParameters.similarityTestThreshold)), "Minimum similarity for an admissible correspondence. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.kNN.FlagConsistencyTest", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.nnParameters.flagConsistencyTest)), "If true, each feature in a valid correspondence is among the best k candidates for the other")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.flagBucketFilter)), "If true, the algorithm enforces a spatial consistency constraint between neighbouring correspondences")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.SpatialConsistency.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity for two spatial bins to be considered associated, relative to the maximum similarity. [0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.SpatialConsistency.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.minQuorum)), "If the number correspondences in a bin is below this value, it is held exempt from the spatial consistency check. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.Matching.SpatialConsistency.BucketDensity", value<string>()->default_value(lexical_cast<string>(1.0/defaultParameters.pipelineParameters.geometryEstimatorParameters.matcherParameters.bucketDimensions[0])), "Number of bins per standard deviation. >0")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.MinConfidence", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.minConfidence)), "Minimum probability that RANSAC finds an acceptable solution. (0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.PerturbationFactor", value<string>()->default_value(lexical_cast<string>(defaultPerturbationFactorR)), "Maximum admissible distance between the true and the estimated position of an image feature, in multiples of the noise standard deviation. Affects the number of iterations. >0")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration)), "Maximum number of iterations. >=minIteration")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MinIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration)), "Minimum number of iterations. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.minError)), "If the error for the solution is below this value, the process is terminated")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.Main.MaxError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxError)), "If the error for the solution is above this value, it is rejected. >=minError")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagPROSAC)), "If true, PROSAC is enabled")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage1.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.Main.EligibilityRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.eligibilityRatio)), "Percentage of eligible correspondences. Lower values delay the convergence. (0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.Main.MinError", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.minError)), "If the error for the solution is below this value, the process is terminated")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.NoIterations", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations)), "Number of local optimisation iterations. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport)), "Minimum support size in the observation set to trigger LO, in multiples of the generator size. >=1")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.MaxErrorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxErrorRatio)), "If the ratio of the error for the current hypothesis to that of the best hypotehsis is below this value, LO is triggered. >=1")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.LORANSAC.GeneratorRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.loGeneratorRatioR)), "Generator size for the LO iterations, in multiples of the minimal generator. Ignored if the solver cannot handle nonminimal sets. >=1")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MaxNoCluster", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxSolution)), "Maximum number of solutions. >=1")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MinCoverage", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.minCoverage)), "Minimum percentage validation support a model should achieve to start a new cluster. [0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MinOverlapIndex", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.minModelSimilarity)), "Minimum overlap index between the inlier sets of two models, for a valid association. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.ParsimonyFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.parsimonyFactor)), "A new cluster is created only if the new total inlier count relative to the current is above this value . >=1")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.DiversityFactor", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.diversityFactor)), "A MORANSAC action is rejected if the new total inlier count relative to the current is below this value. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.MORANSAC.MaxAlgebraicDistance", value<string>()->default_value(lexical_cast<string>(defaultParameters.moMaxAlgebraicDistanceR)), "Number of local optimisation iterations. >=0")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.PROSAC.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagPROSAC)), "If true, PROSAC is enabled")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.PROSAC.GrowthRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate)), "Growth rate of the effective observation set, as a percentage of the full observation set. (0,1]")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.SPRT.InitialEpsilon", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.epsilon)), "Initial value of p(inlier|good hypothesis). (0,1)")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.SPRT.InitialDelta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.delta)), "Initial value of p(inlier|bad hypothesis). (0,1) and <initialEpsilon")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Stage2.SPRT.MaxDeltaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxDeltaTolerance)), "If the relative difference between the current value of delta, and that for which SPRT is designed is above this value, SPRT is updated. (0,1]")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.pdlParameters.maxIteration)), "Maximum number of iterations. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.Epsilon1", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.pdlParameters.epsilon1)), "Gradient convergence criterion, as a function of the magnitude of the gradient vector. >0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.Epsilon2", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.pdlParameters.epsilon2)), "Solution convergence criterion, as the relative distance between two successive updates. >0")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Refinement.TrustRegion.Delta0", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.pdlParameters.delta0)), "Initial size of the trust region. >0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Refinement.TrustRegion.LambdaShrink", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.pdlParameters.lambdaShrink)), "Shrinkage rate for the trust region. (0,1)")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.Refinement.TrustRegion.LambdaGrowth", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.geometryEstimatorParameters.pdlParameters.lambdaGrowth)), "Growth rate for the trust region. >1")

	("EstimationPipeline.Refinement.GeometryEstimationPipeline.CovarianceEstimation.Alpha", value<string>()->default_value("1"), "Controls the sample spread for SUT. If omitted, calculated automatically. (0,1]")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.CovarianceEstimation.Beta", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.sutParameters.beta)), "Compansates for errors in the high order moments. >=0")
	("EstimationPipeline.Refinement.GeometryEstimationPipeline.CovarianceEstimation.Kappa", value<string>()->default_value(lexical_cast<string>(defaultParameters.pipelineParameters.geometryEstimatorParameters.sutParameters.kappa)), "Controls the sample spread for SUT.")

	("Input.CameraFile1", value<string>(), "File for the parameters of the first camera")
	("Input.CameraFile2", value<string>(), "File for the parameters of the second camera")
	("Input.FeatureFile1", value<string>(), "File for the first feature set")
	("Input.FeatureFile2", value<string>(), "File for the second feature set")
	("Input.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.gridSpacing)), "Minimum distance between two image features, in pixels. >=0")
	("Input.RegionOfInterest", value<string>()->default_value("0 0 1919 1079"), "Any image features outside of this window are ignored. [Upper-left; lower-right]. If omitted, all features are included")

	("Output.Root", value<string>()->default_value(""), "Root directory for the output files. Should be terminated with a slash")
	("Output.Model", value<string>(), "Root name for the model files")
	("Output.Covariance", value<string>(), "Root name for the covariance files")
	("Output.Correspondences", value<string>(), "Root name for the correspondence files")
	("Output.Log", value<string>(), "File for the application log")
	("Output.FlagVerbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "If true, verbose output")
	;
}	//AppMultimodelGeometryEstimator22ImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppMultimodelGeometryEstimator22ImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppMultimodelGeometryEstimator22ImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppMultimodelGeometryEstimator22ParametersC defaultParameters;

	tree.put("xmlcomment", "multimodelGeometryEstimator22 configuration file");

	 //General
	tree.put("General","");
	tree.put("General.NoThreads", defaultParameters.nThreads);
	tree.put("General.Seed",defaultParameters.seed);

	//Problem
	tree.put("Problem","");
	tree.put("Problem.Type", "Fundamental");
	tree.put("Problem.NoiseVariance", defaultParameters.noiseVariance);

	//Estimation pipeline

	tree.put_child("EstimationPipeline", InterfaceMultimodelGeometryEstimationPipelineC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel));

	if(detailLevel>1)
	    tree.put("EstimationPipeline.Main.InlierRejectionProbability", defaultParameters.inlierRejectionProbability);

	InterfaceRANSACC::InsertGeometryProblemParameters(tree, "EstimationPipeline.Identification.SequentialRANSAC.RANSAC", detailLevel);

	InterfaceRANSACC::InsertGeometryProblemParameters(tree, "EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC", detailLevel);

	if(detailLevel>1)
	    tree.put("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.CovarianceEstimation.Alpha", 1);	//Just to expose the parameter

	if(detailLevel<=1)
	    if(tree.get_child_optional("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC"))
            tree.get_child("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");

    //Input
    tree.put("Input", "");
    tree.put("Input.CameraFile1", defaultParameters.cameraFile1);
    tree.put("Input.CameraFile2", defaultParameters.cameraFile2);
    tree.put("Input.FeatureFile1", defaultParameters.featureFile1);
    tree.put("Input.FeatureFile2", defaultParameters.featureFile2);

    if(detailLevel>1)
    {
        tree.put("Input.GridSpacing", defaultParameters.gridSpacing);
        tree.put("Input.RegionOfInterest", "0 0 1919 1079");
    }

    //Output
    tree.put("Output","");
    tree.put("Output.Root", defaultParameters.outputPath);
    tree.put("Output.Model", defaultParameters.modelRoot);
    tree.put("Output.Covariance", defaultParameters.covarianceRoot);
    tree.put("Output.Correspondences", defaultParameters.correspondenceRoot);
    tree.put("Output.Log", defaultParameters.logFile);
    tree.put("Output.FlagVerbose", defaultParameters.flagVerbose);

    tree=PruneParameterTree(tree);

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Removes the redundant parameters from the tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree AppMultimodelGeometryEstimator22ImplementationC::PruneParameterTree(const ptree& src)
{
	ptree output(src);

    if(src.get_child_optional("EstimationPipeline.Main.NoThreads"))
    	output.get_child("EstimationPipeline.Main").erase("NoThreads");

    if(src.get_child_optional("EstimationPipeline.Main.FlagVerbose"))
		output.get_child("EstimationPipeline.Main").erase("FlagVerbose");

	return output;
}	//void PruneParameterTree(ptree& parameters)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppMultimodelGeometryEstimator22ImplementationC::SetParameters(const ptree& tree)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto o01=[](const double& val){return val>0 && val<1;};

	ptree lTree = PruneParameterTree(tree);

	AppMultimodelGeometryEstimator22ParametersC defaultParameters;

	//General
	unsigned int nThreads = min((double)omp_get_max_threads(), ReadParameter(lTree, "General.NoThreads", gt0, "is not >0", optional<double>(defaultParameters.pipelineParameters.nThreads)));
	parameters.nThreads=nThreads;

	parameters.seed=lTree.get<double>("General.Seed", defaultParameters.seed);

	//Problem
	if(lTree.get_optional<string>("Problem.Type"))
	{
		map<string, ProblemIdentifierT> problemTypeSelector{{"Homography", ProblemIdentifierT::HMG2}, {"Fundamental", ProblemIdentifierT::FND}, {"Essential", ProblemIdentifierT::ESS}};
		auto itProblemType=problemTypeSelector.find(lTree.get<string>("Problem.Type",""));

		if(itProblemType==problemTypeSelector.end())
			throw(invalid_argument(string("AppMultimodelGeometryEstimator22ImplementationC::SetParameters : Invalid problem type. Value=") + itProblemType->first) );

		parameters.problemType=itProblemType->second;
	}	//if(lTree.get_optional<string>("Problem.Type"))
	else
		parameters.problemType=ProblemIdentifierT::AUTO;

	parameters.noiseVariance=ReadParameter(lTree, "Problem.NoiseVariance", gt0, "is not >0", optional<double>(defaultParameters.noiseVariance));

	//Estimation pipeline
	if(lTree.get_child_optional("EstimationPipeline"))
		parameters.pipelineParameters=InterfaceMultimodelGeometryEstimationPipelineC::MakeParameterObject(lTree.get_child("EstimationPipeline"));

	parameters.inlierRejectionProbability=ReadParameter(lTree, "EstimationPipeline.Main.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(defaultParameters.inlierRejectionProbability));

	if(lTree.get_child_optional("EstimationPipeline.Identification.SequentialRANSAC.RANSAC"))
	{
		map<string, string> ransacExtra=InterfaceRANSACC::ExtractGeometryProblemParameters(lTree.get_child("EstimationPipeline.Identification.SequentialRANSAC.RANSAC"));

		auto itE=ransacExtra.end();

		auto it1=ransacExtra.find("LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatioI=lexical_cast<double>(it1->second);

		auto it2=ransacExtra.find("MORANSAC.MaxAlgebraicDistance");
		if(it2!=itE)
			parameters.moMaxAlgebraicDistanceI=lexical_cast<double>(it2->second);
	}	//if(lTree.get_child_optional("EstimationPipeline.Identification.SequentialRANSAC"))

	if(lTree.get_child_optional("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC"))
	{
		map<string, string> ransacExtra=InterfaceRANSACC::ExtractGeometryProblemParameters(lTree.get_child("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC"));

		auto itE=ransacExtra.end();

		auto it1=ransacExtra.find("LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatioI=lexical_cast<double>(it1->second);

		auto it2=ransacExtra.find("MORANSAC.MaxAlgebraicDistance");
		if(it2!=itE)
			parameters.moMaxAlgebraicDistanceI=lexical_cast<double>(it2->second);

	}	//if(lTree.get_child_optional("EstimationPipeline.Refinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC"))

	//Input
	parameters.cameraFile1=lTree.get<string>("Input.CameraFile1","");
	parameters.featureFile1=lTree.get<string>("Input.FeatureFile1","");

	parameters.featureFile2=lTree.get<string>("Input.FeatureFile2","");
	parameters.cameraFile2=lTree.get<string>("Input.CameraFile2","");

	parameters.gridSpacing=ReadParameter(lTree, "Input.GridSpacing", geq0, "is not >=0", optional<double>(defaultParameters.gridSpacing));

	if(lTree.get_child_optional("Input.RegionOfInterest"))
	{
		vector<double> boundingBox=ReadParameterArray<double>(lTree, "Input.RegionOfInterest", 4, [](const double&){return true;}, "", string(" "));
		parameters.roi=FrameT(Coordinate2DT(boundingBox[0], boundingBox[1]), Coordinate2DT(boundingBox[2], boundingBox[3]));
	}

	//Output
	parameters.outputPath=lTree.get<string>("Output.Root","");
	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("AppMultimodelGeometryEstimator22ImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.modelRoot=lTree.get<string>("Output.Model","");
	parameters.covarianceRoot=lTree.get<string>("Output.Covariance","");
	parameters.correspondenceRoot=lTree.get<string>("Output.Correspondences","");
	parameters.logFile=lTree.get<string>("Output.Log","");
	parameters.flagVerbose=lTree.get<bool>("Output.FlagVerbose", defaultParameters.flagVerbose);

	//Overridden pipeline parameters
	parameters.pipelineParameters.nThreads=nThreads;
	parameters.pipelineParameters.flagVerbose=parameters.flagVerbose;

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Saves the output
 * @param[in] solutions Solution list
 * @param[in] featureList1 First feature set
 * @param[in] featureList2 Second feature set
 */
void AppMultimodelGeometryEstimator22ImplementationC::SaveOutput(const vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2 ) const
{
	size_t c=0;
	for(const auto& current : solutions)
	{
		if(!parameters.modelRoot.empty())
			ArrayIOC<Matrix3d>::WriteArray(parameters.outputPath+parameters.modelRoot+string(".")+lexical_cast<string>(c), get<iModel>(current));

		if(!parameters.covarianceRoot.empty() && get<iCovariance>(current))
			ArrayIOC<MatrixXd>::WriteArray(parameters.outputPath+parameters.covarianceRoot+string(".")+lexical_cast<string>(c), *get<iCovariance>(current));

		if(!parameters.correspondenceRoot.empty())
		{
			vector<Coordinate2DT> coordinateList1=MakeCoordinateVector(featureList1);
			vector<Coordinate2DT> coordinateList2=MakeCoordinateVector(featureList2);
			CoordinateCorrespondenceList2DT correspondenceList=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(get<iInlierList>(current), coordinateList1, coordinateList2);
			CoordinateCorrespondenceIOC::WriteCorrespondenceFile(correspondenceList, parameters.outputPath+parameters.correspondenceRoot+string(".")+lexical_cast<string>(c) );
		}	//if(!parameters.correspondenceFile.empty())

		++c;
	}	//for(const auto& current : solutions)
} //void SaveOutput(const vector<SolutionT>& solutions, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2 ) const

/**
 * @brief Saves the log
 * @param[in] diagnostics Diagnostics object
 */
void AppMultimodelGeometryEstimator22ImplementationC::SaveLog(const MultimodelGeometryEstimationPipelineDiagnosticsC& diagnostics)
{
	ofstream logfile(parameters.outputPath+parameters.logFile);

	if(logfile.fail())
		throw(runtime_error(string("AppMultimodelGeometryEstimator22ImplementationC::SaveOutput: Cannot open ")+parameters.outputPath+parameters.logFile));

	string timeString= to_simple_string(second_clock::universal_time());
	logfile<<"multimodelGeometryEstimator22 log file created on "<<timeString<<"\n";
	logfile<<"# Features in the first set: "<<logger["nFeature1"]<<"\n";
	logfile<<"# Features in the second set: "<<logger["nFeature2"]<<"\n";
	logfile<<"# Models: "<<logger["nModels"]<<"\n";
	logfile<<"Error: "<<diagnostics.error<<"\n";
	logfile<<"Support: "<<diagnostics.support<<"\n";

	logfile<<"Execution time: "<<logger["runtime"]<<"s";
}	//void SaveLog()

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppMultimodelGeometryEstimator22ImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the cameras
	CameraC camera1=LoadCamera(parameters.cameraFile1);
	CameraC camera2=LoadCamera(parameters.cameraFile2);

	//Determine the problem type and verify feasibility
	if(parameters.problemType==ProblemIdentifierT::AUTO)
		parameters.problemType=InferProblemType(camera1, camera2);
	else
		if(!VerifyProblem(camera1, camera2))
			throw(runtime_error( string("AppMultimodelGeometryEstimator22ImplementationC::Run : Camera file is missing some parameters required by the problem. Value=") + problemTypeToString[parameters.problemType]));

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Problem type: "<<problemTypeToString[parameters.problemType]<<"\n";

	//Load and preprocess the features

	vector<FeatureT> featureList1;
	optional<AffineTransform2T> normaliser1;
	LensDistortionC distortion1;
	optional<AffineTransform2T> denormaliser1;

	vector<FeatureT> featureList2;
	optional<AffineTransform2T> normaliser2;
	LensDistortionC distortion2;
	optional<AffineTransform2T> denormaliser2;

#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)
{
	#pragma omp section
	std::tie(featureList1, normaliser1, distortion1, denormaliser1)=LoadImageFeatures(parameters.featureFile1, camera1);

	#pragma omp section
	std::tie(featureList2, normaliser2, distortion2, denormaliser2)=LoadImageFeatures(parameters.featureFile2, camera2);
}	//#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)

	optional<IntrinsicCalibrationMatrixT> mK1;
	optional<IntrinsicCalibrationMatrixT> mK2;

	if(denormaliser1)
		mK1=denormaliser1->matrix();

	if(denormaliser2)
		mK2=denormaliser2->matrix();

	logger["nFeature1"]=featureList1.size();
	logger["nFeature2"]=featureList2.size();

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Loaded "<<featureList1.size()<<" features for the first set, and "<<featureList2.size()<<" for the second. Running the estimation pipeline...\n";

	//Pipeline

	vector<SolutionT> solutions;
	MultimodelGeometryEstimationPipelineDiagnosticsC diagnostics;

    //Homography estimator
    if(parameters.problemType==ProblemIdentifierT::HMG2)
    	diagnostics=ComputeHomography(solutions, featureList1, featureList2);

    //Fundamental matrix estimator
    if(parameters.problemType==ProblemIdentifierT::FND)
    	diagnostics=ComputeFundamentalMatrix(solutions, featureList1, featureList2);

    //Essential matrix estimator
    if(parameters.problemType==ProblemIdentifierT::ESS)
    {
    	if(!mK1)
    		throw(runtime_error("AppMultimodelGeometryEstimator22ImplementationC::Run: Missing intrinsic calibration parameters for the first camera."));

    	if(!mK2)
			throw(runtime_error("AppMultimodelGeometryEstimator22ImplementationC::Run: Missing intrinsic calibration parameters for the second camera."));

    	diagnostics=ComputeEssentialMatrix(solutions, featureList1, featureList2, *mK1, *mK2);
    }	//if(parameters.problemType==Geometry22ProblemT::ESS)

    logger["nModels"]=solutions.size();

    if(parameters.flagVerbose)
	{
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s:";

		if(!diagnostics.flagSuccess)
			cout<<" Geometry estimator failed!\n";
		else
		{
			size_t nModels=solutions.size();
			cout<<" Found "<<nModels<<" models."<<" Error: "<<diagnostics.error<<" Support: "<<diagnostics.support<<"\n";
			for(size_t c=0; c<nModels; ++c)
				cout<<"Model "<<c<<" : Support "<<get<iInlierList>(solutions[c]).size()<<"\n";
		}	//if(!diagnostics.flagSuccess)

	}	//if(parameters.flagVerbose)

    //Denormalise the features
    ImageFeatureIOC::PostprocessFeatureSet(featureList1, distortion1, denormaliser1);
    ImageFeatureIOC::PostprocessFeatureSet(featureList2, distortion2, denormaliser2);

    //Save the output
    SaveOutput(solutions, featureList1,featureList2);

    logger["runtime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

    if(!parameters.logFile.empty())
    	SaveLog(diagnostics);

	return true;
}	//bool Run()

}	//AppMultimodelGeometryEstimator22N
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("150920");
    std::string header("multimodelGeometryEstimator22: Simultaneous estimation of multiple 2D geometric entities");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppMultimodelGeometryEstimator22N::AppMultimodelGeometryEstimator22ImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )

