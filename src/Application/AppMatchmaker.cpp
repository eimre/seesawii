/*
 * @file AppMatchmaker.cpp Implementation of the image feature matching application
 * @author Evren Imre
 * @date 20 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.

 ***************************************************************************/

#include <omp.h>
#include "Application.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Camera.h"
#include "../IO/ImageFeatureIO.h"
#include "../IO/CoordinateCorrespondenceIO.h"
#include "../IO/ArrayIO.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Metrics/Constraint.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Matcher/NearestNeighbourMatcher.h"
#include "../Matcher/FeatureMatcher.h"
#include "../Numeric/NumericFunctor.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Geometry/LensDistortion.h"
#include "../Matcher/GroupMatchingProblem.h"
#include "../Wrappers/BoostBimap.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Wrappers/BoostTokenizer.h"

/*#include <cuda_runtime.h>
#include "cublas_v2.h"

cublasHandle_t cublasHandle;
*/
namespace SeeSawN
{
namespace AppMatchmakerN
{

using namespace ApplicationN;

using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::ComputeBoundingBox2D;
using SeeSawN::ElementsN::MakeFeatureCorrespondenceList;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::CoordinateCorrespondenceIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::DistanceConstraintC;
using SeeSawN::MetricsN::EuclideanDistance2DT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::MetricsN::EuclideanDistanceConstraint2DT;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::MetricsN::SymmetricTransferErrorConstraint2DT;
using SeeSawN::MetricsN::SymmetricTransferErrorH2DT;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::MatcherN::NearestNeighbourMatcherC;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::NumericN::MakeRankN;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::CoordinateStatistics2DT;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;
using SeeSawN::GeometryN::LensDistortionNoDC;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::MatcherN::GroupMatchingProblemC;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Tokenise;

//Notes on behaviour- to be incorporated into the doxygen documentation
//MaxFeatureDistance: A low value reduces the number of match candidates, which, in turn, reduces the effectiveness of the ratio and the consistency tests. The threshold should only throw out the obviously wrong candidates
//MaxRatio: Good for discarding potential outliers, unfortunately, along with serviceable inliers
//NeighbourhoodCardinality: A higher value does not necessarily mean a jump in the number of outliers. It merely shifts the burden of detecting the outliers from the ratio test to the consistency test.
//                          The consistency test is tolerant towards correspondecens with a poor (high) ratio score.
//Max distance for X2 is roughly nbins^0.25
//X2 is a bit better than Euclidean, but twice as slow
//An essential matrix is not a valid constraint: normalised coordinates
//The output ft2 files are normalised
/**
 * @brief Parameters object of MatchmakerImplementationC
 * @ingroup Application
 */
struct MatchmakerParametersC
{
    double maxRatio;    ///< Maximum similarity score ratio threshold
    double maxFeatureDistance;  ///< Maximum distance between two feature vectors, as a percentage of the maximum possible distance
    double neighbourhoodCardinality;    ///< Neighbourhood size

    unsigned int constraintType;    ///< Type of the constraint: 0- None, 1- Euclidean distance, 2- Homography, 3- Epipolar geometry
    double constraintTolerance;    ///< Maximum tolerance for constraint verification
    string constraintFile;  ///< Filename for the matrix specifying a geometric constraint

    bool flagBucketFilter;  ///< \c true = bucket filter enabled
    double minBucketSimilarity; ///< Similarity threshold for the bucket filter
    unsigned int minQuorum; ///< Minimum number of votes for a valid bucket similarity measurement
    double bucketDensity;  ///< Number of buckets per standard deviation

    string filename1;   ///< Filename for the first feature file
    Coordinate2DT ul1;  ///< Upper-left corner of the bounding box in the first image
    Coordinate2DT lr1;  ///< Lower right corner of the bounding box in the first image
    bool flagAutoBoundingBox1;  ///< true: Bounding box is not initialised and should be set automatically
    double spacing1;	///< Grid spacing for the first set
    LensDistortionC distortion1;	///< Lens distortion for the first image

    string filename2;   ///< Filename for the second feature file
    Coordinate2DT ul2; ///< Upper-left corner of the bounding box in the second image
    Coordinate2DT lr2; ///< Lower right corner of the bounding box in the second image
    bool flagAutoBoundingBox2;  ///< true: Bounding box is not initialised and should be set automatically
    double spacing2;     ///< Grid spacing for the second set
    LensDistortionC distortion2;	///< Lens distortion for the second image

    string outputPath;	///< Output path
    string filenameC22; ///< Correspondence file
    string filenameLog; ///< Log file
    string filenameFt21;	///< Feature file for the correspondences from the first set
    string filenameFt22;	///< Feature file for the correspondences from the second set
    bool flagVerbose;   ///< Verbosity flag

    unsigned int nThreads;  ///< Number of threads available to the program
};  //struct MatchmakerParametersC

/**
 * @brief Implementation of matchmaker
 * @ingroup Application
 */
class MatchmakerImplementationC
{
    private:

        auto_ptr<options_description> commandLineOptions; ///< Command line options
                                                          // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

        MatchmakerParametersC parameters;  ///< Application parameters

        typedef ImageFeatureC FeatureT;	///< Feature type
        typedef FeatureT::coordinate_type CoordinateT;	///< Coordinate type

        /**@name Implementation details */ //@{
        vector<FeatureT> LoadFeatures(unsigned int setId) const;	///< Loads and preprocesses the features
        void PostprocessFeatures(vector<ImageFeatureC>& featureList, unsigned int setId) const;	///< Post-processing for the output
        void SaveLog(const FeatureMatcherDiagnosticsC& diagnostics, double runtime) const;   ///< Saves the log
        tuple<vector<CoordinateT>, vector<CoordinateT> > GetCorrespondenceCoordinates(const CorrespondenceListT& correspondences, const vector<FeatureT>& features1, const vector<FeatureT>& features2) const;  ///< Gets the coordinates of the corresponding features
        //@}

    public:

        /**@name Mandatory interface */ //@{
        MatchmakerImplementationC();    ///< Constructor
        const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
        static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
        bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
        bool Run(); ///< Runs the application
        //@}

};  //class MatchmakerImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Loads and preprocesses the features
 * @param[in] setId Id of the feature set. 1 or 2
 */
auto MatchmakerImplementationC::LoadFeatures(unsigned int setId) const -> vector<FeatureT>
{
	bool flagAutoBoundingBox = (setId==1) ? parameters.flagAutoBoundingBox1 : parameters.flagAutoBoundingBox2;
	optional< tuple<CoordinateT, CoordinateT> > boundingBox;
	string filename = (setId==1) ? parameters.filename1 : parameters.filename2;
	double spacing = (setId==1) ? parameters.spacing1 : parameters.spacing2;
	LensDistortionC distortion= (setId==1) ? parameters.distortion1 : parameters.distortion2;
	optional<CoordinateTransformations2DT::projective_transform_type> dummyNormaliser;

	if(!flagAutoBoundingBox)
		boundingBox=make_tuple( (setId==1) ? parameters.ul1 : parameters.ul2, (setId==1) ? parameters.lr1 : parameters.lr2 );

	vector<FeatureT> featureList;
#pragma omp critical (GE22_LF1)
	featureList=ImageFeatureIOC::ReadFeatureFile(filename, true);

	ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, spacing, true, distortion, dummyNormaliser);
	return featureList;
}	//vector<FeatureT> LoadFeatures(unsigned int setId)

/**
 * @brief Post-processing for the output
 * @param[in, out] featureList Feature list to be post-processed
 * @param[in] setId Id of the feature set. 1 or 2
 */
void MatchmakerImplementationC::PostprocessFeatures(vector<ImageFeatureC>& featureList, unsigned int setId) const
{
	LensDistortionC distortion= (setId==1) ? parameters.distortion1 : parameters.distortion2;
	optional<CoordinateTransformations2DT::projective_transform_type> dummyDenormaliser;

	ImageFeatureIOC::PostprocessFeatureSet(featureList, distortion, dummyDenormaliser);
}	//void PostprocessFeatures(vector<ImageFeatureC>& featureList, unsigned int setId) const

/**
 * @brief Saves the log
 * @param[in] diagnostics Diagnostics
 * @param[in] runtime Run time
 * @throws runtime_error If the file cannot be opened
 */
void MatchmakerImplementationC::SaveLog(const FeatureMatcherDiagnosticsC& diagnostics, double runtime) const
{
    string filename=parameters.filenameLog;

    if(filename.empty())
        return;

    filename = parameters.outputPath+filename;
    ofstream logfile(filename);

    if(logfile.fail())
        throw(runtime_error(string("MatchmakerImplementationC::SaveLog : ")+filename+string(" cannot be opened for writing.")));

    string timeString= to_simple_string(second_clock::universal_time());

    logfile<<"Matchmaker log file created on "<<timeString<<"\n";
    logfile<<"Number of features in the first set :"<<diagnostics.nnDiagnostics.sSet1<<"\n";
    logfile<<"Number of features in the second set :"<<diagnostics.nnDiagnostics.sSet2<<"\n";
    logfile<<"Number of correspondences before the bucket filter:"<<diagnostics.nnDiagnostics.sCorrespondences<<"\n";
    logfile<<"Number of correspondences after the bucket filter:"<<diagnostics.sCorrespondences<<"\n";
    logfile<<"Runtime:"<<runtime<<"s";
}   //void SaveLog(const NearestNeighbourMatcherDiagnosticsC& diagnostics)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
MatchmakerImplementationC::MatchmakerImplementationC()
{
    commandLineOptions.reset(new(options_description)("Application command line options"));
    commandLineOptions->add_options()
    ("Environment.NumThreads", value<string>()->default_value("1"), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")

    ("Matcher.FeatureMatcher.MaxRatio", value<string>()->default_value("0.8"), "Maximum ratio of the similarity score of the best outsider to that of the best insider. See the documentation of NearestNeighbourMatcherC. [0,1]")
    ("Matcher.FeatureMatcher.MaxFeatureDistance", value<string>()->default_value("0.5"), "Maximum distance between the normalised descriptor vectors of two corresponding points, as a percentage of the maximum possible distance.[0 1]")
    ("Matcher.FeatureMatcher.NeighbourhoodCardinality", value<string>()->default_value("1"), "k in k-NN: Each feature can be associated with up to k of its nearest neighbours. >0")
    ("Matcher.BucketFilter.Enable", value<string>()->default_value("1"), "If true, the bucket filter is enabled. Useful for eliminating outliers. 0 or 1")
    ("Matcher.BucketFilter.MinSimilarity", value<string>()->default_value("0.5"), "Minimum similarity between two buckets for a valid bucket match. [0,1]")
    ("Matcher.BucketFilter.MinQuorum", value<string>()->default_value("3"), "If no buckets in a pair has this many votes, the pair is exempted due to insufficient evidence for rejection (i.e., automatic success).>=0")
	("Matcher.BucketFilter.BucketDensity", value<string>()->default_value("10"), "Number of buckets, per unit standard deviation. >0")
    ("Matcher.Constraint.Enable", value<string>()->default_value("0"), "If true, a geometric conditions constrains the location of possible correspondences. 0 or 1")
    ("Matcher.Constraint.Type", value<string>()->default_value("Distance"), "Type of the constraint: Distance, Homography or Fundamental.")
    ("Matcher.Constraint.Filename", value<string>(), "A file that holds a 3x3 matrix. Ignored in Distance.")
    ("Matcher.Constraint.NoiseVariance", value<string>()->default_value("4"), "Variance of the noise on the coordinates, in pixel^2. Ignored in Distance.>=0" )
    ("Matcher.Constraint.pValue", value<string>()->default_value("0.05"), "Probability of rejecting an inlier. Lower values mean higher tolerance in Homography and Fundamental. Ignored in Distance.[0,1]")
    ("Matcher.Constraint.MaxImageDistance", value<string>()->default_value("1e16"), "Maximum Euclidean distance between two corresponding features, in pixels. Tolerance in Distance, ignored in Homography and Fundamental. >=0")

    ("FeatureSet1.Filename", value<string>(), ".ft2 file containing the first feature list" )
    ("FeatureSet1.RoI", value<string>(), "Region-of-interest in the first image, in pixels. If not specified, bounding box of the features. [left upper right lower]")
    ("FeatureSet1.GridSpacing", value<string>()->default_value("0"), "Affects the decimation factor of the initial set. 0 means no decimation. See the documentation of DecimateFeatures2D. >=0")
    ("FeatureSet1.Distortion.Enable", value<string>()->default_value("0"), "If 0, no lens distortion is assumed. 0 or 1")
    ("FeatureSet1.Distortion.Model", value<string>()->default_value("FullPolynomial"), "Distortion model. Division, FullPolynomial, OddPolynomial")
    ("FeatureSet1.Distortion.Centre", value<string>()->default_value("960 540"), "Distortion centre. Should be the centre of the image")
    ("FeatureSet1.Distortion.Coefficient", value<string>()->default_value("0"), "Distortion coefficient.")

    ("FeatureSet2.Filename", value<string>(), ".ft2 file containing the second feature list" )
    ("FeatureSet2.RoI", value<string>(), "Region-of-interest in the second image, in pixels. If not specified, bounding box of the features. [left upper right lower]")
    ("FeatureSet2.GridSpacing", value<string>()->default_value("0"), "Affects the decimation factor of the initial set. 0 means no decimation. See the documentation of DecimateFeatures2D. >=0")
    ("FeatureSet2.Distortion.Enable", value<string>()->default_value("0"), "If 0, no lens distortion is assumed. 0 or 1")
    ("FeatureSet2.Distortion.Model", value<string>()->default_value("FullPolynomial"), "Distortion model. Division, FullPolynomial, OddPolynomial")
    ("FeatureSet2.Distortion.Centre", value<string>()->default_value("960 540"), "Distortion centre. Should be the centre of the image")
    ("FeatureSet2.Distortion.Coefficient", value<string>()->default_value("0"), "Distortion coefficient.")

    ("Output.Root", value<string>(), "Root directory for the output files")
    ("Output.Correspondences", value<string>(), "Correspondences as a .c22 file.")
    ("Output.Log", value<string>()->default_value(""), "Log file")
    ("Output.FeatureList1", value<string>()->default_value(""), "Corresponding features from the first set")
    ("Output.FeatureList2", value<string>()->default_value(""), "Corresponding features from the second set")
    ("Output.Verbose", value<string>()->default_value("1"), "Verbose output")
    ;
}   //MatchmakerImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& MatchmakerImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void MatchmakerImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
    ptree tree; //Options tree

    tree.put("xmlcomment", "matchmaker configuration file");

    tree.put("Environment", "");
    tree.put("Environment.NumThreads", "1");

    tree.put("Matcher", "");

    tree.put("Matcher.FeatureMatcher","");
    tree.put("Matcher.FeatureMatcher.NeighbourhoodCardinality", "1");

    tree.put("Matcher.BucketFilter","");

    tree.put("Matcher.Constraint","");

    tree.put("FeatureSet1", "");
    tree.put("FeatureSet1.Filename", "/home/f1.ft2");

    tree.put("FeatureSet1.Distortion", "");

    tree.put("FeatureSet2", "");
    tree.put("FeatureSet2.Filename", "/home/f1.ft2");

    tree.put("FeatureSet2.Distortion", "");

    tree.put("Output", "");
    tree.put("Output.Root","/home/");
    tree.put("Output.Correspondences", "out.c22");
	tree.put("Output.Log", "out.log");

    //Advanced options
    if(detailLevel>1)
    {
        tree.put("Matcher.FeatureMatcher.MaxRatio", "0.8");
        tree.put("Matcher.FeatureMatcher.MaxFeatureDistance", "0.5");

        tree.put("Matcher.BucketFilter.Enable", "1");
        tree.put("Matcher.BucketFilter.MinSimilarity","0.5");
        tree.put("Matcher.BucketFilter.MinQuorum","3");
		tree.put("Matcher.BucketFilter.BucketDensity","10");

        tree.put("Matcher.Constraint.Enable", "0");
        tree.put("Matcher.Constraint.Type", "Distance");
        tree.put("Matcher.Constraint.Filename", "/home/mF.txt");
        tree.put("Matcher.Constraint.NoiseVariance", "4");
        tree.put("Matcher.Constraint.pValue", "0.05");
        tree.put("Matcher.Constraint.MaxImageDistance", lexical_cast<string>(numeric_limits<double>::max()) );

        tree.put("FeatureSet1.RoI", "0 0 1919 1079");
        tree.put("FeatureSet1.GridSpacing", "0");

        tree.put("FeatureSet1.Distortion.Enable", "0");
        tree.put("FeatureSet1.Distortion.Model", "FullPolynomial");
        tree.put("FeatureSet1.Distortion.Centre", "960 540");
        tree.put("FeatureSet1.Distortion.Coefficient", "0");

        tree.put("FeatureSet2.RoI", "0 0 1919 1079");
        tree.put("FeatureSet2.GridSpacing", "0");

        tree.put("FeatureSet2.Distortion.Enable", "0");
        tree.put("FeatureSet2.Distortion.Model", "FullPolynomial");
        tree.put("FeatureSet2.Distortion.Centre", "960 540");
        tree.put("FeatureSet2.Distortion.Coefficient", "0");

        tree.put("Output.FeatureList1", "c1.ft2");
        tree.put("Output.FeatureList2", "c2.ft2");
        tree.put("Output.Verbose", "1");
    }	//if(!flagBasic)

    xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
    write_xml(filename, tree, locale(), settings);
}   //void GenerateConfigFile(const string& filename)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool MatchmakerImplementationC::SetParameters(const ptree& tree)
{
    //Set and validate the parameters

    //Environment
    parameters.nThreads=min(omp_get_max_threads(), max(1, tree.get<int>("Environment.NumThreads", 1)));

    //Matcher

    //Feature matcher

    parameters.maxRatio=tree.get<double>("Matcher.FeatureMatcher.MaxRatio", 0.8);
    if(parameters.maxRatio<0 || parameters.maxRatio>1)
        throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.FeatureMatcher.MaxRatio is not within [0, 1]. Value=")+lexical_cast<string>(parameters.maxRatio) ) );

    parameters.maxFeatureDistance=tree.get<double>("Matcher.FeatureMatcher.MaxFeatureDistance", 0.5);
    if(parameters.maxRatio<0 || parameters.maxRatio>1)
        throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.FeatureMatcher.MaxFeatureDistance is not within [0, 1]. Value=")+lexical_cast<string>(parameters.maxFeatureDistance) ) );

    parameters.neighbourhoodCardinality=tree.get<unsigned int>("Matcher.FeatureMatcher.NeighbourhoodCardinality", 1);

    //Bucket filter

    parameters.flagBucketFilter=tree.get<bool>("Matcher.BucketFilter.Enable", true);

    parameters.minBucketSimilarity=tree.get<double>("Matcher.BucketFilter.MinSimilarity", 0.5);
    if(parameters.minBucketSimilarity<0 || parameters.minBucketSimilarity>1)
        throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.BucketFilter.MinSimilarity is not in [0,1]. Value=")+lexical_cast<string>(parameters.minBucketSimilarity)));

    parameters.minQuorum=tree.get<unsigned int> ("Matcher.BucketFilter.MinQuorum", 3);

    parameters.bucketDensity=tree.get<double>("Matcher.BucketFilter.BucketDensity", 0.1);
    if(parameters.bucketDensity<=0)
        throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.BucketFilter.BucketDensity is not positive. Value=")+lexical_cast<string>(parameters.bucketDensity)));

    //Constraint

    bool flagConstraint=tree.get<bool>("Matcher.Constraint.Enable", false);

    if(!flagConstraint)
    {
        parameters.constraintType=0;
        parameters.constraintTolerance=numeric_limits<double>::infinity();
        parameters.constraintFile="";
    }   //if(!flagConstraint)
    else
    {
        string constraintType=tree.get<string>("Matcher.Constraint.Type", "");
        map<string, unsigned int> validTypes{ {"Distance",1}, {"Homography",2}, {"Fundamental",3}};

        auto it=validTypes.find(constraintType);
        if(it==validTypes.end())
            throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Invalid constraint type. Value=")+constraintType));

        parameters.constraintType=it->second;
        parameters.constraintFile=tree.get<string>("Matcher.Constraint.Filename", "");

        if(parameters.constraintType==1)
        {
            parameters.constraintTolerance=tree.get<double>("Matcher.Constraint.MaxImageDistance", numeric_limits<double>::infinity());

            if(parameters.constraintTolerance<0)
                throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.Constraint.MaxImageDistance is negative. Value=")+lexical_cast<string>(parameters.constraintTolerance) ) );
        }   //if(parameters.constraintType==1)

        if(parameters.constraintType==2 || parameters.constraintType==3)
        {
            double noise=tree.get<double>("Matcher.Constraint.NoiseVariance", 4);

            if(noise<0)
                throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.Constraint.NoiseVariance is negative. Value=")+lexical_cast<string>(noise) ) );

            double pValue=tree.get<double>("Matcher.Constraint.pValue", 0.05);

            if(pValue<0 && pValue>1)
                throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: Matcher.Constraint.pValue is not in [0,1]. Value=")+lexical_cast<string>(pValue) ) );

            parameters.constraintTolerance = (parameters.constraintType==2) ? SymmetricTransferErrorH2DT::ComputeOutlierThreshold(pValue, noise) : EpipolarSampsonErrorC::ComputeOutlierThreshold(pValue, noise);
        }   //if(parameters.constraintType==2 || parameters.constraintType==3)
    }   //if(!flagConstraint)

    //FeatureSet1

    parameters.filename1=tree.get<string>("FeatureSet1.Filename", "");

    optional<string> roi1=tree.get_optional<string>("FeatureSet1.RoI");
    if(!roi1)
        parameters.flagAutoBoundingBox1=true;
    else
    {
    	vector<double> boundingBox1=Tokenise<double>(*roi1, " ");
        parameters.flagAutoBoundingBox1= boundingBox1.size()!=4;

        if(!parameters.flagAutoBoundingBox1)
        {
            parameters.ul1=Coordinate2DT(boundingBox1[0], boundingBox1[1]);
            parameters.lr1=Coordinate2DT(boundingBox1[2], boundingBox1[3]);
        }   //if(!parameters.flagAutoBoundingBox1)
    }   //if(!roi1)

    parameters.spacing1=tree.get<double>("FeatureSet1.GridSpacing", 7);
    if(parameters.spacing1<0)
       throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: FeatureSet1.GridSpacing is negative. Value=")+lexical_cast<string>(parameters.spacing1) ) );

    map<string, LensDistortionCodeT> distortionLabelToCode{{"None", LensDistortionCodeT::NOD}, {"Division", LensDistortionCodeT::DIV}, {"FullPolynomial", LensDistortionCodeT::FP1}, {"OddPolynomial", LensDistortionCodeT::OP1}};

    bool flagDistortion1 = flagConstraint && tree.get<bool>("FeatureSet1.Distortion.Enable", false);
    string distortionLabel1= flagDistortion1 ?  tree.get<string>("FeatureSet1.Distortion.Model", "") : "None";
    auto itDistortion1=distortionLabelToCode.find(distortionLabel1);
    if(itDistortion1==distortionLabelToCode.end())
    	throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: FeatureSet1.Distortion.Model has an invalid value. Value=")+distortionLabel1));

    double distortionCoefficient1=tree.get<double>("FeatureSet1.Distortion.Coefficient", 0);

    string sDC1=tree.get<string>("FeatureSet1.Distortion.Centre", "0 0");
    vector<double> distortionCentre1=Tokenise<double>(sDC1, " ");
    if(distortionCentre1.size()!=2)
    	throw(string("MatchmakerImplementationC::SetParameters: FeatureSet1.Distortion.Centre must have two elements. Value=")+sDC1);

    parameters.distortion1.kappa=distortionCoefficient1;
    parameters.distortion1.modelCode=itDistortion1->second;
    parameters.distortion1.centre=Coordinate2DT(distortionCentre1[0], distortionCentre1[1]);

    //FeatureSet2

    parameters.filename2=tree.get<string>("FeatureSet2.Filename", "");

    optional<string> roi2=tree.get_optional<string>("FeatureSet2.RoI");
    if(!roi2)
        parameters.flagAutoBoundingBox2=true;
    else
    {
    	vector<double> boundingBox2=Tokenise<double>(*roi2, " ");
        parameters.flagAutoBoundingBox2= boundingBox2.size()!=4;

        if(!parameters.flagAutoBoundingBox2)
        {
            parameters.ul2=Coordinate2DT(boundingBox2[0], boundingBox2[1]);
            parameters.lr2=Coordinate2DT(boundingBox2[2], boundingBox2[3]);
        }   //if(!parameters.flagAutoBoundingBox1)
    }   //if(!roi2)

    parameters.spacing2=tree.get<double>("FeatureSet2.GridSpacing", 7);
    if(parameters.spacing2<0)
       throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: FeatureSet2.GridSpacing is negative. Value=")+lexical_cast<string>(parameters.spacing2) ) );

    bool flagDistortion2 = flagConstraint && tree.get<bool>("FeatureSet2.Distortion.Enable", false);
    string distortionLabel2= flagDistortion2 ?  tree.get<string>("FeatureSet2.Distortion.Model", "") : "None";
    auto itDistortion2=distortionLabelToCode.find(distortionLabel2);
    if(itDistortion2==distortionLabelToCode.end())
    	throw(invalid_argument(string("MatchmakerImplementationC::SetParameters: FeatureSet2.Distortion.Model has an invalid value. Value=")+distortionLabel2));

    double distortionCoefficient2=tree.get<double>("FeatureSet2.Distortion.Coefficient", 0);

    string sDC2=tree.get<string>("FeatureSet2.Distortion.Centre", "0 0");
    vector<double> distortionCentre2=Tokenise<double>(sDC2, " ");
    if(distortionCentre2.size()!=2)
    	throw(string("MatchmakerImplementationC::SetParameters: FeatureSet2.Distortion.Centre must have two elements. Value=")+ sDC2);

    parameters.distortion2.kappa=distortionCoefficient2;
    parameters.distortion2.modelCode=itDistortion2->second;
    parameters.distortion2.centre=Coordinate2DT(distortionCentre2[0], distortionCentre2[1]);

    //Output
	parameters.outputPath=tree.get<string>("Output.Root","");

	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("MatchmakerImplementationC::Run: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

    parameters.filenameC22=tree.get<string>("Output.Correspondences", "");
    parameters.filenameLog=tree.get<string>("Output.Log", "");
    parameters.filenameFt21=tree.get<string>("Output.FeatureList1","");
    parameters.filenameFt22=tree.get<string>("Output.FeatureList2","");
    parameters.flagVerbose=tree.get<bool>("Output.Verbose", true);

    return true;
}   //bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool MatchmakerImplementationC::Run()
{
    auto t0=high_resolution_clock::now();   //Start the timer

    if(parameters.flagVerbose)
    {
    	cout<< "Running on "<<parameters.nThreads<<" threads\n";
        cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the features...\n";
    }	//if(parameters.flagVerbose)

    vector<FeatureT> featureList1;
	vector<FeatureT> featureList2;

#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)
{
	#pragma omp section
	{
		featureList1=LoadFeatures(1);
		if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Set 1 has "<<featureList1.size()<<" features \n";
	}

	#pragma omp section
    {
    	featureList2=LoadFeatures(2);
    	if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Set 2 has "<<featureList2.size()<<" features. Matching...\n";
    }	//#pragma omp section
}	//#pragma omp parallel sections if(parameters.flagMultithreaded)

    if(parameters.flagVerbose && (featureList1.empty() || featureList2.empty()))
        cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Empty feature set encountered. Terminating...\n";

    //Solve the matching problem

    typedef ReciprocalC<EuclideanDistanceIDT::result_type> MapperT;
    typedef InverseEuclideanIDConverterT SimilarityMetricT;
    MapperT distanceToSimilarityMap;
    SimilarityMetricT similarityMetric(EuclideanDistanceIDT(), distanceToSimilarityMap);
    double maxPossibleDistance=sqrt(2);

    FeatureMatcherParametersC matcherParameters;
    matcherParameters.nnParameters.flagConsistencyTest=true;
    matcherParameters.nnParameters.neighbourhoodSize12=parameters.neighbourhoodCardinality;
    matcherParameters.nnParameters.neighbourhoodSize21=parameters.neighbourhoodCardinality;
    matcherParameters.nnParameters.ratioTestThreshold=parameters.maxRatio;
    matcherParameters.nnParameters.similarityTestThreshold=distanceToSimilarityMap(parameters.maxFeatureDistance * maxPossibleDistance);
    matcherParameters.flagStatic=true;
    matcherParameters.nThreads=parameters.nThreads;
    matcherParameters.flagBucketFilter=parameters.flagBucketFilter;
    matcherParameters.bucketSimilarityThreshold=parameters.minBucketSimilarity;
    matcherParameters.bucketDimensions.fill(1.0/parameters.bucketDensity);
    matcherParameters.minQuorum=parameters.minQuorum;

    //Select the required matching constraint, and run the matcher
    CorrespondenceListT correspondences;    //Correspondence indices
    FeatureMatcherDiagnosticsC diagnostics; //Diagnostics
    switch(parameters.constraintType)
    {
        //Symmetric transfer error constraint: Translations, rotations, planar structures
        case 2:
        {
            typedef SymmetricTransferErrorH2DT::projector_type MatrixT;
            MatrixT mH=ArrayIOC<MatrixT>::ReadArray(parameters.constraintFile, 3, 3);

            SymmetricTransferErrorH2DT error(mH);
            optional<SymmetricTransferErrorConstraint2DT> constraint=SymmetricTransferErrorConstraint2DT(error, parameters.constraintTolerance, parameters.nThreads);

            typedef ImageFeatureMatchingProblemC<SimilarityMetricT, SymmetricTransferErrorConstraint2DT> ProblemT;
            optional<ProblemT> problem;

            if(parameters.flagVerbose)
                cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Homography constraint, tolerance "<<parameters.constraintTolerance<<" pixels\n";

            typedef FeatureMatcherC<ProblemT> FeatureMatcherT;
            diagnostics=FeatureMatcherT::Run(correspondences, problem, matcherParameters, similarityMetric, constraint, featureList1, featureList2);

            break;
        }   //case 2:

        //Epipolar constraint constraint: General 3D scenes
        case 3:
        {
            typedef EpipolarSampsonErrorC::projector_type MatrixT;
            MatrixT mF=ArrayIOC<MatrixT>::ReadArray(parameters.constraintFile, 3, 3);

            unsigned int rankF;
            tie(rankF, mF)=MakeRankN<Eigen::NoQRPreconditioner>(mF, 2);

            EpipolarSampsonErrorC error(mF);
            optional<EpipolarSampsonConstraintT> constraint=EpipolarSampsonConstraintT(error, parameters.constraintTolerance, parameters.nThreads);

            typedef ImageFeatureMatchingProblemC<SimilarityMetricT, EpipolarSampsonConstraintT> ProblemT;
            optional<ProblemT> problem;

            if(parameters.flagVerbose)
                cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Epipolar constraint, tolerance "<<parameters.constraintTolerance<<" pixels\n";

            typedef FeatureMatcherC<ProblemT> FeatureMatcherT;
            diagnostics=FeatureMatcherT::Run(correspondences, problem, matcherParameters, similarityMetric, constraint, featureList1, featureList2);

            break;
        }   //case 3:

        //Euclidean distance: A general-purpose constraint, when not much is known about the underlying transformation
        default:
        {
            optional<EuclideanDistanceConstraint2DT> constraint;

            if(parameters.constraintType==1)
            {
                constraint=EuclideanDistanceConstraint2DT(EuclideanDistance2DT(), parameters.constraintTolerance, parameters.nThreads);

                if(parameters.flagVerbose)
                    cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Distance constraint, tolerance "<<parameters.constraintTolerance<<" pixels\n";
            }   //if(parameters.constraintType==1)

            typedef ImageFeatureMatchingProblemC<SimilarityMetricT, EuclideanDistanceConstraint2DT> ProblemT;
            optional<ProblemT> problem;

            typedef FeatureMatcherC<ProblemT> FeatureMatcherT;
            diagnostics=FeatureMatcherT::Run(correspondences, problem, matcherParameters, similarityMetric, constraint, featureList1, featureList2);

            break;
        }   //default:
    }

    if(parameters.flagVerbose)
    {
    	cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: ";
    	if(diagnostics.flagSuccess)
    		 cout<<"Found "<<correspondences.size()<<" correspondences. Saving...\n";
    	else
    	{
    		cout<<"Matching process failed. Terminating.\n";
    		return false;
    	}
    }	//if(parameters.flagVerbose)
    //Output

    //Apply back the lens distortion, and convert to coordinate list
    vector<CoordinateT> coordinates1;
    vector<CoordinateT> coordinates2;
#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)
{
	#pragma omp section
	{
    PostprocessFeatures(featureList1, 1);
	coordinates1=MakeCoordinateVector(featureList1);
	}

	#pragma omp section
	{
	PostprocessFeatures(featureList2, 2);
	coordinates2=MakeCoordinateVector(featureList2);
	}	//#pragma omp section
}	//#pragma omp parallel sections if(parameters.flagMultithreaded)
    CoordinateCorrespondenceList2DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(correspondences, coordinates1, coordinates2);

    double runtime=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;
    SaveLog(diagnostics, runtime);
    CoordinateCorrespondenceIOC::WriteCorrespondenceFile(coordinateCorrespondences, parameters.outputPath+parameters.filenameC22);

    if(!parameters.filenameFt21.empty() || !parameters.filenameFt22.empty())
    {
    	MakeFeatureCorrespondenceList(featureList1, featureList2, correspondences);

    	if(!parameters.filenameFt21.empty())
    		ImageFeatureIOC::WriteFeatureFile(featureList1, parameters.outputPath+parameters.filenameFt21);

    	if(!parameters.filenameFt22.empty())
			ImageFeatureIOC::WriteFeatureFile(featureList2, parameters.outputPath+parameters.filenameFt22);
    }	//if(!parameters.filenameFt21.empty() || !parameters.filenameFt22.empty())

    if(parameters.flagVerbose)
        cout<<runtime<<"s: Finished!\n";

    return true;
}   //bool Run()

}   //AppMatchmakerN
}   //SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("130820");
    std::string header("matchmaker: Image feature matching");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppMatchmakerN::MatchmakerImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
