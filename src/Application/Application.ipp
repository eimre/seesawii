/**
 * @file Application.ipp Implementation of ApplicationC
 * @author Evren Imre
 * @date 19 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef APPLICATION_IPP_5462123
#define APPLICATION_IPP_5462123

#include <boost/concept_check.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional.hpp>
#include <string>
#include <iostream>
#include <memory>
#include <climits>
#include "ApplicationImplementationConcept.h"

//Common includes, for convenience. Not directly needed here
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <chrono>
#include <stdexcept>
#include <locale>
#include <iterator>
#include <vector>
#include <list>
#include <array>
#include <tuple>
#include <cmath>
#include <map>
#include <set>
#include <random>
#include "../ApplicationInterface/InterfaceUtility.h"

namespace SeeSawN
{
namespace ApplicationN
{

namespace xml_parser=boost::property_tree::xml_parser;

using boost::program_options::options_description;
using boost::program_options::positional_options_description;
using boost::program_options::value;
using boost::program_options::variables_map;
using boost::program_options::store;
using boost::program_options::notify;
using boost::program_options::command_line_parser;
using boost::property_tree::ptree;
using boost::property_tree::xml_parser::read_xml;
using boost::property_tree::ini_parser::read_ini;
using boost::property_tree::info_parser::read_info;
using boost::property_tree::json_parser::read_json;
using boost::optional;
using std::string;
using std::cout;
using SeeSawN::ApplicationN::ApplicationImplementationConceptC;

//Common functions, not directly needed here
using boost::lexical_cast;
using boost::tokenizer;
using boost::char_separator;
using boost::property_tree::xml_parser::write_xml;
using boost::property_tree::xml_parser::xml_writer_settings;
using boost::posix_time::second_clock;
using boost::posix_time::to_simple_string;
using std::runtime_error;
using std::invalid_argument;
using std::domain_error;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::seconds;
using std::chrono::nanoseconds;
using std::chrono::duration;
using std::uniform_real_distribution;
using std::auto_ptr;
using std::ifstream;
using std::ofstream;
using std::locale;
using std::numeric_limits;
using std::distance;
using std::advance;
using std::next;
using std::vector;
using std::map;
using std::set;
using std::list;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;
using std::pow;
using std::sqrt;
using std::array;
using std::min;
using std::max;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::ReadParameterArray;

/**
 * @brief Driver for an application implementation
 * @tparam ImplementationT An application implementation
 * @pre \c ImplementationT is a model of ApplicationImplementationConceptC
 * @remarks Infrastructure for reading the command line parameters, and basic operations
 * @remarks The interface can read parameters from .xml, .json., .ini and .info files.
 * @ingroup Application
 */
template<class ImplementationT>
class ApplicationC
{
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((ApplicationImplementationConceptC<ImplementationT>));
    //@endcond

    public:
        static bool Run(int argc, char* argv[], const string& helpHeader, const string& version);  ///< Runs the application with the command line options

};  //class ApplicationC

/**
 * @brief Runs the application with the command line options
 * @param[in] argc Command line argument count
 * @param[in] argv Command line argument string
 * @param[in] helpHeader Header for the help text
 * @param[in] version Version
 * @return \c false if the application fails
 * @remarks Precedence: Command line > Parameter file > Defaults
 */
template<class ImplementationT>
bool ApplicationC<ImplementationT>::Run(int argc, char* argv[], const string& helpHeader, const string& version)
{
    //Describe the options
    options_description genericOptions ( "Generic options" );
    genericOptions.add_options()
    ( "help,h", "Displays this message" )
    ( "version,v", "Program version")
    ( "config-xml,c", value<string>(), "Configuration file, XML format" )
    ( "config-ini", value<string>(), "Configuration file, INI format" )
    ( "config-info", value<string>(), "Configuration file, INFO format described in boost.Property Tree" )
    ( "config-json", value<string>(), "Configuration file, JSON format" )
    ( "generate-config, g", value<string>(), "Generates a fresh configuration file with a user-specified name. Most parameters." )
    ( "generate-config-basic,b", value<string>(), "Generates a fresh configuration file with a user-specified name. Simplified interface." )
    ( "generate-config-full,f", value<string>(), "Generates a fresh configuration file with a user-specified name. All parameters." )
    ;

    //If no option names are specified, the default is config
    positional_options_description positionalOptions;
    positionalOptions.add ( "config-xml", -1 );

    //Initialize the application
    ImplementationT implementation;

    //Get the option description and the variable map of the application
    options_description appOptions(implementation.GetCommandLineOptions()); //The application does not have to expose all of its parameters through the command line

    //Create a variable map with the default values
    char* dummyArgV=new char[1];    //Empty, dummy argv
    dummyArgV[0]='\0';
    
    variables_map defaultValues;
    store(command_line_parser(1, &dummyArgV).options(appOptions).allow_unregistered().run(), defaultValues);    //Simulate an empty command line to get the defaults    
    notify(defaultValues);
    
    delete[] dummyArgV;
    
    //Combined generic and application options
    options_description combinedOptions;
    combinedOptions.add(genericOptions).add(appOptions);

    //Parse the command line
    variables_map values;
    store(command_line_parser(argc,argv).options(combinedOptions).positional(positionalOptions).allow_unregistered().run(), values );
    notify(values);

    //Generic options

    //Help
    if (values.count("help")!=0)
    {
        cout << helpHeader << "\n"; //Header
        cout << combinedOptions << "\n"; //Command line values help
        return true;
    }   //if (values.count("help")!=0)

    //Version
    if (values.count("version")!=0)
    {
        cout<<version<<"\n";
        return true;
    }   //if (values.count("version")!=0)

   //Generate a new config file
   if ( values.count ( "generate-config" ) != 0 )
   {
       ImplementationT::GenerateConfigFile ( values["generate-config"].as<string>(), 2 );
       return true;
   }    //if ( values.count ( "generate-config" ) != 0 )

   if ( values.count ( "generate-config-basic" ) != 0 )
   {
       ImplementationT::GenerateConfigFile ( values["generate-config-basic"].as<string>(), 1 );
       return true;
   }    //if ( values.count ( "generate-config" ) != 0 )

   if ( values.count ( "generate-config-full" ) != 0 )
   {
	   ImplementationT::GenerateConfigFile ( values["generate-config-full"].as<string>(), numeric_limits<int>::max() );
	   return true;
   }    //if ( values.count ( "generate-config" ) != 0 )


    //Read the configuration file, if any. Can handle different formats

    ptree optionTree;

    if (values.count("config-xml")!=0)
        read_xml(values["config-xml"].as<string>(), optionTree, xml_parser::no_comments | xml_parser::trim_whitespace);

    if (values.count("config-ini")!=0)
        read_ini(values["config-ini"].as<string>(), optionTree);

    if (values.count("config-info")!=0)
        read_info(values["config-info"].as<string>(), optionTree);

    if (values.count("config-json")!=0)
        read_json(values["config-json"].as<string>(), optionTree);

    //Insert the command line options into the tree
    //variables_map is derived from stl map!
    string optionName;
    string optionValue;
    auto itE=defaultValues.end();
    for(const auto& current : values)
    {
        optionName=current.first;
        optionValue=current.second.as<string>();

        //Is there a default
        auto itDefault=defaultValues.find(optionName);
        if(itDefault!=itE)
        {
            //Default not overwritten by the command line
            if(optionValue.compare(itDefault->second.as<string>())==0)
                if(optionTree.get_optional<string>(optionName))    //There is already a value in the file
                    continue;   //Skip
        }   //if(itDefault!=itE)

        optionTree.put(optionName, optionValue);    //Save
    }   //for(const auto& current : values)

    bool flagValid=implementation.SetParameters(optionTree);   //Failure indicates invalid parameters or incomplete parameter set

    //Run the application
    if(flagValid)
        return implementation.Run();
    else    //Print the help text
    {
        cout << "ApplicationC::Run : Incomplete or invalid parameter set! \n";
        cout << helpHeader << "\n"; //Header
        cout << combinedOptions << "\n"; //Command line values help
        return true;
    }   //if(flagValid)

   return true;
}   //bool Run(int argc, char* argv[], const string& helpHeader, const string& version)

}   //ApplicationN
}	//SeeSawN

//Extern templates
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
#endif /* APPLICATION_IPP_5462123 */
