/**
 * @file AppNodalCameraTracker.cpp Implementation of nodalCameraTracker
 * @author Evren Imre
 * @date 5 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/math/special_functions.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include <functional>
#include "Application.h"
#include "../ApplicationInterface/InterfaceNodalCameraTrackingPipeline.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../IO/CameraIO.h"
#include "../IO/SceneFeatureIO.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../GeometryEstimationPipeline/NodalCameraTrackingPipeline.h"
#include "../GeometryEstimationComponents/P2PComponents.h"
#include "../GeometryEstimationComponents/P2PfComponents.h"
#include "../GeometryEstimationComponents/GenericPnPComponents.h"
#include "../Matcher/SceneImageFeatureMatchingProblem.h"
#include "../Metrics/GeometricConstraint.h"

namespace SeeSawN
{
namespace AppNodalCameraTrackerN
{

using namespace ApplicationN;

using boost::format;
using boost::str;
using boost::property_tree::ptree;
using boost::property_tree::write_xml;
using boost::optional;
using boost::for_each;
using boost::math::pow;
using Eigen::Matrix3d;
using std::greater;
using std::greater_equal;
using std::bind;
using std::tuple;
using std::ignore;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineC;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineDiagnosticsC;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineParametersC;
using SeeSawN::GeometryN::P2PEstimatorProblemT;
using SeeSawN::GeometryN::P2PfEstimatorProblemT;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::BinaryImageFeatureIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::ION::SceneFeatureIOC;
using SeeSawN::MatcherN::SceneImageFeatureMatchingProblemC;
using SeeSawN::MatcherN::BinarySceneImageFeatureMatchingProblemC;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::MetricsN::InverseHammingSceneImageFeatureDistanceConverterT;
using SeeSawN::MetricsN::InverseEuclideanSceneImageFeatureDistanceConverterT;

/**
 * @brief Parameters for nodalCameraTracker
 * @ingroup Application
 */
struct AppNodalCameraTrackerParametersC
{
	//General
	unsigned int nThreads;	///< Number of threads available to the application. >0
	int seed;	///< Random number generator seed

	//Input
	string featureFilePattern;	///< Filename pattern for the feature files
	size_t initialFrame;	///< Id of the first frame
	size_t nFrames;	///< Number of frames
	string worldFile;	///< File keeping the world map
	bool flagBinary;	///< \c true if binary features
	string referenceCameraFile;	///< Camera filename for the known camera parameters
	double gridSpacing;	///< Minimum distance two neighbouring image features

	//Output
	string outputPath;	///< Output path
	string cameraTrackFile;	///< Camera trajectory
	string logFile;	///< Filename for the log file
	bool flagVerbose;   ///< Verbosity flag

	NodalCameraTrackingPipelineParametersC pipelineParameters;

	AppNodalCameraTrackerParametersC() : nThreads(1), seed(0), initialFrame(0), nFrames(0), flagBinary(true), gridSpacing(0), flagVerbose(false)
	{
	    pipelineParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold=0;
        pipelineParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12=3;
        pipelineParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21=3;
        pipelineParameters.pipelineParameters.matcherParameters.flagBucketFilter=false;
        pipelineParameters.pipelineParameters.maxIteration=3;
        pipelineParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration=10;
	}

};	//AppSparseUnitSphereBuilderParametersC

/**
 * @brief Implmenentation of nodalCameraTracker
 * @ingroup Application
 * @nosubgrouping
 */
class AppNodalCameraTrackerImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		AppNodalCameraTrackerParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string, double> logger;	///< Logger
		//@}

		/** @name Implementation details */ //@{
		static ptree PruneNodalCameraTrackingPipeline(const ptree& src);	///<Removes the redundant nodal camera tracking pipeline parameters
		static ptree InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters);  ///< Injects the application-specific default parameters into the parameter tree

		tuple<CameraC, IntrinsicCalibrationMatrixT> LoadCamera();	///< Loads the reference camera

		vector<SceneFeatureC> LoadWorld(const CameraC& referenceCamera, const SceneFeatureC& dummy);	///< Loads a world map
		vector<OrientedBinarySceneFeatureC> LoadWorld(const CameraC& referenceCamera, const OrientedBinarySceneFeatureC& dummy);	///< Loads a world map, binary variant

		map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> > Track(const vector<OrientedBinarySceneFeatureC>& world, const CameraC& referenceCamera, const IntrinsicCalibrationMatrixT& mK,  const high_resolution_clock::time_point& t0);	///< Dispatcher for the tracker
		map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> > Track(const vector<SceneFeatureC>& world, const CameraC& referenceCamera, const IntrinsicCalibrationMatrixT& mK, const high_resolution_clock::time_point& t0);	///< Dispatcher for the tracker
		template<class MatchingProblemT> map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> > Track(const vector<typename MatchingProblemT::feature_type1>& world,  const CameraC& referenceCamera, const IntrinsicCalibrationMatrixT& mK, const high_resolution_clock::time_point& t0);	///< Performs the camera tracking task

		vector<ImageFeatureC> LoadFeatures(const CameraC& referenceCamera, const Matrix3d& normaliser, unsigned int index, const ImageFeatureC& dummy);	///< Loads the features
		vector<BinaryImageFeatureC> LoadFeatures(const CameraC& referenceCamera, const Matrix3d& normaliser, unsigned int index, const BinaryImageFeatureC& dummy);	///< Loads the features

		void SaveOutput(const map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack,  const CameraC& referenceCamera);	///< Saves the output
		void SaveLog(const map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack);	///< Saves the log file
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppNodalCameraTrackerImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& treeO);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}

};	//AppSparseUnitSphereBuilderImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Removes the redundant nodal camera tracking pipeline parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree AppNodalCameraTrackerImplementationC::PruneNodalCameraTrackingPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Problem.ImageDiagonal"))
		output.get_child("Problem").erase("ImageDiagonal");

	if(src.get_child_optional("Problem.FocalLength"));
		output.get_child("Problem").erase("FocalLength");

	return output;
}	//ptree PruneNodalCameraTrackingipeline(const ptree& src)

/**
 * @brief Injects the application-specific default parameters into the parameter tree
 * @param[in] src Parameter tree
 * @param[in] defaultParameters Default parameters
 * @return Updated tree
 */
ptree AppNodalCameraTrackerImplementationC::InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters)
{
    ptree output(src);


    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.Matching.kNN.MinSimilarity"))
        output.put("Tracker.GeometryEstimationPipeline.Matching.kNN.MinSimilarity", defaultParameters.pipelineParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality"))
        output.put("Tracker.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality", defaultParameters.pipelineParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable"))
        output.put("Tracker.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable", defaultParameters.pipelineParameters.pipelineParameters.matcherParameters.flagBucketFilter);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration"))
        output.put("Tracker.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration", defaultParameters.pipelineParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio"))
        output.put("Tracker.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio", defaultParameters.pipelineParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration"))
        output.put("Tracker.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration", defaultParameters.pipelineParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.Main.MaxIteration"))
         output.put("Tracker.GeometryEstimationPipeline.Main.MaxIteration", defaultParameters.pipelineParameters.pipelineParameters.maxIteration);

    if(!src.get_child_optional("Tracker.GeometryEstimationPipeline.Decimation.MaxObservationDensity"))
        output.put("Tracker.GeometryEstimationPipeline.Decimation.MaxObservationDensity", defaultParameters.pipelineParameters.pipelineParameters.maxObservationDensity);

   return output;
}   //ptree InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters)

/**
 * @brief Loads the reference camera
 * @return A tuple: reference camera; intrinsic calibration matrix
 * @throws invalid_argument If the camera file is not valid
 */
auto AppNodalCameraTrackerImplementationC::LoadCamera() -> tuple<CameraC, IntrinsicCalibrationMatrixT>
{
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.referenceCameraFile);

	if(cameraList.size()!=1)
		throw(string("AppNodalCameraTrackerImplementationC::LoadCamera : The camera file must have exactly one element. Value=") + lexical_cast<string>(cameraList.size()) );

	if( !cameraList[0].Extrinsics() || !cameraList[0].Extrinsics()->position)
		throw("AppNodalCameraTrackerImplementationC::LoadCamera: Camera centre must be defined" );

	if(!cameraList[0].FrameBox())
		throw("AppNodalCameraTrackerImplementationC::LoadCamera: FrameBox must be defined" );

	//Normaliser
	//Normaliser ignores the focal length- performed in the tracker
	IntrinsicCalibrationMatrixT mK=IntrinsicCalibrationMatrixT::Identity();

	if(cameraList[0].Intrinsics())
	{
		mK(0,1)=cameraList[0].Intrinsics()->skewness;
		mK(1,1)=cameraList[0].Intrinsics()->aspectRatio;
	}	//if(cameraList[0].Intrinsics())

	Coordinate2DT principalPoint;
	if(cameraList[0].Intrinsics() && cameraList[0].Intrinsics()->principalPoint)
		principalPoint=*cameraList[0].Intrinsics()->principalPoint;
	else
		cameraList[0].FrameBox()->center();

	mK.col(2).segment(0,2)=principalPoint;

	return make_tuple(cameraList[0], mK);
}	//tuple<CameraC, ProblemCategoryT> LoadCamera()

/**
 * @brief Loads a world map
 * @param[in] referenceCamera Reference camera
 * @param[in] dummy Dummy parameter for overload resolution
 * @return World map
 */
vector<SceneFeatureC> AppNodalCameraTrackerImplementationC::AppNodalCameraTrackerImplementationC::LoadWorld(const CameraC& referenceCamera, const
SceneFeatureC& dummy)
{
	vector<SceneFeatureC> output=SceneFeatureIOC::ReadFeatureFile(parameters.worldFile, true);
	for_each(output, [&](SceneFeatureC& current){current.Coordinate()-=*referenceCamera.Extrinsics()->position;} );	//Shift the origin to the camera centre
	return output;
}	//vector<SceneFeatureC> LoadWorld(const CameraC& referenceCamera)

/**
 * @brief Loads a world map, binary variant
 * @param[in] referenceCamera Reference camera
 * @param[in] dummy Dummy parameter for overload resolution
 * @return World map
 */
vector<OrientedBinarySceneFeatureC> AppNodalCameraTrackerImplementationC::AppNodalCameraTrackerImplementationC::LoadWorld(const CameraC& referenceCamera, const OrientedBinarySceneFeatureC& dummy)
{
	vector<OrientedBinarySceneFeatureC> output=OrientedBinarySceneFeatureIOC::ReadFeatureFile(parameters.worldFile, true);
	for_each(output, [&](OrientedBinarySceneFeatureC& current){current.Coordinate()-=*referenceCamera.Extrinsics()->position;} );	//Shift the origin to the camera centre
	return output;
}	//vector<OrientedBinarySceneFeatureC> LoadWorld(const CameraC& referenceCamera)

/**
 * @brief Loads the features
 * @param[in] referenceCamera Reference camera
 * @param[in] normaliser Normaliser
 * @param[in] index Frame index
 * @param[in] dummy Dummy variable for overload resolution
 * @return Features
 */
vector<ImageFeatureC> AppNodalCameraTrackerImplementationC::LoadFeatures(const CameraC& referenceCamera, const Matrix3d& normaliser, unsigned int index, const ImageFeatureC& dummy)
{
	string featureFile=str(format(parameters.featureFilePattern)%index);
	vector<ImageFeatureC> featureList=ImageFeatureIOC::ReadFeatureFile(featureFile, true);

	//Lens distortion
	LensDistortionC noDistortion; noDistortion.modelCode=LensDistortionCodeT::NOD;
	LensDistortionC distortion = referenceCamera.Distortion() ? *referenceCamera.Distortion() : noDistortion;

	optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
	optional<CoordinateTransformations2DT::affine_transform_type> normaliserTransform=CoordinateTransformations2DT::affine_transform_type(); normaliserTransform->matrix()=normaliser;
	ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, parameters.gridSpacing, true, distortion, normaliserTransform);

	return featureList;
}	//vector<ImageFeatureC> LoadFeatures(const CameraC& referenceCamera, const Matrix3d& normaliser, const ImageFeatureC& dummy)

/**
 * @brief Loads the features
 * @param[in] referenceCamera Reference camera
 * @param[in] normaliser Normaliser
 * @param[in] index Frame index
 * @param[in] dummy Dummy variable for overload resolution
 * @return Features
 */
vector<BinaryImageFeatureC> AppNodalCameraTrackerImplementationC::LoadFeatures(const CameraC& referenceCamera, const Matrix3d& normaliser, unsigned int index, const BinaryImageFeatureC& dummy)
{
	string featureFile=str(format(parameters.featureFilePattern)%index);
	vector<BinaryImageFeatureC> featureList=BinaryImageFeatureIOC::ReadFeatureFile(featureFile, true);

	//Lens distortion
	LensDistortionC noDistortion; noDistortion.modelCode=LensDistortionCodeT::NOD;
	LensDistortionC distortion = referenceCamera.Distortion() ? *referenceCamera.Distortion() : noDistortion;

	optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
	optional<CoordinateTransformations2DT::affine_transform_type> normaliserTransform=CoordinateTransformations2DT::affine_transform_type(); normaliserTransform->matrix()=normaliser;
	BinaryImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, parameters.gridSpacing, true, distortion, normaliserTransform);

	return featureList;

}	//vector<BinaryImageFeatureC> LoadFeatures(const CameraC& referenceCamera, const Matrix3d& normaliser, const BinaryImageFeatureC& dummy)

/**
 * @brief Dispatcher for the tracker
 * @param[in] world World map
 * @param[in] referenceCamera Reference camera
 * @param[in] mK Known elements of the intrinsic calibration matrix
 * @param[in] t0 Reference time point
 * @return Camera trajectory
 */
map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC>> AppNodalCameraTrackerImplementationC::Track(const vector<OrientedBinarySceneFeatureC>& world, const CameraC& referenceCamera, const IntrinsicCalibrationMatrixT& mK, const high_resolution_clock::time_point& t0)
{
	typedef BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> HammingSceneImageFeatureMatchingProblemT;
	return Track<HammingSceneImageFeatureMatchingProblemT>(world, referenceCamera, mK, t0);
}	//map<unsigned int, CameraMatrixT> Track(const vector<OrientedBinarySceneFeatureC>& world, const CameraC& referenceCamera, const Matrix3d& normaliser, const high_resolution_clock::time_point& t0)

/**
 * @brief Dispatcher for the tracker
 * @param[in] world World map
 * @param[in] referenceCamera Reference camera
 * @param[in] mK Known elements of the intrinsic calibration matrix
 * @param[in] t0 Reference time point
 * @return Camera trajectory
 */
map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC>> AppNodalCameraTrackerImplementationC::Track(const vector<SceneFeatureC>& world, const CameraC& referenceCamera, const IntrinsicCalibrationMatrixT& mK, const high_resolution_clock::time_point& t0)
{
	typedef SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> EuclideanSceneImageFeatureMatchingProblemT;
	return Track<EuclideanSceneImageFeatureMatchingProblemT>(world, referenceCamera, mK, t0);
}	//map<unsigned int, CameraMatrixT> Track(const vector<SceneFeatureC>& world, const CameraC& referenceCamera, const Matrix3d& normaliser, const high_resolution_clock::time_point& t0)

/**
 * @brief Performs the camera tracking task
 * @tparam MatchingProblemT Matching problem type
 * @param[in] world World map
 * @param[in] referenceCamera Reference camera
 * @param[in] mK Known elements of the intrinsic calibration matrix
 * @param[in] t0 Reference time point
 * @return Camera trajectory
 */
template<class MatchingProblemT>
map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> > AppNodalCameraTrackerImplementationC::Track(const vector<typename MatchingProblemT::feature_type1>& world, const CameraC& referenceCamera, const IntrinsicCalibrationMatrixT& mK, const high_resolution_clock::time_point& t0)
{
	Matrix3d normaliser=mK.inverse();

	typedef NodalCameraTrackingPipelineC<P2PEstimatorProblemT, P2PfEstimatorProblemT, MatchingProblemT> TrackerT;
	TrackerT tracker(world, parameters.pipelineParameters);

	//Tracking loop
	typename TrackerT::rng_type rng;

	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> > output;
	for(size_t c=0; c<parameters.nFrames; ++c)
	{
		//Load image features
		vector<typename MatchingProblemT::feature_type2> image=LoadFeatures(referenceCamera, normaliser, c+parameters.initialFrame, typename MatchingProblemT::feature_type2());

		//Update the tracker
		NodalCameraTrackingPipelineDiagnosticsC trackerDiagnostics=tracker.Update(rng, image);

		//Save the result
		if(trackerDiagnostics.flagSuccess)
		{
			typename TrackerT::TrackerStateC trackerState=tracker.GetState();

			IntrinsicCalibrationMatrixT mKc=mK; mKc(0,0)=trackerState.focalLength; mKc(1,1)=mKc(0,0)*mK(1,1)/mK(0,0);
			output.emplace(c, std::forward_as_tuple(ComposeCameraMatrix(mKc, *referenceCamera.Extrinsics()->position, trackerState.orientation), trackerDiagnostics) );
		}	//if(trackerDiagnostics.flagSuccess)

		if(parameters.flagVerbose)
		{
			cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Frame "<<c<<" ";
			if(!trackerDiagnostics.flagSuccess)
				std::cout<<"FAIL!";

			std::cout<<"\n";
		}	//if(parameters.flagVerbose)
	}	//for(size_t c=parameters.initialFrame; c<parameters.nFrames; ++c)

	logger["nFrames"]=parameters.nFrames;

	return output;
}	//map<unsigned int, CameraMatrixT> Track(const vector<typename MatchingProblemT::feature_type1>& world, const vector<typename MatchingProblemT::feature_type2>& image, const CameraC& referenceCamera, const Matrix3d& normaliser, const high_resolution_clock::time_point& t0)

/**
 * @brief Saves the output
 * @param[in] cameraTrack Camera track
 * @param[in] referenceCamera Reference camera
 */
void AppNodalCameraTrackerImplementationC::SaveOutput(const map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack,  const CameraC& referenceCamera)
{
	if(parameters.cameraTrackFile.empty())
		return;

	list<CameraC> cameraList;
	for(const auto& current : cameraTrack)
	{
		CameraC currentCamera=referenceCamera;

		IntrinsicCalibrationMatrixT mK;
		RotationMatrix3DT mR;
		tie(mK, ignore, mR)=DecomposeCamera(get<0>(current.second));

		currentCamera.Tag()=lexical_cast<string>(current.first);
		currentCamera.Extrinsics()->orientation=QuaternionT(mR);
		currentCamera.Intrinsics()->focalLength=mK(0,0);
		cameraList.push_back(currentCamera);
	}	//for(const auto& current : cameraTrack)

	CameraIOC::WriteCameraFile(cameraList, parameters.outputPath+parameters.cameraTrackFile, "", false);

}	//void SaveOutput(const map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack,  const CameraC& referenceCamera)

/**
 * @brief Saves the log file
 * @param[in] cameraTrack Camera track
 */
void AppNodalCameraTrackerImplementationC::SaveLog(const map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack)
{
	if(parameters.logFile.empty())
		return;

	ofstream logfile(parameters.outputPath+parameters.logFile);

	if(logfile.fail())
		throw(runtime_error(string("AppNodalCameraTrackerImplementationC::SaveLog : Failed to open file at ")+parameters.logFile) );

	string timeString= to_simple_string(second_clock::universal_time());
	logfile<<"nodalCameraTracker log file created on "<<timeString<<"\n";
	logfile<<"Number of frames "<<logger["nFrames"]<<"\n";
	logfile<<"Number of successfully tracked frames "<<cameraTrack.size()<<"\n";
	logfile<<"Time elapsed "<<logger["timeElapsed"]<<"s\n";
	logfile<<"\n Per frame diagnostics\n";
	logfile<<"Frame    No. Inliers   No. Registrations"<<"\n";

	for(const auto& current : cameraTrack)
		logfile<<current.first<<" "<<get<1>(current.second).nInliers<<" "<<get<1>(current.second).nRegistration<<"\n";
}	//void SaveLog(const map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppNodalCameraTrackerImplementationC::AppNodalCameraTrackerImplementationC()
{
	AppNodalCameraTrackerParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the application. >0" )
	("General.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.seed)), "Random number generator seed. A negative value indicates the default seed" )

	("Input.FeatureFilename", value<string>(), "Feature filename, with a single printf-like token (e.g. feature%02d.ft2)")
	("Input.InitialFrame", value<string>()->default_value(lexical_cast<string>(defaultParameters.initialFrame)),"Initial frame" )
	("Input.NoFrames", value<string>()->default_value(lexical_cast<string>(defaultParameters.nFrames)),"Number of frames" )
	("Input.FlagBinaryFeatures", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagBinary)), "Indicates whether the input features involve binary descriptors")
	("Input.WorldFilename", value<string>(), "Filename for the 3D features representing the world" )
	("Input.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.gridSpacing)), "Minimum distance between two image features, in pixels. >=0")
	("Input.ReferenceCameraFile", value<string>(), "Camera filename for the known camera parameters")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.CameraTrack", value<string>(), "File for the estimated camera parameters")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output");
}	//AppSparseUnitSphereBuilderImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppNodalCameraTrackerImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppNodalCameraTrackerImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppNodalCameraTrackerParametersC defaultParameters;

	tree.put("xmlcomment", "nodalCameraTracker configuration file");

	tree.put("General","");
	tree.put("Tracker","");
	tree.put("Input","");
	tree.put("Output","");

	tree.put("General.NoThreads", defaultParameters.nThreads);
	tree.put("General.Seed", defaultParameters.seed);

	tree.put("Input.FeatureFilename", defaultParameters.featureFilePattern);
	tree.put("Input.InitialFrame", defaultParameters.initialFrame);
	tree.put("Input.NoFrames", defaultParameters.nFrames);
	tree.put("Input.FlagBinaryFeatures", defaultParameters.flagBinary);

	if(detailLevel>1)
	    tree.put("Input.GridSpacing", defaultParameters.gridSpacing);

	tree.put("Input.ReferenceCameraFile", defaultParameters.referenceCameraFile);
	tree.put("Input.WorldFilename", defaultParameters.worldFile);

	tree.put("Output.Root", defaultParameters.outputPath);
	tree.put("Output.CameraTrack", defaultParameters.cameraTrackFile);
	tree.put("Output.Log", defaultParameters.logFile);
	tree.put("Output.Verbose", defaultParameters.flagVerbose);

	tree.put_child("Tracker", PruneNodalCameraTrackingPipeline(InterfaceNodalCameraTrackingPipelineC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel)));

	if(detailLevel<=1)
        if(tree.get_child_optional("Tracker.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC") )
            tree.get_child("Tracker.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");


	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] treeO Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppNodalCameraTrackerImplementationC::SetParameters(const ptree& treeO)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);

	AppNodalCameraTrackerParametersC defaultParameters;
    ptree lTree=InjectDefaultValues(treeO, defaultParameters);

	//General
	parameters.nThreads=ReadParameter(lTree, "General.NoThreads", gt0, "is not >0", optional<double>(defaultParameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	parameters.seed=lTree.get<int>("General.Seed", parameters.seed);

	//Input
	parameters.featureFilePattern=lTree.get<string>("Input.FeatureFilename");
	parameters.initialFrame=lTree.get<unsigned int>("Input.InitialFrame");
	parameters.nFrames=lTree.get<unsigned int>("Input.NoFrames");
	parameters.flagBinary=lTree.get<bool>("Input.FlagBinaryFeatures");
	parameters.worldFile=lTree.get<string>("Input.WorldFilename");
	parameters.referenceCameraFile=lTree.get<string>("Input.ReferenceCameraFile");
	parameters.gridSpacing=ReadParameter(lTree, "Input.GridSpacing", geq0, "is not >=0", optional<double>(defaultParameters.gridSpacing));

	//Output
	parameters.outputPath=lTree.get<string>("Output.Root");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppPfMImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.cameraTrackFile=lTree.get<string>("Output.CameraTrack","");
	parameters.logFile=lTree.get<string>("Output.Log","");
	parameters.flagVerbose=lTree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);

	parameters.pipelineParameters=InterfaceNodalCameraTrackingPipelineC::MakeParameterObject(PruneNodalCameraTrackingPipeline(lTree.get_child("Tracker")));
	parameters.pipelineParameters.nThreads=parameters.nThreads;

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppNodalCameraTrackerImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the reference camera
	CameraC referenceCamera;
	Matrix3d mK;
	tie(referenceCamera, mK)=LoadCamera();

	//Additional pipeline parameters
	parameters.pipelineParameters.imageDiagonal=referenceCamera.FrameBox()->diagonal().norm();		//Diagonal

	if(referenceCamera.Intrinsics() && referenceCamera.Intrinsics()->focalLength)
		parameters.pipelineParameters.focalLength= *referenceCamera.Intrinsics()->focalLength;

	//Load the world map
	optional<vector<SceneFeatureC> > worldSF;
	optional<vector<OrientedBinarySceneFeatureC> > worldOBSF;

	if(parameters.flagBinary)
		worldOBSF=LoadWorld(referenceCamera, OrientedBinarySceneFeatureC());
	else
		worldSF=LoadWorld(referenceCamera, SceneFeatureC());

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read a world map with "<< ((worldOBSF) ? worldOBSF->size() : worldSF->size())<< " features. Tracking...\n";

	//Tracker
	map<unsigned int, tuple<CameraMatrixT, NodalCameraTrackingPipelineDiagnosticsC> > cameraTrack;
	if(parameters.flagBinary)
		cameraTrack=Track(*worldOBSF, referenceCamera, mK, t0);
	else
		cameraTrack=Track(*worldSF, referenceCamera, mK, t0);

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Finished! \n";

	logger["timeElapsed"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	SaveOutput(cameraTrack, referenceCamera);
	SaveLog(cameraTrack);

	return true;
}	//bool Run()

}	//AppNodalCameraTrackerN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("160806");
    std::string header("nodalCameraTracker: Tracks a PTZ camera");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppNodalCameraTrackerN::AppNodalCameraTrackerImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
