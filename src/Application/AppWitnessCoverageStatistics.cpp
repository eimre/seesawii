/**
 * @file AppWitnessCoverageStatistics.cpp Implementation of \c witnessCoverageStatistics
 * @author Evren Imre
 * @date 9 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/optional.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/properties.hpp>
#include <boost/property_map/property_map.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <utility>
#include "Application.h"
#include "../ApplicationInterface/InterfaceUtility.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../IO/CameraIO.h"
#include "../IO/ArrayIO.h"
#include "../DataGeneration/LatticeGenerator.h"
#include "../Geometry/SceneCoverageEstimation.h"

namespace SeeSawN
{
namespace AppWitnessCoverageStatisticsN
{

using namespace ApplicationN;

using boost::optional;
using boost::adjacency_matrix;
using boost::undirectedS;
using boost::edge_weight_t;
using boost::make_label_writer;
using boost::write_graphviz;
using boost::property;
using boost::get;
using Eigen::Array;
using Eigen::Matrix;
using std::pair;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::DataGeneratorN::GenerateRectangularLattice;
using SeeSawN::GeometryN::SceneCoverageEstimationC;
using SeeSawN::GeometryN::SceneCoverageEstimationParametersC;

//TODO Focus field constraint missing! (seriously!?!)
//TODO We need a way to disable some output forms. Moving camera and affinity matrix is pointless.
/**
 * @brief Parameters for witnessCoverageStatistics
 * @ingroup Application
 */
struct WitnessCoverageStatisticsParametersC
{
	unsigned int minRedundancy;	///< Minimum number of cameras by which a vertex must be covered
	double minSize;	///<Minimum area of a volume element, relative to the image
	FrameT framing;	///< Region-of-interest, with the coordinates normalised to (0,0) and (1,1)
	double maxRelativeScale;	///< Maximum relative scale between two features, for a successful match
	double maxViewpointDifference;	///< Maximum viewpoint difference between two features, for a successful match. Radians

	string cameraFile;	///< Camera filename
	string pointCloudFile;	///< Filename for the point cloud
	optional<Array<double, 3, 2> > captureVolume;	///< Extent of the capture volume
	double density;	///< Number of points per unit length

	string outputPath;	///< Output path
	string logFile;	///< Filename for the log file
	string sceneCoverageFile;	///< Filename for the coverage of the scene
	string viewContributionFile;	///< Filename for the contribution of each view

	unsigned int minAffinity;	///< Minimum affinity for a relationship to be represented on the affinity graph. >0
	string affinityMatrixFile;	///< Filename for the affinity matrix
	bool flagVerbose;	///< Verbose output

	SceneCoverageEstimationParametersC sceParameters;	///< Parameters for the scene coverage estimation engine

	WitnessCoverageStatisticsParametersC() : minRedundancy(1), minSize(5e-5), maxRelativeScale(2), maxViewpointDifference(0.7), density(5), minAffinity(50), flagVerbose(true)
	{}
};	//struct WitnessCoverageStatisticsParametersC

/**
 * @brief Implementation of witnessCoverageStatistics
 * @ingroup Application
 */
class WitnessCoverageStatisticsImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		WitnessCoverageStatisticsParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ ///@{
		map<string, double> log;	///< Log: [Variable name; value]
		///@}

		/** @name Implementation details */ //@{
		typedef SceneCoverageEstimationC::unary_constraint_type UnaryConstraintT;	///< A requirement on a single camera
		vector<UnaryConstraintT> MakeConstraint(const vector<CameraC>& cameraList) const;	///< Makes the constraints

		void SaveLog();	///< Saves the log

		typedef map<unsigned int, tuple<unsigned int, unsigned int> > ContributionMapT;	///< Map holding the contributions of individual cameras to the coverage: camera id, covered, uniquely covered
		typedef Array<unsigned int, Eigen::Dynamic, Eigen::Dynamic> ArrayUT;	///< A dynamic array of unsigned integers
		void SaveOutput(const vector<Coordinate3DT>& scene, const vector<size_t>& coveredIndices, const vector<size_t>& redundancy, const ContributionMapT& contributions, const ArrayUT& affinityMatrix) const;	///< Saves the output
		//@}

	public:

	 	/**@name ApplicationImplementationConceptC interface */ //@{
		WitnessCoverageStatisticsImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//class WitnessCoverageStatisticsImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Makes the constraints
 * @param[in] cameraList Camera list
 * @return Constraints vector
 * @throws runtime_error If frame size is not defined for a camera
 */
auto WitnessCoverageStatisticsImplementationC::MakeConstraint(const vector<CameraC>& cameraList) const -> vector<UnaryConstraintT>
{
	//Verify the cameras
	size_t nCameras=cameraList.size();
	for(size_t c=0; c<nCameras; ++c)
		if(!cameraList[c].FrameBox())
			throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::MakeConstraint : FrameBox not defined for camera ")+lexical_cast<string>(c) ));

	vector<UnaryConstraintT> constraints(nCameras);
	for(size_t c=0; c<nCameras; ++c)
	{
		UnaryConstraintT constraint;
		get<SceneCoverageEstimationC::iArea>(constraint)=parameters.minSize;

		auto extent=cameraList[c].FrameBox()->sizes();

		Coordinate2DT ul=parameters.framing.corner(FrameT::BottomLeft);
		Coordinate2DT lr=parameters.framing.corner(FrameT::TopRight);
		get<SceneCoverageEstimationC::iRoI>(constraint)=FrameT(ul.cwiseProduct(extent), lr.cwiseProduct(extent));

		constraints[c].swap(constraint);
	}	//for(size_t c=0; c<nCameras; ++c)

	return constraints;
}	//vector<UnaryConstraintT> MakeConstraint(const vector<CameraC>& cameraList)

/**
 * @brief Saves the log
 * @throws runtime_error If the write operation fails
 */
void WitnessCoverageStatisticsImplementationC::SaveLog()
{
	if(parameters.logFile.empty())
		return;

	string filename=parameters.outputPath+parameters.logFile;
	ofstream output(filename);

	if(output.fail())
		throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::SaveLog : Failed to open the file for writing at ")+filename));

	string timeString= to_simple_string(second_clock::universal_time());
	output<<"witnessCoverageStatistics log file created on "<<timeString<<"\n";

	output<<"Number of scene points: "<<log["nPoints"]<<"\n";
	output<<"Number of cameras: "<<log["nCameras"]<<"\n";
	output<<"Number of points covered by the set-up: "<<log["nCovered"]<<"\n";

	output<<"Run time: "<<log["runtime"]<<"s \n";
}	//void SaveLog()

/**
 * @brief Saves the output
 * @param[in] scene 3D point cloud to be covered by the cameras
 * @param[in] coveredIndices Indices covered by the cameras
 * @param[in] redundancy Redundancy for the covered indices
 * @param[in] contributions Contribution of each camera to the coverage
 * @param[in] affinityMatrix Affinity scores between the cameras
 * @throws runtime_error If the write operation fails
 */
void WitnessCoverageStatisticsImplementationC::SaveOutput(const vector<Coordinate3DT>& scene, const vector<size_t>& coveredIndices, const vector<size_t>& redundancy, const ContributionMapT& contributions, const ArrayUT& affinityMatrix) const
{
	//Coverage
	if(!parameters.sceneCoverageFile.empty())
	{
		string filename=parameters.outputPath+parameters.sceneCoverageFile;
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::SaveOutput : Failed to open the file for writing at ")+filename));

		output<<"# witnessCoverageStatistics scene coverage file"<<"\n";
		output<<"# 3D coordinates (3); # cameras covering the point(1)"<<"\n";

		//Save
		size_t nCovered=coveredIndices.size();
		for(size_t c=0; c<nCovered; ++c)
		{
			size_t index=coveredIndices[c];
			output<< scene[index][0]<<" "<<scene[index][1]<<" "<<scene[index][2]<<" "<<redundancy[c]<<"\n";
		}	//for(size_t c=0; c<nCovered; ++c)

	}	//if(!parameters.sceneCoverageFile.empty())

	//Contribution
	if(!parameters.viewContributionFile.empty())
	{
		string filename=parameters.outputPath+parameters.viewContributionFile;
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::SaveOutput : Failed to open the file for writing at ")+filename));

		output<<"# witnessCoverageStatistics camera contribution file"<<"\n";
		output<<"# Camera Id(1); # points covered by the camera (1); # points uniquely covered by the camera"<<"\n";
		for(const auto& current : contributions)
			output<<current.first<<" "<<get<0>(current.second)<<" "<<get<1>(current.second)<<"\n";
	}	//if(!parameters.viewContributionFile.empty())

	//Matchability array
	if(!parameters.affinityMatrixFile.empty())
	{
		string filename=parameters.outputPath+parameters.affinityMatrixFile;
		ofstream output(filename);

		if(output.fail())
			throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::SaveOutput : Failed to open the file for writing at ")+filename));

		output<<"# witnessCoverageStatistics affinity matrix file"<<"\n";
		output<<"# Each entry is a predictor for the level of success of a matching task between the corresponding camera pairs"<<"\n";
		ArrayIOC<ArrayUT>::WriteArray(output, affinityMatrix);

		//Now, make a graph, and save in .dot format

		//Edges

		size_t nCameras=affinityMatrix.rows();
		if(nCameras==0)
			return;

		vector<pair<int, int> > edges; edges.reserve(nCameras*(nCameras-1)/2 );
		vector<unsigned int> edgeWeights; edgeWeights.reserve(edges.capacity());

		for(size_t c2=0; c2<nCameras; ++c2)
			for(size_t c1=c2+1; c1<nCameras; ++c1)
				if(affinityMatrix(c1,c2)>=parameters.minAffinity)
				{
					edges.emplace_back(c1,c2);
					edgeWeights.push_back(affinityMatrix(c1,c2));
				}	//if(affinityMatrix(c1,c2)>=parameters.minAffinity)

		typedef property<edge_weight_t, unsigned int> EdgePropertyContainerT;
		typedef adjacency_matrix<undirectedS, vector<unsigned int>, EdgePropertyContainerT > GraphT;
		GraphT graph(edges.begin(), edges.end(), edgeWeights.begin(), nCameras);

		string filenameDot=filename+".dot";
		ofstream outputG(filenameDot);
		if(output.fail())
			throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::SaveOutput : Failed to open the file for writing at ")+filenameDot));

		write_graphviz(outputG, graph, make_label_writer(get(boost::vertex_index, graph)), make_label_writer(get(boost::edge_weight, graph)));
	}	//if(!parameters.matchabilityMatrixFile.empty())
}	//void SaveOutput(const vector<size_t>& coveredIndices, const vector<size_t>& redundancy, const ContributionMapT& contributions, const ArrayUT& matchabilityMatrix)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
WitnessCoverageStatisticsImplementationC::WitnessCoverageStatisticsImplementationC()
{
	WitnessCoverageStatisticsParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("Constraints.MinRedundancy", value<string>()->default_value(lexical_cast<string>(defaultParameters.minRedundancy)), "Minimum number of cameras by which a vertex must be covered. >=0")
	("Constraints.MinSize", value<string>()->default_value(lexical_cast<string>(defaultParameters.minSize)), "Minimum area of a volume element, relative to the image. [0,1) ")
	("Constraints.Framing", value<string>()->default_value("0 0 1 1"), "Region-of-interest, in normalised coordinates. 4 entries between [0,1]")
	("Constraints.MaxRelativeScale", value<string>()->default_value(lexical_cast<string>(defaultParameters.maxRelativeScale)), "Maximum relative scale between two features, for a successful match. >1")
	("Constraints.MaxViewpointDifference", value<string>()->default_value(lexical_cast<string>(defaultParameters.maxViewpointDifference)), "Maximum viewpoint difference between two features, for a successful match. Radians. >0")
	("Scene.PointCloudFile", value<string>(), "File containing the point cloud, as an Nx3 array. If specified, CaptureVolume is ignored.")
	("Scene.CaptureVolume.HorizontalExtent", value<string>(), "Horizontal extent of the capture volume, in m. [min max]")
	("Scene.CaptureVolume.VerticalExtent", value<string>(), "Vertical extent of the capture volume, in m. [min max]")
	("Scene.CaptureVolume.DepthExtent", value<string>(), "Extent of the capture volume in depth, in m. [min max]")
	("Scene.CaptureVolume.Density", value<string>()->default_value(lexical_cast<string>(defaultParameters.density)), "Number of points per unit length. >0")
	("Scene.VolumeElementRadius", value<string>()->default_value(lexical_cast<string>(defaultParameters.sceParameters.radius)), "Radius of the sphere representing a volume element centred on each vertex. >0.1")
	("Scene.FocusPoint", value<string>()->default_value(""), "Focus point for the set-up, in m. Omit to disable")
	("CameraFile", value<string>(), "Camera filename")
	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Log", value<string>(), "Log file")
	("Output.Coverage", value<string>(), "Coverage for each scene element")
	("Output.Contriburtion", value<string>(), "Contribution of each camera to the coverage")
	("Output.MinAffinity", value<string>()->default_value("50"), "If the affinity score between two cameras is below this value, it is not represented in the vision graph")
	("Output.AffinityMatrix", value<string>()->default_value(""), "Affinity matrix. Prediction for the success of a matching task. Optional.")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output")
	;
}	//WitnessCoverageStatisticsImplementationC()

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& WitnessCoverageStatisticsImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const


/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void WitnessCoverageStatisticsImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	WitnessCoverageStatisticsParametersC defaultParameters;

    ptree tree; //Options tree


    tree.put("xmlcomment", "witnessCoverageStatistics configuration file");

    tree.put("Constraints", "");
    tree.put("Constraints.MinRedundancy", defaultParameters.minRedundancy);
    tree.put("Constraints.MinSize", defaultParameters.minSize);
    tree.put("Constraints.Framing", "0 0 1 1");
    tree.put("Constraints.MaxRelativeScale", defaultParameters.maxRelativeScale);
    tree.put("Constraints.MaxViewpointDifference", defaultParameters.maxViewpointDifference);

    tree.put("Scene","");
    tree.put("Scene.CaptureVolume", "");
    tree.put("Scene.CaptureVolume.HorizontalExtent", "-2 2");
    tree.put("Scene.CaptureVolume.VerticalExtent", "0 2");
    tree.put("Scene.CaptureVolume.DepthExtent", "-2 2");
    tree.put("Scene.CaptureVolume.Density",defaultParameters.density);
    tree.put("Scene.FocusPoint", "0 1 0"); tree.get_child("Scene.FocusPoint").put("xmlcomment", "Value for exposition only! Omit if not needed.");
    tree.put("Scene.PointCloudFile", "/home/points.asc");
    tree.put("Scene.VolumeElementRadius",defaultParameters.sceParameters.radius);

    tree.put("CameraFile","/home/camera.cam");

    tree.put("Output","");
    tree.put("Output.Root","/home/Output/");
    tree.put("Output.Log", "log.log");
    tree.put("Output.Coverage", "coverage.txt");
    tree.put("Output.Contribution", "contribution.txt");
    tree.put("Output.AffinityMatrix", "affinity.txt");

    tree.put("Output.MinAffinity", defaultParameters.minAffinity);
    tree.put("Output.Verbose", defaultParameters.flagVerbose);


	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool WitnessCoverageStatisticsImplementationC::SetParameters(const ptree& tree)
{
	WitnessCoverageStatisticsParametersC defaultParameters;

	//Constraints

	unsigned int minRedundancy=tree.get<unsigned int>("Constraints.MinRedundancy",defaultParameters.minRedundancy);
	if(minRedundancy==0)
		throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Constraints.MinRedundancy must be >0. Value=") + lexical_cast<string>(minRedundancy) );
	else
		parameters.minRedundancy=minRedundancy;

	double minSize=tree.get<double>("Constraints.MinSize", defaultParameters.minSize);
	if(minSize<0 || minSize>=1)
		throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Constraints.MinSize must be in [0,1). Value=") + lexical_cast<string>(minSize) );
	else
		parameters.minSize=minSize;

	auto c01=[](double val){return val>=0 && val<=1;};
	vector<double> framing=ReadParameterArray<double>(tree, "Constraints.Framing", 4, c01, "WitnessCoverageStatisticsImplementationC::SetParameters : Constraints.Framing must have 4 elements, with values in [0,1].", " ");
	parameters.framing=FrameT(Coordinate2DT(framing[0], framing[1]), Coordinate2DT(framing[2], framing[3]));

	double maxRelativeScale=tree.get<double>("Constraints.MaxRelativeScale", defaultParameters.maxRelativeScale);
	if(maxRelativeScale<=1)
		throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Constraints.MaxRelativeScale must be >1. Value=") + lexical_cast<string>(maxRelativeScale) );
	else
		parameters.maxRelativeScale=maxRelativeScale;

	double maxViewpointDifference=tree.get<double>("Constraints.MaxViewpointDifference", defaultParameters.maxViewpointDifference);
	if(maxViewpointDifference<=0)
		throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Constraints.MaxViewpointDifference must be >0. Value=") + lexical_cast<string>(maxViewpointDifference) );
	else
		parameters.maxViewpointDifference=maxViewpointDifference;

	//Scene

	parameters.pointCloudFile=tree.get<string>("Scene.PointCloudFile","");

	double radius=tree.get<double>("Scene.VolumeElementRadius",defaultParameters.sceParameters.radius);
	if(radius<=0)
		throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Constraints.VolumeElementRadius must be >0. Value=") + lexical_cast<string>(radius) );
	else
		parameters.sceParameters.radius=radius;

	//Capture volume

	double density=tree.get<double>("Scene.CaptureVolume.Density", defaultParameters.density);
	if(density<=0)
		throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Scene.CaptureVolume.Density must be >0. Value=") + lexical_cast<string>(density) );
	else
		parameters.density=density;

	//Capture volume extents
	string hExtentS=tree.get<string>("Scene.CaptureVolume.HorizontalExtent","");
	vector<double> hExtent=Tokenise<double>(hExtentS);

	string vExtentS=tree.get<string>("Scene.CaptureVolume.VerticalExtent","");
	vector<double> vExtent=Tokenise<double>(vExtentS);

	string dExtentS=tree.get<string>("Scene.CaptureVolume.DepthExtent","");
	vector<double> dExtent=Tokenise<double>(dExtentS);

	if(hExtent.size()==2 && vExtent.size()==2 && dExtent.size()==2)
	{
		Array<double, 3, 2> captureVolume;
		captureVolume<<hExtent[0], hExtent[1], vExtent[0], vExtent[1], dExtent[0], dExtent[1];
		parameters.captureVolume=captureVolume;
	}
	else
		if(parameters.pointCloudFile.empty())
			throw(string("WitnessCoverageStatisticsImplementationC::SetParameters : Scene not specified. No point cloud file, or valid capture volume definition."));

	auto alwaysTrue=[](double val){return true;};
	if(tree.get_optional<string>("Scene.FocusPoint") && !tree.get<string>("Scene.FocusPoint").empty())
	{
		vector<double> focusPoint=ReadParameterArray<double>(tree, "Scene.FocusPoint", 3, alwaysTrue, "Scene.FocusPoint must have 3 elements", " ");
		parameters.sceParameters.focusPoint=Coordinate3DT(focusPoint[0], focusPoint[1], focusPoint[2]);
	}

	//Camera
	parameters.cameraFile=tree.get<string>("CameraFile","");

	//Output

	parameters.outputPath=tree.get<string>("Output.Root","");
	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("WitnessCoverageStatisticsImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.logFile=tree.get<string>("Output.Log","");
	parameters.sceneCoverageFile=tree.get<string>("Output.Coverage", "");
	parameters.viewContributionFile=tree.get<string>("Output.Contribution","");
	parameters.affinityMatrixFile=tree.get<string>("Output.AffinityMatrix","");
	parameters.flagVerbose=tree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);

	parameters.minAffinity=tree.get<unsigned int>("Output.MinAffinity", defaultParameters.minAffinity);

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool WitnessCoverageStatisticsImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	//Read in the input data

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";

	//Cameras
	vector<CameraC> cameras=CameraIOC::ReadCameraFile(parameters.cameraFile);
	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<cameras.size()<<" camera files.\n";

	//Scene
	vector<Coordinate3DT> scene;
	if(parameters.pointCloudFile.empty())
	{
		scene=GenerateRectangularLattice(*parameters.captureVolume, parameters.density);
		if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Generated "<<scene.size()<<" points. Adding the cameras... \n";
	}	//if(parameters.pointCloudFile.empty())
	else
	{
		//Load from file
		typedef Array< ValueTypeM<Coordinate3DT>::type, Eigen::Dynamic, 3> PointArrayT;
		PointArrayT pointArray=ArrayIOC<PointArrayT>::ReadArray(parameters.pointCloudFile);

		size_t nPoint=pointArray.rows();
		scene.reserve(nPoint);
		for(size_t c=0; c<nPoint; ++c)
			scene.push_back(pointArray.row(c).transpose());

		if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<scene.size()<<" points. Adding the cameras...\n";
	}	//if(parameters.pointCloudFile.empty())

	//Constraints
	vector<SceneCoverageEstimationC::unary_constraint_type> constraints=MakeConstraint(cameras);

	//Operation

	SceneCoverageEstimationC engine(scene, parameters.sceParameters);	//Initialise the engine

	//Add the views
	size_t nCameras=cameras.size();
	for(size_t c=0; c<nCameras; ++c)
	{
		optional<tuple<unsigned int, unsigned int> > addResult=engine.AddView(cameras[c], constraints[c]);

		if(!addResult)
		{
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Camera "<<c<<": Add operation failed. Camera matrix cannot be computed or frame box is not defined.\n";
			continue;
		}	//if(!addResult)

		unsigned int nCovered;
		unsigned int nUnique;
		tie(nCovered, nUnique)=*addResult;

		if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Camera "<<c<<": Covers "<<nCovered<<" points, "<<nUnique<<" of which are unique \n";
	}	//for(size_t c=0; c<nCameras; ++c)

	//Statistics

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Generating the statistics\n";

	vector<size_t> covered;
	vector<size_t> redundancy;
	tie(covered, redundancy)=engine.GetCoveredVertices(parameters.minRedundancy);

	ContributionMapT contribution=engine.ComputeCoverageStatistics();

	//Affinity matrix?
	ArrayUT affinityMatrix(nCameras, nCameras); affinityMatrix.setZero();
	if(!parameters.affinityMatrixFile.empty() )
	{
		for(size_t c1=0; c1<nCameras; ++c1)
			for(size_t c2=c1+1; c2<nCameras; ++c2)
			{
				affinityMatrix(c1,c2)=engine.PredictMatchingPerformance(c1, c2, parameters.maxViewpointDifference, parameters.maxRelativeScale);
				affinityMatrix(c2,c1)=affinityMatrix(c1,c2);
			}	//for(size_t c2=c1+1; c2<nCameras; ++c2)
	}

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Statistics generated. Saving the output...\n";

	//Output
	SaveOutput(scene, covered, redundancy, contribution, affinityMatrix);

	log["nPoints"]=scene.size();
	log["nCameras"]=cameras.size();
	log["nCovered"]=covered.size();
	log["runtime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;
	SaveLog();

	return true;
}	//bool Run()


}	//AppWitnessCoverageStatisticsN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("140309");
    std::string header("witnessCoverageStatistics: Generates statistics for the coverage provided by the witness cameras");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppWitnessCoverageStatisticsN::WitnessCoverageStatisticsImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )

/********** EXTERN TEMPLATES **********/
extern template class std::map<std::string, double>;
