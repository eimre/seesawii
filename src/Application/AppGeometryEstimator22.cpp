/*
 * AppGeometryEstimator22.cpp Implementation of the 2D pairwise geometry estimation application
 * @author Evren Imre
 * @date 1 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/optional.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <omp.h>
#include "Application.h"
#include "../GeometryEstimationComponents/OneSidedFundamentalComponents.h"
#include "../GeometryEstimationComponents/Homography2DComponents.h"
#include "../GeometryEstimationComponents/Fundamental7Components.h"
#include "../GeometryEstimationComponents/Fundamental8Components.h"
#include "../GeometryEstimationComponents/EssentialComponents.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Elements/Camera.h"
#include "../Elements/Correspondence.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Wrappers/BoostTokenizer.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../IO/CameraIO.h"
#include "../IO/ArrayIO.h"
#include "../IO/ImageFeatureIO.h"
#include "../IO/CoordinateCorrespondenceIO.h"

namespace SeeSawN
{
namespace AppGeometryEstimator22N
{

using namespace ApplicationN;
using boost::optional;
using boost::logic::tribool;
using boost::logic::indeterminate;
using boost::math::pow;
using boost::math::chi_squared;
using boost::math::cdf;
using boost::math::constants::pi;
using Eigen::Matrix3d;
using Eigen::MatrixXd;
using std::tan;
using SeeSawN::GeometryN::GeometryEstimationPipelineDiagnosticsC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineHomography2DProblem;
using SeeSawN::GeometryN::Homography2DPipelineProblemT;
using SeeSawN::GeometryN::Homography2DPipelineT;
using SeeSawN::GeometryN::Homography2DEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACHomography2DProblem;
using SeeSawN::GeometryN::RANSACHomography2DProblemT;
using SeeSawN::GeometryN::MakePDLHomography2DProblem;
using SeeSawN::GeometryN::PDLHomography2DProblemT;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineFundamental7Problem;
using SeeSawN::GeometryN::Fundamental7PipelineProblemT;
using SeeSawN::GeometryN::Fundamental7PipelineT;
using SeeSawN::GeometryN::Fundamental7EstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACFundamental7Problem;
using SeeSawN::GeometryN::RANSACFundamental7ProblemT;
using SeeSawN::GeometryN::MakePDLFundamentalProblem;
using SeeSawN::GeometryN::PDLFundamentalProblemT;
using SeeSawN::GeometryN::MakeGeometryEstimationPipelineEssentialProblem;
using SeeSawN::GeometryN::EssentialPipelineProblemT;
using SeeSawN::GeometryN::EssentialPipelineT;
using SeeSawN::GeometryN::EssentialEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACEssentialProblem;
using SeeSawN::GeometryN::RANSACEssentialProblemT;
using SeeSawN::GeometryN::MakeRANSACFundamental8Problem;
using SeeSawN::GeometryN::RANSACFundamental8ProblemT;
using SeeSawN::GeometryN::Fundamental8PipelineProblemT;
using SeeSawN::GeometryN::MakePDLEssentialProblem;
using SeeSawN::GeometryN::PDLEssentialProblemT;
using SeeSawN::GeometryN::OSFundamentalPipelineProblemT;
using SeeSawN::GeometryN::OSFundamentalPipelineT;
using SeeSawN::GeometryN::OSFundamentalEstimatorProblemT;
using SeeSawN::GeometryN::MakeRANSACOSFundamentalProblem;
using SeeSawN::GeometryN::RANSACOSFundamentalProblemT;
using SeeSawN::GeometryN::MakePDLOSFundamentalProblem;
using SeeSawN::GeometryN::PDLOSFundamentalProblemT;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::LensDistortionNoDC;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::GeometryN::CoordinateStatistics2DT;
using SeeSawN::RANSACN::ComputeBinSize;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::MakeCoordinateCorrespondenceList;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::FundamentalToEssential;
using SeeSawN::ElementsN::CameraToFundamental;
using SeeSawN::ElementsN::CameraToHomography;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::ArrayIOC;
using SeeSawN::ION::ImageFeatureIOC;
using SeeSawN::ION::CoordinateCorrespondenceIOC;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::WrappersN::Tokenise;
using SeeSawN::WrappersN::ValueTypeM;

//DOCUMENTME
//Usage notes
// The algorithm attempts to use all available information in the camera files. So, comment out anything that is not relevant to the current problem
// OSFundamental1- One-sided fundamental matrix. Focal length for the first camera is known
// OSFundamental2- One-sided fundamental matrix. Focal length for the second camera is known
// Essential: Scale factor issue: potential problem if the focal lengths are very different
// Bucket filter significantly improves the inlier ratio in the initial set
// Grid spacing: Small images may require tighter grids for better performance- pixels/feature
// Max perturbation and noise: #iteration for the second stage is determined by the ratio of maxPerturbation^2/noise . Since matching is expensive, it is better to run more RANSAC iterations
//MORANSAC is useful if the algorithm is oscillating between several plausible models
//Stage-2 RANSAC, PROSAC growth rate. Since S2 works on inliers, a higher growth rate can be employed, so that the entire observation set can be sampled before the iterations run out
//Since the input is always a metric camera, one-sided fundamental matrix problems cannot be inferred automatically
//OSFundamental uses the original f, if available in the calibration file
/**
 * @brief Parameter object for GeometryEstimator22ImplementationC
 * @ingroup Application
 */
struct GeometryEstimator22ParametersC
{
	enum class Geometry22ProblemT{AUTO, HMG2, FND, OSFND, ESS};	//2D geometry problem types: Homography, fundamental matrix, one-sided fundamental matrix (f1 known), essential matrix

	unsigned int nThreads;  ///< Number of threads available to the program
	int seed;	///< RNG seed
	Geometry22ProblemT problemType;	///< Type of the problem
	string initialEstimateFile;	///< Filename for the initial estimate
	double noiseVariance;	///< Variance of the noise on the image coordinates
	double initialEstimateError;	///< Additional variance of the noise on the image coordinates due to the errors in the initial estimate
	tribool flagInitialEstimate;	///< \c true if an initial estimate exists (either as a matrix, or a camera calibration file)

	double binDensity;	///< Number of bins per standard deviation, for spatial quantisation

	double pValue;	///< Probability of rejecting an inlier

	double eligibilityRatio;	///< Percentage of correspondence pair that is within 1-pixel of their true, noise-free values

	double maxAlgebraicDistance;	///< If the algebraic distance between two models is below this value, they are deemed similar. Percentage of the maximum distance.
	double loGeneratorRatio1;	///< Ratio of the cardinality of the LO generator to that of the minimal generator, first stage
	double loGeneratorRatio2;	///< Ratio of the cardinality of the LO generator to that of the minimal generator, second stage

	string featureFile1;   ///< Filename for the first feature file
	string cameraFile1;	///< Filename for the first camera file
	bool flagCamera1;	///< \c true if there is a camera matrix associated with the first feature set
	FrameT boundingBox1;	///< Bounding box in the first image
	bool flagAutoBoundingBox1;  ///< true: Bounding box is not initialised and should be set automatically
	double spacing1;	///< Grid spacing for the first set

	string featureFile2;   ///< Filename for the second feature file
	string cameraFile2;	///< Filename for the second camera file
	bool flagCamera2;	///< \c true if there is a camera matrix associated with the second feature set
	FrameT boundingBox2;	///< Bounding box in the second image
	bool flagAutoBoundingBox2;  ///< true: Bounding box is not initialised and should be set automatically
	double spacing2;     ///< Grid spacing for the second set

	string outputPath;	///< Output path
	string modelFile;	///< Geometric model
	string covarianceFile;	///< Covariance of the estimated model
	string correspondenceFile;	///< Correspondences
	string correspondingFeatureFile1;	///< Corresponding features in the first set
	string correspondingFeatureFile2;	///< Corresponding features in the second set
	string logFile;	///< Log
	bool flagVerbose;   ///< Verbosity flag

	GeometryEstimationPipelineParametersC pipelineParameters;	///< Parameters for the geometry estimation pipeline
};	//struct GeometryEstimator22ParametersC

/**
 * @brief Implementation of GeometryEstimtor22
 * @ingroup Application
 */
class GeometryEstimator22ImplementationC
{
	private:

    	auto_ptr<options_description> commandLineOptions; ///< Command line options
                                                      	  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

    	GeometryEstimator22ParametersC parameters;  ///< Application parameters

    	typedef ImageFeatureC FeatureT;	///< Feature type
		typedef FeatureT::coordinate_type CoordinateT;	///< Coordinate type

		typedef GeometryEstimator22ParametersC::Geometry22ProblemT Geometry22ProblemT;

    	/** @name Implementation details */ //@{
    	vector<FeatureT> LoadFeatures(const LensDistortionC& distortion, const optional<IntrinsicCalibrationMatrixT>& mK, unsigned int setId, bool flagNormalise) const;	///< Loads and preprocesses the features
    	void PostprocessFeatures(vector<FeatureT>& featureList, const LensDistortionC& distortion, const  optional<IntrinsicCalibrationMatrixT>& mK, bool flagDenormalise) const;	///< Postprocessing of features, prior to saving

    	optional<CameraC> LoadCamera(const string& cameraFile) const;	///< Loads a camera file

    	tuple<LensDistortionC, optional<IntrinsicCalibrationMatrixT> > ExtractInternalParameters(const CameraC& camera) const;	///< Extracts the lens distortion and the internal calibration parameters
    	Geometry22ProblemT InferProblemType(const CameraC& camera1, const CameraC& camera2) const;	///< Infers the problem type
    	optional<Matrix3d> ComputeInitialEstimate(const CameraC& camera1, const CameraC& camera2) const;	///< Computes an initial estimate
    	auto ProcessCameras(const optional<CameraC>& camera1, const optional<CameraC>& camera2) -> tuple<optional<IntrinsicCalibrationMatrixT>, LensDistortionC, optional<IntrinsicCalibrationMatrixT>, LensDistortionC, optional<Matrix3d> >;	///< Processes the cameras to extract various information

    	typedef RANSACHomography2DProblemT::dimension_type1 dimension_type;
    	tuple<dimension_type, dimension_type> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const;	///< Computes the bin sizes for RANSAC

    	GeometryEstimationPipelineDiagnosticsC ComputeHomography(Homography2DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography2DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2);	///< Runs the homography estimation pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeFundamentalMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2);	///< Runs the fundamental matrix estimation pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeEssentialMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2);	///< Runs the essential matrix estimation pipeline
    	GeometryEstimationPipelineDiagnosticsC ComputeOneSidedFundamentalMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2);	///< Runs the one-sided fundamental matrix estimation pipeline, with known first camera

    	void SaveOutput(vector<FeatureT>& featureList1, vector<FeatureT>& featureList2, const Matrix3d& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics, double runtime) const;	///< Saves the output
    	//@}

	public:

    	/**@name ApplicationImplementationConceptC interface */ //@{
    	GeometryEstimator22ImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
    	//@}
};	//class GeometryEstimator22ImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Loads and preprocesses the features
 * @param[in] distortion Lens distortion
 * @param[in] mK Intrinsic calibration matrix. Invalid if not known
 * @param[in] setId Feature set id
 * @param[in] flagNormalise If \c true , feature set is normalised with the inverse of \c mK
 * @return A vector of image features
 */
auto GeometryEstimator22ImplementationC::LoadFeatures(const LensDistortionC& distortion, const optional<IntrinsicCalibrationMatrixT>& mK, unsigned int setId, bool flagNormalise) const -> vector<FeatureT>
{
	bool flagAutoBoundingBox = (setId==1) ? parameters.flagAutoBoundingBox1 : parameters.flagAutoBoundingBox2;
	optional< tuple<CoordinateT, CoordinateT> > boundingBox;
	string filename = (setId==1) ? parameters.featureFile1 : parameters.featureFile2;
	double spacing = (setId==1) ? parameters.spacing1 : parameters.spacing2;

	optional<CoordinateTransformations2DT::affine_transform_type> normaliser;
	if(mK && flagNormalise)
	{
		normaliser=CoordinateTransformations2DT::affine_transform_type();
		normaliser->matrix()=mK->inverse();
	}	//if(mK && flagNormalise)

	if(!flagAutoBoundingBox)
		boundingBox=make_tuple( (setId==1) ? parameters.boundingBox1.corner(FrameT::BottomLeft) : parameters.boundingBox2.corner(FrameT::BottomLeft), (setId==1) ? parameters.boundingBox1.corner(FrameT::TopRight) : parameters.boundingBox2.corner(FrameT::TopRight) );

	vector<FeatureT> featureList;
#pragma omp critical (GE22_LF1)
	featureList=ImageFeatureIOC::ReadFeatureFile(filename, true);

	ImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, spacing, true, distortion, normaliser);
	return featureList;
}	//vector<FeatureT> LoadFeatures(const LensDistortionC& distortion, unsigned int setId) const

/**
 * @brief Postprocessing of features, prior to saving
 * @param[in, out] featureList Feature list to be processed
 * @param[in] distortion Lens distortion
 * @param[in] mK Intrinsic calibration matrix
 * @param[in] flagDenormalise If \c true denormalise
 */
void GeometryEstimator22ImplementationC::PostprocessFeatures(vector<FeatureT>& featureList, const LensDistortionC& distortion, const  optional<IntrinsicCalibrationMatrixT>& mK, bool flagDenormalise) const
{
	optional<CoordinateTransformations2DT::projective_transform_type> denormaliser;

	if(mK && flagDenormalise)
	{
		denormaliser=CoordinateTransformations2DT::projective_transform_type();
		denormaliser->matrix()=*mK;
	}	//if(mK && flagNormalise)

	ImageFeatureIOC::PostprocessFeatureSet(featureList, distortion, denormaliser);
}	//PostprocessFeatures(const LensDistortionC& distortion, const  optional<IntrinsicCalibrationMatrixT>& mK, unsigned int setId, bool flagDenormalise)

/**
 * @brief Loads a camera file
 * @param[in] cameraFile Camera file
 * @return A camera. Invalid if cameraFile does not contain any cameras
 */
optional<CameraC> GeometryEstimator22ImplementationC::LoadCamera(const string& cameraFile) const
{
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(cameraFile);

	if(parameters.flagVerbose)
	{
		if(cameraList.empty())
			cout<<"Empty camera file : "<<cameraFile<<"\n";

		if(cameraList.size()>1)
			cout<<"Multiple cameras in file "<<cameraFile<<". No camera selected.\n";
	}	//if(parameters.flagVerbose)

	if(cameraList.size()==1)
		return cameraList[0];
	else
		return optional<CameraC>();
}	//optional<CameraC> ReadCamera(const string& cameraFile) const

/**
 * @brief Extracts the lens distortion and the internal calibration parameters
 * @param[in] camera Camera
 * @return A tuple containing the lens distotion and the intrinsic calibration parameters
 */
tuple<LensDistortionC, optional<IntrinsicCalibrationMatrixT> > GeometryEstimator22ImplementationC::ExtractInternalParameters(const CameraC& camera) const
{
	LensDistortionC distortion=LensDistortionC();

	if(camera.Distortion())
		distortion=*camera.Distortion();
	else
		distortion.modelCode=LensDistortionCodeT::NOD;

	optional<IntrinsicCalibrationMatrixT> mK=camera.MakeIntrinsicCalibrationMatrix();

	//Unknown focal length, but the other parameters are known
	if(parameters.problemType==Geometry22ProblemT::OSFND && !mK && camera.Intrinsics()->principalPoint)
	{
		mK=IntrinsicCalibrationMatrixT::Identity();
		mK->block(0,2, 2, 1)=*camera.Intrinsics()->principalPoint;
		(*mK)(0,1)=camera.Intrinsics()->skewness;

		(*mK)(0,0)=(*mK)(0,2)/tan(0.5*50*2*pi<double>()/360);	//As suggested in "One-sided fundamental matrix estimation, " Brito et al.
		(*mK)(1,1)=(*mK)(0,0)*camera.Intrinsics()->aspectRatio;
	}	//if(!mK && camera.Intrinsics()->principalPoint)

	return make_tuple(distortion, mK);
}	//tuple< optional<LensDistortionC>, optional<IntrinsicCalibrationMatrixT> > ExtractInternalParameters(const CameraC& camera) const

/**
 * @brief Infers the problem type
 * @param[in] camera1 Camera associated with the first set
 * @param[in] camera2 Camera associated with the second set
 * @return Problem code
 */
auto GeometryEstimator22ImplementationC::InferProblemType(const CameraC& camera1, const CameraC& camera2) const -> Geometry22ProblemT
{
	//If the camera centres are coincident, homography
	if(camera1.Extrinsics() && camera1.Extrinsics()->position && camera2.Extrinsics() && camera2.Extrinsics()->position)
	{
		Coordinate3DT vC1=*camera1.Extrinsics()->position;
		Coordinate3DT vC2=*camera2.Extrinsics()->position;

		if(vC1.isApprox(vC2,  numeric_limits<CameraC::real_type>::epsilon()))
			return Geometry22ProblemT::HMG2;
	}	//if(camera1.Extrinsics() && camera1.Extrinsics()->position && camera2.Extrinsics() && camera2.Extrinsics()->position)

	//If not a homography, try the epipolar alternatives

	bool flagK1=(bool)camera1.MakeIntrinsicCalibrationMatrix();
	bool flagK2=(bool)camera2.MakeIntrinsicCalibrationMatrix();

	if(flagK1 && flagK2)
		return Geometry22ProblemT::ESS;

	//Since the input is always a metric camera, one-sided fundamental matrix problems cannot be inferred automatically

	return Geometry22ProblemT::FND;
}	//Geometry22ProblemT InferProblemType(const CameraC& camera1, const CameraC& camera2) const

/**
 * @brief Computes an initial estimate
 * @param[in] camera1 Camera associated with the first set
 * @param[in] camera2 Camera associated with the second set
 * @return Initial estimate. Invalid, if the cameras do not have enough information for an initial estimate
 */
optional<Matrix3d> GeometryEstimator22ImplementationC::ComputeInitialEstimate(const CameraC& camera1, const CameraC& camera2) const
{
	optional<IntrinsicCalibrationMatrixT> mK1=camera1.MakeIntrinsicCalibrationMatrix();
	optional<IntrinsicCalibrationMatrixT> mK2=camera2.MakeIntrinsicCalibrationMatrix();

	if(parameters.problemType==Geometry22ProblemT::HMG2)
	{
		//Derive an initial estimate only if the camera centres are coincident
		optional<RotationMatrix3DT> mR1=camera1.MakeRotationMatrix();
		optional<Coordinate3DT> vC1;
		if(camera1.Extrinsics())
			vC1=camera1.Extrinsics()->position;

		optional<RotationMatrix3DT> mR2=camera2.MakeRotationMatrix();
		optional<Coordinate3DT> vC2;
		if(camera2.Extrinsics())
			vC2=camera2.Extrinsics()->position;

		bool flagNoTranslation= (vC1 && vC2) && ( vC1->isApprox(*vC2,  numeric_limits<CameraC::real_type>::epsilon()));
		if(flagNoTranslation && mR1 && mR2 && mK1 && mK2)
			return CameraToHomography(*mR1, *mK1, *mR2, *mK2);
	}	//if(parameters.problemType==Geometry22ProblemT::HMG2)

	if(parameters.problemType==Geometry22ProblemT::FND)
	{
		optional<CameraMatrixT> mP1=camera1.MakeCameraMatrix();
		optional<CameraMatrixT> mP2=camera2.MakeCameraMatrix();

		if(mP1 && mP2)
			return CameraToFundamental(*mP1, *mP2);
	}	//if(parameters.problemType==Geometry22ProblemT::FND)

	if(parameters.problemType==Geometry22ProblemT::OSFND && mK1)
	{
		optional<CameraMatrixT> mP1n=camera1.MakeNormalisedCameraMatrix();
		optional<CameraMatrixT> mP2=camera2.MakeCameraMatrix();

		if(mP1n && mP2)
			return CameraToFundamental(*mP1n, *mP2);	//Still needs to be multiplied on the left by a normaliser
	}	//if(parameters.problemType==Geometry22ProblemT::OSFND1 && mK1)

	if(parameters.problemType==Geometry22ProblemT::ESS && mK1 && mK2)
	{
		optional<CameraMatrixT> mP1n=camera1.MakeNormalisedCameraMatrix();
		optional<CameraMatrixT> mP2n=camera2.MakeNormalisedCameraMatrix();

		if(mP1n && mP2n)
		    return CameraToFundamental(*mP1n, *mP2n);
	}	//if(parameters.problemType==Geometry22ProblemT::ESS && mK1 && mK2)

	return optional<Matrix3d>();
}	//optional<Matrix3d> ComputeInitialEstimate(const CameraC& camera1, const CameraC& camera2)

/**
 * @brief Processes the cameras to extract various information
 * @param[in] camera1 First camera
 * @param[in] camera2 Second camera
 * @return A tuple of camera parameters: Intrinsic calibration matrices, lens distortion and the initial estimate
 */
auto GeometryEstimator22ImplementationC::ProcessCameras(const optional<CameraC>& camera1, const optional<CameraC>& camera2) -> tuple<optional<IntrinsicCalibrationMatrixT>, LensDistortionC, optional<IntrinsicCalibrationMatrixT>, LensDistortionC, optional<Matrix3d> >
{
	bool flagCamera1=(bool)camera1;
	LensDistortionC distortion1;
	optional<IntrinsicCalibrationMatrixT> mK1;
	if(flagCamera1)
		std::tie(distortion1, mK1)=ExtractInternalParameters(*camera1);

	bool flagCamera2=(bool)camera2;
	LensDistortionC distortion2;
	optional<IntrinsicCalibrationMatrixT> mK2;
	if(flagCamera2)
		std::tie(distortion2, mK2)=ExtractInternalParameters(*camera2);

	//Infer problem type
	if(parameters.problemType==Geometry22ProblemT::AUTO)
	{
		if(flagCamera1 && flagCamera2)
			parameters.problemType=InferProblemType(*camera1, *camera2);
		else
			parameters.problemType=Geometry22ProblemT::FND;
	}	//if(parameters.problemType==Geometry22ProblemT::AUTO)

	//Initial estimate
	optional<Matrix3d> initialEstimate;
	if(flagCamera1 && flagCamera2 && indeterminate(parameters.flagInitialEstimate))
		initialEstimate=ComputeInitialEstimate(*camera1, *camera2);

	return make_tuple(mK1, distortion1, mK2, distortion2, initialEstimate);
}	//void ProcessCameras(const optional<CameraC>& camera1, const optional<CameraC>& camera2) const

/**
 * @brief Computes the bin sizes for RANSAC
 * @param[in] featureList1 First feature list
 * @param[in] featureList2 Second feature list
 * @pre \c featureList1 is nonempty
 * @pre \c featureList2 is nonempty
 * @return A pair of bins. [First set; Second set]
 */
auto GeometryEstimator22ImplementationC::ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const -> tuple<dimension_type, dimension_type>
{
	//Preconditions
	assert(!featureList1.empty());
	assert(!featureList2.empty());

	vector<CoordinateT> coordinateList1=MakeCoordinateVector(featureList1);
	dimension_type binSize1=*ComputeBinSize<CoordinateT>(coordinateList1, parameters.binDensity);

	vector<CoordinateT> coordinateList2=MakeCoordinateVector(featureList2);
	dimension_type binSize2=*ComputeBinSize<CoordinateT>(coordinateList2, parameters.binDensity);

	return make_tuple(binSize1, binSize2);
}	//tuple<dimension_type1, dimension_type2> ComputeRANSACBinSize(const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
GeometryEstimator22ImplementationC::GeometryEstimator22ImplementationC()
{
	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("Environment.NoThreads", value<string>()->default_value("1"), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")
	("Environment.Seed", value<string>()->default_value("0"), "Seed for the random number generator. If <0, rng default")

	("General.GeometryType", value<string>()->default_value(""), "Geometric relation between the feature sets. Homography, Fundamental, OSFundamental, Essential. If empty, the application automatically determines an appropriate model.")
	("General.InitialEstimateFile", value<string>()->default_value(""), "A file containing a 3x3 matrix, corresponding to an initial estimate of the geometry. If no initial estimate available, empty")
	("General.DeriveInitialEstimate", value<string>()->default_value("1"), "If true, the program attempts to derive an initial estimate from the provided camera files, if no initial estimate file is specified")
	("General.Noise", value<string>()->default_value("4"), "Variance of the noise on the image coordinates, due to the detector. pixel^2. >0")
	("General.InitialEstimateNoise", value<string>()->default_value("0"), "Variance of the noise on the image coordinates, due to the errors in the initial estimate. Cumulative with the detector noise, but applied only in the first guided matching iteration. pixel^2. >=0")

	("GuidedMatching.MaxIteration", value<string>()->default_value("10"), "Maximum number of iterations. >0")
	("GuidedMatching.MinRelativeDifference", value<string>()->default_value("0.01"), "If the relative difference between the fitness scores of two successive iterations is below this value, terminate. >0")
	("GuidedMatching.BinDensity", value<string>()->default_value("4"), "Number of feature bins per standard deviation. Used in spatial quantisation. >0")
	("GuidedMatching.pValue", value<string>()->default_value("0.05"), "Probability of rejection for an inlier. Determines the inlier threshold. [0,1]")
	("GuidedMatching.EligibilityRatio", value<string>()->default_value("0.72"), "Ratio of observation pairs which are within 1-pixel of their noise-free values. >0")

	("GuidedMatching.Decimation.MaxObservationDensity", value<string>()->default_value("5"), "Number of features per bin. Determines the number of observations. >0")
	("GuidedMatching.Decimation.MinObservationRatio", value<string>()->default_value("10"), "Minimum ratio of the number of observations to the size of the generator. Determines the minimum size of the observation set. >1")
	("GuidedMatching.Decimation.ValidationRatio", value<string>()->default_value("10"), "Ratio of the number of validators to the size of the generator. >1")
	("GuidedMatching.Decimation.MaxAmbiguity", value<string>()->default_value("0.85"), "Maximum value of Matcher.FeatureMatcher.MaxRatio in a decimated set. Used only in the first pass, if there is no initial estimate. [0,1]")

	("Matcher.FeatureMatcher.MaxRatio", value<string>()->default_value("0.8"), "Maximum ratio of the similarity score of the best outsider to that of the best insider. See the documentation of NearestNeighbourMatcherC. [0,1]")
	("Matcher.FeatureMatcher.MaxFeatureDistance", value<string>()->default_value("0.5"), "Maximum distance between the normalised descriptor vectors of two corresponding points, as a percentage of the maximum possible distance.[0 1]")
	("Matcher.FeatureMatcher.NeighbourhoodCardinality", value<string>()->default_value("1"), "k in k-NN: Each feature can be associated with up to k of its nearest neighbours. >0")

	("Matcher.BucketFilter.Enable", value<string>()->default_value("1"), "If true, the bucket filter is enabled. Useful for eliminating outliers. 0 or 1")
	("Matcher.BucketFilter.MinSimilarity", value<string>()->default_value("0.5"), "Minimum similarity between two buckets for a valid bucket match. [0,1]")
	("Matcher.BucketFilter.MinQuorum", value<string>()->default_value("3"), "If no buckets in a pair has this many votes, the pair is exempted due to insufficient evidence for rejection (i.e., automatic success).>=0")
	("Matcher.BucketFilter.BucketDensity", value<string>()->default_value("10"), "Number of buckets, per unit standard deviation. >0")

	("GeometryEstimator.TSRANSAC.Confidence", value<string>()->default_value("0.99"), "Probability that the solution estimated by RANSAC is 'good'. Affects the number of iterations. [0,1]")

	("GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", value<string>()->default_value("10000"), "Maximum number of iterations. >0")
	("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations", value<string>()->default_value("10"), "Number of LO iterations. >=0")
	("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value("4"), "If the ratio of the number of inlier observations to the size of the generator set is below this value, LO-RANSAC does not trigger. Prevents LO-RANSAC from triggering at poor hypotheses. >=1")
	("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio", value<string>()->default_value("2"), "Ratio of the cardinality of the LO generator to that of the minimal generator. >=1 ")
	("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate", value<string>()->default_value("0.01"), "Rate of expansion of the observation set, as a percentage of the total number of observations. Any value >1 disables PROSAC. >0")

	("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations", value<string>()->default_value("10"), "Number of LO iterations. >=0")
	("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio", value<string>()->default_value("4"), "If the ratio of the number of inlier observations to the size of the generator set is below this value, LO-RANSAC does not trigger. Prevents LO-RANSAC from triggering at poor hypotheses. >=1")
	("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio", value<string>()->default_value("2"), "Ratio of the cardinality of the LO generator to that of the minimal generator. >=1 ")
	("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", value<string>()->default_value("0.01"), "Rate of expansion of the observation set, as a percentage of the total number of observations. Any value >1 disables PROSAC. >0")
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution", value<string>()->default_value("1"), "Number of solutions RANSAC returns")
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage", value<string>()->default_value("1"), "Minimum percentage of validators that are inliers to a model. Suppresses poor solutions. The first model is exempt. [0,1]")
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusion", value<string>()->default_value("0.75"), "Given the inlier validator sets A and B, if |intersection(A,B)|/min(|A|,|B|) is above this value, the corresponding models are deemed similar. [0,1]" )
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance", value<string>()->default_value("0.1"), "If the algebraic distance between two models below this value, they are deemed similar. Percentage of the maximum distance. [0,1]" )
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor", value<string>()->default_value("1.05"), "Lower values encourage MO-RANSAC to generate more solutions. >=1" )
	("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor", value<string>()->default_value("0.95"), "Lower values encourage MO-RANSAC to accept updates that reduce the diversity of the solution set. >=0" )

	("GeometryEstimator.Optimisation.MaxIteration", value<string>()->default_value("100"), "Maximum number of nonlinear least squares iterations. >=0")

	("GeometryEstimator.Covariance.Enable", value<string>()->default_value("1"), "If false, covariance estimation is disabled")
	("GeometryEstimator.Covariance.Alpha", value<string>()->default_value("-1"), "Sample spread parameter for SUT. Lower values a better for highly nonlinear transformations, however, too small values may lead to ill-conditioned covariance matrices. (0,1]. If not defined/invalid, automatic.")

	("FeatureSet1.FeatureFile", value<string>(), ".ft2 file containing the first feature set")
	("FeatureSet1.CameraFile", value<string>(), ".cam file containing the known calibration parameters")
	("FeatureSet1.RoI", value<string>(), "Region-of-interest in the first image, in pixels. If not specified, bounding box of the features. [left upper right lower]")
	("FeatureSet1.GridSpacing", value<string>()->default_value("7"), "Affects the decimation factor of the initial set. 0 means no decimation. See the documentation of DecimateFeatures2D. >=0")

	("FeatureSet2.FeatureFile", value<string>(), ".ft2 file containing the second feature set")
	("FeatureSet2.CameraFile", value<string>(), ".cam file containing the known calibration parameters")
	("FeatureSet2.RoI", value<string>(), "Region-of-interest in the second image, in pixels. If not specified, bounding box of the features. [left upper right lower]")
	("FeatureSet2.GridSpacing", value<string>()->default_value("7"), "Affects the decimation factor of the initial set. 0 means no decimation. See the documentation of DecimateFeatures2D. >=0")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Model", value<string>(), "Estimated model, a 3x3 matrix")
	("Output.Covariance", value<string>(), "Covariance matrix for the estimated model")
	("Output.Correspondences", value<string>(), "Correspondences as a .c22 file")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.FeatureList1", value<string>()->default_value(""), "Corresponding features from the first set")
	("Output.FeatureList2", value<string>()->default_value(""), "Corresponding features from the second set")
	("Output.Verbose", value<string>()->default_value("1"), "Verbose output")
	;
}	//GeometryEstimator22ImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& GeometryEstimator22ImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void GeometryEstimator22ImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
    ptree tree; //Options tree

    tree.put("xmlcomment", "geometryEstimator22 configuration file");

    tree.put("Environment", "");
    tree.put("Environment.NoThreads", "1");
    tree.put("Environment.Seed", "0");

    tree.put("General", "");
    tree.put("General.GeometryType", "Homography");

    tree.put("GuidedMatching", "");
	tree.put("GuidedMatching.EligibilityRatio", "0.72");

	tree.put("GuidedMatching.Decimation", "");

	tree.put("Matcher", "");

	tree.put("Matcher.FeatureMatcher","");
	tree.put("Matcher.FeatureMatcher.NeighbourhoodCardinality", "1");

	tree.put("GeometryEstimator","");

	tree.put("GeometryEstimator.TSRANSAC","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration","10000");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC","");
	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations","10");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC","");
	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations","10");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC","");

	tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC","");

	tree.put("GeometryEstimator.Optimisation","");
	tree.put("GeometryEstimator.Optimisation.MaxIteration","100");

	tree.put("ScaledUnscentedTransformation","");

	tree.put("FeatureSet1","");
	tree.put("FeatureSet1.FeatureFile","/home/f1.ft2");
	tree.put("FeatureSet1.CameraFile","/home/c1.cam");

	tree.put("FeatureSet2","");
	tree.put("FeatureSet2.FeatureFile","/home/f2.ft2");
	tree.put("FeatureSet2.CameraFile","/home/c2.cam");

	tree.put("Output","");
	tree.put("Output.Root","/home/");
	tree.put("Output.Model","mH.txt");
	tree.put("Output.Covariance","mCov.txt");
	tree.put("Output.Correspondences","corr.c22");
	tree.put("Output.Log","GeometryEstimator22.log");
	tree.put("Output.FeatureList1","o1.ft2");
	tree.put("Output.FeatureList2","o2.ft2");
	tree.put("Output.Verbose","1");

	//Advanced interface
	if(detailLevel>1)
	{
	    tree.put("General.InitialEstimateFile", "/home/mH");
	    tree.put("General.DeriveInitialEstimate", "1");
	    tree.put("General.Noise", "4");
	    tree.put("General.InitialEstimateNoise", "0");

	    tree.put("GuidedMatching.MaxIteration", "10");
		tree.put("GuidedMatching.MinRelativeDifference", "0.01");
		tree.put("GuidedMatching.BinDensity", "4");
		tree.put("GuidedMatching.pValue", "0.05");

		tree.put("GuidedMatching.Decimation.MaxObservationDensity", "5");
		tree.put("GuidedMatching.Decimation.MinObservationRatio", "10");
		tree.put("GuidedMatching.Decimation.ValidationRatio", "10");
		tree.put("GuidedMatching.Decimation.MaxAmbiguity", "0.85");

		tree.put("Matcher.FeatureMatcher.MaxRatio", "0.8");
		tree.put("Matcher.FeatureMatcher.MaxFeatureDistance", "0.5");

		tree.put("Matcher.BucketFilter","");
		tree.put("Matcher.BucketFilter.Enable","1");
		tree.put("Matcher.BucketFilter.MinSimilarity","0.5");
		tree.put("Matcher.BucketFilter.MinQuorum","3");
		tree.put("Matcher.BucketFilter.BucketDensity","10");

		tree.put("GeometryEstimator.TSRANSAC.Confidence","0.99");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio","4");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio","2");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate","0.01");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio","4");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio","2");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate","0.01");

		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution","1");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage","1");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusion","0.75");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance","0.1");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor", "1.05");
		tree.put("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor", "0.95");

		tree.put("GeometryEstimator.Covariance.Enable", "1");
		tree.put("GeometryEstimator.Covariance.Alpha", "-1");

		tree.put("FeatureSet1.RoI","0 0 1919 1079");
		tree.put("FeatureSet1.GridSpacing","7");

		tree.put("FeatureSet2.RoI","0 0 1919 1079");
		tree.put("FeatureSet2.GridSpacing","7");
	}	//if(!flagBasic)

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool GeometryEstimator22ImplementationC::SetParameters(const ptree& tree)
{
	//Environment
	parameters.nThreads=min(omp_get_max_threads(), max(1, tree.get<int>("Environment.NoThreads", 1)));
	parameters.seed=tree.get<int>("Environment.Seed", -1);

	parameters.pipelineParameters.nThreads=parameters.nThreads;

	//General

	string geometryType=tree.get<string>("General.GeometryType", "Automatic");
	map<string, Geometry22ProblemT> validGeometryTypes{ {"Automatic", Geometry22ProblemT::AUTO}, {"Homography", Geometry22ProblemT::HMG2}, {"Fundamental", Geometry22ProblemT::FND}, {"OSFundamental", Geometry22ProblemT::OSFND}, {"Essential", Geometry22ProblemT::ESS}};
	auto itGT=validGeometryTypes.find(geometryType);
	if(itGT==validGeometryTypes.end())
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Invalid problem type. Value=")+geometryType));
	else
		parameters.problemType=itGT->second;

	parameters.flagInitialEstimate=indeterminate;
	parameters.initialEstimateFile=tree.get<string>("General.InitialEstimateFile","");
	if(!parameters.initialEstimateFile.empty())
		parameters.flagInitialEstimate=true;
	else
	{
		if(!tree.get<bool>("General.DeriveInitialEstimate",false))
			parameters.flagInitialEstimate=false;
	}	//if(!parameters.initialEstimateFile.empty())

	double noise=tree.get<double>("General.Noise", 4);
	if(noise<=0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: General.Noise must be a positive value. Value=")+lexical_cast<string>(noise) ) );
	else
		parameters.noiseVariance=noise;

	double initialEstimateNoise=tree.get<double>("General.InitialEstimateNoise", 0);
	if(initialEstimateNoise<0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: General.InitialEstimateNoise must be a non-negative value. Value=")+lexical_cast<string>(initialEstimateNoise) ) );
	else
		parameters.initialEstimateError=initialEstimateNoise;

	//Guided matching

	double eligibilityRatio=tree.get<double>("GuidedMatching.EligibilityRatio",1);
	if(eligibilityRatio<=0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.EligibilityRatio must be a positive value. Value=")+lexical_cast<string>(eligibilityRatio) ) );
	else
		parameters.eligibilityRatio=eligibilityRatio;

	unsigned int maxGMIteration=tree.get<unsigned int>("GuidedMatching.MaxIteration",10);
	parameters.pipelineParameters.maxIteration=maxGMIteration;

	double minRelativeDifference=tree.get<double>("GuidedMatching.MinRelativeDifference",0.01);
	if(minRelativeDifference<0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.minRelativeDifference must be a nonnegative value. Value=")+lexical_cast<string>(minRelativeDifference) ) );
	else
		parameters.pipelineParameters.minRelativeDifference=minRelativeDifference;

	unsigned int binDensity=tree.get<unsigned int>("GuidedMatching.BinDensity",4);
	if(binDensity==0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.BinDensity must be a positive value. Value=")+lexical_cast<string>(binDensity) ) );
	else
	{
		parameters.binDensity=binDensity;
		parameters.pipelineParameters.binDensity1=binDensity;
		parameters.pipelineParameters.binDensity2=binDensity;
	}	//if(binDensity==0)

	double pValue=tree.get<double>("GuidedMatching.pValue", 0.05);
	if(pValue<=0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.pValue must be a positive value. Value=")+lexical_cast<string>(pValue) ) );
	else
		parameters.pValue=pValue;

	//GuidedMatching.Decimation

	unsigned int maxObservationDensity=tree.get<unsigned int>("GuidedMatching.Decimation.MaxObservationDensity", 5);
	if(maxObservationDensity<1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Decimation.MaxObservationDensity must be >1. Value=")+lexical_cast<string>(maxObservationDensity) ) );
	else
		parameters.pipelineParameters.maxObservationDensity=maxObservationDensity;

	double minObservationRatio=tree.get<double>("GuidedMatching.Decimation.MinObservationRatio",1);
	if(minObservationRatio<1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.Decimation.MinObservationRatio must be >=1. Value=")+lexical_cast<string>(minObservationRatio) ) );
	else
		parameters.pipelineParameters.minObservationRatio=minObservationRatio;

	double validationRatio=tree.get<double>("GuidedMatching.Decimation.ValidationRatio", 10);
	if(validationRatio<=1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.Decimation.ValidationRatio must be >1. Value=")+lexical_cast<string>(validationRatio) ) );
	else
		parameters.pipelineParameters.validationRatio=validationRatio;

	parameters.pipelineParameters.maxObservationRatio= pow<2>(2*3*binDensity)*maxObservationDensity;	//Approximate number of bins x density. Functionally, infinite

	double maxAmbiguity=tree.get<double>("GuidedMatching.Decimation.MaxAmbiguity", 0.85);
	if(maxAmbiguity<0 || maxAmbiguity>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GuidedMatching.Decimation.MaxAmbiguity must be in [0,1]. Value=")+lexical_cast<string>(maxAmbiguity)));
	else
		parameters.pipelineParameters.maxAmbiguity=maxAmbiguity;

	//Matcher.FeatureMatcher

	parameters.pipelineParameters.matcherParameters.flagStatic=false;

	double maxRatio=tree.get<double>("Matcher.FeatureMatcher.MaxRatio", 0.8);
	if(maxRatio<0 || maxRatio>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Matcher.FeatureMatcher.MaxRatio must be in [0,1]. Value=")+lexical_cast<string>(maxRatio) ) );
	else
		parameters.pipelineParameters.matcherParameters.nnParameters.ratioTestThreshold=maxRatio;

	double maxFeatureDistance=tree.get<double>("Matcher.FeatureMatcher.MaxFeatureDistance", 0.5);
	if(maxFeatureDistance<0 || maxFeatureDistance>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Matcher.FeatureMatcher.MaxFeatureDistance must be in [0,1]. Value=")+lexical_cast<string>(maxFeatureDistance) ) );
	else
		parameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold=maxFeatureDistance*sqrt(2);	//Max distance between two unit-norm vectors is sqrt(2)

	unsigned int neighbourhoodCardinality=tree.get<unsigned int>("Matcher.FeatureMatcher.NeighbourhoodCardinality", 1);
	if(neighbourhoodCardinality==0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Matcher.FeatureMatcher.NeighbourhoodCardinality must be positive. Value=")+lexical_cast<string>(neighbourhoodCardinality) ) );
	else
	{
		parameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12=neighbourhoodCardinality;
		parameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21=neighbourhoodCardinality;
	}	//if(neighbourhoodCardinality==0)

	parameters.pipelineParameters.matcherParameters.nnParameters.flagConsistencyTest=true;

	bool flagBucketFilter=tree.get<bool>("Matcher.BucketFilter.Enable", true);
	parameters.pipelineParameters.matcherParameters.flagBucketFilter=flagBucketFilter;

	if(flagBucketFilter)
	{
	    double minBucketSimilarity=tree.get<double>("Matcher.BucketFilter.MinSimilarity", 0.5);
	    if(minBucketSimilarity<0 || minBucketSimilarity>1)
	        throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Matcher.BucketFilter.MinSimilarity is not in [0,1]. Value=")+lexical_cast<string>(minBucketSimilarity)));
	    else
	    	parameters.pipelineParameters.matcherParameters.bucketSimilarityThreshold=minBucketSimilarity;

	    unsigned int minQuorum=tree.get<unsigned int> ("Matcher.BucketFilter.MinQuorum", 3);
	    parameters.pipelineParameters.matcherParameters.minQuorum=minQuorum;

	    unsigned int bucketDensity=tree.get<unsigned int>("Matcher.BucketFilter.BucketDensity", 10);
	    if(bucketDensity<=0)
	        throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: Matcher.BucketFilter.BucketDensity is not positive. Value=")+lexical_cast<string>(bucketDensity)));

	    parameters.pipelineParameters.matcherParameters.bucketDimensions.fill(1.0/bucketDensity);
	}	//if(flagBucketFilter)

	//GeometryEstimator.TSRANSAC

	double confidence=tree.get<double>("GeometryEstimator.TSRANSAC.Confidence", 0.99);
	if(confidence<0 || confidence>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.Confidence is not in [0,1]. Value=")+lexical_cast<string>(confidence)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.minConfidence=confidence;

	unsigned int maxIterationRS1=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration", 10000);
	if(maxIterationRS1==0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.MaxIteration must be positive. Value=")+lexical_cast<string>(maxIterationRS1)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration=maxIterationRS1;

	double growthRateRS1=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate", 0.01);
	if(growthRateRS1<=0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.PROSAC.GrowthRate must be positive. Value=")+lexical_cast<string>(growthRateRS1)));
	else
	{
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.flagPROSAC= (growthRateRS1<=1);

		if(growthRateRS1<=1)
			parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.growthRate=growthRateRS1;
	}	//if(growthRateRS1<=0)

	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.eligibilityRatio=1;
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxErrorRatio=1.05;

	unsigned int nLOIteration1=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.NoIterations", 10);
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.nLOIterations=nLOIteration1;

	double loMinObservationSupport1=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio",4);
	if(loMinObservationSupport1<0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.MinimumObservationSupportRatio must be positive. Value=")+lexical_cast<string>(loMinObservationSupport1)));
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minObservationSupport=loMinObservationSupport1;

	double loGeneratorRatio1=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage1.LORANSAC.GeneratorRatio", 2);
	if(loGeneratorRatio1<1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio must be >=1. Value=")+lexical_cast<string>(loGeneratorRatio1)));
	parameters.loGeneratorRatio1=loGeneratorRatio1;

	unsigned int nLOIteration2=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.NoIterations", 10);
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.nLOIterations=nLOIteration2;

	double loMinObservationSupport2=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio",4);
	if(loMinObservationSupport2<0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.MinimumObservationSupportRatio must be positive. Value=")+lexical_cast<string>(loMinObservationSupport2)));
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minObservationSupport=loMinObservationSupport2;

	double loGeneratorRatio2=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio", 2);
	if(loGeneratorRatio2<1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.LORANSAC.GeneratorRatio must be >=1. Value=")+lexical_cast<string>(loGeneratorRatio2)));
	parameters.loGeneratorRatio2=loGeneratorRatio2;

	double growthRateRS2=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate", 0.01);
	if(growthRateRS2<=0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.PROSAC.GrowthRate must be positive. Value=")+lexical_cast<string>(growthRateRS2)));
	else
	{
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.flagPROSAC=(growthRateRS2<=1);

		if(growthRateRS2<=1)
			parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.growthRate=growthRateRS2;
	}	//if(growthRateRS2<=0)

	unsigned int nSolution=tree.get<unsigned int>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution", 1);
	if(nSolution==0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.NoSolution must be positive. Value=")+lexical_cast<string>(nSolution)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxSolution=nSolution;

	double minCoverage=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage", 1);
	if(minCoverage<0 || minCoverage>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinCoverage must be in [0,1]. Value=")+lexical_cast<string>(minCoverage)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minCoverage=minCoverage;

	double minInclusion=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusion", 1);
	if(minInclusion<0 || minInclusion>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MinInclusuion must be in [0,1]. Value=")+lexical_cast<string>(minInclusion)));
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.minModelSimilarity=minInclusion;

	double maxAlgebraicDistance=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance", 1);
	if(maxAlgebraicDistance<0 || maxAlgebraicDistance>1)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.MaxAlgebraicDistance must be in [0,1]. Value=")+lexical_cast<string>(maxAlgebraicDistance)));
	else
		parameters.maxAlgebraicDistance=maxAlgebraicDistance;

	double parsimonyFactor=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor", 1.05);
	if(parsimonyFactor<1)
		throw(invalid_argument( string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.ParsimonyFactor must be >=1. Value: ")  + lexical_cast<string>(parsimonyFactor) ) );
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.parsimonyFactor=parsimonyFactor;

	double diversityFactor=tree.get<double>("GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor", 0.95);
	if(diversityFactor<0)
		throw(invalid_argument( string("GeometryEstimator22ImplementationC::SetParameters: GeometryEstimator.TSRANSAC.RANSACStage2.MORANSAC.DiversityFactor must be >=0. Value: ")  + lexical_cast<string>(diversityFactor) ) );
	else
		parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.diversityFactor=diversityFactor;

	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.eligibilityRatio=1;
	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters2.maxErrorRatio=1.05;

	parameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=parameters.eligibilityRatio;

	//GeometryEstimator.Optimisation
	unsigned int maxLSIteration=tree.get<unsigned int>("GeometryEstimator.Optimisation.MaxIteration", 100);
	parameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration=maxLSIteration;
	parameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.flagCovariance=false;

	//GeometryEstimator.Covariance
	parameters.pipelineParameters.flagCovariance=tree.get<bool>("GeometryEstimator.Covariance.Enable", true);

	double alpha=tree.get<double>("GeometryEstimator.Covariance.Alpha", -1);

	if(! (alpha<=0 || alpha>1) )
		parameters.pipelineParameters.sutParameters.alpha=alpha;

	//FeatureSet1
	parameters.featureFile1=tree.get<string>("FeatureSet1.FeatureFile","");
	parameters.cameraFile1=tree.get<string>("FeatureSet1.CameraFile","");
	parameters.flagCamera1=!parameters.cameraFile1.empty();

	optional<string> roi1=tree.get_optional<string>("FeatureSet1.RoI");
	char_separator<char> separator(" ");
	if(!roi1)
		parameters.flagAutoBoundingBox1=true;
	else
	{
		vector<ValueTypeM<Coordinate2DT>::type> tokens=Tokenise<ValueTypeM<Coordinate2DT>::type>(*roi1, string(" "));
		parameters.flagAutoBoundingBox1= (tokens.size()!=4);
		if(!parameters.flagAutoBoundingBox1)
			parameters.boundingBox1=FrameT(Coordinate2DT(tokens[0], tokens[1]), Coordinate2DT(tokens[2], tokens[3]));
	}	//if(!roi1)

	double spacing1=tree.get<double>("FeatureSet1.GridSpacing",7);
	if(spacing1<0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: FeatureSet1.Grid must be non-negative. Value=")+lexical_cast<string>(spacing1)));
	parameters.spacing1=spacing1;

	//FeatureSet2
	parameters.featureFile2=tree.get<string>("FeatureSet2.FeatureFile","");
	parameters.cameraFile2=tree.get<string>("FeatureSet2.CameraFile","");
	parameters.flagCamera2=!parameters.cameraFile2.empty();

    optional<string> roi2=tree.get_optional<string>("FeatureSet2.RoI");
    if(!roi2)
        parameters.flagAutoBoundingBox2=true;
    else
    {
    	vector<ValueTypeM<Coordinate2DT>::type> tokens=Tokenise<ValueTypeM<Coordinate2DT>::type>(*roi2, string(" "));
		parameters.flagAutoBoundingBox2= (tokens.size()!=4);
		if(!parameters.flagAutoBoundingBox2)
			parameters.boundingBox2=FrameT(Coordinate2DT(tokens[0], tokens[1]), Coordinate2DT(tokens[2], tokens[3]));
    }   //if(!roi2)

	double spacing2=tree.get<double>("FeatureSet2.GridSpacing",7);
	if(spacing2<0)
		throw(invalid_argument(string("GeometryEstimator22ImplementationC::SetParameters: FeatureSet2.Grid must be non-negative. Value=")+lexical_cast<string>(spacing2)));
	parameters.spacing2=spacing2;

	//Output
	parameters.outputPath=tree.get<string>("Output.Root","");

	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("GeometryEstimator22ImplementationC::SetParameters: Output path does not exist, or no write permission. Value:")+parameters.outputPath));

	parameters.modelFile=tree.get<string>("Output.Model","");
	parameters.covarianceFile=tree.get<string>("Output.Covariance","");
	parameters.correspondenceFile=tree.get<string>("Output.Correspondences","");
	parameters.logFile=tree.get<string>("Output.Log","");
	parameters.correspondingFeatureFile1=tree.get<string>("Output.FeatureList1","");
	parameters.correspondingFeatureFile2=tree.get<string>("Output.FeatureList2","");
	parameters.flagVerbose=tree.get<bool>("Output.Verbose",true);

	parameters.pipelineParameters.flagVerbose=parameters.flagVerbose;
	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the homography estimation pipeline
 * @param[out] model Estimated homography
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Features in the first image
 * @param[in] featureList2 Features in the second image
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator22ImplementationC::ComputeHomography(Homography2DT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<Homography2DT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2)
{
	unsigned int loGeneratorSize1= Homography2DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2= Homography2DPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACHomography2DProblemT::data_container_type dummyList;
	RANSACHomography2DProblemT ransacProblem1=MakeRANSACHomography2DProblem(dummyList, dummyList, parameters.noiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACHomography2DProblemT ransacProblem2=MakeRANSACHomography2DProblem(dummyList, dummyList, parameters.noiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLHomography2DProblemT optimisationProblem=MakePDLHomography2DProblem(Matrix3d::Zero(), dummyList, optional<MatrixXd>());

	Homography2DEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	Homography2DPipelineProblemT problem=MakeGeometryEstimationPipelineHomography2DProblem(featureList1, featureList2, geometryEstimationProblem, parameters.noiseVariance, parameters.pValue, initialEstimate, parameters.noiseVariance+parameters.initialEstimateError, parameters.pValue, parameters.nThreads);

	Homography2DPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return Homography2DPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeHomography(RANSACHomography2DProblemT::model_type& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const GeometryEstimationPipelineParametersC& parameters)

/**
 * @brief Runs the fundamental matrix estimation pipeline
 * @param[out] model Estimated fundamental matrix
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Features in the first image
 * @param[in] featureList2 Features in the second image
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator22ImplementationC::ComputeFundamentalMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2)
{
	unsigned int loGeneratorSize1=Fundamental7PipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSize2=Fundamental7PipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACFundamental7ProblemT::data_container_type dummyList;
	RANSACFundamental7ProblemT ransacProblem1=MakeRANSACFundamental7Problem(dummyList, dummyList, parameters.noiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize1, binSize1, binSize2);
	RANSACFundamental7ProblemT ransacProblem2=MakeRANSACFundamental7Problem(dummyList, dummyList, parameters.noiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSize2, binSize1, binSize2);
	PDLFundamentalProblemT optimisationProblem=MakePDLFundamentalProblem(Matrix3d::Zero(), dummyList, optional<MatrixXd>());

	Fundamental7EstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	Fundamental7PipelineProblemT problem=MakeGeometryEstimationPipelineFundamental7Problem(featureList1, featureList2, geometryEstimationProblem, parameters.noiseVariance, parameters.pValue, initialEstimate, parameters.noiseVariance+parameters.initialEstimateError, parameters.pValue, parameters.nThreads);

	Fundamental7PipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	GeometryEstimationPipelineDiagnosticsC diagnostics=Fundamental7PipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);

	return diagnostics;
}	//GeometryEstimationPipelineDiagnosticsC ComputeFundamentalMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const GeometryEstimator22ParametersC& parameters)

/**
 * @brief Runs the essential matrix estimation pipeline
 * @param[out] model Estimated essential matrix
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Normalised features in the first image
 * @param[in] featureList2 Normalised features in the second image
 * @param[in] mK1 Intrinsic calibration matrix for the first camera
 * @param[in] mK2 Intrinsic calibration matrix for the second camera
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator22ImplementationC::ComputeEssentialMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2)
{
	unsigned int loGeneratorSizeS1=Fundamental8PipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSizeS2=EssentialPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);
	double scale=(mK1(0,0)+mK2(0,0))*0.5;	//Scale factor for the inlier thresholds
	double effectiveNoiseVariance=parameters.noiseVariance/pow<2>(scale);
	double effectiveInitialEstimateError=parameters.initialEstimateError/pow<2>(scale);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACEssentialProblemT::data_container_type dummyList;
	RANSACFundamental8ProblemT ransacProblem1=MakeRANSACFundamental8Problem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSizeS1, binSize1, binSize2);
	RANSACEssentialProblemT ransacProblem2=MakeRANSACEssentialProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSizeS2, binSize1, binSize2);
	PDLEssentialProblemT optimisationProblem=MakePDLEssentialProblem(Matrix3d::Zero(), dummyList, optional<MatrixXd>());

	EssentialEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, initialEstimate);

	EssentialPipelineProblemT problem=MakeGeometryEstimationPipelineEssentialProblem(featureList1, featureList2, geometryEstimationProblem, effectiveNoiseVariance, parameters.pValue, initialEstimate, effectiveNoiseVariance+effectiveInitialEstimateError, parameters.pValue, parameters.nThreads);

	EssentialPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	return EssentialPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);
}	//GeometryEstimationPipelineDiagnosticsC ComputeEssentialMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const GeometryEstimator22ParametersC& parameters)

/**
 * @brief Runs the one-sided fundamental matrix estimation pipeline, with known first camera
 * @param[out] model Estimated essential matrix
 * @param[out] covariance Covariance. Invalid, if failed
 * @param[out] correspondences Inlier validation correspondences to the model
 * @param[in] initialEstimate Initial estimate. Invalid, if it does not exist
 * @param[in] featureList1 Normalised features in the first image
 * @param[in] featureList2 Normalised features in the second image
 * @param[in] mK1 Intrinsic calibration matrix for the first camera
 * @param[in] mK2 Normaliser matrix for the second camera (known calibration parameters and a plausible focal length estimate)
 * @return Diagnostics object
 */
GeometryEstimationPipelineDiagnosticsC GeometryEstimator22ImplementationC::ComputeOneSidedFundamentalMatrix(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const IntrinsicCalibrationMatrixT& mK1, const IntrinsicCalibrationMatrixT& mK2)
{
	unsigned int loGeneratorSizeS1=Fundamental8PipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio1);
	unsigned int loGeneratorSizeS2=OSFundamentalPipelineProblemT::GetMinimalGeneratorSize() * max(1.0, parameters.loGeneratorRatio2);
	double scale=(mK1(0,0)+mK2(0,0))*0.5;	//Scale factor for the inlier thresholds
	double effectiveNoiseVariance=parameters.noiseVariance/pow<2>(scale);
	double effectiveInitialEstimateError=parameters.initialEstimateError/pow<2>(scale);

	dimension_type binSize1;
	dimension_type binSize2;
	tie(binSize1, binSize2)=ComputeRANSACBinSize(featureList1, featureList2);

	RANSACOSFundamentalProblemT::data_container_type dummyList;
	RANSACFundamental8ProblemT ransacProblem1=MakeRANSACFundamental8Problem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSizeS1, binSize1, binSize2);
	RANSACOSFundamentalProblemT ransacProblem2=MakeRANSACOSFundamentalProblem(dummyList, dummyList, effectiveNoiseVariance, parameters.pValue, parameters.maxAlgebraicDistance, loGeneratorSizeS2, binSize1, binSize2);
	PDLOSFundamentalProblemT optimisationProblem=MakePDLOSFundamentalProblem(Matrix3d::Zero(), dummyList, optional<MatrixXd>());

	//If there is an initial estimate, normalise
	optional<EpipolarMatrixT> normalisedInitialEstimate;
	if(initialEstimate)
	{
		normalisedInitialEstimate = mK2.transpose() * (*initialEstimate);
		normalisedInitialEstimate->normalize();
	}

	OSFundamentalEstimatorProblemT geometryEstimationProblem(ransacProblem1, ransacProblem2, optimisationProblem, normalisedInitialEstimate);

	OSFundamentalPipelineProblemT problem=MakeGeometryEstimationPipelineOSFundamentalProblem(featureList1, featureList2, geometryEstimationProblem, effectiveNoiseVariance, parameters.pValue, normalisedInitialEstimate, effectiveNoiseVariance+effectiveInitialEstimateError, parameters.pValue, parameters.nThreads);

	OSFundamentalPipelineT::rng_type rng;
	if(parameters.seed>=0)
		rng.seed(parameters.seed);

	GeometryEstimationPipelineDiagnosticsC diagnostics=OSFundamentalPipelineT::Run(model, covariance, correspondences, problem, rng, parameters.pipelineParameters);

	//Denormalise the output
	if(diagnostics.flagSuccess)
	{
		model= (mK2.transpose()).inverse()*model;
		model.normalize();
	}
	return diagnostics;
}	//GeometryEstimationPipelineDiagnosticsC ComputeOneSidedFundamentalMatrix1(EpipolarMatrixT& model, optional<MatrixXd>& covariance, CorrespondenceListT& correspondences, const optional<EpipolarMatrixT>& initialEstimate, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2, const GeometryEstimator22ParametersC& parameters, const IntrinsicCalibrationMatrixT& mK1)

/**
 * @brief Saves the output
 * @param[in, out] featureList1 Feature list for the first image
 * @param[in, out] featureList2 Feature list for the second image
 * @param[in] model Estimated model
 * @param[in] covariance Model covariance. Invalid, if not estimated
 * @param[in] inliers Inlier indices
 * @param[in] diagnostics Geometry estimation pipeline diagnostics
 * @param[in] runtime Run time
 */
void GeometryEstimator22ImplementationC::SaveOutput(vector<FeatureT>& featureList1, vector<FeatureT>& featureList2, const Matrix3d& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const GeometryEstimationPipelineDiagnosticsC& diagnostics, double runtime) const
{
	//Model
	if(diagnostics.flagSuccess && !parameters.modelFile.empty())
		ArrayIOC<Matrix3d>::WriteArray(parameters.outputPath+parameters.modelFile, model);

	//Covariance
	if(diagnostics.flagSuccess && covariance && !parameters.covarianceFile.empty())
		ArrayIOC<MatrixXd>::WriteArray(parameters.outputPath+parameters.covarianceFile, *covariance);

	//Log
	if(!parameters.logFile.empty())
	{
	    string filename = parameters.outputPath+parameters.logFile;
	    ofstream logfile(filename);

	    string timeString= to_simple_string(second_clock::universal_time());
	    logfile<<"GeometryEstimator22 log file created on "<<timeString<<"\n";
	    logfile<<"Number of features in the first set: "<<featureList1.size()<<"\n";
	    logfile<<"Number of features in the second set: "<<featureList2.size()<<"\n";
	    logfile<<"Number of correspondences:"<<inliers.size()<<"\n";

	    if(diagnostics.flagSuccess)
	    {
		    logfile<<"Number of guided matching iterations: "<<diagnostics.nIteration<<"\n";
	    	logfile<<"Error: "<<diagnostics.error<<"\n";
	    }	//if(diagnostics.flagSuccess)

	    logfile<<"Run time: "<<runtime<<"s";
	}	//if(!parameters.logFile.empty())

	//Correspondences
	if(!parameters.correspondenceFile.empty())
	{
		vector<CoordinateT> coordinates1=MakeCoordinateVector(featureList1);
		vector<CoordinateT> coordinates2=MakeCoordinateVector(featureList2);
	    CoordinateCorrespondenceList2DT coordinateCorrespondences=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(inliers, coordinates1, coordinates2);
	    CoordinateCorrespondenceIOC::WriteCorrespondenceFile(coordinateCorrespondences, parameters.outputPath+parameters.correspondenceFile);
	}	//if(!parameters.correspondenceFile.empty())

	if(!parameters.correspondingFeatureFile1.empty() || !parameters.correspondingFeatureFile2.empty())
	{
		MakeFeatureCorrespondenceList(featureList1, featureList2, inliers);

		if(!parameters.correspondingFeatureFile1.empty())
    		ImageFeatureIOC::WriteFeatureFile(featureList1, parameters.outputPath+parameters.correspondingFeatureFile1);

		if(!parameters.correspondingFeatureFile2.empty())
			ImageFeatureIOC::WriteFeatureFile(featureList2, parameters.outputPath+parameters.correspondingFeatureFile2);
	}	//	if(!parameters.correspondingFeatureFile1.empty() || !parameters.correspondingFeatureFile2.empty())
}	//void SaveOutput(const Matrix3d& model, const optional<MatrixXd>& covariance, const CorrespondenceListT& inliers, const vector<FeatureT>& featureList1, const vector<FeatureT>& featureList2) const;

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool GeometryEstimator22ImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the cameras
	optional<CameraC> camera1;
	if(parameters.flagCamera1)
		camera1=LoadCamera(parameters.cameraFile1);

	optional<CameraC> camera2;
	if(parameters.flagCamera2)
		camera2=LoadCamera(parameters.cameraFile2);

	//Process the cameras
	optional<Matrix3d> initialEstimate;
	optional<IntrinsicCalibrationMatrixT> mK1;
	optional<IntrinsicCalibrationMatrixT> mK2;
	LensDistortionC distortion1;
	LensDistortionC distortion2;
	std::tie(mK1, distortion1, mK2, distortion2, initialEstimate)=ProcessCameras(camera1, camera2);

	//Load the initial estimate
	if(parameters.flagInitialEstimate)
		initialEstimate=ArrayIOC<Matrix3d>::ReadArray(parameters.initialEstimateFile, 3, 3);

	if(parameters.flagVerbose)
	{
		map<Geometry22ProblemT, string> codeToProblem{ {Geometry22ProblemT::HMG2, "Homography"}, {Geometry22ProblemT::FND, "Fundamental"}, {Geometry22ProblemT::OSFND, "OSFundamental"}, {Geometry22ProblemT::ESS, "Essential"}};
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Problem type: "<<codeToProblem[parameters.problemType]<<"\n";
		cout<<"              Initial estimate: "<<(initialEstimate ? (parameters.flagInitialEstimate ? "User-specified" : "Derived")  :"None")<<"\n";
		cout<<"              Intrinsic calibration (1/2): "<< (mK1 ? "1" : "0")<<"/"<<(mK2 ? "1" : "0")<<"\n";
		cout<<"              Lens distortion (1/2): "<< (distortion1.modelCode!=LensDistortionCodeT::NOD)<<"/"<< (distortion2.modelCode!=LensDistortionCodeT::NOD)<<"\n";	//Does not compile without the pointer comparison
	}	//if(parameters.flagVerbose)

	//Load the feature files
    vector<FeatureT> featureList1;
	vector<FeatureT> featureList2;

	bool flagNormaliseSet=(parameters.problemType==Geometry22ProblemT::ESS || parameters.problemType==Geometry22ProblemT::OSFND);

#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)
{
	#pragma omp section
	{
		featureList1=LoadFeatures(distortion1, mK1, 1, flagNormaliseSet);
		if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Set 1 has "<<featureList1.size()<<" features \n";
	}

	#pragma omp section
    {
    	featureList2=LoadFeatures(distortion2, mK2, 2, flagNormaliseSet);
    	if(parameters.flagVerbose)
			cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Set 2 has "<<featureList2.size()<<" features. Peforming guided matching...\n";
    }	//#pragma omp section
}	//#pragma omp parallel sections if(parameters.flagMultithreaded)

    if(parameters.flagVerbose && (featureList1.empty() || featureList2.empty()))
    {
        cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Empty feature set encountered. Terminating...\n";
        return true;
    }	//if(parameters.flagVerbose && (featureList1.empty() || featureList2.empty()))

    Matrix3d model;
    optional<MatrixXd> covariance;
    CorrespondenceListT correspondences;
    GeometryEstimationPipelineDiagnosticsC diagnostics;

    //Homography estimator
    if(parameters.problemType==Geometry22ProblemT::HMG2)
    	diagnostics=ComputeHomography(model, covariance, correspondences, initialEstimate, featureList1, featureList2);

    //Fundamental matrix estimator
    if(parameters.problemType==Geometry22ProblemT::FND)
    	diagnostics=ComputeFundamentalMatrix(model, covariance, correspondences, initialEstimate, featureList1, featureList2);

    //Essential matrix estimator
    if(parameters.problemType==Geometry22ProblemT::ESS)
    {
    	if(!mK1)
    		throw(runtime_error("GeometryEstimator22ImplementationC::Run: Missing intrinsic calibration parameters for the first camera."));

    	if(!mK2)
			throw(runtime_error("GeometryEstimator22ImplementationC::Run: Missing intrinsic calibration parameters for the second camera."));

    	diagnostics=ComputeEssentialMatrix(model, covariance, correspondences, initialEstimate, featureList1, featureList2, *mK1, *mK2);
    }	//if(parameters.problemType==Geometry22ProblemT::ESS)

    if(parameters.problemType==Geometry22ProblemT::OSFND)
    {
    	if(!mK1)
    		throw(runtime_error("GeometryEstimator22ImplementationC::Run: Missing intrinsic calibration parameters for the first camera."));

    	if(!mK2)
			throw(runtime_error("GeometryEstimator22ImplementationC::Run: Missing principal point for the second camera."));

    	diagnostics=ComputeOneSidedFundamentalMatrix(model, covariance, correspondences, initialEstimate, featureList1, featureList2, *mK1, *mK2);
    }	//if(parameters.problemType=Geometry22ProblemT::OSFND)

	if(parameters.flagVerbose)
	{
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s:";

		if(!diagnostics.flagSuccess)
			cout<<" Geometry estimator failed!\n";
		else
		{
			cout<<"#Correspondences: "<<correspondences.size()<<" Error:"<<diagnostics.error<<"\n";
			cout<<" Result:\n"<<model<<"\n";
		}	//if(!diagnostics.flagSuccess)

	}	//if(parameters.flagVerbose)

    //Output
    double runtime=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

    if(!(parameters.correspondenceFile.empty() && parameters.correspondingFeatureFile1.empty() && parameters.correspondingFeatureFile2.empty()))
    {
    	bool flagDenormalise=flagNormaliseSet;

	#pragma omp parallel sections if(parameters.nThreads>1) num_threads(parameters.nThreads)
	{
		#pragma omp section
			PostprocessFeatures(featureList1, distortion1, mK1, flagDenormalise);

		#pragma omp section
			PostprocessFeatures(featureList2, distortion2, mK2, flagDenormalise);
	}
    }	//if(!(parameters.correspondenceFile.empty() && parameters.correspondingFeatureFile1.empty() && parameters.correspondingFeatureFile2.empty()))

    SaveOutput(featureList1, featureList2, model, covariance, correspondences, diagnostics, runtime);

	return true;
}	//bool Run()

}	//AppGeometryEstimator22N
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("131101");
    std::string header("geometryEstimator22: 2D pairwise geometry estimation");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppGeometryEstimator22N::GeometryEstimator22ImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )

