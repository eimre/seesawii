/**
 * @file AppSparseUnitSphereBuilder.cpp Implementation of sparseUnitSphereBuilder
 * @author Evren Imre
 * @date 25 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <omp.h>
#include <functional>
#include "Application.h"
#include "../GeometryEstimationPipeline/SparseUnitSphereReconstructionPipeline.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Elements/Camera.h"
#include "../Elements/Coordinate.h"
#include "../IO/CameraIO.h"
#include "../IO/SceneFeatureIO.h"
#include "../IO/ArrayIO.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Metrics/Similarity.h"
#include "../ApplicationInterface/InterfaceSparseUnitSphereReconstruction.h"

namespace SeeSawN
{
namespace AppSparseUnitSphereBuilderN
{

using namespace ApplicationN;

using boost::format;
using boost::str;
using boost::property_tree::ptree;
using boost::property_tree::write_xml;
using boost::optional;
using Eigen::RowVector3d;
using std::greater;
using std::greater_equal;
using std::bind;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineC;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineParametersC;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineDiagnosticsC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ElementsN::LensDistortionCodeT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::BinaryImageFeatureIOC;
using SeeSawN::ION::BinaryImageFeatureC;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::MetricsN::InverseHammingBIDConverterT;

/**
 * @brief Parameters for sparseUnitSphereBuilder
 * @ingroup Application
 */
struct AppSparseUnitSphereBuilderParametersC
{
	//General
	unsigned int nThreads;	///< Number of threads available to the application

	//Problem
	SparseUnitSphereReconstructionPipelineParametersC pipelineParameters;	///< Parameters for the reconstruction engine

	//Input
	string featureFilePattern;	///< Filename pattern for the feature files
	string cameraFile;	///< Camera filename
	double gridSpacing;	///< Minimum distance two neighbouring image features

	//Output
	string outputPath;	///< Output path
	string pointCloudFile;	///< Point cloud
	string sceneFeatureFile;	///< Scene features
	string logFile;	///< Filename for the log file
	bool flagVerbose;   ///< Verbosity flag

	AppSparseUnitSphereBuilderParametersC() : nThreads(1), gridSpacing(0), flagVerbose(false)
	{
	    pipelineParameters.matcherParameters.matcherParameters.nnParameters.similarityTestThreshold=0;
	}

};	//AppSparseUnitSphereBuilderParametersC

/**
 * @brief Implementation of sparseUnitSphereBuilder
 * @ingroup Application
 * @nosubgrouping
 */
class AppSparseUnitSphereBuilderImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		AppSparseUnitSphereBuilderParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string, double> logger;	///< Logger
		//@}

		/** @name Implementation details */ //@{
		static ptree PruneSUSPipeline(const ptree& src);	///< Removes the redundant sparse unit sphere reconstruction parameters
        static ptree InjectDefaultValues(const ptree& src, const AppSparseUnitSphereBuilderParametersC& defaultParameters);  ///< Injects the application-specific default parameters into the parameter tree

		vector<vector<BinaryImageFeatureC>> LoadFeatures(const vector<CameraC>& cameraList);	///< Loads the features
		void SaveOutput(const vector<Coordinate3DT>& pointCloud, const vector<OrientedBinarySceneFeatureC>& sceneFeatures);	///< Saves the output
		void SaveLog(const SparseUnitSphereReconstructionPipelineDiagnosticsC& diagnostics);	///< Saves the log file
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppSparseUnitSphereBuilderImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& treeO);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}

};	//AppSparseUnitSphereBuilderImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Saves the output
 * @param[in] pointCloud Point cloud
 * @param[in] sceneFeatures Scene features
 */
void AppSparseUnitSphereBuilderImplementationC::SaveOutput(const vector<Coordinate3DT>& pointCloud, const vector<OrientedBinarySceneFeatureC>& sceneFeatures)
{
	//Point cloud
	if(!parameters.pointCloudFile.empty())
	{
		string filename=parameters.outputPath+"/"+parameters.pointCloudFile;
		ofstream modelFile(filename);

		if(modelFile.fail())
			throw(runtime_error(string("AppSparseUnitSphereBuilderImplementationC::SaveOutput : Failed to open the file for writing at ")+filename));

		typedef SeeSawN::ION::ArrayIOC<RowVector3d> ArrayIOT;

		for(const auto& current : pointCloud)
			ArrayIOT::WriteArray(modelFile, current.transpose());
	}	//if(!parameters.pointCloudFile.empty())

	//Scene features
	if(!parameters.sceneFeatureFile.empty())
	{
		string filename=parameters.outputPath+"/"+parameters.sceneFeatureFile;
		OrientedBinarySceneFeatureIOC::WriteFeatureFile(sceneFeatures, filename);
	}	//if(!parameters.sceneFeatureFile.empty())

}	//void SaveOutput(const vector<Coordinate3DT>& pointCloud, const vector<OrientedBinarySceneFeatureC>& sceneFeatures)

/**
 * @brief Saves the log file
 * @param[in] diagnostics Diagnostics
 */
void AppSparseUnitSphereBuilderImplementationC::SaveLog(const SparseUnitSphereReconstructionPipelineDiagnosticsC& diagnostics)
{
	if(parameters.logFile.empty())
		return;

	string filename=parameters.outputPath+"/"+parameters.logFile;
	ofstream logFile(filename);

	if(logFile.fail())
		throw(runtime_error(string("AppSparseUnitSphereBuilderImplementationC::SaveLog : Failed to open the file for writing at ")+filename));

	string timeString= to_simple_string(second_clock::universal_time());
	logFile<<"sparseUnitSphereBuilder log file created on "<<timeString<<"\n";

	logFile<<"Number of camera pairs: "<<diagnostics.matcherDiagnostics.nSourcePairs<<"\n";
	logFile<<"Number of observed correspondences: "<<diagnostics.matcherDiagnostics.nObserved<<"\n";
	logFile<<"Number of inferred correspondences: "<<diagnostics.matcherDiagnostics.nImplied<<"\n";
	logFile<<"Number of points on the unit sphere: "<<diagnostics.panoramaDiagnostics.nPoints<<"\n";
	logFile<<"Execution time: "<<logger["ExecutionTime"]<<"s\n";
}	//void SaveLog(const SparseUnitSphereReconstructionPipelineDiagnosticsC& diagnostics)

/**
 * @brief Removes the redundant sparse unit sphere reconstruction parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree AppSparseUnitSphereBuilderImplementationC::PruneSUSPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("FeatureMatching.Main.NoThreads"))
		output.get_child("FeatureMatching.Main").erase("NoThreads");

    if(src.get_child_optional("FeatureMatching.Main.FlagVerbose"))
        output.get_child("FeatureMatching.Main").erase("FlagVerbose");

	return output;
}	//ptree PrunePfMPipeline(const ptree& src)

/**
 * @brief Injects the application-specific default parameters into the parameter tree
 * @param[in] src Parameter tree
 * @param[in] defaultParameters Default parameters
 * @return Updated tree
 */
ptree AppSparseUnitSphereBuilderImplementationC::InjectDefaultValues(const ptree& src, const AppSparseUnitSphereBuilderParametersC& defaultParameters)
{
    ptree output(src);

    if(!src.get_child_optional("Pipeline.FeatureMatching.Matcher.kNN.MinSimilarity"))
      output.put("Pipeline.FeatureMatching.Matcher.kNN.MinSimilarity", defaultParameters.pipelineParameters.matcherParameters.matcherParameters.nnParameters.similarityTestThreshold );

    return output;
}

/**
 * @brief Loads the features
 * @param[in] cameraList Cameras
 * @return A vector of image feature vectors
 * @remarks The features are undistorted, but not normalised
 */
vector<vector<BinaryImageFeatureC>> AppSparseUnitSphereBuilderImplementationC::LoadFeatures(const vector<CameraC>& cameraList)
{
	size_t nCamera=cameraList.size();
	vector<vector<BinaryImageFeatureC> > output(nCamera);

	size_t c=0;
	LensDistortionC noDistortion; noDistortion.modelCode=LensDistortionCodeT::NOD;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) num_threads(parameters.nThreads)
	for(c=0; c<nCamera; ++c)
	{
		string featureFile=str(format(parameters.featureFilePattern)%c);
		vector<BinaryImageFeatureC> featureList=BinaryImageFeatureIOC::ReadFeatureFile(featureFile, true);

		//Lens distortion
		LensDistortionC distortion = cameraList[c].Distortion() ? *cameraList[c].Distortion() : noDistortion;

		optional< tuple<Coordinate2DT, Coordinate2DT> > boundingBox;
		optional<CoordinateTransformations2DT::affine_transform_type> normaliser;
		BinaryImageFeatureIOC::PreprocessFeatureSet(featureList, boundingBox, parameters.gridSpacing, true, distortion, normaliser);

	#pragma omp critical (AMSfM_LF)
		{
			cout<<"Loaded "<<featureList.size()<<" features from Set "<<c<<"\n";
			output[c].swap(featureList);
		}
	}	//for(; c<nCamera; ++c)

	return output;
}	//vector<vector<ImageFeatureC>> LoadFeatures(const vector<CameraC>& cameraList);

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppSparseUnitSphereBuilderImplementationC::AppSparseUnitSphereBuilderImplementationC()
{
	AppSparseUnitSphereBuilderParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the application. >0" )

	("Input.FeatureFilename", value<string>(), "Feature filename, with a single printf-like token (e.g. feature%02d.bft2)")
	("Input.GridSpacing", value<string>()->default_value(lexical_cast<string>(defaultParameters.gridSpacing)), "Minimum distance between two image features, in pixels. >=0")
	("Input.CameraFile", value<string>(), "Camera filename")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.PointCloud", value<string>(), "Point cloud file")
	("Output.SceneFeatures", value<string>(), "Scene features")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output");
}	//AppSparseUnitSphereBuilderImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppSparseUnitSphereBuilderImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppSparseUnitSphereBuilderImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppSparseUnitSphereBuilderParametersC defaultParameters;

	tree.put("xmlcomment", "sparseUnitSphereBuilder configuration file");

	tree.put("General","");
	tree.put("Pipeline","");
	tree.put("Input","");
	tree.put("Output","");

	tree.put("General.NoThreads", defaultParameters.nThreads);

	tree.put("Input","");
	tree.put("Input.FeatureFilename","%03d.bft2");
	tree.put("Input.CameraFile","cameras.cam");

	tree.put("Output.Root", "/home/");
	tree.put("Output.PointCloud","PointCloud.asc");
	tree.put("Output.SceneFeatures","SceneFeatures.bft3");
	tree.put("Output.Log","log.log");
	tree.put("Output.Verbose", defaultParameters.flagVerbose);

	tree.put_child("Pipeline", PruneSUSPipeline(InterfaceSparseUnitSphereReconstructionC::MakeParameterTree(defaultParameters.pipelineParameters, detailLevel)));

	if(detailLevel>1)
	    tree.put("Input.GridSpacing",defaultParameters.gridSpacing);

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] treeO Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppSparseUnitSphereBuilderImplementationC::SetParameters(const ptree& treeO)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);

	AppSparseUnitSphereBuilderParametersC defaultParameters;
    ptree lTree=InjectDefaultValues(treeO, defaultParameters);

	//General
	parameters.nThreads=ReadParameter(lTree, "General.NoThreads", gt0, "is not >0", optional<double>(defaultParameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	//Pipeline
	parameters.pipelineParameters=InterfaceSparseUnitSphereReconstructionC::MakeParameterObject(PruneSUSPipeline(lTree.get_child("Pipeline")));
	parameters.pipelineParameters.matcherParameters.nThreads=parameters.nThreads;

	//Input
	parameters.featureFilePattern=lTree.get<string>("Input.FeatureFilename");
	parameters.gridSpacing=ReadParameter(lTree, "Input.GridSpacing", geq0, "is not >=0", optional<double>(defaultParameters.gridSpacing));
	parameters.cameraFile=lTree.get<string>("Input.CameraFile");

	//Output
	parameters.outputPath=lTree.get<string>("Output.Root");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppSparseUnitSphereBuilderImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.pointCloudFile=lTree.get<string>("Output.PointCloud","");
	parameters.sceneFeatureFile=lTree.get<string>("Output.SceneFeatures","");
	parameters.logFile=lTree.get<string>("Output.Log","");
	parameters.flagVerbose=lTree.get<bool>("Output.Verbose", defaultParameters.flagVerbose);

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppSparseUnitSphereBuilderImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Load the cameras
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.cameraFile);
	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<cameraList.size()<<" cameras.\n";

	//Load the feature files
	vector<vector<BinaryImageFeatureC> > featureStack=LoadFeatures(cameraList);
	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Loaded the image features. Running the sparse unit sphere reconstruction pipeline...\n";

	//Pipeline

	vector<CameraMatrixT> cameras; cameras.reserve(cameraList.size());
	for(const auto& current : cameraList)
		cameras.push_back(*current.MakeCameraMatrix());

	InverseHammingBIDConverterT similarityMetric;

	vector<Coordinate3DT> pointCloud;
	vector<OrientedBinarySceneFeatureC> sceneFeatures;
	SparseUnitSphereReconstructionPipelineDiagnosticsC diagnostics=SparseUnitSphereReconstructionPipelineC::Run(pointCloud, sceneFeatures, cameras, featureStack, similarityMetric, parameters.pipelineParameters);

	logger["ExecutionTime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reconstruction finished \n";

	//Output
	SaveOutput(pointCloud, sceneFeatures);
	SaveLog(diagnostics);

	return true;
}	//bool Run()

}	//AppSparseUnitSphereBuilderN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("160725");
    std::string header("sparseUnitSphereBuilder: Sparse unit sphere reconstruction from 2D images acquired by known cameras, with binary image features");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppSparseUnitSphereBuilderN::AppSparseUnitSphereBuilderImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )

