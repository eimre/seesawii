/**
 * @file AppMulticameraSynchronisation.cpp Implementation of \c multicameraSynchronisation
 * @author Evren Imre
 * @date 11 Dec 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/range/algorithm.hpp>
#include <omp.h>
#include <functional>
#include <fstream>
#include "Application.h"
#include "../Synchronisation/MulticameraSynchronisation.h"
#include "../Synchronisation/RelativeSynchronisation.h"
#include "../StateEstimation/ViterbiRelativeSynchronisationProblem.h"
#include "../ApplicationInterface/InterfaceMulticameraSynchronisation.h"
#include "../ApplicationInterface/InterfaceUtility.h"
#include "../Wrappers/BoostFilesystem.h"
#include "../Elements/Camera.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Feature.h"
#include "../IO/ImageFeatureTrajectoryIO.h"
#include "../IO/CameraIO.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace AppMulticameraSynchronisationN
{

using namespace ApplicationN;
using boost::count_if;
using boost::accumulate;
using std::greater;
using std::bind;
using SeeSawN::SynchronisationN::MulticameraSynchonisationParametersC;
using SeeSawN::SynchronisationN::MulticameraSynchonisationDiagnosticsC;
using SeeSawN::SynchronisationN::MulticameraSynchonisationC;
using SeeSawN::SynchronisationN::RelativeSynchronisationC;
using SeeSawN::StateEstimationN::ViterbiRelativeSynchronisationProblemC;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::ElementsN::ImageFeatureTrajectoryT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::LensDistortionC;
using SeeSawN::ION::ImageFeatureTrajectoryIOC;
using SeeSawN::ION::CameraIOC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;

/**
 * @brief Parameters for \c multicameraSynchronisation
 * @ingroup Application
 */
struct AppMulticameraSynchronisationParametersC
{
	unsigned int nThreads;	///< Number of threads available to the application

	MulticameraSynchonisationParametersC synchronisationParameters;	///< Parameters for the synchronisation engine
	double minActivity;	///< The minimum feature count for a frame with a significant level of activity, relative to the frame with the second largest feature count. Per sequence. [0,1]

	//Input
	string inputFilename;	///< XML file pointing to the input, and containing the parameters
	vector<string> trackFiles;	///< List of track files
	vector<string> cameraFiles;	///< List of camera files
	map<unsigned int, MulticameraSynchonisationC::initial_estimate_type> initialEstimates;	///< Initial estimates

	//Output
	string outputPath;	///< Output path
	string logFile;	///< Filename for the log file
	string resultFile;	///< Filename for the result
	bool flagVerbose;   ///< Verbosity flag

	AppMulticameraSynchronisationParametersC() : nThreads(1), minActivity(0.05), flagVerbose(true)
	{}
};	//struct AppMulticameraSynchronisationParametersC

/**
 * @brief Implementation of \c multicameraSynchronisation
 * @ingroup Application
 */
class AppMulticameraSynchronisationImplementationC
{
	private:

		/** @name Configuration */ //@{
		auto_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed
		AppMulticameraSynchronisationParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string,double> logger;	///< Logs various values
		//@}

		typedef MulticameraSynchonisationC::initial_estimate_type InitialEstimateT;	///< Initial estimate type
		typedef RelativeSynchronisationC::image_sequence_type ImageSequenceT;	///< An image sequence
		typedef MulticameraSynchonisationC::result_type ResultT;	///< A synchronisation result

		/** @name Implementation details */ //@{
		static ptree PruneParameterTree(const ptree& src);	///< Removes the redundant parameters from a parameter tree
		void ReadInputDescriptor();	///< Reads an input descriptor tree
		vector<ImageSequenceT> LoadInput();	///< Loads the input data

		void SaveLog(const MulticameraSynchonisationDiagnosticsC& diagnostics, const ResultT& synchronisation);	///< Saves the log
		void SaveOutput(const ResultT& synchronisation);	///< Saves the output
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppMulticameraSynchronisationImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& tree);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//class AppMulticameraSynchronisationImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Removes the redundant parameters from the tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree AppMulticameraSynchronisationImplementationC::PruneParameterTree(const ptree& src)
{
	ptree output(src);

    if(src.get_child_optional("Synchronisation.Main.NoThreads"))
    	output.get_child("Synchronisation.Main").erase("NoThreads");

    if(src.get_child_optional("Synchronisation.Main.FlagVerbose"))
    	output.get_child("Synchronisation.Main").erase("FlagVerbose");

    return output;
}	//void PruneParameterTree(ptree& parameters)

/**
 * @brief Reads an input descriptor tree
 */
void AppMulticameraSynchronisationImplementationC::ReadInputDescriptor()
{
	auto gt0=std::bind(greater<double>(),std::placeholders::_1,0);
	auto noConstraint=[](double){return true;};

	ptree src;
	read_xml(parameters.inputFilename, src, boost::property_tree::xml_parser::no_comments | boost::property_tree::xml_parser::trim_whitespace);

	unsigned int nSequence=src.count("Sequence");
	parameters.trackFiles.reserve(nSequence);
	parameters.cameraFiles.reserve(nSequence);

	constexpr size_t iAlphaRange=MulticameraSynchonisationC::iAlphaRange;
	constexpr size_t iTauRange=MulticameraSynchonisationC::iTauRange;
	constexpr size_t iAdmissibleAlpha=MulticameraSynchonisationC::iAdmissibleAlpha;

	size_t c=0;
	for(const auto& current : src.get_child(""))
	{
		if(current.first!="Sequence")
			continue;

		parameters.trackFiles.push_back(current.second.get<string>("TrackFile"));
		parameters.cameraFiles.push_back(current.second.get<string>("CameraFile"));

		//Initial estimates
		if(current.second.get_child_optional("InitialEstimate"))
		{
			InitialEstimateT initialEstimate;
			if(current.second.get_child_optional("InitialEstimate.AlphaRange"))
			{
				vector<double> alphaRange=ReadParameterArray<double>(current.second, "InitialEstimate.AlphaRange", 2, gt0, "is not >0", string(" "));
				get<iAlphaRange>(initialEstimate)=ViterbiRelativeSynchronisationProblemC::interval_type(alphaRange[0], alphaRange[1]);
			}	//if(current.second.get_child_optional("InitialEstimate.AlphaRange"))

			if(current.second.get_child_optional("InitialEstimate.TauRange"))
			{
				vector<double> tauRange=ReadParameterArray<double>(current.second, "InitialEstimate.TauRange", 2, noConstraint, " ", string(" "));
				get<iTauRange>(initialEstimate)=ViterbiRelativeSynchronisationProblemC::interval_type(tauRange[0], tauRange[1]);
			}	//if(current.second.get_child_optional("InitialEstimate.AlphaRange"))

			if(current.second.get_child_optional("InitialEstimate.AdmissibleAlpha"))
				get<iAdmissibleAlpha>(initialEstimate)=Tokenise<double>(current.second.get<string>("InitialEstimate.AdmissibleAlpha"), string(" "));

			if(get<iAlphaRange>(initialEstimate) && get<iTauRange>(initialEstimate) && get<iAdmissibleAlpha>(initialEstimate))
				parameters.initialEstimates.emplace(c, initialEstimate);
		}	//if(current.second.get_child_optional("InitialEstimate"))

		++c;
	}	//for(const auto& current : src.get_child(""))

}	//void ReadInputDescriptor(const ptree& src)

/**
 * @brief Loads the input data
 * @return Returns a list of image sequences
 */
auto AppMulticameraSynchronisationImplementationC::LoadInput() -> vector<ImageSequenceT>
{
	size_t nSequences=parameters.cameraFiles.size();

	//Load the cameras
	vector< vector<CameraMatrixT> > cameraStack(nSequences);
	vector<LensDistortionC> lensDistortionList(nSequences);
	size_t indexSequence=0;
	for(const auto& current : parameters.cameraFiles)
	{
		vector<CameraC> cameraList=CameraIOC::ReadCameraFile(current);
		optional<LensDistortionC> distortion;

		vector<CameraMatrixT> mPList; mPList.reserve(cameraList.size());
		for(const auto& currentCamera : cameraList)
		{
			optional<CameraMatrixT> mP=currentCamera.MakeCameraMatrix();
			if(!mP)
				throw(invalid_argument(string("AppMulticameraSynchronisationImplementationC::LoadInput : Invalid camera matrix. Sequence:")+lexical_cast<string>(indexSequence)+string(" Frame:")+lexical_cast<string>(mPList.size())));

			mPList.push_back(*mP);

			if(!distortion)
				distortion=currentCamera.Distortion();
		}	//for(const auto& currentCamera : cameraList)

		cameraStack[indexSequence].swap(mPList);
		lensDistortionList[indexSequence] = (distortion ? *distortion : LensDistortionC());	//The default object is no distortion

		++indexSequence;
	}	//for(const auto& current : parameters.cameraFiles)

	//Load the trajectories, and build the output
	vector<ImageSequenceT> output(nSequences);
	size_t c=0;
#pragma omp parallel for if(parameters.nThreads>1) schedule(static) private(c) num_threads(parameters.nThreads)
	for(c=0; c<nSequences; ++c)
	{
		vector<ImageFeatureTrajectoryT> trajectoryList;
	#pragma omp critical(AMS_LI1)
		trajectoryList=ImageFeatureTrajectoryIOC<>::ReadTrajectoryFile(parameters.trackFiles[c]);	//Load
		optional<CoordinateTransformations2DT::projective_transform_type> dummy;
		ImageFeatureTrajectoryIOC<>::PreprocessMeasurements(trajectoryList, true, lensDistortionList[c], dummy);	//Apply preprocessing to the features

		//Convert to an image sequence
		ImageSequenceT imageSequence=RelativeSynchronisationC::MakeImageSequence(trajectoryList, cameraStack[c], parameters.minActivity);

	#pragma omp critical(AMS_LI2)
		output[c].swap(imageSequence);
	}	//for(c=0; c<nSequences; ++c)

	return output;
}	//vector<ImageSequenceT> LoadInput();

/**
 * @brief Saves the log
 * @param[in] diagnostics Diagnostics
 * @param[in] synchronisation Result
 */
void AppMulticameraSynchronisationImplementationC::SaveLog(const MulticameraSynchonisationDiagnosticsC& diagnostics, const ResultT& synchronisation)
{
	if(parameters.logFile.empty())
		return;

	ofstream logfile(parameters.outputPath+parameters.logFile);
	if(logfile.fail())
		throw(runtime_error(string("AppMulticameraSynchronisationImplementationC::SaveOutput: Cannot open file ")+parameters.outputPath+parameters.logFile));

	string timeString= to_simple_string(second_clock::universal_time());
	logfile<<"MulticameraSynchronisation log file created on "<<timeString<<"\n";

	logfile<<"No. sequences: "<<parameters.cameraFiles.size()<<"\n";

	size_t nSuccessful=count_if(synchronisation, [](const ResultT::value_type& val){return !!val;});
	logfile<<"No. successfully synchronised sequences: "<<nSuccessful<<"\n";

	if(diagnostics.flagSuccess)
	{
		size_t nDrops=accumulate(synchronisation, 0, [](const size_t& acc, const ResultT::value_type& val){return acc + (val ? get<MulticameraSynchonisationC::iDropList>(*val).size() : 0); } );

		logfile<<"No. frame drop events: "<<nDrops<<"\n";
		logfile<<"Score: "<<diagnostics.score<<"\n";
	}

	logfile<<"Time elapsed: "<<logger["TimeElapsed"]<<" s";
}	//void SaveLog()

/**
 * @brief Saves the output
 * @param[in] synchronisation Result
 */
void AppMulticameraSynchronisationImplementationC::SaveOutput(const ResultT& synchronisation)
{

	if(parameters.resultFile.empty())
		return;

	ptree outputTree;

	size_t c=0;
	for(const auto& current : synchronisation)
	{
		if(!current)
			continue;

		ptree& node=outputTree.add("Camera","");
		node.put("Id", c);
		node.put("FrameRate", get<MulticameraSynchonisationC::iFrameRate>(*current));
		node.put("TemporalOffset", get<MulticameraSynchonisationC::iTemporalOffset>(*current));
		node.put("FrameDrops", "");
		for(const auto& currentDrop : get<MulticameraSynchonisationC::iDropList>(*current))
		{
			node.put("FrameDrops.Begin", get<RelativeSynchronisationC::iInterval>(currentDrop).lower());
			node.put("FrameDrops.End", get<RelativeSynchronisationC::iInterval>(currentDrop).upper());
			node.put("FrameDrops.Loss", get<RelativeSynchronisationC::iDropped>(currentDrop));
		}	//for(const auto& currentDrop : get<MulticameraSynchonisationC::iDropList>(*current))
		++c;
	}	//for(const auto& current : synchronisation)

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(parameters.outputPath+parameters.resultFile, outputTree, locale(), settings);
}	//void SaveOutput(const ResultT& synchronisation)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppMulticameraSynchronisationImplementationC::AppMulticameraSynchronisationImplementationC()
{
	AppMulticameraSynchronisationParametersC defaultParameters;
	defaultParameters.synchronisationParameters.maxAttitudeDifference=0.7;

	commandLineOptions.reset(new(options_description)("Application command line options"));
	commandLineOptions->add_options()
	("General.NoThreads", value<string>()->default_value(lexical_cast<string>(defaultParameters.nThreads)), "Number of threads available to the program. If 0, set to 1. If <0 or >#Cores, set to #Cores")

	("Synchronisation.Fusion.Seed", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.seed)), "Seed for the random number generator. If <0, rng default")
	("Synchronisation.Fusion.NoIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.nHypothesis)), "Number of absolute synchronisation hypotheses to be instantiated. >=0")
	("Synchronisation.Fusion.MeasurementInlierThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.measurementInlierTh)), "A pairwise synchronisation measurement is an inlier to an absolute synchronisation hypothesis, if the percentage of the support set that is compatible with the prediction is above this threshold [0,1]")
	("Synchronisation.Fusion.AlphaTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.alphaTolerance)), "Effective minimum tolerance (range/2) of alpha for absolute synchronisation. Prevents the rejection of a good hypothesis due to a small error. >0")
	("Synchronisation.Fusion.TauTolerance", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.tauTolerance)), "Effective minimum tolerance (range/2) of tau for absolute synchronisation. Prevents the rejection of a good hypothesis due to a small error. >0")
	("Synchronisation.Fusion.ReferenceSequence", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.referenceSequence)), " Index of the sequence which defines the reference timeline")
	("Synchronisation.Fusion.ReferenceFrameRate", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.referenceFrameRate)), "< Actual frame rate of the reference sequence. Acts as a scaling factor to the solution. >0")

	("Synchronisation.RelativeSynchronisation.Problem.FlagFrameDrop", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.flagNoFrameDrop)), "If true, frame drop detection is enabled" )
	("Synchronisation.RelativeSynchronisation.Problem.MaxAttitudeDifference", value<string>()->default_value(lexical_cast<string>(*defaultParameters.synchronisationParameters.maxAttitudeDifference)), "If the attitude difference between a pair of cameras is above this value, relative synchronisation fails. Should be set to infinity for projective cameras. In radians. >0" )

	("Synchronisation.RelativeSynchronisation.Estimation.MinSegmentSupport", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.minSegmentSupport)), "Minimum absolute support for a line segment. The effective threshold is the minimum of the relative and the absolute thresholds. >0" )
	("Synchronisation.RelativeSynchronisation.Estimation.SegmentInlierThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.segmentInlierTh)), "Segment inlier threshold. >0" )
	("Synchronisation.RelativeSynchronisation.Estimation.TauQuantisationThreshold", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.tauQuantisationTh)), "Maximum distance between the offset of a line segment, and that of a candidate from the list of permissible offsets. >0" )
	("Synchronisation.RelativeSynchronisation.Estimation.MaxOverlap", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.maxOverlap)), "Maximum value of the overlap coefficient between two line segments. Used in the merging step. (0,1]" )
	("Synchronisation.RelativeSynchronisation.Estimation.MinSpan", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.minSpan)), "Minimum span of a line segment, as a percentage of the span of the reference set. Span refers to the size of the interval defined by the correspondences. (0,1]" )

	("Synchronisation.RelativeSynchronisation.GuidedSearchRegion.Enable", value<string>()->default_value("0.1 1 "), "Range of the frame rate and the offset. If omitted, guided synchronisation is disabled. >=0." )

	("Synchronisation.RelativeSynchronisation.FrameMatching.NoiseVariance", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.noiseVariance)), "Variance of the image noise, in pixel^2. >0" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.MinFeatureCorrespondence", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.minFeatureCorrespondence)), "Minimum number of feature correspondences for a valid index pairing." )

	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.kNN.MinRatio", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.nnParameters.ratioTestThreshold)), "Maximum ratio of the best similarity score to the second best, for an admissible correspondence. Quality-quantity trade-off.[0,1]" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.kNN.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.nnParameters.similarityTestThreshold)), "Minimum similarity for an admissible correspondence. >=0]" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.kNN.FlagConsistencyTest", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.nnParameters.flagConsistencyTest)), "If true, each feature in a valid correspondence is among the best k candidates for the other" )

	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.SpatialConsistency.Enable", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.flagBucketFilter)), "If true, the algorithm enforces a spatial consistency constraint between neighbouring correspondences" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.SpatialConsistency.MinSimilarity", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.bucketSimilarityThreshold)), "Minimum similarity for two spatial bins to be considered associated, relative to the maximum similarity. [0,1]" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.SpatialConsistency.MinQuorum", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.minQuorum)), "If the number correspondences in a bin is below this value, it is held exempt from the spatial consistency check. >=0" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.FeatureMatcher.SpatialConsistency.BucketDensity", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.matcherParameters.bucketDimensions[0])), "Number of bins per standard deviation. >0" )

	("Synchronisation.RelativeSynchronisation.FrameMatching.Refinement.Main.MaxIteration", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.pdlParameters.maxIteration)), "Maximum number of iterations. >=0" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.Refinement.Main.Epsilon1", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.pdlParameters.epsilon1)), "Gradient convergence criterion, as a function of the magnitude of the gradient vector. >0" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.Refinement.Main.Epsilon2", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.pdlParameters.epsilon2)), "Solution convergence criterion, as the relative distance between two successive updates. >0" )

	("Synchronisation.RelativeSynchronisation.FrameMatching.Refinement.TrustRegion.Delta0", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.pdlParameters.delta0)), "Initial size of the trust region. >0" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.Refinement.TrustRegion.LambdaShrink", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.pdlParameters.lambdaShrink)), "Shrinkage rate for the trust region. (0,1)" )
	("Synchronisation.RelativeSynchronisation.FrameMatching.Refinement.TrustRegion.LambdaGrowth", value<string>()->default_value(lexical_cast<string>(defaultParameters.synchronisationParameters.relativeSynchronisationParameters.pdlParameters.lambdaGrowth)), "Growth rate for the trust region. >1" )

	("Input.InputDescription", value<string>(), "XML file containing the input. See the generated example")
	("Input.MinActivity", value<string>()->default_value(lexical_cast<string>(defaultParameters.minActivity)), "Minimum number of features in a frame with a significant level of activity, relative to the frame with the second-largest feature count. Per sequence. [0,1]")

	("Output.Root", value<string>(), "Root directory for the output files")
	("Output.Synchronisation", value<string>()->default_value(""), "File for the estimated synchronisation")
	("Output.Log", value<string>()->default_value(""), "Log file")
	("Output.Verbose", value<string>()->default_value(lexical_cast<string>(defaultParameters.flagVerbose)), "Verbose output")
	;
}	//AppMulticameraSynchronisationImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppMulticameraSynchronisationImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppMulticameraSynchronisationImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppMulticameraSynchronisationParametersC defaultParameters;
	defaultParameters.synchronisationParameters.maxAttitudeDifference=0.7;

	tree.put("xmlcomment", "multicameraSynchronisation configuration file");

	tree.put("General","");
    tree.put("General.NoThreads", defaultParameters.nThreads);

    tree.put("Synchronisation", "");

    tree.put("Input","");
    tree.put("Input.InputDescription","/home/input.xml");
    tree.put("Input.MinActivity", defaultParameters.minActivity);

    tree.put("Output","");
    tree.put("Output.Root","/");
    tree.put("Output.Synchronisation","synchronisation.txt");
    tree.put("Output.Log","multicameraSynchronisation.log");
    tree.put("Output.Verbose","1");

    //Synchronisation parameter tree
	defaultParameters.synchronisationParameters.relativeSynchronisationParameters.guidedSearchRegion=array<double,2>{0.1, 1};	//Guided matching default

	ptree msTree=InterfaceMulticameraSynchronisationC::MakeParameterTree(defaultParameters.synchronisationParameters, detailLevel);
	tree.put_child("Synchronisation", msTree);
	tree=PruneParameterTree(tree);

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);

	//Input descriptor
	ptree inputTree;
	inputTree.put("xmlcomment", "multicameraSynchronisation input description");
	inputTree.put("xmlcomment", "Add more Sequence nodes as needed. InitialEstimate section is optional");
	inputTree.put("xmlcomment", "CameraFile has either a single camera, or a separate camera for each frame in the sequence");

	inputTree.put("Sequence","");
	inputTree.put("Sequence.TrackFile","tracks.trj");
	inputTree.put("Sequence.CameraFile", "cameras.cam");

	inputTree.put("Sequence.InitialEstimate","");
	inputTree.put("Sequence.InitialEstimate.AlphaRange", "12 33");
	inputTree.put("Sequence.InitialEstimate.TauRange", "-15 15");
	inputTree.put("Sequence.InitialEstimate.AdmissibleAlpha", "15 24 25 30");

	write_xml(filename+string(".input.dsc"), inputTree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, bool flagBasic)

/**
 * @brief Sets and validates the application parameters
 * @param[in] tree Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any preconditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppMulticameraSynchronisationImplementationC::SetParameters(const ptree& tree)
{
	AppMulticameraSynchronisationParametersC defaultParameters;
	defaultParameters.synchronisationParameters.maxAttitudeDifference=0.7;

	ptree pruned=PruneParameterTree(tree);

	//General
	double nThreads=pruned.get<double>("General.NoThreads",defaultParameters.nThreads);
	parameters.nThreads=min(omp_get_max_threads(), (int)max(0.0, nThreads));

	//Synchronisation

	if(pruned.get_child_optional("Synchronisation"))
		parameters.synchronisationParameters=InterfaceMulticameraSynchronisationC::MakeParameterObject(pruned.get_child("Synchronisation"));

	parameters.synchronisationParameters.nThreads=nThreads;

	//Input
	parameters.inputFilename=pruned.get<string>("Input.InputDescription");
	ReadInputDescriptor();

	auto c01=[](double value){ return value>=0 && value<=1;};
	parameters.minActivity=ReadParameter(tree, "Input.MinActivity", c01, "AppCalibrationVerificationImplementationC::SetParameters: Input.MinActivity must be in [0,1]", optional<double>(defaultParameters.minActivity));

	//Output

	parameters.outputPath=pruned.get<string>("Output.Root");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppCalibrationVerificationImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.resultFile=pruned.get<string>("Output.Synchronisation", defaultParameters.resultFile);
	parameters.logFile=pruned.get<string>("Output.Log", defaultParameters.logFile);
	parameters.flagVerbose=pruned.get<bool>("Output.Verbose", defaultParameters.flagVerbose);

	parameters.synchronisationParameters.flagVerbose=parameters.flagVerbose;

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppMulticameraSynchronisationImplementationC::Run()
{
	omp_set_nested(1);

	auto t0=high_resolution_clock::now();   //Start the timer

	if(parameters.flagVerbose)
	{
		cout<< "Running on "<<parameters.nThreads<<" threads\n";
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Reading the input data...\n";
	}	//if(parameters.flagVerbose)

	//Read the input
	vector<ImageSequenceT> sequenceList=LoadInput();

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Read "<<sequenceList.size()<<" sequences. Synchronising...\n";

	//Run the pipeline
	ResultT synchronisation;
	MulticameraSynchonisationDiagnosticsC diagnostics=MulticameraSynchonisationC::Run(synchronisation, sequenceList, parameters.initialEstimates, parameters.synchronisationParameters);

	if(parameters.flagVerbose)
	{
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: ";

		if(!diagnostics.flagSuccess)
			cout<<"Synchronisation failed."<<"\n";
		else
			cout<<"Synchronisation successful. Writing the output..."<<"\n";
	}	//if(parameters.flagVerbose)

	logger["TimeElapsed"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	//Output
	SaveLog(diagnostics, synchronisation);
	SaveOutput(synchronisation);

	if(parameters.flagVerbose)
	{
		size_t c=0;
		for(const auto& current : synchronisation)
		{
			cout<<"Camera "<<c<<" ";

			if(!current)
				cout<<"Failed \n";
			else
				cout<<" Frame rate: "<<get<MulticameraSynchonisationC::iFrameRate>(*current)<<" Temporal offset: "<<get<MulticameraSynchonisationC::iTemporalOffset>(*current)<<" # Frame drops: "<<get<MulticameraSynchonisationC::iDropList>(*current).size()<<"\n";

			++c;
		}	//for(const auto& current : synchronisation)
	}	//if(parameters.flagVerbose)
	return true;
}	//bool Run()

}	//AppMulticameraSynchronisationN
}	//SeeSawN

int main ( int argc, char* argv[] )
{
    std::string version("141211");
    std::string header("multicameraSynchronisation: Synchronises a multicamera setup");

    return SeeSawN::ApplicationN::ApplicationC<SeeSawN::AppMulticameraSynchronisationN::AppMulticameraSynchronisationImplementationC>::Run(argc, argv, header, version) ? 1 : 0;
}   //int main ( int argc, char* argv[] )
