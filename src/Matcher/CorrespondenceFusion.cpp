/**
 * @file CorrespondenceFusion.cpp Implementation of \c CorrespondenceFusionC
 * @author Evren Imre
 * @date 8 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "CorrespondenceFusion.h"
namespace SeeSawN
{
namespace MatcherN
{

/**
 * @brief Attempts to insert a vertex and returns the vertex descriptor
 * @param[out] correspondenceGraph Correspondence graph
 * @param[in,out] elementToVertexMap Element-to-vertex id map
 * @param[in] sourceId Source id
 * @param[in] elementId Element id
 * @return Vertex descriptor
 * @remarks If the vertex already exists, no insertion
 */
auto CorrespondenceFusionC::InsertVertex(GraphT& correspondenceGraph, map<size_t, VertexT>& elementToVertexMap, unsigned int sourceId, size_t elementId) -> VertexT
{
	VertexT vertex;
	if(elementToVertexMap.find(elementId)==elementToVertexMap.end())
	{
		vertex=add_vertex(cluster_member_type(sourceId, elementId), correspondenceGraph);
		elementToVertexMap[elementId]=vertex;
	}
	else
		vertex=elementToVertexMap[elementId];

	return vertex;
}	//auto GetVertex(map<size_t, VertexT>& elementToVertexMap, size_t elementId) -> VertexT

/**
 * @brief Filters the clusters
 * @param[in] unfiltered Unfiltered correspondences
 * @param[in] minCardinality Minimum cardinality of a cluster
 * @param[in] flagConsistency If \c true , a valid cluster cannot have multiple elements from the same source
 * @return Filtered correspondences
 */
auto CorrespondenceFusionC::FilterClusters(const vector<cluster_type>& unfiltered, unsigned int minCardinality, bool flagConsistency) -> vector<cluster_type>
{
	vector<cluster_type> filtered; filtered.reserve(unfiltered.size());

	for(const auto& current : unfiltered)
	{
		//Cardinality test
		size_t sCluster=current.size();
		if(sCluster<minCardinality)
			continue;

		//Consistency test
		if(flagConsistency)
		{
			//Get the unique source ids
			set<unsigned int> sourceIndices;
			for(const auto& element : current)
				sourceIndices.insert(get<iSourceId>(element));

			if(sourceIndices.size()<sCluster)	//Multiple elements from the same source?
				continue;
		}	//if(flagConsistency)

		filtered.emplace_back(current);
	}	//for(const auto& current : clusters)

	filtered.shrink_to_fit();
	return filtered;
}	//vector<cluster_type> FilterClusters(const vector<cluster_type>& unfiltered, unsigned int minCardinality, bool flagConsistency)

/**
 * @brief Builds the clusters
 * @param[in] correspondenceGraph Correspondence graph
 * @return A vector of clusters
 */
auto CorrespondenceFusionC::BuildClusters(GraphT& correspondenceGraph) -> vector<cluster_type>
{
	//Connected components
	vector_property_map<size_t> vertexToClusterMap(num_vertices(correspondenceGraph));
	size_t nComponents=connected_components(correspondenceGraph, vertexToClusterMap);

	//Form the clusters
	vector<cluster_type> clusters(nComponents);
	property_map<GraphT, vertex_name_t>::type vertexToElementMap=boost::get(vertex_name, correspondenceGraph);

	size_t index=0;
	auto itE=vertexToClusterMap.storage_end();
	for(auto it=vertexToClusterMap.storage_begin(); it!=itE; advance(it,1))
	{
		clusters[*it].insert(vertexToElementMap[index]);
		++index;
	}	//for(auto it=vertexToClusterMap.storage_begin(); it!=itE; advance(it,1))

	return clusters;
}	//auto BuildClusters(const GraphT& correspondenceGraph) -> vector<cluster_type>

/**
 * @brief Inserts a correspondence to the appropriate set
 * @param[in,out] observed Observed correspondences
 * @param[in,out] implied Implied correspondences
 * @param[in] original Original pairwise correspondences
 * @param[in] item1 First member of the correspondence to be inserted
 * @param[in] item2 Second member of the correspondence to be inserted
 * @param[in] clusterIndex Id of the cluster
 */
void CorrespondenceFusionC::Insert(pairwise_view_type<correspondence_container_type>& observed, pairwise_view_type<correspondence_container_type>& implied, const pairwise_view_type<bimap<unordered_multiset_of<size_t>, list_of<size_t>, list_of_relation> >& original, const cluster_member_type& item1, const cluster_member_type& item2, size_t clusterIndex)
{
	pair_id_type pairId(get<iSourceId>(item1), get<iSourceId>(item2));	//Query item

	//Determine whether the pair is observed or implied

	bool flagImplied=false;
	bool flagObserved=false;

	//Is there such an observed source pair?
	auto itOriginal=original.find(pairId);

	//If the source pair exists, look up for the correspondence
	if(itOriginal!=original.end())
	{
		const auto& currentList=itOriginal->second;

		//Get all instances of the left key
		auto itRange=currentList.left.equal_range(get<iElementId>(item1));

		//Look for an element with the correct right member
		for(auto it=itRange.first; (it!=itRange.second) && (!flagObserved); std::advance(it,1))
			flagObserved = it->second==get<iElementId>(item2);
	}	//if(itOriginal==original.end())

	flagImplied=!flagObserved;	//If the pair is not in the original list, it is implied

	//Insert an observed pair
	if(flagObserved)
		observed.find(pairId)->second.push_back(correspondence_container_type::value_type(get<iElementId>(item1), get<iElementId>(item2), clusterIndex));

	//Insert an implied pair
	if(flagImplied)
	{
		correspondence_container_type& current=implied[pairId];
		current.push_back(correspondence_container_type::value_type(get<iElementId>(item1), get<iElementId>(item2), clusterIndex));
	}	//if(flagImplied)
}	//void Insert(pairwise_view_type<correspondence_container_type>& observed, pairwise_view_type<correspondence_container_type>& implied, const pairwise_view_type<bimap<unordered_multiset_of<size_t>, list_of<size_t>, list_of_relation> >& original, unsigned int source1, unsigned int source2)

/**
 * @brief Converts a pairwise view to a unified view
 * @param[in] src Pairwise view to be converted
 * @return Unified view corresponding to \c src
 * @remarks \c output might have empty elements
 */
auto CorrespondenceFusionC::ConvertToUnified(const pairwise_view_type<correspondence_container_type>& src) -> unified_view_type
{
	//Find the maximum cluster id
	size_t maxId=0;
	for(const auto& currentContainer : src)
		for(const auto& current : currentContainer.second)
			if(current.info>maxId)
				maxId=current.info;

	unified_view_type output(maxId+1);

	//Convert
	for(const auto& currentContainer : src)
		for(const auto& current : currentContainer.second)
		{
			output[current.info].emplace(get<0>(currentContainer.first), current.left);
			output[current.info].emplace(get<1>(currentContainer.first), current.right);
		}

	return output;
}	//unified_view_type ConvertToUnified(const pairwise_view_type<correspondence_container_type>& src)

/**
 * @brief Splices two correspondence stacks
 * @param[in] view1 First list
 * @param[in] view2 Second list
 * @return Merged list
 */
auto CorrespondenceFusionC::SplicePairwiseViews(const pairwise_view_type<correspondence_container_type>& view1, const pairwise_view_type<correspondence_container_type>& view2) -> pairwise_view_type<correspondence_container_type>
{
	//Image pairs in the lists
	pairwise_view_type<correspondence_container_type> output;
	for_each(view1, [&](const pairwise_view_type<correspondence_container_type>::value_type& current){output.emplace(current.first, correspondence_container_type());} );
	for_each(view2, [&](const pairwise_view_type<correspondence_container_type>::value_type& current){output.emplace(current.first, correspondence_container_type());} );

	//Splice
	auto itE1=view1.end();
	auto itE2=view2.end();
	for(auto& current : output)
	{
		auto it1=view1.find(current.first);
		if(it1!=itE1)
			current.second.insert(current.second.end(), it1->second.begin(), it1->second.end());

		auto it2=view2.find(current.first);
		if(it2!=itE2)
			current.second.insert(current.second.end(), it2->second.begin(), it2->second.end());
	}	//for(auto& current : output)

	return output;
}	//pairwise_view_type<correspondence_container_type> SplicePairwiseViews(const pairwise_view_type<correspondence_container_type>& view1, const pairwise_view_type<correspondence_container_type>& view2)

}	//MatcherN
}	//SeeSawN

