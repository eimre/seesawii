/**
 * @file ClosestPointAssignmentProblem.cpp Instantiations for ClosestPointAssignmentProblemC . Implementation of non-template members
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "ClosestPointAssignmentProblem.h"

namespace SeeSawN
{
namespace MatcherN
{

/**
 * @brief Default constructor
 * @post The object is not initialised
 */
ClosestPointAssignmentProblemC::ClosestPointAssignmentProblemC()
{
    flagValid=false;
}   //ClosestPointAssignmentProblemC()


/**
 * @brief Returns the number of elements in the first set
 * @return Number of elements in the first set
 */
size_t ClosestPointAssignmentProblemC::GetSize1() const
{
    return similarityArray.rows();
}   //size_t GetSize1()

/**
 * @brief Returns the number of elements in the second set
 * @return Number of elements in the second set
 */
size_t ClosestPointAssignmentProblemC::GetSize2() const
{
    return similarityArray.cols();
}   //size_t GetSize2()

/**
 * @brief Computes the similarity score for the specified elements
 * @param[in] i1 Index into the first set
 * @param[in] i2 Index into the second set
 * @return Similarity score
 */
optional<double> ClosestPointAssignmentProblemC::ComputeSimilarity(size_t i1, size_t i2) const
{
    return similarityArray(i1, i2);
}   //optional<double> ComputeSimilarity(size_t i1, size_t i2)

/**
 * @brief Reports whether the object is valid
 * @return \c false if the object is not valid
 */
bool ClosestPointAssignmentProblemC::IsValid() const
{
    return flagValid;
}   //bool IsValid()

}   //MatcherN
}	//SeeSawN


