/**
 * @file GroupMatchingProblem.h Public interface for GroupMatchingProblemC
 * @author Evren Imre
 * @date 26 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GROUP_MATCHING_PROBLEM_H_2135613
#define GROUP_MATCHING_PROBLEM_H_2135613

#include "GroupMatchingProblem.ipp"
#include <set>
#include <vector>
#include <cstddef>
#include "../Elements/Correspondence.h"

namespace SeeSawN
{
namespace MatcherN
{

class GroupMatchingProblemC;    ///< Establishes correspondences between two sets of groups, given the correspondences between the group members

/********** EXTERN TEMPLATES **********/

using std::vector;
using std::set;
using std::size_t;
using SeeSawN::ElementsN::CorrespondenceListT;
extern template GroupMatchingProblemC::GroupMatchingProblemC(const vector<set<unsigned> >&, const vector<set<unsigned> >&, const vector<double>&,  unsigned int, const set<unsigned int>&);
}   //MatcherN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<std::size_t>;
extern template class std::vector<unsigned int>;
extern template class std::set<int>;
extern template class boost::optional<double>;
extern template class std::set<unsigned int>;
#endif /* GROUP_MATCHING_PROBLEM_H_2135613 */
