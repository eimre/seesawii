/**
 * @file GroupMatchingProblem.ipp Implementation of GroupMatchingProblemC
 * @author Evren Imre
 * @date 26 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GROUP_MATCHING_PROBLEM_IPP_6423126
#define GROUP_MATCHING_PROBLEM_IPP_6423126

#include <boost/concept_check.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <cstddef>
#include <tuple>
#include <iterator>
#include <stdexcept>
#include <type_traits>
#include "../Elements/Correspondence.h"
#include "../Wrappers/BoostBimap.h"

namespace SeeSawN
{
namespace MatcherN
{

using boost::ForwardRangeConcept;
using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::optional;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::list_of_relation;
using boost::bimaps::with_info;
using Eigen::Array;
using Eigen::ArrayXXf;
using std::vector;
using std::size_t;
using std::set;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::advance;
using std::logic_error;
using std::is_integral;
using std::is_unsigned;
using std::is_convertible;
using SeeSawN::WrappersN::VectorToBimap;
using SeeSawN::WrappersN::BimapToVector;

//TODO Improve the documentation. More on the algorithm and the intended usage. Point to FeatureMatcherC
/**
 * @brief Establishes correspondences between two sets of groups, given the correspondences between the group members
 * @remarks Input:
 *          - A bimap of member correspondences, with weights
 *          - Each correspondence holds the id's of the groups to which it is a member. Multiple memberships are allowed.
 * @remarks Usage:
  *          - Run NearestNeighbourMatcherC with the problem. This yields a set of group correspondences
 *          - If the aim is to filter the original (member) correspondence set with the knowledge of group correspondences: call FilterInput with the result of the matcher, and the input data.
 *          The result is the indices of the member correspondences consistent with the group correspondences
 * @remarks Use case: Image feature matching with repetitive texture. Assign the features to overlapping bins in the image. Feature correspondences identify the matching bins, which, in turn, used to resolve the ambiguous correspondences
 * @remarks The algorithm: Each member correspondence holds a set of memberships, and it votes for all pairs of memberships. Then the votes are normalised by the average number of total votes for each pair of groups.
 * @ingroup Problem
 * @nosubgrouping
 */
class GroupMatchingProblemC
{
    private:

        /** @name Configuration */ //@{
        unsigned int minQuorum; ///< Minimum number of members in a group for a valid similarity score
        //@}

        /** @name State */ //@{
        ArrayXXf scoreArray; ///< Holds the similarity scores
        vector<size_t> indexMap1;   ///< Index map for the first set
        vector<size_t> indexMap2;   ///< Index map for the second set
        vector<float> histogram1;  ///< Weighted group membership histogram for the first set
        vector<float> histogram2;  ///< Weighted group membership histogram for the second set
        bool flagValid; ///< \c false if the object is invalid
        //@}

        /**@name Implementation details */ //@{
        template<class MembershipT> tuple<vector<size_t>, vector<float> > Preprocess(const vector<MembershipT>& input, const vector<double>& weights);    ///< Preprocessing. Index map and histogram

        //@}
    public:

        /**@name Constructors */ //@{
        template<class MembershipT> GroupMatchingProblemC(const vector<MembershipT>& left, const vector<MembershipT>& right, const vector<double>& weights, unsigned int mminQuorum, const MembershipT& dummy);    ///< Constructor
        //@}

        /**@name Mandatory interface*/ //@{
        GroupMatchingProblemC();    ///< Dummy constructor
        typedef float real_type;

        size_t GetSize1() const;  ///< Returns the number of elements in the first set
        size_t GetSize2() const;  ///< Returns the number of elements in the second set
        optional<float> ComputeSimilarity(size_t i1, size_t i2);   ///< Computes the similarity score for the specified elements
        bool IsValid() const; ///< \c true if the problem is valid
        //@}

        /** @name Utilities */ //@{
         void Clear();   ///< Clears the state
        template<class MembershipT, class GroupCorrespondenceRangeT> vector<size_t> FilterInput(const GroupCorrespondenceRangeT& groupCorrespondences, const vector<MembershipT>& left, const vector<MembershipT>& right);  ///< Filters the original input with the group correspondences
        //@}
};  //class GroupMatchingProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Preprocessing. Index map and histogram
 * @tparam MembershipT A container holding the id's of the groups to which an element belongs
 * @param[in] input Input
 * @param[in] weights Weights
 * @return A tuple: IndexMap; Weighted histogram
 * @pre \c MembershipT is a single pass range of elements convertible to \c int
 */
template<class MembershipT>
tuple<vector<size_t>, vector<float> > GroupMatchingProblemC::Preprocess(const vector<MembershipT>& input, const vector<double>& weights)
{
	//Precondition
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<MembershipT>));
	static_assert(is_convertible<typename boost::range_value<MembershipT>::type, int>::value, "GroupMatchingProblemC::Preprocess : MembershipT must hold values convertible to int");

    //Indices present in the set
    set<int> present;	//The usual way is sort+unique. But not as efficient when there are many duplicates
    for(const auto& current : input)
    	present.insert(boost::const_begin(current), boost::const_end(current));

    //Index map
    vector<size_t> indexMap(*present.rbegin()+1, 0);

    size_t index=0;
    for(auto c : present)
    {
       indexMap[c]=index;
        ++index;
    }   //for(auto c : present)

    //Histogram
    vector<float> histogram(present.size(), 0);
    size_t index2=0;
    for(const auto& current : input)
    {
        for(auto c : current)
            histogram[ indexMap[c] ]+=weights[index2];

        ++index2;
    }	//for(const auto& current : input)

    return make_tuple(indexMap, histogram);
}   //vector<size_t> Preprocess(const vector_of<MembershipRangeT>& input, unsigned int maxIndex)

/**
 * @brief Constructor
 * @tparam MembershipT A container holding the ids of the groups to which an element belongs
 * @param[in] left Memberships belonging to the left members of each corresponding pair
 * @param[in] right Memberships belonging to the right members of each corresponding pair
 * @param[in] weights Weight for each correspondence
 * @param[in] mminQuorum Minimum number of votes for a valid match
 * @param[in] dummy Dummy parameter for identifying \c MembershipT . Apparently, it is not possible to call a templated constructor the way a function is called.
 * @pre \c MembershipT is a forward range holding unsigned integral values
 * @pre \c left, \c right and \c weights have the same number of elements
 * @remarks \c left[i] , \c right[i] and \c weights[i] describe a correspondence
 */
template<class MembershipT>
GroupMatchingProblemC::GroupMatchingProblemC(const vector<MembershipT>& left, const vector<MembershipT>& right, const vector<double>& weights, unsigned int mminQuorum, const MembershipT& dummy)
{
	//Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<MembershipT>));
    static_assert(is_integral< typename range_value<MembershipT>::type >::value, "GroupMatchingProblemC::GroupMatchingProblemC : MembershipT must hold integral values." );
    static_assert(is_unsigned< typename range_value<MembershipT>::type >::value, "GroupMatchingProblemC::GroupMatchingProblemC : MembershipT must hold unsigned values." );
    assert(left.size()==right.size());
    assert(left.size()==weights.size());


	 //Preprocess the input
	tie(indexMap1, histogram1)=Preprocess(left, weights);
	size_t nElement1=histogram1.size();

	tie(indexMap2, histogram2)=Preprocess(right, weights);
	size_t nElement2=histogram2.size();

	//Count the votes
	ArrayXXf votes(nElement1, nElement2);  // Votes
	votes.setConstant(0);
	size_t sLeft=left.size();
	for(size_t c=0; c<sLeft; ++c)
		//Each index pair casts a vote
		for(auto c2 : right[c])
			for(auto c1 : left[c])
				votes( indexMap1[c1], indexMap2[c2] )+=weights[c];	//Weighted votes

	//The actual score is 2*vote/(TotalWeight1+TotalWeight2), so that max score is 1
	scoreArray.resize(nElement1, nElement2);
	for(size_t c2=0; c2<nElement2; ++c2)
		for(size_t c1=0; c1<nElement1; ++c1)
			scoreArray(c1,c2) = 2*votes(c1,c2)/(histogram1[c1]+histogram2[c2]);

	minQuorum=mminQuorum;
	flagValid=true;

}	//GroupMatchingProblemC(const vector<MembershipT>& leftRange, const vector<MembershipT>& rightRange, const vector<double>& weightRange, unsigned int mminQuorum, , const MembershipT& dummy)

/**
@brief Filters the original input with the group correspondences
 * @tparam GroupCorrespondenceRangeT An index correspondence bimap
 * @tparam MembershipT A membership set
 * @param[in] groupCorrespondences Group correspondences
 * @param[in] left Memberships belonging to the left members of each corresponding pair
 * @param[in] right Memberships belonging to the right members of each corresponding pair
 * @return A set of indices into \c memberCorrespondences, indicating the surviving elements
 * @pre \c GroupCorrespondenceRangeT is one of the index correspondence bimaps in \c Correspondence.h (unenforced)
 * @pre \c MembershipT is a forward range of unsigned integrals
 * @pre The object is valid
 * @pre \c left and \c right have the same number of elements
 * @throws logic_error If the object is invalid.
 * @remarks Acceptance criteria: Either the member correspondence share a matching group, or the minimum quorum requirement is not met (i.e., not enough evidence to reject)
 * @remarks \c left and \c right should be identical to the input passed into the constructor
 */
template<class MembershipT, class GroupCorrespondenceRangeT>
vector<size_t> GroupMatchingProblemC::FilterInput(const GroupCorrespondenceRangeT& groupCorrespondences, const vector<MembershipT>& left, const vector<MembershipT>& right)
{
	 //Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<MembershipT>));
	static_assert(is_integral< typename range_value<MembershipT>::type >::value, "GroupMatchingProblemC::FilterInput : MembershipT must hold integral values." );
	static_assert(is_unsigned< typename range_value<MembershipT>::type >::value, "GroupMatchingProblemC::FilterInput : MembershipT must hold unsigned values." );
	assert(left.size()==right.size());

	if(!IsValid())
		throw(logic_error("GroupMatchingProblemC::MakeOutput : Invalid object"));

	//Make a correspondence array
	Array<bool, Eigen::Dynamic, Eigen::Dynamic> correspondenceArray(GetSize1(), GetSize2());
	correspondenceArray.setConstant(false);
	for(const auto& current : groupCorrespondences)
		correspondenceArray(current.left, current.right)=true;

	//Filter the index correspondences
	size_t nIndex=boost::distance(left);
	vector<size_t> output; output.reserve(nIndex);

	for(size_t c=0; c<nIndex; ++c)
	{
		//For all pairs
		bool flagPass=false;
		for(auto c2 : right[c])
		{
			for(auto c1 : left[c])
			{
				//Pass: Either verified as a corresponding group, or exempt from the test due to low quorum
				flagPass= (correspondenceArray( indexMap1[c1], indexMap2[c2])) || (histogram1[indexMap1[c1]]<minQuorum && histogram2[indexMap2[c2]]<minQuorum);

				if(flagPass)
					break;
			}   //for(auto c2 : current.right)

			if(flagPass)
				break;
		}   //for(auto c2 : current.right)

		if(flagPass)
			output.push_back(c);
	}   //for(const auto& current : indexCorrespondences)

	output.shrink_to_fit();
	return output;

}	//vector<size_t> FilterInput(const GroupCorrespondenceRangeT& groupCorrespondences, const vector<MembershipT>& left, const vector<MembershipT>& right)

}   //MatcherN
}	//SeeSawN

#endif /* GROUP_MATCHING_PROBLEM_IPP_6423126 */
