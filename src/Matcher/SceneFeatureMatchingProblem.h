/**
 * @file SceneFeatureMatchingProblem.h Public interface for \c SceneFeatureMatchingProblemC
 * @author Evren Imre
 * @date 28 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_FEATURE_MATCHING_PROBLEM_H_5932423
#define SCENE_FEATURE_MATCHING_PROBLEM_H_5932423

#include "SceneFeatureMatchingProblem.ipp"
#include "../Metrics/Constraint.h"
#include "../Metrics/Similarity.h"

namespace SeeSawN
{
namespace MatcherN
{

template<class SimilarityT, class ConstraintT> class SceneFeatureMatchingProblemC;	///< Problem class for nearest-neighbour matching problem for scene features

/********** EXTERN TEMPLATES *********/
using SeeSawN::MetricsN::EuclideanDistanceConstraint3DT;
using SeeSawN::MetricsN::InverseEuclideanSceneFeatureDistanceConverterT;
using SeeSawN::ElementsN::CorrespondenceListT;
extern template class SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, EuclideanDistanceConstraint3DT >;
extern template SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, EuclideanDistanceConstraint3DT>::SceneFeatureMatchingProblemC( const InverseEuclideanSceneFeatureDistanceConverterT&, const optional<EuclideanDistanceConstraint3DT>&, const vector<SceneFeatureC>&, const vector<SceneFeatureC>&, bool, unsigned int );
extern template void FeatureMatchingProblemBaseC<SceneFeatureC, SceneFeatureC, InverseEuclideanSceneFeatureDistanceConverterT, EuclideanDistanceConstraint3DT>::AssessAmbiguity( CorrespondenceListT&, unsigned int, unsigned int) const;

}	//MatcherN
}	//SeeSawN

#endif /* SCENE_FEATURE_MATCHING_PROBLEM_H_5932423 */
