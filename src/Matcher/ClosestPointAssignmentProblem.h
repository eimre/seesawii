/**
 * @file ClosestPointAssignmentProblem.h Public interface of ClosestPointAssignmentProblemC
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CLOSEST_POINT_ASSIGNMENT_PROBLEM_H_5264652
#define CLOSEST_POINT_ASSIGNMENT_PROBLEM_H_5264652

#include "ClosestPointAssignmentProblem.ipp"
#include <vector>
#include "../Metrics/Similarity.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/BoostStatisticalDistributions.h"

namespace SeeSawN
{
namespace MatcherN
{

class ClosestPointAssignmentProblemC;   ///< A N-nearest neighbour matching problem for closest-point assignment

}   //MatcherN
}	//SeeSawN


#endif /* CLOSEST_POINT_ASSIGNMENT_PROBLEM_H_5264652 */
