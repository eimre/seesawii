/**
 * @file SceneImageFeatureMatchingProblem.cpp Instantiations for \c SceneImageFeatureMatchingProblemC
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "SceneImageFeatureMatchingProblem.h"
namespace SeeSawN
{
namespace MatcherN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT >;
template SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT>::SceneImageFeatureMatchingProblemC( const InverseEuclideanSceneImageFeatureDistanceConverterT&, const optional<ReprojectionErrorConstraintT>&, const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, bool, unsigned int );
template void FeatureMatchingProblemBaseC<SceneFeatureC, ImageFeatureC, InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT>::AssessAmbiguity( CorrespondenceListT&, unsigned int, unsigned int) const;

template class BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT >;
template BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT>::BinarySceneImageFeatureMatchingProblemC( const InverseHammingSceneImageFeatureDistanceConverterT&, const optional<ReprojectionErrorConstraintT>&, const vector<OrientedBinarySceneFeatureC>&, const vector<BinaryImageFeatureC>&, bool, unsigned int );
template void FeatureMatchingProblemBaseC<OrientedBinarySceneFeatureC, BinaryImageFeatureC, InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT>::AssessAmbiguity( CorrespondenceListT&, unsigned int, unsigned int) const;

}	//MatcherN
}	//SeeSawN

