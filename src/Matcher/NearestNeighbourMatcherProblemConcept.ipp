/**
 * @file NearestNeighbourMatcherProblemConcept.ipp
 * @author Evren Imre
 * @date 13 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NEAREST_NEIGHBOUR_MATCHER_PROBLEM_CONCEPT_IPP_1601535
#define NEAREST_NEIGHBOUR_MATCHER_PROBLEM_CONCEPT_IPP_1601535

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <vector>

namespace SeeSawN
{
namespace MatcherN
{

using boost::CopyConstructible;
using boost::Assignable;
using boost::optional;
using std::vector;

/**
 * @brief Nearest neighbour matching problem concept
 * @tparam TestT Class to be tested
 * @ingroup Concept
 */
template<class TestT>
class NearestNeighbourMatchingProblemConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

    public:

        BOOST_CONCEPT_USAGE(NearestNeighbourMatchingProblemConceptC)
        {
            TestT tested;
            size_t nItem1=tested.GetSize1(); (void)nItem1;
            size_t nItem2=tested.GetSize2(); (void)nItem2;

            typedef typename TestT::real_type RealT;

            size_t i1;
            size_t i2;
            optional<RealT> score=tested.ComputeSimilarity(i1, i2);

            bool flag=tested.IsValid(); (void)flag;
        }   //BOOST_CONCEPT_USAGE(NearestNeighbourMatchingProblemConceptC)
	///@endcond
};  //class NearestNeighbourMatchingProblemConceptC

}   //MatcherN
}	//SeeSawN

#endif /* NEAREST_NEIGHBOUR_MATCHER_PROBLEM_CONCEPT_IPP_1601535 */
