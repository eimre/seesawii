/**
 * @file NearestNeighbourMatcher.ipp Implementation of NearestNeighbourMatcherC
 * @author Evren Imre
 * @date 13 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NEAREST_NEIGHBOUR_MATCHER_IPP_4319231
#define NEAREST_NEIGHBOUR_MATCHER_IPP_4319231

#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/heap/pairing_heap.hpp>
#include <tuple>
#include <array>
#include <algorithm>
#include <iterator>
#include <utility>
#include <stdexcept>
#include <map>
#include <vector>
#include <string>
#include <functional>
#include <cstddef>
#include <cmath>
#include "NearestNeighbourMatcherProblemConcept.h"
#include "../Elements/Correspondence.h"

namespace SeeSawN
{
namespace MatcherN
{

using boost::optional;
using boost::lexical_cast;
using boost::heap::pairing_heap;
using std::tuple;
using std::tie;
using std::get;
using std::size_t;
using std::multimap;
using std::map;
using std::vector;
using std::array;
using std::greater;
using std::any_of;
using std::partition_point;
using std::next;
using std::prev;
using std::advance;
using std::make_pair;
using std::pair;
using std::invalid_argument;
using std::string;
using std::max;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::MatcherN::NearestNeighbourMatchingProblemConceptC;

/**
 * @brief Diagnostics object for NearestNeighbourMatcherC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct NearestNeighbourMatcherDiagnosticsC
{
    bool flagSuccess;   ///< \c true if the operation is successful
    size_t sSet1;   ///< Number of elements in the first set
    size_t sSet2;   ///< Number of elements in the second set
    size_t sCorrespondences;    ///< Number of correspondences

    /**
     * @brief Constructor
     */
    NearestNeighbourMatcherDiagnosticsC() : flagSuccess(false), sSet1(0), sSet2(0), sCorrespondences(0)
    {}
};  //struct NearestNeighbourMatcherDiagnosticsC

/**
 * @brief Parameter object for NearestNeighbourMatcherC
 * @remarks The default value for \c similarityTestThreshold is the reciprocal of the half of the maximum distance between two unit-norm vectors
 * @ingroup Parameters
 * @nosubgrouping
 */
struct NearestNeighbourMatcherParametersC
{
    double similarityTestThreshold;  ///< Minimum similarity score for an eligible match
    double ratioTestThreshold;   ///< Maximum ratio of similarity score of the first item outside of the neighbourhood to that of the best item in the neighbourhood. >1 disables the ratio test entirely (i.e., both distinctiveness and plausibility)
    unsigned int neighbourhoodSize12;    ///< Number of nearest neighbours to an item of the first set in the second set
    unsigned int neighbourhoodSize21;    ///< Number of nearest neighbours to an item of the second set in the first set
    bool flagConsistencyTest;   ///< If \c false the left-right consistency check is skipped

    /**
     * @brief Constructor
     */
    NearestNeighbourMatcherParametersC() : similarityTestThreshold(1.41), ratioTestThreshold(0.75), neighbourhoodSize12(7), neighbourhoodSize21(7), flagConsistencyTest(true)
    {}

};  //struct NearestNeighbourMatcherParametersC

/**
 * @brief Nearest neighbour matcher
 * @tparam ProblemT A nearest neighbour matcher problem
 * @pre \c ProblemT satisfies NearestNeighbourMatchingProblemConceptC
 * @remarks Implementation of the naive N-nearest neighbour algorithm. Neighbourhood size dictates the matching strategy, i.e. 1:1 or M:N. Spatial partitioning algorithms such as k-d trees are faster in smaller dimensionalities
 * @remarks Tests:
 * - Similarity test: The similarity score of a match candidate must be above this value.
 * - Ratio test- distinctiveness:  The ratio of the similarity score of the best outsider to that of the best insider must be below a specified threshold.
 * - Ratio test- plausibility: The ratio of the similarity score of an insider to that of the best insider must be above the distinctiveness threshold.
 * - Consistency test: For a valid match, items must be in each other's neighbourhoods
 * @remarks The (generalised) ratio test aims to remove any neighbourhoods that are not sufficiently distinct from the rest of the set (i.e. the best outsider). Also, it retains only the plausible candidates (i.e. reasonably similar to the best candidate).
 * @remarks Neighbourhood size:
 * - If the neighbourhood sizes are not equal, the solution is not symmetric- i.e. changing the order of the sets yields a different solution
 * - M:0 matching is allowed: querying in a database, k-means (1:0). However, consistency test should be disabled, or the algorithm does not return any correspondences.
 * @remarks The output is a bimap of index pairs. The first element of the info component is the similarity score. The second is the ratio between the second-best and the best similarity scores. Of the two ratios associated with each pair, the worse (higher) is selected.
 * @ingroup Algorithm
 * @nosubgrouping
 */
//Experiment: Boost.Pool as allocator for NeighbourhoodT and RatioCacheT. With mutex, inefficient. with null_mutex, when multiple instances are run simultaneously, crash. Problem: singleton pool
//TODO: Constrained matching: Potential acceleration in BuildNeighbourhoods by iterating only over the list of index pairs that satisfy the constrait.
template<class ProblemT>
class NearestNeighbourMatcherC
{
    private:

        //Preconditions
        //@cond CONCEPT_CHECK
        BOOST_CONCEPT_ASSERT((NearestNeighbourMatchingProblemConceptC<ProblemT>));
        //@endcond

        typedef typename ProblemT::real_type RealT; ///< A floating point type

        typedef vector< pair<RealT, size_t> > QueueContainerT;
        typedef pairing_heap<typename QueueContainerT::value_type, boost::heap::compare<greater<typename QueueContainerT::value_type >>>	QueueT;
        typedef multimap<RealT, size_t, greater<RealT>> NeighbourhoodT;	///< A neighbourhood. [score id]
        typedef vector<NeighbourhoodT> CandidateListT;   ///< A list of match candidates. Each element is the N-nearest neighbours to the corresponding item

        /**@name Implementation details*/ //@{
        static void ValidateParameters(const NearestNeighbourMatcherParametersC& parameters);  ///< Validates the parameter

        static inline void InsertQueue(QueueT& dst, RealT score, size_t id, size_t sN, double similarityTestThreshold);	///< Inserts a candidate to the queue
        static tuple<CandidateListT, CandidateListT> BuildNeighbourhoods(ProblemT& problem, const NearestNeighbourMatcherParametersC& parameters, bool flagRatioTest); ///< Builds the N-neighbourhoods

        typedef vector<map<size_t, RealT, std::less<size_t>> >  RatioCacheT;   ///< A container holding the similarity score ratios of the match candidates
        static void ApplyRatioTest(CandidateListT& neighbourhoods, optional<RatioCacheT>& ratioCache, RealT ratioTh, size_t sN);  ///< Applies the ratio test

        static void ApplyConsistencyTest(CandidateListT& neighbourhoods12, const CandidateListT& neighbourhoods21); ///< Applies the consistency test

        static CorrespondenceListT BuildCorrespondenceList(const NearestNeighbourMatcherParametersC& parameters, const CandidateListT& neighbourhoods12, optional<RatioCacheT>& ratioCache12, optional<RatioCacheT>& ratioCache21, bool flagRatioTest); ///< Builds the correspondence list
        //@}

    public:

        static NearestNeighbourMatcherDiagnosticsC Run(CorrespondenceListT& correspondences, ProblemT& problem, const NearestNeighbourMatcherParametersC& parameters); ///< Runs the matching algorithm

};  //class NearestNeighbourMatcherC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameter
 * @param[in] parameters Parameters to be validated
 * @pre \c neighbourhoodSize12 is positive
 * @throws invalid_argument if \c neighbourhoodSize12 is not positive
 */
template<class ProblemT>
void NearestNeighbourMatcherC<ProblemT>::ValidateParameters(const NearestNeighbourMatcherParametersC& parameters)
{
    if(parameters.neighbourhoodSize12==0)
        throw(invalid_argument(string("NearestNeighbourMatcherC<ProblemT>::ValidateParameters: neighbourhoodSize12 must be a positive value, not ") + lexical_cast<string>(parameters.neighbourhoodSize12) ) );
 }   //void ValidateParameters(const NearestNeighbourMatcherParametersC& parameters)

/**
 * @brief Inserts a candidate to the queue
 * @param[in, out] dst Queue to be updated
 * @param[in] score Candidate score
 * @param[in] id Id of the matching item
 * @param[in] sN Neighbourhood size
 * @param[in] similarityTestThreshold Similarity threshold
 * @remarks Inline, because profiler says so
 * @pre \c score>0
 */
template<class ProblemT>
void NearestNeighbourMatcherC<ProblemT>::InsertQueue(QueueT& dst, RealT score, size_t id, size_t sN, double similarityTestThreshold)
{
	//Preconditions
	assert(score>0);

	//Logic
	//If the set is empty, insert
	//If the set is not full, insert if the score is above the threshold, or the worst score is less than the current
	//If the set is full, insert if the worst score is less than the current
	//So, at a given time, the queue has at most one element <threshold, and that is the "best outsider" encountered so far

	if(sN==0)
		return;

	//Add
	if(dst.empty() || ( (dst.size()!=sN) && (score>=similarityTestThreshold || (score<similarityTestThreshold && dst.top().first<similarityTestThreshold))))
	{
		dst.emplace(score,id);
		return;
	}

	//Replace
	if(score>dst.top().first && (dst.size()==sN || (dst.size()!=sN && score<similarityTestThreshold) ) )
	{
		dst.pop();
		dst.emplace(score,id);
		return;
	}

}	//void InsertQueue(QueueT& dst, RealT score, size_t id, size_t sN)

/**
 * @brief Builds the N-neighbourhoods
 * @param[in, out] problem Problem
 * @param[in] parameters Parameters
 * @param[in] flagRatioTest \c true if a best outsider is needed for the ratio test
 * @return Neighbourhoods
 * @remarks Best outsider, too, satisfies the similarity constraint.
 */
template<class ProblemT>
auto NearestNeighbourMatcherC<ProblemT>::BuildNeighbourhoods(ProblemT& problem, const NearestNeighbourMatcherParametersC& parameters, bool flagRatioTest)-> tuple<CandidateListT, CandidateListT>
{
    //Number of items to be matched
    size_t nItem1=problem.GetSize1();
    size_t nItem2=problem.GetSize2();

    size_t sN12=parameters.neighbourhoodSize12+(flagRatioTest?1:0);    //Last element for the best out-of-neighbourhood candidate, if there is a ratio test
    size_t sN21=parameters.neighbourhoodSize21+(flagRatioTest?1:0);

    vector<QueueT> queueList1(nItem1);
    vector<QueueT> queueList2(nItem2);

    //Compute the similarity scores, and enforce the similarity constraint
    //TODO This assumes a column-major similarity matrix. The problem should communicate its preferred mode of access

    optional<RealT> score;
    for(size_t c2=0; c2<nItem2; ++c2)
    {
    	QueueT& pQueue2=queueList2[c2];
        for(size_t c1=0; c1<nItem1; ++c1)
        {
        	score=problem.ComputeSimilarity(c1, c2);   //Uninitialised if the match is not eligible

            //If the match is not eligible, continue. The similarity threshold is imposed later on
            if(!score || *score==0)
                continue;

            //Insertion
            //The similarity test is performed in the insertion stage
            InsertQueue(queueList1[c1], *score, c2, sN12, parameters.similarityTestThreshold);
            InsertQueue(pQueue2, *score, c1, sN21, parameters.similarityTestThreshold);
        }   //for(size_t c1=0; c1<nItem1; ++c1)
    }	//for(size_t c2=0; c2<nItem2; ++c2)

    //Output
    tuple<CandidateListT, CandidateListT > output;
    get<0>(output).resize(nItem1);
    get<1>(output).resize(nItem2);

    NeighbourhoodT tmp;
    auto neighbourhoodInserter=[&](QueueT& current){ tmp.insert(current.top()); current.pop();};	//Assumes that tmp is cleared at each iteration

    //Preparing the output
    //Eliminate the 1-element neighbourhoods failing the similarity test

    for(size_t c=0; c<nItem1; ++c)
    {
        if(queueList1[c].size()==1 && queueList1[c].top().first<parameters.similarityTestThreshold)
            continue;

        while(!queueList1[c].empty())
            neighbourhoodInserter(queueList1[c]);

        get<0>(output)[c].swap(tmp);	//This clears tmp
    }	//for(size_t c=0; c<nItem1; ++c)

    for(size_t c=0; c<nItem2; ++c)
    {
    	if(queueList2[c].size()==1 && queueList2[c].top().first<parameters.similarityTestThreshold)
			continue;

    	while(!queueList2[c].empty())
    		neighbourhoodInserter(queueList2[c]);

    	get<1>(output)[c].swap(tmp);
    }	//for(size_t c=0; c<nItem2; ++c)

    //At this point, the similarity test is completed. Any non-empty neighbourhoods have at least one element satisfying the constraint

    return output;
}   //auto BuildNeighbourhoods(ProblemT& problem,  const NearestNeighbourMatcherParametersC& parameters)-> array<CandidateListT, 2>

/**
 * @brief Applies the ratio test
 * @param[in, out] neighbourhoods Neighbourhoods
 * @param[out] ratioCache Similarity ratio cache
 * @param[in] ratioTh Ratio threshold
 * @param[in] sN Neighbourhood size
 */
template<class ProblemT>
void NearestNeighbourMatcherC<ProblemT>::ApplyRatioTest(CandidateListT& neighbourhoods, optional<RatioCacheT>& ratioCache, RealT ratioTh, size_t sN)
{
    //For all neighbourhoods

    //Distinctiveness test
    RealT score1;
    RealT score2;
    RealT ratio;

    //Plausibility test
    auto plausibilityTester=[&](const typename NeighbourhoodT::value_type& candidate){return candidate.first/score1 > ratioTh; };
    size_t c=0;
    for(auto& current : neighbourhoods)
    {
        ++c;
        size_t cm1=c-1;
        size_t sNc=current.size();

        //If empty or single element, automatic success
        if( sNc==0 || sNc==1)
        {
            if(ratioCache && sNc==1)
                (*ratioCache)[cm1].emplace(current.begin()->second, 0);    //Ratio is 0 as there are no other candidates

            continue;
        }   //if(current.empty() || sNc==1)

        //The following could be executed more easily: test until the first element that satisfies the distinctiveness test, and keep only the failures (plausibility)- as long as the the test passes for one element
        //However, this is faster, assuming that most pairs will fail the distinctiveness test

        //If full, distinctiveness test
        score1=current.begin()->first;
        score2=current.rbegin()->first;
        if(sNc==sN)
        {
            ratio= (score2!=0) ? score2/score1 : ( (score1==0) ? 1 : 0);

            if(ratio>ratioTh )
            {
                //Test failed, discard the neighbourhood
                current.clear();
                continue;
            }   //if( (*current.rbegin())/(*current.begin()) > ratioTh )
        }   //if(sNc==sN)

        //Plausibility test. The best outsider is automatically discarded (or else, distinctiveness test would have failed)
        current.erase( partition_point( next(current.begin(),1), current.end(), plausibilityTester ) , current.end());   //Find the first candidate that fails the test. Then remove the implausible candidates

        //If anything left, update the ratio cache
        if(ratioCache && !current.empty() )
        {
            size_t sCurrent=current.size();
            auto it=current.begin();
            auto& pRatioCache=(*ratioCache)[cm1];

            //If only one element left, use the best outsider
            if(sCurrent==1)
            {
            	pRatioCache.emplace( it->second, score2/score1);
                continue;
            }

            auto itLast=prev(current.end(),1);

            //Special cases, the first, and the last elements
            pRatioCache.emplace( it->second, next(it,1)->first/score1 );
            pRatioCache.emplace( itLast->second, itLast->first/prev(itLast,1)->first);

            //for all in between
            advance(it,1);
            for(;it!=itLast; advance(it,1))
            	pRatioCache.emplace( it->second, max( (next(it,1)->first / it->first), it->first/prev(it,1)->first  ) );
        }   //if(ratioCache && !current.empty() )
    }   //for(auto& currentNeighbourhood : neighbourhoods)
}   //void ApplyRatioTest(CandidateListT& neighbourhoods, double ratioTh, size_t sN)

/**
 * @brief Applies the consistency test
 * @param[in, out] neighbourhoods12 Match candidates for the items in the first set
 * @param[in, out] neighbourhoods21 Match candidates for the items in the second set
 */
template<class ProblemT>
void NearestNeighbourMatcherC<ProblemT>::ApplyConsistencyTest(CandidateListT& neighbourhoods12, const CandidateListT& neighbourhoods21)
{
    //TODO The function requires finding elements by value (as opposed to key). Alternatives: a bimap (slower insertion?)

    //Iterate over the neighbourhoods
    size_t id1=0;   //Id of the item in the first list
    size_t id2; //Id of the item in the second list
    typename NeighbourhoodT::iterator it1;  //Iterator into the elements of neighbourhoods12 for faster deletion
    typename NeighbourhoodT::iterator it1E;
    auto ValueTester=[&](const typename NeighbourhoodT::value_type& c){return c.second==id1;};

    for(auto& current : neighbourhoods12)
    {
        //Iterate over the candidates
        it1E=current.end();
        for(it1=current.begin(); it1!=it1E;)
        {
            id2=it1->second;

            //If in the neighbourhood, continue
            if(any_of(neighbourhoods21[id2].begin(), neighbourhoods21[id2].end(), ValueTester) )
                advance(it1,1);
            else
                it1=current.erase(it1);   //Else delete

        }   //for(it1=neighbourhood.begin(); it1!=it1E;)
        ++id1;
    }   //for(auto& neighborhood : neighbourhoods12)
}   //void ApplyConsistencyTest(CandidateListT& neighbourhoods12, const CandidateListT& neighbourhoods21)

/**
 * @brief Builds the correspondence list
 * @param[in] parameters Parameters
 * @param[in] neighbourhoods12 Neighbourhoods into the second set
 * @param[in] ratioCache12 Ratio cache 1->2
 * @param[in] ratioCache21 Ratio cache 2->1
 * @param[in] flagRatioTest \c true if the ratio test is active
 * @return Correspondence list
 */
template<class ProblemT>
auto NearestNeighbourMatcherC<ProblemT>::BuildCorrespondenceList(const NearestNeighbourMatcherParametersC& parameters, const CandidateListT& neighbourhoods12, optional<RatioCacheT>& ratioCache12, optional<RatioCacheT>& ratioCache21, bool flagRatioTest)->CorrespondenceListT
{
    CorrespondenceListT output;
    typedef CorrespondenceListT::value_type CorrespondenceT;

    size_t id1=0;
    for(const auto& neighbourhood : neighbourhoods12)
    {
        for(const auto& item : neighbourhood)
        {
        	RealT ambiguity= flagRatioTest ? max( (*ratioCache12)[id1][item.second], (*ratioCache21)[item.second][id1] ) : 0;
            output.push_back( CorrespondenceT(id1, item.second, MatchStrengthC(item.first, ambiguity) ) );
        }   //for(const auto& item : neighbourhood )

        ++id1;
    }   //for(const auto& neighbourhood : neighbourhoods12)

    return output;
}   //CorrespondenceListT BuildCorrespondenceList(const CandidateListT& neighbourhoods12, const optional<RatioCacheT>& ratioCache12)

/**
 * @brief Runs the matching algorithm
 * @param[out] correspondences Correspondences
 * @param[in, out] problem Matching problem
 * @param[in] parameters Algorithm parameters
 * @return Diagnostics
 * @pre \c problem is not properly initialised
 */
template<class ProblemT>
NearestNeighbourMatcherDiagnosticsC NearestNeighbourMatcherC<ProblemT>::Run(CorrespondenceListT& correspondences, ProblemT& problem, const NearestNeighbourMatcherParametersC& parameters)
{
    correspondences.clear();
    NearestNeighbourMatcherDiagnosticsC diagnostics;

    if(!problem.IsValid())
        throw(invalid_argument("NearestNeighbourMatcherC::Run : Invalid problem"));

    ValidateParameters(parameters); //Validate the parameters

    bool flagRatioTest = parameters.ratioTestThreshold<1; // if >=1, the test is moot

    //Almost the entire computational effort is spent in this function
    //The similarity constraint is imposed in 2-stages, in a somewhat complicated way, so that the ratio test operates correctly, while the queue insertions are minimised
    CandidateListT neighbourhoods12;
    CandidateListT neighbourhoods21;
    std::tie(neighbourhoods12, neighbourhoods21)=BuildNeighbourhoods(problem, parameters, flagRatioTest);    //Build the neighbourhoods

    optional<RatioCacheT> ratioCache12=RatioCacheT(neighbourhoods12.size());
    optional<RatioCacheT> ratioCache21=RatioCacheT(neighbourhoods21.size());

    //Apply the ratio test
    if(flagRatioTest)
    {
        ApplyRatioTest(neighbourhoods12, ratioCache12, parameters.ratioTestThreshold, parameters.neighbourhoodSize12+1);
        ApplyRatioTest(neighbourhoods21, ratioCache21, parameters.ratioTestThreshold, parameters.neighbourhoodSize21+1);
    }   //if(flagRatioTest)

    //Apply the consistency check
    if(parameters.flagConsistencyTest)
        ApplyConsistencyTest(neighbourhoods12, neighbourhoods21);

    correspondences=BuildCorrespondenceList(parameters, neighbourhoods12, ratioCache12, ratioCache21, flagRatioTest);

    //Diagnostics
    diagnostics.flagSuccess=true;
    diagnostics.sSet1=neighbourhoods12.size();
    diagnostics.sSet2=neighbourhoods21.size();
    diagnostics.sCorrespondences=correspondences.size();

    return diagnostics;
}   //NearestNeighbourMatcherDiagnosticsC Run(CorrespondenceListT& correspondences, ProblemT& problem, const NearestNeighbourMatcherParametersC& parameters)

}   //MatcherN
}	//SeeSawN

#endif /* NEAREST_NEIGHBOUR_MATCHER_IPP_4319231 */
