/**
 * @file FeatureMatcher.ipp Implementation of FeatureMatcherC
 * @author Evren Imre
 * @date 28 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_MATCHER_IPP_9312575
#define FEATURE_MATCHER_IPP_9312575

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/iterator/permutation_iterator.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/lexical_cast.hpp>
#include <vector>
#include <cstddef>
#include <tuple>
#include <set>
#include <array>
#include <cmath>
#include <iterator>
#include <stdexcept>
#include <type_traits>
#include <algorithm>
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/FeatureUtility.h"
#include "../Wrappers/BoostBimap.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Wrappers/EigenMetafunction.h"
#include "NearestNeighbourMatcher.h"
#include "GroupMatchingProblem.h"
#include "FeatureMatcherProblemConcept.h"

#include <iostream>

namespace SeeSawN
{
namespace MatcherN
{

using boost::make_permutation_iterator;
using boost::make_iterator_range;
using boost::range_value;
using boost::lexical_cast;
using boost::RandomAccessRangeConcept;
using std::vector;
using std::array;
using std::size_t;
using std::tie;
using std::set;
using std::pow;
using std::advance;
using std::invalid_argument;
using std::is_same;
using std::max_element;
using std::accumulate;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::ElementsN::DecomposeFeatureRangeM;
using SeeSawN::MatcherN::NearestNeighbourMatcherParametersC;
using SeeSawN::MatcherN::NearestNeighbourMatcherDiagnosticsC;
using SeeSawN::MatcherN::NearestNeighbourMatcherC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::WrappersN::FilterBimap;
using SeeSawN::GeometryN::CoordinateStatisticsT;
using SeeSawN::GeometryN::CoordinateTransformationsT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;

/**
 * @brief Parameters of FeatureMatcherC
 * @ingroup Parameters
 * @nosubgrouping
 */
struct FeatureMatcherParametersC
{
    NearestNeighbourMatcherParametersC nnParameters;    ///< Parameters of the nearest neighbour matching problem for feature matching
    bool flagStatic;    ///< If \c true, the generated matching problem cannot be modified
    unsigned int nThreads; ///< Number of threads
    array<double,2> bucketDimensions;    ///< Size of a bucket, after the coordinates are normalised to unity variance. Each bucket is a square/cube with the specified dimensions in the normalised domain. [set1; set2]
    unsigned int minQuorum; ///< Bucket filter- minimum number of votes for a valid comparison
    double bucketSimilarityThreshold;   ///< Bucket filter- minimum similarity for a successful bucket match. [0,1]
    bool flagBucketFilter;  ///< If \c false , the bucket filter is disabled

    /** @brief Default constructor*/
    FeatureMatcherParametersC() : flagStatic(true), nThreads(1), bucketDimensions{{0.2, 0.2}}, minQuorum(0), bucketSimilarityThreshold(0.5), flagBucketFilter(true)
    {}

};  //struct FeatureMatcherParameterC

/**
 * @brief Diagnostics for FeatureMatcherC
 * @ingroup Diagnostics
 * @nosubgrouping
 */
struct FeatureMatcherDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm is successfully completed
    NearestNeighbourMatcherDiagnosticsC nnDiagnostics;  ///< Diagnostics for the feature matcher
    NearestNeighbourMatcherDiagnosticsC bfDiagnostics;  ///< Diagnostics for the bucket filter
    unsigned int sCorrespondences;  ///< Number of correspondences after the bucket filter
};  //struct FeatureMatcherDiagnosticsC

/**
 * @brief Generic feature matcher
 * @tparam ProblemT A feature matcher problem
 * @pre \c ProblemT is a model of FeatureMatcherProblemConceptC
 * @remarks Algorithm flow
 *          - Apply nearest neighbour matching
 *          - Apply the bucket filter
 * @remarks In successive applications of the matcher (e.g. within the context of estimation), the problem object initialised in the first pass can be reused.
 * @remarks Bucket filter:
 *          - Leverages the observation that, if two features sit in the same bucket, their correspondences are likely to be in the same, or nearby buckets. A feature whose correspondence is in a bucket far from the rest is likely to be an outlier.
 *          - Useful for weeding out the outliers in N:N matching
 *          - A bucket match hypothesis is rejected only if there are sufficiently many members in one of the buckets (minQuorum). This preserves the feature correspondences in the feature-poor regions: if a bucket match hypothesis is not rejected, the relevant memeber correspondences are retained
 *          - The similarity score for a bucket pair is 2*votes for the bucket pair/(total votes for each bucket)
 *          - If the matching is not 1:1, feature correspondences do not have equal votes. The strength of the vote for a feature correspondence is the sum of the weights for the individual features. The weight for a feature is the reciprocal of the number of corresponding feature pairs it appears in.
 *          - In each neighbourhood, there is only one correct match. Therefore, the similarity threshold is divided by the average neighbourhood size, to counter the ambiguity introduced by the false matches.
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class ProblemT>
class FeatureMatcherC
{
    //Preconditions
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<ProblemT>));
    //@endcond

    private:

        typedef typename ProblemT::real_type RealT; ///< A floating point type
        typedef typename ProblemT::feature_type1 Feature1T;   ///< A feature in the first set
        typedef typename ProblemT::feature_type2 Feature2T;   ///< A feature in the second set
        typedef typename ProblemT::similarity_type SimilarityT; ///< A similarity metric
        typedef typename ProblemT::constraint_type ConstraintT; ///< A constraint
        typedef typename Feature1T::coordinate_type Coordinate1T;   ///< A coordinate in the first feature set
        typedef typename Feature2T::coordinate_type Coordinate2T;   ///< A coordinate in the second feature set

        /**@name Implementation details */ //@{
        static void ValidateParameters(const FeatureMatcherParametersC& parameters);  ///< Validates the parameters

        typedef vector<unsigned int> MembershipT;  ///< List of groups with which a feature is associated
        template<class FeatureRangeT> static optional<vector<MembershipT> > AssignBuckets(const FeatureRangeT& features, const vector<size_t>& indexList, RealT bucketSize);    ///< Assigns the coordinates to buckets
        static vector<double> AssignWeights(const vector<size_t>& indexList1, const vector<size_t>& indexList2);	///< Assigns weights to each correspondence
        static NearestNeighbourMatcherDiagnosticsC ApplyBucketFilter(vector<size_t>& output, const vector<MembershipT>& bucketised1, const vector<MembershipT>& bucketised2, const vector<double>& weights, size_t minQuorum, RealT similarityThreshold, size_t dim1, size_t dim2); ///< Applies the bucket filter
        //@}

    public:

        template<class FeatureRange1T, class FeatureRange2T> static FeatureMatcherDiagnosticsC Run(CorrespondenceListT& correspondences, optional<ProblemT>& problem, const FeatureMatcherParametersC& parameters, const SimilarityT& similarityMetric, const optional<ConstraintT>& constraint, const FeatureRange1T& features1, const FeatureRange2T& features2); ///<Runs the matching algorithm
};  //class FeatureMatcherC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Validates the parameters
 * @param[in] parameters Parameters
 * @throws invalid_argument If \c parameters.bucketSimilarityThreshold is not in [0,1]
 * @throws invalid_argument If \c parameters.bucketDimensions<=0
 */
template<class ProblemT>
void FeatureMatcherC<ProblemT>::ValidateParameters(const FeatureMatcherParametersC& parameters)
{
    if(parameters.bucketSimilarityThreshold<0 || parameters.bucketSimilarityThreshold>1)
        throw(invalid_argument(string("FeatureMatcherC::ValidateParameters : parameters.bucketSimilarityThreshold must be within [0,1], not ") + lexical_cast<string>(parameters.bucketSimilarityThreshold) ));

    if(parameters.bucketDimensions[0]<=0 || parameters.bucketDimensions[1]<=0)
        throw(invalid_argument(string("FeatureMatcherC::ValidateParameters : parameters.bucketDimensions must be positive values, not ") + lexical_cast<string>(parameters.bucketDimensions[0]) + string(";") + lexical_cast<string>(parameters.bucketDimensions[1]) ) );
}   //void ValidateParameters(const FeatureMatcherParametersC& parameters)

/**
 * @brief Assigns the coordinates to buckets
 * @tparam FeatureRangeT A feature range
 * @param[in] features Feature set
 * @param[in] indexList Feature indices for which there is a correspondence
 * @param[in] bucketSize Bucket size
 * @return A vector of bucket memberships. Invalid if all correspondences have the same coordinates
 * @pre \c indexList.size()<=features.size()
 */
template<class ProblemT>
template<class FeatureRangeT>
auto FeatureMatcherC<ProblemT>::AssignBuckets(const FeatureRangeT& features, const vector<size_t>& indexList, RealT bucketSize)-> optional<vector<MembershipT> >
{
    //Preconditions
    assert(boost::distance(features)==indexList.size());

    typedef typename DecomposeFeatureRangeM<FeatureRangeT>::coordinate_type CoordinateT;

    //Get the coordinates
    auto itB=make_permutation_iterator(boost::begin(features), indexList.begin());
    auto itE=make_permutation_iterator(boost::end(features), indexList.end());
    vector<CoordinateT> coordinates=MakeCoordinateVector(make_iterator_range(itB, itE));

    //Normalise & get the bounding box
    typedef CoordinateTransformationsT<CoordinateT> TransformationsT;
    typedef CoordinateStatisticsT<CoordinateT> StatisticsT;

    typename TransformationsT::affine_transform_type mN=TransformationsT::ComputeNormaliser(coordinates);   //Normalising transformation
    if(!mN.matrix().allFinite())
    	return optional<vector<MembershipT> >();

    vector<CoordinateT> normalised=TransformationsT::ApplyAffineTransformation(coordinates, mN);  //Normalise
    typename StatisticsT::ArrayD2 extent=*StatisticsT::ComputeExtent(normalised);   //Extent of the point cloud

    //Assign to buckets
    typename TransformationsT::ArrayD bucketDimensions; bucketDimensions.setConstant(bucketSize);

    optional<vector<MembershipT> > output=TransformationsT::Bucketise(normalised, bucketDimensions, extent, true);   //Overlapping buckets: each feature is assigned up to 4 buckets
    return output;
}   //vector<MembershipT> AssignBuckets(const CoordinateRangeT& coordinates)

/**
 * @brief Assigns weights to each correspondence
 * @param[in] indexList1 Corresponding features in the first set
 * @param[in] indexList2 Corresponding features in the second set
 * @return A vector of weights
 * @remarks An index mapped to multiple indices should have a lower weight per correspondence. So, the weight for an index is the reciprocal of its number of occurrence.
 * The weight for a correspondence is the sum of the weights of the constituent features
 */
template<class ProblemT>
vector<double> FeatureMatcherC<ProblemT>::AssignWeights(const vector<size_t>& indexList1, const vector<size_t>& indexList2)
{
	auto max1=max_element(indexList1.begin(), indexList1.end());
	vector<unsigned int> histogram1(*max1+1,0);

	auto max2=max_element(indexList2.begin(), indexList2.end());
	vector<unsigned int> histogram2(*max2+1,0);

	size_t nCorrespondence=indexList1.size();
	for(size_t c=0; c<nCorrespondence; ++c)
	{
		++histogram1[indexList1[c]];
		++histogram2[indexList2[c]];
	}	//for(size_t c=0; c<nCorrespondence; ++c)

	vector<double> weights(nCorrespondence,0);
	for(size_t c=0; c<nCorrespondence; ++c)
		weights[c]= 0.5/histogram1[indexList1[c]] + 0.5/histogram2[indexList2[c]];

	return weights;
}	//vector<double> AssignWeights(const vector<size_t>& indexList1, const vector<size_t>& indexList2)

/**
 * @brief Applies the bucket filter
 * @param[out] output Correspondence indices that survived the bucket filter
 * @param[in] bucketised1 Memberships for the features in the first set. Each cell is a list of buckets to which the feature belongs
 * @param[in] bucketised2 Memberships for the corresponding features in the second set.
 * @param[in] weights Weights for each correspondence
 * @param[in] minQuorum Minimum number of votes in a bucket for a valid match
 * @param[in] similarityThreshold Similarity threshold for the bucket filter
 * @param[in] dim1 Dimensionality of a coordinate in the first set
 * @param[in] dim2 Dimensionality of a coordinate in the second set
 * @return Diagnostics for the bucket filter
 */
template<class ProblemT>
NearestNeighbourMatcherDiagnosticsC FeatureMatcherC<ProblemT>::ApplyBucketFilter(vector<size_t>& output, const vector<MembershipT>& bucketised1, const vector<MembershipT>& bucketised2, const vector<double>& weights, size_t minQuorum, RealT similarityThreshold, size_t dim1, size_t dim2)
{
    output.clear();
    NearestNeighbourMatcherParametersC parameters;
    parameters.flagConsistencyTest=false; // When there is a scale change, multiple regions can map to a single region
    parameters.neighbourhoodSize12=pow((float)2, (float)dim1);  // Due to 50% overlap, each item affects 2^dim buckets
    parameters.neighbourhoodSize21=pow((float)2, (float)dim2);  // Due to 50% overlap, each item affects 2^dim buckets
    parameters.ratioTestThreshold=0.99;

    //Similarity threshold is divided by the average neighbourhood size
    double nDistinct=accumulate(weights.begin(), weights.end(), 0.0);	//By construction, sum of the weights is equal to the total number of distinct indices in the correspondences, divided by 2
    double nAvgNeighbourhoodSize=bucketised1.size()/nDistinct;
    parameters.similarityTestThreshold=similarityThreshold/nAvgNeighbourhoodSize;

    GroupMatchingProblemC problem(bucketised1, bucketised2, weights, minQuorum, MembershipT());

    CorrespondenceListT correspondences;
    NearestNeighbourMatcherDiagnosticsC diagnostics=NearestNeighbourMatcherC<GroupMatchingProblemC>::Run(correspondences, problem, parameters);

    output=problem.FilterInput<MembershipT>(correspondences, bucketised1, bucketised2);

    return diagnostics;
}   //vector<size_t> ApplyBucketFilter(const vector<MembershipT>& bucketised1, const vector<MembershipT>& bucketised2, double similarityThreshold)

/**
 * @brief Runs the matching algorithm
 * @tparam FeatureRange1T A feature range
 * @tparam FeatureRange2T A feature range
 * @param[out] correspondences Indices of the corresponding features
 * @param[in, out] problem Matching problem. If invalid at the input, initialised internally
 * @param[in] parameters Parameters
 * @param[in] similarityMetric Similarity metric for the feature matcher. Ignored if there is an externally supplied problem
 * @param[in] constraint Constraint for the feature matcher. An invalid argument indicates no constraint. Ignored if there is an externally supplied problem
 * @param[in] features1 First feature set
 * @param[in] features2 Second feature set
 * @return Diagnostics
 * @pre If there is a problem, it must be valid
 * @pre \c FeatureRange1T is a random access range holding values of type \c ProblemT::feature_type1
 * @pre \c FeatureRange2T is a random access range holding values of type \c ProblemT::feature_type2
 */
template<class ProblemT>
template<class FeatureRange1T, class FeatureRange2T>
FeatureMatcherDiagnosticsC FeatureMatcherC<ProblemT>::Run(CorrespondenceListT& correspondences, optional<ProblemT>& problem, const FeatureMatcherParametersC& parameters, const SimilarityT& similarityMetric, const optional<ConstraintT>& constraint, const FeatureRange1T& features1, const FeatureRange2T& features2)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<FeatureRange1T>));
    BOOST_CONCEPT_ASSERT((RandomAccessRangeConcept<FeatureRange2T>));
    static_assert(is_same< typename range_value<FeatureRange1T>::type, typename ProblemT::feature_type1 >::value, "FeatureMatcherC::Run : FeatureRange1T must hold elements of type ProblemT::feature_type1" );
    static_assert(is_same< typename range_value<FeatureRange2T>::type, typename ProblemT::feature_type2 >::value, "FeatureMatcherC::Run : FeatureRange2T must hold elements of type ProblemT::feature_type2" );

    if(problem && !problem->IsValid())
        throw(invalid_argument("FeatureMatcherC::Run : Invalid problem"));

    ValidateParameters(parameters);

    correspondences.clear();
    FeatureMatcherDiagnosticsC diagnostics;

    //If there is not an externally-supplied problem, construct a new one
    if(!problem)
    {
        problem=ProblemT();
        problem->Initialise(similarityMetric, constraint, features1, features2, parameters.flagStatic, parameters.nThreads);
    }   //if(!problem)

    //Nearest neighbour matcher
    CorrespondenceListT nnCorrespondences;
    typedef NearestNeighbourMatcherC<ProblemT> MatcherT;
    diagnostics.nnDiagnostics=MatcherT::Run(nnCorrespondences, *problem, parameters.nnParameters);

    if(!diagnostics.nnDiagnostics.flagSuccess)
    {
    	diagnostics.flagSuccess=false;
    	return diagnostics;
    }	//if(!diagnostics.nnDiagnostics.flagSuccess)
    else
    	diagnostics.flagSuccess=true;

    //Bucket filter
    if(parameters.flagBucketFilter && nnCorrespondences.size()>1)	//If a single correspondence, no bucket filter is needed
    {
        vector<size_t> indexList1;
        vector<size_t> indexList2;
        tie(indexList1, indexList2)=BimapToVector<size_t, size_t>(nnCorrespondences);

        optional<vector<MembershipT> > bucketised1=AssignBuckets(features1, indexList1, parameters.bucketDimensions[0]);
        optional<vector<MembershipT> > bucketised2=AssignBuckets(features2, indexList2, parameters.bucketDimensions[1]);

        if(bucketised1 && bucketised2)
        {
			vector<double> weights=AssignWeights(indexList1, indexList2);

			unsigned int dim1=features1.begin()->Coordinate().size();
			unsigned int dim2=features2.begin()->Coordinate().size();
			vector<size_t> bfIndexList;
			diagnostics.bfDiagnostics=ApplyBucketFilter(bfIndexList, *bucketised1, *bucketised2, weights, parameters.minQuorum, parameters.bucketSimilarityThreshold, dim1, dim2);

			if(diagnostics.bfDiagnostics.flagSuccess)
				correspondences=FilterBimap(nnCorrespondences, bfIndexList);  //Filter nnCorrespondences with bfIndexList
        }	//if(bucketised1 && bucketised2)
    }   //if(parameters.flagBucketFilter)

    //Bucket filter not applied
    if(!diagnostics.bfDiagnostics.flagSuccess)
        correspondences.swap(nnCorrespondences);

    diagnostics.sCorrespondences=correspondences.size();

    return diagnostics;
}   //FeatureMatcherDiagnosticsC Run(CorrespondenceListT& correspondences, optional<ProblemT>& problem, const FeatureMatcherParametersC& parameters, SimilarityT& similarityMetric, optional<ConstraintT>& constraint, const FeatureRange1T& features1, const FeatureRange2T& features2, const optional<TransformT>& projectionOperator);
}   //MatcherN
}	//SeeSawN

#endif /* FEATURE_MATCHER_IPP_9312575 */
