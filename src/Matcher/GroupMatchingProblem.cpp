/**
 * @file GroupMatchingProblem.cpp Instantiations of GroupMatchingProblemC
 * @author Evren Imre
 * @date 26 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "GroupMatchingProblem.h"
namespace SeeSawN
{
namespace MatcherN
{

/**
 * @brief Dummy constructor
 * @post The object is invalid
 */
GroupMatchingProblemC::GroupMatchingProblemC() :  minQuorum(0), flagValid(false)
{}

/**
 * @brief Returns the number of elements in the first set
 * @return \c scoreArray.rows()
 */
size_t GroupMatchingProblemC::GetSize1() const
{
    return scoreArray.rows();
}   //size_t GetSize1() const

/**
 * @brief Returns the number of elements in the second set
 * @return \c scoreArray.cols()
 */
size_t GroupMatchingProblemC::GetSize2() const
{
    return scoreArray.cols();
}   //size_t GetSize2() const

/**
 * @brief Computes the similarity score for the specified elements
 * @param[in] i1 Index into the first set
 * @param[in] i2 Index into the second set
 * @return Score, if the quorum constraint is satisfied
 */
optional<float> GroupMatchingProblemC::ComputeSimilarity(size_t i1, size_t i2)
{
	return (histogram1[i1]>=minQuorum || histogram2[i2]>=minQuorum) ? optional<float>(scoreArray(i1,i2) ) : optional<float>();
}   //optional<double> ComputeSimilarity(size_t i1, size_t i2)

/**
 * @brief  \c true if the problem is valid
 * @return \c flagValid
 */
bool GroupMatchingProblemC::IsValid() const
{
    return flagValid;
}   //bool IsValid() const

/**
 * @brief Clears the state
 * @post The state members are cleared, and \c flagValid set to false
 */
void GroupMatchingProblemC::Clear()
{
    scoreArray.resize(0,0);
    histogram1.clear(); histogram1.shrink_to_fit();
    histogram2.clear(); histogram2.shrink_to_fit();
    indexMap1.clear(); indexMap1.shrink_to_fit();
    indexMap2.clear(); indexMap2.shrink_to_fit();
    flagValid=false;
}   //void Clear()


/********** EXPLICIT INSTANTIATIONS **********/
template GroupMatchingProblemC::GroupMatchingProblemC(const vector<set<unsigned> >&, const vector<set<unsigned> >&, const vector<double>&,  unsigned int, const set<unsigned int>&);
}   //MatcherN
}	//SeeSawN


