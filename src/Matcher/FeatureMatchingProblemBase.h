/**
 * @file FeatureMatchingProblemBase.h Public interface for \c FeatureMatchingProblemBaseC
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_MATCHING_PROBLEM_BASE_H_6420931
#define FEATURE_MATCHING_PROBLEM_BASE_H_6420931

#include "FeatureMatchingProblemBase.ipp"
namespace SeeSawN
{
namespace MatcherN
{
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT> class FeatureMatchingProblemBaseC;	///< Base class for feature matching problems

}	//MatcherN
}	//SeeSawN

/********** EXTERN TEMPLATES *********/
extern template class boost::optional<double>;
extern template class boost::optional<float>;
#endif /* FEATURE_MATCHING_PROBLEM_BASE_H_6420931 */
