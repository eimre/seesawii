/**
 * @file FeatureMatchingProblemBase.ipp
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_MATCHING_PROBLEM_BASE_IPP_5132093
#define FEATURE_MATCHING_PROBLEM_BASE_IPP_5132093

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/algorithm.hpp>
#include <Eigen/Dense>
#include <vector>
#include <type_traits>
#include <climits>
#include <stdexcept>
#include <omp.h>
#include <cmath>
#include "../Elements/FeatureConcept.h"
#include "../Elements/FeatureUtility.h"
#include "../Metrics/InvertibleBinarySimilarityConcept.h"
#include "../Metrics/BinaryConstraintConcept.h"
#include "../Metrics/MatchAmbiguityMetric.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace MatcherN
{

using boost::DefaultConstructibleConcept;
using boost::AssignableConcept;
using boost::ForwardRangeConcept;
using boost::optional;
using boost::range_value;
using boost::range::for_each;
using Eigen::Array;
using std::vector;
using std::is_floating_point;
using std::is_same;
using std::numeric_limits;
using std::logic_error;
using std::max;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::ElementsN::MakeCoordinateVector;
using SeeSawN::MetricsN::InvertibleBinarySimilarityConceptC;
using SeeSawN::MetricsN::BinaryConstraintConceptC;
using SeeSawN::MetricsN::RayleighMetaRecognitionMetricC;
using SeeSawN::GeometryN::CoordinateTransformationsC;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;

/**
 * @brief Base class for feature matching problems
 * @tparam Feature1T Type of the features in the first set
 * @tparam Feature2T Type of the features in the second set
 * @tparam SimilarityT Type of the feature similarity metric
 * @tparam ConstraintT Type of the matching constraint
 * @pre \c Feature1T satisfies \c FeatureConceptC
 * @pre \c Feature2T satisfies \c FeatureConceptC
 * @pre \c SimilarityT is assignable and default constructible
 * @pre \c SimilarityT satisfies \c InvertibleBinarySimilarityConceptC
 * @pre \c ConstraintT is assignable and default constructible
 * @pre \c ConstraintT satisfies \c BinaryConstraintConceptC
 * @remarks The problem can be initialised for static operation,  for the cases where the constraint will not be modified later on. Static operation mode saves memory.
 *          However a problem initialised this way cannot be assigned a constraint outside of the constructor. So, it is not suitable for guided matching.
 * @remarks Similarity score-based ambiguity is not necessarily an indicator of quality in the case of guided matching. Geometric fitness works better.
 * @remarks Tested through \c ImageFeatureMatchingProblemC
 * @ingroup Problem
 * @nosubgrouping
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
class FeatureMatchingProblemBaseC
{

	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((FeatureConceptC<Feature1T>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<Feature2T>));
	BOOST_CONCEPT_ASSERT((DefaultConstructibleConcept<SimilarityT>));
	BOOST_CONCEPT_ASSERT((AssignableConcept<SimilarityT>));
	///@endcond

	private:

		typedef typename Feature1T::coordinate_type Coordinate1T;	///< Type of the coordinate for a feature in the first set
		typedef typename Feature1T::descriptor_type Descriptor1T;	///< Type of the descriptor for a feature in the first set

		typedef typename Feature2T::coordinate_type Coordinate2T;	///< Type of the coordinate for a feature in the second set
		typedef typename Feature2T::descriptor_type Descriptor2T;	///< Type of the descriptor for a feature in the second set

		///@cond CONCEPT_CHECK
		BOOST_CONCEPT_ASSERT((InvertibleBinarySimilarityConceptC<SimilarityT, typename SimilarityT::result_type, Descriptor1T, Descriptor2T>));
		BOOST_CONCEPT_ASSERT((BinaryConstraintConceptC<ConstraintT, Coordinate1T, Coordinate2T>));
		static_assert(is_floating_point<typename SimilarityT::result_type>::value, "FeatureMatchingProblemBaseC: SimilarityT::result_type must be a floating point value.");
		///@endcond

        typedef typename SimilarityT::result_type RealT; ///< A floating point type

        /**@name Configuration */ //@{
        SimilarityT similarityMetric; ///< Similarity metric
        optional<ConstraintT> constraint; ///< Matching constraint
        optional<vector<Feature1T> > set1; ///< First feature set
        optional<vector<Feature2T> > set2; ///< Second feature set
        //@}

        /**@name State */ //@{
		typedef Array<RealT, Eigen::Dynamic, Eigen::Dynamic> RealArrayT; ///< A real array
		RealArrayT similarityCache;   ///< Cache of already computed similarity scores

		typedef Array<bool, Eigen::Dynamic, Eigen::Dynamic> BoolArrayT;   ///< A bool array
		BoolArrayT similarityValidityArray;   ///< Indicates whether the similarity score for a pair is already calculated
		optional<BoolArrayT> constraintCache;    ///< Cache for constraint evaluations
		//@}

		/**@name Implementation details */ //@{
		template<class FeatureT, class TransformationT> void ApplyCoordinateTransformation(optional<vector<FeatureT> >& featureSet, const TransformationT& transformation);	///< Applies a coordinate transformation to a feature set

		template<class FeatureRangeT> void SimilarityCacheInnerLoopST( const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const BoolArrayT& cconstraintCache, const Feature2T& feature2, size_t index2, const FeatureRangeT& set1);   ///< Inner loop of the initialisation of the similarity cache, single-threaded operation

		typedef Array<bool, Eigen::Dynamic, 1> ArrayXbT;
		typedef Array<RealT, Eigen::Dynamic, 1> ArrayXrT;
		template<class FeatureRangeT> void SimilarityCacheInnerLoopMT(ArrayXbT& validityArray, ArrayXrT& similarityArray, const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const BoolArrayT& cconstraintCache, const Feature2T& feature2,  size_t index2, const FeatureRangeT& set1);   ///< Inner loop of the initialisation of the similarity cache, multithreaded operation
		//@}

	protected:
		bool flagStatic;    ///< \c true if the problem is in static operation mode
		bool flagValid; ///< \c true if the problem is initialised

	public:

		/** @name Constructors */ //@{
		template<class FeatureRange1T, class FeatureRange2T> FeatureMatchingProblemBaseC(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const FeatureRange1T& sset1, const FeatureRange2T& sset2, bool fflagStatic, unsigned int nThreads);   ///< Constructor
		//@}

		/**@name NearestNeighbourMatchingProblemConceptC interface */ //@{
		typedef RealT real_type;    ///< Value type of the similarity, not the coordinate!

		FeatureMatchingProblemBaseC();   ///< Default constructor
		size_t GetSize1() const;  ///< Returns the number of elements in the first set
		size_t GetSize2() const;  ///< Returns the number of elements in the second set
		optional<RealT> ComputeSimilarity(size_t i1, size_t i2);   ///< Computes the similarity score for the specified elements
		bool IsValid() const; ///< \c true if the problem is valid
		//@}

        /** @name FeatureMatchingProblemConceptC interaface*/ //@{
        typedef Feature1T feature_type1;
        typedef Feature2T feature_type2;
        typedef SimilarityT similarity_type;
        typedef ConstraintT constraint_type;
        template<class FeatureRange1T, class FeatureRange2T> void Initialise(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const FeatureRange1T& sset1, const FeatureRange2T& sset2, bool fflagStatic, unsigned int nThreads);   ///< Initialises a problem
        template<class CorrespondenceRangeT> void AssessAmbiguity(CorrespondenceRangeT& correspondences, unsigned int nThreads=1, unsigned int sSample=10) const;	///< Computes an ambiguity metric to evaluate the reliability of correspondences

        template<class TransformationT> void ApplyCoordinateTransformation1(const TransformationT& transformation);	///< Applies a coordinate transformation to the first feature set
        template<class TransformationT> void ApplyCoordinateTransformation2(const TransformationT& transformation);	///< Applies a coordinate transformation to the second feature set
        //@}

        /**@name Accessors */ //@{
        const optional<ConstraintT>&  GetConstraint() const;    ///< Getter \c for constraint
        void  SetConstraint(const optional<ConstraintT>& cconstraint);    ///< Setter \c for constraint
        //@}
};	//class FeatureMatchingProblemBaseC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Inner loop of the initialisation of the similarity cache, single-threaded operation
 * @param[in] ssimilarityMetric Similarity metric
 * @param[in] cconstraint Constraint
 * @param[in] cconstraintCache Constraint cache
 * @param[in] feature2 A feature in the second set
 * @param[in] index2 Index of \c feature2 in the second set
 * @param[in] set1 First feature set
 * @pre \c FeatureRangeT is a forward range of \c Feature1T
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class FeatureRangeT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::SimilarityCacheInnerLoopST( const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const BoolArrayT& cconstraintCache, const Feature2T& feature2, size_t index2, const FeatureRangeT& set1)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
    static_assert(is_same< typename range_value<FeatureRangeT>::type, Feature1T >::value, "FeatureMatchingProblemBaseC::SimilarityCacheInnerLoopST : FeatureRangeT must hold elements of type Feature1T");

    RealT minScore=-numeric_limits<RealT>::infinity();

    //Iterate over the first set
    size_t c1=0;
    for(const auto& feature1 : set1)
    {
        //If no applicable constraint, or the constraint is met, compute the score. Else, set to minus infinity
        similarityValidityArray(c1, index2)= (!cconstraint) || (cconstraint && cconstraintCache(c1,index2));
        similarityCache(c1, index2)=similarityValidityArray(c1, index2)  ? ssimilarityMetric(feature1.Descriptor(), feature2.Descriptor()) : minScore;
        ++c1;
    }   //for(const auto& feature1 : sset1)
}   //void SimilarityCacheInnerLoopST( SimilarityT& ssimilarityMetric, optional<ConstraintT>& cconstraint, TriboolArrayT& cconstraintCache, const Feature2T& feature2, size_t index2, const FeatureRangeT& set1)

/**
 * @brief Inner loop of the initialisation of the similarity cache, multithreaded operation
 * @param[out] validityArray Similarity score validity array
 * @param[out] similarityArray Similarity scores
 * @param[in] ssimilarityMetric Similarity metric
 * @param[in] cconstraint Constraint
 * @param[in] cconstraintCache Constraint cache
 * @param[in] feature2 A feature in the second set
 * @param[in] index2 Index of \c feature2 in the second set
 * @param[in] set1 First feature set
 * @pre ssimilarityMetric and constraint are thread-safe (unenforced)
 * @pre \c FeatureRangeT is a forward range of \c Feature1T
 * @remarks Stores the results in temporary arrays, to prevent a concurrent write
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class FeatureRangeT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::SimilarityCacheInnerLoopMT(ArrayXbT& validityArray, ArrayXrT& similarityArray, const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const BoolArrayT& cconstraintCache, const Feature2T& feature2,  size_t index2, const FeatureRangeT& set1)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
    static_assert(is_same< typename range_value<FeatureRangeT>::type, Feature1T >::value, "FeatureMatchingProblemBaseC::SimilarityCacheInnerLoopMT : FeatureRangeT must hold elements of type Feature1T");

    RealT minScore=-numeric_limits<RealT>::infinity();

    //Iterate over the elements of the first feature set
    size_t c1=0;
    for(const auto& feature1 : set1)
    {
        //If no applicable constraint, or the constraint is met, compute the score. Else, set to minus infinity
        validityArray(c1)= (!cconstraint) || (cconstraint && cconstraintCache(c1,index2));
        similarityArray(c1)=validityArray(c1)  ? ssimilarityMetric(feature1.Descriptor(), feature2.Descriptor()) : minScore;
        ++c1;
    }   //for(const auto& feature1 : sset1)
}   //void SimilarityCacheInnerLoopMT( ArrayXtT& constraintArray, ArrayXbT& validityArray, ArrayXdT& similarityArray, SimilarityT& ssimilarityMetric, optional<ConstraintT>& cconstraint, const Feature2T& feature2,  const FeatureRangeT& set1);   ///< Inner loop of the initialisation of the similarity cache, multithreaded operation

/**
 * @brief Default constructor
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::FeatureMatchingProblemBaseC()
{
    flagValid=false;
    flagStatic=true;
}   //ImageFeatureMatchingProblemC()

/**
 * @brief Constructor
 * @tparam FeatureRange1T A range of features for the first set
 * @tparam FeatureRange2T A range of features for the second set
 * @param[in] ssimilarityMetric Similarity metric
 * @param[in] cconstraint Matching constraint. An invalid input signifies no constraints
 * @param[in] sset1 First feature set
 * @param[in] sset2 Second feature set
 * @param[in] fflagStatic Similarity score and constraint evaluation will be performed only once, in the constructor. If \c true, the problem state is composed of only the similarity cache. Not suitable for guided matching applications
 * @param[in] nThreads Number of threads, for multihreaded operation
 * @remarks If \c flagFixed=true , setting a constraint later on is not possible
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class FeatureRange1T, class FeatureRange2T>
FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::FeatureMatchingProblemBaseC(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const FeatureRange1T& sset1, const FeatureRange2T& sset2, bool fflagStatic, unsigned int nThreads)
{
    Initialise(ssimilarityMetric, cconstraint, sset1, sset2, fflagStatic, nThreads);
}   //ImageFeatureMatchingProblemC(SimilarityT& ssimilarityMetric, optional<ConstraintT>& cconstraint, const FeatureRangeT& sset1, const FeatureRangeT& sset2, bool flagOneShot)

/**
 * @brief Returns the number of elements in the first set
 * @return Number of rows in \c similarityCache
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
size_t FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::GetSize1() const
{
    return similarityCache.rows();
}   //size_t GetSize1()

/**
 * @brief Returns the number of elements in the second set
 * @return Number of columns in \c similarityCache
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
size_t FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::GetSize2() const
{
    return similarityCache.cols();
}   //size_t GetSize2()

/**
 * @brief \c true if the problem is valid
 * @return \c flagValid
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
bool FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::IsValid() const
{
    return flagValid;
}   //bool IsValid()

/**
 * @brief Computes the similarity score for the specified elements
 * @param[in] i1 Index of the first feature
 * @param[in] i2 Index of the second feature
 * @return If the constraint is satisfied, a valid similarity score. Otherwise, an invalid object
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
auto FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::ComputeSimilarity(size_t i1, size_t i2)  -> optional<RealT>
{
    optional<RealT> score;

    //If static operation, use the cache
    if(flagStatic)
    {
        //An invalid score implies a constraint failure at the constructor
        if(similarityValidityArray(i1,i2))
            score=similarityCache(i1,i2);
    }   //if(!set1)
    else
    {
        bool flagConstraintSuccess = !constraint  || (constraint && (*constraintCache)(i1, i2) );   //If no constraint, automatic success. Else, check the constraint cache

        //If the constraint is satisfied, get the similarity score
        if(flagConstraintSuccess)
        {
            //Do we need to compute the similarity score?
            if(!similarityValidityArray(i1,i2))
            {
                similarityCache(i1,i2) = similarityMetric((*set1)[i1].Descriptor(), (*set2)[i2].Descriptor());
                similarityValidityArray(i1,i2)=true;
            }   //if(!similarityValidityArray(i1,i2))

            score=similarityCache(i1,i2);
        }   //if(!constraint || (*constraintCache)(i1, i2) )
    }   //if(!set1)

    return score;
}   //optional<double> ComputeSimilarity(size_t i1, size_t i2)

/**
 * @brief Getter \c for constraint
 * @return Returns a constant reference to the constraint
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
const optional<ConstraintT>& FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::GetConstraint() const
{
    return constraint;
}   //const optional<ConstraintT>&  GetConstraint() const

/**
 * @brief Setter \c for constraint
 * @param[in] cconstraint New constraint
 * @pre The object is initialised for the static operation mode
 * @throws logic_error If the object is initalised for the static operation mode
 * @post Overwrites \c constraint and reinitialises \c constraintCache
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::SetConstraint(const optional<ConstraintT>& cconstraint)
{
    //If static mode operation, features are not stored, and the constraint cannot be evaluated
    if(flagStatic)
        throw(logic_error("FeatureMatchingProblemBaseC::SetConstraint : This operation cannot be performed in the static operation mode.") );

    constraint=cconstraint;

    if(constraint)
    {
		vector<Coordinate1T> coordinates1=MakeCoordinateVector(*set1);
		vector<Coordinate2T> coordinates2=MakeCoordinateVector(*set2);
		constraintCache=constraint->BatchConstraintEvaluation(coordinates1, coordinates2);
    }
    else
    	constraintCache=optional<BoolArrayT>();
}   //void  SetConstraint(const optional<ConstraintT>& cconstraint)

/**
 * @brief Initialises a problem
 * @tparam FeatureRange1T A range of features for the first set
 * @tparam FeatureRange2T A range of features for the second set
 * @param[in] ssimilarityMetric Similarity metric
 * @param[in] cconstraint Matching constraint. An invalid input signifies no constraints
 * @param[in] sset1 First feature set
 * @param[in] sset2 Second feature set
 * @param[in] fflagStatic Similarity score and constraint evaluation will be performed only once, in the constructor. If \c true, the problem state is composed of only the similarity cache. Not suitable for guided matching applications
 * @param[in] nThreads Number of threads, for multithreaded operation
 * @pre If \c nThreads>1 \c ssimilarityMetric and \c constraint are thread-safe (unenforced)
 * @pre \c FeatureRange1T is a forward range of \c Feature1T
 * @pre \c FeatureRange2T is a forward range of \c Feature2T
 * @pre \c flagValid=false
 * @throws logic_error If the object is already initialised
 * @remarks If \c flagFixed=true , setting a constraint later on is not possible
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class FeatureRange1T, class FeatureRange2T>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::Initialise(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const FeatureRange1T& sset1, const FeatureRange2T& sset2, bool fflagStatic, unsigned int nThreads)
{
    //Preconditions
    if(flagValid)
        throw(logic_error("FeatureMatchingProblemBaseC::Initialise : Initialise cannot be called on an already initialised object"));

    nThreads=max((unsigned int)1, nThreads);

    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRange1T>));
    static_assert(is_same< typename range_value<FeatureRange1T>::type, Feature1T >::value, "FeatureMatchingProblemBaseC::Initialise : FeatureRange1T must hold elements of type Feature1T");
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRange2T>));
    static_assert(is_same< typename range_value<FeatureRange2T>::type, Feature2T >::value, "FeatureMatchingProblemBaseC::Initialise : FeatureRange2T must hold elements of type Feature2T");

    flagStatic=fflagStatic;

    size_t nFeatures1=boost::distance(sset1);
	size_t nFeatures2=boost::distance(sset2);
    Array<bool, 1, Eigen::Dynamic> flagSet2Viability(nFeatures2); flagSet2Viability.setConstant(true);	//If false, the corresponding item does not satisfy the constraint in any pair

    //Initialise the constraint
    BoolArrayT cconstraintCache;
    if(cconstraint)
    {
        vector<Coordinate1T> coordinates1=MakeCoordinateVector(sset1);
        vector<Coordinate2T> coordinates2=MakeCoordinateVector(sset2);
        cconstraintCache=cconstraint->BatchConstraintEvaluation(coordinates1, coordinates2);
        flagSet2Viability=cconstraintCache.colwise().any();
    }	//if(cconstraint)

    //Compute the similarity scores
    similarityCache.resize(nFeatures1, nFeatures2);
    similarityValidityArray.resize(nFeatures1, nFeatures2);

    //For each feature pair
    size_t c2=0;
    ArrayXbT validityArray(nFeatures1);
    ArrayXrT similarityArray(nFeatures1);
#pragma omp parallel for if(nThreads>1) schedule(static) private(c2) firstprivate(validityArray, similarityArray) num_threads(nThreads)
    for(c2=0; c2<nFeatures2; ++c2)
    {
    	if(!flagSet2Viability[c2])
    	{
    		similarityCache.col(c2).setZero();
    		similarityValidityArray.col(c2).setConstant(false);
    		continue;
    	}

        //If single-threaded, iterate over the second set
        if(nThreads==1)
            SimilarityCacheInnerLoopST(ssimilarityMetric, cconstraint, cconstraintCache, sset2[c2], c2, sset1);
        else
        {
            SimilarityCacheInnerLoopMT(validityArray, similarityArray, ssimilarityMetric, cconstraint, cconstraintCache, sset2[c2], c2, sset1);
        //Even when the critical section is removed completely, there is not much improvement. So, no worthwhile gain by processing multiple features at a time, as opposed to a single row (and hence, reducing the number of entrances to the critical section)
        #pragma omp critical(IFMP_I)
        {
            similarityValidityArray.col(c2).swap(validityArray);
            similarityCache.col(c2).swap(similarityArray);
        }   //#pragma omp critical

        }   //if(!flagMultithreaded)
    }   //for(const auto& feature1 : sset1)

    //Store the constraint cache
    if(cconstraint)
    {
    	constraintCache=BoolArrayT();
    	constraintCache->swap(cconstraintCache);
    }	//if(cconstraint)

    //Dynamic operation?
    if(!flagStatic)
    {
    	set1=vector<Feature1T>(); set1->reserve(nFeatures1);
    	for_each(sset1, [&](const Feature1T& current){set1->push_back(current);});

    	set2=vector<Feature2T>(); set2->reserve(nFeatures2);
    	for_each(sset2, [&](const Feature2T& current){set2->push_back(current);});

        similarityMetric=ssimilarityMetric;

        //Is there a constraint?
        if(cconstraint)
            constraint=*cconstraint;
    }   //if(!flagFixed)

    flagValid=true;
}   //void Initialise(SimilarityT& ssimilarityMetric, optional<ConstraintT>& cconstraint, const FeatureRangeT& sset1, const FeatureRangeT& sset2, bool fflagStatic, bool flagMultithreaded);

/**
 * @brief Computes an ambiguity metric to evaluate the reliability of correspondences
 * @tparam CorrespondenceRangeT A correspondence range
 * @param[in,out] correspondences Correspondences. In the output \c strength.ambiguity is overwritten by the computed ambiguity metric
 * @param[in] nThreads Number of threads
 * @param[in] sSample Max cardinality of the sample from which the metric is computed
 * @remarks A lower value indicates a more reliable correspondence
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class CorrespondenceRangeT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::AssessAmbiguity(CorrespondenceRangeT& correspondences, unsigned int nThreads, unsigned int sSample) const
{
	RayleighMetaRecognitionMetricC<SimilarityT> ambiguityMetric;
	ambiguityMetric(correspondences, similarityMetric, similarityCache, similarityValidityArray, constraintCache, nThreads, sSample);
}	//void AssessReliability(CorrespondenceRangeT& correspondences)

/**
 * @brief Applies a coordinate transformation to a feature set
 * @tparam FeatureT Feature type
 * @tparam TransformationT Transformation type
 * @param[in] featureSet Feature set
 * @param[in] transformation Transformation
 * @pre \c FeatureT satisfies \c FeatureConceptC
 * @pre \c TransformationT is one of the types in \c CoordinateTransformationsC
 * @pre The problem is valid
 * @throws logic_error If the object is initalised for the static operation mode
 * @post Invalidates the constraint cache
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class FeatureT, class TransformationT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::ApplyCoordinateTransformation(optional<vector<FeatureT> >& featureSet, const TransformationT& transformation)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<FeatureT>));

	assert(flagValid);

	typedef CoordinateTransformationsC<typename ValueTypeM<typename FeatureT::coordinate_type>::type, RowsM<typename FeatureT::coordinate_type>::value> TransformerT;
	static_assert(is_same<typename TransformerT::projective_transform_type, TransformationT>::value || is_same<typename TransformerT::affine_transform_type, TransformationT>::value, "FeatureMatchingProblemBaseC::ApplyCoordinateTransformation1: TransformationT must be one of the transformations defined in CoordinateTransformationsC" );

	//If static mode operation, features are not stored
	if(flagStatic)
		throw(logic_error("FeatureMatchingProblemBaseC::ApplyCoordinateTransformation : This operation cannot be performed in the static operation mode.") );

	if(!featureSet)
		return;

	for_each(*featureSet, [&](FeatureT& current){current.Coordinate()=TransformerT::TransformCoordinate(current.Coordinate(), transformation);});	//Apply the transformation

	SetConstraint(optional<ConstraintT>());	//Invalidate the constraint
}	//void ApplyCoordinateTransformation(optional<vector<FeatureT> >& featureSet, const TransformationT& transformation)

/**
 * @brief Applies a coordinate transformation to the first feature set
 * @tparam TransformationT A coordinate transformation
 * @param transformation Transformation
 * @post Invalidates the constraint cache
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class TransformationT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::ApplyCoordinateTransformation1(const TransformationT& transformation)
{
	ApplyCoordinateTransformation(set1, transformation);
}	//void ApplyCoordinateTransformation1(const TransformationT& transformation)

/**
 * @brief Applies a coordinate transformation to the second feature set
 * @tparam TransformationT A coordinate transformation
 * @param transformation Transformation
 * @post Invalidates the constraint cache
 */
template<class Feature1T, class Feature2T, class SimilarityT, class ConstraintT>
template<class TransformationT>
void FeatureMatchingProblemBaseC<Feature1T, Feature2T, SimilarityT, ConstraintT>::ApplyCoordinateTransformation2(const TransformationT& transformation)
{
	ApplyCoordinateTransformation(set2, transformation);
}	//void ApplyCoordinateTransformation2(const TransformationT& transformation)

}	//MatcherN
}	//SeeSawN

#endif /* FEATURE_MATCHING_PROBLEM_BASE_IPP_5132093 */
