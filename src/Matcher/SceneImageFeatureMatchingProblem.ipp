/**
 * @file SceneImageFeatureMatchingProblem.ipp Implementation of \c SceneImageFeatureMatchingProblemC
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SCENE_IMAGE_FEATURE_MATCHING_PROBLEM_IPP_5320943
#define SCENE_IMAGE_FEATURE_MATCHING_PROBLEM_IPP_5320943

#include <boost/optional.hpp>
#include "FeatureMatchingProblemBase.h"
#include "../Elements/Feature.h"

namespace SeeSawN
{
namespace MatcherN
{

using boost::optional;
using SeeSawN::MatcherN::FeatureMatchingProblemBaseC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;

/**
 * @brief Problem class for nearest-neighbour matching problem for a scene and an image feature
 * @tparam SimilarityT A similarity metric
 * @tparam ConstraintT A matching constraint
 * @ingroup Problem
 * @nosubgrouping
 */
template<class SimilarityT, class ConstraintT>
class SceneImageFeatureMatchingProblemC : public FeatureMatchingProblemBaseC<SceneFeatureC, ImageFeatureC, SimilarityT, ConstraintT>
{
	private:

		typedef FeatureMatchingProblemBaseC<SceneFeatureC, ImageFeatureC, SimilarityT, ConstraintT> BaseT;	///< Type of the base

	public:

		/** @name Constructors */ //@{
		SceneImageFeatureMatchingProblemC();   ///< Default constructor
		template<class SceneFeatureRangeT, class ImageFeatureRangeT> SceneImageFeatureMatchingProblemC(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const SceneFeatureRangeT& sset1, const ImageFeatureRangeT& sset2, bool fflagStatic, unsigned int nThreads);   ///< Constructor
		//@}
};	//SceneFeatureMatchingProblemC

/**
 * @brief Problem class for nearest-neighbour matching problem for a scene and an image feature
 * @tparam SimilarityT A similarity metric
 * @tparam ConstraintT A matching constraint
 * @ingroup Problem
 * @nosubgrouping
 */
template<class SimilarityT, class ConstraintT>
class BinarySceneImageFeatureMatchingProblemC : public FeatureMatchingProblemBaseC<OrientedBinarySceneFeatureC, BinaryImageFeatureC, SimilarityT, ConstraintT>
{
	private:

		typedef FeatureMatchingProblemBaseC<OrientedBinarySceneFeatureC, BinaryImageFeatureC, SimilarityT, ConstraintT> BaseT;	///< Type of the base

	public:

		/** @name Constructors */ //@{
		BinarySceneImageFeatureMatchingProblemC();   ///< Default constructor
		template<class SceneFeatureRangeT, class ImageFeatureRangeT> BinarySceneImageFeatureMatchingProblemC(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const SceneFeatureRangeT& sset1, const ImageFeatureRangeT& sset2, bool fflagStatic, unsigned int nThreads);   ///< Constructor
		//@}
};	//BinarySceneImageFeatureMatchingProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/********** SceneImageFeatureMatchingProblemC **********/

/**
 * @brief Default constructor
 */
template<class SimilarityT, class ConstraintT>
SceneImageFeatureMatchingProblemC<SimilarityT, ConstraintT>::SceneImageFeatureMatchingProblemC()
{
    BaseT::flagValid=false;
    BaseT::flagStatic=true;
}   //SceneFeatureMatchingProblemC()

/**
 * @brief Constructor
 * @tparam SceneFeatureRangeT A range of scene features
 * @tparam ImageFeatureRangeT A range of image features
 * @param[in] ssimilarityMetric Similarity metric
 * @param[in] cconstraint Matching constraint. An invalid input signifies no constraints
 * @param[in] sset1 Scene feature set
 * @param[in] sset2 Image feature set
 * @param[in] fflagStatic Similarity score and constraint evaluation will be performed only once, in the constructor. If \c true, the problem state is composed of only the similarity cache. Not suitable for guided matching applications
 * @param[in] nThreads Number of threads, for multihreaded operation
 * @remarks If \c flagStatic=true , setting a constraint later on is not possible
 */
template<class SimilarityT, class ConstraintT>
template<class SceneFeatureRangeT, class ImageFeatureRangeT>
SceneImageFeatureMatchingProblemC<SimilarityT, ConstraintT>::SceneImageFeatureMatchingProblemC(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const SceneFeatureRangeT& sset1, const ImageFeatureRangeT& sset2, bool fflagStatic, unsigned int nThreads)
{
	BaseT::Initialise(ssimilarityMetric, cconstraint, sset1, sset2, fflagStatic, nThreads);
}   //SceneFeatureMatchingProblemC(SimilarityT& ssimilarityMetric, optional<ConstraintT>& cconstraint, const FeatureRangeT& sset1, const FeatureRangeT& sset2, bool flagOneShot)

/********** BinarySceneImageFeatureMatchingProblemC **********/

/**
 * @brief Default constructor
 */
template<class SimilarityT, class ConstraintT>
BinarySceneImageFeatureMatchingProblemC<SimilarityT, ConstraintT>::BinarySceneImageFeatureMatchingProblemC()
{
    BaseT::flagValid=false;
    BaseT::flagStatic=true;
}   //SceneFeatureMatchingProblemC()

/**
 * @brief Constructor
 * @tparam SceneFeatureRangeT A range of scene features
 * @tparam ImageFeatureRangeT A range of image features
 * @param[in] ssimilarityMetric Similarity metric
 * @param[in] cconstraint Matching constraint. An invalid input signifies no constraints
 * @param[in] sset1 Scene feature set
 * @param[in] sset2 Image feature set
 * @param[in] fflagStatic Similarity score and constraint evaluation will be performed only once, in the constructor. If \c true, the problem state is composed of only the similarity cache. Not suitable for guided matching applications
 * @param[in] nThreads Number of threads, for multihreaded operation
 * @remarks If \c flagStatic=true , setting a constraint later on is not possible
 */
template<class SimilarityT, class ConstraintT>
template<class SceneFeatureRangeT, class ImageFeatureRangeT>
BinarySceneImageFeatureMatchingProblemC<SimilarityT, ConstraintT>::BinarySceneImageFeatureMatchingProblemC(const SimilarityT& ssimilarityMetric, const optional<ConstraintT>& cconstraint, const SceneFeatureRangeT& sset1, const ImageFeatureRangeT& sset2, bool fflagStatic, unsigned int nThreads)
{
	BaseT::Initialise(ssimilarityMetric, cconstraint, sset1, sset2, fflagStatic, nThreads);
}   //SceneFeatureMatchingProblemC(SimilarityT& ssimilarityMetric, optional<ConstraintT>& cconstraint, const FeatureRangeT& sset1, const FeatureRangeT& sset2, bool flagOneShot)

}	//MatcherN
}	//SeeSawN

#endif /* SCENEIMAGEFEATUREMATCHINGPROBLEM_IPP_ */
