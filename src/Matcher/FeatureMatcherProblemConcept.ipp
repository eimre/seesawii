/**
 * @file FeatureMatcherProblemConcept.ipp Implementation of FeatureMatcherProblemConceptC
 * @author Evren Imre
 * @date 28 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_MATCHER_PROBLEM_CONCEPT_IPP_5313265
#define FEATURE_MATCHER_PROBLEM_CONCEPT_IPP_5313265

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <vector>
#include "../Elements/FeatureConcept.h"
#include "../Elements/Correspondence.h"
#include "NearestNeighbourMatcherProblemConcept.h"

namespace SeeSawN
{
namespace MatcherN
{

using SeeSawN::MatcherN::NearestNeighbourMatchingProblemConceptC;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::ElementsN::CorrespondenceListT;
using std::vector;
/**
 * @brief Feature matcher problem concept checker
 * @ingroup Concept
 */
template<class TestT>
class FeatureMatcherProblemConceptC
{
    ///@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((NearestNeighbourMatchingProblemConceptC<TestT>));
    ///@endcond

    public:

        ///@cond CONCEPT_CHECK
        BOOST_CONCEPT_USAGE(FeatureMatcherProblemConceptC)
        {
            typedef typename TestT::feature_type1 Feature1T;
            typedef typename TestT::feature_type2 Feature2T;
            typedef typename TestT::similarity_type SimilarityT;
            typedef typename TestT::constraint_type ConstraintT;

            BOOST_CONCEPT_ASSERT((FeatureConceptC<Feature1T>));
            BOOST_CONCEPT_ASSERT((FeatureConceptC<Feature2T>));

            SimilarityT s;
            optional<ConstraintT> c;
            vector<Feature1T> v1;
            vector<Feature2T> v2;

            TestT tested(s, c, v1, v2, true, true);

            CorrespondenceListT index;
            unsigned int nThreads;
            tested.AssessAmbiguity(index, nThreads);
        }   //BOOST_CONCEPT_USAGE(FeatureMatcherProblemConceptC)

        ///@endcond
};  //class FeatureMatcherProblemConceptC

}   //MatcherN
}	//SeeSawN

#endif /* FEATURE_MATCHER_PROBLEM_CONCEPT_IPP_5313265 */
