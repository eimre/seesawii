/**
 * @file ClosestPointAssignmentProblem.ipp Implementation of ClosestPointAssignmentProblemC
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CLOSEST_POINT_ASSIGNMENT_PROBLEM_IPP_2546259
#define CLOSEST_POINT_ASSIGNMENT_PROBLEM_IPP_2546259

#include <boost/concept_check.hpp>
#include <boost/range.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <cstddef>
#include <type_traits>

namespace SeeSawN
{
namespace MatcherN
{

using boost::optional;
using boost::distance;
using boost::range_value;
using boost::ForwardRangeConcept;
using boost::AdaptableBinaryFunctionConcept;
using Eigen::ArrayXXd;
using std::size_t;
using std::is_convertible;

/**
 * @brief A N-nearest neighbour matching problem for closest-point assignment
 * @remarks A model of NearestNeighbourMatchingProblemConceptC
 * @remarks All similarity scores are computed up-front, upon initialisation
 * @ingroup Problem
 * @nosubgrouping
 */
class ClosestPointAssignmentProblemC
{
    private:

        /**@name State */ //@{
        bool flagValid; ///< \c true if the object is initialised
        ArrayXXd similarityArray;   ///< Array holding the similarity scores
        //@}

    public:

        /** @name Constructors */ //@{
        template<class SimilarityT, class DescriptorRange1T, class DescriptorRange2T> ClosestPointAssignmentProblemC(const SimilarityT& similarityMetric, const DescriptorRange1T& set1, const DescriptorRange2T& set2);   ///< Constructor
        //@}

        /** @name NearestNeighbourProblemConcept interface */ //@{

        ClosestPointAssignmentProblemC();   ///< Default constructor
        typedef double real_type;

        size_t GetSize1() const;  ///< Returns the number of elements in the first set
        size_t GetSize2() const;  ///< Returns the number of elements in the second set
        optional<double> ComputeSimilarity(size_t i1, size_t i2) const;   ///< Computes the similarity score for the specified elements
        bool IsValid() const; ///< \c true if the problem is valid
        //@}
};  //class ClosestPointAssignmentProblemC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Constructor
 * @param[in] similarityMetric A similarity metric
 * @param[in] set1 First set
 * @param[in] set2 Second set
 * @pre \c SimilarityT is a model of \c AdaptableBinaryFunctionConcept and returns a value convertible to a double
 * @pre \c DescriptorRange1T and \c DescriptorRange2T are models of \c ForwardRangeConcept
 * @post The object is initialised
 */
template<class SimilarityT, class DescriptorRange1T, class DescriptorRange2T>
ClosestPointAssignmentProblemC::ClosestPointAssignmentProblemC(const SimilarityT& similarityMetric, const DescriptorRange1T& set1, const DescriptorRange2T& set2)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<DescriptorRange1T>));
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<DescriptorRange2T>));
    BOOST_CONCEPT_ASSERT((AdaptableBinaryFunctionConcept<SimilarityT, typename SimilarityT::result_type, typename range_value<DescriptorRange1T>::type, typename range_value<DescriptorRange2T>::type>));
    static_assert(is_convertible<typename SimilarityT::result_type, double>::value, "ClosestPointAssignmentProblemC::ClosestPointAssignmentProblemC : SimilarityT::result_type must be convertible to a double");

    //Compute the similarity score for all pairs

    size_t size1=distance(set1);
    size_t size2=distance(set2);
    similarityArray.resize(size1, size2);

    //Iterate over all pairs
    size_t i1=0;
    size_t i2=0;
    for(const auto& item1 : set1)
    {
        for(const auto& item2 : set2)
        {
            similarityArray(i1, i2)=similarityMetric(item1, item2);
            ++i2;
        }
        ++i1;
        i2=0;
    }   //for(const auto& item1 : set1)

    flagValid=true;
}   //ClosestPointAssignmentProblemC(SimilarityT& ssimilarityCalculator, const DescriptorRange1T& set1, const DescriptorRange2T& set2)


}   //MatcherN
}	//SeeSawN

//Extern templates
extern template class boost::optional<double>;
#endif /* CLOSEST_POINT_ASSIGNMENT_PROBLEM_IPP_2546259 */
