/**
 * @file ImageFeatureMatchingProblem.h
 * @author Evren Imre
 * @date 21 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef IMAGE_FEATURE_MATCHING_PROBLEM_H_13546232
#define IMAGE_FEATURE_MATCHING_PROBLEM_H_13546232

#include "ImageFeatureMatchingProblem.ipp"
#include "../Metrics/Distance.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Constraint.h"
#include "../Elements/Correspondence.h"

namespace SeeSawN
{
namespace MatcherN
{

template<class SimilarityT, class ConstraintT> class ImageFeatureMatchingProblemC; ///< Problem class for nearest-neighbour matching problem for image features
template<class SimilarityT, class ConstraintT> class BinaryImageFeatureMatchingProblemC; ///< Problem class for nearest-neighbour matching problem for binary image features

/********** EXTERN TEMPLATES *********/
using SeeSawN::MetricsN::EuclideanDistanceConstraint2DT;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::ElementsN::CorrespondenceListT;
extern template class ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT >;
extern template ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT>::ImageFeatureMatchingProblemC( const InverseEuclideanIDConverterT&, const optional<EuclideanDistanceConstraint2DT>&, const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, bool, unsigned int );
extern template void FeatureMatchingProblemBaseC<ImageFeatureC, ImageFeatureC, InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT>::AssessAmbiguity( CorrespondenceListT&, unsigned int, unsigned int) const;
}   //MatcherN
}	//SeeSawN


#endif /* IMAGE_FEATURE_MATCHING_PROBLEM_H_13546232 */
