/*
 * @file TestMatcher.cpp Unit tests for Matcher
 * @author Evren Imre
 * @date 15 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE MATCHER

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/math/distributions.hpp>
#include <Eigen/Dense>
#include <vector>
#include <set>
#include <algorithm>
#include <cstddef>
#include <cmath>
#include <stdexcept>
#include <tuple>
#include <map>
#include "NearestNeighbourMatcher.h"
#include "ClosestPointAssignmentProblem.h"
#include "GroupMatchingProblem.h"
#include "ImageFeatureMatchingProblem.h"
#include "SceneFeatureMatchingProblem.h"
#include "SceneImageFeatureMatchingProblem.h"
#include "CorrespondenceFusion.h"
#include "MultisourceFeatureMatcher.h"
#include "MultisourceFeatureMatcher.ipp"
#include "../Metrics/Constraint.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/Similarity.h"
#include "../Elements/Correspondence.h"
#include "../Metrics/Distance.h"
#include "../Metrics/Similarity.h"
#include "../Wrappers/BoostStatisticalDistributions.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestMatcherN
{

using namespace SeeSawN::MatcherN;

using boost::math::cauchy_distribution;
using boost::math::pdf;
using Eigen::Matrix3d;
using std::vector;
using std::set;
using std::cout;
using std::equal;
using std::size_t;
using std::sqrt;
using std::logic_error;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::map;
using SeeSawN::MetricsN::EuclideanDistance2DT;
using SeeSawN::MetricsN::EuclideanDistance3DT;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::InverseEuclideanSceneFeatureDistanceConverterT;
using SeeSawN::MetricsN::EuclideanDistanceConstraint2DT;
using SeeSawN::MetricsN::EuclideanDistanceConstraint3DT;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::MetricsN::InverseHammingBIDConverterT;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::WrappersN::PDFWrapperC;
using SeeSawN::MetricsN::DistanceToSimilarityConverterC;
using SeeSawN::GeometryN::CoordinateTransformations2DT;

BOOST_AUTO_TEST_SUITE(Matching_Problems)

BOOST_AUTO_TEST_CASE(Closest_Point_Assignment_Problem)
{
    vector<Coordinate3DT> pointList1(2);
    pointList1[0]=Coordinate3DT::Random();
    pointList1[1]=Coordinate3DT::Random();

    vector<Coordinate3DT> pointList2(1);
    pointList2[0]=Coordinate3DT::Random();

    ClosestPointAssignmentProblemC problem1;
    BOOST_CHECK_EQUAL(problem1.IsValid(), false);

    EuclideanDistance3DT euclid;
    typedef PDFWrapperC< cauchy_distribution<> > MapperT;
    MapperT cauchy;
    DistanceToSimilarityConverterC< EuclideanDistance3DT, MapperT > e2c(euclid, cauchy);
    ClosestPointAssignmentProblemC problem2(e2c, pointList1, pointList2);

    BOOST_CHECK_EQUAL(problem2.IsValid(), true);
    BOOST_CHECK_EQUAL(problem2.GetSize1(), (unsigned int)2);
    BOOST_CHECK_EQUAL(problem2.GetSize2(), (unsigned int)1);

    BOOST_CHECK_CLOSE(*problem2.ComputeSimilarity(0,0), cauchy( (*(pointList1.begin()) - *(pointList2.begin()) ).norm()), 1e-5 );
    BOOST_CHECK_CLOSE(*problem2.ComputeSimilarity(1,0), cauchy( (*(pointList1.rbegin()) - *(pointList2.begin()) ).norm()), 1e-5 );
}   //BOOST_AUTO_TEST_CASE(Closest_Point_Assignment_Problem)

BOOST_AUTO_TEST_CASE(Group_Matching_Problem)
{
	GroupMatchingProblemC problem1;

	typedef set<unsigned int> MembershipT;
	vector<MembershipT> memberships1{ {1,2}, {1,3}, {2,3}, {1,2}, {1,2} };
	vector<MembershipT> memberships2{ {1,2}, {1,3}, {2,3}, {0,3}, {1,2} };
	vector<double> weights{1, 1, 0.5, 1, 0.25};

	set<unsigned int> dummy;
	GroupMatchingProblemC problem2(memberships1, memberships2, weights, 1, dummy);

	BOOST_CHECK_EQUAL(problem2.GetSize1(), (size_t)3);
	BOOST_CHECK_EQUAL(problem2.GetSize2(), (size_t)4);
	BOOST_CHECK(problem2.IsValid());
	BOOST_CHECK_CLOSE(*problem2.ComputeSimilarity(0,0), 0.47059, 5e-3);
	BOOST_CHECK_CLOSE(*problem2.ComputeSimilarity(1,2), 0.77777, 5e-3);

	CorrespondenceListT correspondingGroups;
	correspondingGroups.push_back(typename CorrespondenceListT::value_type(0, 1, MatchStrengthC(0,0)));

	vector<size_t> index=problem2.FilterInput<set<unsigned int> >(correspondingGroups, memberships1, memberships2);
	BOOST_CHECK_EQUAL(index.size(), (size_t)3);
	BOOST_CHECK_EQUAL(index[0], 0);
	BOOST_CHECK_EQUAL(index[1], 1);
	BOOST_CHECK_EQUAL(index[2], 4);

	problem2.Clear();
	BOOST_CHECK(!problem2.IsValid());
}	//BOOST_AUTO_TEST_CASE(Group_Matching_Problem)

BOOST_AUTO_TEST_CASE(Image_Feature_Matching_Problem)
{
	//Also tests FeatureMatchingProblemBaseC

	ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT> problem1;
	BOOST_CHECK(!problem1.IsValid());

	vector<ImageFeatureC> featureList1(2);
 	featureList1[0].Coordinate()<<100, 25;
	featureList1[0].Descriptor().resize(2); featureList1[0].Descriptor()[0]=0.25; featureList1[0].Descriptor()[1]=0.5;
 	featureList1[1].Coordinate()<<10, 25;
	featureList1[1].Descriptor().resize(2); featureList1[1].Descriptor()[0]=0.5; featureList1[1].Descriptor()[1]=0.25;

	vector<ImageFeatureC> featureList2(2);
 	featureList2[0].Coordinate()<<25, 20;
	featureList2[0].Descriptor().resize(2); featureList2[0].Descriptor()[0]=0.25; featureList2[0].Descriptor()[1]=0.5;
 	featureList2[1].Coordinate()<<15, 25;
	featureList2[1].Descriptor().resize(2); featureList2[1].Descriptor()[0]=0.2; featureList2[1].Descriptor()[1]=0.3;

	optional<EuclideanDistanceConstraint2DT> constraint;
	InverseEuclideanIDConverterT similarity;
	ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT> problem2(similarity, constraint, featureList1, featureList2, true, 1);

	problem1.Initialise(similarity, constraint, featureList1, featureList2, true, 1);
	BOOST_CHECK(problem1.IsValid());

	BOOST_CHECK(problem2.IsValid());
	BOOST_CHECK_EQUAL(problem2.GetSize1(), (size_t)2);
	BOOST_CHECK_EQUAL(problem2.GetSize2(), (size_t)2);

	BOOST_CHECK_EQUAL(*problem1.ComputeSimilarity(0,0), *problem2.ComputeSimilarity(0,0));
	BOOST_CHECK_CLOSE(*problem2.ComputeSimilarity(1,0), 1.0/(0.25*sqrt(2)), 2e-6);

	optional<EuclideanDistanceConstraint2DT> constraint2=problem2.GetConstraint();
	constraint2=EuclideanDistanceConstraint2DT(EuclideanDistance2DT(), 5, false);
	BOOST_CHECK_EXCEPTION(problem2.SetConstraint(constraint2), logic_error, [](const logic_error&){return true;});

	CorrespondenceListT candidates;
	candidates.push_back(CorrespondenceListT::value_type(1,1, MatchStrengthC(0.2, 0)));
	problem2.AssessAmbiguity(candidates);
	BOOST_CHECK_CLOSE(candidates.begin()->info.Ambiguity(), 0.74, 1e-5);

	//Coordinate transformations
	Matrix3d mT=Matrix3d::Identity(); mT(0,2)=5; mT(1,2)=1;
	CoordinateTransformations2DT::projective_transform_type transformation(mT);

	BOOST_CHECK_EXCEPTION(problem2.ApplyCoordinateTransformation1(transformation), logic_error, [](const logic_error&){return true;});
	BOOST_CHECK_EXCEPTION(problem2.ApplyCoordinateTransformation2(transformation), logic_error, [](const logic_error&){return true;});

	ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EuclideanDistanceConstraint2DT> problem3(similarity, constraint, featureList1, featureList2, false, 1);

	//Since the problem does not offer access to the feature set, the result is not verified
	problem3.ApplyCoordinateTransformation1(transformation);
	problem3.ApplyCoordinateTransformation2(transformation);

	//Instantiation test for the binary variant. The rest is tested above
	BinaryImageFeatureMatchingProblemC<InverseHammingBIDConverterT, EuclideanDistanceConstraint2DT> binaryProblem1;

}	//BOOST_AUTO_TEST_CASE(Image_Feature_Matching_Problem)

BOOST_AUTO_TEST_CASE(Scene_Feature_Matching_Problem)
{
	typedef SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, EuclideanDistanceConstraint3DT> ProblemT;

	ProblemT invalidProblem;
	BOOST_CHECK(!invalidProblem.IsValid());

	vector<ProblemT::feature_type1> features1(5);
	vector<ProblemT::feature_type2> features2(2);
	optional<EuclideanDistanceConstraint3DT> constraint;
	InverseEuclideanSceneFeatureDistanceConverterT similarity;
	ProblemT validProblem(similarity, constraint, features1, features2, true, 1);
	BOOST_CHECK(validProblem.IsValid());
}	//BOOST_AUTO_TEST_CASE(Scene_Feature_Matching_Problem)

BOOST_AUTO_TEST_CASE(Scene_Image_Feature_Matching_Problem)
{
	typedef SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> ProblemT;

	ProblemT invalidProblem;
	BOOST_CHECK(!invalidProblem.IsValid());

	vector<ProblemT::feature_type1> features1(5);
	vector<ProblemT::feature_type2> features2(2);
	optional<ReprojectionErrorConstraintT> constraint;
	InverseEuclideanSceneImageFeatureDistanceConverterT similarity;
	ProblemT validProblem(similarity, constraint, features1, features2, true, 1);
	BOOST_CHECK(validProblem.IsValid());

	//Instantiation test for the binary variant. The rest is tested above
	BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> binaryProblem;

}	//BOOST_AUTO_TEST_CASE(Scene_Feature_Matching_Problem)

BOOST_AUTO_TEST_SUITE_END() //Matching_Problems

BOOST_AUTO_TEST_SUITE(Matcher)

BOOST_AUTO_TEST_CASE(Nearest_Neighbour_Matcher)
{
    vector<Coordinate3DT> pointList1(10);
    pointList1[0]<<0.7056680, 0.7602566, 0.0118095;
    pointList1[1]<<0.0473364,0.0041743,0.5398475;
    pointList1[2]<<0.2292486,0.2879903,0.0098491;
    pointList1[3]<<0.8762749,0.6107362,0.2690101;
    pointList1[4]<<0.7016065,0.9468590,0.8794282;
    pointList1[5]<<0.8584846,0.6874528,0.0139674;
    pointList1[6]<<0.5844969,0.8609913,0.6404234;
    pointList1[7]<<0.9828177,0.1509989,0.0804770;
    pointList1[8]<<0.0663416,0.3749450,0.6659576;
    pointList1[9]<<0.1915607,0.3845943,0.1786845;

    vector<Coordinate3DT> pointList2(7);
    pointList2[0]<<0.574378,0.072620,0.924977;
    pointList2[1]<<0.846500,0.598459,0.576821;
    pointList2[2]<<0.171277,0.946230,0.542838;
    pointList2[3]<<0.598929,0.104138,0.076883;
    pointList2[4]<<0.945514,0.153661,0.389960;
    pointList2[5]<<0.490244,0.909978,0.874825;
    pointList2[6]<<0.825443,0.367874,0.846955;

    EuclideanDistance3DT euclid;
    cauchy_distribution<> cauchyPDF(0, 0.5);
    typedef PDFWrapperC< cauchy_distribution<> > MapperT;
    MapperT cauchy(cauchyPDF);
    DistanceToSimilarityConverterC< EuclideanDistance3DT, MapperT > e2c(euclid, cauchy);

    typedef NearestNeighbourMatcherC<ClosestPointAssignmentProblemC> MatcherT;

    NearestNeighbourMatcherDiagnosticsC diagnostics;
    NearestNeighbourMatcherParametersC parameters;
    parameters.similarityTestThreshold=pdf(cauchyPDF,0.75);
    parameters.ratioTestThreshold=0.9;
    parameters.neighbourhoodSize12=1;
    parameters.neighbourhoodSize21=1;

    //Self-matching, 1:1
    ClosestPointAssignmentProblemC problem1(e2c, pointList1, pointList1);
    CorrespondenceListT correspondences;
    diagnostics=MatcherT::Run(correspondences, problem1, parameters);
    BOOST_CHECK_EQUAL(correspondences.size(), pointList1.size());
    size_t c1=0;
    for(const auto& current : correspondences.left)
    {
        BOOST_CHECK_EQUAL(current.first, c1);
        BOOST_CHECK_EQUAL(current.second, c1);
        BOOST_CHECK_CLOSE(current.info.Similarity(), pdf(cauchyPDF, 0), 1e-5);
        ++c1;
    }

    //Match other, 1:1
    ClosestPointAssignmentProblemC problem2(e2c, pointList1, pointList2);
    diagnostics=MatcherT::Run(correspondences, problem2, parameters);
    BOOST_CHECK_EQUAL(correspondences.size(), (unsigned int)2);
    BOOST_CHECK_EQUAL(correspondences.left.begin()->first, (unsigned int)3);
    BOOST_CHECK_EQUAL(correspondences.left.begin()->second, (unsigned int)1);
    BOOST_CHECK_CLOSE(correspondences.left.begin()->info.Similarity(), 0.460272, 1e-3);
    BOOST_CHECK_CLOSE(correspondences.left.begin()->info.Ambiguity(), 0.882973, 1e-3);
    BOOST_CHECK_EQUAL(correspondences.left.rbegin()->first, (unsigned int)7);
    BOOST_CHECK_EQUAL(correspondences.left.rbegin()->second, (unsigned int)4);
    BOOST_CHECK_CLOSE(correspondences.left.rbegin()->info.Similarity(), 0.458424, 1e-3);
    BOOST_CHECK_CLOSE(correspondences.left.rbegin()->info.Ambiguity(), 0.868859, 1e-3);

    //Match other, 3:2
    parameters.neighbourhoodSize12=3;
    parameters.neighbourhoodSize21=2;
    diagnostics=MatcherT::Run(correspondences, problem2, parameters);
    BOOST_CHECK_EQUAL(correspondences.size(), (unsigned int)7);

    //Just check 3 correspondences randomly

    auto it2=next(correspondences.left.begin(),2);
    BOOST_CHECK_EQUAL(it2->first, (unsigned int)3);
    BOOST_CHECK_EQUAL(it2->second, (unsigned int)1);
    BOOST_CHECK_CLOSE(it2->info.Similarity(), 0.460272, 1e-3);
    BOOST_CHECK_CLOSE(it2->info.Ambiguity(), 0.714508, 1e-3);

    auto it4=next(correspondences.left.begin(),4);
    BOOST_CHECK_EQUAL(it4->first, (unsigned int)6);
    BOOST_CHECK_EQUAL(it4->second, (unsigned int)5);
    BOOST_CHECK_CLOSE(it4->info.Similarity(), 0.503293, 1e-3);
    BOOST_CHECK_CLOSE(it4->info.Ambiguity(), 0.936210, 1e-3);

    auto it6=next(correspondences.left.begin(),6);
    BOOST_CHECK_EQUAL(it6->first, (unsigned int)8);
    BOOST_CHECK_EQUAL(it6->second, (unsigned int)0);
    BOOST_CHECK_CLOSE(it6->info.Similarity(), 0.238759, 1e-3);
    BOOST_CHECK_CLOSE(it6->info.Ambiguity(), 0.979156, 1e-3);

    //K-means, 1:0
    parameters.neighbourhoodSize21=0;
    parameters.neighbourhoodSize12=1;
    parameters.flagConsistencyTest=false;
    parameters.similarityTestThreshold=0;
    parameters.ratioTestThreshold=1.1;
    diagnostics=MatcherT::Run(correspondences, problem2, parameters);

    //3 random checks
    it2=next(correspondences.left.begin(),2);
    BOOST_CHECK_EQUAL(it2->first, (unsigned int)2);
    BOOST_CHECK_EQUAL(it2->second, (unsigned int)3);
    BOOST_CHECK_CLOSE(it2->info.Similarity(), 0.374519, 1e-3);
    BOOST_CHECK_EQUAL(it2->info.Ambiguity(), 0);

    it4=next(correspondences.left.begin(),4);
    BOOST_CHECK_EQUAL(it4->first, (unsigned int)4);
    BOOST_CHECK_EQUAL(it4->second, (unsigned int)5);
    BOOST_CHECK_CLOSE(it4->info.Similarity(), 0.537585, 1e-3);
    BOOST_CHECK_EQUAL(it4->info.Ambiguity(), 0);

    it6=next(correspondences.left.begin(),6);
    BOOST_CHECK_EQUAL(it6->first, (unsigned int)6);
    BOOST_CHECK_EQUAL(it6->second, (unsigned int)5);
    BOOST_CHECK_CLOSE(it6->info.Similarity(), 0.503293, 1e-3);
    BOOST_CHECK_EQUAL(it6->info.Ambiguity(), 0);

    ClosestPointAssignmentProblemC problem3;
    BOOST_CHECK_THROW(MatcherT::Run(correspondences, problem3, parameters), invalid_argument);

    parameters.neighbourhoodSize12=0;
    BOOST_CHECK_THROW(MatcherT::Run(correspondences, problem2, parameters), invalid_argument);
}   //BOOST_AUTO_TEST_CASE(Nearest_Neighbour_Matcher)

BOOST_AUTO_TEST_SUITE_END() //Matcher

BOOST_AUTO_TEST_SUITE(Correspondence_Fusion)

BOOST_AUTO_TEST_CASE(Correspondence_Fusion_Operation)
{
	//Data

	CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT> correspondenceStack;

	CorrespondenceListT list1;
	list1.push_back(CorrespondenceListT::value_type(0,1, MatchStrengthC()));
	list1.push_back(CorrespondenceListT::value_type(3,4, MatchStrengthC()));
	list1.push_back(CorrespondenceListT::value_type(1,2, MatchStrengthC()));
	correspondenceStack.emplace(make_tuple(0,1), list1);

	CorrespondenceListT list2;
	list2.push_back(CorrespondenceListT::value_type(8,4, MatchStrengthC()));
	list2.push_back(CorrespondenceListT::value_type(0,3, MatchStrengthC()));
	list2.push_back(CorrespondenceListT::value_type(1,5, MatchStrengthC()));
	list2.push_back(CorrespondenceListT::value_type(3,7, MatchStrengthC()));
	correspondenceStack.emplace(make_tuple(1,2), list2);

	CorrespondenceListT list3;
	list3.push_back(CorrespondenceListT::value_type(4,2, MatchStrengthC()));
	list3.push_back(CorrespondenceListT::value_type(3,5, MatchStrengthC()));
	list3.push_back(CorrespondenceListT::value_type(0,3, MatchStrengthC()));
	list3.push_back(CorrespondenceListT::value_type(2,7, MatchStrengthC()));
	correspondenceStack.emplace(make_tuple(0,2), list3);

	CorrespondenceListT list4;
	list4.push_back(CorrespondenceListT::value_type(3,0, MatchStrengthC()));
	list4.push_back(CorrespondenceListT::value_type(2,6, MatchStrengthC()));
	correspondenceStack.emplace(make_tuple(2,1), list4);

	//Consistent
	//IMPORTANT: bimap::value_type equality check ignores info!

	vector<CorrespondenceFusionC::cluster_type> clusters;
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> observed;
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> implied;
	std::tie(clusters, observed, implied)=CorrespondenceFusionC::Run<CorrespondenceListT>(correspondenceStack, 3, true);

	vector<CorrespondenceFusionC::cluster_type> clustersGT(2);
	clustersGT[0].emplace(0,4); clustersGT[0].emplace(1,6); clustersGT[0].emplace(2,2);
	clustersGT[1].emplace(0,2); clustersGT[1].emplace(1,3); clustersGT[1].emplace(2,7);

	BOOST_CHECK_EQUAL(clusters.size(), 2);
	BOOST_CHECK(equal(clustersGT.begin(), clustersGT.end(), clusters.begin()));

	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> observedGT;
	observedGT.emplace(make_tuple(0,1), CorrespondenceFusionC::correspondence_container_type());

	auto itO02=observedGT.emplace(make_tuple(0,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itO02->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(4,2,0));
	itO02->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(2,7,1));

	auto itO12=observedGT.emplace(make_tuple(1,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itO12->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(6,2,0));
	itO12->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,7,1));

	BOOST_CHECK_EQUAL(observed.size(), 3);
	BOOST_CHECK(equal(observedGT.begin(), observedGT.end(), observed.begin()));


	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> impliedGT;
	auto itI01=impliedGT.emplace(make_tuple(0,1), CorrespondenceFusionC::correspondence_container_type()).first;
	itI01->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(4,6,0));
	itI01->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(2,3,1));

	impliedGT.emplace(make_tuple(0,2), CorrespondenceFusionC::correspondence_container_type());
	impliedGT.emplace(make_tuple(1,2), CorrespondenceFusionC::correspondence_container_type());

	BOOST_CHECK_EQUAL(implied.size(), 3);
	BOOST_CHECK(equal(impliedGT.begin(), impliedGT.end(), implied.begin()));

	//Inconsistent

	vector<CorrespondenceFusionC::cluster_type> clusters2;
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> observed2;
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> implied2;
	std::tie(clusters2, observed2, implied2)=CorrespondenceFusionC::Run<CorrespondenceListT>(correspondenceStack, 4, false);

	vector<CorrespondenceFusionC::cluster_type> clusters2GT(1);
	clusters2GT[0].emplace(0,0); clusters2GT[0].emplace(0,3); clusters2GT[0].emplace(1,0); clusters2GT[0].emplace(1,1);
	clusters2GT[0].emplace(1,4); clusters2GT[0].emplace(2,3); clusters2GT[0].emplace(2,5);

	BOOST_CHECK_EQUAL(clusters2.size(), 1);
	BOOST_CHECK(equal(clusters2GT.begin(), clusters2GT.end(), clusters2.begin()));

	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> observed2GT;
	auto itO201=observed2GT.emplace(make_tuple(0,1), CorrespondenceFusionC::correspondence_container_type()).first;
	itO201->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,1,0));
	itO201->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,4,0));

	auto itO202=observed2GT.emplace(make_tuple(0,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itO202->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,3,0));
	itO202->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,5,0));

	auto itO212=observed2GT.emplace(make_tuple(1,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itO212->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,3,0));
	itO212->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(1,5,0));

	BOOST_CHECK_EQUAL(observed2.size(), 3);
	BOOST_CHECK(equal(observed2GT.begin(), observed2GT.end(), observed2.begin()));

	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> implied2GT;

	auto itI200=impliedGT.emplace(make_tuple(0,0), CorrespondenceFusionC::correspondence_container_type()).first;
	itI200->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,3,0));

	auto itI201=impliedGT.emplace(make_tuple(0,1), CorrespondenceFusionC::correspondence_container_type()).first;
	itI201->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,0,0));
	itI201->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,4,0));
	itI201->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,0,0));
	itI201->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,1,0));

	auto itI202=impliedGT.emplace(make_tuple(0,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itI202->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,5,0));
	itI202->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,3,0));

	auto itI211=impliedGT.emplace(make_tuple(1,1), CorrespondenceFusionC::correspondence_container_type()).first;
	itI211->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,1,0));
	itI211->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,4,0));
	itI211->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(1,4,0));

	auto itI212=impliedGT.emplace(make_tuple(1,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itI212->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(0,5,0));
	itI212->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(1,3,0));
	itI212->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(4,3,0));
	itI212->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(4,5,0));

	auto itI222=impliedGT.emplace(make_tuple(2,2), CorrespondenceFusionC::correspondence_container_type()).first;
	itI222->second.push_back(CorrespondenceFusionC::correspondence_container_type::value_type(3,5,0));

	BOOST_CHECK_EQUAL(implied2.size(), 6);
	BOOST_CHECK(equal(implied2GT.begin(), implied2GT.end(), implied2.begin()));

	//Convert to unified
	CorrespondenceFusionC::unified_view_type unified=CorrespondenceFusionC::ConvertToUnified(observed);
	BOOST_CHECK(unified==clustersGT);

	//Splice
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> spliced=CorrespondenceFusionC::SplicePairwiseViews(observed, implied);
	CorrespondenceFusionC::pairwise_view_type<CorrespondenceFusionC::correspondence_container_type> splicedGT=observed;
	for(auto& current : splicedGT)
	{
		auto it=implied.find(current.first);
		if(it!=implied.end())
			current.second.insert(current.second.end(), it->second.begin(), it->second.end());
	}	//for(auto& current : splicedGT)

	BOOST_CHECK(equal(splicedGT.begin(), splicedGT.end(), spliced.begin()));

}	//BOOST_AUTO_TEST_CASE(Correspondence_Fusion_Operation)

BOOST_AUTO_TEST_SUITE_END()	//Correspondence_Fusion

BOOST_AUTO_TEST_SUITE(Multisource_Feature_Matcher)

BOOST_AUTO_TEST_CASE(Compile)
{
	//No unit tests, just for compilation
	MultisourceFeatureMatcherC matcher; (void)matcher;
}	//BOOST_AUTO_TEST_CASE(Compile)

BOOST_AUTO_TEST_SUITE_END()	//Multisource_Feature_Matcher

}   //TestMatcherN
}   //UnitTestsN
}	//SeeSawN


