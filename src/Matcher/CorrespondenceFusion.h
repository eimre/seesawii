/**
 * @file CorrespondenceFusion.h Public interface for \c CorrespondenceFusionC
 * @author Evren Imre
 * @date 8 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CORRESPONDENCE_FUSION_H_0212903
#define CORRESPONDENCE_FUSION_H_0212903

#include "CorrespondenceFusion.ipp"
namespace SeeSawN
{
namespace MatcherN
{

class CorrespondenceFusionC;	///< Algorithm for the fusion of pairwise correspondences
}	//MatcherN
}	//SeeSawN

/********** EXPLICIT TEMPLATES **********/
extern template class std::set<unsigned int>;
#endif /* CORRESPONDENCE_FUSION_H_0212903 */
