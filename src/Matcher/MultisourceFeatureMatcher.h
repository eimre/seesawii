/**
 * @file MultisourceFeatureMatcher.h Public interface for multisource feature matching algorithm
 * @author Evren Imre
 * @date 10 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTISOURCE_FEATURE_MATCHER_H_2882389
#define MULTISOURCE_FEATURE_MATCHER_H_2882389

#include "MultisourceFeatureMatcher.ipp"
namespace SeeSawN
{
namespace MatcherN
{

struct MultisourceFeatureMatcherParametersC;	///< Parameters for \c MultisourceFeatureMatcherC
struct MultisourceFeatureMatcherDiagnosticsC;	///< Diagnostics for \c MultisourceFeatureMatcherC
class MultisourceFeatureMatcherC;	///< Multisource feature matching algorithm
}	//MatcherN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, unsigned int>(const unsigned int&);
#endif /* MULTISOURCEFEATUREMATCHER_H_ */
