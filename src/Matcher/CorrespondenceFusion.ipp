/**
 * @file CorrespondenceFusion.ipp Implementation of \c CorrespondenceFusionC
 * @author Evren Imre
 * @date 8 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CORRESPONDENCE_FUSION_IPP_0802312
#define CORRESPONDENCE_FUSION_IPP_0802312

#include <boost/range/concepts.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/bimap/unordered_multiset_of.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/property_map/property_map.hpp>
#include <vector>
#include <map>
#include <set>
#include <tuple>
#include <cstddef>
#include <type_traits>
#include <iterator>
#include <utility>

namespace SeeSawN
{
namespace MatcherN
{

using boost::ForwardRangeConcept;
using boost::bimap;
using boost::bimaps::list_of;
using boost::bimaps::unordered_multiset_of;
using boost::bimaps::left_based;
using boost::bimaps::list_of_relation;
using boost::bimaps::with_info;
using boost::adjacency_list;
using boost::vecS;
using boost::listS;
using boost::undirectedS;
using boost::add_vertex;
using boost::add_edge;
using boost::num_vertices;
using boost::connected_components;
using boost::graph_traits;
using boost::vector_property_map;
using boost::property_map;
using boost::property;
using boost::vertex_name;
using boost::vertex_name_t;
using boost::for_each;
using std::vector;
using std::map;
using std::make_tuple;
using std::set;
using std::tuple;
using std::tie;
using std::get;
using std::size_t;
using std::is_same;
using std::advance;
using std::next;
using std::pair;

/**
 * @brief Algorithm for the fusion of pairwise correspondences
 * @remarks Terminology
 * 	- Source: A list
 * 	- Pairwise correspondence: A pair of corresponding elements between two sources. Undirected (i.e., A<->B)
 * 	- Cluster: A collection of elements with mutual observed or implied correspondence relationships. A cluster is in source-first ascending order
 * @remarks Usage notes:
 * 	- The algorithm does not distinguish between the source pairs (P,Q) and (Q,P), and merges them at the output
 * 	- An inconsistent fusion implies correspondences within the same source
 * @ingroup Algorithm
 * @nosubgrouping
 */
class CorrespondenceFusionC
{
	public:
		typedef tuple<unsigned int, unsigned int> pair_id_type;	///< Type identifying a source pair. [First source (left); Second source (right)]
		template<class CorrespondenceRangeT> using pairwise_view_type=map<pair_id_type, CorrespondenceRangeT>;	///< A collection of pairwise correspondence ranges. [Pair id; correspondences]

		/** @name Cluster */ //@{
		typedef tuple<unsigned int, size_t> cluster_member_type;	///< An element of a cluster.[source id; element id]
		static constexpr unsigned int iSourceId=0;	///< Index of the source id component
		static constexpr unsigned int iElementId=1;	///< Index of the element id component

		typedef set<cluster_member_type> cluster_type;	///< A cluster: a set of elements corresponding to the same entity
		typedef vector<cluster_type> unified_view_type;	///< A collection of clusters
		///@}

		typedef bimap<list_of<size_t>, list_of<size_t>, left_based, with_info<size_t> > correspondence_container_type;	///< A bimap for pairwise correspondences.[left element, right element, cluster id]

	private:

		typedef adjacency_list<listS, vecS, undirectedS, property<vertex_name_t, cluster_member_type> > GraphT;	///< Correspondence graph type
																						//connected_components does not compile unless the VertexList type is VecS
		typedef typename graph_traits<GraphT>::vertex_descriptor VertexT;	///< Vertex descriptor

		/** @name Implementation details */ //@{
		template<class CorrespondenceRangeT> static GraphT BuildCorrespondenceGraph(const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences);	///< Builds the correspondence graph
		static VertexT InsertVertex(GraphT& correspondenceGraph, map<size_t, VertexT>& elementToVertexMap, unsigned int sourceId, size_t elementId);	///< Attempts to insert a vertex and returns the vertex descriptor

		static vector<cluster_type> BuildClusters(GraphT& correspondenceGraph);	///< Builds the clusters
		static vector<cluster_type> FilterClusters(const vector<cluster_type>& unfiltered, unsigned int minCardinality, bool flagConsistency);	///< Filters the clusters

		template<class CorrespondenceRangeT> static tuple<pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> > BuildCorrespondenceLists(const vector<cluster_type>& clusters, const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences);	///< Builds the filtered correspondence lists
		static void Insert(pairwise_view_type<correspondence_container_type>& observed, pairwise_view_type<correspondence_container_type>& implied, const pairwise_view_type<bimap<unordered_multiset_of<size_t>, list_of<size_t>, list_of_relation> >& original, const cluster_member_type& item1, const cluster_member_type& item2, size_t clusterIndex);	///< Inserts a correspondence to the appropriate set
		//@}

	public:

		template<class CorrespondenceRangeT> static tuple<unified_view_type, pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> > Run(const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences, unsigned int minCardinality, bool flagConsistency);	///< Runs the algorithm

		/** @name Utilities */ //@{
		static unified_view_type ConvertToUnified(const pairwise_view_type<correspondence_container_type>& src);	///< Converts a pairwise view to a unified view
		static pairwise_view_type<correspondence_container_type> SplicePairwiseViews(const pairwise_view_type<correspondence_container_type>& view1, const pairwise_view_type<correspondence_container_type>& view2);	///< Splices two correspondence stacks
		//@}

};	//class CorrespondenceFusionC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Builds the correspondence graph
 * @tparam CorrespondenceRangeT A correspondence range
 * @param[in] pairwiseCorrespondences Pairwise correspondences
 * @return Correspondence graph
 * @pre \c CorrespondenceRangeT is a bimap, with left and right key types of \c size_t (unenforced)
 */
template<class CorrespondenceRangeT>
auto CorrespondenceFusionC::BuildCorrespondenceGraph(const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences) ->GraphT
{
	//Preconditions
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, size_t>::value, "CorrespondenceFusionC::BuildCorrespondenceGraph : CorrespondenceRangeT::left_key_type must be size_t");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, size_t>::value, "CorrespondenceFusionC::BuildCorrespondenceGraph : CorrespondenceRangeT::right_key_type must be size_t");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));

	GraphT correspondenceGraph;
	typedef typename graph_traits<GraphT>::vertex_descriptor VertexT;

	map<unsigned int, map<size_t, VertexT> > elementToVertexMap;	// Holds the vertex descriptor for each element

	//For each correspondence list
	for(const auto& currentList : pairwiseCorrespondences)
	{
		//Initialise the maps for each source
		unsigned int id1;
		unsigned int id2;
		tie(id1, id2)=currentList.first;
		map<size_t, VertexT>& list1=elementToVertexMap[id1];
		map<size_t, VertexT>& list2=elementToVertexMap[id2];

		//Iterate over the correspondences
		for(const auto& current : currentList.second)
		{
			//Get the vertex descriptors
			VertexT vertex1=InsertVertex(correspondenceGraph, list1, id1, current.left);
			VertexT vertex2=InsertVertex(correspondenceGraph, list2, id2, current.right);

			add_edge(vertex1, vertex2, correspondenceGraph);	//Add the correspondence as an edge
		}	//for(const auto& current : currentList)
	}	//for(const auto& currentList : pairwiseCorrespondences)

	return correspondenceGraph;
}	//auto BuildCorrespondenceGraph(const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences) ->GraphT

/**
 * @brief Builds the filtered correspondence lists
 * @tparam CorrespondenceRangeT A correspondence range
 * @param[in] clusters Correspondence clusters
 * @param[in] pairwiseCorrespondences Unfiltered pairwise correspondences
 * @return A tuple: Observed correspondences; implied correspondences
 * @pre \c CorrespondenceRangeT is a bimap, with left and right key types of \c size_t (unenforced)
 */
template<class CorrespondenceRangeT>
auto CorrespondenceFusionC::BuildCorrespondenceLists(const vector<cluster_type>& clusters, const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences) -> tuple<pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> >
{
	//Preconditions
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, size_t>::value, "CorrespondenceFusionC::BuildCorrespondenceLists : CorrespondenceRangeT::left_key_type must be size_t");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, size_t>::value, "CorrespondenceFusionC::BuildCorrespondenceLists : CorrespondenceRangeT::right_key_type must be size_t");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));

	tuple<pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> > output;

	//Move the original lists into a set
	//The set is in first-source-major order
	typedef bimap<unordered_multiset_of<size_t>, list_of<size_t>, list_of_relation> CorrespondenceMapT;
	pairwise_view_type<CorrespondenceMapT> originalCorrespondences;
	for(const auto& currentList : pairwiseCorrespondences)
	{
		pair_id_type pairId=currentList.first;

		//Convention: smaller id first
		bool flagSwap=false;	// true: swap the left and right lists
		if(get<0>(pairId)>get<1>(pairId))
		{
			flagSwap=true;
			pairId=pair_id_type(get<1>(currentList.first), get<0>(currentList.first));
		}

		auto& currentOriginal=originalCorrespondences.emplace(pairId, CorrespondenceMapT()).first->second;

		if(!flagSwap)
		{
			for(const auto& current : currentList.second)
				currentOriginal.push_back(typename CorrespondenceMapT::value_type(current.left, current.right) );

			//Generate the corresponding output structures
			const correspondence_container_type& dummy1=get<0>(output)[currentList.first]; (void)dummy1;
			const correspondence_container_type& dummy2=get<1>(output)[currentList.first]; (void)dummy2;
		}
		else
			for(const auto& current : currentList.second)
				currentOriginal.push_back(typename CorrespondenceMapT::value_type(current.right, current.left) );
	}	//for(const auto& currentPair : pairwiseCorrespondences)

	//Iterate over the clusters

	size_t clusterIndex=0;
	for(const auto& currentCluster : clusters)
	{
		//Within a cluster, instantiate all pairwise combinations. If in the original list, "observed", else "implied";
		auto itE=currentCluster.cend();
		for(auto it1=currentCluster.cbegin(); it1!=itE; advance(it1,1))
			for(auto it2=next(it1,1); it2!=itE; advance(it2,1))
				Insert(get<0>(output), get<1>(output), originalCorrespondences, *it1, *it2, clusterIndex);

		++clusterIndex;
	}	//for(const auto& currentCluster : clusters)

	return output;
}	//tuple<pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> > BuildCorrespondenceLists(const vector<cluster_type>& clusters, const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences)

/**
 * @brief Runs the algorithm
 * @tparam CorrespondenceRangeT Type of input correspondence range
 * @param[in] pairwiseCorrespondences List of pairwise correspondences. [Source pair id; Correspondences]
 * @param[in] minCardinality Minimum cluster size
 * @param[in] flagConsistency If \c true , a cluster cannot have more than one element from a source
 * @return A tuple:[Clusters; Observed correspondences; Implied correspondences]
 * @pre \c CorrespondenceRangeT is a bimap, with left and right key types of \c size_t (unenforced)
 * @remarks \c CorrespondenceRangeT is meant to be a proxy to the actual correspondences, i.e., one that only holds identifiers for the actual elements
 */
template<class CorrespondenceRangeT>
auto CorrespondenceFusionC::Run(const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences, unsigned int minCardinality, bool flagConsistency) -> tuple<unified_view_type, pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> >
{
	//Preconditions
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, size_t>::value, "CorrespondenceFusionC::Run : CorrespondenceRangeT::left_key_type must be size_t");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, size_t>::value, "CorrespondenceFusionC::Run : CorrespondenceRangeT::right_key_type must be size_t");
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));

	GraphT correspondenceGraph=BuildCorrespondenceGraph(pairwiseCorrespondences);	//Build the graph
	vector<cluster_type> clusters=FilterClusters(BuildClusters(correspondenceGraph), minCardinality, flagConsistency);	//Build the clusters

	//Filtered pairwise views
	pairwise_view_type<correspondence_container_type> observed;	//Observed correspondences
	pairwise_view_type<correspondence_container_type> implied;	//Correspondences implied by the pairwise relationships
	std::tie(observed, implied)=BuildCorrespondenceLists(clusters, pairwiseCorrespondences);

	tuple<unified_view_type, pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> > output;
	get<0>(output).swap(clusters);
	get<1>(output).swap(observed);
	get<2>(output).swap(implied);

	return output;
}	//tuple<unified_view_type, pairwise_view_type<correspondence_container_type>, pairwise_view_type<correspondence_container_type> > Run(const pairwise_view_type<CorrespondenceRangeT>& pairwiseCorrespondences, unsigned int minClusterSize, bool flagConsistency)


}	//MatcherN
}	//SeeSawN

#endif /* CORRESPONDENCE_FUSION_IPP_0802312 */
