/**
 * @file MultisourceFeatureMatcher.cpp Implementation of \c MultisourceFeatureMatcherC
 * @author Evren Imre
 * @date 16 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "MultisourceFeatureMatcher.h"
namespace SeeSawN
{
namespace MatcherN
{

/**
 * @brief Validates the parameters
 * @param[in,out] parameters Parameters
 * @throws invalid_argument If a parameter has an invalid value
 */
void MultisourceFeatureMatcherC::ValidateParameters(MultisourceFeatureMatcherParametersC& parameters)
{
	if(parameters.nThreads==0)
		invalid_argument(string("MultisourceFeatureMatcherC::ValidateParameters : nThreads must be a positive value. Value=")+lexical_cast<string>(parameters.nThreads));

	parameters.matcherParameters.flagStatic=true;
}	//void ValidateParameters(const MultisourceFeatureMatcherC& parameters)

/**
 * @brief Prepares the matching tasks
 * @param[in] featureCounts Number of features for each source
 * @param[in] parameters Parameters
 * @return A tuple: A list of source pairs ordered wrt/ascending complexity; distribution of threads per task
 * @remarks Thread distribution is taken into account only when there are more cores than tasks
 */
auto MultisourceFeatureMatcherC::MakeTasks(const vector<size_t>& featureCounts, const MultisourceFeatureMatcherParametersC& parameters) -> tuple<vector<SizePairT>, vector<unsigned int> >
{
	//Order the tasks wrt/increasing complexity
	multimap<unsigned int, SizePairT, greater<unsigned int> > orderedTasks;
	double totalComplexity=0;
	size_t nSource=featureCounts.size();
	for(size_t c1=0; c1<nSource; ++c1)
		for(size_t c2=c1+1; c2<nSource; ++c2)
		{
			double complexity=featureCounts[c1]*featureCounts[c2];	//The complexity of the matching tasks is N1xN2
			orderedTasks.emplace( complexity, SizePairT(c1,c2) );
			totalComplexity+=complexity;
		}	//for(size_t c2=c1+1; c2<nSource; ++c2)

	size_t nTask=nSource*(nSource-1)/2;
	vector<SizePairT> taskList(nTask);
	vector<unsigned int> threadDistribution(nTask,1);

	copy(orderedTasks | map_values, taskList.begin());	//Prepare the task list

	//If sequential operation, each task gets all available threads
	if(parameters.flagSequentialOperation)
		fill(threadDistribution,parameters.nThreads);

	//If there are more threads than tasks, some tasks get multiple threads
	if(!parameters.flagSequentialOperation && nTask<parameters.nThreads)
	{
		double nThreadPerComplexity = totalComplexity/(parameters.nThreads-nTask);

		size_t index=0;
		unsigned int allocated=0;
		for(const auto& current : orderedTasks)
		{
			threadDistribution[index]+=floor(nThreadPerComplexity*current.first);
			allocated+=threadDistribution[index];
			++index;
		}	//for(const auto& current : orderedTasks)

		//Distribute the remaining threads
		for(size_t c=0; allocated<parameters.nThreads; ++c, ++allocated)
			++threadDistribution[c];
	}	//if(!parameters.flagSequentialOperation && nTask<parameters.nThreads)

	return make_tuple(taskList, threadDistribution);
}	//tuple<vector<SizePairT>, vector<unsigned int> > MakeTasks(const vector<vector<FeatureT> >& features, const MultisourceFeatureMatcherParametersC& parameters)

}	//MatcherN
}	//SeeSawN

