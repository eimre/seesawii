/**
 * @file NearestNeighbourMatcher.h Public interface for NearestNeighbourMatcherC
 * @author Evren Imre
 * @date 13 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef NEAREST_NEIGHBOUR_MATCHER_H_8135426
#define NEAREST_NEIGHBOUR_MATCHER_H_8135426

#include "NearestNeighbourMatcher.ipp"

namespace SeeSawN
{
namespace MatcherN
{

template<class ProblemT> class NearestNeighbourMatcherC;  ///< Nearest neighbour matcher
struct NearestNeighbourMatcherParametersC;  ///< Parameter object for NearestNeighbourMatcherC
struct NearestNeighbourMatcherDiagnosticsC; ///< Diagnostics object for NearestNeighbourMatcherC

}   //MatcherN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class boost::optional<double>;
extern template std::string boost::lexical_cast<std::string, double>(const double&);
extern template class std::multimap<double, std::size_t, std::greater<double> >;
extern template class std::map<std::size_t, double>;
#endif /* NEAREST_NEIGHBOUR_MATCHER_H_8135426 */
