/**
 * @file MultisourceFeatureMatcher.ipp Implementation of multisource feature matching algorithm
 * @author Evren Imre
 * @date 10 Feb 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef MULTISOURCE_FEATURE_MATCHER_IPP_5082190
#define MULTISOURCE_FEATURE_MATCHER_IPP_5082190

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <omp.h>
#include <map>
#include <tuple>
#include <cstddef>
#include <vector>
#include <type_traits>
#include <cmath>
#include <stdexcept>
#include <functional>
#include "FeatureMatcher.h"
#include "FeatureMatcherProblemConcept.h"
#include "CorrespondenceFusion.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Coordinate.h"
#include "../Elements/FeatureConcept.h"
#include "../Metrics/BinaryConstraintConcept.h"

namespace SeeSawN
{
namespace MatcherN
{

using boost::optional;
using boost::BinaryPredicateConcept;
using boost::range::copy;
using boost::range::fill;
using boost::adaptors::map_values;
using std::map;
using std::multimap;
using std::tuple;
using std::get;
using std::tie;
using std::vector;
using std::size_t;
using std::is_same;
using std::max;
using std::min;
using std::floor;
using std::invalid_argument;
using std::greater;
using SeeSawN::MatcherN::FeatureMatcherC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;
using SeeSawN::MatcherN::FeatureMatcherDiagnosticsC;
using SeeSawN::MatcherN::FeatureMatcherParametersC;
using SeeSawN::ElementsN::CorrespondenceListT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::MetricsN::BinaryConstraintConceptC;

/**
 * @brief Parameters for \c MultisourceFeatureMatcherC
 * @ingroup Parameters
 */
struct MultisourceFeatureMatcherParametersC
{
	unsigned int nThreads;	///< Number of threads available to the algorithm
	unsigned int minCardinality;	///< Minimum cluster cardinality
	bool flagConsistency;	///< If \c true , consistency constraint is enforced (i.e., a cluster cannot have multiple features from the same source)

	bool flagSequentialOperation;	///< If \c true, the image pairs are processed sequentially. Recommended for large datasets and low-memory environments

	FeatureMatcherParametersC matcherParameters;	///< Feature matcher parameters

	MultisourceFeatureMatcherParametersC() : nThreads(1), minCardinality(2), flagConsistency(true), flagSequentialOperation(false)
	{}
};	//struct MultisourceFeatureMatcherParametersC

/**
 * @brief Diagnostics for \c MultisourceFeatureMatcherC
 * @ingroup Diagnostics
 */
struct MultisourceFeatureMatcherDiagnosticsC
{
	bool flagSuccess;	///< \c true if the algorithm terminates successfully

	map<tuple<size_t, size_t>, FeatureMatcherDiagnosticsC > matcherDiagnostics;	///< Feature matcher diagnostics. [Source pair id; matcher diagnostics]

	unsigned int nObserved;	///< Total number of observed correspondences
	unsigned int nImplied;	///< Total number of implied correspondences
	unsigned int nClusters;	///< Number of clusters
	unsigned int nSourcePairs;	///< Number of source pairs

	MultisourceFeatureMatcherDiagnosticsC() : flagSuccess(false), nObserved(0), nImplied(0), nClusters(0), nSourcePairs(0)
	{}
};	//struct MultisourceFeatureMatcherDiagnosticsC

/**
 * @brief Multisource feature matching algorithm
 * @remarks No similarity test for the implied correspondences: if they were similar, they would be observed. Mainly for facilitating wide-baseline image correspondences
 * @remarks Thread distribution:
 * 	- Order the source pairs with increasing complexity
 * 	- If more tasks than threads, guided
 * 	- If more threads than tasks, distribute the excess wrt complexity
 * @remarks Untested: too complex for a unit test
 * @ingroup Algorithm
 */
class MultisourceFeatureMatcherC
{
	private:

		typedef tuple<size_t, size_t> SizePairT;	///< Type for a size_t pair

		typedef CorrespondenceFusionC::unified_view_type UnifiedViewT;	///< Type for a collection of clusters
		typedef CorrespondenceFusionC::correspondence_container_type CorrespondenceContainerT;	///< Correspondence container type
		typedef CorrespondenceFusionC::pairwise_view_type<CorrespondenceContainerT> PairwiseViewT;	///< Pairwise view type for a collection of clusters

		/** @name Implementation details */ //@{
		static void ValidateParameters(MultisourceFeatureMatcherParametersC& parameters);	///< Validates the parameters
		static tuple<vector<SizePairT>, vector<unsigned int> > MakeTasks(const vector<size_t>& featureCounts, const MultisourceFeatureMatcherParametersC& parameters);	///< Prepares the matching tasks
		template<class FeatureT, class ConstraintT> static PairwiseViewT FilterImplied(const PairwiseViewT& unfiltered, const vector<vector<FeatureT> >& features, const map<SizePairT, ConstraintT> & constraints);	///< Filters the implied correspondences with the pairwise constraints
		//@}

	public:

		typedef UnifiedViewT unified_view_type;	///< Cluster view
		typedef PairwiseViewT pairwise_view_type;	///< Pairwise view
		template<class ProblemT> static MultisourceFeatureMatcherDiagnosticsC Run(UnifiedViewT& clusters, PairwiseViewT& observed, PairwiseViewT& implied, const vector<vector<typename ProblemT::feature_type1> >& features, const map<SizePairT, typename ProblemT::constraint_type>& constraints, const typename ProblemT::similarity_type& similarity, MultisourceFeatureMatcherParametersC parameters);	///< Runs the algorithm

};	//class MultisourceFeatureMatcherC

/**
 * @brief Filters the implied correspondences with the pairwise constraints
 * @tparam FeatureT A feature
 * @tparam ConstraintT A matching constraint
 * @param[in] unfiltered Unfiltered correspondences
 * @param[in] features Image features
 * @param[in] constraints Pairwise constraints
 * @return Filtered correspondences
 * @pre \c FeatureT is a model of \c FeatureConceptC
 * @pre \c ConstraintT is a model of \c BinaryConstraintConceptC
 */
template<class FeatureT, class ConstraintT>
auto MultisourceFeatureMatcherC::FilterImplied(const PairwiseViewT& unfiltered, const vector<vector<FeatureT> >& features, const map<SizePairT, ConstraintT >& constraints) -> PairwiseViewT
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<FeatureT>));
	BOOST_CONCEPT_ASSERT((BinaryConstraintConceptC<ConstraintT, typename FeatureT::coordinate_type, typename FeatureT::coordinate_type>));

	PairwiseViewT filtered;	//Output

	auto itConstraintE=constraints.end();
	for(const auto& current : unfiltered)
	{
		//Is there a constraint?
		auto itConstraint=constraints.find(current.first);

		//If no constraint, all implied correspondences pass
		if(itConstraint==itConstraintE)
		{
			filtered[current.first]=current.second;
			continue;
		}	//if(itConstraint==itConstraintE)

		//Else, verify
		CorrespondenceContainerT& currentFiltered=filtered[current.first];

		size_t id1=get<0>(current.first);
		size_t id2=get<1>(current.first);

		for(const auto& currentCorrespondence : current.second)
		{
			const Coordinate2DT& coordinate1=features[id1][currentCorrespondence.left].Coordinate();
			const Coordinate2DT& coordinate2=features[id2][currentCorrespondence.right].Coordinate();

			if( (itConstraint->second)(coordinate1, coordinate2))
				currentFiltered.push_back(currentCorrespondence);
		}	//for(const auto& currentCorrespondence : current.second)
	}	//for(const auto& current : unfiltered)

	return filtered;
}	//PairwiseViewT FilterImplied(const PairwiseViewT& unfiltered, const map<SizePairT, optional<ConstraintT> >& constraints)

/**
 * @brief Runs the algorithm
 * @tparam ProblemT A matching problem
 * @param[out] clusters Features corresponding to the same entity in different sources
 * @param[out] observed Observed pairwise correspondences
 * @param[out] implied Implied pairwise correspondences
 * @param[in] features Feature sources
 * @param[in] constraints Constraints between the sources. Does not have to have an element for each pair. [Source pair id, constraint]
 * @param[in] similarity Feature similarity metric
 * @param[in] parameters Parameters. Pass-by-value on purpose
 * @return Diagnostics object
 * @pre \c ProblemT is a feature matching problem
 * @pre \c ProblemT::feature_type1 and \c ProblemT::feature_type2 are the same
 */
template<class ProblemT>
MultisourceFeatureMatcherDiagnosticsC MultisourceFeatureMatcherC::Run(UnifiedViewT& clusters, PairwiseViewT& observed, PairwiseViewT& implied, const vector<vector<typename ProblemT::feature_type1> >& features, const map<SizePairT, typename ProblemT::constraint_type>& constraints, const typename ProblemT::similarity_type& similarity,  MultisourceFeatureMatcherParametersC parameters)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<ProblemT>));
	static_assert(is_same<typename ProblemT::feature_type1, typename ProblemT::feature_type2>::value, "MultisourceFeatureMatcherC::Run : Source feature types must be identical.");

	//Validate the parameters
	ValidateParameters(parameters);

	MultisourceFeatureMatcherDiagnosticsC diagnostics;	//Diagnostics object

	//Compute the pairwise correspondences

	//Order the source pairs for better load balancing
	size_t nSource=features.size();
	vector<size_t> featureCounts(nSource);
	for(size_t c=0; c<nSource; ++c)
		featureCounts[c]=features[c].size();

	vector<SizePairT> pairList;
	vector<unsigned int> threadDistribution;
	tie(pairList, threadDistribution)=MakeTasks(featureCounts, parameters);

	int nThreads= parameters.flagSequentialOperation ? 1 : min((size_t)parameters.nThreads, pairList.size()) ;	//Number of threads for the parallel matching tasks

	size_t nPairs=pairList.size();	//Number of source pairs
	diagnostics.nSourcePairs=nPairs;
	auto itConstraintE=constraints.end();

	typename CorrespondenceFusionC::pairwise_view_type<CorrespondenceListT> correspondenceStack;	//Correspondences, in pairwise view
	FeatureMatcherParametersC fmParameters=parameters.matcherParameters;	//Local copy of the parameters object
	size_t c=0;

#pragma omp parallel for if(nThreads>1) schedule(dynamic) private(c) firstprivate(fmParameters) num_threads(nThreads)
	for(c=0; c<nPairs; ++c)
	{
		//Get the constraint
		optional<typename ProblemT::constraint_type> dummyConstraint;	//Default, if no constraint is found
		auto itConstraint=constraints.find(pairList[c]);

		fmParameters.nThreads=threadDistribution[c];

		//Pairwise correspondences
		optional<ProblemT> dummyProblem;
		CorrespondenceListT correspondences;
		FeatureMatcherDiagnosticsC fmDiagnostics=FeatureMatcherC<ProblemT>::Run(correspondences, dummyProblem,fmParameters, similarity, ((itConstraint==itConstraintE) ? dummyConstraint : (itConstraint->second)), features[get<0>(pairList[c])], features[get<1>(pairList[c])]);

	#pragma omp critical (MFM_RUN)
	{
		//Save
		diagnostics.matcherDiagnostics.emplace(pairList[c], fmDiagnostics);
		correspondenceStack[pairList[c]]=correspondences;
	}	//#pragma omp critical (MFM_RUN)
	}	//for(size_t c=0; c<nPairs; ++c)

	//Correspondence fusion
	PairwiseViewT rawImplied;	//Unfiltered implied correspondences
	std::tie(clusters, observed, rawImplied)=CorrespondenceFusionC::Run<CorrespondenceListT>(correspondenceStack, parameters.minCardinality, parameters.flagConsistency);

	//Verify the implied correspondences
	implied=FilterImplied(rawImplied, features, constraints);

	//Diagnostics
	diagnostics.flagSuccess=true;

	diagnostics.nClusters=clusters.size();

	for(const auto& current : implied)
		diagnostics.nImplied+=current.second.size();

	for(const auto& current : observed)
		diagnostics.nObserved+=current.second.size();

	return diagnostics;
}	//MultisourceFeatureMatcherDiagnosticsC Run(UnifiedViewT& clusters, PairwiseViewT& observed, PairwiseViewT& implied, const FeatureStackT& features, const map<SizePairT, optional<ConstraintT> >& constraints, const SimilarityT& similarity, const MultisourceFeatureMatcherParametersC& parameters)

}	//MatcherN
}	//SeeSawN

#endif /* MULTISOURCE_FEATURE_MATCHER_IPP_5082190 */
