/**
 * @file OneSidedFundamentalComponents.ipp Implementation of the components for the 6-point one-sided fundamental matrix estimator
 * @author Evren Imre
 * @date 21 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ONE_SIDED_FUNDAMENTAL_COMPONENTS_IPP_0480243
#define ONE_SIDED_FUNDAMENTAL_COMPONENTS_IPP_0480243

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include "Fundamental8Components.h"
#include "../Geometry/OneSidedFundamentalSolver.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../RANSAC/RANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLOneSidedFundamentalMatrixEstimationProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Numeric/NumericFunctor.h"
#include "../Wrappers/BoostBimap.h"
#include "../Elements/Feature.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::Matrix3d;
using Eigen::MatrixXd;
using std::vector;
using std::tie;
using SeeSawN::GeometryN::RANSACFundamental8ProblemT;
using SeeSawN::GeometryN::OneSidedFundamentalSolverC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GenericGeometryEstimationPipelineProblemC;
using SeeSawN::GeometryN::CoordinateStatistics2DT;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::RANSACN::RANSACC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PDLOneSidedFundamentalMatrixEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::NumericN::ReciprocalC;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::ElementsN::ImageFeatureC;

typedef RANSACGeometryEstimationProblemC<OneSidedFundamentalSolverC, OneSidedFundamentalSolverC> RANSACOSFundamentalProblemT;
typedef PDLOneSidedFundamentalMatrixEstimationProblemC PDLOSFundamentalProblemT;
typedef GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACOSFundamentalProblemT, PDLOSFundamentalProblemT> OSFundamentalEstimatorProblemT;
typedef GenericGeometryEstimationPipelineProblemC<OSFundamentalEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > OSFundamentalPipelineProblemT;

/**
 * @brief Makes a geometry estimation pipeline problem for 6-point one-sided fundamental matrix estimation
 * @param[in] featureSet1 First feature set
 * @param[in] featureSet2 Second feature set
 * @param[in] geometryEstimationProblem Problem for the geometry estimator
 * @param[in] noiseVariance	Coordinate noise variance (pixel^2)
 * @param[in] pRejection Inlier rejection probability
 * @param[in] initialEstimate Initial one-sided fundamental matrix estimate. Invalid, if there is none
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier (for the first guided matching pass)
 * @param[in] nThreads Number of threads
 * @return Problem object
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @ingroup OneSidedFundamental
 */
template<class FeatureRange1T, class FeatureRange2T>
OSFundamentalPipelineProblemT MakeGeometryEstimationPipelineOSFundamentalProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const OSFundamentalEstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)
{
	ReciprocalC<typename EuclideanDistanceIDT::result_type> inverter;
	InverseEuclideanIDConverterT featureSimilarity(EuclideanDistanceIDT(), inverter);

	//Initial constraint
	optional<EpipolarSampsonConstraintT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=EpipolarSampsonConstraintT(EpipolarSampsonErrorC(*initialEstimate), inlierTh, nThreads);
	}	//if(initialEstimate)

	EpipolarSampsonConstraintT shellConstraint(EpipolarSampsonErrorC(), EpipolarSampsonErrorC::ComputeOutlierThreshold(pRejection, noiseVariance), nThreads);

	vector<typename OSFundamentalPipelineProblemT::covariance_type1> covarianceList(1);
	covarianceList[0]<<noiseVariance,0, 0, noiseVariance;

	return OSFundamentalPipelineProblemT(featureSet1, featureSet2, covarianceList, covarianceList, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//OSFundamentalPipelineProblemT MakeGeometryEstimationPipelineOSFundamentalProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const OSFundamentalEstimatorProblemT& geometryEstimationProblem, double noiseVariance, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)

}	//GeometryN
}	//SeeSawN

#endif /* ONE_SIDED_FUNDAMENTAL_COMPONENTS_IPP_0480243 */
