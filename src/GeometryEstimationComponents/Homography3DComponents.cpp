/**
 * @file Homography3DComponents.cpp Implementation 3D homography estimation pipeline components
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Homography3DComponents.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 3D homography estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for 3D homography estimation. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup Homography3D
 */
RANSACHomography3DProblemT MakeRANSACHomography3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACHomography3DProblemT::dimension_type1& binSize1, const RANSACHomography3DProblemT::dimension_type2& binSize2)
{
	double inlierTh=SymmetricTransferErrorH3DT::ComputeOutlierThreshold(pRejection, noiseVariance);

	Homography3DSolverC::loss_type lossFunction(inlierTh, inlierTh);
	SymmetricTransferErrorH3DT errorMetric;
	SymmetricTransferErrorConstraint3DT constraint(errorMetric, inlierTh, 1);

	Homography3DSolverC solver;
	return RANSACHomography3DProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACHomography3DProblemT MakeRANSACHomography3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, double binDensity)

/**
 * @brief Makes a PDL problem for 3D homography estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for 3D homography estimation
 * @remarks No unit test. Tested through the applications
 * @ingroup Homography3D
 */
PDLHomography3DProblemT MakePDLHomography3DProblem(const Homography3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	SymmetricTransferErrorH3DT errorMetric;
	Homography3DSolverC solver;
	return PDLHomography3DProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLHomography3DProblemT MakePDLHomography3DProblem(const Homography3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>())

/********** EXPLICIT INSTANTIATIONS **********/
template Homography3DPipelineProblemT MakeGeometryEstimationPipelineHomography3DProblem(const vector<SceneFeatureC>&, const vector<SceneFeatureC>&, const Homography3DEstimatorProblemT&, const vector<Homography3DPipelineProblemT::covariance_type1>&, const vector<Homography3DPipelineProblemT::covariance_type2>&, double, double, const optional<Matrix4d>&, double, double, unsigned int);
template class GenericGeometryEstimationProblemC<RANSACHomography3DProblemT, RANSACHomography3DProblemT, PDLHomography3DProblemT>;
template class GenericGeometryEstimationPipelineProblemC<Homography3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> >;

template class GeometryEstimationPipelineC<Homography3DPipelineProblemT>;
}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Homography3DSolverC, GeometryN::Homography3DSolverC>;
template class OptimisationN::PDLHomographyXDEstimationProblemC<GeometryN::Homography3DSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Homography3DSolverC>;

}	//SeeSawN

