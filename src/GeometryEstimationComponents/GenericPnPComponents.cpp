/**
 * @file GenericPnPComponents.cpp Instantiations for PnP component generators
 * @author Evren Imre
 * @date 3 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "GenericPnPComponents.h"

namespace SeeSawN
{
namespace GeometryN
{

/********** EXPLICIT INSTANTIATIONS **********/
template GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, HammingSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<OrientedBinarySceneFeatureC>&, const vector<BinaryImageFeatureC>&, const P2PEstimatorProblemT&, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);
template RANSACP2PProblemT MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT&, const CoordinateCorrespondenceList32DT&, double, double, double, unsigned int, const typename RANSACP2PProblemT::dimension_type1&, const typename RANSACP2PProblemT::dimension_type2&);
template PDLP2PProblemT MakePDLPnPProblem(const typename PDLP2PProblemT::model_type&, const CoordinateCorrespondenceList32DT&, const optional<MatrixXd>&);

template GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, HammingSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<OrientedBinarySceneFeatureC>& featureSet1, const vector<BinaryImageFeatureC>& featureSet2, const P2PfEstimatorProblemT& geometryEstimationProblem, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);
template RANSACP2PfProblemT MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT&, const CoordinateCorrespondenceList32DT&, double, double, double, unsigned int, const typename RANSACP2PfProblemT::dimension_type1& binSize1, const typename RANSACP2PfProblemT::dimension_type2& binSize2);
template PDLP2PfProblemT MakePDLPnPProblem(const typename PDLP2PfProblemT::model_type&, const CoordinateCorrespondenceList32DT&, const optional<MatrixXd>&);

template GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, EuclideanSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PEstimatorProblemT&, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);
template GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, EuclideanSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PfEstimatorProblemT&, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);

//<PDLP2PProblemT, P2PSolverC>
}	//namespace SeeSawN
}	//namespace GeometryN


