/**
 * @file Rotation3DComponents.ipp Implementation 3D rotation estimation pipeline components
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROTATION_3D_COMPONENTS_IPP_9033122
#define ROTATION_3D_COMPONENTS_IPP_9033122

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLRotation3DEstimationProblem.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../Geometry/Rotation3DSolver.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Matcher/SceneFeatureMatchingProblem.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Metrics/SceneFeatureDistance.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Elements/GeometricEntity.h"
#include "../Wrappers/BoostBimap.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::Matrix4d;
using std::vector;
using std::tie;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GenericGeometryEstimationPipelineProblemC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::Rotation3DSolverC;
using SeeSawN::GeometryN::CoordinateTransformations3DT;
using SeeSawN::GeometryN::CoordinateStatistics3DT;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PDLRotation3DEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::MatcherN::SceneFeatureMatchingProblemC;
using SeeSawN::MetricsN::InverseEuclideanSceneFeatureDistanceConverterT;
using SeeSawN::MetricsN::SymmetricTransferErrorConstraint3DT;
using SeeSawN::MetricsN::SymmetricTransferErrorH3DT;
using SeeSawN::MetricsN::EuclideanSceneFeatureDistanceT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList3DT;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::NumericN::ReciprocalC;

/** @name Similarity3D */ //@{
typedef RANSACGeometryEstimationProblemC<Rotation3DSolverC, Rotation3DSolverC> RANSACRotation3DProblemT;
typedef PDLRotation3DEstimationProblemC PDLRotation3DProblemT;
typedef GenericGeometryEstimationProblemC<RANSACRotation3DProblemT, RANSACRotation3DProblemT, PDLRotation3DProblemT> Rotation3DEstimatorProblemT;
typedef GenericGeometryEstimationPipelineProblemC<Rotation3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> >Rotation3DPipelineProblemT;
//@}

/**
 * @brief Makes a geometry estimation pipeline problem for 3D rotation estimation
 * @tparam FeatureRange1T A feature range for the first set
 * @tparam FeatureRange2T A feature range for the second set
 * @tparam CovarianceRange1T A covariance range for the first set
 * @tparam CovarianceRange2T A covariance range for the second set
 * @param[in] featureSet1 First feature set
 * @param[in] featureSet2 Second feature set
 * @param[in] geometryEstimationProblem Problem for the geometry estimator
 * @param[in] covarianceList1 Covariances for the elements of the first set
 * @param[in] covarianceList2 Covariances for the elements of the second set
 * @param[in] effectiveNoiseVariance Coordinate noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] initialEstimate Initial rotation estimate. Invalid, if there is none
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier (for the first guided matching pass)
 * @param[in] nThreads Number of threads
 * @return Problem object
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @ingroup Rotation3D
 */

template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T, class CovarianceRange2T>
Rotation3DPipelineProblemT MakeGeometryEstimationPipelineRotation3DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Rotation3DEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, const CovarianceRange2T& covarianceList2, double effectiveNoiseVariance, double pRejection, const optional<Matrix4d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)
{
	ReciprocalC<typename EuclideanSceneFeatureDistanceT::result_type> inverter;
	InverseEuclideanSceneFeatureDistanceConverterT featureSimilarity(EuclideanSceneFeatureDistanceT(), inverter);

	//Initial constraint
	optional<SymmetricTransferErrorConstraint3DT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=SymmetricTransferErrorH3DT::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=SymmetricTransferErrorConstraint3DT(SymmetricTransferErrorH3DT(*initialEstimate), inlierTh, nThreads);
	}	//if(initialEstimate)

	SymmetricTransferErrorConstraint3DT shellConstraint(SymmetricTransferErrorH3DT(), SymmetricTransferErrorH3DT::ComputeOutlierThreshold(pRejection, effectiveNoiseVariance), nThreads);

	return Rotation3DPipelineProblemT(featureSet1, featureSet2, covarianceList1, covarianceList2, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//Homography3DPipelineProblemT MakeGeometryEstimationPipelineHomography2DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Homography2DEstimatorProblemT& geometryEstimationProblem, double noiseVariance, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)

}	//GeometryN
}	//SeeSawN

#endif /* ROTATION_3D_COMPONENTS_IPP_9033122 */
