/**
 * @file P2PfComponents.h Public interface for the geometry estimation pipeline components for the 2-point orientation and focal length solver
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2PF_COMPONENTS_H_6918021
#define P2PF_COMPONENTS_H_6918021

#include "P2PfComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

typedef RANSACGeometryEstimationProblemC<P2PfSolverC, P2PfSolverC> RANSACP2PfProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACP2PfProblemT> RANSACP2PfEngineT;	///< Two-stage RANSAC

typedef PDLP2PfProblemC PDLP2PfProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLP2PfProblemT> PDLP2PfEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACP2PfProblemT, RANSACP2PfProblemT, PDLP2PfProblemT> P2PfEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<P2PfEstimatorProblemT> P2PfEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<P2PfSolverC> SUTP2PfProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTP2PfProblemT> SUTP2PfEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> > P2PfPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<P2PfPipelineProblemT> P2PfPipelineT;	///< Pipeline

//Problem makers
RANSACP2PfProblemT MakeRANSACP2PfProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP2PfProblemT::dimension_type1& binSize1, const RANSACP2PfProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 2-point orientation and focal length estimation
PDLP2PfProblemT MakePDLP2PfProblem(const P2PfSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 2-point orientation and focal length estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T> P2PfPipelineProblemT MakeGeometryEstimationPipelineP2PfProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P2PfEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 2-point orientation and focal length estimation

/********** EXTERN TEMPLATES **********/
extern template P2PfPipelineProblemT MakeGeometryEstimationPipelineP2PfProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PfEstimatorProblemT&, const vector<P2PfPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

extern template class GenericGeometryEstimationProblemC<RANSACP2PfProblemT, RANSACP2PfProblemT, PDLP2PfProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
extern template class GeometryEstimationPipelineC<P2PfPipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P2PfSolverC, GeometryN::P2PfSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P2PfSolverC>;

}	//SeeSawN

#endif /* P2PF_COMPONENTS_H_6918021 */
