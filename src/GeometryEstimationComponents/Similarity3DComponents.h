/**
 * @file Similarity3DComponents.h Public interface 3D similarity transformation estimation pipeline components
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef SIMILARITY_3D_COMPONENTS_H_5380932
#define SIMILARITY_3D_COMPONENTS_H_5380932

#include "Similarity3DComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{
typedef RANSACGeometryEstimationProblemC<Similarity3DSolverC, Similarity3DSolverC> RANSACSimilarity3DProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACSimilarity3DProblemT> RANSACSimilarity3DEngineT;	///< Two-stage RANSAC

typedef PDLSimilarity3DEstimationProblemC PDLSimilarity3DProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLSimilarity3DProblemT> PDLSimilarity3DEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACSimilarity3DProblemT, RANSACSimilarity3DProblemT, PDLSimilarity3DProblemT> Similarity3DEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<Similarity3DEstimatorProblemT> Similarity3DEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<Similarity3DSolverC> SUTSimilarity3DProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTSimilarity3DProblemT> SUTSimilarity3DEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<Similarity3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> > Similarity3DPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<Similarity3DPipelineProblemT> Similarity3DPipelineT;	///< Pipeline

//Problem makers
RANSACSimilarity3DProblemT MakeRANSACSimilarity3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACSimilarity3DProblemT::dimension_type1& binSize1, const RANSACSimilarity3DProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 3D similarity transformation estimation
PDLSimilarity3DProblemT MakePDLSimilarity3DProblem(const Similarity3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 3D similarity transformation estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T, class CovarianceRange2T> Similarity3DPipelineProblemT MakeGeometryEstimationPipelineSimilarity3DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Similarity3DEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, const CovarianceRange2T& covarianceList2, double effectiveNoiseVariance, double pRejection, const optional<Matrix4d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 3D similarity transformation estimation

/********** EXTERN TEMPLATES **********/
extern template Similarity3DPipelineProblemT MakeGeometryEstimationPipelineSimilarity3DProblem(const vector<SceneFeatureC>&, const vector<SceneFeatureC>&, const Similarity3DEstimatorProblemT&, const vector<Similarity3DPipelineProblemT::covariance_type1>&, const vector<Similarity3DPipelineProblemT::covariance_type2>&, double, double, const optional<Matrix4d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACSimilarity3DProblemT, RANSACSimilarity3DProblemT, PDLSimilarity3DProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<Similarity3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> >;

extern template class GeometryEstimationPipelineC<Similarity3DPipelineProblemT>;
}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Similarity3DSolverC, GeometryN::Similarity3DSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Similarity3DSolverC>;


}	//SeeSawN

#endif /* SIMILARITY_3D_COMPONENTS_H_5380932 */
