/**
 * @file Homography2DComponents.cpp Implementation and instantiations for the components of the 2D homography estimation pipeline
 * @author Evren Imre
 * @date 25 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Homography2DComponents.h"

namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 2D homography estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for 2D homography estimation. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup Homography2D
 */
RANSACHomography2DProblemT MakeRANSACHomography2DProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACHomography2DProblemT::dimension_type1& binSize1, const RANSACHomography2DProblemT::dimension_type2& binSize2)
{
	double inlierTh=SymmetricTransferErrorH2DT::ComputeOutlierThreshold(pRejection, noiseVariance);

	Homography2DSolverC::loss_type lossFunction(inlierTh, inlierTh);
	SymmetricTransferErrorH2DT errorMetric;
	SymmetricTransferErrorConstraint2DT constraint(errorMetric, inlierTh, 1);

	Homography2DSolverC solver;
	return RANSACHomography2DProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACHomography2DProblemT MakeRANSACHomography2DProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double modelSimilarityTh, unsigned int loGeneratorSize, double binDensity)

/**
 * @brief Makes a PDL problem for 2D homography estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for 2D homography estimation
 * @remarks No unit test. Tested through the applications
 * @ingroup Homography2D
 */
PDLHomography2DProblemT MakePDLHomography2DProblem(const Homography2DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	SymmetricTransferErrorH2DT errorMetric;
	Homography2DSolverC solver;
	return PDLHomography2DProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLHomography2DEstimationProblemC MakePDLHomography2DEstimationProblem(const Homography2DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet)

/********** EXPLICIT INSTANTIATIONS **********/
template Homography2DPipelineProblemT MakeGeometryEstimationPipelineHomography2DProblem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const Homography2DEstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
template class GenericGeometryEstimationProblemC<RANSACHomography2DProblemT, RANSACHomography2DProblemT, PDLHomography2DProblemT>;
template class GenericGeometryEstimationPipelineProblemC<Homography2DEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, SymmetricTransferErrorConstraint2DT> >;

template class GeometryEstimationPipelineC<Homography2DPipelineProblemT>;
}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Homography2DSolverC, GeometryN::Homography2DSolverC>;
template class OptimisationN::PDLHomographyXDEstimationProblemC<GeometryN::Homography2DSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Homography2DSolverC>;

}	//SeeSawN


