/**
 * @file P2PComponents.h Public interface for the geometry estimation pipeline components for the 2-point orientation solver
 * @author Evren Imre
 * @date 8 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2P_COMPONENTS_H_8909801
#define P2P_COMPONENTS_H_8909801

#include "P2PComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

typedef RANSACGeometryEstimationProblemC<P2PSolverC, P2PSolverC> RANSACP2PProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACP2PProblemT> RANSACP2PEngineT;	///< Two-stage RANSAC

typedef PDLP2PProblemC PDLP2PProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLP2PProblemT> PDLP2PEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACP2PProblemT, RANSACP2PProblemT, PDLP2PProblemT> P2PEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<P2PEstimatorProblemT> P2PEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<P2PSolverC> SUTP2PProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTP2PProblemT> SUTP2PEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> > P2PPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<P2PPipelineProblemT> P2PPipelineT;	///< Pipeline

//Problem makers
RANSACP2PProblemT MakeRANSACP2PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP2PProblemT::dimension_type1& binSize1, const RANSACP2PProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 2-point orientation estimation
PDLP2PProblemT MakePDLP2PProblem(const P2PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 2-point orientation estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T> P2PPipelineProblemT MakeGeometryEstimationPipelineP2PProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P2PEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 2-point orientation estimation

/********** EXTERN TEMPLATES **********/
extern template P2PPipelineProblemT MakeGeometryEstimationPipelineP2PProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PEstimatorProblemT&, const vector<P2PPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

extern template class GenericGeometryEstimationProblemC<RANSACP2PProblemT, RANSACP2PProblemT, PDLP2PProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
extern template class GeometryEstimationPipelineC<P2PPipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P2PSolverC, GeometryN::P2PSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P2PSolverC>;
}	//SeeSawN

#endif /* P2P_COMPONENTS_H_8909801 */
