/**
 * @file Homography2DComponents.h Public interface for the components of the 2D homography estimation pipeline
 * @author Evren Imre
 * @date 25 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY_2D_COMPONENTS_H_9192132
#define HOMOGRAPHY_2D_COMPONENTS_H_9192132

#include "Homography2DComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

/** @name Homography2D */ //@{
typedef RANSACGeometryEstimationProblemC<Homography2DSolverC, Homography2DSolverC> RANSACHomography2DProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACHomography2DProblemT> RANSACHomography2DEngineT;	///< Two-stage RANSAC

typedef PDLHomographyXDEstimationProblemC<Homography2DSolverC> PDLHomography2DProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLHomography2DProblemT> PDLHomography2DEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACHomography2DProblemT, RANSACHomography2DProblemT, PDLHomography2DProblemT> Homography2DEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<Homography2DEstimatorProblemT> Homography2DEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<Homography2DSolverC> SUTHomography2DProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTHomography2DProblemT> SUTHomography2DEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<Homography2DEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, SymmetricTransferErrorConstraint2DT> > Homography2DPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<Homography2DPipelineProblemT> Homography2DPipelineT;	///< Pipeline
//@}

//Problem makers
RANSACHomography2DProblemT MakeRANSACHomography2DProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACHomography2DProblemT::dimension_type1& binSize1, const RANSACHomography2DProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 2D homography estimation
PDLHomography2DProblemT MakePDLHomography2DProblem(const Homography2DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 2D homography estimation
template<class FeatureRange1T, class FeatureRange2T> Homography2DPipelineProblemT MakeGeometryEstimationPipelineHomography2DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Homography2DEstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 2D homography estimation

/********** EXTERN TEMPLATES **********/
extern template Homography2DPipelineProblemT MakeGeometryEstimationPipelineHomography2DProblem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const Homography2DEstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACHomography2DProblemT, RANSACHomography2DProblemT, PDLHomography2DProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<Homography2DEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, SymmetricTransferErrorConstraint2DT> >;

extern template class GeometryEstimationPipelineC<Homography2DPipelineProblemT>;
}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Homography2DSolverC, GeometryN::Homography2DSolverC>;
extern template class OptimisationN::PDLHomographyXDEstimationProblemC<GeometryN::Homography2DSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Homography2DSolverC>;
}	//SeeSawN


//@}



#endif /* HOMOGRAPHY_2D_COMPONENTS_H_9192132 */
