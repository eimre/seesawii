/**
 * @file P4PComponents.cpp Implementation of the geometry estimation pipeline components for the 4-point pose and focal length solver
 * @author Evren Imre
 * @date 19 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P4PComponents.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 4-point pose and focal length estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Image noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for P4P. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup P4P
 */
RANSACP4PProblemT MakeRANSACP4PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP4PProblemT::dimension_type1& binSize1, const RANSACP4PProblemT::dimension_type2& binSize2)
{
	double inlierTh=TransferErrorH32DT::ComputeOutlierThreshold(pRejection, noiseVariance);

	P4PSolverC::loss_type lossFunction(inlierTh, inlierTh);
	TransferErrorH32DT errorMetric;
	ReprojectionErrorConstraintT constraint(errorMetric, inlierTh, 1);

	P4PSolverC solver;
	return RANSACP4PProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACP4PProblemT MakeRANSACP4PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP4PProblemT::dimension_type1& binSize1, const RANSACP4PProblemT::dimension_type2& binSize2)

/**
 * @brief Makes a PDL problem for 4-point pose and focal length estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for P4P
 * @remarks No unit test. Tested through the applications
 * @ingroup P4P
 */
PDLP4PProblemT MakePDLP4PProblem(const P4PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	TransferErrorH32DT errorMetric;
	P4PSolverC solver;
	return PDLP4PProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLP4PProblemT MakePDLP4PProblem(const P4PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)

/********** EXTERN TEMPLATES **********/
template P4PPipelineProblemT MakeGeometryEstimationPipelineP4PProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P4PEstimatorProblemT&, const vector<P4PPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

template class GenericGeometryEstimationProblemC<RANSACP4PProblemT, RANSACP4PProblemT, PDLP4PProblemT>;
template class GenericGeometryEstimationPipelineProblemC<P4PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
template class GeometryEstimationPipelineC<P4PPipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P4PSolverC, GeometryN::P4PSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P4PSolverC>;

}	//SeeSawN

