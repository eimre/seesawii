/**
 * @file Homography2DComponents.ipp Implementation details for the components of the 2D homography estimation pipeline
 * @author Evren Imre
 * @date 25 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY_2D_COMPONENTS_IPP_7008032
#define HOMOGRAPHY_2D_COMPONENTS_IPP_7008032

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLHomographyXDEstimationProblem.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../Geometry/Homography2DSolver.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Coordinate.h"
#include "../Elements/Feature.h"
#include "../Numeric/NumericFunctor.h"
#include "../Wrappers/BoostBimap.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::MatrixXd;
using Eigen::Matrix3d;
using std::vector;
using std::tie;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GenericGeometryEstimationPipelineProblemC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::Homography2DSolverC;
using SeeSawN::GeometryN::CoordinateStatistics2DT;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PDLHomographyXDEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::MetricsN::SymmetricTransferErrorConstraint2DT;
using SeeSawN::MetricsN::SymmetricTransferErrorH2DT;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::NumericN::ReciprocalC;

/** @name Homography2D */ //@{
typedef RANSACGeometryEstimationProblemC<Homography2DSolverC, Homography2DSolverC> RANSACHomography2DProblemT;	///< RANSAC problem
typedef PDLHomographyXDEstimationProblemC<Homography2DSolverC> PDLHomography2DProblemT;	///< Optimisation problem
typedef GenericGeometryEstimationProblemC<RANSACHomography2DProblemT, RANSACHomography2DProblemT, PDLHomography2DProblemT> Homography2DEstimatorProblemT;	///< Geometry estimator problem
typedef GenericGeometryEstimationPipelineProblemC<Homography2DEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, SymmetricTransferErrorConstraint2DT> > Homography2DPipelineProblemT;
//@}

/**
 * @brief Makes a geometry estimation pipeline problem for 2D homography estimation
 * @tparam FeatureRange1T A feature range for the first set
 * @tparam FeatureRange2T A feature range for the second set
 * @param[in] featureSet1 First feature set
 * @param[in] featureSet2 Second feature set
 * @param[in] geometryEstimationProblem Problem for the geometry estimator
 * @param[in] noiseVariance	Coordinate noise variance (pixel^2)
 * @param[in] pRejection Inlier rejection probability
 * @param[in] initialEstimate Initial homography estimate. Invalid, if there is none
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier (for the first guided matching pass)
 * @param[in] nThreads Number of threads
 * @return Problem object
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @ingroup Homography2D
 */
template<class FeatureRange1T, class FeatureRange2T>
Homography2DPipelineProblemT MakeGeometryEstimationPipelineHomography2DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Homography2DEstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)
{
	ReciprocalC<typename EuclideanDistanceIDT::result_type> inverter;
	InverseEuclideanIDConverterT featureSimilarity(EuclideanDistanceIDT(), inverter);

	//Initial constraint
	optional<SymmetricTransferErrorConstraint2DT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=SymmetricTransferErrorH2DT::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=SymmetricTransferErrorConstraint2DT(SymmetricTransferErrorH2DT(*initialEstimate), inlierTh, nThreads);
	}	//if(initialEstimate)

	SymmetricTransferErrorConstraint2DT shellConstraint(SymmetricTransferErrorH2DT(), SymmetricTransferErrorH2DT::ComputeOutlierThreshold(pRejection, noiseVariance), nThreads);

	vector<typename Homography2DPipelineProblemT::covariance_type1> covarianceList(1);
	covarianceList[0]<<noiseVariance,0, 0, noiseVariance;

	return Homography2DPipelineProblemT(featureSet1, featureSet2, covarianceList, covarianceList, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//Homography2DEstimationPipelineProblemT MakeGeometryEstimationPipelineHomography2DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const RANSACHomography2DProblemT& ransacProblem1, const RANSACHomography2DProblemT& ransacProblem1, const RANSACHomography2DProblemT& ransacProblem2, const PDLHomography2DEstimationProblemC& optimisationProblem, double initialPassFactor)


}	//GeometryN
}	//SeeSawN

#endif /* HOMOGRAPHY_2D_COMPONENTS_IPP_7008032 */
