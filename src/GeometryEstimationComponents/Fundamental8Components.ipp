/**
 * @file Fundamental8Components.ipp Implementation details for the 8-point fundamental matrix estimation pipeline
 * @author Evren Imre
 * @date 8 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FUNDAMENTAL8_COMPONENTS_IPP_5093342
#define FUNDAMENTAL8_COMPONENTS_IPP_5093342

#include <boost/optional.hpp>
#include <tuple>
#include <vector>
#include "Fundamental7Components.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/GeometricConstraint.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLFundamentalMatrixEstimationProblem.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../Geometry/FundamentalSolver.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/BoostBimap.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using std::tie;
using std::vector;
using SeeSawN::GeometryN::PDLFundamentalMatrixEstimationProblemC;
using SeeSawN::GeometryN::MakePDLFundamentalProblem;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GenericGeometryEstimationPipelineProblemC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::Fundamental8SolverC;
using SeeSawN::GeometryN::CoordinateStatistics2DT;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PDLFundamentalMatrixEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::NumericN::ReciprocalC;

typedef RANSACGeometryEstimationProblemC<Fundamental8SolverC, Fundamental8SolverC> RANSACFundamental8ProblemT;
typedef GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACFundamental8ProblemT, PDLFundamentalProblemT> Fundamental8EstimatorProblemT;
typedef GenericGeometryEstimationPipelineProblemC<Fundamental8EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > Fundamental8PipelineProblemT;

/**
 * @brief Makes a geometry estimation pipeline problem for 8-point fundamental matrix estimation
 * @param[in] featureSet1 First feature set
 * @param[in] featureSet2 Second feature set
 * @param[in] geometryEstimationProblem Problem for the geometry estimator
 * @param[in] noiseVariance	Coordinate noise variance (pixel^2)
 * @param[in] pRejection Inlier rejection probability
 * @param[in] initialEstimate Initial fundamental matrix estimate. Invalid, if there is none
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier (for the first guided matching pass)
 * @param[in] nThreads Number of threads
 * @return Problem object
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @ingroup Fundamental8
 */
template<class FeatureRange1T, class FeatureRange2T>
Fundamental8PipelineProblemT MakeGeometryEstimationPipelineFundamental8Problem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Fundamental8EstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)
{
	ReciprocalC<typename EuclideanDistanceIDT::result_type> inverter;
	InverseEuclideanIDConverterT featureSimilarity(EuclideanDistanceIDT(), inverter);

	//Initial constraint
	optional<EpipolarSampsonConstraintT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=EpipolarSampsonConstraintT(EpipolarSampsonErrorC(*initialEstimate), inlierTh, nThreads);
	}	//if(initialEstimate)

	EpipolarSampsonConstraintT shellConstraint(EpipolarSampsonErrorC(), EpipolarSampsonErrorC::ComputeOutlierThreshold(pRejection, noiseVariance), nThreads);

	vector<typename Fundamental8PipelineProblemT::covariance_type1> covarianceList(1);
	covarianceList[0]<<noiseVariance,0, 0, noiseVariance;

	return Fundamental8PipelineProblemT(featureSet1, featureSet2, covarianceList, covarianceList, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//Fundamental8PipelineProblemT MakeGeometryEstimationPipelineFundamental8Problem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const RANSACHomography2DProblemT& ransacProblem1, const RANSACHomography2DProblemT& ransacProblem1, const RANSACHomography2DProblemT& ransacProblem2, const PDLHomography2DEstimationProblemC& optimisationProblem, double initialPassFactor)

}	//GeometryN
}	//SeeSawN

#endif /* FUNDAMENTAL8_COMPONENTS_IPP_5093342 */
