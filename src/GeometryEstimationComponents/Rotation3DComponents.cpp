/**
 * @file Rotation3DComponents.cpp Implementation 3D rotation estimation pipeline components
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Rotation3DComponents.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 3D rotation estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for 3D rotation estimation. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup Rotation3D
 */
RANSACRotation3DProblemT MakeRANSACRotation3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACRotation3DProblemT::dimension_type1& binSize1, const RANSACRotation3DProblemT::dimension_type2& binSize2)
{
	double inlierTh=SymmetricTransferErrorH3DT::ComputeOutlierThreshold(pRejection, noiseVariance);

	Rotation3DSolverC::loss_type lossFunction(inlierTh, inlierTh);
	SymmetricTransferErrorH3DT errorMetric;
	SymmetricTransferErrorConstraint3DT constraint(errorMetric, inlierTh, 1);

	Rotation3DSolverC solver;
	return RANSACRotation3DProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACHomography3DProblemT MakeRANSACHomography3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, double binDensity)

/**
 * @brief Makes a PDL problem for 3D rotation estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for 3D rotation estimation
 * @remarks No unit test. Tested through the applications
 * @ingroup Rotation3D
 */
PDLRotation3DProblemT MakePDLRotation3DProblem(const Rotation3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	SymmetricTransferErrorH3DT errorMetric;
	Rotation3DSolverC solver;
	return PDLRotation3DProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLHomography3DProblemT MakePDLHomography3DProblem(const Homography3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>())

/********** EXPLICIT INSTANTIATIONS **********/
template Rotation3DPipelineProblemT MakeGeometryEstimationPipelineRotation3DProblem(const vector<SceneFeatureC>&, const vector<SceneFeatureC>&, const Rotation3DEstimatorProblemT&, const vector<Rotation3DPipelineProblemT::covariance_type1>&, const vector<Rotation3DPipelineProblemT::covariance_type2>&, double, double, const optional<Matrix4d>&, double, double, unsigned int);
template class GenericGeometryEstimationProblemC<RANSACRotation3DProblemT, RANSACRotation3DProblemT, PDLRotation3DProblemT>;
template class GenericGeometryEstimationPipelineProblemC<Rotation3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> >;

template class GeometryEstimationPipelineC<Rotation3DPipelineProblemT>;
}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Rotation3DSolverC, GeometryN::Rotation3DSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Rotation3DSolverC>;

}	//SeeSawN

