/**
 * @file OneSidedFundamentalComponents.cpp Implementation of the components
 * @author Evren Imre
 * @date 21 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "OneSidedFundamentalComponents.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 6-point one-sided fundamental matrix estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for 6-point one-sided fundamental matrix estimation. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup OneSidedFundamental
 */
RANSACOSFundamentalProblemT MakeRANSACOSFundamentalProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACOSFundamentalProblemT::dimension_type1& binSize1, const RANSACOSFundamentalProblemT::dimension_type2& binSize2)
{
	double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(pRejection, noiseVariance);

	OneSidedFundamentalSolverC::loss_type lossFunction(inlierTh, inlierTh);
	EpipolarSampsonErrorC errorMetric;
	EpipolarSampsonConstraintT constraint(errorMetric, inlierTh, 1);

	OneSidedFundamentalSolverC solver;
	return RANSACOSFundamentalProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACHomography2DProblemT MakeRANSACHomography2DProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double modelSimilarityTh, unsigned int loGeneratorSize, double binDensity)

/**
 * @brief Makes a PDL problem for one-sided fundamental matrix estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for one-sided fundamental matrix estimation
 * @remarks No unit test. Tested through the applications
 * @ingroup OneSidedFundamental
 */
PDLOSFundamentalProblemT MakePDLOSFundamentalProblem(const OneSidedFundamentalSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	EpipolarSampsonErrorC errorMetric;
	OneSidedFundamentalSolverC solver;
	return PDLOSFundamentalProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLEssentialProblemT MakePDLEssentialProblem(const EssentialSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)

/********** EXPLICIT INSTANTIATIONS **********/
template class GenericGeometryEstimationPipelineProblemC<OSFundamentalEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;
template class GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACOSFundamentalProblemT, PDLOSFundamentalProblemT>;
template class GeometryEstimatorC<OSFundamentalEstimatorProblemT>;
template OSFundamentalPipelineProblemT MakeGeometryEstimationPipelineOSFundamentalProblem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const OSFundamentalEstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);

}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::OneSidedFundamentalSolverC, GeometryN::OneSidedFundamentalSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::OneSidedFundamentalSolverC>;

}	//SeeSawN
