/**
 * @file P2PfComponents.cpp Implementation of the geometry estimation pipeline components for the 2-point orientation and focal length solver
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P2PfComponents.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 2-point orientation and focal length estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Image noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for P2Pf. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup P2Pf
 */
RANSACP2PfProblemT MakeRANSACP2PfProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP2PfProblemT::dimension_type1& binSize1, const RANSACP2PfProblemT::dimension_type2& binSize2)
{
	double inlierTh=TransferErrorH32DT::ComputeOutlierThreshold(pRejection, noiseVariance);

	P2PSolverC::loss_type lossFunction(inlierTh, inlierTh);
	TransferErrorH32DT errorMetric;
	ReprojectionErrorConstraintT constraint(errorMetric, inlierTh, 1);

	P2PfSolverC solver;
	return RANSACP2PfProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACP2PfProblemT MakeRANSACP2PfProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP2PfProblemT::dimension_type1& binSize1, const RANSACP2PfProblemT::dimension_type2& binSize2)

/**
 * @brief Makes a PDL problem for 2-point orientation and focal length estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for P2Pf
 * @remarks No unit test. Tested through the applications
 * @ingroup P2Pf
 */
PDLP2PfProblemT MakePDLP2PfProblem(const P2PfSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	TransferErrorH32DT errorMetric;
	P2PfSolverC solver;
	return PDLP2PfProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLP2PfProblemT MakePDLP2PfProblem(const P2PfSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)

/********** EXPLICIT INSTANTIATIONS **********/
template P2PfPipelineProblemT MakeGeometryEstimationPipelineP2PfProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PfEstimatorProblemT&, const vector<P2PfPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

template class GenericGeometryEstimationProblemC<RANSACP2PfProblemT, RANSACP2PfProblemT, PDLP2PfProblemT>;
template class GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
template class GeometryEstimationPipelineC<P2PfPipelineProblemT>;
}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P2PfSolverC, GeometryN::P2PfSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P2PfSolverC>;

}	//SeeSawN

