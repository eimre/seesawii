/**
 * @file GenericPnPComponents.h Public interface for PnP component generators
 * @author Evren Imre
 * @date 3 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef GENERIC_PNP_COMPONENTS_H_990120
#define GENERIC_PNP_COMPONENTS_H_990120

#include "GenericPnPComponents.ipp"

#include <Eigen/Dense>
#include <vector>
#include "P2PComponents.h"
#include "P2PfComponents.h"
#include "../Matcher/SceneImageFeatureMatchingProblem.h"
#include "../Elements/Feature.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/Similarity.h"

namespace SeeSawN
{
namespace GeometryN
{

template<class GeometryEstimatorProblemT, class MatchingProblemT, class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T> GenericGeometryEstimationPipelineProblemC<GeometryEstimatorProblemT, MatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const GeometryEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads, bool flagFastConstraintEvaluation);	///< Makes a geometry estimation pipeline problem for a PnP problem
template<class MinimalSolverT, class LOSolverT> RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT> MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const typename RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT>::dimension_type1& binSize1, const typename RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT>::dimension_type2& binSize2);	///< Makes a RANSAC problem for a generic PnP problem
template<class PDLProblemT> PDLProblemT MakePDLPnPProblem(const typename PDLProblemT::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix);	///< Makes a PDL problem for a generic PnP problem

/********** EXTERN TEMPLATES **********/
using Eigen::Matrix3d;
using std::vector;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;

using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::MetricsN::InverseHammingSceneImageFeatureDistanceConverterT;
typedef SeeSawN::MatcherN::BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> HammingSceneImageFeatureMatchingProblemT;

using SeeSawN::GeometryN::P2PEstimatorProblemT;
using SeeSawN::GeometryN::RANSACP2PProblemT;
using SeeSawN::GeometryN::PDLP2PProblemT;
extern template GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, HammingSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<OrientedBinarySceneFeatureC>& featureSet1, const vector<BinaryImageFeatureC>& featureSet2, const P2PEstimatorProblemT& geometryEstimationProblem, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);
extern template RANSACP2PProblemT MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT&, const CoordinateCorrespondenceList32DT&, double, double, double, unsigned int, const typename RANSACP2PProblemT::dimension_type1&, const typename RANSACP2PProblemT::dimension_type2&);
extern template PDLP2PProblemT MakePDLPnPProblem(const typename PDLP2PProblemT::model_type&, const CoordinateCorrespondenceList32DT&, const optional<MatrixXd>&);

using SeeSawN::GeometryN::P2PfEstimatorProblemT;
using SeeSawN::GeometryN::RANSACP2PfProblemT;
using SeeSawN::GeometryN::PDLP2PfProblemT;
extern template GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, HammingSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<OrientedBinarySceneFeatureC>&, const vector<BinaryImageFeatureC>&, const P2PfEstimatorProblemT&, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);
extern template RANSACP2PfProblemT MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT&, const CoordinateCorrespondenceList32DT&, double, double, double, unsigned int, const typename RANSACP2PfProblemT::dimension_type1&, const typename RANSACP2PfProblemT::dimension_type2&);
extern template PDLP2PfProblemT MakePDLPnPProblem(const typename PDLP2PfProblemT::model_type&, const CoordinateCorrespondenceList32DT&, const optional<MatrixXd>&);

using SeeSawN::MetricsN::InverseEuclideanSceneImageFeatureDistanceConverterT;
typedef SeeSawN::MatcherN::SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> EuclideanSceneImageFeatureMatchingProblemT;

extern template GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, EuclideanSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PEstimatorProblemT&, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);
extern template GenericGeometryEstimationPipelineProblemC<P2PfEstimatorProblemT, EuclideanSceneImageFeatureMatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P2PfEstimatorProblemT&, const vector<Matrix3d>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int, bool);

}	//namespace SeeSawN
}	//namespace GeometryN





#endif /* GENERIC_PNP_COMPONENTS_H_990120 */
