/**
 * @file P6PComponents.h Public interface for the geometry estimation pipeline components for the 6-point projective camera solver
 * @author Evren Imre
 * @date 5 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P6P_COMPONENTS_H_6823012
#define P6P_COMPONENTS_H_6823012

#include "P6PComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

typedef RANSACGeometryEstimationProblemC<P6PSolverC, P6PSolverC> RANSACP6PProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACP6PProblemT> RANSACP6PEngineT;	///< Two-stage RANSAC

typedef PDLHomographyXDEstimationProblemC<P6PSolverC> PDLP6PProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLP6PProblemT> PDLP6PEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACP6PProblemT, RANSACP6PProblemT, PDLP6PProblemT> P6PEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<P6PEstimatorProblemT> P6PEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<P6PSolverC> SUTP6PProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTP6PProblemT> SUTP6PEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<P6PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> > P6PPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<P6PPipelineProblemT> P6PPipelineT;	///< Pipeline

//Problem makers
RANSACP6PProblemT MakeRANSACP6PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP6PProblemT::dimension_type1& binSize1, const RANSACP6PProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 6-point projective camera estimation
PDLP6PProblemT MakePDLP6PProblem(const P6PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 6-point projective camera estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T> P6PPipelineProblemT MakeGeometryEstimationPipelineP6PProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P6PEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 6-point projective camera estimation

/********** EXTERN TEMPLATES **********/
extern template P6PPipelineProblemT MakeGeometryEstimationPipelineP6PProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P6PEstimatorProblemT&, const vector<P6PPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

extern template class GenericGeometryEstimationProblemC<RANSACP6PProblemT, RANSACP6PProblemT, PDLP6PProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<P6PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
extern template class GeometryEstimationPipelineC<P6PPipelineProblemT>;
}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P6PSolverC, GeometryN::P6PSolverC>;
extern template class OptimisationN::PDLHomographyXDEstimationProblemC<GeometryN::P6PSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P6PSolverC>;

}	//SeeSawN


#endif /* P6P_COMPONENTS_H_6823012 */
