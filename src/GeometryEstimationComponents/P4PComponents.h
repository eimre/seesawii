/**
 * @file P4PComponents.h Public interface for the geometry estimation pipeline components for the 4-point pose and focal length solver
 * @author Evren Imre
 * @date 19 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P4P_COMPONENTS_H_8120821
#define P4P_COMPONENTS_H_8120821

#include "P4PComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

typedef RANSACGeometryEstimationProblemC<P4PSolverC, P4PSolverC> RANSACP4PProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACP4PProblemT> RANSACP4PEngineT;	///< Two-stage RANSAC

typedef PDLP4PProblemC PDLP4PProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLP4PProblemT> PDLP4PEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACP4PProblemT, RANSACP4PProblemT, PDLP4PProblemT> P4PEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<P4PEstimatorProblemT> P4PEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<P4PSolverC> SUTP4PProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTP4PProblemT> SUTP4PEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<P4PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> > P4PPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<P4PPipelineProblemT> P4PPipelineT;	///< Pipeline

//Problem makers
RANSACP4PProblemT MakeRANSACP4PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP4PProblemT::dimension_type1& binSize1, const RANSACP4PProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 4-point pose and focal length estimation
PDLP4PProblemT MakePDLP4PProblem(const P4PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 4-point pose and focal length estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T> P4PPipelineProblemT MakeGeometryEstimationPipelineP4PProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P4PEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 4-point pose and focal length estimation

/********** EXTERN TEMPLATES **********/
extern template P4PPipelineProblemT MakeGeometryEstimationPipelineP4PProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P4PEstimatorProblemT&, const vector<P4PPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

extern template class GenericGeometryEstimationProblemC<RANSACP4PProblemT, RANSACP4PProblemT, PDLP4PProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<P4PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
extern template class GeometryEstimationPipelineC<P4PPipelineProblemT>;

}	//GeometryN


/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P4PSolverC, GeometryN::P4PSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P4PSolverC>;


}	//SeeSawN


#endif /* P4P_COMPONENTS_H_8120821 */
