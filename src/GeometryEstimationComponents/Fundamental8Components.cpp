/**
 * @file Fundamental8Components.cpp Implementation and instantiation of the components for the 8-point fundamental matrix estimation pipeline
 * @author Evren Imre
 * @date 8 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Fundamental8Components.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 8-point fundamental matrix estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for 8-point fundamental matrix estimation. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup Fundamental7
 */
RANSACFundamental8ProblemT MakeRANSACFundamental8Problem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACFundamental8ProblemT::dimension_type1& binSize1, const RANSACFundamental8ProblemT::dimension_type2& binSize2)
{
	double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(pRejection, noiseVariance);

	Fundamental8SolverC::loss_type lossFunction(inlierTh, inlierTh);
	EpipolarSampsonErrorC errorMetric;
	EpipolarSampsonConstraintT constraint(errorMetric, inlierTh, 1);

	Fundamental8SolverC solver;
	return RANSACFundamental8ProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACHomography2DProblemT MakeRANSACHomography2DProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double modelSimilarityTh, unsigned int loGeneratorSize, double binDensity)


/********** EXPLICIT INSTANTIATIONS **********/
template Fundamental8PipelineProblemT MakeGeometryEstimationPipelineFundamental8Problem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const Fundamental8EstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
template class GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACFundamental8ProblemT, PDLFundamentalProblemT>;
template class GenericGeometryEstimationPipelineProblemC<Fundamental8EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;

template class GeometryEstimationPipelineC<Fundamental7PipelineProblemT>;

}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Fundamental8SolverC, GeometryN::Fundamental8SolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Fundamental8SolverC>;
}	//SeeSawN

