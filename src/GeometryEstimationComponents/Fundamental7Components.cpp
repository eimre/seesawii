/**
 * @file Fundamental7Components.cpp Implementation and instantiation of the components for the 7-point fundamental matrix estimation pipeline
 * @author Evren Imre
 * @date 8 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Fundamental7Components.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 7-point fundamental matrix estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for 7-point fundamental matrix estimation. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup Fundamental7
 */
RANSACFundamental7ProblemT MakeRANSACFundamental7Problem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACFundamental7ProblemT::dimension_type1& binSize1, const RANSACFundamental7ProblemT::dimension_type2& binSize2)
{
	double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(pRejection, noiseVariance);

	Fundamental7SolverC::loss_type lossFunction(inlierTh, inlierTh);
	EpipolarSampsonErrorC errorMetric;
	EpipolarSampsonConstraintT constraint(errorMetric, inlierTh, 1);

	Fundamental7SolverC solver;
	return RANSACFundamental7ProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACHomography2DProblemT MakeRANSACHomography2DProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double modelSimilarityTh, unsigned int loGeneratorSize, double binDensity)

/**
 * @brief Makes a PDL problem for fundamental matrix estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for fundamental matrix estimation
 * @remarks No unit test. Tested through the applications
 * @ingroup Fundamental7
 */
PDLFundamentalProblemT MakePDLFundamentalProblem(const Fundamental7SolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	EpipolarSampsonErrorC errorMetric;
	Fundamental7SolverC solver;
	return PDLFundamentalProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLHomography2DEstimationProblemC MakePDLHomography2DEstimationProblem(const Homography2DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet)

/********** EXPLICIT INSTANTIATIONS **********/
template Fundamental7PipelineProblemT MakeGeometryEstimationPipelineFundamental7Problem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const Fundamental7EstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
template class GenericGeometryEstimationProblemC<RANSACFundamental7ProblemT, RANSACFundamental7ProblemT, PDLFundamentalProblemT>;
template class GenericGeometryEstimationPipelineProblemC<Fundamental7EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;

template class GeometryEstimationPipelineC<Fundamental7PipelineProblemT>;

}	//GeometryN

/********** EXPLICIT INSTANTIATIONS **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Fundamental7SolverC, GeometryN::Fundamental7SolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Fundamental7SolverC>;
}	//SeeSawN

