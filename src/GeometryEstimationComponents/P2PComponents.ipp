/**
 * @file P2PComponents.ipp Implementation of the geometry estimation pipeline components for the 2-point orientation solver
 * @author Evren Imre
 * @date 8 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P2P_COMPONENTS_IPP_3798212
#define P2P_COMPONENTS_IPP_3798212

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <vector>
#include "../Geometry/P2PSolver.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../Optimisation/PDLP2PProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../Matcher/SceneImageFeatureMatchingProblem.h"
#include "../Metrics/GeometricConstraint.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/Distance.h"
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../Elements/Feature.h"
#include "../Numeric/NumericFunctor.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::MatrixXd;
using std::vector;
using SeeSawN::GeometryN::P2PSolverC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::OptimisationN::PDLP2PProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GenericGeometryEstimationPipelineProblemC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::MatcherN::SceneImageFeatureMatchingProblemC;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::MetricsN::TransferErrorH32DT;
using SeeSawN::MetricsN::InverseEuclideanSceneImageFeatureDistanceConverterT;
using SeeSawN::MetricsN::EuclideanSceneImageFeatureDistanceT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::NumericN::ReciprocalC;

/** @name P2P */ //@{
typedef RANSACGeometryEstimationProblemC<P2PSolverC, P2PSolverC> RANSACP2PProblemT;
typedef PDLP2PProblemC PDLP2PProblemT;
typedef GenericGeometryEstimationProblemC<RANSACP2PProblemT, RANSACP2PProblemT, PDLP2PProblemT> P2PEstimatorProblemT;
typedef GenericGeometryEstimationPipelineProblemC<P2PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> > P2PPipelineProblemT;
//@}

/**
 * @brief Makes a geometry estimation pipeline problem for 2-point orientation estimation
 * @tparam FeatureRange1T A feature range for the first set
 * @tparam FeatureRange2T A feature range for the second set
 * @tparam CovarianceRange1T A covariance range for the first set
 * @param[in] featureSet1 First feature set. Scene points
 * @param[in] featureSet2 Second feature set. Image points
 * @param[in] geometryEstimationProblem Geometry estimation problem
 * @param[in] covarianceList1 Covariance list for the scene points
 * @param[in] imageNoiseVariance Image noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] initialEstimate Initial estimate, if any
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier for the first guided matching pass
 * @param[in] nThreads Number of threads
 * @return A P2P pipeline problem
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @remarks No dedicated unit tests
 * @ingroup P2P
 */
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T>
P2PPipelineProblemT MakeGeometryEstimationPipelineP2PProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P2PEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)
{
	P2PPipelineProblemT::matching_problem_type::similarity_type::map_type inverter;
	P2PPipelineProblemT::matching_problem_type::similarity_type featureSimilarity(P2PPipelineProblemT::matching_problem_type::similarity_type::distance_type(), inverter);

	//Initial constraint
	optional<ReprojectionErrorConstraintT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=P2PPipelineProblemT::matching_problem_type::constraint_type::error_type::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=ReprojectionErrorConstraintT(P2PPipelineProblemT::matching_problem_type::constraint_type::error_type(*initialEstimate), inlierTh, nThreads);
	}	//if(initialEstimate)

	ReprojectionErrorConstraintT shellConstraint(TransferErrorH32DT(), TransferErrorH32DT::ComputeOutlierThreshold(pRejection, imageNoiseVariance), nThreads);

	vector<typename P2PPipelineProblemT::covariance_type2> covarianceList2(1);
	covarianceList2[0]<<imageNoiseVariance,0, 0, imageNoiseVariance;

	return P2PPipelineProblemT(featureSet1, featureSet2, covarianceList1, covarianceList2, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//P2PPipelineProblemT MakeGeometryEstimationPipelineP2PProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P2PEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)

}	//GeometryN
}	//SeeSawN

#endif /* P2P_COMPONENTS_IPP_3798212 */
