/**
 * @file OneSidedFundamentalComponents.h Public interface for the components of the 6-point one-sided fundamental matrix estimator
 * @author Evren Imre
 * @date 21 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ONE_SIDED_FUNDAMENTAL_COMPONENTS_H_4109831
#define ONE_SIDED_FUNDAMENTAL_COMPONENTS_H_4109831

#include "OneSidedFundamentalComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

/** @name OneSidedFundamental */ //@{
typedef RANSACGeometryEstimationProblemC<OneSidedFundamentalSolverC, OneSidedFundamentalSolverC> RANSACOSFundamentalProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACFundamental8ProblemT, RANSACOSFundamentalProblemT> RANSACOSFundamentalEngineT;	///< Two-stage RANSAC

typedef PDLOneSidedFundamentalMatrixEstimationProblemC PDLOSFundamentalProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLOSFundamentalProblemT> PDLOSFundamentalEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACOSFundamentalProblemT, PDLOSFundamentalProblemT> OSFundamentalEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<OSFundamentalEstimatorProblemT> OSFundamentalEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<OneSidedFundamentalSolverC> SUTOSFundamentalProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTOSFundamentalProblemT> SUTOSFundamentalEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<OSFundamentalEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > OSFundamentalPipelineProblemT;	///< Pipeline problem
typedef GeometryEstimationPipelineC<OSFundamentalPipelineProblemT> OSFundamentalPipelineT;	///< Pipeline

//@}

//Problem makers
RANSACOSFundamentalProblemT MakeRANSACOSFundamentalProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACOSFundamentalProblemT::dimension_type1& binSize1, const RANSACOSFundamentalProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 6-point one-sided fundamental matrix estimation
PDLOSFundamentalProblemT MakePDLOSFundamentalProblem(const EssentialSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for one-sided fundamental matrix estimation
template<class FeatureRange1T, class FeatureRange2T> OSFundamentalPipelineProblemT MakeGeometryEstimationPipelineOSFundamentalProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const OSFundamentalEstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 6-point one-sided fundamental matrix estimation

/********** EXTERN TEMPLATES **********/
extern template class GenericGeometryEstimationPipelineProblemC<OSFundamentalEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;
extern template class GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACOSFundamentalProblemT, PDLOSFundamentalProblemT>;

extern template class GeometryEstimatorC<OSFundamentalEstimatorProblemT>;

extern template OSFundamentalPipelineProblemT MakeGeometryEstimationPipelineOSFundamentalProblem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const OSFundamentalEstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::OneSidedFundamentalSolverC, GeometryN::OneSidedFundamentalSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::OneSidedFundamentalSolverC>;



}	//SeeSawN


#endif /* ONE_SIDED_FUNDAMENTAL_COMPONENTS_H_4109831 */
