/**
 * @file Fundamental7Components.h Public interface for the components for the 7-point fundamental matrix estimation pipeline
 * @author Evren Imre
 * @date 8 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FUNDAMENTAL7_COMPONENTS_H_8012523
#define FUNDAMENTAL7_COMPONENTS_H_8012523

#include "Fundamental7Components.ipp"
namespace SeeSawN
{
namespace GeometryN
{

/** @name Fundamental7 */ //@{

typedef RANSACGeometryEstimationProblemC<Fundamental7SolverC, Fundamental7SolverC> RANSACFundamental7ProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACFundamental7ProblemT> RANSACFundamental7EngineT;	///< Two-stage RANSAC

typedef PDLFundamentalMatrixEstimationProblemC PDLFundamentalProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLFundamentalProblemT> PDLFundamentalEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACFundamental7ProblemT, RANSACFundamental7ProblemT, PDLFundamentalProblemT> Fundamental7EstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<Fundamental7EstimatorProblemT> Fundamental7EstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<Fundamental7SolverC> SUTFundamental7ProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTFundamental7ProblemT> SUTFundamental7EngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<Fundamental7EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > Fundamental7PipelineProblemT;	///< Pipeline problem
typedef GeometryEstimationPipelineC<Fundamental7PipelineProblemT> Fundamental7PipelineT;	///< Pipeline
//@}

//Problem makers
RANSACFundamental7ProblemT MakeRANSACFundamental7Problem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACFundamental7ProblemT::dimension_type1& binSize1, const RANSACFundamental7ProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 7-point fundamental matrix estimation
PDLFundamentalProblemT MakePDLFundamentalProblem(const Fundamental7SolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for fundamental matrix estimation
template<class FeatureRange1T, class FeatureRange2T>Fundamental7PipelineProblemT MakeGeometryEstimationPipelineFundamental7Problem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Fundamental7EstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 7-point fundamental matrix estimation

/********** EXTERN TEMPLATES **********/
extern template Fundamental7PipelineProblemT MakeGeometryEstimationPipelineFundamental7Problem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const Fundamental7EstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACFundamental7ProblemT, RANSACFundamental7ProblemT, PDLFundamentalProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<Fundamental7EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;

extern template class GeometryEstimationPipelineC<Fundamental7PipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Fundamental7SolverC, GeometryN::Fundamental7SolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Fundamental7SolverC>;
}	//SeeSawN

#endif /* FUNDAMENTAL7_COMPONENTS_H_8012523 */
