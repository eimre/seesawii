/**
 * @file P3PComponents.h Public interface for the geometry estimation pipeline components for the 3-point pose solver
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef P3P_COMPONENTS_H_0801269
#define P3P_COMPONENTS_H_0801269

#include "P3PComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

typedef RANSACGeometryEstimationProblemC<P3PSolverC, P3PSolverC> RANSACP3PProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACP3PProblemT> RANSACP3PEngineT;	///< Two-stage RANSAC

typedef PDLP3PProblemC PDLP3PProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLP3PProblemT> PDLP3PEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACP3PProblemT, RANSACP3PProblemT, PDLP3PProblemT> P3PEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<P3PEstimatorProblemT> P3PEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<P3PSolverC> SUTP3PProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTP3PProblemT> SUTP3PEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<P3PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> > P3PPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<P3PPipelineProblemT> P3PPipelineT;	///< Pipeline

//Problem makers
RANSACP3PProblemT MakeRANSACP3PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP3PProblemT::dimension_type1& binSize1, const RANSACP3PProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 3-point pose estimation
PDLP3PProblemT MakePDLP3PProblem(const P3PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 3-point pose estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T> P3PPipelineProblemT MakeGeometryEstimationPipelineP3PProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const P3PEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 3-point pose estimation

/********** EXTERN TEMPLATES **********/
extern template P3PPipelineProblemT MakeGeometryEstimationPipelineP3PProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P3PEstimatorProblemT&, const vector<P3PPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

extern template class GenericGeometryEstimationProblemC<RANSACP3PProblemT, RANSACP3PProblemT, PDLP3PProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<P3PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
extern template class GeometryEstimationPipelineC<P3PPipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P3PSolverC, GeometryN::P3PSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P3PSolverC>;
}	//SeeSawN

#endif /* P3P_COMPONENTS_H_0801269 */
