/**
 * @file Homography3DComponents.h Public interface for 3D homography estimation pipeline components
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef HOMOGRAPHY_3D_COMPONENTS_H_1799028
#define HOMOGRAPHY_3D_COMPONENTS_H_1799028

#include "Homography3DComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{
typedef RANSACGeometryEstimationProblemC<Homography3DSolverC, Homography3DSolverC> RANSACHomography3DProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACHomography3DProblemT> RANSACHomography3DEngineT;	///< Two-stage RANSAC

typedef PDLHomographyXDEstimationProblemC<Homography3DSolverC> PDLHomography3DProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLHomography3DProblemT> PDLHomography3DEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACHomography3DProblemT, RANSACHomography3DProblemT, PDLHomography3DProblemT> Homography3DEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<Homography3DEstimatorProblemT> Homography3DEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<Homography3DSolverC> SUTHomography3DProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTHomography3DProblemT> SUTHomography3DEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<Homography3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> > Homography3DPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<Homography3DPipelineProblemT> Homography3DPipelineT;	///< Pipeline

//Problem makers
RANSACHomography3DProblemT MakeRANSACHomography3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACHomography3DProblemT::dimension_type1& binSize1, const RANSACHomography3DProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 3D homography estimation
PDLHomography3DProblemT MakePDLHomography3DProblem(const Homography3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 3D homography estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T, class CovarianceRange2T> Homography3DPipelineProblemT MakeGeometryEstimationPipelineHomography3DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Homography3DEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, const CovarianceRange2T& covarianceList2, double effectiveNoiseVariance, double pRejection, const optional<Matrix4d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 3D homography estimation

/********** EXTERN TEMPLATES **********/
extern template Homography3DPipelineProblemT MakeGeometryEstimationPipelineHomography3DProblem(const vector<SceneFeatureC>&, const vector<SceneFeatureC>&, const Homography3DEstimatorProblemT&, const vector<Homography3DPipelineProblemT::covariance_type1>&, const vector<Homography3DPipelineProblemT::covariance_type2>&, double, double, const optional<Matrix4d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACHomography3DProblemT, RANSACHomography3DProblemT, PDLHomography3DProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<Homography3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> >;

extern template class GeometryEstimationPipelineC<Homography3DPipelineProblemT>;
}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Homography3DSolverC, GeometryN::Homography3DSolverC>;
extern template class OptimisationN::PDLHomographyXDEstimationProblemC<GeometryN::Homography3DSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Homography3DSolverC>;

}	//SeeSawN


#endif /* HOMOGRAPHY_3D_COMPONENTS_H_1799028 */
