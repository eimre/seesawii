/**
 * @file EssentialComponents.ipp Implementation details for the 5-point essential matrix estimation pipeline
 * @author Evren Imre
 * @date 10 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ESSENTIAL_COMPONENTS_IPP_6890293
#define ESSENTIAL_COMPONENTS_IPP_6890293

#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <tuple>
#include <vector>
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationProblem.h"
#include "../Matcher/ImageFeatureMatchingProblem.h"
#include "../Metrics/Similarity.h"
#include "../Metrics/Distance.h"
#include "../Metrics/GeometricError.h"
#include "../Metrics/GeometricConstraint.h"
#include "../RANSAC/TwoStageRANSAC.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../Optimisation/PowellDogLeg.h"
#include "../Optimisation/PDLEssentialMatrixEstimationProblem.h"
#include "../UncertaintyEstimation/ScaledUnscentedTransformation.h"
#include "../UncertaintyEstimation/SUTGeometryEstimationProblem.h"
#include "../Geometry/EssentialSolver.h"
#include "../Geometry/FundamentalSolver.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Elements/Correspondence.h"
#include "../Elements/Feature.h"
#include "../Elements/Coordinate.h"
#include "../Wrappers/BoostBimap.h"
#include "../Numeric/NumericFunctor.h"
#include "../GeometryEstimationComponents/Fundamental8Components.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::Matrix3d;
using Eigen::MatrixXd;
using std::tie;
using std::vector;
using SeeSawN::GeometryN::GeometryEstimationPipelineC;
using SeeSawN::GeometryN::GenericGeometryEstimationPipelineProblemC;
using SeeSawN::GeometryN::GeometryEstimatorC;
using SeeSawN::GeometryN::GenericGeometryEstimationProblemC;
using SeeSawN::GeometryN::Fundamental8SolverC;
using SeeSawN::GeometryN::EssentialSolverC;
using SeeSawN::GeometryN::CoordinateStatistics2DT;
using SeeSawN::GeometryN::RANSACFundamental8ProblemT;
using SeeSawN::MatcherN::ImageFeatureMatchingProblemC;
using SeeSawN::RANSACN::TwoStageRANSACC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::OptimisationN::PowellDogLegC;
using SeeSawN::OptimisationN::PDLEssentialMatrixEstimationProblemC;
using SeeSawN::UncertaintyEstimationN::ScaledUnscentedTransformationC;
using SeeSawN::UncertaintyEstimationN::SUTGeometryEstimationProblemC;
using SeeSawN::MetricsN::InverseEuclideanIDConverterT;
using SeeSawN::MetricsN::EuclideanDistanceIDT;
using SeeSawN::MetricsN::EpipolarSampsonConstraintT;
using SeeSawN::MetricsN::EpipolarSampsonErrorC;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::WrappersN::BimapToVector;
using SeeSawN::NumericN::ReciprocalC;

typedef RANSACGeometryEstimationProblemC<EssentialSolverC, EssentialSolverC> RANSACEssentialProblemT;
typedef PDLEssentialMatrixEstimationProblemC PDLEssentialProblemT;
typedef GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACEssentialProblemT, PDLEssentialProblemT> EssentialEstimatorProblemT;
typedef GenericGeometryEstimationPipelineProblemC<EssentialEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > EssentialPipelineProblemT;

/**
 * @brief Makes a geometry estimation pipeline problem for 5-point essential matrix estimation
 * @param[in] featureSet1 First feature set
 * @param[in] featureSet2 Second feature set
 * @param[in] geometryEstimationProblem Problem for the geometry estimator
 * @param[in] noiseVariance	Coordinate noise variance (pixel^2)
 * @param[in] pRejection Inlier rejection probability
 * @param[in] initialEstimate Initial essential matrix estimate. Invalid, if there is none
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier (for the first guided matching pass)
 * @param[in] nThreads Number of threads
 * @return Problem object
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @ingroup Essential
 */
template<class FeatureRange1T, class FeatureRange2T>
EssentialPipelineProblemT MakeGeometryEstimationPipelineEssentialProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const EssentialEstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)
{
	ReciprocalC<typename EuclideanDistanceIDT::result_type> inverter;
	InverseEuclideanIDConverterT featureSimilarity(EuclideanDistanceIDT(), inverter);

	//Initial constraint
	optional<EpipolarSampsonConstraintT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=EpipolarSampsonErrorC::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=EpipolarSampsonConstraintT(EpipolarSampsonErrorC(*initialEstimate), inlierTh, nThreads);
	}	//if(initialEstimate)

	EpipolarSampsonConstraintT shellConstraint(EpipolarSampsonErrorC(), EpipolarSampsonErrorC::ComputeOutlierThreshold(pRejection, noiseVariance), nThreads);

	vector<typename EssentialPipelineProblemT::covariance_type1> covarianceList(1);
	covarianceList[0]<<noiseVariance,0, 0, noiseVariance;

	return EssentialPipelineProblemT(featureSet1, featureSet2, covarianceList, covarianceList, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//EssentialPipelineProblemT MakeGeometryEstimationPipelineEssentialProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const EssentialEstimatorProblemT& geometryEstimationProblem, double noiseVariance, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)

}	//GeometryN
}	//SeeSawN


#endif /* ESSENTIAL_COMPONENTS_IPP_6890293 */
