/**
 * @file Fundamental8Components.h Public interface for the components for the 8-point fundamental matrix estimation pipeline
 * @author Evren Imre
 * @date 8 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FUNDAMENTAL8_COMPONENTS_H_8012523
#define FUNDAMENTAL8_COMPONENTS_H_8012523

#include "Fundamental8Components.ipp"
namespace SeeSawN
{
namespace GeometryN
{

/** @name Fundamental8 */ //@{

typedef RANSACGeometryEstimationProblemC<Fundamental8SolverC, Fundamental8SolverC> RANSACFundamental8ProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACFundamental8ProblemT> RANSACFundamental8EngineT;	///< Two-stage RANSAC

typedef GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACFundamental8ProblemT, PDLFundamentalProblemT> Fundamental8EstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<Fundamental8EstimatorProblemT> Fundamental8EstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<Fundamental8SolverC> SUTFundamental8ProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTFundamental8ProblemT> SUTFundamental8EngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<Fundamental8EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > Fundamental8PipelineProblemT;	///< Pipeline problem
typedef GeometryEstimationPipelineC<Fundamental8PipelineProblemT> Fundamental8PipelineT;	///< Pipeline

//@}

//Problem makers
RANSACFundamental8ProblemT MakeRANSACFundamental8Problem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACFundamental8ProblemT::dimension_type1& binSize1, const RANSACFundamental8ProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 7-point fundamental matrix estimation
template<class FeatureRange1T, class FeatureRange2T>Fundamental8PipelineProblemT MakeGeometryEstimationPipelineFundamental8Problem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Fundamental8EstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 7-point fundamental matrix estimation

/********** EXTERN TEMPLATES **********/
extern template Fundamental8PipelineProblemT MakeGeometryEstimationPipelineFundamental8Problem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const Fundamental8EstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACFundamental8ProblemT, PDLFundamentalProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<Fundamental8EstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;

extern template class GeometryEstimationPipelineC<Fundamental8PipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Fundamental8SolverC, GeometryN::Fundamental8SolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Fundamental8SolverC>;
}	//SeeSawN

#endif /* FUNDAMENTAL8_COMPONENTS_H_8012523 */
