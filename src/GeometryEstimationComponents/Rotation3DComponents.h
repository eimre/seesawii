/**
 * @file Rotation3DComponents.h Public interface for 3D rotation estimation pipeline components
 * @author Evren Imre
 * @date 29 Mar 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROTATION_3D_COMPONENTS_H_7243003
#define ROTATION_3D_COMPONENTS_H_7243003

#include "Rotation3DComponents.ipp"
namespace SeeSawN
{
namespace GeometryN
{

typedef RANSACGeometryEstimationProblemC<Rotation3DSolverC, Rotation3DSolverC> RANSACRotation3DProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACRotation3DProblemT> RANSACRotation3DEngineT;	///< Two-stage RANSAC

typedef PDLRotation3DEstimationProblemC PDLRotation3DProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLRotation3DProblemT> PDLRotation3DEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACRotation3DProblemT, RANSACRotation3DProblemT, PDLRotation3DProblemT> Rotation3DEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<Rotation3DEstimatorProblemT> Rotation3DEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<Rotation3DSolverC> SUTRotation3DProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTRotation3DProblemT> SUTRotation3DEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<Rotation3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> > Rotation3DPipelineProblemT; ///< Pipeline problem
typedef GeometryEstimationPipelineC<Rotation3DPipelineProblemT> Rotation3DPipelineT;	///< Pipeline

//Problem makers
RANSACRotation3DProblemT MakeRANSACRotation3DProblem(const CoordinateCorrespondenceList3DT& observationSet, const CoordinateCorrespondenceList3DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACRotation3DProblemT::dimension_type1& binSize1, const RANSACRotation3DProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 3D rotation estimation
PDLRotation3DProblemT MakePDLRotation3DProblem(const Rotation3DSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList3DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for 3D rotation estimation
template<class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T, class CovarianceRange2T> Rotation3DPipelineProblemT MakeGeometryEstimationPipelineRotation3DProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const Rotation3DEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, const CovarianceRange2T& covarianceList2, double effectiveNoiseVariance, double pRejection,  const optional<Matrix4d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 3D rotation estimation

/********** EXTERN TEMPLATES **********/
extern template Rotation3DPipelineProblemT MakeGeometryEstimationPipelineRotation3DProblem(const vector<SceneFeatureC>&, const vector<SceneFeatureC>&, const Rotation3DEstimatorProblemT&, const vector<Rotation3DPipelineProblemT::covariance_type1>&, const vector<Rotation3DPipelineProblemT::covariance_type2>&, double, double, const optional<Matrix4d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACRotation3DProblemT, RANSACRotation3DProblemT, PDLRotation3DProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<Rotation3DEstimatorProblemT, SceneFeatureMatchingProblemC<InverseEuclideanSceneFeatureDistanceConverterT, SymmetricTransferErrorConstraint3DT> >;

extern template class GeometryEstimationPipelineC<Rotation3DPipelineProblemT>;
}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::Rotation3DSolverC, GeometryN::Rotation3DSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::Rotation3DSolverC>;

}	//SeeSawN

#endif /* ROTATION_3D_COMPONENTS_H_7243003 */
