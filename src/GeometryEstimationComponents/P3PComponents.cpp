/**
 * @file P3PComponents.cpp Implementation of the geometry estimation pipeline components for the 3-point pose solver
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "P3PComponents.h"
namespace SeeSawN
{
namespace GeometryN
{

/**
 * @brief Makes a RANSAC problem for 3-point pose estimation
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Image noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for P3P. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @ingroup P3P
 */
RANSACP3PProblemT MakeRANSACP3PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP3PProblemT::dimension_type1& binSize1, const RANSACP3PProblemT::dimension_type2& binSize2)
{
	double inlierTh=TransferErrorH32DT::ComputeOutlierThreshold(pRejection, noiseVariance);

	P3PSolverC::loss_type lossFunction(inlierTh, inlierTh);
	TransferErrorH32DT errorMetric;
	ReprojectionErrorConstraintT constraint(errorMetric, inlierTh, 1);

	P3PSolverC solver;
	return RANSACP3PProblemT(observationSet, validationSet, constraint, lossFunction, solver, solver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACP3PProblemT MakeRANSACP3PProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACP3PProblemT::dimension_type1& binSize1, const RANSACP3PProblemT::dimension_type2& binSize2)

/**
 * @brief Makes a PDL problem for 3-point pose estimation
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for P3P
 * @remarks No unit test. Tested through the applications
 * @ingroup P3P
 */
PDLP3PProblemT MakePDLP3PProblem(const P3PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)
{
	TransferErrorH32DT errorMetric;
	P3PSolverC solver;
	return PDLP3PProblemT(initialEstimate, observationSet, solver, errorMetric, observationWeightMatrix);
}	//PDLP3PProblemT MakePDLP3PProblem(const P3PSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix)

/********** EXTERN TEMPLATES **********/
template P3PPipelineProblemT MakeGeometryEstimationPipelineP3PProblem(const vector<SceneFeatureC>&, const vector<ImageFeatureC>&, const P3PEstimatorProblemT&, const vector<P3PPipelineProblemT::covariance_type1>&, double, double, const optional<CameraMatrixT>&, double, double, unsigned int);

template class GenericGeometryEstimationProblemC<RANSACP3PProblemT, RANSACP3PProblemT, PDLP3PProblemT>;
template class GenericGeometryEstimationPipelineProblemC<P3PEstimatorProblemT, SceneImageFeatureMatchingProblemC<InverseEuclideanSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> >;
template class GeometryEstimationPipelineC<P3PPipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::P3PSolverC, GeometryN::P3PSolverC>;
template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::P3PSolverC>;
}	//SeeSawN

