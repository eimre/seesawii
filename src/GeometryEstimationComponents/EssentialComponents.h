/**
 * @file EssentialComponents.h Public interface for the components for the 5-point essential matrix estimation pipeline
 * @author Evren Imre
 * @date 10 Jan 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ESSENTIAL_COMPONENTS_H_7012032
#define ESSENTIAL_COMPONENTS_H_7012032

#include "EssentialComponents.ipp"

namespace SeeSawN
{
namespace GeometryN
{
/** @name Essential */ //@{

typedef RANSACGeometryEstimationProblemC<EssentialSolverC, EssentialSolverC> RANSACEssentialProblemT;	///< RANSAC problem
typedef TwoStageRANSACC<RANSACFundamental8ProblemT, RANSACEssentialProblemT> RANSACEssentialEngineT;	///< Two-stage RANSAC

typedef PDLEssentialMatrixEstimationProblemC PDLEssentialProblemT;	///< Optimisation problem
typedef PowellDogLegC<PDLEssentialProblemT> PDLEssentialEngineT;	///< Optimisation engine

typedef GenericGeometryEstimationProblemC<RANSACFundamental8ProblemT, RANSACEssentialProblemT, PDLEssentialProblemT> EssentialEstimatorProblemT;	///< Geometry estimator problem
typedef GeometryEstimatorC<EssentialEstimatorProblemT> EssentialEstimatorT;	///< Geometry estimation engine

typedef SUTGeometryEstimationProblemC<EssentialSolverC> SUTEssentialProblemT;	///< SUT problem
typedef ScaledUnscentedTransformationC<SUTEssentialProblemT> SUTEssentialEngineT;	///< SUT engine

typedef GenericGeometryEstimationPipelineProblemC<EssentialEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> > EssentialPipelineProblemT;	///< Pipeline problem
typedef GeometryEstimationPipelineC<EssentialPipelineProblemT> EssentialPipelineT;	///< Pipeline
//@}

//Problem makers
RANSACEssentialProblemT MakeRANSACEssentialProblem(const CoordinateCorrespondenceList2DT& observationSet, const CoordinateCorrespondenceList2DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACEssentialProblemT::dimension_type1& binSize1, const RANSACEssentialProblemT::dimension_type2& binSize2);	///< Makes a RANSAC problem for 5-point essential matrix estimation
PDLEssentialProblemT MakePDLEssentialProblem(const EssentialSolverC::model_type& initialEstimate, const CoordinateCorrespondenceList2DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>());	///< Makes a PDL problem for essential matrix estimation
template<class FeatureRange1T, class FeatureRange2T> EssentialPipelineProblemT MakeGeometryEstimationPipelineEssentialProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const EssentialEstimatorProblemT& geometryEstimationProblem, double noiseVariance, double pRejection, const optional<Matrix3d>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads);	///< Makes a geometry estimation pipeline problem for 5-point essential matrix estimation

/********** EXTERN TEMPLATES **********/
extern template EssentialPipelineProblemT MakeGeometryEstimationPipelineEssentialProblem(const vector<ImageFeatureC>&, const vector<ImageFeatureC>&, const EssentialEstimatorProblemT&, double, double, const optional<Matrix3d>&, double, double, unsigned int);
extern template class GenericGeometryEstimationProblemC<RANSACEssentialProblemT, RANSACEssentialProblemT, PDLEssentialProblemT>;
extern template class GenericGeometryEstimationPipelineProblemC<EssentialEstimatorProblemT, ImageFeatureMatchingProblemC<InverseEuclideanIDConverterT, EpipolarSampsonConstraintT> >;

extern template class GeometryEstimationPipelineC<EssentialPipelineProblemT>;

}	//GeometryN

/********** EXTERN TEMPLATES **********/
extern template class RANSACN::RANSACGeometryEstimationProblemC<GeometryN::EssentialSolverC, GeometryN::EssentialSolverC>;
extern template class UncertaintyEstimationN::SUTGeometryEstimationProblemC<GeometryN::EssentialSolverC>;
}	//SeeSawN

#endif /* ESSENTIAL_COMPONENTS_H_7012032 */
