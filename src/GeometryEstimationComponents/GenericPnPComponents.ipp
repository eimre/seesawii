/**
 * @file GenericPnPComponents.ipp Implementation of PnP component generators
 * @author Evren Imre
 * @date 3 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef GENERIC_PNP_COMPONENTS_IPP_6800912
#define GENERIC_PNP_COMPONENTS_IPP_6800912

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <type_traits>
#include "../Elements/Correspondence.h"
#include "../Elements/GeometricEntity.h"
#include "../GeometryEstimationPipeline/GenericGeometryEstimationPipelineProblem.h"
#include "../RANSAC/RANSACGeometryEstimationProblem.h"
#include "../GeometryEstimationPipeline/GeometryEstimatorProblemConcept.h"
#include "../Matcher/FeatureMatcherProblemConcept.h"
#include "../Optimisation/PowellDogLegProblemConcept.h"
#include "../Optimisation/PDLGeometryEstimationProblemBase.h"
#include "../Geometry/GeometrySolverConcept.h"

namespace SeeSawN
{
namespace GeometryN
{

using boost::optional;
using Eigen::MatrixXd;
using std::is_base_of;
using SeeSawN::ElementsN::CoordinateCorrespondenceList32DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::GeometryN::GeometryEstimatorProblemConceptC;
using SeeSawN::RANSACN::RANSACGeometryEstimationProblemC;
using SeeSawN::OptimisationN::PowellDogLegProblemConceptC;
using SeeSawN::OptimisationN::PDLGeometryEstimationProblemBaseC;
using SeeSawN::MatcherN::FeatureMatcherProblemConceptC;

/**
 * @brief Makes a geometry estimation pipeline problem for a PnP problem
 * @tparam GeometryEstimatorProblemT A geometry estimator problem
 * @tparam MatchingProblemT A feature matching problem
 * @tparam FeatureRange1T A feature range for the first set
 * @tparam FeatureRange2T A feature range for the second set
 * @tparam CovarianceRange1T A covariance range for the first set
 * @param[in] featureSet1 First feature set. Scene points
 * @param[in] featureSet2 Second feature set. Image points
 * @param[in] geometryEstimationProblem Geometry estimation problem
 * @param[in] covarianceList1 Covariance list for the scene points
 * @param[in] imageNoiseVariance Image noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] initialEstimate Initial estimate, if any
 * @param[in] initialNoiseVariance Effective noise variance for the first guided matching pass
 * @param[in] initialPRejection Probability of rejection for an inlier for the first guided matching pass
 * @param[in] nThreads Number of threads
 * @param[in] flagFastConstraintEvaluation If \c true fast and approximate constraint evaluation enabled, which may skip some potential matches
 * @return A generic geometry estimation pipeline problem
 * @pre \c GeometryEstimatorProblemT is a model of \c GeometryEstimatorProblemConceptC
 * @pre \c MatchingProblemT is a model of \c FeatureMatcherProblemConceptC
 * @remarks \c shellMatchingConstraint is initialised from \c ransacProblem2
 * @remarks No dedicated unit tests
 * @ingroup Geometry
 */
template<class GeometryEstimatorProblemT, class MatchingProblemT, class FeatureRange1T, class FeatureRange2T, class CovarianceRange1T>
GenericGeometryEstimationPipelineProblemC<GeometryEstimatorProblemT, MatchingProblemT> MakeGeometryEstimationPipelinePnPProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const GeometryEstimatorProblemT& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, double pRejection, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads, bool flagFastConstraintEvaluation)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((GeometryEstimatorProblemConceptC<GeometryEstimatorProblemT>));
	BOOST_CONCEPT_ASSERT((FeatureMatcherProblemConceptC<MatchingProblemT>));

	typedef GenericGeometryEstimationPipelineProblemC<GeometryEstimatorProblemT, MatchingProblemT> PipelineProblemT;
	typedef typename MatchingProblemT::similarity_type SimilarityT;
	typedef typename MatchingProblemT::constraint_type ConstraintT;
	typedef typename ConstraintT::error_type ErrorT;

	typename SimilarityT::map_type inverter;
	SimilarityT featureSimilarity(typename SimilarityT::distance_type(), inverter);

	//Initial constraint
	optional<ConstraintT> initialConstraint;
	if(initialEstimate)
	{
		double inlierTh=ErrorT::ComputeOutlierThreshold(initialPRejection, initialNoiseVariance);
		initialConstraint=ConstraintT(ErrorT(*initialEstimate), inlierTh, nThreads, flagFastConstraintEvaluation);
	}	//if(initialEstimate)

	ConstraintT shellConstraint=ConstraintT(ErrorT(), ErrorT::ComputeOutlierThreshold(pRejection, imageNoiseVariance), nThreads, flagFastConstraintEvaluation);

	vector<typename PipelineProblemT::covariance_type2> covarianceList2(1);
	covarianceList2[0]<<imageNoiseVariance,0, 0, imageNoiseVariance;

	return PipelineProblemT(featureSet1, featureSet2, covarianceList1, covarianceList2, featureSimilarity, shellConstraint, initialConstraint, geometryEstimationProblem);
}	//PipelineProblemT MakeGeometryEstimationPipelinePnPProblem(const FeatureRange1T& featureSet1, const FeatureRange2T& featureSet2, const PipelineProblemT::geometry_estimation_problem_type& geometryEstimationProblem, const CovarianceRange1T& covarianceList1, double imageNoiseVariance, const optional<CameraMatrixT>& initialEstimate, double initialNoiseVariance, double initialPRejection, unsigned int nThreads)

/**
 * @brief Makes a RANSAC problem for a generic PnP problem
 * @tparam MinimalSolverT Minimal solver type
 * @tparam LOSolverT LO solver type
 * @param[in] observationSet Correspondences for the observation set
 * @param[in] validationSet Correspondences for the validation set
 * @param[in] noiseVariance Image noise variance
 * @param[in] pRejection Probability of rejection for an inlier
 * @param[in] modelSimilarityTh Algebraic model similarity threshold, as a percentage of the maximum
 * @param[in] loGeneratorSize Size of the generator set for the LO stage
 * @param[in] binSize1 Bin size for the first RANSAC problem
 * @param[in] binSize2 Bin size for the second RANSAC problem
 * @return A RANSAC problem for PnP problems. If \c observationSet or \c validationSet is empty, default problem
 * @remarks No unit tests. Tested via the applications
 * @pre \c MinimalSolverT is a model of \c GeometrySolverConceptC
 * @pre \c LOSolverT is a model of \c GeometrySolverConceptC
 * @ingroup Geometry
 */
template<class MinimalSolverT, class LOSolverT>
RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT> MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const typename RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT>::dimension_type1& binSize1, const typename RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT>::dimension_type2& binSize2)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<MinimalSolverT>));
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<LOSolverT>));

	typedef RANSACGeometryEstimationProblemC<MinimalSolverT, LOSolverT> RANSACProblemT;
	typedef typename MinimalSolverT::error_type ErrorT;
	typedef typename MinimalSolverT::constraint_type ConstraintT;
	typedef typename MinimalSolverT::loss_type LossT;

	double inlierTh=ErrorT::ComputeOutlierThreshold(pRejection, noiseVariance);
	LossT lossFunction(inlierTh, inlierTh);
	ErrorT errorMetric;
	ConstraintT constraint(errorMetric, inlierTh, 1);

	MinimalSolverT minimalSolver;
	LOSolverT loSolver;

	return RANSACProblemT(observationSet, validationSet, constraint, lossFunction, minimalSolver, loSolver, modelSimilarityTh, loGeneratorSize, binSize1, binSize2);
}	//RANSACProblemT MakeRANSACPnPProblem(const CoordinateCorrespondenceList32DT& observationSet, const CoordinateCorrespondenceList32DT& validationSet, double noiseVariance, double pRejection, double modelSimilarityTh, unsigned int loGeneratorSize, const RANSACProblemT::dimension_type1& binSize1, const RANSACProblemT::dimension_type2& binSize2)

/**
 * @brief Makes a PDL problem for a generic PnP problem
 * @tparam PDLProblemT A PDL optimisation problem
 * @param[in] initialEstimate Initial estimate
 * @param[in] observationSet Observation set
 * @param[in] observationWeightMatrix Observation weights
 * @return A PDL problem for a PnP problem
 * @remarks No unit test. Tested through the applications
 * @pre \c PDLProblemT is a model of \c PowellDogLegProblemConceptC
 * @pre \c PDLProblemT is derived from \c PDLGeometryEstimationProblemBaseC
 * @ingroup Geometry
 */
template<class PDLProblemT>
PDLProblemT MakePDLPnPProblem(const typename PDLProblemT::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>())
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((PowellDogLegProblemConceptC<PDLProblemT>));
	static_assert(is_base_of<PDLGeometryEstimationProblemBaseC<PDLProblemT, typename PDLProblemT::solver_type>, PDLProblemT >::value,"MakePDLPnPProblem: PDLProblemT must be derived from PDLGeometryEstimationProblemBaseC" );

	typedef typename PDLProblemT::solver_type SolverT;
	typedef typename SolverT::error_type ErrorT;
	return PDLProblemT(initialEstimate, observationSet, SolverT(), ErrorT(), observationWeightMatrix);
}	//PDLProblemT MakePDLPnPProblem(const PDLProblemT::model_type& initialEstimate, const CoordinateCorrespondenceList32DT& observationSet, const optional<MatrixXd>& observationWeightMatrix=optional<MatrixXd>())


}	//namespace SeeSawN
}	//namespace GeometryN






#endif /* GENERIC_PNP_COMPONENTS_IPP_6800912 */
