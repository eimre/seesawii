/**
 * @file InterfaceLinearAutocalibrationPipeline.cpp Implementation of \c InterfaceLinearAutocalibrationPipelineC
 * @author Evren Imre
 * @date 3 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceLinearAutocalibrationPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant RANSAC parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceLinearAutocalibrationPipelineC::PruneRANSAC(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("PROSAC"))
		output.erase("PROSAC");

	if(src.get_child_optional("MORANSAC"))
		output.erase("MORANSAC");

	return output;
}	//ptree PruneRANSAC(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @remarks Assumption: Unit-norm feature vectors
 * @remarks \c FeatureMatcherParametersC::flagStatic is application-dependent, and is not set here
 * @return A parameter object
 */
auto InterfaceLinearAutocalibrationPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);

	ParameterObjectT parameters;

	parameters.nLACIteration=ReadParameter(src, "Main.NoWeights", gt0, "is not >0", optional<double>(parameters.nLACIteration));
	parameters.inlierTh=ReadParameter(src, "Main.InlierThreshold", gt0, "is not >0", optional<double>(parameters.inlierTh));

	//RANSAC
	if(src.get_child_optional("RANSAC"))
		parameters.ransacParameters=InterfaceRANSACC::MakeParameterObject(PruneRANSAC(src.get_child("RANSAC")));

	//Additional RANSAC parameters
	auto geqMinGen= bind(greater_equal<double>(),_1, DualAbsoluteQuadricSolverC::GeneratorSize());
	parameters.sGenerator=ReadParameter(src, "RANSAC.Main.GeneratorSize", geqMinGen, "is smaller than the minimal sample size", optional<double>(parameters.sGenerator));
	parameters.sGenerator=ReadParameter(src, "RANSAC.LORANSAC.GeneratorSize", geqMinGen, "is smaller than the minimal sample size", optional<double>(parameters.sLOGenerator));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 * @remarks Assumption: Unit-norm feature vectors
 */
ptree InterfaceLinearAutocalibrationPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	tree.put("Main","");
	tree.put("RANSAC","");

	ptree ransacTree=InterfaceRANSACC::MakeParameterTree(src.ransacParameters, level);
	tree.put_child("RANSAC", PruneRANSAC(ransacTree));

	if(level>0)
	{
		tree.put("Main.InlierThreshold", src.inlierTh);
	}	//if(level>0)

	if(level>1)
	{
		tree.put("Main.NoWeights", src.nLACIteration);

		tree.put("RANSAC.Main.GeneratorSize", src.sGenerator);
		tree.put("RANSAC.LORANSAC.GeneratorSize", src.sLOGenerator);
	}	//if(level>2)

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

