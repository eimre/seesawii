/**
 * @file InterfaceGeometryEstimator.cpp Implementation of InterfaceGeometryEstimatorC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceGeometryEstimator.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant refinement parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceGeometryEstimatorC::PruneRefinement(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.FlagCovariance"))
		output.get_child("Main").erase("FlagCovariance");

	return output;
}	//ptree PruneRefinement(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceGeometryEstimatorC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT parameters;

	if(src.get_child_optional("RANSAC"))
		parameters.ransacParameters=InterfaceTwoStageRANSACC::MakeParameterObject(src.get_child("RANSAC"));

	if(src.get_child_optional("Refinement"))
		parameters.pdlParameters=InterfacePowellDogLegC::MakeParameterObject(PruneRefinement(src.get_child("Refinement")));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceGeometryEstimatorC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put_child("RANSAC", InterfaceTwoStageRANSACC::MakeParameterTree(src.ransacParameters, level));
	output.put_child("Refinement", PruneRefinement(InterfacePowellDogLegC::MakeParameterTree(src.pdlParameters, level)));

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

