/**
 * @file InterfaceGeometryEstimationPipeline.cpp Implementation of InterfaceGeometryEstimationPipelineC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceGeometryEstimationPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant geometry estimator parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceGeometryEstimationPipelineC::PruneGeometryEstimator(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("RANSAC.Main.NoThreads"))
		output.get_child("RANSAC.Main").erase("NoThreads");

	if(src.get_child_optional("RANSAC.Stage1.Main.FlagHistory"))
		output.get_child("RANSAC.Stage1.Main").erase("FlagHistory");

	if(src.get_child_optional("RANSAC.Stage2.Main.FlagHistory"))
		output.get_child("RANSAC.Stage2.Main").erase("FlagHistory");

	return output;
}	//ptree PruneGeometryEstimator(const ptree& src)

/**
 * @brief Removes the redundant matcher parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceGeometryEstimationPipelineC::PruneMatcher(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneGeometryEstimator(const ptree& src)

/**
 * @brief Removes the redundant SUT parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceGeometryEstimationPipelineC::PruneSUT(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("NoThreads"))
		output.erase("NoThreads");

	return output;
}	//ptree PruneGeometryEstimator(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceGeometryEstimationPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto c01=[](double val){return val>=0 && val<=1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.maxIteration=ReadParameter(src, "Main.MaxIteration", gt0, "is not >0", optional<double>(parameters.maxIteration));
	parameters.minRelativeDifference=ReadParameter(src, "Main.MinRelativeDifference", gt0, "is not >0", optional<double>(parameters.minRelativeDifference));
	parameters.refreshThreshold=ReadParameter(src, "Main.RefreshTreshold", geq1, "is not >=1", optional<double>(parameters.refreshThreshold));
	parameters.flagCovariance=src.get<bool>("Main.FlagCovariance", parameters.flagCovariance);
	parameters.flagVerbose=src.get<bool>("Main.FlagVerbose", parameters.flagVerbose);

	parameters.binDensity1=ReadParameter(src, "Decimation.BinDensity", gt0, "is not >0", optional<double>(parameters.binDensity1));
	parameters.binDensity2=parameters.binDensity1;
	parameters.maxObservationDensity=ReadParameter(src, "Decimation.MaxObservationDensity", gt0, "is not >0", optional<double>(parameters.maxObservationDensity));
	parameters.minObservationRatio=ReadParameter(src, "Decimation.MinObservationRatio", geq1, "is not >=1", optional<double>(parameters.minObservationRatio));
	parameters.maxObservationRatio=ReadParameter(src, "Decimation.MaxObservationRatio", geq1, "is not >=1", optional<double>(parameters.maxObservationRatio));
	parameters.validationRatio=ReadParameter(src, "Decimation.ValidationRatio", geq1, "is not >=1", optional<double>(parameters.validationRatio));
	parameters.maxAmbiguity=ReadParameter(src, "Decimation.MaxAmbiguity", c01, "is not in [0,1]", optional<double>(parameters.maxAmbiguity));

	if(src.get_child_optional("Matching"))
		parameters.matcherParameters=InterfaceFeatureMatcherC::MakeParameterObject(PruneMatcher(src.get_child("Matching")));

	if(src.get_child_optional("GeometryEstimation"))
		parameters.geometryEstimatorParameters=InterfaceGeometryEstimatorC::MakeParameterObject(PruneGeometryEstimator(src.get_child("GeometryEstimation")));

	if(src.get_child_optional("CovarianceEstimation"))
		parameters.sutParameters=InterfaceScaledUnscentedTransformationC::MakeParameterObject(PruneSUT(src.get_child("CovarianceEstimation")));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceGeometryEstimationPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	if(level>0)
	{
		output.put("Main","");
		output.put("Decimation","");
		output.put("Matching","");
		output.put("GeometryEstimation","");
		output.put("CovarianceEstimation","");

		output.put("Main.NoThreads", src.nThreads);
		output.put("Main.FlagCovariance", src.flagCovariance);
		output.put("Main.FlagVerbose", src.flagVerbose);
		output.put("Main.MaxIteration", src.maxIteration);
	}	//if(level>0)

	if(level>1)
	{
		output.put("Decimation.MaxObservationDensity", src.maxObservationDensity);
		output.put("Decimation.MinObservationRatio", src.minObservationRatio);
		output.put("Decimation.MaxObservationRatio", src.maxObservationRatio);
		output.put("Decimation.ValidationRatio", src.validationRatio);
	}	//if(level>1)

	if(level>2)
	{
		output.put("Main.MinRelativeDifference", src.minRelativeDifference);
		output.put("Main.RefreshTreshold", src.refreshThreshold);

		output.put("Decimation.BinDensity", src.binDensity1);
		output.put("Decimation.MaxAmbiguity", src.maxAmbiguity);
	}	//if(level>2)

	output.put_child("Matching", PruneMatcher(InterfaceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level)));
	output.put_child("GeometryEstimation", PruneGeometryEstimator(InterfaceGeometryEstimatorC::MakeParameterTree(src.geometryEstimatorParameters, level)));
	output.put_child("CovarianceEstimation", PruneSUT(InterfaceScaledUnscentedTransformationC::MakeParameterTree(src.sutParameters, level)));

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

