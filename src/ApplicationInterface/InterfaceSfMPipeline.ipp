/**
 * @file InterfaceSfMPipeline.ipp Implementation details for the application interface for the SfM pipeline
 * @author Evren Imre
 * @date 19 Jan 2017
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_SFM_PIPELINE_IPP_6888017
#define INTERFACE_SFM_PIPELINE_IPP_6888017

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include <map>
#include <string>
#include "../GeometryEstimationPipeline/SfMPipeline.h"
#include "InterfaceGeometryEstimator.h"
#include "InterfaceTwoStageRANSAC.h"
#include "InterfaceVisionGraphPipeline.h"
#include "InterfaceScaledUnscentedTransformation.h"
#include "InterfacePoseGraphPipeline.h"
#include "InterfaceLinearAutocalibrationPipeline.h"
#include "InterfaceSparse3DReconstructionPipeline.h"
#include "InterfacePowellDogLeg.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::greater;
using std::bind;
using std::map;
using std::string;
using SeeSawN::GeometryN::SfMPipelineParametersC;
using SeeSawN::ApplicationN::InterfaceGeometryEstimatorC;
using SeeSawN::ApplicationN::InterfaceTwoStageRANSACC;
using SeeSawN::ApplicationN::InterfaceVisionGraphPipelineC;
using SeeSawN::ApplicationN::InterfaceScaledUnscentedTransformationC;
using SeeSawN::ApplicationN::InterfacePoseGraphPipelineC;
using SeeSawN::ApplicationN::InterfaceLinearAutocalibrationPipelineC;
using SeeSawN::ApplicationN::InterfaceSparse3DReconstructionPipelineC;
using SeeSawN::ApplicationN::InterfacePowellDogLegC;

/**
 * @brief Interface for the SfM pipeline
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceSfMPipelineC
{
	private:

		/** @name Implementation details */ //@{

		static ptree PruneVisionGraphPipeline(const ptree& src);	///< Removes the redundant elements from a vision graph pipeline property tree
		static ptree PruneSUT(const ptree& src);	///< Removes the redundant elements from a SUT property tree
		static ptree PrunePoseGraphPipeline(const ptree& src);	///< Removes the redundant elements from a pose graph pipeline property tree
		static ptree PruneIntrinsicCalibrationPipeline(const ptree& src);	///< Removes the redundant elements from a intrinsic calibration pipeline property tree
		static ptree PruneSparse3DReconstructionPipeline(const ptree& src);	///< Removes the redundant elements from a sparse 3D reconstruction pipeline property tree
		static ptree PruneGeometryEstimationPipeline(const ptree& src);	///< Removes the redundant elements from a geometry estimation pipeline property tree

		//@}
	public:

		typedef SfMPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceSfMPipelineC

}	//ApplicationN
}	//SeeSawN




#endif /* INTERFACE_SFM_PIPELINE_IPP_6888017 */
