/**
 * @file InterfaceMultiviewPanoramaBuilder.ipp Implementation details for \c InterfaceMultiviewPanoramaBuilder
 * @author Evren Imre
 * @date 5 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_MULTIVIEW_PANORAMA_BUILDER_IPP_7901232
#define INTERFACE_MULTIVIEW_PANORAMA_BUILDER_IPP_7901232

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/math/constants/constants.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "../Geometry/MultiviewPanoramaBuilder.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using boost::math::constants::pi;
using std::bind;
using std::greater;
using std::greater_equal;
using SeeSawN::GeometryN::MultiviewPanoramaBuilderParametersC;
using SeeSawN::ApplicationN::ReadParameter;

/**
 * @brief Helper class for initialising a multiview panorama builder parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceMultiviewPanoramaBuilderC
{
	public:

		typedef MultiviewPanoramaBuilderParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object

};	//class InterfaceMultiviewTriangulationC

}	//namespace ApplicationN
}	//namespace SeeSawN



#endif /* INTERFACE_MULTIVIEW_PANORAMA_BUILDER_IPP_7901232 */
