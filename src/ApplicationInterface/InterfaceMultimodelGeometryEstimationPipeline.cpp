/**
 * @file InterfaceMultimodelGeometryEstimationPipeline.cpp Implementation of InterfaceMultimodelGeometryEstimationPipelineC
 * @author Evren Imre
 * @date 18 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceMultimodelGeometryEstimationPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant matcher parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceMultimodelGeometryEstimationPipelineC::PruneMatcher(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("RANSAC.Main.FlagHistory"))
		output.get_child("RANSAC.Main").erase("FlagHistory");

	return output;
}	//ptree PruneMatcher(const ptree& src)

/**
 * @brief Removes the redundant sequential RANSAC parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceMultimodelGeometryEstimationPipelineC::PruneSequentialRANSAC(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Main.MinInlierRatio"))
		output.get_child("Main").erase("MinInlierRatio");

	return output;
}	//ptree PruneSequentialRANSAC(const ptree& src)

/**
 * @brief Removes the redundant geometry estimation pipeline parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceMultimodelGeometryEstimationPipelineC::PruneGeometryEstimationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Main.FlagVerbose"))
		output.get_child("Main").erase("FlagVerbose");


	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)


/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @throws If a parameter has an invalid value
 * @return A parameter object
 */
auto InterfaceMultimodelGeometryEstimationPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT parameters;

	auto gt0=bind(greater<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.minInlierRatio=ReadParameter(src, "Main.MinInlierRatio", gt0, "is not >0", optional<double>(parameters.minInlierRatio));
	parameters.flagVerbose=src.get("Main.FlagVerbose", parameters.flagVerbose);

	if(src.get_optional<string>("Main.AssignmentStrategy"))
	{
		typedef ParameterObjectT::AssignmentStrategyT StrategyT;
		map<string, StrategyT> stringToStrategy{ {"SOFT", StrategyT::SOFT}, {"HARD", StrategyT::HARD}, {"UNIQUE", StrategyT::UNIQUE} };
		auto it=stringToStrategy.find(src.get<string>("Main.AssignmentStrategy"));

		if(it==stringToStrategy.end())
			throw(invalid_argument(string("Invalid value for Main.AssignmentStrategy. Value=")+it->first));

		parameters.assignmentType=it->second;
	}	//if(src.get_optional("Main.AssignmentStrategy"))

	parameters.maxObservationDensity=ReadParameter(src, "Identification.Decimator.MaxObservationDensity", gt0, "is not >0", optional<double>(parameters.maxObservationDensity));
	parameters.observationRatio=ReadParameter(src, "Identification.Decimator.ObservationRatio", geq1, "is not >=1", optional<double>(parameters.observationRatio));
	parameters.validatorRatio=ReadParameter(src, "Identification.Decimator.ValidatorRatio", geq1, "is not >=1", optional<double>(parameters.validatorRatio));
	parameters.binDensity1=ReadParameter(src, "Identification.Decimator.BinDensity1", gt0, "is not >0", optional<double>(parameters.binDensity1));
	parameters.binDensity2=ReadParameter(src, "Identification.Decimator.BinDensity2", gt0, "is not >0", optional<double>(parameters.binDensity2));

	if(src.get_child_optional("Identification.FeatureMatcher"))
		parameters.matcherParameters=InterfaceFeatureMatcherC::MakeParameterObject(PruneMatcher(src.get_child("Identification.FeatureMatcher")));

	if(src.get_child_optional("Identification.SequentialRANSAC"))
		parameters.ransacParameters=InterfaceSequentialRANSACC::MakeParameterObject(PruneSequentialRANSAC(src.get_child("Identification.SequentialRANSAC")));

	if(src.get_child_optional("Refinement.GeometryEstimationPipeline"))
		parameters.geometryEstimatorParameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(PruneGeometryEstimationPipeline(src.get_child("Refinement.GeoometryEstimationPipeline")));

	return parameters;
}	//ParameterObjectT MakeParameterObject(const ptree& src)

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceMultimodelGeometryEstimationPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output; //Options tree

	if(level>0)
	{
		output.put("Main","");

		output.put("Identification","");
		output.put("Identification.Decimator", "");
		output.put("Identification.FeatureMatcher", "");
		output.put("Identification.SequentialRANSAC", "");

		output.put("Refinement", "");
		output.put("Refinement.GeoometryEstimationPipeline", "");

		output.put("Main.NoThreads", src.nThreads);
		output.put("Main.MinInlierRatio", src.minInlierRatio);

		typedef ParameterObjectT::AssignmentStrategyT StrategyT;
		map<StrategyT, string> strategyToString{ {StrategyT::SOFT, "SOFT"}, {StrategyT::HARD, "HARD"}, {StrategyT::UNIQUE, "UNIQUE"} };
		output.put("Main.AssignmentStrategy", strategyToString[src.assignmentType]);
		output.put("Main.FlagVerbose", src.flagVerbose);
	}	//if(level>0)

	if(level>1)
	{
		output.put("Identification.Decimator.MaxObservationDensity", src.maxObservationDensity);
		output.put("Identification.Decimator.ObservationRatio", src.observationRatio);
		output.put("Identification.Decimator.ValidatorRatio", src.validatorRatio);
		output.put("Identification.Decimator.BinDensity1", src.binDensity1);
		output.put("Identification.Decimator.BinDensity2", src.binDensity2);
	}	//if(level>1)

	output.put_child("Identification.FeatureMatcher", PruneMatcher(InterfaceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level)));
	output.put_child("Identification.SequentialRANSAC", PruneSequentialRANSAC(InterfaceSequentialRANSACC::MakeParameterTree(src.ransacParameters, level)));
	output.put_child("Refinement.GeometryEstimationPipeline", PruneGeometryEstimationPipeline(InterfaceGeometryEstimationPipelineC::MakeParameterTree(src.geometryEstimatorParameters, level)));

	return output;
}	//ptree MakeParameterTree(const RANSACParametersC& parameters)


}	//ApplicationN
}	//SeeSawN

