/**
 * @file InterfaceSparse3DReconstructionPipeline.ipp Implementation of InterfaceSparse3DReconstructionPipelineC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_SPARSE_3D_RECONSTRUCTION_PIPELINE_IPP_0589213
#define INTERFACE_SPARSE_3D_RECONSTRUCTION_PIPELINE_IPP_0589213

#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "InterfaceMultiviewTriangulation.h"
#include "InterfaceMultisourceFeatureMatcher.h"
#include "../GeometryEstimationPipeline/Sparse3DReconstructionPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::lexical_cast;
using std::bind;
using std::greater;
using std::greater_equal;
using std::less;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceMultisourceFeatureMatcherC;
using SeeSawN::ApplicationN::InterfaceMultiviewTriangulationC;
using SeeSawN::GeometryN::Sparse3DReconstructionPipelineParametersC;

/**
 * @brief Helper class for initialising a feature matcher parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceSparse3DReconstructionPipelineC
{
	private:

		static ptree PruneTriangulation(const ptree& src);	///< Removes the redundant triangulation parameters
		static ptree PruneMatcher(const ptree& src);	///< Removes the redundant matcher parameters

	public:

		typedef Sparse3DReconstructionPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//	class InterfaceSparse3DReconstructionPipelineC


}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_SPARSE_3D_RECONSTRUCTION_PIPELINE_IPP_0589213 */
