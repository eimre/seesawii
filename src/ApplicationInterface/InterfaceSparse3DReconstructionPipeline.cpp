/**
 * @file InterfaceSparse3DReconstructionPipeline.cpp Implementation of InterfaceSparse3DReconstructionPipelineC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceSparse3DReconstructionPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant triangulation parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSparse3DReconstructionPipelineC::PruneTriangulation(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.MaxError"))
		output.get_child("Main").erase("MaxError");

	if(src.get_child_optional("Covariance.NoiseVariance"))
		output.get_child("Covariance").erase("NoiseVariance");

	return output;
}	//ptree PruneTriangulation(const ptree& src)

/**
 * @brief Removes the redundant matcher parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSparse3DReconstructionPipelineC::PruneMatcher(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Matcher.kNN.NeighbourhoodCardinality"))
		output.get_child("Matcher.kNN").erase("NeighbourhoodCardinality");

	return output;
}	//ptree PruneMatcher(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @remarks Assumption: Unit-norm feature vectors
 * @return A parameter object
 */
auto InterfaceSparse3DReconstructionPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto o01=[](double val){return val>0 && val<1;};

	ParameterObjectT parameters;

	parameters.flagVerbose=src.get<bool>("Main.FlagVerbose", parameters.flagVerbose);
	parameters.pRejection=ReadParameter(src, "Main.RejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.pRejection));
	parameters.noiseVariance=ReadParameter(src, "Main.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));
	parameters.flagCompact=src.get<bool>("Main.FlagCompact", parameters.flagVerbose);

	if(src.get_child_optional("Triangulation"))
	{
		ptree treeTriangulation=src.get_child("Triangulation");
		parameters.triangulationParameters=InterfaceMultiviewTriangulationC::MakeParameterObject(PruneTriangulation(treeTriangulation));
	}

	if(src.get_child_optional("MultiviewMatching"))
	{
		ptree treeMatcher=src.get_child("MultiviewMatching");
		parameters.matcherParameters=InterfaceMultisourceFeatureMatcherC::MakeParameterObject(PruneMatcher(treeMatcher));
	}

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceSparse3DReconstructionPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Triangulation","");
		tree.put("MultiviewMatching","");

		tree.put("Main.NoiseVariance", src.noiseVariance);
		tree.put("Main.FlagCompact", src.flagCompact);
	}

	if(level>1)
	{
		tree.put("Main.RejectionProbability", src.pRejection);
		tree.put("Main.FlagVerbose", src.flagVerbose);
	}

	ptree treeTriangulation=InterfaceMultiviewTriangulationC::MakeParameterTree(src.triangulationParameters, level);
	tree.put_child("Triangulation", PruneTriangulation(treeTriangulation));

	ptree treeMatching=InterfaceMultisourceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level);
	tree.put_child("MultiviewMatching", PruneMatcher(treeMatching));

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

