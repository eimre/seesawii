/**
 * @file InterfaceRelativeSynchronisation.ipp Implementation details for \c InterfaceRelativeSynchronisationC
 * @author Evren Imre
 * @date 5 Dec 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_RELATIVE_SYNCHRONISATION_IPP_1702840
#define INTERFACE_RELATIVE_SYNCHRONISATION_IPP_1702840

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <functional>
#include <vector>
#include <array>
#include <stdexcept>
#include "InterfaceUtility.h"
#include "InterfaceFeatureMatcher.h"
#include "InterfacePowellDogLeg.h"
#include "../Synchronisation/RelativeSynchronisation.h"
#include "../StateEstimation/ViterbiRelativeSynchronisationProblem.h"
#include "../Wrappers/BoostTokenizer.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::optional;
using boost::property_tree::ptree;
using boost::lexical_cast;
using std::greater_equal;
using std::greater;
using std::string;
using std::vector;
using std::array;
using std::invalid_argument;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::ReadParameterArray;
using SeeSawN::ApplicationN::InterfaceFeatureMatcherC;
using SeeSawN::ApplicationN::InterfacePowellDogLegC;
using SeeSawN::SynchronisationN::RelativeSynchronisationParametersC;
using SeeSawN::StateEstimationN::ViterbiRelativeSynchronisationProblemC;
using SeeSawN::WrappersN::Tokenise;

/**
 * @brief Helper class for initialising a relative synchronisation parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceRelativeSynchronisationC
{

	private:

		static ptree PrunePDL(const ptree& src);	///< Removes the redundant PDL parameters
		static ptree PruneFeatureMatcher(const ptree& src);	///< Removes the redundant feature matcher parameters

	public:

		typedef RelativeSynchronisationParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object

};	//class InterfaceRelativeSynchronisationC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_RELATIVE_SYNCHRONISATION_IPP_1702840 */
