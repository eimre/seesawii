/**
 * @file InterfaceNodalCameraTrackingPipeline.ipp Implementation details for the application interface for \c NodalCameraTrackingPipelineC
 * @author Evren Imre
 * @date 12 Aug 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_NODAL_CAMERA_TRACKING_PIPELINE_IPP_6801990
#define INTERFACE_NODAL_CAMERA_TRACKING_PIPELINE_IPP_6801990

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/math/constants/constants.hpp>
#include <functional>
#include <map>
#include <string>
#include <cmath>
#include "InterfaceUtility.h"
#include "InterfaceGeometryEstimationPipeline.h"
#include "InterfaceTwoStageRANSAC.h"
#include "../GeometryEstimationPipeline/NodalCameraTrackingPipeline.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using boost::math::constants::pi;
using std::greater;
using std::greater_equal;
using std::bind;
using std::map;
using std::string;
using std::tan;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineParametersC;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceGeometryEstimationPipelineC;
using SeeSawN::ApplicationN::InterfaceTwoStageRANSACC;

/**
 * @brief Interface for the nodal camera tracking pipeline
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceNodalCameraTrackingPipelineC
{
	private:

		static ptree PruneGeometryEstimationPipeline(const ptree& src);	///<Removes the redundant geometry estimation pipeline parameters

	public:

		typedef NodalCameraTrackingPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceNodalCameraTrackingPipelineC

}	//namespace ApplicationN
}	//namespace SeeSawN



#endif /* INTERFACE_NODAL_CAMERA_TRACKING_PIPELINE_IPP_6801990 */
