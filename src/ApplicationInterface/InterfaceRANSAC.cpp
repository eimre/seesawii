/**
 * @file InterfaceRANSAC.cpp Implementation of InterfaceRANSACC
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceRANSAC.h"
namespace SeeSawN
{
namespace	ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceRANSACC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT parameters;

	//Common functors
	auto lo01=[](double val){return val>0 && val<=1;};
	auto c01=[](double val){return val>=0 && val<=1;};
	auto o01=[](double val){return val>0 && val<1;};
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);

	//Main
	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.minConfidence=ReadParameter(src, "Main.MinConfidence", lo01, "is not in (0,1]", optional<double>(parameters.minConfidence));
	parameters.minIteration=ReadParameter(src, "Main.MinIteration", geq0, "is not >=0", optional<double>(parameters.minIteration));
	parameters.maxIteration=ReadParameter(src, "Main.MaxIteration", bind(greater_equal<double>(),_1,parameters.minIteration), "is not >=Main.MinIteration", optional<double>(parameters.maxIteration));
	parameters.eligibilityRatio=ReadParameter(src, "Main.EligibilityRatio", lo01, "is not in (0,1]", optional<double>(parameters.eligibilityRatio));
	parameters.minError=src.get<double>("Main.MinError", parameters.minError);
	parameters.maxError=ReadParameter(src, "Main.MaxError", bind(greater_equal<double>(),_1,parameters.minError), "is not >=Main.MinError", optional<double>(parameters.maxError));

	if(src.get_optional<double>("Main.TopN"))
		parameters.topN=ReadParameter(src, "Main.TopN", lo01, "is not in (0,1]", optional<double>(-1));	//The default value triggers an exception!

	//LORANSAC
	parameters.nLOIterations=ReadParameter(src, "LORANSAC.NoIterations", geq0, "is not >=0", optional<double>(parameters.nLOIterations));
	parameters.maxErrorRatio=ReadParameter(src, "LORANSAC.MaxErrorRatio",geq1, "is not >=1", optional<double>(parameters.maxErrorRatio));
	parameters.minObservationSupport=ReadParameter(src, "LORANSAC.MinObservationRatio", geq1, "is not >=1", optional<double>(parameters.minObservationSupport));

	//PROSAC
	parameters.flagPROSAC=src.get<bool>("PROSAC.Enable", parameters.flagPROSAC);
	parameters.growthRate=ReadParameter(src, "PROSAC.GrowthRate", lo01, "is not in (0,1]", optional<double>(parameters.growthRate) );

	//MORANSAC
	parameters.maxSolution=ReadParameter(src, "MORANSAC.MaxNoCluster", geq1, "is not >=1", optional<double>(parameters.maxSolution));
	parameters.minCoverage=ReadParameter(src, "MORANSAC.MinCoverage", c01, "is not in [0,1]", optional<double>(parameters.minCoverage) );
	parameters.minModelSimilarity=ReadParameter(src, "MORANSAC.MinOverlapIndex", c01, "is not in [0,1]", optional<double>(parameters.minModelSimilarity) );
	parameters.parsimonyFactor=ReadParameter(src, "MORANSAC.ParsimonyFactor", geq1, "is not >=1", optional<double>(parameters.parsimonyFactor));
	parameters.diversityFactor=ReadParameter(src, "MORANSAC.DiversityFactor", geq0, "is not >=1", optional<double>(parameters.diversityFactor));

	//SPRT
	parameters.flagWaldSAC=src.get<bool>("SPRT.Enable", parameters.flagWaldSAC);
	parameters.epsilon=ReadParameter(src, "SPRT.InitialEpsilon", o01, "is not in (0,1)", optional<double>(parameters.epsilon) );
	parameters.delta=ReadParameter(src, "SPRT.InitialDelta", o01, "is not in (0,1)", optional<double>(parameters.delta) );
	parameters.delta=ReadParameter(src, "SPRT.InitialDelta", bind(less<double>(), _1, parameters.epsilon), "is not <SPRT.InitialEpsilon", optional<double>(parameters.delta) );
	parameters.maxDeltaTolerance=ReadParameter(src, "SPRT.MaxDeltaTolerance", lo01, "is not in (0,1]", optional<double>(parameters.maxDeltaTolerance));

	parameters.flagHistory=src.get<bool>("Main.FlagHistory", parameters.flagHistory);

	return parameters;
}	//RANSACParametersC MakeParameterObject(const ptree& parameterTree)

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceRANSACC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Main.NoThreads", src.nThreads);

		tree.put("Main.MaxIteration", src.maxIteration);

		tree.put("LORANSAC","");
		tree.put("LORANSAC.NoIterations", src.nLOIterations);

		tree.put("MORANSAC","");
		tree.put("MORANSAC.MaxNoCluster", src.maxSolution);
	}	//if(level>0)

	if(level>1)
	{
		tree.put("Main.MinIteration", src.minIteration);
		tree.put("Main.EligibilityRatio", src.eligibilityRatio);
		tree.put("Main.MinConfidence", src.minConfidence);
		tree.put("Main.FlagHistory", src.flagHistory);

		if(src.topN)
			tree.put("Main.TopN", *src.topN);


		tree.put("LORANSAC.MinimumObservationSupportRatio", src.minObservationSupport);	//Minimum observation support to trigger LORANSAC, as a multiple of the minimal generator size

		tree.put("PROSAC","");
		tree.put("PROSAC.Enable", src.flagPROSAC);
		tree.put("PROSAC.GrowthRate", src.growthRate);

		tree.put("SPRT","");
		tree.put("SPRT.Enable", src.flagWaldSAC);

		tree.put("MORANSAC.MinCoverage", src.minCoverage);
		tree.put("MORANSAC.MinOverlapIndex", src.minModelSimilarity);
		tree.put("MORANSAC.ParsimonyFactor", src.parsimonyFactor);
		tree.put("MORANSAC.DiversityFactor", src.diversityFactor);
	}	//if(level>1)


	if(level>2)
	{
		tree.put("Main.MinError", src.minError);
		tree.put("Main.MaxError", src.maxError);

		tree.put("LORANSAC.MaxErrorRatio", src.maxErrorRatio);

		tree.put("SPRT.InitialEpsilon", src.epsilon);
		tree.put("SPRT.InitialDelta", src.delta);
		tree.put("SPRT.MaxDeltaTolerance", src.maxDeltaTolerance);
	}	//if(level>2)

	return tree;
}	//ptree MakeParameterTree(const RANSACParametersC& parameters)

/**
 * @brief Augments the tree with the parameters for \c RANSACGeometryEstimationProblemC
 * @param[in,out] src Property tree to be augmented
 * @param[in] rootPath Path to the root of the tree
 * @param[in] level Higher levels expose more parameters
 */
void InterfaceRANSACC::InsertGeometryProblemParameters(ptree& src, const string& rootPath, unsigned int level)
{
    if(level>1)
    {
        src.put(rootPath+"."+"LORANSAC.GeneratorRatio","3");	//Size of the generator set for LO, as a multiplier of the minimal generator
        src.put(rootPath+"."+"MORANSAC.MaxAlgebraicDistance", "1");	//Max permissible algebraic distance between two models in the same cluster, relative to the maximum possible distance
    }
}	//void InsertGeometryProblemParameters(ptree& src)

/**
 * @brief Extracts the parameters for \c RANSACGeometryEstimationProblemC
 * @param[in] src Property tree holding the parameters
 * @return A map with the extracted parameters
 */
auto InterfaceRANSACC::ExtractGeometryProblemParameters(const ptree& src) -> ParameterMapT
{
	ParameterMapT parameters;

	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);

	double generatorRatio=ReadParameter(src, "LORANSAC.GeneratorRatio", geq1, "is not >=1", optional<double>(3));
	parameters.emplace("LORANSAC.GeneratorRatio", lexical_cast<string>(generatorRatio));

	double maxAlgebraicDistance=ReadParameter(src, "MORANSAC.MaxAlgebraicDistance", geq0, "is not >=0", optional<double>(1));
	parameters.emplace("MORANSAC.MaxAlgebraicDistance", lexical_cast<string>(maxAlgebraicDistance));

	return parameters;
}	//ParameterMapT ExtractGeometryProblemParameters(const ptree& src)

}	//ApplicationN
}	//SeeSawN

