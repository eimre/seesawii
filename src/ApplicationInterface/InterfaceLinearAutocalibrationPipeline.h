/**
 * @file InterfaceLinearAutocalibrationPipeline.h Public interface for \c InterfaceLinearAutocalibrationPipelineC
 * @author Evren Imre
 * @date 3 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_LINEAR_AUTOCALIBRATION_PIPELINE_H_9612397
#define INTERFACE_LINEAR_AUTOCALIBRATION_PIPELINE_H_9612397

#include "InterfaceLinearAutocalibrationPipeline.ipp"
namespace SeeSawN
{
namespace ApplicationN
{

class InterfaceLinearAutocalibrationPipelineC;	///< Helper class for initialising a linear autocalibration pipeline parameter object from a property tree

}	//ApplicationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template std::string boost::lexical_cast<std::string, double>(const double&);

#endif /* INTERFACE_LINEAR_AUTOCALIBRATION_PIPELINE_H_9612397 */
