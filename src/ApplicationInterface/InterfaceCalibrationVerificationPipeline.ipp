/**
 * @file InterfaceCalibrationVerificationPipeline.ipp Implementation of \c InterfaceCalibrationVerificationPipelineC
 * @author Evren Imre
 * @date 3 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_CALIBRATION_VERIFICATION_PIPELINE_IPP_1680131
#define INTERFACE_CALIBRATION_VERIFICATION_PIPELINE_IPP_1680131

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <string>
#include <functional>
#include <map>
#include "InterfaceGeometryEstimationPipeline.h"
#include "InterfaceTwoStageRANSAC.h"
#include "InterfaceUtility.h"
#include "../GeometryEstimationPipeline/CalibrationVerificationPipeline.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::bind;
using std::greater;
using std::greater_equal;
using std::string;
using std::map;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;
using SeeSawN::GeometryN::CalibrationVerificationPipelineParametersC;
using SeeSawN::ApplicationN::InterfaceGeometryEstimationPipelineC;
using SeeSawN::ApplicationN::InterfaceTwoStageRANSACC;
using SeeSawN::ApplicationN::ReadParameter;

/**
 * @brief Helper class for initialising a calibration verification pipeline parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceCalibrationVerificationPipelineC
{
	private:

		static ptree PruneGeometryEstimationPipeline(const ptree& src);	///< Removes the redundant geometry estimation pipeline parameters

	public:

		typedef CalibrationVerificationPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object

};	//class InterfaceCalibrationVerificationPipelineC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACECALIBRATIONVERIFICATIONPIPELINE_IPP_ */
