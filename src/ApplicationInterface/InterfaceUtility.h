/**
 * @file InterfaceUtility.h
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_UTILITY_H_1709213
#define INTERFACE_UTILITY_H_1709213

#include "InterfaceUtility.ipp"
namespace SeeSawN
{
namespace ApplicationN
{

template<typename ValueT, class ValidatorT> ValueT ReadParameter(const ptree& src, const string& tag, const ValidatorT& validator, const string& errorMessage, const optional<ValueT>& defaultValue);	///< Reads and validates a parameter from a property tree
template<typename ValueT, class ValidatorT> vector<ValueT> ReadParameterArray(const ptree& src, const string& tag, size_t nElements, const ValidatorT& validator, const string& errorMessage, const string& delimiters);	///< Reads and validates a parameter array from a property tree

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_UTILITY_H_1709213 */

