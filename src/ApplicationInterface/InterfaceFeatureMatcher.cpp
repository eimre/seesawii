/**
 * @file InterfaceFeatureMatcher.cpp Implementation of InterfaceFeatureMatcherC
 * @author Evren Imre
 * @date 17 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceFeatureMatcher.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @remarks Assumption: Unit-norm feature vectors
 * @remarks \c FeatureMatcherParametersC::flagStatic is application-dependent, and is not set here
 * @return A parameter object
 */
auto InterfaceFeatureMatcherC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto gt0=bind(greater<double>(),_1,0);
	auto c01=[](double val){return val>=0 && val<=1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));

	//Bucket density
	parameters.flagBucketFilter=src.get<bool>("SpatialConsistency.Enable", parameters.flagBucketFilter);
	parameters.bucketSimilarityThreshold=ReadParameter(src, "SpatialConsistency.MinSimilarity", c01, "is not in [0,1]", optional<double>(parameters.bucketSimilarityThreshold));
	parameters.minQuorum=ReadParameter(src, "SpatialConsistency.MinQuorum", geq0, "is not >=0", optional<double>(parameters.minQuorum));

	double bucketDensity=ReadParameter(src, "SpatialConsistency.BucketDensity", gt0, "is not >0", optional<double>(1.0/parameters.bucketDimensions[0]));
	parameters.bucketDimensions[0]=1.0/bucketDensity;
	parameters.bucketDimensions[1]=parameters.bucketDimensions[0];

	//NN matcher

	parameters.nnParameters.ratioTestThreshold=ReadParameter(src, "kNN.MaxRatio", c01, "is not in [0,1]", optional<double>(parameters.nnParameters.ratioTestThreshold));

	unsigned int neighbourhoodCardinality=ReadParameter(src, "kNN.NeighbourhoodCardinality", geq1, "is not >=1", optional<double>(parameters.nnParameters.neighbourhoodSize12));
	parameters.nnParameters.neighbourhoodSize12=neighbourhoodCardinality;
	parameters.nnParameters.neighbourhoodSize21=neighbourhoodCardinality;
	parameters.nnParameters.similarityTestThreshold=ReadParameter(src, "kNN.MinSimilarity", geq0, "is not >=0", optional<double>(parameters.nnParameters.similarityTestThreshold));
	parameters.nnParameters.flagConsistencyTest=src.get<bool>("kNN.FlagConsistencyTest", parameters.nnParameters.flagConsistencyTest);

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 * @remarks Assumption: Unit-norm feature vectors
 */
ptree InterfaceFeatureMatcherC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Main.NoThreads", src.nThreads);

		tree.put("kNN","");
		tree.put("kNN.MaxRatio", src.nnParameters.ratioTestThreshold);
		tree.put("kNN.NeighbourhoodCardinality", src.nnParameters.neighbourhoodSize12);


		tree.put("SpatialConsistency","");
		tree.put("SpatialConsistency.Enable", src.flagBucketFilter);
	}	//if(level>0)

	if(level>1)
	{
	    tree.put("kNN.MinSimilarity", src.nnParameters.similarityTestThreshold);

		tree.put("SpatialConsistency.MinSimilarity", src.bucketSimilarityThreshold);
		tree.put("SpatialConsistency.MinQuorum", src.minQuorum);
		tree.put("SpatialConsistency.BucketDensity", 1.0/src.bucketDimensions[0]);

		tree.put("kNN.FlagConsistencyTest", src.nnParameters.flagConsistencyTest);
	}	//if(level>1)


	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

