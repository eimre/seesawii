/**
 * @file InterfaceUtility.ipp
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_UTILITY_IPP_5690123
#define INTERFACE_UTILITY_IPP_5690123

#include <boost/concept_check.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <stdexcept>
#include <iostream>
#include <vector>
#include "../Wrappers/BoostTokenizer.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using boost::lexical_cast;
using boost::UnaryPredicateConcept;
using std::invalid_argument;
using std::string;
using std::vector;
using SeeSawN::WrappersN::Tokenise;

/**
 * @brief Reads and validates a parameter from a property tree
 * @tparam ValueT Value type
 * @tparam ValidatorT Validator type
 * @param[in] src Property tree holding the parameter
 * @param[in] tag Tag identifying the parameter
 * @param[in] validator Validation condition
 * @param[in] errorMessage Error message denoting the constraint failure
 * @param[in] defaultValue Default value. If invalid, no default value
 * @return A parameter value
 * @pre \c ValidatorT is a unary predicate for arguments of type \c ValueT
 * @throws invalid_argument If the parameter fails the validator, or if the parameter is not found, and has no default value
 * @ingroup Utility
 */
template<typename ValueT, class ValidatorT>
ValueT ReadParameter(const ptree& src, const string& tag, const ValidatorT& validator, const string& errorMessage, const optional<ValueT>& defaultValue=optional<ValueT>())
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((UnaryPredicateConcept<ValidatorT, ValueT>));

	ValueT val;
	if(defaultValue)
		val=src.get<ValueT>(tag,*defaultValue);
	else
		val=src.get<ValueT>(tag);

	if(!validator(val))
		throw(invalid_argument(string("ReadParameter: ")+tag+" "+errorMessage+string(". Value=")+lexical_cast<string>(val)) );

	return val;
}	//typename Constraint1T::argument_type ReadParameter(const string& tag, const Constraint1T::argument_type& defaultValue, const Constraint1T& constraint1, const Constraint2T& constraint2, const string& errorMessage)

/**
 * @brief Reads and validates a parameter array from a property tree
 * @tparam ValueT Value type
 * @tparam ValidatorT Validator type
 * @param[in] src Property tree holding the parameter
 * @param[in] tag Tag identifying the parameter
 * @param[in] nElements Number of elements in the array
 * @param[in] validator Validation condition for the individual elements
 * @param[in] errorMessage Error message denoting the constraint failure
 * @param[in] delimiters Delimiter characters
 * @return A parameter value
 * @pre \c ValidatorT is a unary predicate for arguments of type \c ValueT
 * @throws invalid_argument If the parameter fails the validator, or if the parameter is not found
 * @throws invalid_argument If the array does not contain the specified number of elements
 * @ingroup Utility
 */
template<typename ValueT, class ValidatorT>
vector<ValueT> ReadParameterArray(const ptree& src, const string& tag, size_t nElements, const ValidatorT& validator, const string& errorMessage, const string& delimiters=string(" "))
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((UnaryPredicateConcept<ValidatorT, ValueT>));

	string val=src.get<string>(tag);	//Read as a string

	//Extract the tokens
	vector<ValueT> tokens=Tokenise<ValueT>(val, delimiters);

	if(tokens.size()!=nElements)
		throw(invalid_argument(string("ReadParameter: ")+tag+" does not have "+lexical_cast<string>(nElements)+" elements. Value="+val));

	//Validate the tokens
	for(const auto& current : tokens)
		if(!validator(current))
			throw(invalid_argument(string("ReadParameter: ")+tag+" "+errorMessage+string(". Value=")+lexical_cast<string>(val)) );

	return tokens;
}	//vector<ValueT> ReadParameterArray(const ptree& src, const string& tag, size_t nElements, const ValidatorT& validator, const string& errorMessage, const optional<ValueT>& defaultValue)

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_UTILITY_IPP_5690123 */
