/**
 * @file InterfaceSequentialRANSAC.cpp Implementation of InterfaceSequentialRANSACC
 * @author Evren Imre
 * @date 18 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceSequentialRANSAC.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant RANSAC parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSequentialRANSACC::PruneRANSAC(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneRANSAC(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceSequentialRANSACC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT parameters;

	auto gt0=bind(greater<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.maxIteration=src.get<double>("Main.MaxIteration", parameters.maxIteration);
	parameters.minInlierRatio=ReadParameter(src, "Main.MinInlierRatio", gt0, "is not >0", optional<double>(parameters.minInlierRatio));

	if(src.get_child_optional("RANSAC"))
		parameters.ransacParameters=InterfaceRANSACC::MakeParameterObject(PruneRANSAC(src.get_child("RANSAC")));

	return parameters;
}	//ParameterObjectT MakeParameterObject(const ptree& src)


/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceSequentialRANSACC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output; //Options tree

	if(level>0)
	{
		output.put("Main","");
		output.put("Main.NoThreads", src.nThreads);
		output.put("Main.MaxIteration", src.maxIteration);
		output.put("Main.MinInlierRatio", src.minInlierRatio);
	}	//if(level>0)

	output.put_child("RANSAC", PruneRANSAC(InterfaceRANSACC::MakeParameterTree(src.ransacParameters, level)));

	return output;
}	//ptree MakeParameterTree(const RANSACParametersC& parameters)

}	//ApplicationN
}	//SeeSawN

