/**
 * @file InterfaceMulticameraSynchronisation.ipp Implementation details for \c InterfaceMulticameraSynchronisationC
 * @author Evren Imre
 * @date 11 Dec 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_MULTICAMERA_SYNCHRONISATION_IPP_1589212
#define INTERFACE_MULTICAMERA_SYNCHRONISATION_IPP_1589212

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <string>
#include <functional>
#include "InterfaceUtility.h"
#include "InterfaceRelativeSynchronisation.h"
#include "../Synchronisation/MulticameraSynchronisation.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::string;
using std::bind;
using std::greater_equal;
using std::greater;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceRelativeSynchronisationC;
using SeeSawN::SynchronisationN::MulticameraSynchonisationParametersC;

/**
 * @brief Helper class for initialising a multicamera synchronisation parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceMulticameraSynchronisationC
{
	private:

		static ptree PruneRelativeSynchronisation(const ptree& src);	///< Removes the redundant relative synchronisation parameters

	public:

		typedef MulticameraSynchonisationParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceMulticameraSynchronisationC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_MULTICAMERA_SYNCHRONISATION_IPP_1589212 */
