/**
 * @file InterfacePowellDogLeg.cpp
 * @author Evren Imre
 * @date 16 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfacePowellDogLeg.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfacePowellDogLegC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	//Common functors
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto gt1=bind(greater<double>(),_1,1);
	auto o01=[](double val){return val>0 && val<1;};

	ParameterObjectT parameters;

	//Main
	parameters.maxIteration=ReadParameter(src, "Main.MaxIteration", geq0, "is not >=0", optional<double>(parameters.maxIteration));
	parameters.epsilon1=ReadParameter(src, "Main.Epsilon1", gt0, "is not >0", optional<double>(parameters.epsilon1));
	parameters.epsilon2=ReadParameter(src, "Main.Epsilon2", gt0, "is not >0", optional<double>(parameters.epsilon2));

	parameters.flagCovariance=src.get<bool>("Main.FlagCovariance", false);

	//TrustRegion
	parameters.delta0=ReadParameter(src, "TrustRegion.Delta0", gt0, "is not >0", optional<double>(parameters.delta0));
	parameters.lambdaShrink=ReadParameter(src, "TrustRegion.LambdaShrink", o01, "is not in (0,1)", optional<double>(parameters.lambdaShrink));
	parameters.lambdaGrowth=ReadParameter(src, "TrustRegion.LambdaGrowth", gt1, "is not >1", optional<double>(parameters.lambdaGrowth));

	return parameters;
}	//RANSACParametersC MakeParameterObject(const ptree& parameterTree)

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfacePowellDogLegC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Main.MaxIteration", src.maxIteration);
	}	//if(level>0)

	if(level>1)
		tree.put("Main.FlagCovariance", src.flagCovariance);

	if(level>2)
	{
		tree.put("Main.Epsilon1", src.epsilon1);
		tree.put("Main.Epsilon2", src.epsilon2);

		tree.put("TrustRegion","");
		tree.put("TrustRegion.Delta0", src.delta0);
		tree.put("TrustRegion.LambdaShrink", src.lambdaShrink);
		tree.put("TrustRegion.LambdaGrowth", src.lambdaGrowth);
	}	//if(level>2)

	return tree;
}	//ptree MakeParameterTree(const RANSACParametersC& parameters)

}	//ApplicationN
}	//SeeSawN

