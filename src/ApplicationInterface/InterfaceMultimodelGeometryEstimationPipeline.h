/**
 * @file InterfaceMultimodelGeometryEstimationPipeline.h Public interface for InterfaceMultimodelGeometryEstimationPipelineC
 * @author Evren Imre
 * @date 18 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_H_8612093
#define INTERFACE_MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_H_8612093

#include "InterfaceMultimodelGeometryEstimationPipeline.ipp"
namespace SeeSawN
{
namespace ApplicationN
{
class InterfaceMultimodelGeometryEstimationPipelineC;	///< Helper class for initialising an multimodel geometry estimation pipeline parameter object from a property tree
}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_H_8612093 */
