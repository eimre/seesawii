/**
 * @file InterfaceMultimodelGeometryEstimationPipeline.ipp Implementation details for InterfaceMultimodelGeometryEstimationPipelineC
 * @author Evren Imre
 * @date 18 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_IPP_7601293
#define INTERFACE_MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_IPP_7601293

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include <map>
#include <string>
#include "InterfaceFeatureMatcher.h"
#include "InterfaceSequentialRANSAC.h"
#include "InterfaceGeometryEstimationPipeline.h"
#include "InterfaceUtility.h"
#include "../GeometryEstimationPipeline/MultimodelGeometryEstimationPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::map;
using std::string;
using std::string;
using std::bind;
using std::greater;
using std::greater_equal;
using std::less;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceFeatureMatcherC;
using SeeSawN::ApplicationN::InterfaceSequentialRANSACC;
using SeeSawN::ApplicationN::InterfaceGeometryEstimationPipelineC;
using SeeSawN::GeometryN::MultimodelGeometryEstimationPipelineParametersC;

/**
 * @brief Helper class for initialising a Sequential RANSAC parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceMultimodelGeometryEstimationPipelineC
{
	private:

		static ptree PruneMatcher(const ptree& src);	///< Prunes the redundant matcher parameters
		static ptree PruneSequentialRANSAC(const ptree& src);	///< Prunes the redundant sequential RANSAC parameters
		static ptree PruneGeometryEstimationPipeline(const ptree& src);	///< Prunes the redundant geometry estimation pipeline parameters

	public:

		typedef MultimodelGeometryEstimationPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceRANSACC


}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_MULTIMODEL_GEOMETRY_ESTIMATION_PIPELINE_IPP_7601293 */
