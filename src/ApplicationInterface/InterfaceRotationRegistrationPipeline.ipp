/**
 * @file InterfaceRotationRegistrationPipeline.ipp Implementation details for \c InterfaceRotationRegistrationPipelineC
 * @author Evren Imre
 * @date 15 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_ROTATION_REGISTRATION_PIPELINE_IPP_9012943
#define INTERFACE_ROTATION_REGISTRATION_PIPELINE_IPP_9012943

#include <boost/property_tree/ptree.hpp>
#include <boost/math/constants/constants.hpp>
#include "InterfaceUtility.h"
#include "InterfaceRandomSpanningTreeSampler.h"
#include "../GeometryEstimationPipeline/RotationRegistrationPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::math::constants::pi;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceRandomSpanningTreeSamplerC;
using SeeSawN::GeometryN::RotationRegistrationPipelineParametersC;

/**
 * @brief Application interface for RotationRegistrationPipelineC
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceRotationRegistrationPipelineC
{
	public:

		typedef RotationRegistrationPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceRotationRegistrationPipelineC

}	//ApplicationN
}	//SeeSawN


#endif /* INTERFACE_ROTATION_REGISTRATION_PIPELINE_IPP_9012943 */
