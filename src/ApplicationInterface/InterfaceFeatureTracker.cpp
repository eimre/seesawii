/**
 * @file InterfaceFeatureTracker.cpp Implementation of InterfaceFeatureTrackerC
 * @author Evren Imre
 * @date 19 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceFeatureTracker.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant matcher parameters from the tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceFeatureTrackerC::PruneMatcher(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("FlagStatic"))
		output.erase("FlagStatic");

	if(src.get_child_optional("kNN.NeighbourhoodCardinality"))
		output.get_child("kNN").erase("NeighbourhoodCardinality");

	return output;
}	//ptree PruneMatcher(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceFeatureTrackerC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto c01=[](double val){return val>=0 && val<=1;};

	ParameterObjectT parameters;

	parameters.minLength=ReadParameter(src, "Main.MinTrackLength", geq0, "is not >=0", optional<double>(parameters.minLength));
	parameters.minNeighbourDistance=ReadParameter(src, "Main.MinNeighbourDistance", geq0, "is not >=0", optional<double>(parameters.minNeighbourDistance));

	parameters.flagStaticDescriptor=src.get<bool>("Association.FlagStaticDescriptor", parameters.flagStaticDescriptor);
	parameters.maxMahalanobisDistance=ReadParameter(src, "Association.MaxMahalanobisDistance", gt0, "is not >0", optional<double>(parameters.maxMahalanobisDistance));
	parameters.nFeaturesPerPage=ReadParameter(src, "Association.NoFeaturesPerPage", gt0, "is not >=0", optional<double>(parameters.nFeaturesPerPage));

	parameters.gracePeriod=ReadParameter(src, "Termination.GracePeriod", geq0, "is not >=0", optional<double>(parameters.gracePeriod));
	parameters.maxConsecutiveMiss=ReadParameter(src, "Termination.MaxConsecutiveMiss", geq0, "is not >=0", optional<double>(parameters.maxConsecutiveMiss));
	parameters.maxMissRatio=ReadParameter(src, "Termination.MaxMissRatio", c01, "is not in [0,1]", optional<double>(parameters.maxMissRatio));

	if(src.get_child_optional("Association.FeatureMatching"))
		parameters.matcherParameters=InterfaceFeatureMatcherC::MakeParameterObject(PruneMatcher(src.get_child("FeatureMatching")));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceFeatureTrackerC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Association","");
		tree.put("Association.FeatureMatching","");
		tree.put("Termination","");

		tree.put("Main.MinTrackLength", src.minLength);

		tree.put("Association.FlagStaticDescriptor", src.flagStaticDescriptor);
	}	//if(level>0)

	if(level>1)
	{
		tree.put("Main.MinNeighbourDistance", src.minNeighbourDistance);

		tree.put("Association.MaxMahalonobisDistance", src.maxMahalanobisDistance);

		tree.put("Termination.GracePeriod", src.gracePeriod);
		tree.put("Termination.MaxConsecutiveMiss", src.maxConsecutiveMiss);
		tree.put("Termination.MaxMissRatio", src.maxMissRatio);
	}	//if(level>1)

	if(level>2)
		tree.put("Association.NoFeaturesPerPage", src.nFeaturesPerPage);

	ptree matcherTree=InterfaceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level);
	tree.put_child("Association.Matcher", PruneMatcher(matcherTree));

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

