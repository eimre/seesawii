/**
 * @file InterfaceSparseUnitSphereReconstruction.ipp Implementation details for \c InterfaceSparseUnitSphereReconstructionC
 * @author Evren Imre
 * @date 26 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_SPARSE_UNIT_SPHERE_RECONSTRUCTION_IPP_9910902
#define INTERFACE_SPARSE_UNIT_SPHERE_RECONSTRUCTION_IPP_9910902

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "InterfaceMultisourceFeatureMatcher.h"
#include "InterfaceMultiviewPanoramaBuilder.h"
#include "../GeometryEstimationPipeline/SparseUnitSphereReconstructionPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::greater;
using std::bind;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceMultisourceFeatureMatcherC;
using SeeSawN::ApplicationN::InterfaceMultiviewPanoramaBuilderC;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineParametersC;

/**
 * @brief Interface for the sparse unit sphere reconstruction pipeline
 */
class InterfaceSparseUnitSphereReconstructionC
{
	private:

		/** @name Implementation details*/ //@{
		typedef SparseUnitSphereReconstructionPipelineParametersC ParameterObjectT;	///< Parameter object type
		static ptree PruneMultisourceFeatureMatcher(const ptree& src);	///< Removes the redundant elements from a multisource feature matcher property tree
		static ptree PrunePanoramaBuilder(const ptree& src);	///< Removes the redundant elements from a multiview panorama builder property tree
		//@}
	public:

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceSparseUnitSphereReconstructionC

}	//ApplicationN
}	//SeeSawN



#endif /* INTERFACESPARSEUNITSPHERERECONSTRUCTION_IPP_ */
