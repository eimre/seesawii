/**
 * @file InterfaceMultiviewTriangulation.cpp
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceMultiviewTriangulation.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceMultiviewTriangulationC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto l01=[](double val){return val>0 && val<=1;};

	ParameterObjectT parameters;

	parameters.flagFastTriangulation=src.get<bool>("Main.FlagFastTriangulation", parameters.flagFastTriangulation);
	parameters.inlierTh=ReadParameter(src, "Main.MaxError", gt0, "is not >0", optional<double>(parameters.inlierTh));
	parameters.maxCovarianceTrace=ReadParameter(src, "Main.MaxTrace", gt0, "is not >0", optional<double>(parameters.maxCovarianceTrace));
	parameters.maxDistanceRank=ReadParameter(src, "Main.MaxRank", l01, "is not in (0,1]", optional<double>(parameters.maxDistanceRank));
	parameters.flagCheirality=src.get<bool>("Main.FlagCheirality", parameters.flagCheirality);
	parameters.binSize=ReadParameter(src, "Main.BinSize", geq0, "is not >=0", optional<double>(parameters.binSize));

	parameters.noiseVariance=ReadParameter(src, "Covariance.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));

	if(src.get_child_optional("Covariance.SUT"))
		parameters.sutParameters=InterfaceScaledUnscentedTransformationC::MakeParameterObject(src.get_child("Covariance.SUT"));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceMultiviewTriangulationC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Main.MaxError", src.inlierTh);

		tree.put("Covariance","");
		tree.put("Covariance.SUT","");
	}	//if(level>0)

	if(level>1)
	{
		tree.put("Main.MaxTrace", src.maxCovarianceTrace);
		tree.put("Main.MaxRank", src.maxDistanceRank);
		tree.put("Main.FlagFastTriangulation", src.flagFastTriangulation);
		tree.put("Main.FlagCheirality", src.flagCheirality);
		tree.put("Main.BinSize", src.binSize);

		tree.put("Covariance.NoiseVariance", src.noiseVariance);
	}

	ptree treeSUT=InterfaceScaledUnscentedTransformationC::MakeParameterTree(src.sutParameters, level);
	tree.put_child("Covariance.SUT", treeSUT);

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)


}	//ApplicationN
}	//SeeSawN

