/**
 * @file InterfaceLinearAutocalibrationPipeline.ipp Implementation details of \c InterfaceLinearAutocalibrationPipelineC
 * @author Evren Imre
 * @date 3 Mar 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_LINEAR_AUTOCALIBRATION_PIPELINE_IPP_1588142
#define INTERFACE_LINEAR_AUTOCALIBRATION_PIPELINE_IPP_1588142

#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "InterfaceRANSAC.h"
#include "../GeometryEstimationPipeline/LinearAutocalibrationPipeline.h"
#include "../Geometry/DualAbsoluteQuadricSolver.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::lexical_cast;
using std::bind;
using std::greater;
using std::greater_equal;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceRANSACC;
using SeeSawN::GeometryN::LinearAutocalibrationPipelineParametersC;
using SeeSawN::GeometryN::DualAbsoluteQuadricSolverC;

/**
 * @brief Helper class for initialising a linear autocalibration pipeline parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceLinearAutocalibrationPipelineC
{
	private:

		static ptree PruneRANSAC(const ptree& src);	///< Removes the redundant RANSAC parameters

	public:

		typedef LinearAutocalibrationPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceLinearAutocalibrationPipelineC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_LINEAR_AUTOCALIBRATION_PIPELINE_IPP_1588142 */
