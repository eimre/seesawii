/**
 * @file InterfaceScaledUnscentedTransformation.h Public interface for InterfaceScaledUnscentedTransformationC
 * @author Evren Imre
 * @date 17 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_SCALED_UNSCENTED_TRANSFORMATION_H_6099012
#define INTERFACE_SCALED_UNSCENTED_TRANSFORMATION_H_6099012

#include "InterfaceScaledUnscentedTransformation.ipp"
namespace SeeSawN
{
namespace ApplicationN
{

class InterfaceScaledUnscentedTransformationC;	///< Helper class for initialising a SUT parameter object from a property tree

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_SCALED_UNSCENTED_TRANSFORMATION_H_6099012 */
