/**
 * @file InterfaceSfMPipeline.cpp Implementation of the application interface for the SfM pipeline
 * @author Evren Imre
 * @date 19 Jan 2017
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceSfMPipeline.h"


namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant elements from a vision graph pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSfMPipelineC::PruneVisionGraphPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Graph.FlagConnected"))
		output.get_child("Graph").erase("FlagConnected");

	if(src.get_child_optional("GeometryEstimationPipeline.Main.FlagCovariance"))
		output.get_child("GeometryEstimationPipeline.Main").erase("FlagCovariance");

	if(src.get_child_optional("GeometryEstimationPipeline.CovarianceEstimation"))
		output.get_child("GeometryEstimationPipeline").erase("CovarianceEstimation");

	if(src.get_child_optional("GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality") )
		output.get_child("GeometryEstimationPipeline.Matching.kNN.").erase("NeighbourhoodCardinality");

	return output;
}	//ptree InterfaceSfMPipelineC::PruneVisionGraphPipeline(const ptree& src)

/**
 * @brief Removes the redundant elements from a SUT property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSfMPipelineC::PruneSUT(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("NoThreads"))
		output.erase("NoThreads");

	return output;
}	// InterfaceSfMPipelineC::PruneSUT(const ptree& src)

/**
 * @brief Removes the redundant elements from a pose graph pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSfMPipelineC::PrunePoseGraphPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
			output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Graph.FlagConnected"))
		output.get_child("Graph").erase("FlagConnected");

	if(src.get_child_optional("GeometryEstimationPipeline.Main.FlagCovariance"))
		output.get_child("GeometryEstimationPipeline.Main").erase("FlagCovariance");

	if(src.get_child_optional("GeometryEstimationPipeline.CovarianceEstimation"))
		output.get_child("GeometryEstimationPipeline").erase("CovarianceEstimation");

	return output;
}	//ptree InterfaceSfMPipelineC::PrunePoseGraphPipeline(const ptree& src)

/**
 * @brief Removes the redundant elements from a intrinsic calibration pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSfMPipelineC::PruneIntrinsicCalibrationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("RANSAC.Main.NoThreads"))
		output.get_child("RANSAC.Main").erase("NoThreads");

	if(src.get_child_optional("RANSAC.Main.FlagHistory"))
		output.get_child("RANSAC.Main").erase("FlagHistory");

	return output;
}	//ptree InterfaceSfMPipelineC::PruneIntrinsicCalibrationPipeline(const ptree& src)

/**
 * @brief Removes the redundant elements from a sparse 3D reconstruction pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSfMPipelineC::PruneSparse3DReconstructionPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.FlagCompact"))
		output.get_child("Main").erase("FlagCompact");

	if(src.get_child_optional("Main.FlagVerbose"))
		output.get_child("Main").erase("FlagVerbose");

	if(src.get_child_optional("Main.NoiseVariance"))
		output.get_child("Main").erase("NoiseVariance");

	if(src.get_child_optional("Triangulation.Main.BinSize"))
		output.get_child("Triangulation.Main").erase("BinSize");

	if(src.get_child_optional("Triangulation.Covariance.SUT.NoThreads"))
		output.get_child("Triangulation.Covariance.SUT").erase("NoThreads");

	if(src.get_child_optional("Triangulation.Covariance.SUT.NoiseVariance"))
		output.get_child("Triangulation.Covariance.SUT").erase("NoiseVariance");

	if(src.get_child_optional("Triangulation.Main.FlagFastTriangulation"))
		output.get_child("Triangulation.Main").erase("FlagFastTriangulation");

	if(src.get_child_optional("Triangulation.Main.FlagCheirality"))
		output.get_child("Triangulation.Main").erase("FlagCheirality");

	if(src.get_child_optional("Triangulation.Main.MaxError"))
		output.get_child("Triangulation.Main").erase("MaxError");

	if(src.get_child_optional("Triangulation.Main.NoiseVariance"))
		output.get_child("Triangulation.Main").erase("NoiseVariance");

	if(src.get_child_optional("Triangulation.Covariance"))
		output.get_child("Triangulation").erase("Covariance");

	if(src.get_child_optional("MultiviewMatching.Main.NoThreads"))
		output.get_child("MultiviewMatching.Main.").erase("NoThreads");

	return output;
}	//ptree InterfaceSfMPipelineC::PruneMultiviewTriangulationPipeline(const ptree& src)

/**
 * @brief Removes the redundant elements from a geometry estimation pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSfMPipelineC::PruneGeometryEstimationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("GeometryEstimation.RANSAC.Main.NoThreads"))
		output.get_child("GeometryEstimation.RANSAC.Main").erase("NoThreads");

	if(src.get_child_optional("GeometryEstimation.RANSAC.Stage1.Main.FlagHistory"))
		output.get_child("GeometryEstimation.RANSAC.Stage1.Main").erase("FlagHistory");

	if(src.get_child_optional("GeometryEstimation.RANSAC.Stage2.Main.FlagHistory"))
		output.get_child("GeometryEstimation.RANSAC.Stage2.Main").erase("FlagHistory");

	if(src.get_child_optional("GeometryEstimation.Refinement.FlagCovariance") )
		output.get_child("GeometryEstimation.Refinement").erase("FlagCovariance");

	if(src.get_child_optional("Main.NoThreads") )
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Main.FlagCovariance") )
		output.get_child("Main").erase("FlagCovariance");

	if(src.get_child_optional("Main.FlagVerbose") )
		output.get_child("Main").erase("FlagVerbose");

	if(src.get_child_optional("CovarianceEstimation") )
		output.erase("CovarianceEstimation");

	return output;
}	//ptree PruneGeometryEstimator(const ptree& src)


/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceSfMPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.flagEuclidean=src.get<bool>("Problem.FlagEuclidean", parameters.flagEuclidean);

	parameters.imageNoiseVariance=ReadParameter(src, "Problem.ImageNoiseVariance", gt0, "is not >0", optional<double>(parameters.imageNoiseVariance));

	parameters.flagUniformFocalLength=src.get<bool>("IntrinsicCalibration.FlagUniformFocalLength", parameters.flagUniformFocalLength);

	parameters.maxMedianDepth=ReadParameter(src, "HypothesisGeneration.MaxMedianDepth", gt0, "is not >0", optional<double>(parameters.maxMedianDepth));
	parameters.edgeTraversalPenalty=ReadParameter(src, "HypothesisGeneration.EdgeTraversalPenalty", geq0, "is not >=0", optional<double>(parameters.edgeTraversalPenalty));

	parameters.maxOrientationError=ReadParameter(src, "HypothesisGeneration.PoseGraphPipeline.MaxOrientationError", gt0, "is not >0", optional<double>(parameters.maxOrientationError));
	parameters.positionPrecision=ReadParameter(src, "HypothesisGeneration.PoseGraphPipeline.PositionPrecision", gt0, "is not >0", optional<double>(parameters.positionPrecision));
	parameters.poseGraphInlierThreshold=ReadParameter(src, "HypothesisGeneration.PoseGraphPipeline.PoseGraphInlierThreshold", gt0, "is not >0", optional<double>(parameters.poseGraphInlierThreshold));

	parameters.referenceTransferError=ReadParameter(src, "RobustRefinement.ReferenceTransferError", geq0, "is not >=0", optional<double>(parameters.referenceTransferError));
	parameters.minSupport=ReadParameter(src, "RobustRefinement.MinimumSupport", geq0, "is not >=0", optional<double>(parameters.minSupport));
	parameters.predictionNoise=ReadParameter(src, "RobustRefinement.PredictionNoise", geq0, "is not >=0", optional<double>(parameters.predictionNoise));

	if(src.get_child_optional("IntrinsicCalibration"))
		parameters.intrinsicCalibrationParameters=InterfaceLinearAutocalibrationPipelineC::MakeParameterObject(PruneIntrinsicCalibrationPipeline(src.get_child("IntrinsicCalibration")));


	if(src.get_child_optional("HypothesisGeneration.VisionGraphPipeline"))
		parameters.vgParameters=InterfaceVisionGraphPipelineC::MakeParameterObject(PruneVisionGraphPipeline(src.get_child("HypothesisGeneration.VisionGraphPipeline")));

	if(src.get_child_optional("HypothesisGeneration.LandmarkCovariance"))
		parameters.sutTriangulationParameters=InterfaceScaledUnscentedTransformationC::MakeParameterObject(PruneSUT(src.get_child("HypothesisGeneration.LandmarkCovariance")));

	if(src.get_child_optional("HypothesisGeneration.PoseGraphPipeline"))
		parameters.pgParameters=InterfacePoseGraphPipelineC::MakeParameterObject(PrunePoseGraphPipeline(src.get_child("HypothesisGeneration.PoseGraphPipeline")));

	if(src.get_child_optional("RobustRefinement.MultiviewTriangulation"))
		parameters.sceneReconstructionParameters=InterfaceSparse3DReconstructionPipelineC::MakeParameterObject(PruneSparse3DReconstructionPipeline(src.get_child("RobustRefinement.MultiviewTriangulation")));

	if(src.get_child_optional("RobustRefinement.Registration"))
	{
		parameters.registration32Parameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(PruneGeometryEstimationPipeline(src.get_child("RobustRefinement.Registration")));

		map<string, string> ransacExtra=InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(src.get_child("RobustRefinement.Registration.GeometryEstimation.RANSAC"));

		auto itE=ransacExtra.end();

		auto it0=ransacExtra.find("Stage1.LORANSAC.GeneratorRatio");
		if(it0!=itE)
			parameters.loGeneratorRatio1=lexical_cast<double>(it0->second);

		auto it1=ransacExtra.find("Stage2.LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatio2=lexical_cast<double>(it1->second);
	}

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceSfMPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("Main","");
	output.put("Problem","");

	output.put("IntrinsicCalibration","");

	output.put("HypothesisGeneration","");
	output.put("HypothesisGeneration.VisionGraphPipeline","");
	output.put("HypothesisGeneration.LandmarkCovariance","");
	output.put("HypothesisGeneration.PoseGraphPipeline","");

	output.put("HypothesisGeneration.Refinement","");
	output.put("HypothesisGeneration.Refinement.Optimisation","");

	output.put("RobustRefinement","");
	output.put("RobustRefinement.MultiviewTriangulation","");
	output.put("RobustRefinement.Registration","");

	output.put("BundleAdjustment","");

	output.put_child("IntrinsicCalibration", PruneIntrinsicCalibrationPipeline(InterfaceLinearAutocalibrationPipelineC::MakeParameterTree(src.intrinsicCalibrationParameters, level)));

	output.put_child("HypothesisGeneration.VisionGraphPipeline", PruneVisionGraphPipeline(InterfaceVisionGraphPipelineC::MakeParameterTree(src.vgParameters, level)));
	output.put_child("HypothesisGeneration.LandmarkCovariance", PruneSUT(InterfaceScaledUnscentedTransformationC::MakeParameterTree(src.sutTriangulationParameters, level)));
	output.put_child("HypothesisGeneration.PoseGraphPipeline", PrunePoseGraphPipeline(InterfacePoseGraphPipelineC::MakeParameterTree(src.pgParameters, level)));

	output.put_child("HypothesisGeneration.Refinement.Optimisation", InterfacePowellDogLegC::MakeParameterTree(src.poseGraphOptimisationParameters, level));

	output.put_child("RobustRefinement.MultiviewTriangulation", PruneSparse3DReconstructionPipeline(InterfaceSparse3DReconstructionPipelineC::MakeParameterTree(src.sceneReconstructionParameters, level)));

	ptree geTree = PruneGeometryEstimationPipeline(InterfaceGeometryEstimationPipelineC::MakeParameterTree(src.registration32Parameters, level));
	InterfaceTwoStageRANSACC::InsertGeometryProblemParameters( geTree, "GeometryEstimation.RANSAC", level);
	output.put_child("RobustRefinement.Registration", geTree);

	if(level>0)
	{
		output.put("Main.NoThreads", src.nThreads);

		output.put("Problem.FlagEuclidean", src.flagEuclidean);
		output.put("Problem.ImageNoiseVariance", src.imageNoiseVariance);

		output.put("IntrinsicCalibration.FlagUniformFocalLength", src.flagUniformFocalLength);

	}	//if(level>0)

	if(level>1)
	{
		output.put("HypothesisGeneration.PoseGraphPipeline.MaxOrientationError", src.maxOrientationError);
		output.put("HypothesisGeneration.PoseGraphPipeline.PositionPrecision", src.positionPrecision);

		output.put("RobustRefinement.ReferenceTransferError", src.referenceTransferError);
		output.put("RobustRefinement.PredictionNoise", src.predictionNoise);
		output.put("RobustRefinement.MinimumSupport", src.minSupport);
	}	//if(level>1)

	if(level>2)
	{
		output.put("HypothesisGeneration.MaxMedianDepth", src.maxMedianDepth);
		output.put("HypothesisGeneration.EdgeTraversalPenalty", src.edgeTraversalPenalty);

		output.put("HypothesisGeneration.PoseGraphPipeline.PoseGraphInlierThreshold", src.poseGraphInlierThreshold);
	}	//if(level>2)

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN
