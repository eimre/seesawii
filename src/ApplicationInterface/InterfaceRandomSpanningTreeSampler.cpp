/**
 * @file InterfaceRandomSpanningTreeSampler.cpp Implementation of \c InterfaceRandomSpanningTreeSamplerC
 * @author Evren Imre
 * @date 15 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceRandomSpanningTreeSampler.h"

namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceRandomSpanningTreeSamplerC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto l01=[](double val){return val>0 && val<=1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", gt0, "is not >0", optional<double>(parameters.nThreads));
	parameters.minConfidence=ReadParameter(src, "Main.MinConfidence", l01, "is not in (0,1]", optional<double>(parameters.minConfidence));
	parameters.minIteration=src.get<unsigned int>("Main.MinIteration", parameters.minIteration);
	parameters.maxIteration=ReadParameter(src, "Main.MaxIteration", [&](unsigned int val){return val>=parameters.minIteration;}, "is not >=minIteration", optional<unsigned int>(parameters.maxIteration));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceRandomSpanningTreeSamplerC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree;

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Main.NoThreads", src.nThreads);
	}	//if(level>0)

	if(level>1)
	{
		tree.put("Main.MinConfidence", src.minConfidence);
		tree.put("Main.MinIteration", src.minIteration);
		tree.put("Main.MaxIteration", src.maxIteration);
	}	//if(level>1)

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)


}	//ApplicationN
}	//SeeSawN

