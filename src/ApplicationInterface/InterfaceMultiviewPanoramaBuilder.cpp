/**
 * @file InterfaceMultiviewPanoramaBuilder.cpp Implementation of \c InterfaceMultiviewPanoramaBuilder
 * @author Evren Imre
 * @date 5 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include "InterfaceMultiviewPanoramaBuilder.h"

namespace SeeSawN
{
namespace ApplicationN
{

/*bool flagFiltering;	///< If \c true , the measurements are filtered wrt symmetric transfer error, reprojection error and cardinality
	unsigned int minObservationCount;	///< Minimum number of observations for a scene point
	double noiseVariance;	///< Noise variance on the image coordinates >0
	double pReject;	///< Probability of rejecting an inlier. [0,1]

	double maxAngularDeviation;	///< Maximum angular deviation for an inlier point, in radians*/

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceMultiviewPanoramaBuilderC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto c01=[](double val){return val>=0 && val<=1;};
	auto r02pi=[](double val){return val>=0 && val<2*pi<double>();};

	ParameterObjectT parameters;

	parameters.flagFiltering=src.get<bool>("Main.FlagFiltering", parameters.flagFiltering);
	parameters.minObservationCount=src.get<unsigned int>("Main.MinObservationCount", parameters.minObservationCount);
	parameters.noiseVariance=ReadParameter(src, "Main.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));
	parameters.pReject=ReadParameter(src, "Main.InlierRejectionProbability", c01, "is not in [0,1]", optional<double>(parameters.pReject));
	parameters.maxAngularDeviation=ReadParameter(src, "Main.MaxAngularDeviation", r02pi, "is not in [0,2pi)", optional<double>(parameters.maxAngularDeviation));
	parameters.binSize=ReadParameter(src, "Main.BinSize", geq0, "is not >0", optional<double>(parameters.binSize));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceMultiviewPanoramaBuilderC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main","");

		tree.put("Main.NoiseVariance", src.noiseVariance);
		tree.put("Main.MaxAngularDeviation", src.maxAngularDeviation);
	}	//if(level>0)

	if(level>1)
	{
		tree.put("Main.FlagFiltering", src.flagFiltering);
		tree.put("Main.MinObservationCount", src.minObservationCount);
		tree.put("Main.BinSize", src.binSize);
	}	//if(level>1)

	if(level>2)
		tree.put("Main.InlierRejectionProbability", src.pReject);

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//namespace ApplicationN
}	//namespace SeeSawN


