/**
 * @file InterfaceMultisourceFeatureMatcher.cpp Implementation of InterfaceMultisourceFeatureMatcherC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceMultisourceFeatureMatcher.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant matcher parameters
 * @param[in] src  Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceMultisourceFeatureMatcherC::PruneMatcher(const ptree& src)
{
	ptree output=src;

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneMatcher(ptree& tree)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @remarks Assumption: Unit-norm feature vectors
 * @remarks \c MultisourceFeatureMatcherParametersC::flagConsistency is application-dependent, and is not set here
 * @return A parameter object
 */
auto InterfaceMultisourceFeatureMatcherC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT parameters;

	auto geq1=bind(greater_equal<double>(),_1,1);
	auto geq2=bind(greater_equal<double>(),_1,2);

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.minCardinality=ReadParameter(src, "Main.MinClusterSize", geq2, "is not >=2", optional<double>(parameters.minCardinality));
	parameters.flagSequentialOperation=src.get<bool>("Main.FlagSequentialOperation", false);

	parameters.matcherParameters=InterfaceFeatureMatcherC::MakeParameterObject(  PruneMatcher(src.get_child("Matcher")) );

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 * @remarks Assumption: Unit-norm feature vectors
 */
ptree InterfaceMultisourceFeatureMatcherC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
		tree.put("Main.NoThreads", src.nThreads);

	if(level>1)
	{
		tree.put("Main.MinClusterSize", src.minCardinality);
		tree.put("Main.FlagSequentialOperation", src.flagSequentialOperation);
	}

	ptree treeMatcher=InterfaceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level);
	tree.put_child("Matcher", PruneMatcher(treeMatcher));

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

