/**
 * @file InterfaceVisionGraphPipeline.cpp Implementation of \c InterfaceVisionGraphPipelineC
 * @author Evren Imre
 * @date 16 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceVisionGraphPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant geometry estimation pipeline parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceVisionGraphPipelineC::PruneGeometryEstimationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Main.FlagVerbose"))
		output.get_child("Main").erase("FlagVerbose");

	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceVisionGraphPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto o01=[](double value){return value>0 && value<1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", gt0, "is not >0", optional<double>(parameters.nThreads));
	parameters.minEdgeStrength=src.get<unsigned int>("Graph.MinEdgeStrength", parameters.minEdgeStrength);
	parameters.flagConnected=src.get<bool>("Graph.FlagConnected", parameters.flagConnected);

	if(src.get_child_optional("GeometryEstimationPipeline"))
		parameters.geometryEstimationPipelineParameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(PruneGeometryEstimationPipeline(src.get_child("GeometryEstimationPipeline")));

	parameters.initialEstimateNoiseVariance=ReadParameter(src, "GeometryEstimationPipeline.Main.InitialEstimateNoiseVariance", geq0, "is not >=0", optional<double>(parameters.initialEstimateNoiseVariance));
	parameters.inlierRejectionProbability= ReadParameter(src, "GeometryEstimationPipeline.Main.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.inlierRejectionProbability));

	if(src.get_child_optional("GeometryEstimationPipeline.GeometryEstimation.RANSAC"))
	{
		map<string, string> ransacExtra=InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(src.get_child("GeometryEstimationPipeline.GeometryEstimation.RANSAC"));

		auto itE=ransacExtra.end();

		auto it0=ransacExtra.find("Stage1.LORANSAC.GeneratorRatio");
		if(it0!=itE)
			parameters.loGeneratorRatio1=lexical_cast<double>(it0->second);

		auto it1=ransacExtra.find("Stage2.LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatio2=lexical_cast<double>(it1->second);
	}	//if(src.get_child_optional("GeometryEstimationPipeline.GeometryEstimation.RANSAC"))

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceVisionGraphPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("Main","");
	output.put("Graph","");

	ptree gepTree=InterfaceGeometryEstimationPipelineC::MakeParameterTree(src.geometryEstimationPipelineParameters, level);
	InterfaceTwoStageRANSACC::InsertGeometryProblemParameters(gepTree, "GeometryEstimation.RANSAC", level);
	output.put_child("GeometryEstimationPipeline", PruneGeometryEstimationPipeline(gepTree));

	if(level>0)
	{
		output.put("Main.NoThreads", src.nThreads);
		output.put("Graph.MinEdgeStrength", src.minEdgeStrength);
		output.put("GeometryEstimationPipeline.Main.InitialEstimateNoiseVariance", src.initialEstimateNoiseVariance);
	}	//if(level>0)

	if(level>1)
		output.put("Graph.FlagConnected", src.flagConnected);

	if(level>2)
		output.put("GeometryEstimationPipeline.Main.InlierRejectionProbability", src.inlierRejectionProbability);

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)


}	//ApplicationN
}	//SeeSawN

