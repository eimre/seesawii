/**
 * @file InterfaceTwoStageRANSAC.cpp Implementation of InterfaceTwoStageRANSACC
 * @author Evren Imre
 * @date 15 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceTwoStageRANSAC.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Prune any redundant parameters from Stage 1 RANSAC configuration
 * @param[in] src Property tree holding the parameter values
 * @return
 */
ptree InterfaceTwoStageRANSACC::PruneStage1(const ptree& src)
{
	ptree output(src);

	if(output.get_child_optional("MORANSAC"))
		output.erase("MORANSAC");

	if(output.get_child_optional("Main.MinConfidence"))
		output.get_child("Main").erase("MinConfidence");

	if(output.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneStage1(const ptree& src)

/**
 * @brief Prune any redundant parameters from Stage 2 RANSAC configuration
 * @param[in] src Property tree holding the parameter values
 * @return
 */
ptree InterfaceTwoStageRANSACC::PruneStage2(const ptree& src)
{
	ptree output(src);

	if(output.get_child_optional("Main.MinConfidence"))
		output.get_child("Main").erase("MinConfidence");

	if(output.get_child_optional("Main.MinIteration"))
		output.get_child("Main").erase("MinIteration");

	if(output.get_child_optional("Main.MaxIteration"))
		output.get_child("Main").erase("MaxIteration");

	if(output.get_child_optional("Main.MaxError"))
		output.get_child("Main").erase("MaxError");

	if(output.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneStage2(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceTwoStageRANSACC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto lo01=[](double val){return val>0 && val<=1;};

	ParameterObjectT parameters;

	parameters.parameters1=InterfaceRANSACC::MakeParameterObject(PruneStage1(src.get_child("Stage1")));
	parameters.parameters2=InterfaceRANSACC::MakeParameterObject(PruneStage2(src.get_child("Stage2")));

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.minConfidence=ReadParameter(src, "Main.MinConfidence", lo01, "is not in (0,1]", optional<double>(parameters.minConfidence));
	parameters.eligibilityRatio=ReadParameter(src, "Main.EligibilityRatio", lo01, "is not in (0,1]", optional<double>(parameters.eligibilityRatio));

	return parameters;
}	//ParameterObjectT MakeParameterObject(const ptree& src)

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceTwoStageRANSACC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("Main.NoThreads", src.nThreads);
		tree.put("Main.EligibilityRatio", src.eligibilityRatio);
	}	//if(level>0)

	if(level>1)
        tree.put("Main.MinConfidence", src.minConfidence);

	ptree tree1=InterfaceRANSACC::MakeParameterTree(src.parameters1, level);
	tree.put_child("Stage1",PruneStage1(tree1));

	ptree tree2=InterfaceRANSACC::MakeParameterTree(src.parameters2, level);
	tree.put_child("Stage2",PruneStage2(tree2));

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

/**
 * @brief Augments the tree with the parameters for \c RANSACGeometryEstimationProblemC
 * @param[in,out] src Property tree to be augmented
 * @param[in] rootPath Path to the root of the tree
 * @param[in] level Higher levels expose more parameters
 */
void InterfaceTwoStageRANSACC::InsertGeometryProblemParameters(ptree& src, const string& rootPath, unsigned int level)
{
	InterfaceRANSACC::InsertGeometryProblemParameters(src, rootPath+".Stage1", level);
	InterfaceRANSACC::InsertGeometryProblemParameters(src, rootPath+".Stage2", level);

	src.put_child(rootPath+".Stage1", PruneStage1(src.get_child(rootPath+".Stage1")));
	src.put_child(rootPath+".Stage2", PruneStage2(src.get_child(rootPath+".Stage2")));
}	//void InsertGeometryProblemParameters(ptree& src)

/**
 * @brief Extracts the parameters for \c RANSACGeometryEstimationProblemC
 * @param[in] src Property tree holding the parameters
 * @return A map with the extracted parameters
 */
auto InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(const ptree& src) -> ParameterMapT
{
	ParameterMapT parameters1;
	if(src.get_child_optional("Stage1"))
		parameters1=InterfaceRANSACC::ExtractGeometryProblemParameters(src.get_child("Stage1"));

	ParameterMapT parameters2;
	if(src.get_child_optional("Stage2"))
		parameters2=InterfaceRANSACC::ExtractGeometryProblemParameters(src.get_child("Stage2"));

	ParameterMapT parameters;

	for(const auto& current : parameters1)
		parameters.emplace("Stage1"+current.first, current.second);

	for(const auto& current : parameters2)
		parameters.emplace("Stage2"+current.first, current.second);

	return parameters;
}	//ParameterMapT ExtractGeometryProblemParameters(const ptree& src)

}	//ApplicationN
}	//SeeSawN

