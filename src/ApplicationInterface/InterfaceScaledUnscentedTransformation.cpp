/**
 * @file InterfaceScaledUnscentedTransformation.cpp Implementation of InterfaceScaledUnscentedTransformationC
 * @author Evren Imre
 * @date 17 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceScaledUnscentedTransformation.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceScaledUnscentedTransformationC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT parameters;

	auto geq1=bind(greater_equal<double>(),_1,1);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto lo01=[](double val){return val>0 && val<=1;};

	parameters.nThreads=ReadParameter(src, "NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.beta=ReadParameter(src, "Beta", geq0, "is not >=0", optional<double>(parameters.beta));
	parameters.kappa=src.get<double>("Kappa", parameters.kappa);

	if(src.get_optional<double>("Alpha"))
		parameters.alpha=ReadParameter(src, "Alpha", lo01, "is not in (0,1]", optional<double>(-1));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceScaledUnscentedTransformationC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree; //Options tree

	if(level>0)
	{
		tree.put("NoThreads", src.nThreads);

		if(src.alpha)
			tree.put("Alpha", *src.alpha);
	}	//if(level>0)

	if(level>1)
		tree.put("Beta", src.beta);

	if(level>2)
		tree.put("Kappa", src.kappa);

	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

