/**
 * @file InterfaceRotationRegistrationPipeline.cpp Implementation of \c InterfaceRotationRegistrationPipelineC
 * @author Evren Imre
 * @date 15 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceRotationRegistrationPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceRotationRegistrationPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto c0pi=[](double value){return value>=0 || value<=pi<double>();};

	ParameterObjectT parameters;

	parameters.inlierThreshold=ReadParameter(src, "Main.InlierThreshold", c0pi, "is not in [0,pi]", optional<double>(parameters.inlierThreshold));
	parameters.flagWeightedLS=src.get<bool>("Main.FlagWeightedLS", parameters.flagWeightedLS);

	if(src.get_child_optional("RandomSpanningTreeSampler"))
		parameters.rstsParameters=InterfaceRandomSpanningTreeSamplerC::MakeParameterObject(src.get_child("RandomSpanningTreeSampler"));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceRotationRegistrationPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree tree;

	if(level>0)
	{
		tree.put("Main","");
		tree.put("Main.InlierThreshold", src.inlierThreshold);
	}	//if(level>0)

	if(level>1)
		tree.put("Main.FlagWeightedLS", src.flagWeightedLS);

	InterfaceRandomSpanningTreeSamplerC::MakeParameterTree(src.rstsParameters, level);
	return tree;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

