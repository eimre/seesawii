/**
 * @file InterfaceRANSAC.h Public interface for InterfaceRANSACC
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_RANSAC_H_6091634
#define INTERFACE_RANSAC_H_6091634

#include "InterfaceRANSAC.ipp"
namespace SeeSawN
{
namespace ApplicationN
{

class InterfaceRANSACC;	///< Helper class for initialising a RANSAC parameter object from a property tree

}	//ApplicationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::map<std::string, std::string>;

#endif /* INTERFACE_RANSAC_H_6091634 */
