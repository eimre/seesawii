/**
 * @file InterfacePoseGraphPipeline.ipp Implementation details for the application interface for the pose graph pipeline
 * @author Evren Imre
 * @date 19 Jan 2017
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_POSE_GRAPH_PIPELINE_IPP_2298019
#define INTERFACE_POSE_GRAPH_PIPELINE_IPP_2298019

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <functional>
#include <map>
#include "InterfaceUtility.h"
#include "InterfaceGeometryEstimationPipeline.h"
#include "InterfaceTwoStageRANSAC.h"
#include "../GeometryEstimationPipeline/PoseGraphPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::optional;
using boost::property_tree::ptree;
using boost::lexical_cast;
using std::bind;
using std::greater;
using std::greater_equal;
using std::map;
using SeeSawN::GeometryN::PoseGraphPipelineParametersC;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceGeometryEstimationPipelineC;
using SeeSawN::ApplicationN::InterfaceTwoStageRANSACC;

/**
 * @brief Application interface for the pose graph pipeline
 * @ingroup IO
 * @nosubgrouping
 */
class InterfacePoseGraphPipelineC
{
	private:

		/** @name Implementation details */ //@{
		static ptree PruneGeometryEstimationPipeline(const ptree& src);	///< Removes the redundant geometry estimation pipeline parameters
		//@}

	public:

		typedef PoseGraphPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//InterfacePoseGraphPipelineC

}	//ApplicationN
}	//SeeSawN




#endif /* INTERFACE_POSE_GRAPH_PIPELINE_IPP_2298019 */
