/**
 * @file InterfaceGeometryEstimationPipeline.ipp Implementation of InterfaceGeometryEstimationPipelineC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_GEOMETRY_ESTIMATION_PIPELINE_IPP_1645165
#define INTERFACE_GEOMETRY_ESTIMATION_PIPELINE_IPP_1645165

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include "InterfaceFeatureMatcher.h"
#include "InterfaceGeometryEstimator.h"
#include "InterfaceScaledUnscentedTransformation.h"
#include "InterfaceUtility.h"
#include "../GeometryEstimationPipeline/GeometryEstimationPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::bind;
using std::greater;
using std::greater_equal;
using SeeSawN::ApplicationN::InterfaceFeatureMatcherC;
using SeeSawN::ApplicationN::InterfaceGeometryEstimatorC;
using SeeSawN::ApplicationN::InterfaceScaledUnscentedTransformationC;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::GeometryN::GeometryEstimationPipelineParametersC;

/**
 * @brief Helper class for initialising a geometry estimation pipeline parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceGeometryEstimationPipelineC
{
	private:

		static ptree PruneGeometryEstimator(const ptree& src);	///< Removes the redundant geometry estimator parameters
		static ptree PruneMatcher(const ptree& src);	///< Removes the redundant matcher parameters
		static ptree PruneSUT(const ptree& src);	///< Removes the redundant SUT parameters

	public:

		typedef GeometryEstimationPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//	class InterfaceGeometryEstimationPipelineC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_GEOMETRY_ESTIMATION_PIPELINE_IPP_1645165 */
