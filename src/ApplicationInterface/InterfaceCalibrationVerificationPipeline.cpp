/**
 * @file InterfaceCalibrationVerificationPipeline.cpp Implementation of \c InterfaceCalibrationVerificationPipelineC
 * @author Evren Imre
 * @date 3 Oct 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceCalibrationVerificationPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant geometry estimation pipeline parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceCalibrationVerificationPipelineC::PruneGeometryEstimationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Main.FlagCovariance"))
		output.get_child("Main").erase("FlagCovariance");

	if(src.get_child_optional("Main.FlagVerbose"))
		output.get_child("Main").erase("FlagVerbose");

	if(src.get_child_optional("CovarianceEstimation"))
		output.erase("CovarianceEstimation");

	return output;
}	//ptree PruneGeometryEstimator(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceCalibrationVerificationPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto gt1=bind(greater<double>(),_1,1);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto c01=[](double val){return val>=0 && val<=1;};
	auto o01=[](double val){return val>0 && val<1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.flagVerbose=src.get<bool>("Main.FlagVerbose", parameters.flagVerbose);
	parameters.seed=src.get<int>("Main.Seed", parameters.seed);

	parameters.noiseVariance=ReadParameter(src, "Problem.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));
	parameters.initialEstimateNoiseVariance=ReadParameter(src, "Problem.InitialEstimateNoiseVariance", geq0, "is not >=0", optional<double>(parameters.initialEstimateNoiseVariance));

	parameters.minInlierRatio=ReadParameter(src, "Decision.MinInlierRatio", c01, "is not in [0,1]", optional<double>(parameters.minInlierRatio));
	parameters.minSupport=ReadParameter(src, "Decision.MinSupport", geq0, "is not >=0", optional<double>(parameters.minSupport));
	parameters.pairwiseDecisionTh=ReadParameter(src, "Decision.PairwiseDecisionThreshold", gt1, "is not >1", optional<double>(parameters.pairwiseDecisionTh));
	parameters.minPairwiseRelation=ReadParameter(src, "Decision.MinPairwiseRelation", geq1, "is not >=1", optional<double>(parameters.minPairwiseRelation));
	parameters.maxPerturbed=ReadParameter(src,"Decision.MaxPerturbed", geq0, "is not >=0", optional<double>(parameters.maxPerturbed));

	if(src.get_child_optional("GeometryEstimationPipeline"))
		parameters.geometryEstimationPipelineParameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(PruneGeometryEstimationPipeline(src.get_child("GeometryEstimationPipeline")));

	parameters.inlierRejectionProbability=ReadParameter(src, "GeometryEsimationPipeline.Main.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.inlierRejectionProbability));

	if(src.get_child_optional("GeometryEstimationPipeline.GeometryEstimation.RANSAC"))
	{
		map<string, string> ransacExtra=InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(src.get_child("GeometryEstimationPipeline.GeometryEstimation.RANSAC"));

		auto itE=ransacExtra.end();

		auto it0=ransacExtra.find("Stage1.LORANSAC.GeneratorRatio");
		if(it0!=itE)
			parameters.loGeneratorRatio1=lexical_cast<double>(it0->second);

		auto it1=ransacExtra.find("Stage2.LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatio2=lexical_cast<double>(it1->second);

		auto it2=ransacExtra.find("Stage2.MORANSAC.MaxAlgebraicDistance");
		if(it2!=itE)
			parameters.moMaxAlgebraicDistance=lexical_cast<double>(it2->second);

		auto it3=ransacExtra.find("Main.EligibilityRatio");
		if(it3!=itE)
			parameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=lexical_cast<double>(it3->second);
	}	//if(src.get_child_optional("GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2"))


	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceCalibrationVerificationPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	if(level>0)
	{
		output.put("Main","");
		output.put("Main.NoThreads", src.nThreads);
		output.put("Main.Seed", src.seed);

		output.put("Problem","");
		output.put("Problem.NoiseVariance", src.noiseVariance);
		output.put("Problem.InitialEstimateNoiseVariance", src.initialEstimateNoiseVariance);

		output.put("Decision","");
		output.put("Decision.PairwiseDecisionThreshold", src.pairwiseDecisionTh);
	}	//if(level>0)

	if(level>1)
	{
		output.put("Decision.MinInlierRatio", src.minInlierRatio);
		output.put("Decision.MinSupport", src.minSupport);
		output.put("Decision.MinPairwiseRelation", src.minPairwiseRelation);
		output.put("Decision.MaxPerturbed", src.maxPerturbed);
	}	//if(level>1)

	ptree gepTree=InterfaceGeometryEstimationPipelineC::MakeParameterTree(src.geometryEstimationPipelineParameters, level);
	InterfaceTwoStageRANSACC::InsertGeometryProblemParameters(gepTree, "GeometryEstimation.RANSAC", level);

	if(level>1)
	    gepTree.put("Main.InlierRejectionProbability", src.inlierRejectionProbability);

	output.put_child("GeometryEstimationPipeline", PruneGeometryEstimationPipeline(gepTree));

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

