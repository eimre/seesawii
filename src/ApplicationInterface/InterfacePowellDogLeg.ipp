/**
 * @file InterfacePowellDogLeg.ipp Implementation of InterfacePowellDogLegC
 * @author Evren Imre
 * @date 16 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_POWELL_DOG_LEG_IPP_7001293
#define INTERFACE_POWELL_DOG_LEG_IPP_7001293

#include <boost/property_tree/ptree.hpp>
#include <map>
#include <functional>
#include <string>
#include "InterfaceUtility.h"
#include "../Optimisation/PowellDogLeg.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using std::map;
using std::string;
using std::bind;
using std::greater;
using std::greater_equal;
using std::less;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::OptimisationN::PowellDogLegParametersC;

/**
 * @brief Helper class for initialising a Powell's Dog Leg parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfacePowellDogLegC
{
	public:

		typedef PowellDogLegParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceRANSACC

}	//ApplicationN
}	//SeeSawN
#endif /* INTERFACE_POWELL_DOG_LEG_IPP_7001293 */
