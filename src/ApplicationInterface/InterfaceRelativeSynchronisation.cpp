/**
 * @file InterfaceRelativeSynchronisation.cpp Implementation of \c InterfaceRelativeSynchronisationC
 * @author Evren Imre
 * @date 5 Dec 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceRelativeSynchronisation.h"

namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant PDL parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceRelativeSynchronisationC::PrunePDL(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.FlagCovariance"))
		output.get_child("Main").erase("FlagCovariance");

	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)

/**
 * @brief Removes the redundant feature matcher parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceRelativeSynchronisationC::PruneFeatureMatcher(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)


/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceRelativeSynchronisationC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto gt0=bind(greater<double>(),_1,0);
	auto noConstraint=[](double){return true;};
	auto lo01=[](double val){return val>0 && val<=1;};


	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));

	//Problem

	if(src.get_child_optional("Problem.AlphaRange"))
	{
		vector<double> tmp=ReadParameterArray<double>(src, "Problem.AlphaRange", 2, gt0, "does not have 2 elements or nonpositive entries", string(" "));
		parameters.alphaRange=ViterbiRelativeSynchronisationProblemC::interval_type(tmp[0], tmp[1]);
	}	//if(src.get_child_optional("Problem.AlphaRange"))

	if(src.get_child_optional("Problem.TauRange"))
	{
		vector<double> tmp=ReadParameterArray<double>(src, "Problem.TauRange", 2, noConstraint, "does not have 2 elements", string(" "));
		parameters.tauRange=ViterbiRelativeSynchronisationProblemC::interval_type(tmp[0], tmp[1]);
	}	//if(src.get_child_optional("Problem.TauRange"))

	if(src.get_child_optional("Problem.AdmissibleAlpha"))
	{
		vector<double> admissibleAlpha=Tokenise<double>(src.get<string>("Problem.AdmissibleAlpha"), string(" "));

		for(auto val : admissibleAlpha)
			if(val<=0)
				throw(invalid_argument(string("InterfaceRelativeSynchronisationC::MakeParameterObject : Admissible alpha values must be positive. Value=")+lexical_cast<string>(val)));

		parameters.admissibleAlpha=admissibleAlpha;
	}	//if(src.get_child_optional("Problem.AdmissibleAlpha"))

	parameters.flagNoFrameDrop=src.get<bool>("Problem.FlagNoFrameDrop", parameters.flagNoFrameDrop);
	parameters.flagFixedCamera=src.get<bool>("Problem.FlagFixedCamera", parameters.flagFixedCamera);

	//Frame matching

	if(src.get_child_optional("FrameMatching.FeatureMatcher"))
		parameters.matcherParameters=InterfaceFeatureMatcherC::MakeParameterObject(PruneFeatureMatcher(src.get_child("FrameMatching.FeatureMatcher")));

	parameters.noiseVariance=ReadParameter(src, "FrameMatching.NoiseVariance", gt0, " is not >0", optional<double>(parameters.noiseVariance));
	parameters.minFeatureCorrespondence=ReadParameter(src, "FrameMatching.MinFeatureCorrespondence", geq0, "is not >=0", optional<double>(parameters.minFeatureCorrespondence));
	parameters.flagPropagateFeatureCorrespondences=src.get<bool>("FrameMatching.FlagPropagateFeatureCorrespondences", parameters.flagPropagateFeatureCorrespondences);

	if(src.get_child_optional("FrameMatching.Refinement"))
		parameters.pdlParameters=InterfacePowellDogLegC::MakeParameterObject(PrunePDL(src.get_child("FrameMatching.Refinement")));

	//Estimation

	parameters.minSegmentSupport=ReadParameter(src, "Estimation.MinSegmentSupport", gt0, "is not >0", optional<double>(parameters.minSegmentSupport));
	parameters.segmentInlierTh=ReadParameter(src, "Estimation.SegmentInlierThreshold", gt0, "is not >0", optional<double>(parameters.segmentInlierTh));
	parameters.tauQuantisationTh=ReadParameter(src, "Estimation.TauQuantisationThreshold", gt0, "is not >0", optional<double>(parameters.tauQuantisationTh));
	parameters.maxOverlap=ReadParameter(src, "Estimation.MaxOverlap", lo01, "is not in (0,1]", optional<double>(parameters.maxOverlap));
	parameters.minSpan=ReadParameter(src, "Estimation.MinSpan", lo01, "is not in (0,1]", optional<double>(parameters.minSpan));
	parameters.isolationTh=ReadParameter(src, "Estimation.IsolationThreshold", gt0, "is not >0", optional<double>(parameters.isolationTh));

	if(src.get_child_optional("Estimation.GuidedSearchRegion"))
	{
		vector<double> tmp=ReadParameterArray<double>(src, "Estimation.GuidedSearchRegion", 2, geq0, "is not >=0", string(" "));
		parameters.guidedSearchRegion=array<double,2>{tmp[0], tmp[1]};
	}

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceRelativeSynchronisationC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	if(level>0)
	{
		output.put("Main","");
		output.put("Main.NoThreads", src.nThreads);

		output.put("Problem","");
		output.put("Problem.FlagNoFrameDrop", src.flagNoFrameDrop);

		output.put("FrameMatching","");
		output.put("FrameMatching.NoiseVariance", src.noiseVariance);
		output.put("FrameMatching.MinFeatureCorrespondence", src.minFeatureCorrespondence);


		output.put("FrameMatching.FeatureMatcher","");
		output.put("FrameMatching.Refinement","");

		output.put("Estimation","");
		output.put("Estimation.MinSegmentSupport", src.minSegmentSupport);
		output.put("Estimation.SegmentInlierThreshold", src.segmentInlierTh);

		if(src.guidedSearchRegion)
            output.put("Estimation.GuidedSearchRegion", lexical_cast<string>((*src.guidedSearchRegion)[0])+" "+lexical_cast<string>((*src.guidedSearchRegion)[1]) );
	}	//if(level>0)

	if(level>1)
	{
		if(src.alphaRange)
			output.put("Problem.AlphaRange", lexical_cast<string>(src.alphaRange->lower())+" "+lexical_cast<string>(src.alphaRange->upper()));

		if(src.tauRange)
			output.put("Problem.TauRange", lexical_cast<string>(src.tauRange->lower())+" "+lexical_cast<string>(src.tauRange->upper()));

		if(src.admissibleAlpha)
		{
			string str;
			for(auto current : *src.admissibleAlpha)
				str+=lexical_cast<string>(current)+" ";

			output.put("Problem.AdmissibleAlpha", str);
		}	//if(src.admissibleAlpha)

		output.put("Problem.FlagFixedCamera", src.flagFixedCamera);

		output.put("FrameMatching.FlagPropagateFeatureCorrespondences", src.flagPropagateFeatureCorrespondences);

		output.put("Estimation.TauQuantisationThreshold", src.tauQuantisationTh);
		output.put("Estimation.MaxOverlap", src.maxOverlap);
		output.put("Estimation.MinSpan", src.minSpan);
		output.put("Estimation.IsolationThreshold", src.isolationTh);
	}	//if(level>1)

	if(level>2)
	{

	}	//if(level>2)

	output.put_child("FrameMatching.FeatureMatcher", PruneFeatureMatcher(InterfaceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level)));
	output.put_child("FrameMatching.Refinement", PrunePDL(InterfacePowellDogLegC::MakeParameterTree(src.pdlParameters, level)));

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

