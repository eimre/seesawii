/**
 * @file InterfaceTwoStageRANSAC.ipp Implementation of InterfaceTwoStageRANSACC
 * @author Evren Imre
 * @date 15 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_TWO_STAGE_RANSAC_IPP_4800128
#define INTERFACE_TWO_STAGE_RANSAC_IPP_4800128

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <map>
#include <string>
#include "InterfaceUtility.h"
#include "InterfaceRANSAC.h"
#include "../RANSAC/TwoStageRANSAC.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using boost::lexical_cast;
using std::map;
using std::string;
using std::greater_equal;
using std::greater;
using std::bind;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceRANSACC;
using SeeSawN::RANSACN::TwoStageRANSACParametersC;

/**
 * @brief Helper class for initialising a two-stage RANSAC parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceTwoStageRANSACC
{
	private:

		static ptree PruneStage1(const ptree& src);	///<Prune any redundant parameters from Stage 1 RANSAC configuration
		static ptree PruneStage2(const ptree& src);	///<Prune any redundant parameters from Stage 2 RANSAC configuration

	public:

		typedef TwoStageRANSACParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object

		typedef map<string,string> ParameterMapT;	///< A parameter map. [name, value]
		static void InsertGeometryProblemParameters(ptree& src, const string& rootPath, unsigned int level);	///< Augments the tree with the parameters for \c RANSACGeometryEstimationProblemC
		static ParameterMapT ExtractGeometryProblemParameters(const ptree& src);	///< Extracts the parameters for \c RANSACGeometryEstimationProblemC

};	//class InterfaceTSRANSACC;

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_TWO_STAGE_RANSAC_IPP_4800128 */
