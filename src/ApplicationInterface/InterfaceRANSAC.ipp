/**
 * @file InterfaceRANSAC.ipp Implementation of InterfaceRANSACC
 * @author Evren Imre
 * @date 12 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_RANSAC_IPP_1690213
#define INTERFACE_RANSAC_IPP_1690213

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <map>
#include <functional>
#include <string>
#include "InterfaceUtility.h"
#include "../RANSAC/RANSAC.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::map;
using std::string;
using std::bind;
using std::greater;
using std::greater_equal;
using std::less;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::RANSACN::RANSACParametersC;

/**
 * @brief Helper class for initialising a RANSAC parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceRANSACC
{

	public:

		typedef RANSACParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object

		typedef map<string,string> ParameterMapT;	///< A parameter map. [name, value]
		static void InsertGeometryProblemParameters(ptree& src, const string& rootPath, unsigned int level);	///< Augments the tree with the parameters for \c RANSACGeometryEstimationProblemC
		static ParameterMapT ExtractGeometryProblemParameters(const ptree& src);	///< Extracts the parameters for \c RANSACGeometryEstimationProblemC
};	//class InterfaceRANSACC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACERANSAC_IPP_ */
