/**
 * @file InterfacePfMPipeline.cpp Implementation details for \c InterfacePfMPipelineC
 * @author Evren Imre
 * @date 17 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_PFM_IPP_7791290
#define INTERFACE_PFM_IPP_7791290

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "InterfaceVisionGraphPipeline.h"
#include "InterfaceRotationRegistrationPipeline.h"
#include "InterfaceMultiviewPanoramaBuilder.h"
#include "InterfaceGeometryEstimator.h"
#include "InterfaceTwoStageRANSAC.h"
#include "InterfaceRandomSpanningTreeSampler.h"
#include "../GeometryEstimationPipeline/PfMPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::greater;
using std::bind;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceVisionGraphPipelineC;
using SeeSawN::ApplicationN::InterfaceRotationRegistrationPipelineC;
using SeeSawN::ApplicationN::InterfaceMultiviewPanoramaBuilderC;
using SeeSawN::ApplicationN::InterfaceGeometryEstimatorC;
using SeeSawN::ApplicationN::InterfaceRandomSpanningTreeSamplerC;
using SeeSawN::ApplicationN::TwoStageRANSACParametersC;
using SeeSawN::GeometryN::PfMPipelineParametersC;

/**
 * @brief Interface for the PfM pipeline
 * @ingroup IO
 * @nosubgrouping
 */
class InterfacePfMPipelineC
{
	private:

		/** @name Implementation details */ //@{
		static ptree PruneVisionGraphPipeline(const ptree& src);	///< Removes the redundant elements from a vision graph pipeline property tree
		static ptree PruneRandomSpanningTreeSampler(const ptree& src);	///< Removes the redundant elements from a RSTS property tree
		static ptree PruneRotationRegistrationPipeline(const ptree& src);	///< Removes the redundant elements from a rotation registration pipeline property tree
		static ptree PruneMultiviewPanoramaBuilderC(const ptree& src);	///< Removes the redundant elements from a multiview panorama builder property tree
		static ptree PruneGeometryEstimator(const ptree& src);	///< Removes the redundant elements from a geometry estimator property tree
		//@}

	public:

		typedef PfMPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfacePfMPipelineC

}	//ApplicationN
}	//SeeSawM



#endif /* INTERFACE_PFM_IPP_7791290 */
