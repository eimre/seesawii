/**
 * @file InterfaceRandomSpanningTreeSampler.h Public interface for \c InterfaceRandomSpanningTreeSamplerC
 * @author Evren Imre
 * @date 15 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_RANDOM_SPANNING_TREE_SAMPLER_H_0016982
#define INTERFACE_RANDOM_SPANNING_TREE_SAMPLER_H_0016982

#include "InterfaceRandomSpanningTreeSampler.ipp"

namespace SeeSawN
{
namespace ApplicationN
{

class InterfaceRandomSpanningTreeSamplerC;	///< Interface for \c RandomSpanningTreeSamplerC

}	//ApplicationN
}	//SeeSawN




#endif /* INTERFACE_RANDOM_SPANNING_TREE_SAMPLER_H_0016982 */
