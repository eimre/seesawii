/**
 * @file InterfacePfMPipeline.cpp Implementation of \c InterfacePfMPipelineC
 * @author Evren Imre
 * @date 17 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfacePfMPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant elements from a vision graph pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfacePfMPipelineC::PruneVisionGraphPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Graph.FlagConnected"))
		output.get_child("Graph").erase("FlagConnected");

	if(src.get_child_optional("GeometryEstimationPipeline.Main.FlagCovariance"))
		output.get_child("GeometryEstimationPipeline.Main").erase("FlagCovariance");

	if(src.get_child_optional("CovarianceEstimation"))
		output.erase("CovarianceEstimation");
	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)

/**
 * @brief Removes the redundant elements from a RSTS property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfacePfMPipelineC::PruneRandomSpanningTreeSampler(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PruneRandomSpanningTreeSampler(const ptree& src)

/**
 * @brief Removes the redundant elements from a rotation registration pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfacePfMPipelineC::PruneRotationRegistrationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("RandomSpanningTreeSampler.Main.NoThreads"))
		output.get_child("RandomSpanningTreeSampler.Main").erase("NoThreads");

	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)

/**
 * @brief Removes the redundant elements from a vision graph pipeline property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfacePfMPipelineC::PruneMultiviewPanoramaBuilderC(const ptree& src)
{
	ptree output(src);
	if(src.get_child_optional("Main.NoiseVariance"))
		output.get_child("Main").erase("NoiseVariance");

	if(src.get_child_optional("Main.InlierRejectionProbability"))
		output.get_child("Main").erase("InlierRejectionProbability");

	return output;
}	//ptree PruneMultiviewPanoramaBuilderC(const ptree& src)

/**
 * @brief Removes the redundant elements from a geometry estimator property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfacePfMPipelineC::PruneGeometryEstimator(const ptree& src)
{
	ptree output(src);
	if(src.get_child_optional("RANSAC.Main.NoThreads"))
		output.get_child("RANSAC.Main").erase("NoThreads");

	if(src.get_child_optional("RANSAC.Stage1.Main.FlagHistory"))
		output.get_child("RANSAC.Stage1.Main").erase("FlagHistory");

	if(src.get_child_optional("RANSAC.Stage2.Main.FlagHistory"))
		output.get_child("RANSAC.Stage2.Main").erase("FlagHistory");

	if(src.get_child_optional("Refinement.FlagCovariance") )
		output.get_child("Refinement").erase("FlagCovariance");

	return output;
}	//ptree PruneGeometryEstimator(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfacePfMPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto o01=[](double value){return value>0 && value<1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", gt0, "is not >0", optional<double>(parameters.nThreads));
	parameters.noiseVariance=ReadParameter(src, "Problem.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));
	parameters.inlierRejectionProbability= ReadParameter(src, "Problem.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.inlierRejectionProbability));

	parameters.flagInitialEstimate=src.get<bool>("Problem.FlagInitialEstimate", parameters.flagInitialEstimate);
	parameters.flagKnownF=src.get<bool>("Problem.FlagKnownF", parameters.flagKnownF);
	parameters.flagUniformFocalLength=src.get<bool>("Problem.FlagUniformFocalLength", parameters.flagUniformFocalLength);
	parameters.maxFocalLengthRatioError=ReadParameter(src, "IntrinsicCalibration.MaxFocalLengthRatioError", gt0, "is not >0", optional<double>(parameters.maxFocalLengthRatioError));

	if(src.get_child_optional("VisionGraphPipeline"))
		parameters.visionGraphParameters=InterfaceVisionGraphPipelineC::MakeParameterObject(PruneVisionGraphPipeline(src.get_child("VisionGraphPipeline")));

	if(src.get_child_optional("IntrinsicCalibration.RandomSpanningTreeSampler"))
		parameters.focalLengthRegistrationPipeline=InterfaceRandomSpanningTreeSamplerC::MakeParameterObject(PruneRandomSpanningTreeSampler(src.get_child("IntrinsicCalibration.RandomSpanningTreeSampler")));

	if(src.get_child_optional("RotationRegistrationPipeline"))
		parameters.rotationRegistrationParameters=InterfaceRotationRegistrationPipelineC::MakeParameterObject(PruneRotationRegistrationPipeline(src.get_child("RotationRegistrationPipeline")));

	if(src.get_child_optional("MultiviewPanoramaBuilder"))
		parameters.panoramaBuilderParameters=InterfaceMultiviewPanoramaBuilderC::MakeParameterObject(PruneMultiviewPanoramaBuilderC(src.get_child("MultiviewPanoramaBuilder")));

	if(src.get_child_optional("EstimateRefinement"))
	{
		parameters.geometryEstimatorParameters=InterfaceGeometryEstimatorC::MakeParameterObject(PruneGeometryEstimator(src.get_child("EstimateRefinement")));

		map<string, string> ransacExtra=InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(src.get_child("EstimateRefinement.RANSAC"));

		auto itE=ransacExtra.end();

		auto it0=ransacExtra.find("Stage1.LORANSAC.GeneratorRatio");
		if(it0!=itE)
			parameters.geLOGeneratorRatio1=lexical_cast<double>(it0->second);

		auto it1=ransacExtra.find("Stage2.LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.geLOGeneratorRatio2=lexical_cast<double>(it1->second);
	}	//if(src.get_child_optional("EstimateRefinement"))

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfacePfMPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("Main","");
	output.put("Problem","");
	output.put("VisionGraphPipeline","");
	output.put("IntrinsicCalibration","");
	output.put("RotationRegistrationPipeline","");
	output.put("EstimateRefinement","");

	output.put_child("VisionGraphPipeline", PruneVisionGraphPipeline(InterfaceVisionGraphPipelineC::MakeParameterTree(src.visionGraphParameters, level)));
	output.put_child("IntrinsicCalibration.RandomSpanningTreeSampler", PruneRandomSpanningTreeSampler(InterfaceRandomSpanningTreeSamplerC::MakeParameterTree(src.focalLengthRegistrationPipeline, level)));
	output.put_child("RotationRegistrationPipeline", PruneRotationRegistrationPipeline(InterfaceRotationRegistrationPipelineC::MakeParameterTree(src.rotationRegistrationParameters, level)));
	output.put_child("MultiviewPanoramaBuilder", PruneMultiviewPanoramaBuilderC(InterfaceMultiviewPanoramaBuilderC::MakeParameterTree(src.panoramaBuilderParameters, level)));

	output.put_child("EstimateRefinement", PruneGeometryEstimator(InterfaceGeometryEstimatorC::MakeParameterTree(src.geometryEstimatorParameters, level)));
	InterfaceTwoStageRANSACC::InsertGeometryProblemParameters(output, "EstimateRefinement.RANSAC", level);

	if(level>0)
	{
		output.put("Main.NoThreads", src.nThreads);
		output.put("Problem.NoiseVariance", src.noiseVariance);
		output.put("Problem.FlagInitialEstimate", src.flagInitialEstimate);
		output.put("Problem.FlagKnownF", src.flagKnownF);
		output.put("Problem.FlagUniformFocalLength", src.flagUniformFocalLength);
		output.put("IntrinsicCalibration.MaxFocalLengthRatioError", src.maxFocalLengthRatioError);

	}	//if(level>0)

	if(level>2)
		output.put("Problem.InlierRejectionProbability", src.inlierRejectionProbability);

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawM


