/**
 * @file InterfaceMulticameraSynchronisation.cpp Implementation of \c InterfaceMulticameraSynchronisationC
 * @author Evren Imre
 * @date 11 Dec 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceMulticameraSynchronisation.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant relative synchronisation parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceMulticameraSynchronisationC::PruneRelativeSynchronisation(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Problem.FlagFixedCamera"))
		output.get_child("Problem").erase("FlagFixedCamera");

	if(src.get_child_optional("Problem.AlphaRange"))
		output.get_child("Problem").erase("AlphaRange");

	if(src.get_child_optional("Problem.TauRange"))
		output.get_child("Problem").erase("TauRange");

	if(src.get_child_optional("Problem.AdmissibleAlpha"))
		output.get_child("Problem").erase("AdmissibleAlpha");

	return output;
}	//ptree PruneRelativeSynchronisation(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceMulticameraSynchronisationC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto gt0=bind(greater<double>(),_1,0);
	auto c01=[](double val){return val>=0 && val<=1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not >=1", optional<double>(parameters.nThreads));
	parameters.flagVerbose=src.get<bool>("Main.FlagVerbose", parameters.flagVerbose);

	//Fusion of pairwise estimates
	parameters.seed=src.get<int>("Fusion.Seed", parameters.seed);
	parameters.nHypothesis=ReadParameter(src, "Fusion.NoIteration", geq0, "is not >=0", optional<double>(parameters.nHypothesis));
	parameters.measurementInlierTh=ReadParameter(src, "Fusion.MeasurementInlierThreshold", c01, "is not >=0", optional<double>(parameters.measurementInlierTh));
	parameters.alphaTolerance=ReadParameter(src, "Fusion.AlphaTolerance", gt0, "is not >0", optional<double>(parameters.alphaTolerance));
	parameters.tauTolerance=ReadParameter(src, "Fusion.TauTolerance", gt0, "is not >0", optional<double>(parameters.tauTolerance));
	parameters.referenceSequence=ReadParameter(src, "Fusion.ReferenceSequence", geq0, "is not >=0", optional<double>(parameters.referenceSequence));
	parameters.referenceFrameRate=ReadParameter(src, "Fusion.ReferenceFrameRate", gt0, "is not >0", optional<double>(parameters.referenceFrameRate));

	if(src.get_child_optional("RelativeSynchronisation"))
	{
		parameters.maxAttitudeDifference=ReadParameter(src.get_child("RelativeSynchronisation"), "Problem.MaxAttitudeDifference", gt0, "is not >0", parameters.maxAttitudeDifference);

		parameters.relativeSynchronisationParameters=InterfaceRelativeSynchronisationC::MakeParameterObject(PruneRelativeSynchronisation(src.get_child("RelativeSynchronisation")));
	}	//if(src.get_child_optional("RelativeSynchronisation"))

	return parameters;
}	//ParameterObjectT MakeParameterObject(const ptree& src)

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceMulticameraSynchronisationC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	if(level>0)
	{
		output.put("Main","");
		output.put("Main.NoThreads", src.nThreads);

		output.put("Fusion","");
		output.put("Fusion.Seed", src.seed);
		output.put("Fusion.NoIteration", src.nHypothesis);
		output.put("Fusion.ReferenceSequence", src.referenceSequence);
		output.put("Fusion.ReferenceFrameRate", src.referenceFrameRate);

		output.put("RelativeSynchronisation","");
	}	//if(level>0)

	if(level>1)
	{
		output.put("Main.FlagVerbose", src.flagVerbose);

		output.put("Fusion.MeasurementInlierThreshold", src.measurementInlierTh);
		output.put("Fusion.AlphaTolerance", src.alphaTolerance);
		output.put("Fusion.TauTolerance", src.tauTolerance);
	}	//if(level>1)

	output.put_child("RelativeSynchronisation", PruneRelativeSynchronisation(InterfaceRelativeSynchronisationC::MakeParameterTree(src.relativeSynchronisationParameters, level)));

	if(src.maxAttitudeDifference)
		output.put("RelativeSynchronisation.Problem.MaxAttitudeDifference", src.maxAttitudeDifference);

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

