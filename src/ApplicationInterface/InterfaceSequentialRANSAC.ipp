/**
 * @file InterfaceSequentialRANSAC.ipp Implementation details for InterfaceSequentialRANSACC
 * @author Evren Imre
 * @date 18 Sep 2015
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_SEQUENTIAL_RANSAC_IPP_3099123
#define INTERFACE_SEQUENTIAL_RANSAC_IPP_3099123

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include <string>
#include "InterfaceUtility.h"
#include "InterfaceRANSAC.h"
#include "../RANSAC/SequentialRANSAC.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::string;
using std::bind;
using std::greater;
using std::greater_equal;
using std::less;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceRANSACC;
using SeeSawN::RANSACN::SequentialRANSACParametersC;

/**
 * @brief Helper class for initialising a Sequential RANSAC parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceSequentialRANSACC
{
	private:

		static ptree PruneRANSAC(const ptree& src);	///< Prunes the redundant RANSAC parameters

	public:

		typedef SequentialRANSACParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceRANSACC



}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_SEQUENTIAL_RANSAC_IPP_3099123 */
