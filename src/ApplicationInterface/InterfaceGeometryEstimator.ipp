/**
 * @file InterfaceGeometryEstimator.ipp Implementation of InterfaceGeometryEstimatorC
 * @author Evren Imre
 * @date 18 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_GEOMETRY_ESTIMATOR_IPP_9512091
#define INTERFACE_GEOMETRY_ESTIMATOR_IPP_9512091

#include <boost/property_tree/ptree.hpp>
#include "InterfacePowellDogLeg.h"
#include "InterfaceTwoStageRANSAC.h"
#include "../GeometryEstimationPipeline/GeometryEstimator.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using SeeSawN::ApplicationN::InterfaceRANSACC;
using SeeSawN::ApplicationN::InterfacePowellDogLegC;
using SeeSawN::GeometryN::GeometryEstimatorParametersC;

/**
 * @brief Helper class for initialising a geometry estimator parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceGeometryEstimatorC
{
	private:

		static ptree PruneRefinement(const ptree& src);	///< Removes the redundant refinement parameters

	public:

		typedef GeometryEstimatorParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//	class InterfaceFeatureMatcherC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_GEOMETRY_ESTIMATOR_IPP_9512091 */
