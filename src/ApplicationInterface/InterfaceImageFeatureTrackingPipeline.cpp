/**
 * @file InterfaceImageFeatureTrackingPipeline.cpp Implementation of InterfaceImageFeatureTrackingPipelineC
 * @author Evren Imre
 * @date 19 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceImageFeatureTrackingPipeline.h"
namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Prunes the redundant track filtering parameters
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceImageFeatureTrackingPipelineC::PruneTrackFiltering(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("StaticCamera.SpatialConsistency"))
		output.get_child("StaticCamera").erase("SpatialConsistency");

	if(src.get_child_optional("StaticCamera.kNN.NeighbourhoodCardinality"))
		output.get_child("StaticCamera.kNN").erase("NeighbourhoodCardinality");

	if(src.get_child_optional("DynamicCamera.Main.FlagVerbose"))
		output.get_child("DynamicCamera.Main").erase("FlagVerbose");

	if(src.get_child_optional("DynamicCamera.Main.FlagCovariance"))
		output.get_child("DynamicCamera.Main").erase("FlagCovariance");

	if(src.get_child_optional("DynamicCamera.Matching.kNN.NeighbourhoodCardinality"))
		output.get_child("DynamicCamera.Matching.kNN").erase("NeighbourhoodCardinality");

	if(src.get_child_optional("DynamicCamera.GeometryEstimation.RANSAC.Stage2.MORANSAC"))
		output.erase("DynamicCamera.GeometryEstimation.RANSAC.Stage2.MORANSAC");

	return output;
}	//ptree PruneTrackFiltering(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceImageFeatureTrackingPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq1=bind(greater_equal<double>(),_1,1);
	auto c01=[](double val){return val>=0 && val<=1;};
	auto o01=[](double val){return val>0 && val<1;};

	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", geq1, "is not>=1", optional<double>(parameters.nThreads));
	parameters.noiseVariance=ReadParameter(src, "Main.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));

	ptree tmp;
	if(src.get_child_optional("TrackFiltering"))
		tmp=PruneTrackFiltering(src.get_child("TrackFiltering"));

	parameters.flagStaticCamera=src.get<bool>("TrackFiltering.Main.FlagStaticCamera", parameters.flagStaticCamera);
	parameters.flagTrackDynamic=src.get<bool>("TrackFiltering.Main.FlagTrackDynamicFeatures", parameters.flagTrackDynamic);
	parameters.flagTrackStatic=src.get<bool>("TrackFiltering.Main.FlagTrackStaticFeatures", parameters.flagTrackStatic);;

	parameters.pRejection=ReadParameter(tmp, "Main.RejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.pRejection) );
	parameters.seed=tmp.get("Main.Seed", parameters.seed);

	if(tmp.get_child_optional("StaticCamera"))
		parameters.matcherParameters=InterfaceFeatureMatcherC::MakeParameterObject(tmp.get_child("StaticCamera"));

	parameters.flagPrediction=tmp.get<bool>("DynamicCamera.Predction.Enable", parameters.flagPrediction);
	parameters.predictionErrorNoise=ReadParameter(tmp, "DynamicCamera.Prediction.PredictionErrorNoiseVariance", gt0, "is not >0", optional<double>(parameters.predictionErrorNoise));
	parameters.predictionFailureTh=ReadParameter(tmp, "DynamicCamera.Prediction.PredictionFailureThreshold", c01, "is not in [0,1]", optional<double>(parameters.predictionFailureTh));

	if(tmp.get_child_optional("DynamicCamera.GeometryEstimation.RANSAC.Stage1"))
	{
		map<string, string> auxRANSAC=InterfaceRANSACC::ExtractGeometryProblemParameters(tmp.get_child("DynamicCamera.GeometryEstimation.RANSAC.Stage1"));

		auto it=auxRANSAC.find("LORANSAC.GeneratorRatio");
		if(it!=auxRANSAC.end())
			parameters.loGeneratorSize1=lexical_cast<double>(it->second)*Homography2DSolverC::GeneratorSize();
	}


	if(tmp.get_child_optional("DynamicCamera.GeometryEstimation.RANSAC.Stage2"))
	{
		map<string, string> auxRANSAC=InterfaceRANSACC::ExtractGeometryProblemParameters(tmp.get_child("DynamicCamera.GeometryEstimation.RANSAC.Stage2"));

		auto it=auxRANSAC.find("LORANSAC.GeneratorRatio");
		if(it!=auxRANSAC.end())
			parameters.loGeneratorSize2=lexical_cast<double>(it->second)*Homography2DSolverC::GeneratorSize();
	}

	if(tmp.get_child_optional("DynamicCamera"))
		parameters.homographyParameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(tmp.get_child("DynamicCamera"));

	if(src.get_child_optional("Tracking"))
		parameters.trackerParameters=InterfaceFeatureTrackerC::MakeParameterObject(src.get_child("Tracking"));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceImageFeatureTrackingPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	if(level>0)
	{
		output.put("Main","");
		output.put("Tracking","");
		output.put("TrackFiltering","");
		output.put("TrackFiltering.Main","");
		output.put("TrackFiltering.StaticCamera","");
		output.put("TrackFiltering.DynamicCamera","");

		output.put("Main.NoThreads", src.nThreads);
		output.put("Main.NoiseVariance", src.noiseVariance);

		output.put("TrackFiltering.Main.FlagStaticCamera", src.flagStaticCamera);
		output.put("TrackFiltering.Main.FlagTrackDynamicFeatures", src.flagTrackDynamic);
		output.put("TrackFiltering.Main.FlagTrackStaticFeatures", src.flagTrackStatic);
	}	//if(level>0)

	if(level>1)
	{
		output.put("TrackFiltering.Main.RejectionProbability", src.pRejection);
		output.put("TrackFiltering.Main.Seed", src.seed);

		output.put("TrackFiltering.DynamicCamera.Prediction.Enable", src.flagPrediction);
		output.put("TrackFiltering.DynamicCamera.Prediction.PredictionErrorNoiseVariance", src.predictionErrorNoise);
		output.put("TrackFiltering.DynamicCamera.Prediction.PredictionFailureThreshold", src.predictionFailureTh);
	}	//if(level>1)

	ptree trackFiltering;

	ptree treeTracking=InterfaceFeatureTrackerC::MakeParameterTree(src.trackerParameters, level);
	output.put_child("Tracking", treeTracking);

	ptree staticFiltering=InterfaceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level);
	trackFiltering.put_child("StaticCamera", staticFiltering);

	ptree dynamicFiltering=InterfaceGeometryEstimationPipelineC::MakeParameterTree(src.homographyParameters, level);
	trackFiltering.put_child("DynamicCamera", dynamicFiltering);

	InterfaceRANSACC::InsertGeometryProblemParameters(trackFiltering, "DynamicCamera.GeometryEstimation.RANSAC.Stage2",level);

	output.put_child("TrackFiltering", PruneTrackFiltering(trackFiltering));
	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//ApplicationN
}	//SeeSawN

