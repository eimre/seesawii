/**
 * @file InterfaceRandomSpanningTreeSampler.ipp Implementation details for \c InterfaceRandomSpanningTreeSamplerC
 * @author Evren Imre
 * @date 15 May 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef INTERFACE_RANDOM_SPANNING_TREE_SAMPLER_IPP_7709127
#define INTERFACE_RANDOM_SPANNING_TREE_SAMPLER_IPP_7709127

#include <boost/property_tree/ptree.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "../RandomSpanningTreeSampler/RandomSpanningTreeSampler.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using std::bind;
using std::greater;
using SeeSawN::RandomSpanningTreeSamplerN::RandomSpanningTreeSamplerParametersC;
using SeeSawN::ApplicationN::ReadParameter;

/**
 * @brief Interface for \c RandomSpanningTreeSamplerC
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceRandomSpanningTreeSamplerC
{
	public:

		typedef RandomSpanningTreeSamplerParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object

};	//class InterfaceRandomSpanningTreeSamplerC

}	//ApplicationN
}	//SeeSawN



#endif /* INTERFACE_RANDOM_SPANNING_TREE_SAMPLER_IPP_7709127 */
