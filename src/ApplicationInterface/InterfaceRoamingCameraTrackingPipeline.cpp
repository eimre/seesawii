/**
 * @file InterfaceRoamingCameraTrackingPipeline.cpp Implementation of \c InterfaceRoamingCameraTrackingPipelineC
 * @author Evren Imre
 * @date 28 Jan 2017
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceRoamingCameraTrackingPipeline.h"

namespace SeeSawN
{
namespace ApplicationN
{


/**
 * @brief Removes the redundant geometry estimation pipeline parameters
 * @param[in] src Source tree
 * @return Pruned tree
 */
ptree InterfaceRoamingCameraTrackingPipelineC::PruneGeometryEstimationPipeline(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Main.FlagVerbose"))
		output.get_child("Main").erase("FlagVerbose");

	if(src.get_child_optional("Main.FlagCovariance"))
		output.get_child("Main").erase("FlagCovariance");

	if(src.get_child_optional("CovarianceEstimation"))
		output.erase("CovarianceEstimation");

	if(src.get_child_optional("GeometryEstimation.RANSAC.Stage2.MORANSAC.MaxAlgebraicDistance"))
		output.get_child("GeometryEstimation.RANSAC.Stage2.MORANSAC").erase("MaxAlgebraicDistance");
	return output;
}	//ptree PruneGeometryEstimationPipeline(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceRoamingCameraTrackingPipelineC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);
	auto o01=[](double value){return value>0 && value<1;};
	auto c01=[](double value){return value>=0 && value<=1;};


	ParameterObjectT parameters;

	parameters.nThreads=ReadParameter(src, "Main.NoThreads", gt0, "is not >0", optional<double>(parameters.nThreads));
	parameters.imageNoiseVariance=ReadParameter(src, "Problem.ImageNoiseVariance", gt0, "is not >0", optional<double>(parameters.imageNoiseVariance));
	parameters.worldNoiseVariance=ReadParameter(src, "Problem.WorldNoiseVariance", geq0, "is not >=0", optional<double>(parameters.worldNoiseVariance));
	parameters.predictionNoise=ReadParameter(src, "Problem.PredictionNoiseVariance", gt0, "is not >0", optional<double>(parameters.predictionNoise));
	parameters.inlierRejectionProbability=ReadParameter(src, "Problem.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.inlierRejectionProbability));

	parameters.imageDiagonal=ReadParameter(src, "Problem.ImageDiagonal", gt0, "is not >0", optional<double>(parameters.imageDiagonal));

	parameters.flagFixedFocalLength=src.get<bool>("Problem.FlagFixedFocalLength", parameters.flagFixedFocalLength);

	if(src.get_optional<double>("Problem.FocalLength"))
		parameters.focalLength=ReadParameter(src, "Problem.FocalLength", gt0, "is not >0", optional<double>(tan(pi<double>()/6)*parameters.imageDiagonal));	//The default assumes a FoV of 60 degrees, but would not normally be used

	parameters.minFocalLength=ReadParameter(src, "Problem.MinFocalLength", gt0, "is not >0", optional<double>(parameters.minFocalLength));
	parameters.maxFocalLength=ReadParameter(src, "Problem.MaxFocalLength", bind(greater_equal<double>(),_1,parameters.minFocalLength), "is not >0", optional<double>(parameters.maxFocalLength));

	parameters.minSupport=ReadParameter(src, "Tracker.MinSupport", geq0, "is not >=0", optional<double>(parameters.minSupport));
	parameters.supportLossTrackingFailure=ReadParameter(src, "Tracker.MinSupportLossTrackingFailure", c01, "is not in [0,1]", optional<double>(parameters.supportLossTrackingFailure));
	parameters.supportLossModeSwitch=ReadParameter(src, "Tracker.MinSupportLossModeSwitch", c01, "is not in [0,1]", optional<double>(parameters.supportLossModeSwitch));
	parameters.focalLengthSensitivity=ReadParameter(src, "Tracker.FocalLengthSensitivity", gt0, "is not >0", optional<double>(parameters.focalLengthSensitivity));

	parameters.zoomMeasurementWeight=ReadParameter(src, "Tracker.ZoomMeasurementWeight", c01, "is not in [0,1]", optional<double>(parameters.zoomMeasurementWeight));
	parameters.rotationMeasurementWeight=ReadParameter(src, "Tracker.RotationMeasurementWeight", c01, "is not in [0,1]", optional<double>(parameters.rotationMeasurementWeight));
	parameters.displacementMeasurementWeight=ReadParameter(src, "Tracker.RotationMeasurementWeight", c01, "is not in [0,1]", optional<double>(parameters.displacementMeasurementWeight));

	parameters.flagApproximateNeighbourhoods=src.get<bool>("Tracker.FlagApproximateNeighbourhoods", parameters.flagApproximateNeighbourhoods);

	if(src.get_child_optional("GeometryEstimationPipeline.GeometryEstimation.RANSAC"))
	{
		map<string, string> ransacExtra=InterfaceTwoStageRANSACC::ExtractGeometryProblemParameters(src.get_child("GeometryEstimationPipeline.GeometryEstimation.RANSAC"));

		auto itE=ransacExtra.end();

		auto it0=ransacExtra.find("Stage1.LORANSAC.GeneratorRatio");
		if(it0!=itE)
			parameters.loGeneratorRatio1=lexical_cast<double>(it0->second);

		auto it1=ransacExtra.find("Stage2.LORANSAC.GeneratorRatio");
		if(it1!=itE)
			parameters.loGeneratorRatio1=lexical_cast<double>(it1->second);
	}	//if(parameters.get_child_optional("GeometryEstimationPipeline.Pipeline.GeometryEstimation.RANSAC"))

	parameters.pipelineParameters=InterfaceGeometryEstimationPipelineC::MakeParameterObject(PruneGeometryEstimationPipeline(src.get_child("GeometryEstimationPipeline")));

	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceRoamingCameraTrackingPipelineC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("Main","");
	output.put("Problem","");
	output.put("Tracker","");
	output.put("GeometryEstimationPipeline","");

	if(level>0)
	{
		output.put("Main.NoThreads", src.nThreads);

		output.put("Problem.ImageNoiseVariance", src.imageNoiseVariance);
		output.put("Problem.WorldNoiseVariance", src.worldNoiseVariance);
		output.put("Problem.PredictionNoiseVariance", src.predictionNoise);
		output.put("Problem.ImageDiagonal", src.imageDiagonal);
		output.put("Problem.FlagFixedFocalLength", src.flagFixedFocalLength);
		output.put("Problem.FocalLength", tan( pi<double>()/6 )*src.imageDiagonal );	//Dummy value
	}	//if(level>0)

	if(level>1)
	{
		output.put("Problem.MinFocalLength", src.minFocalLength);
		output.put("Problem.MaxFocalLength", src.maxFocalLength);

		output.put("Tracker.MinSupport", src.minSupport);
		output.put("Tracker.MinSupportLossTrackingFailure", src.supportLossTrackingFailure);
		output.put("Tracker.MinSupportLossModeSwitch", src.supportLossModeSwitch);
		output.put("Tracker.FocalLengthSensitivity", src.focalLengthSensitivity);
		output.put("Tracker.ZoomMeasurementWeight", src.zoomMeasurementWeight);
		output.put("Tracker.RotationMeasurementWeight", src.rotationMeasurementWeight);
		output.put("Tracker.DisplacementMeasurementWeight", src.displacementMeasurementWeight);
	}	//if(level>1)

	if(level>2)
	{
		output.put("Problem.InlierRejectionProbability", src.inlierRejectionProbability);
		output.put("Tracker.FlagApproximateNeighbourhoods", src.flagApproximateNeighbourhoods);
	}	//if(level>2)

	ptree registrationTree=InterfaceGeometryEstimationPipelineC::MakeParameterTree(src.pipelineParameters, level);
	InterfaceTwoStageRANSACC::InsertGeometryProblemParameters(registrationTree, "GeometryEstimation.RANSAC", level);
	output.put_child("GeometryEstimationPipeline", PruneGeometryEstimationPipeline(registrationTree));

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)
}	//ApplicationN
}	//SeeSawN
