/**
 * @file InterfaceFeatureTracker.ipp Implementation of InterfaceFeatureTrackerC
 * @author Evren Imre
 * @date 19 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_FEATURE_TRACKER_IPP_1902312
#define INTERFACE_FEATURE_TRACKER_IPP_1902312

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <functional>
#include "InterfaceUtility.h"
#include "InterfaceFeatureMatcher.h"
#include "../Tracker/FeatureTracker.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using std::bind;
using std::greater;
using std::greater_equal;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceFeatureMatcherC;
using SeeSawN::TrackerN::FeatureTrackerParametersC;

/**
 * @brief Helper class for initialising an image feature tracker parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceFeatureTrackerC
{
	private:

		static ptree PruneMatcher(const ptree& src);	///< Removes the redundant matcher parameters from the tree

	public:

		typedef FeatureTrackerParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceFeatureTrackerC

}	//ApplicationN
}	//SeeSawN

#endif /* INTERFACE_FEATURE_TRACKER_IPP_1902312 */
