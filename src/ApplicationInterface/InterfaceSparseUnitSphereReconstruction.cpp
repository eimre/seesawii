/**
 * @file InterfaceSparseUnitSphereReconstruction.cpp Implementation of \c InterfaceSparseUnitSphereReconstructionC
 * @author Evren Imre
 * @date 26 Jul 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "InterfaceSparseUnitSphereReconstruction.h"

namespace SeeSawN
{
namespace ApplicationN
{

/**
 * @brief Removes the redundant elements from a multisource feature matcher property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSparseUnitSphereReconstructionC::PruneMultisourceFeatureMatcher(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Matcher.kNN.NeighbourhoodCardinality"))
		output.get_child("Matcher.kNN").erase("NeighbourhoodCardinality");

	if(src.get_child_optional("Matcher.kNN.FlagConsistencyTest"))
		output.get_child("Matcher.kNN").erase("FlagConsistencyTest");

	return output;
}	//ptree PruneMultisourceFeatureMatcher(const ptree& src)

/**
 * @brief Removes the redundant elements from a multiview panorama builder property tree
 * @param[in] src Tree to be pruned
 * @return Pruned tree
 */
ptree InterfaceSparseUnitSphereReconstructionC::PrunePanoramaBuilder(const ptree& src)
{
	ptree output(src);
	if(src.get_child_optional("Main.NoiseVariance"))
		output.get_child("Main").erase("NoiseVariance");

	if(src.get_child_optional("Main.InlierRejectionProbability"))
		output.get_child("Main").erase("InlierRejectionProbability");

	return output;
}	//ptree PrunePanoramaBuilder(const ptree& src)

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceSparseUnitSphereReconstructionC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	auto gt0=bind(greater<double>(),_1,0);
	auto o01=[](double value){return value>0 && value<1;};

	ParameterObjectT parameters;

	parameters.noiseVariance=ReadParameter(src, "Main.NoiseVariance", gt0, "is not >0", optional<double>(parameters.noiseVariance));
	parameters.pRejection=ReadParameter(src, "Main.InlierRejectionProbability", o01, "is not in (0,1)", optional<double>(parameters.pRejection));
	parameters.flagCompact=src.get<bool>("Main.FlagCompact", parameters.flagCompact);
	parameters.flagVerbose=src.get<bool>("Main.FlagVerbose", parameters.flagVerbose);

	if(src.get_child_optional("FeatureMatching"))
		parameters.matcherParameters=InterfaceMultisourceFeatureMatcherC::MakeParameterObject(PruneMultisourceFeatureMatcher(src.get_child("FeatureMatching")));

	if(src.get_child_optional("PanoramaBuilder"))
		parameters.panoramaParameters=InterfaceMultiviewPanoramaBuilderC::MakeParameterObject(PrunePanoramaBuilder(src.get_child("PanoramaBuilder")));
	return parameters;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceSparseUnitSphereReconstructionC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("Main","");
	output.put("FeatureMatching","");
	output.put("PanoramaBuilder","");

	if(level>0)
	{
		output.put("Main.NoiseVariance", src.noiseVariance);
		output.put("Main.FlagCompact", src.flagCompact);
		output.put("Main.FlagVerbose", src.flagVerbose);
	}	//if(level>0)

	if(level>2)
		output.put("Main.InlierRejectionProbability", src.pRejection);

	output.put_child("FeatureMatching", PruneMultisourceFeatureMatcher(InterfaceMultisourceFeatureMatcherC::MakeParameterTree(src.matcherParameters, level)));
	output.put_child("PanoramaBuilder", PrunePanoramaBuilder(InterfaceMultiviewPanoramaBuilderC::MakeParameterTree(src.panoramaParameters, level)));

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)


}	//ApplicationN
}	//SeeSawN
