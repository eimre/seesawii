/**
 * @file InterfaceImageFeatureTrackingPipeline.ipp Implementation of InterfaceImageFeatureTrackingPipelineC
 * @author Evren Imre
 * @date 19 Sep 2014
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef INTERFACE_IMAGE_FEATURE_TRACKING_PIPELINE_IPP_3890123
#define INTERFACE_IMAGE_FEATURE_TRACKING_PIPELINE_IPP_3890123

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <functional>
#include <map>
#include <string>
#include "InterfaceUtility.h"
#include "InterfaceFeatureMatcher.h"
#include "InterfaceGeometryEstimationPipeline.h"
#include "InterfaceRANSAC.h"
#include "InterfaceFeatureTracker.h"
#include "../Tracker/ImageFeatureTrackingPipeline.h"
#include "../Geometry/Homography2DSolver.h"

namespace SeeSawN
{
namespace ApplicationN
{

using boost::property_tree::ptree;
using boost::optional;
using boost::lexical_cast;
using std::bind;
using std::greater;
using std::greater_equal;
using std::map;
using std::string;
using SeeSawN::ApplicationN::ReadParameter;
using SeeSawN::ApplicationN::InterfaceFeatureMatcherC;
using SeeSawN::ApplicationN::InterfaceRANSACC;
using SeeSawN::ApplicationN::InterfaceGeometryEstimationPipelineC;
using SeeSawN::ApplicationN::InterfaceFeatureTrackerC;
using SeeSawN::TrackerN::ImageFeatureTrackingPipelineParametersC;
using SeeSawN::GeometryN::Homography2DSolverC;

/**
 * @brief Helper class for initialising an image feature tracking pipeline parameter object from a property tree
 * @ingroup IO
 * @nosubgrouping
 */
class InterfaceImageFeatureTrackingPipelineC
{
	private:

		static ptree PruneTrackFiltering(const ptree& src);	///< Prunes the redundant track filtering parameters

	public:

		typedef ImageFeatureTrackingPipelineParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceImageFeatureTrackingPipelineC


}	//ApplicationN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template double boost::lexical_cast<double, std::string>(const std::string&);

#endif /* INTERFACE_IMAGE_FEATURE_TRACKING_PIPELINE_IPP_3890123 */
