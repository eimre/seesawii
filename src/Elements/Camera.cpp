/**
 * @file Camera.cpp Implementation of CameraC
 * @author Evren Imre
 * @date 4 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Camera.h"
namespace SeeSawN
{
namespace ElementsN
{

/********** CameraC **********/

/**
 * @brief Default constructor
 */
CameraC::CameraC()
{}

/**
 * @brief Constructor
 * @param[in] eextrinsics Extrinsics
 * @param[in] iintrinsics Intrinsics
 * @param[in] ddistortion Lens distortion
 * @param[in] fframeBox Frame box
 * @param[in] ffNumber F number
 * @param[in] ppixelSize Pixel size, in m
 * @param[in] ttag Camera tag
 */
CameraC::CameraC(const optional<ExtrinsicC>& eextrinsics, const optional<IntrinsicC>& iintrinsics, const optional<LensDistortionC>& ddistortion, const optional<FrameT>& fframeBox, const optional<double>& ffNumber, const optional<double>& ppixelSize, const string& ttag) : extrinsics(eextrinsics), intrinsics(iintrinsics), distortion(ddistortion), frameBox(fframeBox), fNumber(ffNumber), pixelSize(ppixelSize), tag(ttag)
{}

/**
 * @brief Constructor
 * @param[in] mP Camera matrix
 * @param[in] ddistortion Lens distortion
 * @param[in] fframeBox Frame box
 * @param[in] ffNumber F number
 * @param[in] ppixelSize Pixel size, in m
 * @param[in] ttag Camera tag
 */
CameraC::CameraC(const CameraMatrixT& mP, const optional<LensDistortionC>& ddistortion, const optional<FrameT>& fframeBox, const optional<double>& ffNumber, const optional<double>& ppixelSize, const string& ttag) : distortion(ddistortion), frameBox(fframeBox), fNumber(ffNumber), pixelSize(ppixelSize), tag(ttag)
{
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	IntrinsicCalibrationMatrixT mK;
	tie(mK, vC, mR)=DecomposeCamera(mP);

	SetExtrinsics(vC, mR);
	SetIntrinsics(mK);
}	//CameraC(const CameraMatrixT& mP, const optional<LensDistortionC>& ddistortion, const optional<FrameT>& fframeBox)

/**
 * @brief Returns a constant reference to \c extrinsics
 * @return A constant reference to \c extrinsics
 */
const optional<ExtrinsicC>& CameraC::Extrinsics() const
{
	return extrinsics;
}	//const optional<ExtrinsicC>& Extrinsics()

/**
 * @brief Returns a reference to \c extrinsics
 * @return A reference to \c extrinsics
 */
optional<ExtrinsicC>& CameraC::Extrinsics()
{
	return extrinsics;
}	//const optional<ExtrinsicC>& Extrinsics()

/**
 * @brief Sets \c extrinsics from a normalised camera matrix
 * @param[in] mPNormalised A normalised camera matrix
 */
void CameraC::SetExtrinsics(const CameraMatrixT& mPNormalised)
{
	RotationMatrix3DT mR=mPNormalised.topLeftCorner(3,3);
	SetExtrinsics( TranslationToPosition(mPNormalised.col(3) , mR), mR);
}	//void SetExtrinsics(const CameraMatrixT& mPNormalised)

/**
 * @brief Sets \c extrinsics
 * @param[in] position Position
 * @param[in] orientation Orientation as a rotation matrix
 */
void CameraC::SetExtrinsics(const Coordinate3DT& position, const RotationMatrix3DT& orientation)
{
	extrinsics=ExtrinsicC();
	extrinsics->position=position;
	extrinsics->orientation=QuaternionT(orientation);
}	//void SetExtrinsics(const Coordinate3DT& position, const RotationMatrixT& orientation)

/**
 * @brief Returns a constant reference to \c intrinsics
 * @return A constant reference to intrinsics
 */
const optional<IntrinsicC>& CameraC::Intrinsics() const
{
	return intrinsics;
}	//const optional<IntrinsicC>& Intrinsics() const;

/**
 * @brief Returns a reference to \c intrinsics
 * @return A reference to intrinsics
 */
optional<IntrinsicC>& CameraC::Intrinsics()
{
	return intrinsics;
}	//optional<IntrinsicC>& Intrinsics()

/**
 * @brief Sets \c intrinsics from an internal calibration matrix
 * @param[in] mK Internal calibration matrix
 */
void CameraC::SetIntrinsics(const IntrinsicCalibrationMatrixT& mK)
{
	intrinsics=IntrinsicC();
	intrinsics->focalLength=mK(0,0);
	intrinsics->aspectRatio=mK(1,1)/mK(0,0);
	intrinsics->skewness=mK(0,1);
	intrinsics->principalPoint=Coordinate2DT(mK(0,2), mK(1,2));
}	//void SetIntrinsics(const IntrinsicCalibrationMatrixT& mK)

/**
 * @brief Returns a constant reference to \c distortion
 * @return A constant reference to \c distortion
 */
const optional<LensDistortionC>& CameraC::Distortion() const
{
	return distortion;
}	//const optional<LensDistortionC>& Distortion() const

/**
 * @brief Returns a reference to \c distortion
 * @return A reference to \c distortion
 */
optional<LensDistortionC>& CameraC::Distortion()
{
	return distortion;
}	// optional<LensDistortionC>& Distortion()

/**
 * @brief Returns a constant reference to \c rameBox
 * @return A constant reference to \c frameBox
 */
const optional<FrameT>& CameraC::FrameBox() const
{
	return frameBox;
}	//const optional<FrameT>& FrameSize() const

/**
 * @brief Returns a reference to \c frameBox
 * @return A reference to \c frameBox
 */
optional<FrameT>& CameraC::FrameBox()
{
	return frameBox;
}	//optional<FrameT>& FrameSize()

/**
 * @brief Returns a constant reference to the fNumber
 * @return A constant reference to fNumber
 */
const optional<double>& CameraC::FNumber() const
{
	return fNumber;
}	//const optional<double> FNumber() const

/**
 * @brief Returns a reference to fNumber
 * @return A reference to fNumber
 */
optional<double>& CameraC::FNumber()
{
	return fNumber;
}	//optional<double> FNumber()

/**
 * @brief Returns a constant reference to pixelSize
 * @return A constant reference to pixelSize
 */
const optional<double>& CameraC::PixelSize() const
{
	return pixelSize;
}	//const optional<double> PixelSize()

/**
 * @brief Returns a reference to pixelSize
 * @return A reference to pixelSize
 */
optional<double>& CameraC::PixelSize()
{
	return pixelSize;
}	//optional<double> PixelSize()

/**
 * @brief Returns a constant reference to \c tag
 * @return A constant reference to \c tag
 */
const string& CameraC::Tag() const
{
	return tag;
}	//const string& Tag()

/**
 * @brief Returns a reference to \c tag
 * @return A reference to \c tag
 */
string& CameraC::Tag()
{
	return tag;
}	//const string& Tag()

/**
 * @brief Returns the camera matrix for the camera
 * @return Camera matrix. Invalid if \c intrinsics or \c extrinsics is invalid
 */
optional<CameraMatrixT> CameraC::MakeCameraMatrix() const
{
	optional<IntrinsicCalibrationMatrixT> mK=MakeIntrinsicCalibrationMatrix();
	optional<RotationMatrix3DT> mR=MakeRotationMatrix();

	if(mK && mR && extrinsics->position)
		return ComposeCameraMatrix(*mK, *extrinsics->position, *mR);
	else
		return optional<CameraMatrixT>();
}	//optional<CameraMatrixT> MakeCameraMatrix() const

/**
 * @brief Returns the normalised camera matrix for the camera
 * @return Camera matrix. Invalid \c extrinsics is invalid
 */
optional<CameraMatrixT> CameraC::MakeNormalisedCameraMatrix() const
{
	optional<RotationMatrix3DT> mR=MakeRotationMatrix();

	if(mR && extrinsics->position)
		return ComposeCameraMatrix(*extrinsics->position, *mR);
	else
		return optional<CameraMatrixT>();
}	//optional<CameraMatrixT> MakeCameraMatrix() const

/**
 * @brief Returns the rotation matrix corresponding to the orientation of the camera
 * @return Rotation matrix corresponding to the orientation quaternion
 */
optional<RotationMatrix3DT> CameraC::MakeRotationMatrix() const
{
	if(!extrinsics || !extrinsics->orientation)
		return optional<RotationMatrix3DT>();

	return extrinsics->orientation->matrix();
}	//optional<RotationMatrix3DT> MakeRotationMatrix() const

/**
 * @brief Returns the intrinsic calibration matrix for the camera
 * @return Intrinsic calibration matrix
 */
optional<IntrinsicCalibrationMatrixT> CameraC::MakeIntrinsicCalibrationMatrix() const
{
	if(!intrinsics || !intrinsics->focalLength || !intrinsics->principalPoint)
		return optional<IntrinsicCalibrationMatrixT>();

	IntrinsicCalibrationMatrixT mK; mK.setIdentity();

	mK(0,0)= *intrinsics->focalLength;
	mK(1,1)= mK(0,0)*intrinsics->aspectRatio;
	mK(0,1)= intrinsics->skewness;
	mK(0,2)= (*intrinsics->principalPoint)(0);
	mK(1,2)= (*intrinsics->principalPoint)(1);

	return mK;
}	//optional<IntrinsicCalibrationMatrixT> MakeIntrinsicCalibrationMatrix() const

/**
 * @brief Applies a perturbation to a camera
 * @param[in] src Camera to be perturbed
 * @param[in] deltaPose Pose perturbation
 * @param[in] deltaIntrinsics Intrinsic parameter perturbation
 * @param[in] deltaDistortion Distortion perturbation
 * @return Perturbed camera
 * @remarks Any invalid entries in src or the perturbation parameters are skipped
 */
CameraC CameraC::ApplyPerturbation(const CameraC& src, const ExtrinsicC& deltaPose, const IntrinsicC& deltaIntrinsics, const LensDistortionC& deltaDistortion)
{
	CameraC perturbed(src);

	if(perturbed.extrinsics)
	{
		if(deltaPose.position && perturbed.extrinsics->position)
			(*perturbed.extrinsics->position) += (*deltaPose.position);

		if(deltaPose.orientation && perturbed.extrinsics->orientation)
			(*perturbed.extrinsics->orientation) = (*deltaPose.orientation) * (*perturbed.extrinsics->orientation);
	}	//if(perturbed.extrinsics)

	if(perturbed.intrinsics)
	{
		if(perturbed.intrinsics->focalLength && deltaIntrinsics.focalLength)
			(*perturbed.intrinsics->focalLength) += *deltaIntrinsics.focalLength;

		if(perturbed.intrinsics->principalPoint && deltaIntrinsics.principalPoint)
			(*perturbed.intrinsics->principalPoint) += *deltaIntrinsics.principalPoint;

		perturbed.intrinsics->aspectRatio += deltaIntrinsics.aspectRatio;
		perturbed.intrinsics->skewness += deltaIntrinsics.skewness;
	}	//if(perturbed.intrinsics)

	if(perturbed.distortion)
	{
		perturbed.distortion->kappa += deltaDistortion.kappa;
		perturbed.distortion->centre += deltaDistortion.centre;
	}	//if(perturbed.distortion)

	return perturbed;
}	//CameraC ApplyPerturbation(const CameraC& src, const ExtrinsicC& deltaPose, const IntrinsicC& deltaIntrinsics, const LensDistortionC& deltaDistortion)

//TODO There should be a projective camera flag
/**
 * @brief Checks whether the camera is metric or projective
 * @return \c true if the intrinsic calibration parameters are defined
 */
bool CameraC::IsMetric() const
{
	return (!!intrinsics) && (!!intrinsics->focalLength && !!intrinsics->principalPoint);
}	//bool IsMetric() const

/********** FREE FUNCTIONS **********/

/**
 * @brief Decomposes a camera into its constituent elements
 * @param[in] mP Camera matrix to be decomposed
 * @param[in] flagNormalised If \c mP is a normalised camera, \c true
 * @return A tuple: Intrinsic calibration matrix, position vector, rotation matrix
 * @pre The first 3x3 block of \c mP is invertible (unenforced)
 * @pre \c mP is a valid projective camera matrix (unenforced)
 * @remarks Positive focal lengths is not guaranteed for an invalid \c mP
 * @ingroup Elements
 */
tuple<IntrinsicCalibrationMatrixT, Coordinate3DT, RotationMatrix3DT> DecomposeCamera(const CameraMatrixT& mP, bool flagNormalised)
{
	Matrix3d mKR=mP.topLeftCorner(3,3);

	IntrinsicCalibrationMatrixT mK; mK.setIdentity();
	RotationMatrix3DT mR=mKR;
	Coordinate3DT vC;
	if(!flagNormalised)
	{
		tie(mK, mR)=RQDecomposition33(mKR);

		//Transform mK, so that all diagonal entries are positive
		Matrix3d mT; mT.setIdentity();
		for(size_t c=0; c<3; ++c)
			if(mK(c,c)<0)
				mT(c,c)=-1;

		//mT must be a rotation matrix
		if(mT(0,0)*mT(1,1)*mT(2,2)<0)
		{
			//If only mT(2,2)=-1, this correction produces a valid mK. If all 3 entries are negative, then the resulting mK is invalid
			mT*=-1;
			mK*=-1;
		}	//if(mT(0,0)*mT(1,1)*mT(2,2)<0)


		mK*=mT; mK/=mK(2,2);
		mR=mT*mR;	//mT is its own inverse
		vC.noalias() = -mKR.inverse() * mP.col(3);
	}
	else
	{
		mR=sqrt(3)*mR.normalized();	//The norm of a rotation matrix is sqrt(3)
		vC.noalias()= -mR.transpose() * mP.col(3);
	}

	return make_tuple(mK, vC, mR);
}	//tuple<IntrinsicCalibrationMatrixT, Coordinate3DT, RotationMatrix3DT> DecomposeCamera(const CameraMatrixT& mP)

/**
 * @brief Composes a camera matrix
 * @param[in] mK Intrinsic calibration matrix
 * @param[in] position Position
 * @param[in] orientation Orientation
 * @return The camera matrix defined by the input parameters
 * @ingroup Elements
 */
CameraMatrixT ComposeCameraMatrix(const IntrinsicCalibrationMatrixT& mK, const Coordinate3DT& position, const RotationMatrix3DT& orientation)
{
	return mK*ComposeCameraMatrix(position, orientation);
}	//CameraMatrixT ComposeCameraMatrix(const IntrinsicCalibrationMatrixT& mK, const Coordinate3DT& position, const RotationMatrix3DT& orientation)

/**
 * @brief Composes a camera matrix
 * @param[in] mK Intrinsic calibration matrix
 * @param[in] position Position
 * @param[in] orientation Orientation
 * @return The camera matrix defined by the input parameters
 * @ingroup Elements
 */
CameraMatrixT ComposeCameraMatrix(const IntrinsicCalibrationMatrixT& mK, const Coordinate3DT& position, const QuaternionT& orientation)
{
	return ComposeCameraMatrix(mK, position, orientation.matrix());
}	//CameraMatrixT ComposeCameraMatrix(const Coordinate3DT& position, const QuaternionT& orientation)

/**
 * @brief Composes a normalised camera matrix
 * @param[in] position Position
 * @param[in] orientation Orientation
 * @return The camera matrix defined by the input parameters
 * @ingroup Elements
 */
CameraMatrixT ComposeCameraMatrix(const Coordinate3DT& position, const RotationMatrix3DT& orientation)
{
	CameraMatrixT mP;
	mP.topLeftCorner(3,3)=orientation;
	mP.col(3)= -orientation*position;

	return mP;
}	//CameraMatrixT ComposeCameraMatrix(const IntrinsicCalibrationMatrixT& mK, const Coordinate3DT& position, const QuaternionT& orientation)

/**
 * @brief Computes the homography that maps a camera to the canonical camera matrix
 * @param[in] mP Camera matrix
 * @param[in] flagAffine If \c true , the function returns an affine transformation (i.e. the last row is [0 0 0 1])
 * @return A tuple: Homography that converts \c mP to a canonical camera, when multiplied on the right; the corresponding transformation for the reference frame
 * @pre First 3x3 submatrix of \c mP is invertible, if \c flagAffine is \c true (unenforced)
 * @warning \c CameraFromFundamental does not produce a camera matrix with an invertible 3x3 submatrix
 * @ingroup Elements
 */
tuple<Homography3DT,Homography3DT> MapToCanonical(const CameraMatrixT& mP, bool flagAffine)
{
	Homography3DT mH=Homography3DT::Identity();
	Homography3DT mHi;

	if(flagAffine)
	{
		//Construct an affine transformation
		Matrix<ValueTypeM<CameraMatrixT>::type, 3, 3> mMi=mP.block(0,0,3,3).inverse();
		mH.block(0,0,3,3)=mMi;
		mH.block(0,3,3,1)=-mMi*mP.col(3);

		mHi=Homography3DT::Identity();
		mHi.block(0,0,3,4)=mP;
	}
	else
	{
		//Construct a projective homography

		//The pseudo-inverse of the camera matrix is a 4x3 matrix that transforms the first 3x3 submatrix to identity
		Matrix<ValueTypeM<CameraMatrixT>::type, 4,3> mPi=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mP);	//Pseudo-inverse
		mH.block(0,0,4,3)=mPi;

		//The last row is chosen as the camera centre, which is orthogonal to the rows of the camera matrix
		JacobiSVD<CameraMatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd(mP, Eigen::ComputeFullU | Eigen::ComputeFullV);	//SVD
		mH.col(3)=svd.matrixV().col(3);

		mHi=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mH);	//Pseudo-inverse, as mH is not guaranteed to be invertible
	}	//if(flagAffine)

	return make_tuple(mH, mHi);
}	//Homogrpahy3DT MapToCanonical(const CameraMatrixT& mP)

/**
 * @brief Extracts the intrinsic calibration parameters for a camera from the dual absolute quadric
 * @param[in] mQ Dual absolute quadric
 * @param[in] mP Projective camera matrix
 * @return Intrinsic calibration matrix. Invalid if the decomposition fails
 * @pre \c mQ is a positive or negative semi-definite matrix
 */
optional<IntrinsicCalibrationMatrixT> IntrinsicsFromDAQ(const Matrix<ValueTypeM<CameraMatrixT>::type, 4, 4> & mQ, const CameraMatrixT& mP)
{
	//Project
	Matrix3d mW= mP*(mQ*mP.transpose());

	//LDLT decomposition
	Eigen::LLT<Matrix3d> llt;
	llt.compute(mW);

	optional<IntrinsicCalibrationMatrixT> output;

	if(llt.info()==Eigen::Success)
	{
		output=llt.matrixU();
		*output/= (*output)(2,2);
	}	//if(ldlt.info()==Eigen::Success)

	return output;
}	//IntrinsicCalibrationMatrixT IntrinsicsFromDAQ(const Matrix<ValueTypeM<CameraMatrixT>::type, 4, 4> & mQ, const CameraMatrixT& mP)

/**
 * @brief Converts a camera translation to a camera position(i.e., t->C)
 * @param[in] translation Translation vector
 * @param[in] orientation Rotation matrix
 * @return Camera position vector
 * @ingroup Elements
 */
Coordinate3DT TranslationToPosition(const Coordinate3DT translation, const RotationMatrix3DT& orientation)
{
	return -orientation.transpose()*translation;
}	//Coordinate3DT TranslationToPosition(const Coordinate3DT translation, const RotationMatrix3DT& orientation)

/**
 * @brief Computes the field-of-view for a camera
 * @param[in] edgeLength Length of the edge along which the FoV is measured (width, height or diagonal), in pixels
 * @param[in] focalLength Focal length, in pixels
 * @return FoV in radians
 * @pre \c edgeLength>0
 * @pre \c focalLength>0
 * @remarks The equation works as long as \c edgeLength and \c focalLength have the same units. For example, if the size of the CCD is known, edge length and focal length could be in mm
 */
double ComputeFoV(double edgeLength, double focalLength)
{
	assert(edgeLength>0);
	assert(focalLength>0);
	return 2*atan(0.5*edgeLength/focalLength);
}	//double ComputeFoV(double length, double focalLength)

/**
 * @brief Computes the boundaries within which the object is in-focus
 * @param[in] focusDistance Subject distance, in m
 * @param[in] focalLength Focal length, in pixels
 * @param[in] fNumber F number
 * @param[in] pixelSize Edge length of a pixel, in m. If rectangular, take the smaller size. Corresponds to the radius of the circle of confusion.
 * @return A tuple, near and far distances
 * @remarks The width corresponds to the depth-of-field
 * @remarks Reference: "Automatic Sensor Placement for Vision Task Requirements, " K. G. Cowan and P. D. Kovesi, PAMI vol 10, no. 3, pp. 407-416, May 1988
 */
tuple<double, double> ComputeFocusField(double focusDistance, double focalLength, double fNumber, double pixelSize)
{
	double hyperfocal= pow<2>(focalLength) * pixelSize / fNumber;	//in m

	double num= hyperfocal*focusDistance;

	tuple<double, double> output;
	get<0>(output)= num/(hyperfocal + (focusDistance - focalLength*pixelSize));

	//If the subject is at or beyond the hyperfocal distance, the focus field extends to infinity
	get<1>(output)= (hyperfocal>focusDistance) ? num/(hyperfocal - (focusDistance - focalLength*pixelSize)) : numeric_limits<double>::infinity();

	return output;
}	//tuple<double, double> ComputeFocusField(double focusDistance, double focalLength, double fNumber, double rCoC)

/**
 * @brief Computes the canonical camera pair corresponding to a fundamental matrix
 * @param[in] mF Fundamental matrix
 * @return Second camera of the pair
 * @post The returned camera matrix has unit norm
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.256, Result 9.14
 * @ingroup Elements
 */
CameraMatrixT CameraFromFundamental(const EpipolarMatrixT& mF)
{
	JacobiSVD<EpipolarMatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd(mF, Eigen::ComputeFullU | Eigen::ComputeFullV);
	HCoordinate2DT e2=svd.matrixU().col(2);	// Left epipole

	CameraMatrixT mP2;
	mP2.block(0,0,3,3)= MakeCrossProductMatrix(e2) * mF;
	mP2.col(3)=e2;
	mP2.normalize();

	return mP2;
}	//tuple<CameraMatrixT, CameraMatrixT> CameraFromFundamental(const EpipolarMatrixT& mF)

/**
 * @brief Computes the fundamental matrix corresponding to a camera pair
 * @param[in] mP1 First camera
 * @param[in] mP2 Second camera
 * @return Fundamental matrix corresponding to the camera pair
 * @post The returned fundamental matrix has unit norm
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.246, Table 9.1
 * @ingroup Elements
 */
EpipolarMatrixT CameraToFundamental(const CameraMatrixT& mP1, const CameraMatrixT& mP2)
{
	//Epipole in the second image
	JacobiSVD<CameraMatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd(mP1, Eigen::ComputeFullU | Eigen::ComputeFullV);
	HCoordinate2DT e2=mP2*svd.matrixV().col(3);

	typedef typename ValueTypeM<CameraMatrixT>::type RealT;
	Matrix<RealT, 4, 3> mP1i=ComputePseudoInverse<Eigen::FullPivHouseholderQRPreconditioner>(mP1);

	EpipolarMatrixT mE2x= MakeCrossProductMatrix(e2);
	EpipolarMatrixT mF=  mE2x * (mP2 * mP1i).eval();

	mF.normalize();

	return mF;
}	//EpipolarMatrixT CameraToFundamental(const CameraMatrixT& mP1, const CameraMatrixT& mP2)

/**
 * @brief Computes the fundamental matrix corresponding to a canonical camera pair
 * @param[in] mP2 Second camera of the canonical pair
 * @param[in] flagNormalise If \c true , the result is normalised
 * @return Fundamental matrix corresponding to the camera pair
 * @post The returned fundamental matrix has unit norm
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.246, Table 9.1
 * @remarks CameraToFundamental and CameraFromFundamental are not inverses to each other, due to the projective ambiguity mentioned in Result 9.15 (pp. 256).
 * @ingroup Elements
 */
EpipolarMatrixT CameraToFundamental(const CameraMatrixT& mP2, bool flagNormalise)
{
	EpipolarMatrixT mE2x=MakeCrossProductMatrix<typename ValueTypeM<CameraMatrixT>::type >(mP2.col(3));
	EpipolarMatrixT mF= mE2x * mP2.block(0,0,3,3);

	if(flagNormalise)
		mF.normalize();

	return mF;
}	//EpipolarMatrixT CameraToFundamental(const CameraMatrixT& mP2)

/**
 * @brief EpipolarMatrixT Computes the essential matrix corresponding to a camera pair
 * @param[in] vC1 Position of the first camera
 * @param[in] mR1 Orientation of the first camera
 * @param[in] vC2 Position of the second camera
 * @param[in] mR2 Orientation of the second camera
 * @param[in] flagNormalise If \c true , the result is normalised
 * @return Essential matrix corresponding to the camera pair
 */
EpipolarMatrixT CameraToEssential(const Coordinate3DT& vC1, const RotationMatrix3DT& mR1, const Coordinate3DT& vC2, const RotationMatrix3DT& mR2, bool flagNormalise)
{
	RotationMatrix3DT mR=mR1*mR2.transpose();
	Coordinate3DT vC= mR2*(vC1-vC2);
	return ComposeEssential(vC, mR, flagNormalise);
}	//EpipolarMatrixT CameraToEssential(const Coordinate3DT& vC1, const RotationMatrix3DT& mR1, const Coordinate3DT& vC2, const RotationMatrix3DT& mR2, bool flagNormalise)

/**
 * @brief 2D homography between the images acquired by two cameras with coincident camera centres
 * @param[in] mR1 Rotation matrix of the first camera
 * @param[in] mK1 Intrinsic calibration matrix of the first camera
 * @param[in] mR2 Rotation matrix of the second camera
 * @param[in] mK2 Intrinsic calibration matrix of the second camera
 * @param[in] flagNormalise If \c true , the result is normalised
 * @return Homography induced by the rotation and focal length change
 * @pre \c mK1 is invertible (unenforced)
 * @ingroup Utility
 */
Homography2DT CameraToHomography(const RotationMatrix3DT& mR1, const IntrinsicCalibrationMatrixT& mK1, const RotationMatrix3DT& mR2, const IntrinsicCalibrationMatrixT& mK2, bool flagNormalise)
{
	Homography2DT mKRi=mR1.transpose()*mK1.inverse();
	Homography2DT mH=(mK2*mR2).eval()*mKRi;

	if(flagNormalise)
		mH.normalize();

	return mH;
}	//Homography2DT CameraToHomography(const RotationMatrix3DT& mR1, const IntrinsicCalibrationMatrixT& mK1, const RotationMatrix3DT& mR2, const IntrinsicCalibrationMatrixT& mK2)

/**
 * @brief Jacobian for the camera-to-fundamental matrix conversion
 * @param[in] mP Camera matrix
 * @param[in] flagNormalise If \c true , the Jacobian is for the case where the fundamental matrix is scaled to have a unit norm
 * @return Jacobian matrix
 * @remarks For \f$ \mathbf{P=[ M m]}\f$, \f$ \mathbf{J= [ \nabla_M F | \nabla_m F]}\f$
 * @remarks Normalised case is not tested, as there is a separate test for \c JacobianNormalise
 */
Matrix<typename ValueTypeM<CameraMatrixT>::type, 9, 12> JacobianCameraToFundamental(const CameraMatrixT& mP, bool flagNormalise)
{
	typedef ValueTypeM<CameraMatrixT>::type RealT;
	Matrix<RealT, 9, 12> mJ; mJ.setZero();
	Matrix<RealT, 3, 3> mI; mI.setIdentity();

	mJ.block(0,3,3,3) =-mP(2,3)*mI; mJ.block(3,0,3,3) =-mJ.block(0,3,3,3);
	mJ.block(0,6,3,3) = mP(1,3)*mI; mJ.block(6,0,3,3) =-mJ.block(0,6,3,3);
	mJ.block(3,6,3,3) =-mP(0,3)*mI; mJ.block(6,3,3,3) =-mJ.block(3,6,3,3);

	mJ.block(0,10,3,1)= mP.block(2,0,1,3).transpose(); mJ.block(3,9,3,1) =-mJ.block(0,10,3,1);
	mJ.block(0,11,3,1)=-mP.block(1,0,1,3).transpose(); mJ.block(6,9,3,1) =-mJ.block(0,11,3,1);
	mJ.block(3,11,3,1)= mP.block(0,0,1,3).transpose(); mJ.block(6,10,3,1)=-mJ.block(3,11,3,1);

	if(flagNormalise)
	{
		EpipolarMatrixT mF=CameraToFundamental(mP, true);
		Matrix<RealT, 9, 9> mJn=JacobianNormalise<Matrix<RealT, 9, 9> >(mF);
		return mJn*mJ;
	}	//if(flagNormalise)
	else
		return mJ;
}	//Matrix<typename ValueTypeM<CameraMatrixT>::type, 9, 12> JacobianCameraToFundamental(const CameraMatrixT& mP)

/**
 * @brief Jacobian for the normalised camera-to-essential matrix conversion
 * @param[in] mP Normalised camera matrix
 * @param[in] flagNormalise If \c true , the Jacobian is for the case where the essential matrix is scaled to have a unit norm
 * @return Jacobian matrix
 * @remarks For \f$ \mathbf{P=[ R -RC]}\f$, \f$ \mathbf{J= [ \nabla_R E | \nabla_C E]}\f$
 * @remarks Normalised case is not tested, as there is a separate test for \c JacobianNormalise
 */
Matrix<ValueTypeM<CameraMatrixT>::type, 9, 12> JacobianNormalisedCameraToEssential(const CameraMatrixT& mP, bool flagNormalise)
{
	typedef ValueTypeM<CameraMatrixT>::type RealT;
	Matrix<RealT, 9, 12> mJ; mJ.setZero();
	Matrix<RealT, 3, 3> mI; mI.setIdentity();

	RotationMatrix3DT mR=mP.block(0,0,3,3);
	Coordinate3DT vC=TranslationToPosition(mP.col(3), mR);

	//dEdR

	RotationMatrix3DT mRt=mR.transpose();

	mJ.block(0,3,3,3)=-mP(2,3)*mI;
	mJ.block(0,3,3,1)-=vC[0]*mRt.col(2);
	mJ.block(0,4,3,1)-=vC[1]*mRt.col(2);
	mJ.block(0,5,3,1)-=vC[2]*mRt.col(2);

	mJ.block(0,6,3,3)=mP(1,3)*mI;
	mJ.block(0,6,3,1)+=vC[0]*mRt.col(1);
	mJ.block(0,7,3,1)+=vC[1]*mRt.col(1);
	mJ.block(0,8,3,1)+=vC[2]*mRt.col(1);

	mJ.block(3,6,3,3)=-mP(0,3)*mI;
	mJ.block(3,6,3,1)-=vC[0]*mRt.col(0);
	mJ.block(3,7,3,1)-=vC[1]*mRt.col(0);
	mJ.block(3,8,3,1)-=vC[2]*mRt.col(0);

	mJ.block(3,0,3,3)=-mJ.block(0,3,3,3);
	mJ.block(6,0,3,3)=-mJ.block(0,6,3,3);
	mJ.block(6,3,3,3)=-mJ.block(3,6,3,3);

	//dEdC

	mJ.block(0,9,3,1)= mR(2,0)*mRt.col(1)-mR(1,0)*mRt.col(2); mJ(0,9)=0;
	mJ.block(0,10,3,1)= mR(2,1)*mRt.col(1)-mR(1,1)*mRt.col(2); mJ(1,10)=0;
	mJ.block(0,11,3,1)= mR(2,2)*mRt.col(1)-mR(1,2)*mRt.col(2); mJ(2,11)=0;

	mJ.block(3,9,3,1)=-mR(2,0)*mRt.col(0)+mR(0,0)*mRt.col(2); mJ(3,9)=0;
	mJ.block(3,10,3,1)=-mR(2,1)*mRt.col(0)+mR(0,1)*mRt.col(2); mJ(4,10)=0;
	mJ.block(3,11,3,1)=-mR(2,2)*mRt.col(0)+mR(0,2)*mRt.col(2); mJ(5,11)=0;

	mJ.block(6,9,3,1)= mR(1,0)*mRt.col(0)-mR(0,0)*mRt.col(1); mJ(6,9)=0;
	mJ.block(6,10,3,1)= mR(1,1)*mRt.col(0)-mR(0,1)*mRt.col(1); mJ(7,10)=0;
	mJ.block(6,11,3,1)= mR(1,2)*mRt.col(0)-mR(0,2)*mRt.col(1); mJ(8,11)=0;

	if(flagNormalise)
	{
		EpipolarMatrixT mE=ComposeEssential(mP.col(3), mR, true);
		Matrix<RealT, 9, 9> mJn=JacobianNormalise<Matrix<RealT, 9, 9> >(mE);
		return mJn*mJ;
	}
		return mJ;
}	//Matrix<ValueTypeM<CameraMatrixT>::type, 9, 12> JacobianNormalisedCameraToEssential(const CameraMatrixT& mP)

/**
 * @brief Jacobian of the decomposition of a normalised camera matrix
 * @param[in] mP Normalised camera matrix
 * @return Jacobian matrix. [Camera centre(3); Rotation(9)]
 */
Matrix<ValueTypeM<CameraMatrixT>::type, 12,12> JacobiandPdCR(const CameraMatrixT& mP)
{
	IntrinsicCalibrationMatrixT mK;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(mK, vC, mR)=DecomposeCamera(mP);

	typedef ValueTypeM<CameraMatrixT>::type RealT;
	RealT s=sqrt(3)/mP.block(0,0,3,3).norm();	//DecomposeCamera effectively scales mP so that the first 3x3 submatrix has a norm of sqrt(3)

	//P=[R t]

	//dtdR,C

	typedef Matrix<RealT,3,9> Matrix39T;
	Matrix39T dtdR=-JacobiandABdA<Matrix39T>(mR, vC);
	Matrix3d dtdC=-JacobiandABdB<Matrix3d>(mR,vC);

	//dPdR

	Matrix<RealT, 12, 12> mJ; mJ.setZero();

	mJ.block(0,3,3,3)=Matrix3d::Identity(); mJ.block(3,3,1,9)=dtdR.row(0);
	mJ.block(4,6,3,3)=Matrix3d::Identity(); mJ.block(7,3,1,9)=dtdR.row(1);
	mJ.block(8,9,3,3)=Matrix3d::Identity(); mJ.block(11,3,1,9)=dtdR.row(2);

	//dPdC
	mJ.block(3,0,1,3)=dtdC.row(0);
	mJ.block(7,0,1,3)=dtdC.row(1);
	mJ.block(11,0,1,3)=dtdC.row(2);

	return mJ*s;
}	//Matrix<ValueTypeM<CameraMatrixT>::type, 12,12> JacobianNormalisedCameraDecomposition(const CameraMatrixT& mP)

/**
 * @brief Jacobian of the decomposition of a camera matrix
 * @param[in] mP Camera matrix
 * @return Jacobian matrix. [focal length; aspect ratio; skewness; principal point(2); camera centre(3); rotation(9) ]
 */
Matrix<ValueTypeM<CameraMatrixT>::type, 12,17> JacobiandPdKCR(const CameraMatrixT& mP)
{
	IntrinsicCalibrationMatrixT mK;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(mK, vC, mR)=DecomposeCamera(mP);

	//Compute the scale factor so that mK(2,2)=1 and ||mR||=sqrt(3)
	typedef ValueTypeM<CameraMatrixT>::type RealT;
	RealT s= (mK*mR).norm()/mP.block(0,0,3,3).norm();

	// P= K Pn
	CameraMatrixT mPn=ComposeCameraMatrix(vC, mR);

	//dPdK
	typedef ValueTypeM<CameraMatrixT>::type RealT;
	Matrix<RealT, 12, 17> output; output.setZero();

	typedef Matrix<RealT, 12, 9> Matrix129T;
	Matrix129T dPdK=JacobiandABdA<Matrix129T>(mK, mPn);

	typedef Matrix<RealT, 9, 5> Matrix95T;
	vector<size_t> indexK{0,4,1,2,5};	//f1, f2, skewness, principal point
	output.block(0,0,12,5)= dPdK * JacobiandAda<Matrix95T>(9, indexK);
	output.col(0)+=output.col(1)*mK(1,1)/mK(0,0);	// dPdf1 = dPdf1 + dPdf2 df2df1
	output.col(1)*=mK(0,0);	//f2->alpha

	//dPdC,R
	typedef Matrix<RealT, 12, 12> Matrix1212T;
	Matrix1212T dPdPn=JacobiandABdB<Matrix1212T>(mK, mPn);
	output.block(0,5,12,12)= dPdPn * JacobiandPdCR(mPn);

	return output*s;
}	//Matrix<ValueTypeM<CameraMatrixT>::type, 12,17> JacobianCameraDecomposition(const CameraMatrixT& mP)



}	//ElementsN
}	//SeeSawN

