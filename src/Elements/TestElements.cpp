/**
 * @file TestElements.cpp Unit tests for Elements
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#define BOOST_TEST_MODULE ELEMENTS

#include <boost/optional.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/iterator_range.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <tuple>
#include <iterator>
#include <cmath>
#include <algorithm>
#include <random>
#include <array>
#include "Feature.h"
#include "FeatureUtility.h"
#include "Coordinate.h"
#include "Descriptor.h"
#include "RoI.h"
#include "Correspondence.h"
#include "CorrespondenceDecimator.h"
#include "Camera.h"
#include "GeometricEntity.h"
#include "Viewpoint3D.h"
#include "../Geometry/LensDistortion.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace UnitTestsN
{
namespace TestElementsN
{

using namespace SeeSawN::ElementsN;
using boost::geometry::model::d2::point_xy;
using boost::range_value;
using boost::make_iterator_range;
using boost::optional;
using Eigen::Matrix4d;
using std::equal;
using std::transform;
using std::vector;
using std::array;
using std::sqrt;
using std::tie;
using std::tuple;
using std::get;
using std::next;
using std::equal;
using std::atan;
using std::mt19937_64;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::LensDistortionNoDC;
using SeeSawN::GeometryN::LensDistortionDivC;
using SeeSawN::GeometryN::LensDistortionFP1C;
using SeeSawN::GeometryN::LensDistortionOP1C;
using SeeSawN::NumericN::MakeRankN;

BOOST_AUTO_TEST_SUITE(Coordinates)

BOOST_AUTO_TEST_CASE(Coordinates_Functions)
{
	vector<HCoordinate2DT> coordinates{ HCoordinate2DT(0, 1, 1), HCoordinate2DT(0.5, 0.5, -1), HCoordinate2DT(2, -0.005, 1) };
	vector<HCoordinate2DT> signAligned=AlignSign<ValueTypeM<HCoordinate2DT>::type, 2>(coordinates);

	BOOST_CHECK(signAligned[0].dot(coordinates[0])>0);
	BOOST_CHECK(signAligned[1].dot(coordinates[1])<0);
	BOOST_CHECK(signAligned[2].dot(coordinates[2])>0);
}	//BOOST_AUTO_TEST_CASE(Coordinates_Functions)

BOOST_AUTO_TEST_SUITE_END() 	//Coordinates

BOOST_AUTO_TEST_SUITE(Features)

BOOST_AUTO_TEST_CASE(Image_Feature)
{
    ImageFeatureC feature1;

    typedef ImageFeatureC::coordinate_type CoordinateT;
    typedef ImageFeatureC::descriptor_type DescriptorT;
    typedef ImageFeatureC::roi_type RoIT;

    CoordinateT coordinate; coordinate=CoordinateT::Random();
    DescriptorT descriptor; descriptor=DescriptorT::Random(128);
    RoIT roi; roi=RoIT::Random(4);

    ImageFeatureC feature2(coordinate, descriptor, roi);

    BOOST_CHECK(feature2.Coordinate()==coordinate);
    BOOST_CHECK(feature2.Descriptor()==descriptor);
    BOOST_CHECK(feature2.RoI()==roi);

    feature1.Swap(feature2);

    BOOST_CHECK(feature1.Coordinate()==coordinate);
    BOOST_CHECK(feature1.Descriptor()==descriptor);
    BOOST_CHECK(feature1.RoI()==roi);
}   //BOOST_AUTO_TEST_CASE(Image_Feature)

BOOST_AUTO_TEST_CASE(Binary_Image_Feature)
{
	BinaryImageFeatureC feature1;

	typedef BinaryImageFeatureC::coordinate_type CoordinateT;
	typedef BinaryImageFeatureC::descriptor_type DescriptorT;
	typedef BinaryImageFeatureC::roi_type RoIT;

	CoordinateT coordinate; coordinate=CoordinateT::Random();

	vector<DescriptorT::block_type> blockVector{25, 400};
	DescriptorT descriptor(blockVector.begin(), blockVector.end());	//Appends to front! so 400 comes first
	RoIT roi; roi=RoIT::Random(4);

	BinaryImageFeatureC feature2(coordinate, descriptor, roi);

	BOOST_CHECK(feature2.Coordinate()==coordinate);
	BOOST_CHECK(feature2.Descriptor()==descriptor);
	BOOST_CHECK(feature2.RoI()==roi);

	feature1.Swap(feature2);

	BOOST_CHECK(feature1.Coordinate()==coordinate);
	BOOST_CHECK(feature1.Descriptor()==descriptor);
	BOOST_CHECK(feature1.RoI()==roi);
}	//BOOST_AUTO_TEST_CASE(Binary_Image_Feature)

BOOST_AUTO_TEST_CASE(Scene_Feature)
{
    SceneFeatureC feature1;

    typedef SceneFeatureC::coordinate_type CoordinateT;
    typedef SceneFeatureC::descriptor_type DescriptorT;
    typedef SceneFeatureC::roi_type RoIT;

    CoordinateT coordinate; coordinate=CoordinateT::Random();

    RoIT roi;
    roi.insert(0);
    roi.insert(1);

    //Each descriptor is a 2D feature
    DescriptorT descriptor;
    for(size_t c=0; c<2; ++c)
        descriptor[c]=ImageFeatureC(ImageFeatureC::coordinate_type::Random(), ImageFeatureC::descriptor_type::Random(128), ImageFeatureC::roi_type::Random(4));

    SceneFeatureC feature2(coordinate, descriptor, roi);

    BOOST_CHECK(feature2.Coordinate()==coordinate);
    BOOST_CHECK_EQUAL(feature2.Descriptor().size(), descriptor.size());

    size_t index2=0;
    for(const auto& current : feature2.Descriptor())
    {
    	const ImageFeatureC& pDescriptor=descriptor[index2];
    	BOOST_CHECK_EQUAL(current.first, index2);
    	BOOST_CHECK(current.second.Coordinate()==pDescriptor.Coordinate());
    	BOOST_CHECK(current.second.Descriptor()==pDescriptor.Descriptor());
    	BOOST_CHECK(current.second.RoI()==pDescriptor.RoI());
    	++index2;
    }	//for(const auto& current : feature2.Descriptor())

    //Swap
    feature1.Swap(feature2);

    size_t index1=0;
    for(const auto& current : feature1.Descriptor())
    {
    	const ImageFeatureC& pDescriptor=descriptor[index1];
    	BOOST_CHECK_EQUAL(current.first, index1);
    	BOOST_CHECK(current.second.Coordinate()==pDescriptor.Coordinate());
    	BOOST_CHECK(current.second.Descriptor()==pDescriptor.Descriptor());
    	BOOST_CHECK(current.second.RoI()==pDescriptor.RoI());
    	++index1;
    }	//for(const auto& current : feature2.Descriptor())

    //NormaliseDescriptor
    SceneFeatureC feature3(feature1);
    feature3.NormaliseDescriptor();
    BOOST_CHECK_CLOSE((feature3.Descriptor())[0].Descriptor().norm(), 1.0, 1e-5);
    BOOST_CHECK_CLOSE((feature3.Descriptor())[1].Descriptor().norm(), 1.0, 1e-5);

	//MakeCompact
    SceneFeatureC feature4(coordinate, descriptor, roi);

	feature4.MakeCompact();
	BOOST_CHECK_EQUAL(feature4.Descriptor().size(),1 );
	BOOST_CHECK_EQUAL(feature4.RoI().size(),1 );
	BOOST_CHECK_EQUAL(*feature4.RoI().begin(), 1);
}   //BOOST_AUTO_TEST_CASE(Scene_Feature)

BOOST_AUTO_TEST_CASE(Oriented_Binary_Scene_Feature)
{
	OrientedBinarySceneFeatureC feature1;

	typedef OrientedBinarySceneFeatureC::coordinate_type CoordinateT;
	typedef OrientedBinarySceneFeatureC::descriptor_type DescriptorT;
	typedef OrientedBinarySceneFeatureC::roi_type RoIT;

	CoordinateT coordinate; coordinate=CoordinateT::Random();

	RoIT roi;
	roi.insert(0);
	roi.insert(1);

	//Each descriptor is a 2D binary feature
	DescriptorT descriptor;
	vector<BinaryImageFeatureC::descriptor_type::block_type> blockVector{25, 400};
	for(size_t c=0; c<2; ++c)
		descriptor[c]=BinaryImageFeatureC(BinaryImageFeatureC::coordinate_type::Random(), BinaryImageFeatureC::descriptor_type(blockVector.begin(), blockVector.end()), BinaryImageFeatureC::roi_type::Random(4));

	OrientedBinarySceneFeatureC feature2(coordinate, descriptor, roi);

	BOOST_CHECK(feature2.Coordinate()==coordinate);
	BOOST_CHECK_EQUAL(feature2.Descriptor().size(), descriptor.size());

	size_t index2=0;
	for(const auto& current : feature2.Descriptor())
	{
		const BinaryImageFeatureC& pDescriptor=descriptor[index2];
		BOOST_CHECK_EQUAL(current.first, index2);
		BOOST_CHECK(current.second.Coordinate()==pDescriptor.Coordinate());
		BOOST_CHECK(current.second.Descriptor()==pDescriptor.Descriptor());
		BOOST_CHECK(current.second.RoI()==pDescriptor.RoI());
		++index2;
	}	//for(const auto& current : feature2.Descriptor())

	//Swap
	feature1.Swap(feature2);

	size_t index1=0;
	for(const auto& current : feature1.Descriptor())
	{
		const BinaryImageFeatureC& pDescriptor=descriptor[index1];
		BOOST_CHECK_EQUAL(current.first, index1);
		BOOST_CHECK(current.second.Coordinate()==pDescriptor.Coordinate());
		BOOST_CHECK(current.second.Descriptor()==pDescriptor.Descriptor());
		BOOST_CHECK(current.second.RoI()==pDescriptor.RoI());
		++index1;
	}	//for(const auto& current : feature2.Descriptor())

	//MakeCompact
	OrientedBinarySceneFeatureC feature3(coordinate, descriptor, roi);

	feature3.MakeCompact();
	BOOST_CHECK_EQUAL(feature3.Descriptor().size(),1 );
	BOOST_CHECK_EQUAL(feature3.RoI().size(),1 );
	BOOST_CHECK_EQUAL(*feature3.RoI().begin(), 1);
}	//BOOST_AUTO_TEST_CASE(Oriented_Binary_Scene_Feature)


BOOST_AUTO_TEST_SUITE_END() //Features

BOOST_AUTO_TEST_SUITE(Feature_Utilities)

BOOST_AUTO_TEST_CASE(Lexical_Compare)
{
    //LexicalCompare2DC
    DetailN::LexicalCompare2DC<Coordinate2DT> lexicalCompare;
    BOOST_CHECK_EQUAL(lexicalCompare(Coordinate2DT(0,0), Coordinate2DT(1,1) ), true);
    BOOST_CHECK_EQUAL(lexicalCompare(Coordinate2DT(2,2), Coordinate2DT(1,1) ), false);
    BOOST_CHECK_EQUAL(lexicalCompare(Coordinate2DT(1,2), Coordinate2DT(2,1) ), false);
    BOOST_CHECK_EQUAL(lexicalCompare(Coordinate2DT(2,1), Coordinate2DT(2,3) ), true);

    typedef point_xy<double> PointBGT;
    DetailN::LexicalCompare2DC<PointBGT> lexicalCompare2;
    BOOST_CHECK_EQUAL(lexicalCompare2(PointBGT(0,0), PointBGT(1,1) ), true);
    BOOST_CHECK_EQUAL(lexicalCompare2(PointBGT(2,2), PointBGT(1,1) ), false);
    BOOST_CHECK_EQUAL(lexicalCompare2(PointBGT(1,2), PointBGT(2,1) ), false);
    BOOST_CHECK_EQUAL(lexicalCompare2(PointBGT(2,1), PointBGT(2,3) ), true);
}   //Lexical_Compare

BOOST_AUTO_TEST_CASE(Decimatation)
{
    vector<Coordinate2DT> coordinates(10);
    for(size_t c=0; c<10; ++c)
        coordinates[c]=Coordinate2DT(c, sqrt(c));

    //DecimateFeatures2D

    //negative spacing
    vector<size_t> indexList1=DecimateFeatures2D(coordinates, -1, Coordinate2DT(0,0), Coordinate2DT(10,10));
    vector<size_t> indexList1R(10); iota(indexList1R.begin(), indexList1R.end(), 0);
    BOOST_CHECK_EQUAL_COLLECTIONS(indexList1.begin(), indexList1.end(), indexList1R.begin(), indexList1R.end());

    //Degenerate bounding box
    vector<size_t> indexList2=DecimateFeatures2D(coordinates, 2, Coordinate2DT(10,10), Coordinate2DT(0,0));
    BOOST_CHECK(indexList2.empty());

    //Empty coordinate set
    vector<Coordinate2DT> coordinates2;
    vector<size_t> indexList3=DecimateFeatures2D(coordinates2, 2, Coordinate2DT(0,0), Coordinate2DT(10,10));
    BOOST_CHECK(indexList3.empty());

    //Normal operation
    vector<size_t> indexList4=DecimateFeatures2D(coordinates, 2, Coordinate2DT(0,0), Coordinate2DT(10,10));
    vector<size_t> indexList4R{1, 3, 5, 7};
    BOOST_CHECK_EQUAL_COLLECTIONS(indexList4.begin(), indexList4.end(), indexList4R.begin(), indexList4R.end());
}   //BOOST_AUTO_TEST_CASE(Decimatation)

BOOST_AUTO_TEST_CASE(Feature_Collection_Manipulation)
{
    typedef ImageFeatureC::coordinate_type CoordinateT;
    typedef ImageFeatureC::descriptor_type DescriptorT;
    typedef ImageFeatureC::roi_type RoIT;

    vector<ImageFeatureC> features(10);
    for(auto& current : features)
    {
        current.Coordinate()=CoordinateT::Random();
        current.Descriptor()=DescriptorT::Random(3);
        current.RoI()=RoIT::Random(128);
    }   //for(auto& current : features)

    //MakeCoordinateVector
    vector<CoordinateT> coordinateList=MakeCoordinateVector(features);
    BOOST_CHECK_EQUAL(coordinateList.size(), (size_t)10);
    for(size_t c=0; c<10; ++c)
        BOOST_CHECK(coordinateList[c]==features[c].Coordinate());

    //ComputeBoundingBox2D

    coordinateList[3]=CoordinateT(-1, -1);
    coordinateList[7]=CoordinateT(2, 2);

    Coordinate2DT upperLeft;
    Coordinate2DT lowerRight;
    tie(upperLeft, lowerRight)=ComputeBoundingBox2D(coordinateList);

    BOOST_CHECK(upperLeft==coordinateList[3]);
    BOOST_CHECK(lowerRight==coordinateList[7]);

    //ApplyFeatureCoordinateTransformation
    vector<ImageFeatureC> features2(features);
    Matrix3d mT; mT<<2, 0, 0, 0, 2, 1, 0, 0, 1;
    ApplyFeatureCoordinateTransformation(features2, mT);
    BOOST_CHECK(features2[9].Coordinate()==2*features[9].Coordinate()+Coordinate2DT(0,1));

    //FindContained
    FrameT box(Coordinate2DT(0,0), Coordinate2DT(0.5, 0.5));
    vector<size_t> iContained=FindContained(features, box);

    list<size_t> iContainedR;
    for(size_t c=0; c<10; ++c)
    	if(box.contains(features[c].Coordinate()))
    		iContainedR.push_back(c);

    BOOST_CHECK_EQUAL_COLLECTIONS(iContained.begin(), iContained.end(), iContainedR.begin(), iContainedR.end());
}   //BOOST_AUTO_TEST_CASE(Feature_Collection_Manipulation)

BOOST_AUTO_TEST_SUITE_END() //Feature_Utilities

BOOST_AUTO_TEST_SUITE(Correspondences)

BOOST_AUTO_TEST_CASE(Correspondence_Set_Manipulation)
{
	typedef ValueTypeM<Coordinate2DT>::type RealT;

    vector<Coordinate2DT> coordinates1(2);
    coordinates1[0]<<0, 1;
    coordinates1[1]<<1, 2;

    vector<Coordinate2DT> coordinates2(2);
    coordinates2[0]<<3, 4;
    coordinates2[1]<<9, 3;

    MatchStrengthC strength(0.25, 0.5);

    CorrespondenceListT correspondences;
    correspondences.push_back( typename CorrespondenceListT::value_type(0, 1, strength));

    //MakeCoordinateCorrespondenceList

    CoordinateCorrespondenceList2DT coordinateCorrespondence=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(correspondences, coordinates1, coordinates2);

    BOOST_CHECK(coordinateCorrespondence.begin()->left == coordinates1[0]);
    BOOST_CHECK(coordinateCorrespondence.begin()->right == coordinates2[1]);
    BOOST_CHECK_EQUAL(coordinateCorrespondence.begin()->info.Similarity(), strength.Similarity());
    BOOST_CHECK_EQUAL(coordinateCorrespondence.begin()->info.Ambiguity(), strength.Ambiguity());

    //MakeFeatureCorrespondenceList
    typedef ImageFeatureC::coordinate_type CoordinateT;
    typedef ImageFeatureC::descriptor_type DescriptorT;
	typedef ImageFeatureC::roi_type RoIT;

    vector<ImageFeatureC> featureList1(10);
	for(auto& current : featureList1)
	{
		current.Coordinate()=CoordinateT::Random();
		current.Descriptor()=DescriptorT::Random(3);
		current.RoI()=RoIT::Random(128);
	}   //for(auto& current : features)

	vector<ImageFeatureC> featureList2(featureList1);

	vector<ImageFeatureC> purged1(featureList1);
	vector<ImageFeatureC> purged2(featureList2);

	MakeFeatureCorrespondenceList(purged1, purged2, correspondences);

	BOOST_CHECK_EQUAL(purged1.size(), 1);
	BOOST_CHECK_EQUAL(purged2.size(), 1);

	BOOST_CHECK(purged1[0].Coordinate()==featureList1[0].Coordinate());
	BOOST_CHECK(purged1[0].Descriptor()==featureList1[0].Descriptor());
	BOOST_CHECK(purged1[0].RoI()==featureList1[0].RoI());

	BOOST_CHECK(purged2[0].Coordinate()==featureList2[1].Coordinate());
	BOOST_CHECK(purged2[0].Descriptor()==featureList2[1].Descriptor());
	BOOST_CHECK(purged2[0].RoI()==featureList2[1].RoI());

	//ApplyStrengthFilter

	CorrespondenceListT correspondences2;
	correspondences2.push_back(CorrespondenceListT::value_type(0, 0, MatchStrengthC(0.25, 0.25)));
	correspondences2.push_back(CorrespondenceListT::value_type(1, 2, MatchStrengthC(0.1, 0.25)));
	correspondences2.push_back(CorrespondenceListT::value_type(3, 5, MatchStrengthC(0.8, 0.1)));
	correspondences2.push_back(CorrespondenceListT::value_type(4, 6, MatchStrengthC(0.5, 0.3)));

	ApplyStrengthFilter(correspondences2, MatchStrengthC(0.4, 0.2));
	BOOST_CHECK_EQUAL(correspondences2.size(), 1);
	BOOST_CHECK_EQUAL(correspondences2.begin()->left, 3);
	BOOST_CHECK_EQUAL(correspondences2.begin()->right, 5);

	//SortByAmbiguity

	CorrespondenceListT correspondences3;
	correspondences3.push_back(CorrespondenceListT::value_type(0, 0, MatchStrengthC(0.25, 0.25)));
	correspondences3.push_back(CorrespondenceListT::value_type(1, 2, MatchStrengthC(0.1, 0.25)));
	correspondences3.push_back(CorrespondenceListT::value_type(3, 5, MatchStrengthC(0.8, 0.1)));
	correspondences3.push_back(CorrespondenceListT::value_type(4, 6, MatchStrengthC(0.5, 0.3)));

	tuple<CorrespondenceListT, vector<size_t> > sorted;
	sorted=SortByAmbiguity(correspondences3);

	BOOST_CHECK_EQUAL(get<0>(sorted).begin()->info.Ambiguity(),0.1);
	BOOST_CHECK_EQUAL(next(get<0>(sorted).begin(),1)->info.Ambiguity(),0.25);
	BOOST_CHECK_EQUAL(next(get<0>(sorted).begin(),2)->info.Ambiguity(),0.25);
	BOOST_CHECK_EQUAL(next(get<0>(sorted).begin(),3)->info.Ambiguity(),0.3);

	vector<size_t> indexMap{2, 0, 1, 3};
	BOOST_CHECK_EQUAL_COLLECTIONS(indexMap.cbegin(), indexMap.cend(), get<1>(sorted).cbegin(), get<1>(sorted).cend());

	//RankByAmbiguity
	vector<unsigned int> ranks=RankByAmbiguity(correspondences3);
	vector<unsigned int> ranksR{1, 2, 0, 3};
	BOOST_CHECK_EQUAL_COLLECTIONS(ranksR.cbegin(), ranksR.cend(), ranks.cbegin(), ranks.cend());

	//CorrespondenceRangeToVector

	CorrespondenceListT correspondences4;
	correspondences4.push_back( typename CorrespondenceListT::value_type(0, 0, strength));
	correspondences4.push_back( typename CorrespondenceListT::value_type(1, 1, strength));
	CoordinateCorrespondenceList2DT coordinateCorrespondence2=MakeCoordinateCorrespondenceList<CoordinateCorrespondenceList2DT>(correspondences4, coordinates1, coordinates2);

	Matrix<RealT, Eigen::Dynamic, 1> vectorForm=CorrespondenceRangeToVector<RealT>(coordinateCorrespondence2);
	Matrix<RealT, Eigen::Dynamic, 1> vectorFormR(8); vectorFormR<<0,1,3,4,1,2,9,3;
	BOOST_CHECK(vectorForm==vectorFormR);

	//VectorToCorrespondenceRange
	bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> bimapForm=VectorToCorrespondenceRange<Coordinate2DT, Coordinate2DT>(make_iterator_range(vectorForm.data(), vectorForm.data()+8), 2, 2);
	BOOST_CHECK_EQUAL(bimapForm.size(),2);
	BOOST_CHECK(equal(bimapForm.begin(), bimapForm.end(), coordinateCorrespondence2.begin()));

	bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> bimapForm2=VectorToCorrespondenceRange<Coordinate2DT, Coordinate2DT>(make_iterator_range(vectorForm.data(), vectorForm.data()+8));
	BOOST_CHECK(equal(bimapForm2.begin(), bimapForm2.end(), coordinateCorrespondence2.begin()));
	BOOST_CHECK_EQUAL(bimapForm2.size(),2);
}   //BOOST_AUTO_TEST_CASE(Correspondence_Set_Manipulation)

BOOST_AUTO_TEST_CASE(Correspondence_Decimation)
{
	CoordinateCorrespondenceListT<Coordinate2DT, Coordinate2DT> correspondences;
	typedef CoordinateCorrespondenceListT<Coordinate2DT, Coordinate2DT>::value_type ValueT;
	correspondences.push_back(ValueT(Coordinate2DT(0,0), Coordinate2DT(2,1), MatchStrengthC(0.25, 0.25)));
	correspondences.push_back(ValueT(Coordinate2DT(0.1,0.2), Coordinate2DT(2.1,1.1), MatchStrengthC(0.1, 0.25)));
	correspondences.push_back(ValueT(Coordinate2DT(4,3), Coordinate2DT(2,2), MatchStrengthC(0.8, 0.1)));
	correspondences.push_back(ValueT(Coordinate2DT(3.9,3.05), Coordinate2DT(2.01, 2.05), MatchStrengthC(0.5, 0.3)));

	typedef CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT> CorrespondenceDecimatorT;
	typedef typename CorrespondenceDecimatorT::dimension_type1 ArrayLT;
	typedef typename CorrespondenceDecimatorT::dimension_type2 ArrayRT;

	CorrespondenceDecimatorParametersC parameters;
	parameters.maxDensity=1;
	parameters.minElements=0;

	//Sort only
	ArrayLT binSize1a; binSize1a<<1.25,1.25;
	ArrayRT binSize2a; binSize2a<<1.25,1.25;
	CoordinateCorrespondenceList2DT decimated1;
	vector<size_t> indexList1;
	//tie(decimated1, indexList1);	//For some reason, tie does not work
	auto output1=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters);
	indexList1.swap(get<1>(output1));
	decimated1.swap(get<0>(output1));

	BOOST_CHECK_EQUAL(decimated1.size(), 4);
	BOOST_CHECK(decimated1.begin()->left==Coordinate2DT(4,3));
	BOOST_CHECK(decimated1.begin()->right==Coordinate2DT(2,2));
	BOOST_CHECK(decimated1.rbegin()->left==Coordinate2DT(3.9,3.05));
	BOOST_CHECK(decimated1.rbegin()->right==Coordinate2DT(2.01,2.05));

	vector<size_t> sortedIndex{2,0,1,3};
	BOOST_CHECK_EQUAL_COLLECTIONS(indexList1.begin(), indexList1.end(), sortedIndex.begin(), sortedIndex.end());

	//Best only
	CorrespondenceDecimatorParametersC parameters2(parameters); parameters2.maxElements=1;
	CoordinateCorrespondenceList2DT decimated2;
	vector<size_t> indexList2;
	auto output2=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters2);
	indexList2.swap(get<1>(output2));
	decimated2.swap(get<0>(output2));

	BOOST_CHECK_EQUAL(decimated2.size(), 1);
	BOOST_CHECK(decimated2.begin()->left==Coordinate2DT(4,3));
	BOOST_CHECK(decimated2.begin()->right==Coordinate2DT(2,2));

	BOOST_CHECK_EQUAL(indexList2.size(),1);
	BOOST_CHECK_EQUAL(indexList2[0], 2);

	//No sorting
	CorrespondenceDecimatorParametersC parameters3(parameters); parameters3.flagSort=false;
	CoordinateCorrespondenceList2DT decimated3;
	vector<size_t> indexList3;
	auto output3=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters3);
	indexList3.swap(get<1>(output3));
	decimated3.swap(get<0>(output3));

	BOOST_CHECK_EQUAL(decimated3.size(), 4);
	BOOST_CHECK(decimated3.begin()->left==Coordinate2DT(0,0));
	BOOST_CHECK(decimated3.begin()->right==Coordinate2DT(2,1));
	BOOST_CHECK(decimated3.rbegin()->left==Coordinate2DT(3.9,3.05));
	BOOST_CHECK(decimated3.rbegin()->right==Coordinate2DT(2.01,2.05));

	vector<size_t> unsortedIndex{0,1,2,3};
	BOOST_CHECK_EQUAL_COLLECTIONS(indexList3.begin(), indexList3.end(), unsortedIndex.begin(), unsortedIndex.end());

	//Spatial quantisation
	CorrespondenceDecimatorParametersC parameters4(parameters); parameters4.quantisationStrategy=DecimatorQuantisationStrategyT::BOTH;
	CoordinateCorrespondenceList2DT decimated4;
	vector<size_t> indexList4;
	auto output4=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters4);
	indexList4.swap(get<1>(output4));
	decimated4.swap(get<0>(output4));

	BOOST_CHECK_EQUAL(decimated4.size(), 2);
	BOOST_CHECK(decimated4.begin()->left==Coordinate2DT(4,3));
	BOOST_CHECK(decimated4.begin()->right==Coordinate2DT(2,2));
	BOOST_CHECK(decimated4.rbegin()->left==Coordinate2DT(0,0));
	BOOST_CHECK(decimated4.rbegin()->right==Coordinate2DT(2,1));

	vector<size_t> indexList4r{2,0};
	BOOST_CHECK_EQUAL_COLLECTIONS(indexList4.begin(), indexList4.end(), indexList4r.begin(), indexList4r.end());

	//Minimum set size
	CorrespondenceDecimatorParametersC parameters5(parameters); parameters5.minElements=2; parameters5.maxElements=1;
	CoordinateCorrespondenceList2DT decimated5;
	vector<size_t> indexList5;
	auto output5=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters5);
	indexList5.swap(get<1>(output5));
	decimated5.swap(get<0>(output5));

	BOOST_CHECK_EQUAL(decimated5.size(), 2);
	BOOST_CHECK_EQUAL_COLLECTIONS(indexList5.begin(), indexList5.end(), indexList4r.begin(), indexList4r.end());

	//Other quantisation strategies
	CorrespondenceDecimatorParametersC parameters6(parameters); parameters6.quantisationStrategy=DecimatorQuantisationStrategyT::LEFT;
	vector<size_t> indexList6;
	auto output6=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters6);
	indexList6.swap(get<1>(output6));
	BOOST_CHECK_EQUAL_COLLECTIONS(indexList6.begin(), indexList6.end(), indexList4r.begin(), indexList4r.end());

	CorrespondenceDecimatorParametersC parameters7(parameters); parameters7.quantisationStrategy=DecimatorQuantisationStrategyT::RIGHT;
	CoordinateCorrespondenceList2DT decimated7;
	vector<size_t> indexList7;
	auto output7=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters7);
	indexList7.swap(get<1>(output7));
	BOOST_CHECK_EQUAL_COLLECTIONS(indexList7.begin(), indexList7.end(), indexList4r.begin(), indexList4r.end());

	//Ambiguity threshold
	CorrespondenceDecimatorParametersC parameters8(parameters); parameters8.maxAmbiguity=0.2;
	vector<size_t> indexList8;
	auto output8=CorrespondenceDecimatorT::Run(correspondences, binSize1a, binSize2a, parameters8);
	indexList8.swap(get<1>(output8));
	BOOST_CHECK_EQUAL(indexList8.size(),1);
	BOOST_CHECK_EQUAL(indexList8[0], 2);

	//Bin size
	CorrespondenceDecimatorT::dimension_type1 binSize9a;
	CorrespondenceDecimatorT::dimension_type2 binSize9b;
	tie(binSize9a, binSize9b)=CorrespondenceDecimatorT::ComputeBinSize(decimated1, 4, 4);
	BOOST_CHECK(binSize9a.isApprox( CorrespondenceDecimatorT::dimension_type1( 0.48766, 0.366079), 1e-6  ) );
	BOOST_CHECK(binSize9b.isApprox( CorrespondenceDecimatorT::dimension_type2( 0.0105141, 0.122275), 1e-6  ) );
}	//BOOST_AUTO_TEST_CASE(Correspondence_Decimation)

BOOST_AUTO_TEST_SUITE_END() //Correspondences

BOOST_AUTO_TEST_SUITE(Geometric_Entities)

BOOST_AUTO_TEST_CASE(Epipolar_Geometry)
{
	EpipolarMatrixT mF; mF.setRandom();
	unsigned int dummy;
	tie(dummy, mF)=MakeRankN<Eigen::NoQRPreconditioner>(mF, 2);
	mF.normalize();

	IntrinsicCalibrationMatrixT mK1; mK1<<1000, 0, 100, 0, 1010, 125, 0, 0, 1;
	IntrinsicCalibrationMatrixT mK2; mK2<<1100, 0, 80, 0, 1090, 130, 0, 0, 1;

	//FundamentalToEseential
	EpipolarMatrixT mEF=FundamentalToEssential(mF, mK1, mK2);
	BOOST_CHECK_CLOSE(mEF.norm(),1, 1e-6);

	//EssentialToFundamental
	EpipolarMatrixT mFE=EssentialToFundamental(mEF, mK1, mK2);
	BOOST_CHECK_CLOSE(mFE.norm(),1, 1e-6);
	BOOST_CHECK(mFE.isApprox(mF, 1e-6));

	//ComposeEssential
	Coordinate3DT vt2(1,2,3);
	QuaternionT q2(0.73030, 0.18257, 0.36515, 0.54772); q2.normalize();
	RotationMatrix3DT mR2=q2.toRotationMatrix();

	EpipolarMatrixT mE2d=ComposeEssential(vt2, mR2, false);
	EpipolarMatrixT mE2=ComposeEssential(vt2, mR2);

	EpipolarMatrixT mE2r=MakeCrossProductMatrix(vt2)*mR2;
	BOOST_CHECK(mE2d.isApprox(mE2r, 1e-6));

	mE2r/=mE2r.norm();
	BOOST_CHECK(mE2.isApprox(mE2r, 1e-6));

	//DecomposeEssential

	CoordinateCorrespondenceList2DT corr;
	corr.push_back(CoordinateCorrespondenceList2DT::value_type( Coordinate2DT(1,2), Coordinate2DT(-0.80123, 0.66667)) );	//Choice of this pair affects the results
	optional<tuple<Coordinate3DT, RotationMatrix3DT> > relativeCal=DecomposeEssential(mE2, corr, 1e-8);
	BOOST_CHECK(relativeCal);
	BOOST_CHECK(mE2r.isApprox(ComposeEssential(get<0>(*relativeCal),get<1>(*relativeCal)), 1e-6) || mE2r.isApprox(-ComposeEssential(get<0>(*relativeCal),get<1>(*relativeCal)), 1e-6) );

	EpipolarMatrixT mBadE; mBadE.setZero();
	BOOST_CHECK(!DecomposeEssential(mBadE, corr, 1e-8));
}	//BOOST_AUTO_TEST_CASE(Epipolar_Geometry)

BOOST_AUTO_TEST_CASE(Homography_2D)
{
	//DecomposeK2RK1i

	IntrinsicCalibrationMatrixT mK1; mK1<<1000, 0, 0, 0, 1000, 0, 0, 0, 1;
	RotationMatrix3DT mR1=QuaternionT(0.605435, 0.343515, 0.343742, 0.630307).matrix();

	IntrinsicCalibrationMatrixT mK2; mK2<<1250, 0, 0, 0, 1250, 0, 0, 0, 1;
	RotationMatrix3DT mR2=QuaternionT(0.543434, 0.395568, 0.426369, 0.605323).matrix();

	//No noise
	Homography2DT mH=mK2*mR2*mR1.transpose()*mK1.inverse();

	typedef ValueTypeM<Homography2DT>::type RealT;
	RotationMatrix3DT mR3;
	RealT f1;
	RealT f2;
	tie(f1, f2, mR3)=*DecomposeK2RK1i(mH);

	BOOST_CHECK_CLOSE(f1, mK1(0,0), 1e-3);
	BOOST_CHECK_CLOSE(f2, mK2(0,0), 1e-3);
	BOOST_CHECK(mR3.isApprox(mR2*mR1.transpose(), 1e-4));

	//Noisy homography
	Homography2DT mHn= mH + (1e-5)*Homography2DT::Random();
	RotationMatrix3DT mR3n;
	RealT f1n;
	RealT f2n;
	tie(f1n, f2n, mR3n)=*DecomposeK2RK1i(mHn);

	BOOST_CHECK_CLOSE(f1n, mK1(0,0), 5);
	BOOST_CHECK_CLOSE(f2n, mK2(0,0), 5);
	BOOST_CHECK_CLOSE(f2n/f1n, mK2(0,0)/mK1(0,0), 5e-1);
	BOOST_CHECK(mR3n.isApprox(mR2*mR1.transpose(), 1e-2));

	//Failure
	BOOST_CHECK(!DecomposeK2RK1i(Homography2DT::Zero()));

}	//BOOST_AUTO_TEST_CASE(Homography_2D)

BOOST_AUTO_TEST_CASE(Homography_3D)
{
	typedef ValueTypeM<Homography3DT>::type RealT;

	QuaternionT q(0.73030, 0.18257, 0.36515, 0.54772); q.normalize();

	Homography3DT mH1=Homography3DT::Identity();
	mH1.block(0,0,3,3)=2*q.matrix();
	mH1.col(3).segment(0,3)<<2, 1, 3;

	RealT scale1;
	Coordinate3DT vC1;
	RotationMatrix3DT mR1;
	tie(scale1, vC1, mR1)=DecomposeSimilarity3D(mH1);

	BOOST_CHECK(mR1.isApprox(q.matrix(), 1e-6));
	BOOST_CHECK(vC1.isApprox(-mH1.block(0,0,3,3).inverse() * mH1.col(3).segment(0,3)));
	BOOST_CHECK_CLOSE(scale1, 2.0, 1e-8);

	Homography3DT mH2=ComposeSimilarity3D(scale1, vC1, mR1);
	BOOST_CHECK(mH1.isApprox(mH2, 1e-6));

	//Jacobian
	Matrix<RealT, 16, 13> dHdsCR=JacobiandHdsCR(mH1);
	Matrix<RealT, 16, 13> dHdsCRr;
	dHdsCRr<<0.133338,0,0,0,2,0,0,0,0,0,0,0,0
			,-0.666668,0,0,0,0,2,0,0,0,0,0,0,0
			,0.733331,0,0,0,0,0,2,0,0,0,0,0,0
			,1,-0.266676,1.33334,-1.46666,0.199976,0.999989,3.6,-0,-0,-0,-0,-0,-0
			,0.933329,0,0,0,0,0,0,2,0,0,0,0,0
			,0.333343,0,0,0,0,0,0,0,2,0,0,0,0
			,0.133338,0,0,0,0,0,0,0,0,2,0,0,0
			,0.5,-1.86666,-0.666686,-0.266676,-0,-0,-0,0.199976,0.999989,3.6,-0,-0,-0
			,-0.333343,0,0,0,0,0,0,0,0,0,2,0,0
			,0.666661,0,0,0,0,0,0,0,0,0,0,2,0
			,0.666668,0,0,0,0,0,0,0,0,0,0,0,2
			,1.5,0.666686,-1.33332,-1.33334,-0,-0,-0,-0,-0,-0,0.199976,0.999989,3.6
			,0,0,0,0,0,0,0,0,0,0,0,0,0
			,0,0,0,0,0,0,0,0,0,0,0,0,0
			,0,0,0,0,0,0,0,0,0,0,0,0,0
			,0,0,0,0,0,0,0,0,0,0,0,0,0;

	BOOST_CHECK(dHdsCRr.isApprox(dHdsCR, 1e-3));
}	//BOOST_AUTO_TEST_CASE(Homography_3D)

BOOST_AUTO_TEST_CASE(Autocalibration_Functions)
{
	Homography3DT mH; mH<<1, 2, 0, 1, 3, 6, 4, 0, 0, 2, 7, 1, 0, 4, 9, 2;
	mH.normalize();

	//MakeDAQ
	Quadric3DT mQ1=MakeDAQ(mH);

	Quadric3DT mQ1gt; mQ1gt=Quadric3DT::Identity(); mQ1gt(3,3)=0;
	mQ1gt = (mH*mQ1gt*mH.transpose());
	mQ1gt.normalize();

	BOOST_CHECK(mQ1.isApprox(mQ1gt, 1e-6));

	optional<Homography3DT> mH1=ExtractRectifyingHomography(mQ1);

	BOOST_CHECK(mH1);
	BOOST_CHECK(mQ1gt.isApprox( MakeDAQ(*mH1), 1e-6));
}	//BOOST_AUTO_TEST_CASE(Autocalibration)

BOOST_AUTO_TEST_SUITE_END()	//Geometric_Entites

BOOST_AUTO_TEST_SUITE(Camera)

BOOST_AUTO_TEST_CASE(Camera_Test)
{
	typedef CameraC::real_type RealT;

	QuaternionT q(0.25, 0.5, 0.75, 0.15); q.normalize();
	Coordinate3DT vC; vC<<1, 2, 1.5;
	RealT f=1000;
	RealT aspectRatio=1.001;
	RealT skewness=1e-5;
	Coordinate2DT principalPoint; principalPoint<<960, 540;
	FrameT frameBox(Coordinate2DT(0,0), Coordinate2DT(1919, 1079));

	RealT distortionCoefficient=1e-8;
	Coordinate2DT distortionCentre=principalPoint;
	LensDistortionCodeT distortionModel=LensDistortionCodeT::DIV;

	//Constructors

	CameraC camera1;
	BOOST_CHECK(!camera1.Extrinsics());
	BOOST_CHECK(!camera1.Intrinsics());
	BOOST_CHECK(!camera1.Distortion());
	BOOST_CHECK(!camera1.FrameBox());
	BOOST_CHECK(!camera1.FNumber());
	BOOST_CHECK(!camera1.PixelSize());
	BOOST_CHECK(!camera1.IsMetric());
	BOOST_CHECK(camera1.Tag().empty());

	ExtrinsicC extrinsics; extrinsics.orientation=q; extrinsics.position=vC;

	IntrinsicC intrinsics;
	intrinsics.focalLength=f;
	intrinsics.aspectRatio=aspectRatio;
	intrinsics.skewness=skewness;
	intrinsics.principalPoint=principalPoint;

	LensDistortionC distortion;
	distortion.kappa=distortionCoefficient;
	distortion.centre=distortionCentre;
	distortion.modelCode=distortionModel;

	CameraC camera2(extrinsics, intrinsics, distortion, frameBox, 5.6, 4e-6, "Camera2");
	BOOST_CHECK(camera2.Extrinsics());
	BOOST_CHECK(*camera2.Extrinsics()->position==vC);
	BOOST_CHECK(camera2.Extrinsics()->orientation->isApprox(q, 1e-8));

	BOOST_CHECK(camera2.Intrinsics());
	BOOST_CHECK(*camera2.Intrinsics()->focalLength==f);
	BOOST_CHECK(camera2.Intrinsics()->aspectRatio==aspectRatio);
	BOOST_CHECK(camera2.Intrinsics()->skewness==skewness);
	BOOST_CHECK(*camera2.Intrinsics()->principalPoint==principalPoint);

	BOOST_CHECK(camera2.Distortion());
	BOOST_CHECK(camera2.Distortion()->kappa==distortionCoefficient);
	BOOST_CHECK(camera2.Distortion()->centre==distortionCentre);
	BOOST_CHECK(camera2.Distortion()->modelCode==distortionModel);

	BOOST_CHECK(camera2.FNumber());
	BOOST_CHECK_EQUAL(*camera2.FNumber(), 5.6);

	BOOST_CHECK(camera2.PixelSize());
	BOOST_CHECK_EQUAL(*camera2.PixelSize(), 4e-6);

	BOOST_CHECK(camera2.FrameBox());
	BOOST_CHECK(camera2.FrameBox()->isApprox(frameBox, 1e-8));

	BOOST_CHECK(camera2.IsMetric());

	BOOST_CHECK(camera2.Tag()=="Camera2");

	//Conversions
	BOOST_CHECK(!camera1.MakeCameraMatrix());
	BOOST_CHECK(!camera1.MakeNormalisedCameraMatrix());
	BOOST_CHECK(!camera1.MakeIntrinsicCalibrationMatrix());
	BOOST_CHECK(!camera1.MakeRotationMatrix());
	BOOST_CHECK(!camera1.MakeDistortion<LensDistortionDivC>());

	optional<CameraMatrixT> mP2=camera2.MakeCameraMatrix();
	BOOST_CHECK(mP2);

	optional<CameraMatrixT> mP2n=camera2.MakeNormalisedCameraMatrix();
	BOOST_CHECK(mP2n);

	optional<IntrinsicCalibrationMatrixT> mK2=camera2.MakeIntrinsicCalibrationMatrix();
	BOOST_CHECK(mK2);

	optional<RotationMatrix3DT> mR2=camera2.MakeRotationMatrix();
	BOOST_CHECK(mR2);

	optional<LensDistortionOP1C> invalidDistortion2=camera2.MakeDistortion<LensDistortionOP1C>();
	BOOST_CHECK(!invalidDistortion2);

	optional<LensDistortionDivC> distortion2=camera2.MakeDistortion<LensDistortionDivC>();
	BOOST_CHECK(distortion2);

	//Constructors, more
	CameraC camera3(*mP2, distortion, frameBox, 5.6, 4e-6);
	BOOST_CHECK(camera3.Extrinsics()->position->isApprox(vC, 1e-6));
	BOOST_CHECK(camera3.Extrinsics()->orientation->isApprox(q, 1e-6));
	BOOST_CHECK_CLOSE( *camera3.Intrinsics()->focalLength, f, 1e-6);
	BOOST_CHECK_CLOSE( camera3.Intrinsics()->aspectRatio, aspectRatio, 1e-6);
	BOOST_CHECK_CLOSE( camera3.Intrinsics()->skewness, skewness, 1e-6);
	BOOST_CHECK( camera3.Intrinsics()->principalPoint->isApprox(principalPoint, 1e-6));

	BOOST_CHECK(camera3.FrameBox());
	BOOST_CHECK(camera3.FrameBox()->isApprox(frameBox, 1e-8));

	BOOST_CHECK(camera3.FNumber());
	BOOST_CHECK_EQUAL(*camera3.FNumber(), 5.6);

	BOOST_CHECK(camera3.PixelSize());
	BOOST_CHECK_EQUAL(*camera3.PixelSize(), 4e-6);

	BOOST_CHECK(camera3.IsMetric());

	//Random camera constructor

	mt19937_64 rng;
	Array<RealT,5,2> intrinsicsRange; intrinsicsRange<<1200,1600,0.95,1.05,0,0,955,965,520,560;
	Array<RealT,3,2> positionRange; positionRange<<-1,1,-1,1,-1,1;
	Array<RealT,3,2> orientationRange; orientationRange<<-1,1,-1,1,-1,1;
	Array<RealT,1,2> distortionRange(-1e-8, 1e-8);

	CameraC cameraRandom(rng, intrinsicsRange, positionRange, orientationRange, distortionRange, LensDistortionCodeT::DIV);

	//Setters

	camera1.Extrinsics()=extrinsics;
	BOOST_CHECK(*camera1.Extrinsics()->position == extrinsics.position);
	BOOST_CHECK(camera1.Extrinsics()->orientation->isApprox(q, 1e-8));

	camera1.Extrinsics()=optional<ExtrinsicC>();
	camera1.SetExtrinsics(vC, *mR2);
	BOOST_CHECK(*camera1.Extrinsics()->position == extrinsics.position);
	BOOST_CHECK(camera1.Extrinsics()->orientation->isApprox(q, 1e-8));

	camera1.Extrinsics()=optional<ExtrinsicC>();
	camera1.SetExtrinsics( mK2->inverse() * (*mP2));
	BOOST_CHECK(camera1.Extrinsics()->position->isApprox(*extrinsics.position, 1e-6));
	BOOST_CHECK(camera1.Extrinsics()->orientation->isApprox(q, 1e-8));

	camera1.Intrinsics()=intrinsics;
	BOOST_CHECK(*camera1.Intrinsics()->focalLength==f);
	BOOST_CHECK(camera1.Intrinsics()->aspectRatio==aspectRatio);
	BOOST_CHECK(camera1.Intrinsics()->skewness==skewness);
	BOOST_CHECK(*camera1.Intrinsics()->principalPoint==principalPoint);

	camera1.Intrinsics()=optional<IntrinsicC>();
	camera1.SetIntrinsics(*mK2);
	BOOST_CHECK(*camera1.Intrinsics()->focalLength==f);
	BOOST_CHECK_CLOSE(camera1.Intrinsics()->aspectRatio, aspectRatio, 1e-8);
	BOOST_CHECK(camera1.Intrinsics()->skewness==skewness);
	BOOST_CHECK(*camera1.Intrinsics()->principalPoint==principalPoint);

	camera1.Distortion()=distortion;
	BOOST_CHECK(camera1.Distortion()->kappa==distortionCoefficient);
	BOOST_CHECK(camera1.Distortion()->centre==distortionCentre);
	BOOST_CHECK(camera1.Distortion()->modelCode==distortionModel);

	camera1.FNumber()=5.6;
	BOOST_CHECK_EQUAL(*camera1.FNumber(), 5.6);

	camera1.PixelSize()=4e-6;
	BOOST_CHECK_EQUAL(*camera1.PixelSize(), 4e-6);

	camera1.FrameBox()=frameBox;
	BOOST_CHECK(camera1.FrameBox()->isApprox(frameBox, 1e-8));

	camera1.Tag()="Camera1";
	BOOST_CHECK(camera1.Tag()=="Camera1");

	//Perturbation
	CameraMatrixT mP4; mP4.setZero(); mP4(0,0)=1; mP4(1,1)=1; mP4(2,2)=1;
	CameraC camera4(mP4, LensDistortionC());

	CameraC camera4p=CameraC::ApplyPerturbation(camera4, extrinsics, intrinsics, distortion);
	camera4p.Intrinsics()->aspectRatio=aspectRatio;
	camera4p.Intrinsics()->focalLength=f;

	CameraMatrixT mP4p=*camera4p.MakeCameraMatrix();

	BOOST_CHECK(mP2->isApprox(mP4p, 1e-6));
	BOOST_CHECK(camera4p.Distortion()->kappa==distortionCoefficient);
	BOOST_CHECK(camera4p.Distortion()->centre==distortionCentre);
}	//BOOST_AUTO_TEST_CASE(Camera_Test)

BOOST_AUTO_TEST_CASE(Free_Functions)
{
	typedef ValueTypeM<Coordinate3DT>::type RealT;

	QuaternionT q(0.25, 0.5, 0.75, 0.15); q.normalize();
	CameraMatrixT mPn; 	mPn.topLeftCorner(3,3)=RotationMatrix3DT().setIdentity();
	mPn.col(3)=-Coordinate3DT(1, 1.2, 1.5);
	mPn= q.matrix()*mPn;

	IntrinsicCalibrationMatrixT mK; mK.setIdentity();
	mK(0,0)=1000; mK(1,1)=1001; mK(0,1)=1e-5; mK(0,2)=960; mK(1,2)=540;

	CameraMatrixT mP= mK*mPn;

	//Composition and decomposition

	RotationMatrix3DT mR1;
	Coordinate3DT vC1;
	IntrinsicCalibrationMatrixT mK1;
	tie(mK1, vC1, mR1)=DecomposeCamera(mP);
	BOOST_CHECK(mK1.isApprox(mK, 1e-6));

	CameraMatrixT mP2=ComposeCameraMatrix(mK1, vC1, mR1);
	BOOST_CHECK(mP2.isApprox(mP, 1e-6));

	CameraMatrixT mP3n=ComposeCameraMatrix(vC1, mR1);
	BOOST_CHECK(mP3n.isApprox(mPn, 1e-6));

	RotationMatrix3DT mR3n;
	Coordinate3DT vC3n;
	IntrinsicCalibrationMatrixT mK3n;
	tie(mK3n, vC3n, mR3n)=DecomposeCamera(mP3n, true);
	BOOST_CHECK(mR3n.isApprox(mR1, 1e-6));
	BOOST_CHECK(vC3n.isApprox(vC1, 1e-6));
	BOOST_CHECK(mK3n.isIdentity());

	CameraMatrixT mP4=ComposeCameraMatrix(mK1, vC1, QuaternionT(mR1));
	BOOST_CHECK(mP4.isApprox(mP, 1e-6));

	//Position->translation
	Coordinate3DT vC5=TranslationToPosition(-mR1*vC1, mR1 );
	BOOST_CHECK(vC5.isApprox(vC1, 1e-6));

	//CameraToFundamental
	EpipolarMatrixT mF6=CameraToFundamental(mP);
	EpipolarMatrixT mF6r; mF6r<<-0.000345843, -0.000844265, 0.000905974, -0.000504698, 0.00114037, -0.00057583, -0.829008, 0.54732, 0.114816;

	BOOST_CHECK_CLOSE(mF6.norm(), 1, 1e-6);
	BOOST_CHECK(mF6r.isApprox(mF6, 1e-6));

	//CameraFromFundamental
	CameraMatrixT mP7=CameraFromFundamental(mF6);
	CameraMatrixT mP7r; mP7r<<   -0.468388, 0.309235, 0.0648707, 0.425179, 0.352477, -0.232709, -0.0488181, 0.564998, -1.91864e-05, 0.000961869, -0.000756704, -0.000521344;
	BOOST_CHECK_CLOSE(mP7.norm(), 1, 1e-6);
	BOOST_CHECK(mP7r.isApprox(-mP7, 1e-6) || mP7r.isApprox(mP7, 1e-6) );

	//CameraToFundamental
	CameraMatrixT mP8=ComposeCameraMatrix(mPn.col(3)*2, q.matrix());
	EpipolarMatrixT mF8=CameraToFundamental(mP7, mP8);
	EpipolarMatrixT mF8r; mF8r<<0.00264146, 0.00418413, 0.730482, -0.00207433, -0.00185986, -0.550262, 0.00132523, 0.00441221, 0.404417;

	BOOST_CHECK_CLOSE(mF8.norm(), 1.0, 1e-6);
	BOOST_CHECK(mF8r.isApprox(-mF8, 1e-6) || mF8r.isApprox(mF8, 1e-6));

	//CameraToHomography
	QuaternionT q9(0.15, 0.15, 0.55, 0.05); q9.normalize();
	IntrinsicCalibrationMatrixT mK9(mK); mK9(0,0)+=100; mK9(1,1)+=125;
	Homography2DT mH9=CameraToHomography(mR1, mK, q9.matrix(), mK9);
	Homography2DT mH9r; mH9r<<0.0012908, -0.00136495, 0.999028, 0.000798126, 0.00126941, -0.0439714, -2.57661e-07, -4.24421e-07, 0.00204742;
	BOOST_CHECK(mH9.isApprox(mH9r, 1e-6));

	Matrix<QuaternionT::Scalar, 3, 3> mI; mI.setIdentity();
	BOOST_CHECK(mI.isApprox(CameraToHomography(mR1, mK, mR1, mK, false), 1e-6));

	//MapToCanonical
	Homography3DT mH10;
	Homography3DT mH10i;
	tie(mH10, mH10i)=MapToCanonical(mP, true);
	CameraMatrixT mPcanonical; mPcanonical<<1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0;
	BOOST_CHECK(mPcanonical.isApprox(mP*mH10, 1e-6));
	BOOST_CHECK(Homography3DT::Identity().isApprox(mH10*mH10i, 1e-6));

	Homography3DT mH11;
	Homography3DT mH11i;
	tie(mH11, mH11i)=MapToCanonical(mP7, false);
	BOOST_CHECK(mPcanonical.isApprox(mP7*mH11, 1e-6));
	BOOST_CHECK(Homography3DT::Identity().isApprox(mH11*mH11i, 1e-6));

	//IntrinsicsFromDAQ
	Matrix< ValueTypeM<CameraMatrixT>::type, 4, 4 > mQ;
	mQ<<  0.747902, 0.0874981, 0.000159887, 0.0654741, 0.0874981, 0.644501, 8.88769e-05,  -0.0256753,   0.000159887, 8.88769e-05, 1.66612e-07, 1.03949e-05, 0.0654741,  -0.0256753, 1.03949e-05,  0.00748391;
	CameraMatrixT mPDAQ; mPDAQ<<0.00030086, 0,  -0.288675, 0, 0, 0.00030086, -0.162314, 0, 0, 0, 0.57735, 0;

	optional<IntrinsicCalibrationMatrixT> mKdaq=IntrinsicsFromDAQ(mQ, mPDAQ);
	IntrinsicCalibrationMatrixT mKdaqGT; mKdaqGT<<0.984325, 0.00365638, 7.36337e-05, 0, 0.986493, -0.00320303, 0, 0, 1;
	BOOST_CHECK(mKdaq->isApprox(mKdaqGT, 1e-6));

	//ComputeFoV
	BOOST_CHECK_EQUAL( ComputeFoV(1920, 1700), 2*atan(0.5*1920/1700) );

	//ComputeFocusField
	double dNear, dFar;
	tie(dNear, dFar)=ComputeFocusField(2, 1700, 3.7, 3.84e-6);	//Approx standard parameters for canon xh g1

	BOOST_CHECK_CLOSE(dNear, 1.201465, 1e-3);
	BOOST_CHECK_CLOSE(dFar, 5.963640, 1e-3);

	double dNear2, dFar2;
	tie(dNear2, dFar2)=ComputeFocusField(2, 1700, 5.6, 3.84e-6);	//Approx standard parameters for canon xh g1
	BOOST_CHECK_CLOSE(dNear2, 0.99704, 1e-3);
	BOOST_CHECK_EQUAL(dFar2, numeric_limits<double>::infinity());

	//2D Lines

	optional<tuple<RealT, RealT> > yaxpb=DecomposeLine2D(MakeLine2D(6,-3,-3));

	BOOST_CHECK(yaxpb);
	BOOST_CHECK_EQUAL(get<0>(*yaxpb),2);
	BOOST_CHECK_EQUAL(get<1>(*yaxpb),-1);

	Line2DT line2=MakeLine2D(6,0,-3);
	BOOST_CHECK(!DecomposeLine2D(line2));

	//Relative pose
	Coordinate3DT vC11(1, 2, 0.5);
	QuaternionT q11(0.25, 0.5, 0.75, 0.15); q11.normalize();
	Viewpoint3DC viewpoint1(vC11, q11);

	Viewpoint3DC viewpoint1b(ComposeCameraMatrix(vC11, q11.matrix()));
	BOOST_CHECK(vC11.isApprox(viewpoint1b.position, 1e-6));
	BOOST_CHECK(q11.matrix().isApprox(viewpoint1b.orientation.matrix(), 1e-6));

	Viewpoint3DC viewpoint1c(ComposeSimilarity3D(1, vC11, q11.matrix()));
	BOOST_CHECK(vC11.isApprox(viewpoint1c.position, 1e-6));
	BOOST_CHECK(q11.matrix().isApprox(viewpoint1c.orientation.matrix(), 1e-6));

	Coordinate3DT vC12(0, 1, 1);
	QuaternionT q12(0.15, 0.35, 0.25, 0.1); q12.normalize();
	Viewpoint3DC viewpoint2(vC12, q12);

	Viewpoint3DC viewpoint12=ComputeRelativePose(viewpoint1, viewpoint2);

	BOOST_CHECK(viewpoint12.position.isApprox( Coordinate3DT(-0.155989,-1.32591,-0.683844), 1e-4 ) );
	BOOST_CHECK( viewpoint12.orientation.isApprox(QuaternionT( 0.939293, 0.113168, -0.107509, -0.305553), 1e-3 ) );

	//Sanitiy check
	Coordinate3DT vC11I(0,0,0);
	QuaternionT q11I(1,0,0,0);
	Viewpoint3DC viewpoint1I2=ComputeRelativePose( Viewpoint3DC(ComposeCameraMatrix(vC11I, q11I.matrix()) ), viewpoint2);

	BOOST_CHECK(viewpoint2.position==viewpoint1I2.position);
	BOOST_CHECK(viewpoint2.orientation.isApprox(viewpoint1I2.orientation, 1e-6));

	Viewpoint3DC viewpointSelf=ComputeRelativePose(viewpoint2, viewpoint2);
	BOOST_CHECK(Coordinate3DT(0,0,0)==viewpointSelf.position);
	BOOST_CHECK(QuaternionT(1,0,0,0).isApprox(viewpointSelf.orientation, 1e-6));

	Coordinate3DT vX(1, 4, 3);
	Coordinate3DT vX11=q11.matrix()*(vX-vC11);
	Coordinate3DT vX12=q12.matrix()*(vX-vC12);
	Coordinate3DT vXRel=viewpoint12.orientation.matrix()*(vX11 - viewpoint12.position);	//Mapping a point in the world frame to the camera frame
	BOOST_CHECK(vX12.isApprox(vXRel, 1e-12));

	Matrix<Viewpoint3DC::real_type, 7, 14> mJRelativePose=JacobianRelativePose(viewpoint1, viewpoint2);
	Matrix<Viewpoint3DC::real_type, 7, 14> mJRelativePoser; mJRelativePoser.setZero();
	mJRelativePoser.row(0)<<0.303621,  -0.752089,  -0.584958,   0.580558,   -2.48057,    0.79167,    1.37223,  -0.303621,   0.752089,   0.584958,          0,          0,          0,          0;
	mJRelativePoser.row(1)<<  -0.91922,  -0.392758,  0.0278552,   -1.37223,   -0.79167,   -2.48057,   0.580558,    0.91922,   0.392758, -0.0278552,          0,          0,          0,          0;
	mJRelativePoser.row(2)<<  0.250696,  -0.529248,   0.810585,    0.79167,   -1.37223,  -0.580558,   -2.48057,  -0.250696,   0.529248,  -0.810585,          0,          0,          0,          0;
	mJRelativePoser.row(3)<<         0,          0,          0,   0.321634,   0.750479,   0.536056,   0.214423,          0,          0,          0,    0.26389,    0.52778,    0.79167,   0.158334;
	mJRelativePoser.row(4)<<         0,          0,          0,   0.750479,  -0.321634,   0.214423,  -0.536056,          0,          0,          0,   -0.52778,    0.26389,  -0.158334,    0.79167;
	mJRelativePoser.row(5)<<         0,          0,          0,   0.536056,  -0.214423,  -0.321634,   0.750479,          0,          0,          0,   -0.79167,   0.158334,    0.26389,   -0.52778;
	mJRelativePoser.row(6)<<         0,          0,          0,   0.214423,   0.536056,  -0.750479,  -0.321634,          0,          0,          0,  -0.158334,   -0.79167,    0.52778,    0.26389;
	BOOST_CHECK(mJRelativePose.isApprox(mJRelativePoser, 1e-5));

	//CameraToEssential
	EpipolarMatrixT mE13=CameraToEssential(vC11, q11.matrix(), vC12, q12.matrix());
	BOOST_CHECK_CLOSE(mE13.norm(), 1, 1e-6);

	EpipolarMatrixT mE13r; mE13r<<-0.378255, -0.445144,  0.244958,
								   0.472675, -0.247243,  -0.23383,
								  -0.144079,  0.487452, 0.0342084;

	BOOST_CHECK(mE13.isApprox(mE13r, 1e-4));

	//CameraFromEssential

	Coordinate3DT vC14=vC1.normalized();		//It is important that the distance is set to 1
	CameraMatrixT mP14r=ComposeCameraMatrix(vC14, mR1);

	Coordinate3DT vX14 = vC14 + 0.5*mR1.row(2).transpose();	//The point must be in front of both cameras
	Coordinate2DT vx14a(vX14(0)/vX14(2), vX14(1)/vX14(2));	//Project to the identity camera
	Coordinate2DT vx14b= (mP14r*vX14.homogeneous()).eval().hnormalized();
	CoordinateCorrespondenceList2DT correspondenceList14;
	correspondenceList14.push_back(CoordinateCorrespondenceList2DT::value_type(vx14a, vx14b));

	EpipolarMatrixT mE14=ComposeEssential(-mR1*vC14, mR1);
	optional<CameraMatrixT> mP14=CameraFromEssential(mE14, correspondenceList14);

	BOOST_CHECK(mP14);	//Valid
	BOOST_CHECK( mP14->isApprox(mP14r, 1e-6) || mP14->isApprox(-mP14r, 1e-6) );
}	//BOOST_AUTO_TEST_CASE(Free_Functions)

BOOST_AUTO_TEST_CASE(Jacobians)
{

	typedef ValueTypeM<CameraMatrixT>::type RealT;

	//JacobianCameraToFundamental
	CameraMatrixT mP1; mP1<< -7.71686e-05, -0.00490818, 0.0233589, 0.999709, -0.00319573, -0.203254, 0.966864, -0.024141, -2.9067e-05, -2.8472e-06, -0.152583, 7.23184e-05;

	Matrix<RealT, 9, 12> mJ1=JacobianCameraToFundamental(mP1, false);
	Matrix<RealT, 9, 12> mJ1r; mJ1r<<0, 0, 0, -7.23184e-05, -0, -0, -0.024141, -0, -0, 0, -2.9067e-05, 0.00319573, 0, 0, 0, 0, -7.23184e-05, -0, -0, -0.024141, -0, 0, -2.8472e-06, 0.203254, 0, 0, 0, -0, -0, -7.23184e-05, -0, -0, -0.024141, 0, -0.152583, -0.966864, 7.23184e-05, 0, 0, 0, 0, 0, -0.999709, -0, -0, 2.9067e-05, 0, -7.71686e-05, 0, 7.23184e-05, 0, 0, 0, 0, -0, -0.999709, -0, 2.8472e-06, 0, -0.00490818, 0, 0, 7.23184e-05, 0, 0, 0, -0, -0, -0.999709, 0.152583, 0, 0.0233589, 0.024141, 0, 0, 0.999709, 0, 0, 0, 0, 0, -0.00319573, 7.71686e-05, 0, 0, 0.024141, 0, 0, 0.999709, 0, 0, 0, 0, -0.203254, 0.00490818, 0, 0, 0, 0.024141, 0, 0, 0.999709, 0, 0, 0, 0.966864, -0.0233589, 0;
	BOOST_CHECK(mJ1.isApprox(mJ1r, 1e-3));

	//JacobianNormalisedCameraToEssential
	QuaternionT q(0.25, 0.5, 0.75, 0.15); q.normalize();
	Coordinate3DT vC(1, 1.2, 1.5);
	CameraMatrixT mPn=ComposeCameraMatrix(vC, q.matrix());

	Matrix<RealT, 9, 12> mJ2=JacobianNormalisedCameraToEssential(mPn, false);
	Matrix<RealT, 9, 12> mJ2r; mJ2r<< 0, 0, 0, -0.58078,0.300836,0.376045,-0.429526, 1.10306, 1.37883, 0,0.584958,-0.752089,0, 0, 0,-0.529248,-1.46657,-0.793872,0.392758,-0.877437,0.589136,-0.584958, 0,-0.303621,0, 0, 0,0.810585,0.972702,0.384401,-0.0278552,-0.0334262,-1.39053,0.752089,0.303621, 0,0.58078,-0.300836,-0.376045, 0, 0, 0, 1.77994,0.364345,0.455432, 0,-0.0278552,-0.392758,0.529248, 1.46657,0.793872, 0, 0, 0,-0.752089,0.573816,-1.12813,0.0278552, 0, 0.91922,-0.810585,-0.972702,-0.384401, 0, 0, 0,-0.584958,-0.70195,0.598886,0.392758,-0.91922, 0,0.429526,-1.10306,-1.37883,-1.77994,-0.364345,-0.455432, 0, 0, 0, 0,-0.810585,-0.529248,-0.392758,0.877437,-0.589136,0.752089,-0.573816, 1.12813, 0, 0, 0,0.810585, 0,-0.250696,0.0278552,0.0334262, 1.39053,0.584958, 0.70195,-0.598886, 0, 0, 0,0.529248,0.250696, 0;
	BOOST_CHECK(mJ2.isApprox(mJ2r, 1e-2));

	//JacobiandPdRC

	Matrix<RealT,12,12> dPdCR=JacobiandPdCR(mPn.normalized());
	Matrix<RealT,12,12> dPdCRr;
	dPdCRr<<0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
			0.303621, -0.752089, -0.584958,-1,-1.2,-1.5,-0,-0,-0,-0,-0,-0,
			0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
			-0.91922, -0.392758, 0.0278552,-0,-0,-0,-1,-1.2,-1.5,-0,-0,-0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
			0.250696, -0.529248, 0.810585,-0,-0,-0,-0,-0,-0,-1,-1.2,-1.5;
	dPdCRr*=mPn.norm();
	BOOST_CHECK(dPdCRr.isApprox(dPdCR, 1e-6));

	//JacobiandPdKCR
	IntrinsicCalibrationMatrixT mK; mK<<1000, 5e-5, 960, 0, 1001, 540, 0, 0, 1;
	Matrix<RealT,12,17> dPdKCR=JacobiandPdKCR( (mK*mPn).normalized());
	Matrix<RealT,12,17> dPdKCRr;
	dPdKCRr<< -0.303621, 0,0.91922,-0.250696, 0, 0, 0, 0, 1000, 0, 0,5e-05, 0, 0,960, 0, 0
			,0.752089, 0, 0.392758, 0.529248, 0, 0, 0, 0, 0, 1000, 0, 0,5e-05, 0, 0,960, 0
			,0.584958, 0, -0.0278552,-0.810585, 0, 0, 0, 0, 0, 0, 1000, 0, 0,5e-05, 0, 0,960
			,-1.47632, 0, -1.34875, 0.831476, 0, 544.29, -1260.17,193.203,-1000,-1200,-1500, -5e-05, -6e-05, -7.5e-05, -960,-1152,-1440
			,0.920139, 919.22, 0, 0,-0.250696, 0, 0, 0, 0, 0, 0, 1001, 0, 0,540, 0, 0
			,0.39315,392.758, 0, 0, 0.529248, 0, 0, 0, 0, 0, 0, 0, 1001, 0, 0,540, 0
			,-0.027883, -27.8552, 0, 0,-0.810585, 0, 0, 0, 0, 0, 0, 0, 0, 1001, 0, 0,540
			,-1.3501, -1348.75, 0, 0, 0.831476, -784.763, -678.944,465.599, 0, 0, 0,-1001,-1201.2,-1501.5, -540, -648, -810
			,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0
			,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0
			,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
			,0, 0, 0, 0, 0, 0.250696,-0.529248, 0.810585, 0, 0, 0, 0, 0, 0,-1, -1.2, -1.5;
	dPdKCRr*=(mK*mPn).norm();
	BOOST_CHECK(dPdKCRr.isApprox(dPdKCR, 1e-6));
}	//BOOST_AUTO_TEST_CASE(Jacobians)

BOOST_AUTO_TEST_SUITE_END()	//Camera

}   //TestElementsN
}   //UnitTestsN
}	//SeeSawN


