/**
 * @file Viewpoint3D.h Public interface for the 3D viewpoint object
 * @author Evren Imre
 * @date 29 Jan 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VIEWPOINT3D_H_6990123
#define VIEWPOINT3D_H_6990123

#include "Viewpoint3D.ipp"
namespace SeeSawN
{
namespace ElementsN
{

struct Viewpoint3DC;	///< A viewpoint in a 3D coordinate frame

Viewpoint3DC ComputeRelativePose(const Viewpoint3DC& reference, const Viewpoint3DC& viewpoint);	///< Computes the pose of a viewpoint within a new reference frame
Matrix<Viewpoint3DC::real_type, 7, 14> JacobianRelativePose(const Viewpoint3DC& reference, const Viewpoint3DC& viewpoint);	///< Jacobian of a relative pose with respect to the absolute pose parameters

}	//ElementsN
}	//SeeSawN

#endif /* VIEWPOINT3D_H_6990123 */
