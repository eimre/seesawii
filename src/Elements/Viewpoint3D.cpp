/**
 * @file Viewpoint3D.cpp Implementation of the 3D viewpoint object
 * @author Evren Imre
 * @date 29 Jan 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Viewpoint3D.h"

namespace SeeSawN
{
namespace ElementsN
{

/********** Viewpoint3DC **********/

/**
 * @brief Constructor
 * @param[in] pposition Position
 * @param[in] oorientation Orientation
 */
Viewpoint3DC::Viewpoint3DC(const Coordinate3DT& pposition, const Quaternion<ValueTypeM<Coordinate3DT>::type>& oorientation) : position(pposition), orientation(oorientation)
{}

/**
 * @brief Construction from a camera matrix
 * @param[in] mP Camera matrix
 * @remarks The intrinsic calibration matrix component is ignored
 */
Viewpoint3DC::Viewpoint3DC(const CameraMatrixT& mP)
{
	RotationMatrix3DT mR;
	tie(ignore, position, mR)=DecomposeCamera(mP);
	orientation=QuaternionT(mR);
}	//Viewpoint3DC(const CameraMatrixT& mP)

/**
 * @brief Construction from a similarity matrix
 * @param[in] mH Similarity transformation
 * @remarks The scale parameter is ignored
 */
Viewpoint3DC::Viewpoint3DC(const Homography3DT& mH)
{
	RotationMatrix3DT mR;
	tie(ignore, position, mR)=DecomposeSimilarity3D(mH);
	orientation=QuaternionT(mR);
}	//Viewpoint3DC::Viewpoint3DC(const Homography3DT& mH)

/********** FREE FUNCTIONS **********/

/**
 * @brief Computes the pose of a viewpoint within a new reference frame
 * @param[in] reference New reference viewpoint in the global reference frame
 * @param[in] viewpoint Viewpoint to be mapped in the global reference frame
 * @return Viewpoint in the reference frame defined by \c reference
 */
Viewpoint3DC ComputeRelativePose(const Viewpoint3DC& reference, const Viewpoint3DC& viewpoint)
{
	//The transformation that takes a point in the reference frame to the frame of the viewpoint
	Viewpoint3DC relative;
	relative.position = reference.orientation.matrix()  * (viewpoint.position - reference.position);
	relative.orientation = viewpoint.orientation * reference.orientation.conjugate();
	return relative;
}	//Viewpoint3DC ComputeRelativePose(const Viewpoint3DC& reference, const Viewpoint3DC& viewpoint)

/**
 * @brief Jacobian of a relative pose with respect to the absolute pose parameters
 * @param reference New reference viewpoint in the global reference frame
 * @param viewpoint Viewpoint to be mapped in the global reference frame
 * @return Jacobian matrix
 * @warning dCdq1 block is infinity if \c reference.orientation.w()=1
 */
Matrix<Viewpoint3DC::real_type, 7, 14> JacobianRelativePose(const Viewpoint3DC& reference, const Viewpoint3DC& viewpoint)
{
	Matrix<Viewpoint3DC::real_type, 7, 14> output; output.setZero();

	RotationMatrix3DT mR1=reference.orientation.matrix();
	output.block(0,0,3,3) =-JacobiandABdB<Matrix<Viewpoint3DC::real_type, 3, 3> >(mR1, reference.position);
	output.block(0,7,3,3) = JacobiandABdB<Matrix<Viewpoint3DC::real_type, 3, 3> >(mR1, viewpoint.position);

	Coordinate3DT deltaC=viewpoint.position - reference.position;
	Matrix<Viewpoint3DC::real_type, 3, 9> dCdR1= JacobiandABdA< Matrix<Viewpoint3DC::real_type, 3, 9> >(mR1, deltaC);
	output.block(0,3,3,4) = dCdR1 * JacobianQuaternionToRotationMatrix(reference.orientation);

	Matrix<QuaternionT::Scalar, 4, 8> mJqaqb=JacobianQuaternionMultiplication(viewpoint.orientation, reference.orientation.conjugate());

	mJqaqb.block(0,4,4,4)*=JacobianQuaternionConjugation();

	output.block(3,3,4,4)=mJqaqb.block(0,4,4,4);
	output.block(3,10,4,4)=mJqaqb.block(0,0,4,4);

	return output;
}	//Matrix<Viewpoint3DC::real_type, 7, 14> JacobianRelativePose(const Viewpoint3DC& reference, const Viewpoint3DC& viewpoint)

}	//Elements
}	//SeeSawN

