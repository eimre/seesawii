/**
 * @file FeatureUtility.ipp Implementation of various utilities for manipulating features
 * @author Evren Imre
 * @date 19 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_UTILITY_IPP_5431356
#define FEATURE_UTILITY_IPP_5431356

#include <boost/concept_check.hpp>
#include <boost/range/functions.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/concepts.hpp>
#include <boost/geometry/geometry.hpp>
#include <Eigen/Dense>
#include <vector>
#include <map>
#include <set>
#include <cstddef>
#include <algorithm>
#include <cmath>
#include <type_traits>
#include <tuple>
#include <limits>
#include <numeric>
#include "Coordinate.h"
#include "Feature.h"
#include "FeatureConcept.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Geometry/CoordinateTransformation.h"

namespace SeeSawN
{
namespace ElementsN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using boost::geometry::model::d2::point_xy;
using boost::geometry::index::rtree;
using boost::geometry::index::dynamic_rstar;
using boost::geometry::index::nearest;
using Eigen::Matrix;
using std::map;
using std::vector;
using std::tuple;
using std::make_tuple;
using std::numeric_limits;
using std::iota;
using std::is_convertible;
using std::size_t;
using std::copy;
using std::max;
using std::fabs;
using std::set;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::SceneFeatureC;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::ElementsN::DecomposeFeatureRangeM;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::RowsM;
using SeeSawN::GeometryN::CoordinateTransformationsC;

namespace DetailN
{
/**
 * @brief Lexical comparison for 2D points
 * @tparam Point2DT
 * @pre \c Point2DT has the [] operator, and has at least 2 elements (unenforced)
 * @remarks Implements the \c less functor 2D arrays
 * @ingroup Elements
 * @internal
 */
template<class Point2DT>
struct LexicalCompare2DC
{
    bool operator()(const Point2DT& point1, const Point2DT& point2) const;    ///< Comparison operator
};  //struct LexicalCompare2DC

/**
 * @brief Lexical comparison for 2D points, specialised for Boost.Geometry
 * @tparam ValueT An arithmetic type
 * @remarks Implements the \c less functor Boost.Geometry cartesian points
 * @ingroup Elements
 * @internal
 */
template<typename ValueT>
struct LexicalCompare2DC<point_xy<ValueT> >
{
    /** @brief Comparison operator */
    //Keep it inline, as otherwise, template instantiation of DecimateFeature2D fails to compile
    bool operator()(const point_xy<ValueT>& point1, const point_xy<ValueT>& point2) const
    {
        return (point1.y()==point2.y()) ? point1.x()<point2.x() : point1.y()<point2.y();
    }
};  //struct LexicalCompare2DC<point_xy<double> >


}   //DetailN

/********** IMPLEMENTATION STARTS HERE **********/

namespace DetailN
{
/**
 * @brief Comparison operator
 * @param[in] point1 First point
 * @param[in] point2 Second point
 * @return If \c point1 is lexicographically smaller than \c point2
 */
template<class Point2DT>
bool LexicalCompare2DC<Point2DT>::operator ()(const Point2DT& point1, const Point2DT& point2) const
{
    return (point1[1]==point2[1]) ? point1[0]<point2[0] : point1[1]<point2[1];
}   //bool operator ()(const Coordinate2DT& point1, const Coordinate2DT& point2)

}   //DetailN

/********** FREE FUNCTIONS **********/

/**
 * @brief Decimates a 2D coordinate set by picking the elements closest to a specified grid
 * @tparam CoordinateRangeT A coordinate range
 * @param[in] features Coordinates to be decimated
 * @param[in] stride Distance between two successive grid points. If <=0, the original set is returned
 * @param[in] upperLeft Upper left corner of the bounding box in which the decimated features reside
 * @param[in] lowerRight Lower right corner of the bounding box in which the decimated features reside
 * @return Indices of the remaining elements
 * @pre \c CoordinateRangeT is a forward range holding elements of type Coordinate2DT
 * @remarks The function constructs a grid as dictated by the bounding box and \c minDistance . Then it finds the points closest to the grid.
 * @remarks If the bounding box area is zero, the output is empty. If \c stride<=0 , no decimation applied- i.e., the bounding box condition is ignored.
 * @remarks Each grid point captures the features in a \c stridexstride square
 * @ingroup Elements
 */
template<class CoordinateRangeT>
vector<size_t> DecimateFeatures2D(const CoordinateRangeT& features, typename ValueTypeM<typename range_value<CoordinateRangeT>::type>::type stride, const Coordinate2DT& upperLeft, const Coordinate2DT& lowerRight)
{
    vector<size_t> output;

    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_convertible< typename range_value<CoordinateRangeT>::type, Coordinate2DT >::value, "DecimateFeatures2D : CoordinateRangeT must hold elements convertible to Coordinate2DT");

    //If the stride is <=0, return the original set
    if(stride<=0)
    {
        output.resize(features.size());
        iota(output.begin(), output.end(),0);
        return output;
    }   //if(minDistance==0)

    //No input, no output
    if(boost::distance(features)==0)
        return output;

    typedef typename ValueTypeM<Coordinate2DT>::type RealT;

    //If the bounding box has an area of 0, return
    RealT upper=upperLeft[1];
    RealT left=upperLeft[0];
    RealT lower=lowerRight[1];
    RealT right=lowerRight[0];

    if(upper>=lower || left>=right)
        return output;

    typedef point_xy<RealT> PointT;    //A 2D point
    map<PointT, size_t, DetailN::LexicalCompare2DC<PointT> > pointIndexMap; //Maps a point to its index

    //Insert points to an r-tree
    size_t nMaxPage=features.size()/32; nMaxPage=max((size_t)4, nMaxPage);
    size_t c=0;
    vector<PointT> tmp(boost::distance(features));
    for(const auto& current : features)
    {
        tmp[c].x(current[0]); tmp[c].y(current[1]);
        //tree.insert(tmp);
        pointIndexMap[tmp[c]]=c;
        ++c;
    }   //for(const auto& current : features)

    rtree<PointT, dynamic_rstar> tree(tmp, dynamic_rstar(nMaxPage));

    //Construct a regular 2D lattice

    RealT spanH=right-left;    //Horizontal span
    unsigned int nPointH=spanH/stride; //Number of points per row

    RealT spanV=lower-upper;    //Vertical span
    unsigned int nPointV=spanV/stride; //Number of points per column

    //Lattice

    vector<PointT> lattice; lattice.reserve(nPointH*nPointV);

    //Shift the grid points to the centres of the partitioning, so that [upperLeft, lowerRight] defines a bounding box on the output
    RealT hDistance=stride/2;
    right-=hDistance;
    lower-=hDistance;
    for(RealT c1=left+hDistance; c1<right; c1+=stride )
        for(RealT c2=upper+hDistance; c2<lower; c2+=stride)
            lattice.emplace_back(  PointT(c1, c2) );

    //For each point on the lattice, find the nearest neighbour. If it is within maxDistance, save
    PointT nearestPoint;
    set<size_t> indices;
    RealT maxDistance=stride/2;

    for(const auto& current : lattice)
    {
        tree.query(nearest(current,1), &nearestPoint ); //Query with composite predicate (i.e. chaining the max distance constraint) is a lot slower

        //If the distance constraint is satisfied
        if( (fabs(current.x()-nearestPoint.x()) <= maxDistance) && (fabs(current.y()-nearestPoint.y()) <= maxDistance) )   //So, in a stridexstride box centered at nearestPoint
            indices.insert(pointIndexMap[nearestPoint]);    //Weed out the duplicate indices
    }   //for(const auto& current : lattice)

    //Output
    output.resize(indices.size());
    copy(indices.cbegin(), indices.cend(), output.begin());

    return output;
}   //vector<size_t> DecimateFeatures2D(const CoordinateRangeT& features, unsigned int minDistance, const Coordinate2DT& upperLeft, const Coordinate2DT& lowerRight)

/**
 * @brief Extracts the coordinate components of a feature list
 * @tparam FeatureRangeT A feature range
 * @param[in] features Feature list
 * @return A vector of coordinates
 * @pre \c FeatureRangeT is a forward range of features
 * @ingroup Elements
 */
template<class FeatureRangeT>
vector< typename DecomposeFeatureRangeM<FeatureRangeT>::coordinate_type > MakeCoordinateVector(const FeatureRangeT& features)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
    BOOST_CONCEPT_ASSERT((FeatureConceptC<typename range_value<FeatureRangeT>::type >));

    typedef typename DecomposeFeatureRangeM<FeatureRangeT>::coordinate_type CoordinateT;

    vector<CoordinateT> output; output.reserve(boost::distance(features));
    for(const auto& current : features)
        output.emplace_back(current.Coordinate());

    return output;
}   //vector<CoordinateT&> MakeCoordinateView(FeatureRangeT& features)

/**
 * @brief Finds the bounding box for a set of 2D coordinates
 * @tparam CoordinateRangeT A range of coordinates
 * @param[in] coordinates Coordinate set
 * @return Upper-left and lower-right coordinates
 * @pre \c CoordinateRangeT is a forward range holding elements of type \c Coordinate2DT
 * @ingroup Elements
 */
template<class CoordinateRangeT>
tuple<Coordinate2DT, Coordinate2DT> ComputeBoundingBox2D(const CoordinateRangeT& coordinates)
{
    //Preconditions
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRangeT>));
    static_assert(is_convertible< typename range_value<CoordinateRangeT>::type, Coordinate2DT >::value, "ComputeBoundingBox2D : CoordinateRangeT must hold elements convertible to Coordinate2DT");

    typedef typename ValueTypeM<Coordinate2DT>::type RealT;

    RealT upper=numeric_limits<RealT>::max();
    RealT left=numeric_limits<RealT>::max();
    RealT lower=numeric_limits<RealT>::min();
    RealT right=numeric_limits<RealT>::min();

    for(const auto& current : coordinates)
    {
        if(current[1]<upper)
            upper=current[1];
        else if(current[1]>lower)
            lower=current[1];

        if(current[0]<left)
            left=current[0];
        else if(current[0]>right)
            right=current[0];
    }   //for(const auto& current : coordinates)

    return make_tuple(Coordinate2DT(left, upper), Coordinate2DT(right, lower));
}   //tuple<Coordinate2DT, Coordinate2DT> ComputeBoundingBox2D(const CoordinateRangeT& coordinates)

/**
 * @brief Applies a homogeneous transformation to the coordinate components of a range of features
 * @tparam FeatureRangeT A range of features
 * @tparam MatrixT A matrix
 * @param[in,out] featureList Feature list. At the output, coordinate components are transformed
 * @param[in] mT Transformation matrix
 * @pre \c FeatureRangeT is a forward range of features
 */
template<class FeatureRangeT, class MatrixT>
void ApplyFeatureCoordinateTransformation(FeatureRangeT& featureList, const MatrixT& mT)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<typename range_value<FeatureRangeT>::type>));

	typedef typename DecomposeFeatureRangeM<FeatureRangeT>::coordinate_type CoordinateT;

	typedef CoordinateTransformationsC< typename ValueTypeM<CoordinateT>::type, RowsM<CoordinateT>::value> CoordinateTransformerT;
	typename CoordinateTransformerT::projective_transform_type transformation; transformation.matrix()=mT;
	for(auto& current : featureList)
		current.Coordinate()=CoordinateTransformerT::TransformCoordinate(current.Coordinate(), transformation);
}	//ApplyFeatureCoordinateTransformation(const FeatureRangeT& featureList, const MatrixT& mT)

/**
 * @brief Returns the indices of the features, whose coordinates are contained in a specified box
 * @param[in] featureList Features
 * @param[in] box Box
 * @return Indices of the features, whose coordinates are within \c box
 * @pre \c FeatureRangeT is a forward range, holding elements satisfying \c FeatureConceptC
 * @pre \c BoxT is an Eigen axis-aligned box (unenforced)
 * @pre \c FeatureRangeT and \c BoxT hold the same coordinate types (unenforced)
 */
template<class FeatureRangeT, class BoxT>
vector<size_t> FindContained(const FeatureRangeT& featureList, const BoxT& box)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<FeatureRangeT>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<typename range_value<FeatureRangeT>::type>));

	size_t nElements=boost::distance(featureList);
	vector<size_t> output; output.reserve(nElements);
	size_t c=0;
	for(const auto& current : featureList)
	{
		if(box.contains(current.Coordinate())) output.push_back(c);
		++c;
	}	//for(const auto& current : featureList)

	output.shrink_to_fit();
	return output;
}	//vector<size_t> FindContained(const FeatureRangeT& featureList, const BoxT& box)

}   //ElementsN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class boost::geometry::model::d2::point_xy< typename SeeSawN::WrappersN::ValueTypeM<SeeSawN::ElementsN::Coordinate2DT>::type>;
extern template class std::set<std::size_t>;
extern template class std::vector<std::size_t>;

#endif /* FEATURE_UTILITY_IPP_5431356 */
