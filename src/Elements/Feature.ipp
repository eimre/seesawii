/**
 * @file Feature.ipp Implementation of geometric features
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_IPP_41321592
#define FEATURE_IPP_41321592

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <vector>
#include <ostream>
#include <cstddef>
#include <map>
#include <set>
#include <list>
#include <iterator>
#include <climits>
#include <queue>
#include <utility>
#include "Coordinate.h"
#include "Descriptor.h"
#include "RoI.h"
#include "FeatureConcept.h"
#include "../DataStructures/TaggedData.h"
#include "../Wrappers/EigenMetafunction.h"

#include <iostream>

namespace SeeSawN
{
namespace ElementsN
{

using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::to_block_range;
using boost::dynamic_bitset;
using std::vector;
using std::map;
using std::set;
using std::list;
using std::ostream;
using std::size_t;
using std::back_inserter;
using std::next;
using std::pair;
using std::priority_queue;
using SeeSawN::DataStructuresN::TaggedDataC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ImageDescriptorT;
using SeeSawN::ElementsN::BinaryImageDescriptorT;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::WrappersN::ValueTypeM;

/** @ingroup Elements */ //@{
typedef TaggedDataC<unsigned int, ImageDescriptorT> TaggedDescriptorT; ///< An image descriptor with the id of the source image.[Image id;data]
typedef TaggedDataC<unsigned int, ImageDescriptorT> TaggedRoIT; ///< An image RoI tagged with the id of the source image.[Image id;data]
//@}

template<typename TimestampT, class FeatureT> using FeatureTrajectoryT=map<TimestampT, FeatureT>;	///< Type for feature trajectories

/**
 * @brief Base class for geometric features
 * @tparam DerivedT Derived class for CRTP
 * @tparam CoordinateT A coordinate
 * @tparam DescriptorT A descriptor
 * @tparam RoIT A structure that represents a region of interest
 * @ingroup Elements
 * @nosubgrouping
 */
//CRTP allows the use of members from DerivedT: easier extension
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
class FeatureC
{
    private:

        /** @name Data */ //@{
        CoordinateT coordinate; ///< Feature coordinate
        DescriptorT descriptor; ///< Feature descriptor
        RoIT roi; ///< Region-of-interest described by the feature
        //@}

    public:

        /** @name FeatureConceptC interface */ //@{

        typedef CoordinateT coordinate_type;    ///< Coordinate type
        typedef RoIT roi_type;    ///< RoI type
        typedef DescriptorT descriptor_type;    ///< Descriptor type

        FeatureC(); ///< Default constructor
        FeatureC(const CoordinateT& ccoordinate, const DescriptorT& ddescriptor, const RoIT& rroi);   ///< Constructor

        CoordinateT& Coordinate();  ///< Accessor for \c coordinate
        const CoordinateT& Coordinate() const;  ///< Accessor for \c coordinate

        DescriptorT& Descriptor();  ///< Accessor for \c descriptor
        const DescriptorT& Descriptor() const;  ///< Accessor for \c descriptor

        RoIT& RoI();  ///< Accessor for \c roi
        const RoIT& RoI() const;  ///< Accessor for \c roi
        //@}
};  //FeatureC

/**
 * @brief Represents an image feature
 * @remarks 2D point, XD descriptor, XD roi
 * @ingroup Elements
 * @nosubgrouping
 */
class ImageFeatureC : public FeatureC<ImageFeatureC, Coordinate2DT, ImageDescriptorT, ImageRoIT>
{
    private:

        typedef FeatureC<ImageFeatureC, Coordinate2DT, ImageDescriptorT, ImageRoIT> BaseT;  ///< Base type

    public:

        /** @name FeatureConceptC interface */ //@{
        ImageFeatureC();    ///< Default constructor
        ImageFeatureC(const typename BaseT::coordinate_type& ccoordinate, const typename BaseT::descriptor_type& ddescriptor, const typename BaseT::roi_type& roi);   ///< Constructor

        void Swap(ImageFeatureC& src);  ///< Swaps the contents of two image features
        //@}

        friend ostream& operator<<(ostream& str, const ImageFeatureC& feature); ///< Output stream operator
};  //class ImageFeatureC : FeatureC<Coordinate2DT, VectorXd, VectorXd>

/**
 * @brief Represents a binary image feature
 * @remarks 2D point, XD descriptor, XD roi
 * @ingroup Elements
 * @nosubgrouping
 */
class BinaryImageFeatureC : public FeatureC<ImageFeatureC, Coordinate2DT, BinaryImageDescriptorT, ImageRoIT>
{
	private:
		typedef FeatureC<ImageFeatureC, Coordinate2DT, BinaryImageDescriptorT, ImageRoIT> BaseT;  ///< Base type

	public:

        /** @name FeatureConceptC interface */ //@{
		BinaryImageFeatureC();    ///< Default constructor
		BinaryImageFeatureC(const typename BaseT::coordinate_type& ccoordinate, const typename BaseT::descriptor_type& ddescriptor, const typename BaseT::roi_type& roi);   ///< Constructor

        void Swap(BinaryImageFeatureC& src);  ///< Swaps the contents of two image features
        //@}

        friend ostream& operator<<(ostream& str, const BinaryImageFeatureC& feature); ///< Output stream operator
};	//class ImageFeatureC : public FeatureC<ImageFeatureC, Coordinate2DT, BinaryImageDescriptorT, ImageRoIT>

/**
 * @brief Represents a scene feature
 * @remarks 3D point, list of view ids, image feature at each view
 * @ingroup Elements
 * @nosubgrouping
 */
class SceneFeatureC : public FeatureC<SceneFeatureC, Coordinate3DT, FeatureTrajectoryT<unsigned int, ImageFeatureC>, set<unsigned int> >
{
    private:
        typedef FeatureC<SceneFeatureC, Coordinate3DT, FeatureTrajectoryT<unsigned int, ImageFeatureC>, set<unsigned int> > BaseT;  ///< Base type

    public:

        /** @name FeatureConceptC interface */ //@{
        SceneFeatureC();    ///< Default constructor
        SceneFeatureC(const typename BaseT::coordinate_type& ccoordinate, const typename BaseT::descriptor_type & ddescriptor, const BaseT::roi_type& roi);   ///< Constructor

        void Swap(SceneFeatureC& src);  ///< Swaps the contents of two scene features
        //@}

        friend ostream& operator<<(ostream& str, const SceneFeatureC& feature); ///< Output stream operator

        /** @name Utility */ //@{
        void NormaliseDescriptor();	///< Normalises the image descriptors associated with the feature
        void MakeCompact();	///< Summarises a scene feature with its most representative image descriptor
        //@}
};  //class ImageFeatureC : FeatureC<Coordinate2DT, VectorXd, VectorXd>

/**
 * @brief Represents an oriented scene feature, with binary image descriptors
 * @remarks 3D point, direction, 2D appearance descriptor
 * @remarks Direction is encoded as [azimuth, inclination]
 * @ingroup Elements
 * @nosubgrouping
 */
class OrientedBinarySceneFeatureC : public FeatureC<OrientedBinarySceneFeatureC, Coordinate3DT, FeatureTrajectoryT<unsigned int, BinaryImageFeatureC>, set<unsigned int> >
{
	private:
		typedef FeatureC<OrientedBinarySceneFeatureC, Coordinate3DT, FeatureTrajectoryT<unsigned int, BinaryImageFeatureC>, set<unsigned int> > BaseT;	///< Base type

	public:

		/** @name FeatureConceptC interface */ //@{
		OrientedBinarySceneFeatureC();    ///< Default constructor
		OrientedBinarySceneFeatureC(const typename BaseT::coordinate_type& ccoordinate, const typename BaseT::descriptor_type & ddescriptor, const BaseT::roi_type& roi);   ///< Constructor

		void Swap(OrientedBinarySceneFeatureC& src);  ///< Swaps the contents of two scene features
		//@}

		friend ostream& operator<<(ostream& str, const OrientedBinarySceneFeatureC& feature); ///< Output stream operator

		/** Utility */ //@{
		void MakeCompact();	///< Summarises a scene feature with its most representative image descriptor
		//@}
};	//class OrientedBinarySceneFeatureC

/**
 * @brief A metafunction to expose the types associated with a feature in a range
 * @tparam FeatureRangeT A feature range
 * @pre \c FeatureRangeT is a model of SinglePassRangeConcept, and its elements are models of FeatureConceptC
 * @ingroup Elements
 * @nosubgrouping
 */
template<class FeatureRangeT>
struct DecomposeFeatureRangeM
{
    //@cond CONCEPT_CHECK
    BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<FeatureRangeT>));
    BOOST_CONCEPT_ASSERT((FeatureConceptC< typename range_value<FeatureRangeT>::type >));
    //@endcond

    typedef typename range_value<FeatureRangeT>::type feature_type;    ///< Feature type
    typedef typename feature_type::coordinate_type coordinate_type; ///< Coordinate type
    typedef typename feature_type::roi_type roi_type;   ///< RoI type
    typedef typename feature_type::descriptor_type descriptor_type; ///< Descriptor type
};  //struct DecomposeFeatureRangeM

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Default constructor
 * @post Creates an invalid feature
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::FeatureC()
{}

/**
 * @brief Constructor
 * @param[in] ccoordinate Feature coordinate
 * @param[in] ddescriptor Feature descriptor
 * @param[in] rroi Region represented by the feature
 * @post The object is initalised
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::FeatureC(const CoordinateT& ccoordinate, const DescriptorT& ddescriptor, const RoIT& rroi) : coordinate(ccoordinate), descriptor(ddescriptor), roi(rroi)
{}

/**
 * @brief Accessor for \c coordinate
 * @return A reference to \c coordinate
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
auto FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::Coordinate()->CoordinateT&
{
    return coordinate;
}   //auto Coordinate()->CoordinateT&

/**
 * @brief Accessor for \c coordinate
 * @return A constant reference to \c coordinate
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
auto FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::Coordinate() const-> const CoordinateT&
{
    return coordinate;
}   //auto Coordinate() const -> const CoordinateT&

/**
 * @brief Accessor for \c descriptor
 * @return A reference to \c descriptor
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
auto FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::Descriptor()->DescriptorT&
{
    return descriptor;
}   //auto Descriptor()->DescriptorT&

/**
 * @brief Accessor for \c descriptor
 * @return A constant reference to \c descriptor
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
auto FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::Descriptor() const-> const DescriptorT&
{
    return descriptor;
}   //auto Descriptor() const -> const DescriptorT&

/**
 * @brief Accessor for \c roi
 * @return A reference to \c roi
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
auto FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::RoI()->RoIT&
{
    return roi;
}   //auto RoI()->RoIT&

/**
 * @brief Accessor for \c roi
 * @return A constant reference to \c roi
 */
template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT>
auto FeatureC<DerivedT, CoordinateT, DescriptorT, RoIT>::RoI() const-> const RoIT&
{
    return roi;
}   //auto RoI() const -> const RoIT&

/********** FREE FUNCTIONS **********/

/**
 * @brief Writes an image feature trajectory to a stream
 * @tparam TimeStampT Time stamp type
 * @param[in,out] str Output stream
 * @param[in] trajectory Trajectory
 * @return Modified stream
 * @ingroup IO
 * @remarks No dedicated tests
 */
template<typename TimeStampT>
ostream& operator<<(ostream& str, const FeatureTrajectoryT<TimeStampT, ImageFeatureC>& trajectory)
{
	//Track length
	str<<trajectory.size()<<"\n";

	//Measurements
	for(const auto& current : trajectory)
		str<<current.first<<" "<<current.second;

	return str;
}	//ostream& operator<<(ostream& str, const FeatureTrajectoryT<TimeStampT, ImageFeatureC>)


}   //ElementsN
}	//SeeSawN

#endif /* FEATURE_IPP_41321592 */
