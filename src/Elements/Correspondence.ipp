/**
 * @file Correspondence.ipp Details and implementation of the correspondence-related structures
 * @author Evren Imre
 * @date 14 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CORRESPONDENCE_IPP_4612856
#define CORRESPONDENCE_IPP_4612856

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <cmath>
#include <iterator>
#include <vector>
#include <map>
#include <cstddef>
#include <tuple>
#include <utility>
#include <functional>
#include <type_traits>
#include "FeatureConcept.h"
#include "../Wrappers/EigenMetafunction.h"

namespace SeeSawN
{
namespace ElementsN
{

using boost::ForwardRangeConcept;
using boost::SinglePassRangeConcept;
using boost::range_value;
using boost::bimaps::bimap;
using boost::bimaps::list_of;
using boost::bimaps::left_based;
using boost::bimaps::list_of_relation;
using boost::bimaps::with_info;
using Eigen::Matrix;
using std::floor;
using std::next;
using std::vector;
using std::size_t;
using std::is_same;
using std::tuple;
using std::get;
using std::multimap;
using std::make_pair;
using std::function;
using std::is_arithmetic;
using std::advance;
using SeeSawN::ElementsN::FeatureConceptC;
using SeeSawN::WrappersN::RowsM;

class MatchStrengthC;	///< Metrics for the strength of a correspondence
typedef bimap<list_of<size_t>, list_of<size_t>, left_based, with_info<MatchStrengthC> > CorrespondenceListT;

/**
 * @brief Metrics for the strength of a correspondence
 * @remarks Similarity indicates the affinity of the pair. Ambiguity indicates whether there are other candidates with a similar affinity.
 * An ideal match has high similarity and low ambiguity. However, a low ambiguty does not always imply high quality: sometimes there are just no other candidates (guided matching).
 * @ingroup Elements
 * @nosubgrouping
 */
class MatchStrengthC
{
	private:

		/** @name Data */ //@{
		double similarity;	///< Similarity between the members
		double ambiguity;	///< Ambiguity of the match
		//@}

	public:

		/** @name Constructors */ //@{
		explicit MatchStrengthC(double ssimilarity=0, double aambiguity=0);	///< Constructor
		//@}

		/** @name Accessors */ //@{
		double& Similarity();	///< Returns a reference to similarity
		double& Ambiguity();	///< Returns a reference to ambiguity
		const double& Similarity() const;	///< Returns a constant reference to similarity
		const double& Ambiguity() const;	///< Returns a constant reference to ambiguity
		//@}
};	//struct StrengthT

/********** FREE FUNCTIONS **********/
/**
 * @brief Converts a list of corresponding indices to a list of corresponding coordinates
 * @tparam CoordinateCorrespondenceContainerT A bimap of corresponding coordinates
 * @tparam CorrespondenceRangeT A correspondence range
 * @tparam CoordinateRange1T A coordinate range
 * @tparam CoordinateRange2T A coordinate range
 * @param[in] indices Corresponding indices
 * @param[in] set1 First coordinate set
 * @param[in] set2 Second coordinate set
 * @return A bimap containing the corresponding coordinates
 * @pre \c CorrespondenceRangeT is one of the index correspondence structures in Correspondence.h (unenforced)
 * @pre \c CoordinateCorrespondenceContainerT is one of the coordinate correspondence structures Correspondence.h (unenforced)
 * @pre \c CoordinateRange1T is a forward range compatible with \c CoordinateCorrespondenceContainerT (unenforced)
 * @pre \c CoordinateRange2T is a forward range compatible with \c CoordinateCorrespondenceContainerT (unenforced)
 * @pre For each element in \c indices , there is a corresponding element in \c set1 and \c set2 (unenforced)
 * @remarks Inefficient unless \c CoordinateRange1T and \c CoordinateRange2T are random access ranges
 * @ingroup Elements
 */
template<class CoordinateCorrespondenceContainerT, class CorrespondenceRangeT, class CoordinateRange1T, class CoordinateRange2T>
CoordinateCorrespondenceContainerT MakeCoordinateCorrespondenceList(const CorrespondenceRangeT& indices, const CoordinateRange1T& set1, const CoordinateRange2T& set2)
{
    //Precondition
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRange1T>));
    BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CoordinateRange2T>));

    CoordinateCorrespondenceContainerT output;
    typedef typename range_value<CoordinateCorrespondenceContainerT>::type CorrespondenceT;

    auto itB1=boost::const_begin(set1);
    auto itB2=boost::const_begin(set2);
    for(const auto& current : indices)
        output.push_back(CorrespondenceT( *next(itB1, current.left), *next(itB2, current.right), current.info) );

    return output;
}   //CoordinateCorrespondenceContainerT MakeCoordinateCorrespondenceList(const CorrespondenceRangeT& indices, const CoordinateRange1T& set1, const CoordinateRange2T& set2)

/**
 * @brief Purges the non-matching features
 * @tparam CorrespondenceRangeT A range of corresponding indices
 * @tparam Feature1T A feature type
 * @tparam Feature2T A feature type
 * @param[in, out] set1 First feature set. Purged at the output
 * @param[in, out] set2 Second feature set. Purged at the output
 * @param[in] indices Correspondence indices
 * @pre \c CorrespondenceRangeT is one of the index correspondence structures in Correspondence.h (unenforced)
 * @pre \c Feature1T satisfies FeatureConceptC
 * @pre \c Feature2T satisfies FeatureConceptC
 * @ingroup Elements
 */
template<class CorrespondenceRangeT, class Feature1T, class Feature2T>
void MakeFeatureCorrespondenceList(vector<Feature1T>& set1, vector<Feature2T>& set2, const CorrespondenceRangeT& indices)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((FeatureConceptC<Feature1T>));
	BOOST_CONCEPT_ASSERT((FeatureConceptC<Feature2T>));

	size_t nCorrespondence=boost::distance(indices);

	vector<Feature1T> purged1(nCorrespondence);
	vector<Feature2T> purged2(nCorrespondence);

	size_t c=0;
	for(const auto& current : indices)
	{
		purged1[c]=set1[current.left];
		purged2[c]=set2[current.right];
		++c;
	}	//for(const auto& current : indices)

	set1.swap(purged1);
	set2.swap(purged2);
}	//void MakeFeatureCorrespondenceList(FeatureRangeT& set1, FeatureRangeT& set2, const CorrespondenceRangeT& indices)

/**
 * @brief Sorts a correspondence list by ambiguity
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] src Range to be sorted
 * @return A tuple, [Sorted range; original indices]
 * @pre \c CorrespondenceRangeT is compatible with Boost.Bimap interface (unenforced)
 * @pre \c CorrespondenceRangeT::value_type is the same as \c CorrespondenceListT::value_type
 * @ingroup Elements
 */
template<class CorrespondenceRangeT>
tuple<CorrespondenceListT, vector<size_t> > SortByAmbiguity(const CorrespondenceRangeT& src)
{
	//Preconditions
	static_assert(is_same<typename CorrespondenceRangeT::value_type, typename CorrespondenceListT::value_type>::value, "SortByAmbiguity: CorrespondenceRangeT and CorrespondenceListT must have the same value type");

	typedef typename CorrespondenceRangeT::value_type ValueT;
	typedef function<bool(const ValueT&, const ValueT&)> FunctionT;
	FunctionT comparator=[](const ValueT& v1, const ValueT& v2){ return v1.info.Ambiguity()<v2.info.Ambiguity(); };

	multimap<ValueT, size_t, FunctionT > ordered(comparator);

	size_t c=0;
	for(const auto& current : src)
	{
		ordered.emplace(current, c);
		++c;
	}	//for(const auto& current : src)

	tuple<CorrespondenceListT, vector<size_t> > output;
	get<1>(output).reserve(ordered.size());
	for(const auto& current : ordered)
	{
		get<0>(output).push_back(current.first);
		get<1>(output).push_back(current.second);
	}	//for(const auto& current : ordered)

	return output;
}	//tuple<CorrespondenceListT, vector<size_t> > SortByAmbiguity(const CorrespondenceRangeT& src)

/**
 * @brief Ranks a correspondence list by ambiguity
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] src Range to be ranked
 * @return A vector containing the rank of each element wrt/ambiguity. The least ambiguous correspondence has the rank 0
 * @pre \c CorrespondenceRangeT is compatible with Boost.Bimap interface (unenforced)
 * @ingroup Elements
 */
template<class CorrespondenceRangeT>
vector<unsigned int> RankByAmbiguity(const CorrespondenceRangeT& src)
{
	multimap<double, size_t> ordered;
	size_t c=0;
	for(const auto& current : src)
	{
		ordered.emplace(current.info.Ambiguity(), c);
		++c;
	}	//for(const auto& current : src)

	vector<unsigned int> output(ordered.size());
	unsigned int rank=0;
	for(const auto& current : ordered)
	{
		output[current.second]=rank;
		++rank;
	}	//for(const auto& current : ordered)

	return output;
}	//tuple<CorrespondenceListT, vector<size_t> > SortByAmbiguity(const CorrespondenceRangeT& src)

/**
 * @brief Converts a correspondence range to a vector
 * @tparam RealT Floating point type
 * @tparam CorrespondenceRangeT A bimap of correspondences
 * @param[in] src Source range
 * @return A vector composed of the concatenation of corresponding coordinates
 * @pre \c CorrespondenceRangeT is a bimap of coordinates (unenforced)
 * @pre All elements of \c src has the same size
 * @ingroup Utility
 */
template<typename RealT, class CorrespondenceRangeT>
Matrix<RealT, Eigen::Dynamic, 1> CorrespondenceRangeToVector(const CorrespondenceRangeT& src)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<CorrespondenceRangeT>));

	Matrix<RealT, Eigen::Dynamic, 1> output;

	if(boost::empty(src))
		return output;

	//Determine the size of the output vector
	size_t dim1=boost::const_begin(src)->left.rows();
	size_t dim2=boost::const_begin(src)->right.rows();
	output.resize( (dim1+dim2) * boost::distance(src));

	size_t index=0;
	for(const auto& current : src)
	{
		assert(current.left.cols()==dim1 && current.right.cols()==dim2);

		for(size_t c=0; c<dim1; ++c, ++index)
			output[index]=current.left[c];

		for(size_t c=0; c<dim2; ++c, ++index)
			output[index]=current.right[c];
	}	//for(const auto& current : src)

	return output;
}	//Matrix<RealT, Eigen::Dynamic, 1> CorrespondenceRangeToVector(const CorrespondenceRangeT& src)

/**
 * @brief Converts an input range to a correspondence vector
 * @tparam Coordinate1T Type of the first coordinate
 * @tparam Coordinate2T Type of the second coordinate
 * @tparam InputRangeT A range of numbers
 * @param src Range holding the concatenated coordinates
 * @param nDim1 Dimensionality of the first coordinate
 * @param nDim2 Dimensionality of the second coordinate
 * @return A bimap of coordinate correspondences
 * @pre \c InputRangeT is a single pass range of arithmetic values
 * @pre \c Coordinate1T and \c Coordinate2T are Eigen dense objects
 * @remarks If a coordinate type is fixed-size, the corresponding \c nDim parameter is ignored
 * @ingroup Utility
 */
template<class Coordinate1T, class Coordinate2T, class InputRangeT>
bimap<list_of<Coordinate1T>, list_of<Coordinate2T>, left_based> VectorToCorrespondenceRange(const InputRangeT& src, size_t nDim1=RowsM<Coordinate1T>::value, size_t nDim2=RowsM<Coordinate2T>::value)
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((SinglePassRangeConcept<InputRangeT>));
	static_assert( is_arithmetic<typename boost::range_value<InputRangeT>::type>::value, "VectorToCorrespondenceRange : InputRangeT must hold arithmetic values." );

	bimap<list_of<Coordinate1T>, list_of<Coordinate2T>, left_based> output;

	size_t nCorr=floor(boost::distance(src) / (nDim1+nDim2));
	auto itI=boost::const_begin(src);
	Coordinate1T coord1(nDim1);
	Coordinate2T coord2(nDim2);
	for(size_t c=0; c<nCorr; ++c)
	{
		for(size_t c1=0; c1<nDim1; ++c1, advance(itI,1))
			coord1(c1)=*itI;

		for(size_t c1=0; c1<nDim1; ++c1, advance(itI,1))
			coord2(c1)=*itI;

		output.push_back(typename bimap<list_of<Coordinate1T>, list_of<Coordinate2T>, left_based>::value_type(coord1,coord2));
	}	//for(size_t c=0; c<nCorr; ++c)

	return output;
}	//bimap<list_of<Coordinate1T>, list_of<Coordinate2DT>, left_based> VectorToCorrespondenceRange(const InputRangeT& src, size_t nDim1, size_t nDim2)

}   //ElementsN
}	//SeeSawN

#endif /* CORRESPONDENCE_IPP_4612856 */
