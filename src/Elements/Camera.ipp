/**
 * @file Camera.ipp Details for CameraC
 * @author Evren Imre
 * @date 4 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CAMERA_IPP_4612949
#define CAMERA_IPP_4612949

#include <boost/concept_check.hpp>
#include <boost/optional.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <Eigen/SVD>
#include <tuple>
#include <string>
#include <vector>
#include <cstddef>
#include <cmath>
#include <random>
#include <array>
#include "Coordinate.h"
#include "GeometricEntity.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Geometry/LensDistortion.h"
#include "../Geometry/LensDistortionConcept.h"
#include "../Geometry/Rotation.h"
#include "../Numeric/LinearAlgebra.h"
#include "../Numeric/LinearAlgebraJacobian.h"

namespace SeeSawN
{
namespace ElementsN
{

using boost::optional;
using boost::GeneratorConcept;
using boost::math::pow;
using Eigen::Quaternion;
using Eigen::Matrix;
using Eigen::Matrix3d;
using Eigen::Array;
using Eigen::LDLT;
using Eigen::JacobiSVD;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::string;
using std::vector;
using std::size_t;
using std::atan;
using std::fabs;
using std::uniform_real_distribution;
using std::array;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::HCoordinate2DT;
using SeeSawN::ElementsN::HCoordinate3DT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::RotationVectorT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::EpipolarMatrixT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::ComposeEssential;
using SeeSawN::GeometryN::LensDistortionCodeT;
using SeeSawN::GeometryN::LensDistortionConceptC;
using SeeSawN::GeometryN::RotationVectorToAxisAngle;
using SeeSawN::NumericN::RQDecomposition33;
using SeeSawN::NumericN::ComputePseudoInverse;
using SeeSawN::NumericN::JacobianNormalise;
using SeeSawN::NumericN::JacobiandAda;
using SeeSawN::NumericN::JacobiandABdA;
using SeeSawN::NumericN::JacobiandABdB;

//Forward declarations
Coordinate3DT TranslationToPosition(const Coordinate3DT translation, const RotationMatrix3DT& orientation);
CameraMatrixT ComposeCameraMatrix(const Coordinate3DT& position, const RotationMatrix3DT& orientation);

/**
 * @brief Extrinsic calibration parameters
 * @ingroup Elements
 */
struct ExtrinsicC
{
	optional<Coordinate3DT> position;	///< Camera position
	optional<QuaternionT> orientation;	///< Camera orientation
};	//struct ExtrinsicC

/**
 * @brief Intrinsic calibration parameters
 * @ingroup Elements
 */
struct IntrinsicC
{
	typedef ValueTypeM<HCoordinate2DT>::type real_type;	///< Floating point type
	optional<real_type> focalLength;	///< Focal length
	real_type aspectRatio;	///< Aspect ratio
	real_type skewness;	///< Skewness
	optional<Coordinate2DT> principalPoint;	///< Principal point

	/** @brief Default constructor*/
	IntrinsicC() : aspectRatio(1), skewness(0)
	{}
};	//struct IntrinsicC

/**
 * @brief Lens distortion parameters
 * @ingroup Elements
 */
struct LensDistortionC
{
	typedef ValueTypeM<Coordinate2DT>::type real_type;	///< Floating point type
	real_type kappa;	///< Distortion coefficient
	Coordinate2DT centre;	///< Distortion centre
	LensDistortionCodeT modelCode;	///< Lens distortion model

	/** @brief Default constructor*/
	LensDistortionC() : kappa(0), centre(Coordinate2DT(0,0)), modelCode(LensDistortionCodeT::NOD)
	{}
};	//struct LensDistortionC

/**
 * @brief Represents a metric camera
 * @remarks The camera could be partially defined, e.g., only intrinsics might be valid.
 * @ingroup Elements
 * @nosubgrouping
 */
class CameraC
{
	private:

		typedef ValueTypeM<Coordinate3DT>::type RealT;	///< Floating point type

		/**@name Data */ //@{
		optional<ExtrinsicC> extrinsics;	///< Camera extrinsics
		optional<IntrinsicC> intrinsics;	///< Camera intrinsics
		optional<LensDistortionC> distortion;	///< Distortion parameters
		optional<FrameT> frameBox;	///< upper-left and lower-right coordinates of the image
		optional<double> fNumber;	///< F number
		optional<double> pixelSize;	///< Pixel size, in m
		string tag;	///< Camera tag
		//@}

	public:

		typedef RealT real_type;	///< Floating point type

		/** @name Constructors */ //@{
		CameraC();	///< Default constructor
		CameraC(const optional<ExtrinsicC>& eextrinsics, const optional<IntrinsicC>& iintrinsics=optional<IntrinsicC>(), const optional<LensDistortionC>& ddistortion=optional<LensDistortionC>(), const optional<FrameT>& fframeSize=optional<FrameT>(), const optional<double>& ffNumber=optional<double>(), const optional<double>& ppixelSize=optional<double>(), const string& ttag=string());	///< Constructor
		CameraC(const CameraMatrixT& mP, const optional<LensDistortionC>& ddistortion=optional<LensDistortionC>(), const optional<FrameT>& fframeSize=optional<FrameT>(), const optional<double>& ffNumber=optional<double>(), const optional<double>& ppixelSize=optional<double>(),  const string& ttag=string());	///< Constructor
		template<class RNGT> CameraC(RNGT& rng, const Array<real_type,5,2>& intrinsicRange, const Array<real_type,3,2>& positionRange, const Array<real_type,3,2>& orientationRange, const Array<real_type,1,2>& distortionRange, LensDistortionCodeT ddistortionCode);	///< Random camera constructor
		//@}

		/** @name Accessors */ //@{
		const optional<ExtrinsicC>& Extrinsics() const;	///< Returns a constant reference to extrinsics
		optional<ExtrinsicC>& Extrinsics();	///< Returns a reference to extrinsics
		void SetExtrinsics(const CameraMatrixT& mPNormalised);	///< Sets extrinsics from a normalised camera matrix
		void SetExtrinsics(const Coordinate3DT& position, const RotationMatrix3DT& orientation);	///< Sets extrinsics

		const optional<IntrinsicC>& Intrinsics() const;	///< Returns a constant reference to intrinsics
		optional<IntrinsicC>& Intrinsics();	///<Returns a reference to intrinsics
		void SetIntrinsics(const IntrinsicCalibrationMatrixT& mK);	///< Sets intrinsics from an internal calibration matrix

		const optional<LensDistortionC>& Distortion() const;	///< Returns a constant reference to distortion
		optional<LensDistortionC>& Distortion();	///< Returns a reference to distortion

		const optional<FrameT>& FrameBox() const;	///< Returns a constant reference to frameBox
		optional<FrameT>& FrameBox();	///< Returns a reference to frameBox

		const optional<double>& FNumber() const;	///< Returns a constant reference to the fNumber
		optional<double>& FNumber();	///< Returns a reference to fNumber

		const optional<double>& PixelSize() const;	///< Returns a constant reference to pixelSize
		optional<double>& PixelSize();	///< Returns a reference to pixelSize

		bool IsMetric() const;	///< Checks whether the camera is metric or projective

		const string& Tag() const;	///< Returns a constant reference to tag
		string& Tag();	///< Returns a reference to tag
		//@}

		/**@name Utility */ //@{
		optional<CameraMatrixT> MakeCameraMatrix() const;	///< Returns the camera matrix for the camera
		optional<CameraMatrixT> MakeNormalisedCameraMatrix() const;	///< Returns the normalised camera matrix for the camera
		optional<RotationMatrix3DT> MakeRotationMatrix() const;	///< Returns the rotation matrix corresponding to the orientation of the camera
		optional<IntrinsicCalibrationMatrixT> MakeIntrinsicCalibrationMatrix() const;	///< Returns the intrinsic calibration matrix for the camera
		template<class LensDistortionT> optional<LensDistortionT> MakeDistortion();	///< Returns the lens distortion object for the camera

		static CameraC ApplyPerturbation(const CameraC& src, const ExtrinsicC& deltaPose, const IntrinsicC& deltaIntrinsics=IntrinsicC(), const LensDistortionC& deltaDistortion=LensDistortionC());	///< Applies a perturbation to a camera
		//@}
};	//class CameraC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Random camera constructor
 * @tparam RNGT Random number generator
 * @param[in,out] rng Random number generator
 * @param[in] intrinsicRange Intrinsics range. [Focal length, aspect ratio, skewness, principal point(2)]
 * @param[in] positionRange Position range, in m. [X, Y, Z]
 * @param[in] orientationRange Orientation range, in radians. [X, Y, Z]
 * @param[in] distortionRange Distortion range. If \c distortionCode=NOD , ignored
 * @param[in] ddistortionCode Distortion code
 * @pre \c RNGT is a c++ pseudo-random number generator type (unenforced)
 * @pre For each range, the elements in the first column are <= to those in the second column
 * @remarks The camera values are drawn from a uniform distribution between the value specified in the input parameters
 * @remarks Focal length and aspect ratio are always set to positive values
 * @remarks Only instantiation is tested
 */
template<class RNGT>
CameraC::CameraC(RNGT& rng, const Array<real_type,5,2>& intrinsicRange, const Array<real_type,3,2>& positionRange, const Array<real_type,3,2>& orientationRange, const Array<real_type,1,2>& distortionRange, LensDistortionCodeT ddistortionCode)
{
	assert( (intrinsicRange.col(0)<=intrinsicRange.col(1)).all() );
	assert( (positionRange.col(0)<=positionRange.col(1)).all() );
	assert( (orientationRange.col(0)<=orientationRange.col(1)).all() );
	assert(distortionRange[0]<=distortionRange[1]);

	typedef uniform_real_distribution<real_type> UrnT;
	UrnT urn;

	intrinsics=IntrinsicC();
	intrinsics->focalLength=fabs(UrnT(intrinsicRange(0,0), intrinsicRange(0,1))(rng));
	intrinsics->aspectRatio=fabs(UrnT(intrinsicRange(1,0), intrinsicRange(1,1))(rng));
	intrinsics->skewness=UrnT(intrinsicRange(2,0), intrinsicRange(2,1))(rng);

	Coordinate2DT principalPoint;
	principalPoint[0]=UrnT(intrinsicRange(3,0), intrinsicRange(3,1))(rng);
	principalPoint[1]=UrnT(intrinsicRange(4,0), intrinsicRange(4,1))(rng);
	intrinsics->principalPoint=principalPoint;

	Coordinate3DT vC;
	for(size_t c=0; c<3; ++c)
		vC[c]=UrnT(positionRange(c,0), positionRange(c,1))(rng);

	RotationVectorT vAA;
	for(size_t c=0; c<3; ++c)
		vAA[c]=UrnT(orientationRange(c,0), orientationRange(c,1))(rng);

	QuaternionT q(RotationVectorToAxisAngle(vAA));

	real_type randomKappa= (ddistortionCode==LensDistortionCodeT::NOD) ? 0 : UrnT(distortionRange[0], distortionRange[1])(rng);

	extrinsics=ExtrinsicC();
	extrinsics->position=vC;
	extrinsics->orientation=q;

	distortion=LensDistortionC();
	distortion->kappa=randomKappa;
	distortion->centre=*intrinsics->principalPoint;
	distortion->modelCode=ddistortionCode;
}	//CameraC::CameraC(RNGT& rng, const array<interval_type,5>& intrinsicRange, const array<interval_type,3>& positionRange, const array<interval_type,3>& orientationRange, const interval_type& distortion, LensDistortionCodeT distortionCode)

/**
 * @brief Returns the lens distortion object for the camera
 * @tparam LensDistortionT Lens distortion type
 * @return If \c LensDistortionT does not match the distortion model employed by the camera, invalid. Else, the corresponding distortion object
 * @pre \c LensDistortionT is a model of LensDistortionConceptC
 */
template<class LensDistortionT>
optional<LensDistortionT> CameraC::MakeDistortion()
{
	//Preconditions
	BOOST_CONCEPT_ASSERT((LensDistortionConceptC<LensDistortionT>));

	if(!distortion || distortion->modelCode!=LensDistortionT::modelCode)
		return optional<LensDistortionT>();

	return LensDistortionT(distortion->centre, distortion->kappa);
};	//optional<LensDistortionT> MakeDistortion()

/********** FREE FUNCTIONS ***********/
/**
 * @brief Computes the canonical camera corresponding to an essential matrix
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] mE Essential matrix
 * @param[in] validators Image correspondences for the decomposition of \c mE
 * @return Second camera of the pair. Invalid if the decomposition fails
 * @post The returned camera matrix has unit norm
 * @ingroup Elements
 */
template<class CorrespondenceRangeT>
optional<CameraMatrixT> CameraFromEssential(const EpipolarMatrixT& mE, const CorrespondenceRangeT& validators)
{
	optional<tuple<Coordinate3DT, RotationMatrix3DT> > decomposed=DecomposeEssential(mE, validators);

	if(!decomposed)
		return optional<CameraMatrixT>();

	Coordinate3DT vt;
	RotationMatrix3DT mR;
	std::tie(vt, mR)=*decomposed;

	return ComposeCameraMatrix(TranslationToPosition(vt, mR), mR);
}	//CameraMatrixT CameraFromEssential(const EpipolarMatrixT& mE)

}	//ElementsN
}	//SeeSawN

#endif /* CAMERA_IPP_ */
