/**
 * @file Camera.h Public interface for CameraC
 * @author Evren Imre
 * @date 4 Nov 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CAMERA_H_8012349
#define CAMERA_H_8012349

#include "Camera.ipp"
namespace SeeSawN
{
namespace ElementsN
{
struct ExtrinsicC;	///< Extrinsic calibration parameters
struct IntrinsicC;	///< Intrinsic calibration parameters
struct LensDistortionC;	///< Lens distortion parameters
class CameraC;	///< Represents a metric camera

/********** FREE FUNCTIONS **********/

tuple<IntrinsicCalibrationMatrixT, Coordinate3DT, RotationMatrix3DT> DecomposeCamera(const CameraMatrixT& mP, bool flagNormalised=false);	///< Decomposes a camera into its constituent elements
CameraMatrixT ComposeCameraMatrix(const IntrinsicCalibrationMatrixT& mK, const Coordinate3DT& position, const RotationMatrix3DT& orientation);	///< Composes a camera matrix
CameraMatrixT ComposeCameraMatrix(const IntrinsicCalibrationMatrixT& mK, const Coordinate3DT& position, const QuaternionT& orientation);	///< Composes a camera matrix
CameraMatrixT ComposeCameraMatrix(const Coordinate3DT& position, const RotationMatrix3DT& orientation);	///< Composes a normalised camera matrix
tuple<Homography3DT,Homography3DT> MapToCanonical(const CameraMatrixT& mP, bool flagAffine);	///< Computes the homography that maps a camera to the canonical camera matrix
optional<IntrinsicCalibrationMatrixT> IntrinsicsFromDAQ(const Matrix<ValueTypeM<CameraMatrixT>::type, 4, 4> & mQ, const CameraMatrixT& mP);	///< Extracts the intrinsic calibration parameters for a camera from the dual absolute quadric

Coordinate3DT TranslationToPosition(const Coordinate3DT translation, const RotationMatrix3DT& orientation);	///< Converts a camera translation to a camera position(i.e., t->C)

double ComputeFoV(double edgeLength, double focalLength);	///< Computes the field-of-view for a camera
tuple<double, double> ComputeFocusField(double focusDistance, double focalLength, double fNumber, double pixelSize);	///< Computes the boundaries within which the object is in-focus

CameraMatrixT CameraFromFundamental(const EpipolarMatrixT& mF);	///< Computes the canonical camera pair corresponding to a fundamental matrix
EpipolarMatrixT CameraToFundamental(const CameraMatrixT& mP1, const CameraMatrixT& mP2);	///< Computes the fundamental matrix corresponding to a camera pair
EpipolarMatrixT CameraToFundamental(const CameraMatrixT& mP2, bool flagNormalise=true);	///< Computes the fundamental matrix corresponding to a canonical camera pair
template<class CorrespondenceRangeT> optional<CameraMatrixT> CameraFromEssential(const EpipolarMatrixT& mE, const CoordinateCorrespondenceList2DT& correspondences);	///< Computes the canonical camera corresponding to an essential matrix
EpipolarMatrixT CameraToEssential(const Coordinate3DT& vC1, const RotationMatrix3DT& mR1, const Coordinate3DT& vC2, const RotationMatrix3DT& mR2, bool flagNormalise=true);	///< Computes the essential matrix corresponding to a camera pair
Homography2DT CameraToHomography(const RotationMatrix3DT& mR1, const IntrinsicCalibrationMatrixT& mK1, const RotationMatrix3DT& mR2, const IntrinsicCalibrationMatrixT& mK2, bool flagNormalise=true);	///< 2D homography between the images acquired by two cameras with coincident camera centres

Matrix<ValueTypeM<CameraMatrixT>::type, 9, 12> JacobianCameraToFundamental(const CameraMatrixT& mP, bool flagNormalise=true);	///< Jacobian for the camera-to-fundamental matrix conversion
Matrix<ValueTypeM<CameraMatrixT>::type, 9, 12> JacobianNormalisedCameraToEssential(const CameraMatrixT& mP, bool flagNormalise=true);	///< Jacobian for the normalised camera-to-essential matrix conversion
Matrix<ValueTypeM<CameraMatrixT>::type, 12,12> JacobiandPdCR(const CameraMatrixT& mP);	///< Jacobian of the decomposition of a normalised camera matrix
Matrix<ValueTypeM<CameraMatrixT>::type, 12,17> JacobiandPdKCR(const CameraMatrixT& mP);	///< Jacobian of the decomposition of a camera matrix

}	//ElementsN
}	//SeeSawN

#endif /* CAMERA_H_8012349 */
