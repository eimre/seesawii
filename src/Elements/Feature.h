/**
 * @file Feature.h Public interaface for geometric features
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_H_1341326
#define FEATURE_H_1341326

#include "Feature.ipp"

namespace SeeSawN
{

namespace ElementsN
{

template<class DerivedT, class CoordinateT, class DescriptorT, class RoIT> class FeatureBaseC;    ///< Base class for geometric features

class ImageFeatureC;    ///< Represents an image feature
class BinaryImageFeatureC;	///< Represents an image feature with a binary descriptor
class SceneFeatureC;    ///< Represents a scene feature
class OrientedBinarySceneFeatureC;    ///< Represents an oriented scene feature, with binary image descriptors

template<typename TimestampT, class FeatureT> using FeatureTrajectoryT=map<TimestampT, FeatureT>;	///< Type for feature trajectories
typedef FeatureTrajectoryT<unsigned int, ImageFeatureC> ImageFeatureTrajectoryT;	///< Type for image feature trajectories

ostream& operator<<(ostream& str, const ImageFeatureC& feature); ///< Writes an image feature to a stream
ostream& operator<<(ostream& str, const BinaryImageFeatureC& feature); ///< Writes an binary image feature to a stream
ostream& operator<<(ostream& str, const SceneFeatureC& feature); ///< Writes a scene feature to a stream
template<typename TimeStampT> ostream& operator<<(ostream& str, const FeatureTrajectoryT<TimeStampT, ImageFeatureC>& trajectory);	///< Writes an image feature trajectory to a stream

template<class FeatureRangeT> struct DecomposeFeatureRangeM;    ///< A metafunction to expose the types associated with a feature in a range

/********** EXTERN TEMPLATES **********/
extern template ostream& operator<<(ostream&, const FeatureTrajectoryT<unsigned int, ImageFeatureC>&);
extern template ostream& operator<<(ostream&, const FeatureTrajectoryT<double, ImageFeatureC>&);

}   //ElementsN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class SeeSawN::ElementsN::TaggedDataC<unsigned int, SeeSawN::ElementsN::ImageDescriptorT>;
extern template class std::vector<SeeSawN::ElementsN::TaggedDescriptorT>;
extern template class SeeSawN::ElementsN::TaggedDataC<unsigned int, SeeSawN::ElementsN::ImageRoIT>;
extern template class std::vector<SeeSawN::ElementsN::TaggedRoIT>;
extern template class std::vector<SeeSawN::ElementsN::SceneFeatureC>;
extern template class std::vector<SeeSawN::ElementsN::ImageFeatureC>;
extern template class std::vector<SeeSawN::ElementsN::BinaryImageFeatureC>;

extern template class std::map<unsigned int, SeeSawN::ElementsN::ImageFeatureC>;
extern template class std::map<unsigned int, SeeSawN::ElementsN::BinaryImageFeatureC>;

#endif /* FEATURE_H_1341326 */
