/**
 * @file CorrespondenceDecimator.h Public interface for CorrespondenceDecimatorC
 * @author Evren Imre
 * @date 19 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CORRESPONDENCE_DECIMATOR_H_6512312
#define CORRESPONDENCE_DECIMATOR_H_6512312

#include "CorrespondenceDecimator.ipp"
#include "Coordinate.h"

namespace SeeSawN
{
namespace ElementsN
{

struct CorrespondenceDecimatorParametersC;	///< Parameters object for CorrespondenceDecimatorC

template<class Coordinate1T, class Coordinate2T> class CorrespondenceDecimatorC;	///< Decimates a range of coordinate correspondences

/********** EXTERN TEMPLATES **********/
using ::SeeSawN::ElementsN::Coordinate2DT;  // :: for Visual Studio
extern template class CorrespondenceDecimatorC<Coordinate2DT, Coordinate2DT>;
}	//ElementsN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::tuple<unsigned int, unsigned int>;
extern template class std::multimap<double, std::size_t>;
extern template class std::list<std::size_t>;
#endif /* CORRESPONDENCE_DECIMATOR_H_6512312 */
