/**
 * @file FeatureConcept.ipp Implementation of FeatureConceptC
 * @author Evren Imre
 * @date 21 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_CONCEPT_IPP_2323569
#define FEATURE_CONCEPT_IPP_2323569

#include <boost/concept_check.hpp>

namespace SeeSawN
{
namespace ElementsN
{

using boost::CopyConstructible;
using boost::Assignable;

/**
 * @brief Feature concept
 * @tparam TestT Template to be tested
 * @ingroup Concept
 */
template<class TestT>
class FeatureConceptC
{
	///@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((CopyConstructible<TestT>));
	BOOST_CONCEPT_ASSERT((Assignable<TestT>));

    public:

        BOOST_CONCEPT_USAGE(FeatureConceptC)
        {
            TestT tested;

            typedef typename TestT::coordinate_type CoordinateT;
            typedef typename TestT::roi_type RoIT;
            typedef typename TestT::descriptor_type DescriptorT;

            CoordinateT coordinate=tested.Coordinate();
            RoIT roi=tested.RoI();
            DescriptorT descriptor=tested.Descriptor();

            const CoordinateT ccoordinate=tested.Coordinate(); (void)ccoordinate;
            const RoIT croi=tested.RoI(); (void)croi;
            const DescriptorT cdescriptor=tested.Descriptor(); (void)cdescriptor;

            TestT tested2(coordinate, descriptor, roi);

            tested2.Swap(tested);
        }   //BOOST_CONCEPT_USAGE(FeatureConceptC)
	///@endcond
};  //class FeatureConceptC

}   //ElementsN
}	//SeeSawN

#endif /* FEATURECONCEPT_IPP_ */
