/**
 * @file Correspondence.h Public interface for correspondence-related structures
 * @author Evren Imre
 * @date 14 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CORRESPONDENCE_H_9521352
#define CORRESPONDENCE_H_9521352

#include "Correspondence.ipp"
#include <boost/range/iterator_range.hpp>
#include "Coordinate.h"
#include "Feature.h"

namespace SeeSawN
{
namespace ElementsN
{
class MatchStrengthC;	///< Metrics for the strength of a correspondence

/** @ingroup Elements */ //@{

using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;

//Index correspondence
typedef bimap<list_of<size_t>, list_of<size_t>, left_based, with_info<MatchStrengthC> > CorrespondenceListT; ///< A correspondence list. Info: [similarity; ambiguity] (for 1:N matches as well)

//Coordinate correspondence
typedef bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based, with_info<MatchStrengthC> > CoordinateCorrespondenceList2DT; ///< A correspondence list of 2D coordinates. Info: [similarity; ambiguity] (for 1:N matches as well)
typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate2DT>, left_based, with_info<MatchStrengthC> > CoordinateCorrespondenceList32DT; ///< A correspondence list of 3D and 2D coordinates. Info: [similarity; ambiguity] (for 1:N matches as well)
typedef bimap<list_of<Coordinate3DT>, list_of<Coordinate3DT>, left_based, with_info<MatchStrengthC> > CoordinateCorrespondenceList3DT; ///< A correspondence list of 3D coordinates. Info: [similarity; ambiguity] (for 1:N matches as well)
template<class Coordinate1T, class Coordinate2T> using CoordinateCorrespondenceListT=bimap<list_of<Coordinate1T>, list_of<Coordinate2T>, left_based, with_info<MatchStrengthC> >;	///< A correspondence list for generic coordinates
//@}

template<class CoordinateCorrespondenceContainerT, class CorrespondenceRangeT, class CoordinateRange1T, class CoordinateRange2T>
CoordinateCorrespondenceContainerT MakeCoordinateCorrespondenceList(const CorrespondenceRangeT& indices, const CoordinateRange1T& set1, const CoordinateRange2T& set2);   ///< Converts a list of corresponding indices to a list of corresponding coordinates

template<class CorrespondenceRangeT, class Feature1T, class Feature2T> void MakeFeatureCorrespondenceList(vector<Feature1T>& set1, vector<Feature2T>& set2, const CorrespondenceRangeT& indices);	///< Purges the non-matching features

void ApplyStrengthFilter(CorrespondenceListT& correspondences, const MatchStrengthC& threshold);	///< Filters an index correspondence set via match strength
template<class CorrespondenceRangeT> tuple<CorrespondenceListT, vector<size_t> > SortByAmbiguity(const CorrespondenceRangeT& src);	///< Sorts a correspondence list by ambiguity
template<class CorrespondenceRangeT> vector<unsigned int> RankByAmbiguity(const CorrespondenceRangeT& src);	///< Ranks a correspondence list by ambiguity

template<typename RealT, class CorrespondenceRangeT> Matrix<RealT, Eigen::Dynamic, 1> CorrespondenceRangeToVector(const CorrespondenceRangeT& src);	///< Converts a correspondence range to a vector
template<class Coordinate1T, class Coordinate2T, class InputRangeT> bimap<list_of<Coordinate1T>, list_of<Coordinate2T>, left_based> VectorToCorrespondenceRange(const InputRangeT& src, size_t nDim1, size_t nDim2);	///< Converts an input range to a correspondence vector

/********** EXTERN TEMPLATES **********/

extern template CoordinateCorrespondenceList2DT MakeCoordinateCorrespondenceList(const CorrespondenceListT&, const vector<Coordinate2DT>&, const vector<Coordinate2DT>&);

using SeeSawN::ElementsN::ImageFeatureC;
extern template void MakeFeatureCorrespondenceList(vector<ImageFeatureC>&, vector<ImageFeatureC>&, const CorrespondenceListT&);

extern template tuple<CorrespondenceListT, vector<size_t> > SortByAmbiguity(const CorrespondenceListT&);
extern template vector<unsigned int> RankByAmbiguity(const CorrespondenceListT&);

using SeeSawN::WrappersN::ValueTypeM;
extern template Matrix<typename ValueTypeM<Coordinate2DT>::type, Eigen::Dynamic, 1> CorrespondenceRangeToVector<typename ValueTypeM<Coordinate2DT>::type, CoordinateCorrespondenceList2DT>(const CoordinateCorrespondenceList2DT&);

using boost::iterator_range;
extern template bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> VectorToCorrespondenceRange(const iterator_range<ValueTypeM<Coordinate2DT>::type*>&, size_t, size_t);

}   //ElementsN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::size_t>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::size_t>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;


extern template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate2DT>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate2DT>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;

extern template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate3DT>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate2DT>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;

extern template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate3DT>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate3DT>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;

extern template class std::vector<SeeSawN::ElementsN::Coordinate2DT>;

#endif /* CORRESPONDENCE_H_9521352 */
