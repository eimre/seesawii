/**
 * @file Feature.cpp Instantiations of geometric features
 * @author Evren Imre
 * @date 16 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Feature.h"

namespace SeeSawN
{
namespace ElementsN
{

/********** ImageFeatureC **********/

/**
 * @brief Default constructor
 * @post The object is not valid
 */
ImageFeatureC::ImageFeatureC()
{}

/**
 * @brief Constructor
 * @param[in] ccoordinate Feature coordinate
 * @param[in] ddescriptor Feature descriptor
 * @param[in] rroi Region represented by the feature
 */
ImageFeatureC::ImageFeatureC(const Coordinate2DT& ccoordinate, const typename BaseT::descriptor_type& ddescriptor, const typename BaseT::roi_type& rroi) : BaseT(ccoordinate, ddescriptor, rroi)
{}

/**
 * @brief Swaps the contents of two image features
 * @param[in, out] src Feature to be swapped
 */
void ImageFeatureC::Swap(ImageFeatureC& src)
{
    Coordinate().swap(src.Coordinate());
    Descriptor().swap(src.Descriptor());
    RoI().swap(src.RoI());
}   //void Swap(ImageFeatureC& src)

/********** BinaryImageFeatureC **********/
/**
 * @brief Default constructor
 * @post The object is not valid
 */
BinaryImageFeatureC::BinaryImageFeatureC()
{}

/**
 * @brief Constructor
 * @param[in] ccoordinate Feature coordinate
 * @param[in] ddescriptor Feature descriptor
 * @param[in] rroi Region represented by the feature
 */
BinaryImageFeatureC::BinaryImageFeatureC(const Coordinate2DT& ccoordinate, const typename BaseT::descriptor_type& ddescriptor, const typename BaseT::roi_type& rroi) : BaseT(ccoordinate, ddescriptor, rroi)
{}

/**
 * @brief Swaps the contents of two image features
 * @param[in, out] src Feature to be swapped
 */
void BinaryImageFeatureC::Swap(BinaryImageFeatureC& src)
{
    Coordinate().swap(src.Coordinate());
    Descriptor().swap(src.Descriptor());
    RoI().swap(src.RoI());
}   //void Swap(ImageFeatureC& src)

/********** SceneFeatureC **********/

/**
 * @brief Default constructor
 * @post The object is not valid
 */
SceneFeatureC::SceneFeatureC()
{}

/**
 * @brief Constructor
 * @param[in] ccoordinate Feature coordinate
 * @param[in] ddescriptor Feature descriptor
 * @param[in] rroi Region represented by the feature
 */
SceneFeatureC::SceneFeatureC(const Coordinate3DT& ccoordinate, const typename BaseT::descriptor_type& ddescriptor, const typename BaseT::roi_type& rroi) : BaseT(ccoordinate, ddescriptor, rroi)
{}

/**
 * @brief Swaps the contents of two scene features
 * @param[in, out] src Feature to be swapped
 */
void SceneFeatureC::Swap(SceneFeatureC& src)
{
    Coordinate().swap(src.Coordinate());
    Descriptor().swap(src.Descriptor());
    RoI().swap(src.RoI());
}   //void Swap(SceneFeatureC& src)

/**
 * @brief Normalises the image descriptors associated with the feature
 */
void SceneFeatureC::NormaliseDescriptor()
{
	for(auto& current : Descriptor())
		current.second.Descriptor().normalize();
}	//void NormaliseDescriptor()

/**
 * @brief Summarises a scene feature with its most representative image descriptor
 * @post The feature is compacted
 */
void SceneFeatureC::MakeCompact()
{
	if(Descriptor().empty() || Descriptor().size()==1)
		return;

	//Compute the median descriptor
	size_t sDescriptor=Descriptor().begin()->second.Descriptor().size();
	vector<set<ValueTypeM<ImageDescriptorT>::type > > accumulator(sDescriptor);

	for(const auto& current : Descriptor())
	{
		const auto& pImageDescriptor=current.second.Descriptor();
		for(size_t c=0; c<sDescriptor; ++c)
			accumulator[c].insert(pImageDescriptor[c]);
	}	//for(const auto& current : currentFeature)

	size_t iMedian = floor(Descriptor().size()/2);
	ImageFeatureC::descriptor_type medianDescriptor(sDescriptor);
	for(size_t c=0; c<sDescriptor; ++c)
		medianDescriptor[c] = *next(accumulator[c].begin(), iMedian);

	//Find the trajectory point closest to the median
	priority_queue<pair<unsigned int, unsigned int> > sorter;
	for(const auto& current : Descriptor())
		sorter.emplace( (current.second.Descriptor()-medianDescriptor).squaredNorm(), current.first );

	//Update
	size_t iItem=sorter.top().second;
	descriptor_type newDescriptor{*Descriptor().find(iItem)};
	Descriptor().swap(newDescriptor);

	roi_type newRoI{(unsigned int)iItem};
	RoI().swap(newRoI);
}	//OrientedBinarySceneFeatureC MakeCompact(const OrientedBinarySceneFeatureC& feature)

/********** OrientedBinarySceneFeatureC **********/
/**
 * @brief Default constructor
 * @post The object is not valid
 */
OrientedBinarySceneFeatureC::OrientedBinarySceneFeatureC()
{}

/**
 * @brief Constructor
 * @param[in] ccoordinate Feature coordinate
 * @param[in] ddescriptor Feature descriptor
 * @param[in] rroi Region represented by the feature
 */
OrientedBinarySceneFeatureC::OrientedBinarySceneFeatureC(const Coordinate3DT& ccoordinate, const typename BaseT::descriptor_type& ddescriptor, const typename BaseT::roi_type& rroi) : BaseT(ccoordinate, ddescriptor, rroi)
{}

/**
 * @brief Swaps the contents of two scene features
 * @param[in, out] src Feature to be swapped
 */
void OrientedBinarySceneFeatureC::Swap(OrientedBinarySceneFeatureC& src)
{
    Coordinate().swap(src.Coordinate());
    Descriptor().swap(src.Descriptor());
    RoI().swap(src.RoI());
}   //void Swap(SceneFeatureC& src)

/**
 * @brief Summarises a scene feature with its most representative image descriptor
 * @post The feature is compacted
 */
void OrientedBinarySceneFeatureC::MakeCompact()
{
	if(Descriptor().empty() || Descriptor().size()==1)
		return;

	//Compute the median descriptor
	size_t sDescriptor=Descriptor().begin()->second.Descriptor().size();
	vector<unsigned int> counter(sDescriptor,0);

	for(const auto& current : Descriptor())
	{
		const auto& pImageDescriptor=current.second.Descriptor();
		for(size_t c=0; c<sDescriptor; ++c)
			if(pImageDescriptor[c])
				++counter[c];
	}	//for(const auto& current : currentFeature)

	size_t nDescriptor=Descriptor().size();
	unsigned int threshold=floor(nDescriptor/2);
	BinaryImageFeatureC::descriptor_type medianString(sDescriptor);
	for(size_t c=0; c<sDescriptor; ++c)
		medianString[c]=counter[c]>threshold;	//So, we favour zero

	//Find the trajectory point closest to the median string
	priority_queue<pair<unsigned int, unsigned int> > sorter;
	for(const auto& current : Descriptor())
		sorter.emplace(BinaryImageFeatureC::descriptor_type::HammingDistance(current.second.Descriptor(), medianString), current.first);

	//Update
	size_t iItem=sorter.top().second;
	descriptor_type newDescriptor{*Descriptor().find(iItem)};
	Descriptor().swap(newDescriptor);

	roi_type newRoI{(unsigned int)iItem};
	RoI().swap(newRoI);
}	//OrientedBinarySceneFeatureC MakeCompact(const OrientedBinarySceneFeatureC& feature)

/********** FREE FUNCTIONS **********/

/**
 * @brief Writes an image feature to a stream
 * @param[in] str Output stream
 * @param[in] feature Feature
 * @return Modified stream
 * @ingroup IO
 * @remarks No dedicated tests
 */
ostream& operator<<(ostream& str, const ImageFeatureC& feature)
{
    size_t s1=feature.Coordinate().size();
    for(size_t c=0; c<s1; ++c)
        str<<feature.Coordinate()[c]<<" ";

    size_t sRoI=feature.RoI().size();
    for(size_t c=0; c<sRoI; ++c)
        str<<feature.RoI()[c]<<" ";

    size_t sDescriptor=feature.Descriptor().size();
    for(size_t c=0; c<sDescriptor; ++c)
        str<<feature.Descriptor()[c]<<" ";

    str<<"\n";

    return str;
}   //ostream& operator<<(ostream& str, const ImageFeatureC& feature)

/**
 * @brief Writes a binary image feature to a stream
 * @param[in] str Output stream
 * @param[in] feature Feature
 * @return Modified stream
 * @ingroup IO
 * @remarks No dedicated tests
 */
ostream& operator<<(ostream& str, const BinaryImageFeatureC& feature)
{
    size_t s1=feature.Coordinate().size();
    for(size_t c=0; c<s1; ++c)
        str<<feature.Coordinate()[c]<<" ";

    size_t sRoI=feature.RoI().size();
    for(size_t c=0; c<sRoI; ++c)
        str<<feature.RoI()[c]<<" ";

    //If the descriptor size is not a multiple of CHAR_BIT, embed into a larger bitset

    typedef BinaryImageFeatureC::descriptor_type DescriptorT;
    const DescriptorT& descriptor=feature.Descriptor();

    size_t sDescriptor=descriptor.size();
    BinaryImageFeatureC::descriptor_type tempDesc;
    if(sDescriptor%CHAR_BIT!=0)
    {
    	tempDesc.resize(ceil((float)sDescriptor/CHAR_BIT)*CHAR_BIT, false);

    	for(size_t c=0; c<sDescriptor; ++c)
    		tempDesc[c]=descriptor[c];

    	 sDescriptor=tempDesc.size();
    }	//if(sDescriptor%CHAR_BIT!=0)
    else
    	tempDesc=descriptor;


    //Output as char
    unsigned char tmp=0;
    size_t i=1;
    for(int c=sDescriptor-1; c>=0; --c, ++i, tmp<<=1)
    {
    	tmp += tempDesc[c] ? 1:0;

    	//Reset at every byte
    	if(i==CHAR_BIT)
    	{
    		str<<(int)tmp<<" ";
    		tmp=0;
    		i=0;
    	}
    }	//for(size_t c=0; c<sDescriptor; ++c)

    str<<"\n";

    return str;
}   //ostream& operator<<(ostream& str, const ImageFeatureC& feature)

/**
 * @brief Writes a scene feature to a stream
 * @param[in] str Output stream
 * @param[in] feature Feature
 * @return Modified stream
 * @ingroup IO
 * @remarks No dedicated tests
 */
ostream& operator<<(ostream& str, const SceneFeatureC& feature)
{
    //Coordinate
    size_t s1=feature.Coordinate().size();
    for(size_t c=0; c<s1; ++c)
        str<<feature.Coordinate()[c]<<" ";

    str<<"\n";

    //Number of observations
    size_t sList=feature.RoI().size();
    str<<sList<<"\n";

    //Observations
    for(const auto& current: feature.Descriptor())
    	str<<current.first<<" "<<current.second;	//Feed the time stamp, then the 2D feature

    return str;
}   //ostream& operator<<(ostream& str, const SceneFeatureC& feature)

/**
 * @brief Writes a scene feature to a stream
 * @param[in] str Output stream
 * @param[in] feature Feature
 * @return Modified stream
 * @ingroup IO
 * @remarks No dedicated tests
 */
ostream& operator<<(ostream& str, const OrientedBinarySceneFeatureC& feature)
{
    //Coordinate
    size_t s1=feature.Coordinate().size();
    for(size_t c=0; c<s1; ++c)
        str<<feature.Coordinate()[c]<<" ";

    str<<"\n";

    //Number of observations
    size_t sList=feature.RoI().size();
    str<<sList<<"\n";

    //Observations
    for(const auto& current: feature.Descriptor())
    	str<<current.first<<" "<<current.second;	//Feed the time stamp, then the 2D feature

    return str;
}   //ostream& operator<<(ostream& str, const OrientedBinarySceneFeatureC& feature)

/********** EXPLICIT INSTANTIATIONS **********/
template ostream& operator<<(ostream&, const FeatureTrajectoryT<unsigned int, ImageFeatureC>&);
template ostream& operator<<(ostream&, const FeatureTrajectoryT<double, ImageFeatureC>&);

}   //ElementsN
}	//SeeSawN

/********** EXPLICIT INSTANTIATIONS **********/
template class SeeSawN::ElementsN::TaggedDataC<unsigned int, SeeSawN::ElementsN::ImageDescriptorT>;
template class std::vector<SeeSawN::ElementsN::TaggedDescriptorT>;
//template class SeeSawN::ElementsN::TaggedDataC<unsigned int, SeeSawN::ElementsN::ImageRoIT>;  //Uncomment if descriptor and roi are different types
//template class std::vector<SeeSawN::ElementsN::TaggedRoIT>;   //Uncomment if descriptor and roi are different types
template class std::vector<SeeSawN::ElementsN::SceneFeatureC>;
template class std::vector<SeeSawN::ElementsN::ImageFeatureC>;
template class std::vector<SeeSawN::ElementsN::BinaryImageFeatureC>;

template class std::map<unsigned int, SeeSawN::ElementsN::ImageFeatureC>;
template class std::map<unsigned int, SeeSawN::ElementsN::BinaryImageFeatureC>;
