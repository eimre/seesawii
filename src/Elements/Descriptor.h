/**
 * @file Descriptor.h Descriptor types
 * @author Evren Imre
 * @date 29 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef DESCRIPTOR_H_5323692
#define DESCRIPTOR_H_5323692

//#include <boost/dynamic_bitset.hpp>
#include "../Modified/boost_dynamic_bitset.hpp"
#include <Eigen/Dense>
#include <vector>

namespace SeeSawN
{
namespace ElementsN
{

/** @ingroup Elements */ //@{
//Vector type (precision and fixed/dynamic) can be changed
typedef Eigen::VectorXf ImageDescriptorT;	///< An image descriptor
//typedef Eigen::Matrix<float, 192, 1> ImageDescriptorT;   ///< An image descriptor

typedef boost::dynamic_bitset<unsigned int> BinaryImageDescriptorT;	///< A binary image descriptor
																			///< WARNING: do not change the block type as __builtin_popcnt requires unsigned int
//@}
}
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<SeeSawN::ElementsN::ImageDescriptorT>;
extern template class std::vector<SeeSawN::ElementsN::BinaryImageDescriptorT>;


#endif /* DESCRIPTOR_H_5323692 */
