/**
 * @file GeometricEntity.ipp Details for various geometric entities
 * @author Evren Imre
 * @date 1 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_ENTITY_IPP_0804343
#define GEOMETRIC_ENTITY_IPP_0804343

#include <boost/optional.hpp>
#include <boost/concept_check.hpp>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/SVD>
#include <tuple>
#include <limits>
#include <cmath>
#include <vector>
#include <type_traits>
#include "Coordinate.h"
#include "Correspondence.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Wrappers/EigenExtensions.h"
#include "../Numeric/LinearAlgebra.h"

namespace SeeSawN
{
namespace ElementsN
{

using boost::optional;
using Eigen::Matrix;
using Eigen::Hyperplane;
using Eigen::AlignedBox;
using Eigen::Quaternion;
using Eigen::AngleAxis;
using Eigen::JacobiSVD;
using std::tuple;
using std::make_tuple;
using std::tie;
using std::numeric_limits;
using std::sqrt;
using std::fabs;
using std::vector;
using std::is_same;
using SeeSawN::ElementsN::HCoordinate2DT;
using SeeSawN::ElementsN::HCoordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::CoordinateCorrespondenceList2DT;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::WrappersN::MakeCrossProductMatrix;
using SeeSawN::WrappersN::Reshape;
using SeeSawN::NumericN::ComputeClosestRotationMatrix;
using SeeSawN::NumericN::RQDecomposition33;

typedef Matrix< typename ValueTypeM<HCoordinate2DT>::type, 3, 3> EpipolarMatrixT;	///< Fundamental or essential matrix
typedef Matrix<ValueTypeM<Coordinate3DT>::type, 3, 3> RotationMatrix3DT;	///< A 3x3 rotation matrix

/**
 * @brief Decomposes the epipolar matrix into relative calibration terms
 * @tparam CorrespondenceRangeT A range of coordinate correspondences
 * @param[in] mE Essential matrix to be decomposed
 * @param[in] validationSet A set of normalised 2D correspondences used for choosing between the 4 possible camera configurations
 * @param[in] zero Threshold for zero-translation check
 * @return A tuple, translation direction and rotation matrix. Invalid if the zero-translation check fails
 * @pre \c CorrespondenceRangeT is a bimap structure (unenforced)
 * @pre \c CorrespondenceRangeT holds objects of type \c Coordinate2DT
 * @pre \c validationSet is nonempty
 * @remarks J. Weng, T. S. Huang, N. Ahuja, "Motion and Structure from Two Perspective Views: Algorithms, Error Analysis and Error Estimation, " PAMI, Vol. 11, pp. 451-474, May 1989
 * @warning Members of \c validatorSet should be normalised by premultiplying with the inverse of the respective intrinsic calibration matrices
 * @ingroup Utility
 */
template<class CorrespondenceRangeT>
optional<tuple<Coordinate3DT, RotationMatrix3DT> > DecomposeEssential(const EpipolarMatrixT& mE, const CorrespondenceRangeT& validationSet, typename ValueTypeM<EpipolarMatrixT>::type zero=numeric_limits<typename ValueTypeM<EpipolarMatrixT>::type>::epsilon())
{
	//Preconditions
	static_assert(is_same<typename CorrespondenceRangeT::left_key_type, Coordinate2DT>::value, "DecomposeEssential: CorrespondenceRangeT must hold elements of type Coordinate2DT");
	static_assert(is_same<typename CorrespondenceRangeT::right_key_type, Coordinate2DT>::value, "DecomposeEssential: CorrespondenceRangeT must hold elements of type Coordinate2DT");

	assert(!validationSet.empty());

	optional<tuple<Coordinate3DT, RotationMatrix3DT> > output;

	typedef typename ValueTypeM<EpipolarMatrixT>::type RealT;

	//Make rank-2 and set the norm to sqrt(2) (2.11)

	JacobiSVD<EpipolarMatrixT, Eigen::FullPivHouseholderQRPreconditioner> svd(mE, Eigen::ComputeFullU | Eigen::ComputeFullV);
	auto singularValues=svd.singularValues();

	if(svd.nonzeroSingularValues()<2)	//Rank 0 or 1
		return output;

	singularValues[2]=0;
	singularValues *= sqrt(2)/singularValues.norm();	//Set the norm to sqrt(2)

	//Recompose E
	EpipolarMatrixT mE2= svd.matrixU() * (singularValues.asDiagonal() * svd.matrixV().transpose()).eval();

	//t vector (2.12)
	Coordinate3DT vt=svd.matrixU().col(2);	//Left null vector

	//Precompute txX'
	vector<HCoordinate2DT> txXp; txXp.reserve(validationSet.size());
	for(const auto& current : validationSet)
		txXp.emplace_back(vt.cross(current.right.homogeneous().eval()));

	//Sign of t, first pass (2.13)
	RealT acc1=0;
	size_t c1=0;
	for(const auto& current : validationSet)
	{
		acc1+= txXp[c1].dot( mE2*current.left.homogeneous());
		++c1;
	}	//for(const auto& current : validationSet)

	//Flip signs
	if(acc1<0)
	{
		vt=-vt;
		for(auto& current : txXp)
			current=-current;
	}	//if(acc1<0)

	//Compute the rotation

	// 2.17
	RotationMatrix3DT mW;
	mW.col(0)= mE2.col(0).cross(vt) + mE2.col(1).cross(mE2.col(2));
	mW.col(1)= mE2.col(1).cross(vt) + mE2.col(2).cross(mE2.col(0));
	mW.col(2)= mE2.col(2).cross(vt) + mE2.col(0).cross(mE2.col(1));

	//Find the closest rotation matrix to W (2.20 - 2.21)
	RotationMatrix3DT mR=ComputeClosestRotationMatrix(mW);

	//Sign of t, second pass (2.23)
	RealT acc2=0;
	size_t c2=0;
	bool flagZero=false;
	for(const auto& current : validationSet)
	{
		//Zero-translation check
		HCoordinate2DT xpRx= current.right.homogeneous().eval().cross(mR*current.left.homogeneous().eval());
		flagZero |= (xpRx.norm() / sqrt((current.right.squaredNorm()+1) * (current.left.squaredNorm()+1))) < zero;

		acc2+= txXp[c2].dot(xpRx);
		++c2;
	}	//for(const auto& current : validationSet)

	if(flagZero)
		return output;

	if(acc2<0)
		vt=-vt;

	return make_tuple(vt, mR);
}	//optional<tuple<Coordinate3DT, RotationMatrix3DT> > DecomposeEssential(const EpipolarMatrixT& mE, typename ValueTypeM<EpipolarMatrixT>::type zero)

}	//ElementsN
}	//SeeSawN

#endif /* GEOMETRIC_ENTITY_IPP_0804343 */
