/**
 * CorrespondenceDecimator.ipp Implementation of CorrespondenceDecimatorC
 * @author Evren Imre
 * @date 19 Oct 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef CORRESPONDENCE_DECIMATOR_IPP_0901234
#define CORRESPONDENCE_DECIMATOR_IPP_0901234

#include <boost/range/algorithm.hpp>
#include <vector>
#include <list>
#include <map>
#include <numeric>
#include <tuple>
#include <utility>
#include <cmath>
#include <iterator>
#include <algorithm>
#include <climits>
#include <stdexcept>
#include "Correspondence.h"
#include "../Geometry/CoordinateTransformation.h"
#include "../Geometry/CoordinateStatistics.h"
#include "../Wrappers/BoostBimap.ipp"

namespace SeeSawN
{
namespace ElementsN
{

using boost::range::transform;
using std::vector;
using std::list;
using std::multimap;
using std::map;
using std::iota;
using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;
using std::pair;
using std::make_pair;
using std::min;
using std::max;
using std::advance;
using std::copy;
using std::all_of;
using std::numeric_limits;
using std::invalid_argument;
using SeeSawN::ElementsN::CoordinateCorrespondenceListT;
using SeeSawN::ElementsN::RankByAmbiguity;
using SeeSawN::ElementsN::MatchStrengthC;
using SeeSawN::GeometryN::CoordinateTransformationsT;
using SeeSawN::GeometryN::CoordinateStatisticsT;
using SeeSawN::WrappersN::BimapToVector;

enum class DecimatorQuantisationStrategyT {NONE, LEFT, RIGHT, BOTH};	//No quantisation, left-based, right-based, both sources

/**
 * @brief Parameters object for CorrespondenceDecimatorC
 */
struct CorrespondenceDecimatorParametersC
{
	DecimatorQuantisationStrategyT quantisationStrategy = DecimatorQuantisationStrategyT::NONE;	///< Quantisation strategy
	bool flagSort = true;	///< \c true if the correspondences are to be sorted wrt/ambiguity
	size_t maxDensity = numeric_limits<size_t>::max(); ///< Maximum number of correspondences per bin. Priority: Third
	size_t minElements = 0;	///< Minimum number of correspondences in the decimated set. Priority: Second
	size_t maxElements = numeric_limits<size_t>::max();	///< Maximum number of correspondences in the decimated set.
	double maxAmbiguity = numeric_limits<double>::infinity();	///< Maximum ambiguity for a correspondence in the decimated set. Priority: First
};	//struct CorrespondenceDecimatorParametersC

/**
 * @brief Decimates a range of coordinate correspondences
 * @tparam Coordinate1T Type of the first member in a corresponding pair
 * @tparam Coordinate2T Type of the second member in a corresponding pair
 * @remarks Algorithm:
 * - Partition the correspondences into bins
 * - Sort the elements in each bin wrt/ambiguity
 * - Breadth-first scan over the bins, where each visit removes the least ambiguous correspondence.
 * - Balanced distribution: A bin cannot contribute more than a specified number of elements
 * @remarks Final order:
 * - Each breadth-first scan returns a layer, ordered wrt/ambiguity
 * - The output is a concatenation of layers
 * - First layer holds the least ambiguous matches in each bin. Second layer holds the second least ambiguous matches. And so on...
 * - A spatially balanced distribution of best correspondences
 * @remarks Usage notes
 * 	- \c BOTH as a quantisation strategy is advisable only when the correspondence set has a low outlier ratio. Rationale:
 * 		- In one-sided quantisation, an outlier with a high ambiguity score may rank low in its bin.
 * 		- In two-sided, the bin pair for an outlier is often unique- hence it is included in the first layer as a sole representative
 * @ingroup Algorithm
 * @nosubgrouping
 */
template<class Coordinate1T, class Coordinate2T>
class CorrespondenceDecimatorC
{
	private:

		typedef CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> CorrespondenceContainerT;	///< Type for a correspondence container
		typedef vector<size_t> IndexMapT;	///< Type for an index map

		/** @name Implementation details */ //@{
		template<class CoordinateT> static vector<unsigned int> Bucketise(const vector<CoordinateT>& src, const typename CoordinateTransformationsT<CoordinateT>::ArrayD& binSize);	///< Bucketises a range of correspondences
		static multimap<double, size_t> Sort(const vector<MatchStrengthC>& strength);	///< Sorts the correspondences wrt/ambiguity

		typedef tuple<unsigned int, unsigned int> BinIdT;	///< Bin identifying a correspondence bucket
		typedef map<BinIdT, list<pair<double, size_t> > > OrderedBucketT;	///< [Bucket; Correspondences in the bucket]
		static OrderedBucketT MakeOrderedBucket(const vector<unsigned int>& binL, const vector<unsigned int>& binR, const multimap<double, size_t>& rankToIndexMap);	///< Builds the ordered bucket structure
		static list<size_t> Peel(OrderedBucketT& orderedBuckets, size_t maxDensity, size_t minElements, size_t maxElements);	///< Breadth-first scan of the ordered buckets
		//@}

	public:

		typedef typename CoordinateTransformationsT<Coordinate1T>::ArrayD dimension_type1;	///< An array holding the bin dimensions for the first domain
		typedef typename CoordinateTransformationsT<Coordinate2T>::ArrayD dimension_type2;	///< An array holding the bin dimensions for the second domain

		template<class CorrespondenceRangeT> static tuple<CorrespondenceContainerT, IndexMapT> Run(const CorrespondenceRangeT& src, const dimension_type1& binSize1, const dimension_type2& binSize2, const CorrespondenceDecimatorParametersC& parameters);	///< Decimates a correspondence range

		/** @name Utility */ //@{
		static tuple<dimension_type1, dimension_type2>  ComputeBinSize(const CorrespondenceContainerT& src, double binDensity1, double binDensity2);	///< Computes the bin dimensions by specifying the number of bins per standard deviation
		//@}
};	//class CorrespondenceDecimatorC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Bucketises a range of correspondences
 * @param[in] src Coordinates to be bucketised
 * @param[in] binSize Bin dimensions
 * @return Vector of bucket ids
 */
template<class Coordinate1T, class Coordinate2T>
template<class CoordinateT>
vector<unsigned int> CorrespondenceDecimatorC<Coordinate1T, Coordinate2T>::Bucketise(const vector<CoordinateT>& src, const typename CoordinateTransformationsT<CoordinateT>::ArrayD& binSize)
{
	typename CoordinateStatisticsT<CoordinateT>::ArrayD2 extent=*CoordinateStatisticsT<CoordinateT>::ComputeExtent(src);
	return CoordinateTransformationsT<CoordinateT>::Bucketise(src, binSize, extent);
}	//vector<unsigned int> Bucketise(const vector<CoordinateT>& src, const typename CoordinateTransformationsC<CoordinateT>::ArrayD& binSize, bool flagQuantise);

/**
 * @brief Sorts the correspondences wrt/ambiguity
 * @param[in] strength Match strength vector
 * @return A multimap of indices, ordered wrt/increasing ambiguity.
 */
template<class Coordinate1T, class Coordinate2T>
multimap<double, size_t> CorrespondenceDecimatorC<Coordinate1T, Coordinate2T>::Sort(const vector<MatchStrengthC>& strength)
{
	multimap<double, size_t> output;
	size_t index=0;
	for(const auto& current : strength)
	{
		output.emplace(current.Ambiguity(), index);
		++index;
	}	//for(const auto& current, strength)

	return output;
}	//vector<size_t> Sort(const CorrespondenceRangeT& src)

/**
 * @brief Builds the ordered bucket structure
 * @param[in] binL Bins in the first domain
 * @param[in] binR Bins in the second domain
 * @param[in] rankToIndexMap Rank-index map
 * @return Ordered bucket structure
 */
template<class Coordinate1T, class Coordinate2T>
auto CorrespondenceDecimatorC<Coordinate1T, Coordinate2T>::MakeOrderedBucket(const vector<unsigned int>& binL, const vector<unsigned int>& binR, const multimap<double, size_t>& rankToIndexMap) -> OrderedBucketT
{
	OrderedBucketT output;

	for(const auto& current : rankToIndexMap)
	{
		//Attempt to insert a new element
		BinIdT query(binL[current.second], binR[current.second]);
		auto it=output.emplace(query, typename OrderedBucketT::mapped_type());
		(it.first)->second.push_back(current);	//it either points to the new list, or to an existing list
	}	//for(const auto& current : rankToIndexMap)

	return output;
}	//OrderedBucketT MakeOrderedBucket(const vector<unsigned int>& binL, const vector<unsigned int>& binR, const multimap<double, size_t>& rankToIndexMap)

/**
 * @brief Breadth-first scan of the ordered buckets
 * @param[in, out] orderedBuckets Ordered buckets. Consumed by the function
 * @param[in] maxDensity Maximum elements/bucket
 * @param[in] minElements Minimum number of elements in the final set. Overrides everything else
 * @param[in] maxElements Maximum number of elements in the final set
 * @return Indices of the elements visited by the function
 */
template<class Coordinate1T, class Coordinate2T>
list<size_t> CorrespondenceDecimatorC<Coordinate1T, Coordinate2T>::Peel(OrderedBucketT& orderedBuckets, size_t maxDensity, size_t minElements, size_t maxElements)
{
	list<size_t> output;

	size_t cElement=0;
	size_t cLayer=0;
	while(cElement<maxElements)
	{
		//Peel a layer
		multimap<double, size_t> layer;	//Ordered wrt/ambiguity

		auto itEnd=orderedBuckets.end();
		for(auto it=orderedBuckets.begin(); it!=itEnd; advance(it,1) )
			if(!it->second.empty())
			{
				layer.insert(it->second.front());	//Insert
				it->second.pop_front();
			}	//if(!it->second.empty())

		//Update the output
		for(const auto& current : layer)
		{
			output.push_back(current.second);
			++cElement;

			//Can break mid-layer
			if(cElement==maxElements)
				break;
		}	//for(const auto& current : layer)

		++cLayer;
		if(cLayer>=maxDensity && cElement>=minElements)
			break;

		if(all_of(orderedBuckets.begin(), orderedBuckets.end(), [](const pair<BinIdT, list<pair<double, size_t> > >& current){return current.second.empty();}) )
			break;
	}	//while(cElement<maxElements)

	return output;
}	//vector<size_t> Peel(OrderedBucketT& orderedBuckets, size_t maxDensity, size_t maxElements)

/**
 * @brief Decimates a correspondence range
 * @tparam CorrespondenceRangeT A range of correspondences
 * @param[in] src Range to be decimated
 * @param[in] binSize1 Bin dimensions for the first domain. Ignored if \c quantisationStrategy=NONE
 * @param[in] binSize2 Bin dimensions for the second domain. Ignored if \c quantisationStrategy=NONE
 * @param[in] parameters Parameters
 * @return Decimated correspondences, and a vector indicating the ranks in the original list
 * @pre \c CorrespondenceRangeT is one of the coordinate correspondence structures in \c Correspondence.h (unenforced)
 * @remarks Parameters for common operations
 * - \c quantisationStrategy=NONE , \c flagSort=true : Least ambiguous elements, regardless of spatial distribution
 * - \c quantisationStrategy=BOTH , \c flagSort=true : Least ambiguous elements, with balanced spatial distribution
 */
template<class Coordinate1T, class Coordinate2T>
template<class CorrespondenceRangeT>
auto CorrespondenceDecimatorC<Coordinate1T, Coordinate2T>::Run(const CorrespondenceRangeT& src, const dimension_type1& binSize1, const dimension_type2& binSize2, const CorrespondenceDecimatorParametersC& parameters) -> tuple<CorrespondenceContainerT, IndexMapT>
{
	size_t nElements=src.size();

	//Parameters
	DecimatorQuantisationStrategyT quantisationStrategy=parameters.quantisationStrategy;
	size_t minElements=min(parameters.minElements, nElements);
	size_t maxElements=max(minElements, parameters.maxElements);
	size_t maxDensity=(quantisationStrategy!=DecimatorQuantisationStrategyT::NONE) ? parameters.maxDensity:nElements;

	tuple<CorrespondenceContainerT, IndexMapT> output;
	if(src.empty() || maxDensity==0 || maxElements==0)
		return output;

	//Maximum ambiguity test
	size_t iSurvivor=0;
	vector<unsigned int> survivorIndices; survivorIndices.reserve(nElements);
	CorrespondenceContainerT filtered;
	for(const auto& current : src)
	{
		if(current.info.Ambiguity() <= parameters.maxAmbiguity)
		{
			survivorIndices.push_back(iSurvivor);
			filtered.push_back(current);
		}	//if(current.info.Ambiguity() <= parameters.maxAmbiguity)

		++iSurvivor;
	}	//for(const auto& current : src)

	//Decompose the range
	vector<Coordinate1T> srcL;
	vector<Coordinate2T> srcR;
	vector<MatchStrengthC> strength;
	tie(srcL, srcR, strength)=BimapToVector<Coordinate1T, Coordinate2T, MatchStrengthC>(filtered);
	size_t nFiltered=srcL.size();

	//Quantise
	vector<unsigned int> binsDefault=vector<unsigned int>(nFiltered,0);	//No quantisation case

	bool flagQuantiseLeft=(quantisationStrategy==DecimatorQuantisationStrategyT::LEFT || quantisationStrategy==DecimatorQuantisationStrategyT::BOTH);
	vector<unsigned int> binsL= flagQuantiseLeft ? Bucketise(srcL, binSize1) : binsDefault;

	bool flagQuantiseRight=(quantisationStrategy==DecimatorQuantisationStrategyT::RIGHT || quantisationStrategy==DecimatorQuantisationStrategyT::BOTH);
	vector<unsigned int> binsR= flagQuantiseRight ? Bucketise(srcR, binSize2) : binsDefault;

	//Sort
	multimap<double, size_t> rankToIndexMap;

	if(parameters.flagSort)
		rankToIndexMap=Sort(strength);
	else
		for(size_t c=0; c<nFiltered; ++c)
			rankToIndexMap.emplace(0,c);	//Default rank is the original order

	//Best balanced set
	OrderedBucketT orderedBuckets=MakeOrderedBucket(binsL, binsR, rankToIndexMap);
	list<size_t> orderedIndices=Peel(orderedBuckets, maxDensity, minElements, maxElements);

	//Compose the output
	typedef typename CorrespondenceContainerT::value_type CorrespondenceT;
	for(auto current : orderedIndices)
		get<0>(output).push_back(CorrespondenceT(srcL[current], srcR[current], strength[current]) );

	get<1>(output).resize(orderedIndices.size());
	transform(orderedIndices, get<1>(output).begin(), [&](size_t i){return survivorIndices[i];} );

	return output;
}	//CoordinateCorrespondenceListT<Coordinate1T, Coordinate2T> Decimate(const CorrespondenceRangeT& src, const ArrayDL& binSize1, const ArrayDR& binSize2, bool flagSort, unsigned int maxDensity)

/**
 * @brief Computes the bin dimensions by specifying the number of bins per standard deviation
 * @param[in] src A set of coordinate correspondences
 * @param binDensity1 Bin density for the first set, as bins/std
 * @param binDensity2 Bin density for the second set, as bins/std
 * @return A pair of density vectors, for the first and the second sets
 * @pre \c binDensity1 and \c binDensity2 are positive
 * @pre \c src is nonempty
 */
template<class Coordinate1T, class Coordinate2T>
auto CorrespondenceDecimatorC<Coordinate1T, Coordinate2T>::ComputeBinSize(const CorrespondenceContainerT& src, double binDensity1, double binDensity2) -> tuple<dimension_type1, dimension_type2>
{
	//Preconditions
	assert(!src.empty());
	assert(binDensity1>0);
	assert(binDensity2>0);

	//Correspondence->Coordinate conversion
	vector<Coordinate1T> coordinates1;
	vector<Coordinate2T> coordinates2;
	tie(coordinates1, coordinates2)=BimapToVector<Coordinate1T, Coordinate2T>(src);

	 //Standard deviation
	dimension_type1 mean1;
	dimension_type1 std1;
	tie(mean1, std1)=*CoordinateStatisticsT<Coordinate1T>::ComputeMeanAndStD(coordinates1);
	dimension_type1 binSize1=std1/binDensity1;

	dimension_type2 mean2;
	dimension_type2 std2;
	tie(mean2, std2)=*CoordinateStatisticsT<Coordinate2T>::ComputeMeanAndStD(coordinates2);
	dimension_type2 binSize2=std2/binDensity2;

	return make_tuple(binSize1, binSize2);
}	//tuple<dimension_type1, dimension_type2> ComputeBinSize(const CorrespondenceRangeT& src, double binDensity1, double binDensity2)


}	//ElementsN
}	//SeeSawN

#endif /* CORRESPONDENCE_DECIMATOR_IPP_0901234 */
