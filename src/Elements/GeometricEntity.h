/**
 * @file GeometricEntity.h Definition of various geometric entities
 * @author Evren Imre
 * @date 1 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef GEOMETRIC_ENTITY_H_7012334
#define GEOMETRIC_ENTITY_H_7012334

#include "GeometricEntity.ipp"

namespace SeeSawN
{
namespace ElementsN
{

/** @ingroup Elements */ //@{
typedef Hyperplane<typename ValueTypeM<Coordinate2DT>::type, 2> Line2DT;	///< 2D line

typedef Matrix< typename ValueTypeM<HCoordinate2DT>::type, 3, 3> Homography2DT;	///< 2D homography
typedef Matrix< typename ValueTypeM<HCoordinate3DT>::type, 4, 4> Homography3DT;	///< 3D homography
typedef Matrix< typename ValueTypeM<HCoordinate2DT>::type, 3, 3> EpipolarMatrixT;	///< Fundamental or essential matrix
typedef Matrix< typename ValueTypeM<HCoordinate3DT>::type, 4, 4> Quadric3DT;	///< 3D quadric

typedef Quaternion<ValueTypeM<Coordinate3DT>::type > QuaternionT;	///< A quaternion
typedef AngleAxis<ValueTypeM<Coordinate3DT>::type> AxisAngleT;	///< A rotation in axis-angle representation
typedef Matrix<ValueTypeM<Coordinate3DT>::type, 3, 1> RotationVectorT;	///< A rotation in rotation vector representation
typedef Matrix<ValueTypeM<HCoordinate2DT>::type, 3, 3> IntrinsicCalibrationMatrixT;	///< An intrinsic calibration matrix
typedef Matrix<ValueTypeM<Coordinate3DT>::type, 3, 3> RotationMatrix3DT;	///< A 3x3 rotation matrix
typedef Matrix<ValueTypeM<HCoordinate3DT>::type, 3, 4> CameraMatrixT;	///< A camera matrix
typedef AlignedBox<ValueTypeM<Coordinate2DT>::type, 2> FrameT;	///< Image frame type
typedef AlignedBox<ValueTypeM<Coordinate3DT>::type, 3> VolumeT;	///< Volume type
//@}

EpipolarMatrixT FundamentalToEssential(const EpipolarMatrixT& mF, const IntrinsicCalibrationMatrixT mK1, const IntrinsicCalibrationMatrixT mK2);	///< Converts a fundamental matrix to an essential matrix
EpipolarMatrixT EssentialToFundamental(const EpipolarMatrixT& mE, const IntrinsicCalibrationMatrixT mK1, const IntrinsicCalibrationMatrixT mK2);	///< Converts an essential matrix to a fundamental matrix

EpipolarMatrixT ComposeEssential(const Coordinate3DT& vT, const RotationMatrix3DT& mR, bool flagNormalise=true);	///< Essential matrix from relative calibration
template<class CorrespondenceRangeT> optional<tuple<Coordinate3DT, RotationMatrix3DT> > DecomposeEssential(const EpipolarMatrixT& mE, const CorrespondenceRangeT& validationSet, ValueTypeM<EpipolarMatrixT>::type zero);	///< Decomposes the epipolar matrix into relative calibration terms

tuple<ValueTypeM<Homography3DT>::type, Coordinate3DT, RotationMatrix3DT> DecomposeSimilarity3D(const Homography3DT& mH);	///< Decomposes a 3D similarity transformation
Homography3DT ComposeSimilarity3D(ValueTypeM<Homography3DT>::type scale, const Coordinate3DT& origin, const RotationMatrix3DT& orientation);	///< Composes a 3D similarity transformation
Matrix<ValueTypeM<Homography3DT>::type,16,13> JacobiandHdsCR(const Homography3DT& mH);	///< Jacobian of a similarity transformation wrt/ its components

Quadric3DT MakeDAQ(const Homography3DT& mH);	///< Dual absolute quadric from the rectifying homography
optional<Homography3DT> ExtractRectifyingHomography(const Quadric3DT& mQ);	///< Extracts the rectifying homography from the dual absolute quadric

Line2DT MakeLine2D(ValueTypeM<HCoordinate2DT>::type a, ValueTypeM<HCoordinate2DT>::type b, ValueTypeM<HCoordinate2DT>::type c);	///< Makes the line ax+by+c from the coefficients
optional<tuple<ValueTypeM<HCoordinate2DT>::type, ValueTypeM<HCoordinate2DT>::type> > DecomposeLine2D(const Line2DT& line);	///< Returns the parameters of the line y=ax+b

optional<tuple<ValueTypeM<Homography2DT>::type, ValueTypeM<Homography2DT>::type, RotationMatrix3DT> > DecomposeK2RK1i(Homography2DT mH);	///< Extract the focal lengths and relative rotation from the 2D homography due to a change in rotation and/or focal length

/********** EXTERN TEMPLATES **********/
extern template optional<tuple<Coordinate3DT, RotationMatrix3DT> > DecomposeEssential(const EpipolarMatrixT&, const CoordinateCorrespondenceList2DT& validationSet, ValueTypeM<EpipolarMatrixT>::type);

}	//ElementsN
}	//SeeSawN

#endif /* GEOMETRIC_ENTITY_H_7012334 */
