/**
 * @file GeometricEntity.cpp Implementation of various functions related to geometric entities
 * @author Evren Imre
 * @date 4 Dec 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "GeometricEntity.h"
namespace SeeSawN
{

namespace ElementsN
{

//Forward declarations
Matrix<ValueTypeM<CameraMatrixT>::type, 12,12> JacobiandPdCR(const CameraMatrixT&);

/**
 * @brief Converts a fundamental matrix to an essential matrix
 * @param[in] mF Fundamental matrix
 * @param[in] mK1 Intrinsic calibration matrix for the first camera
 * @param[in] mK2 Intrinsic calibration matrix for the second camera
 * @return Essential matrix with unit norm
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.257
 * @ingroup Utility
 */
EpipolarMatrixT FundamentalToEssential(const EpipolarMatrixT& mF, const IntrinsicCalibrationMatrixT mK1, const IntrinsicCalibrationMatrixT mK2)
{
	EpipolarMatrixT mE=mK2.transpose()*mF*mK1;
	mE.normalize();
	return mE;
}	//EpipolarMatrixT FundamentalToEssential(const EpipolarMatrixT& mF, const IntrinsicCalibrationMatrixT mK1, const IntrinsicCalibrationMatrixT mK2)

/**
 * @brief Converts an essential matrix to a fundamental matrix
 * @param[in] mE Essential matrix
 * @param[in] mK1 Intrinsic calibration matrix for the first camera
 * @param[in] mK2 Intrinsic calibration matrix for the second camera
 * @return Fundamental matrix witn unit norm
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.257
 * @ingroup Utility
 */
EpipolarMatrixT EssentialToFundamental(const EpipolarMatrixT& mE, const IntrinsicCalibrationMatrixT mK1, const IntrinsicCalibrationMatrixT mK2)
{
	EpipolarMatrixT mF=(mK2.transpose().inverse())*(mE*mK1.inverse());
	mF.normalize();
	return mF;
}	//EpipolarMatrixT EssentialToFundamental(const EpipolarMatrixT& mE, const IntrinsicCalibrationMatrixT mK1, const IntrinsicCalibrationMatrixT mK2)

/**
 * @brief Essential matrix from relative calibration
 * @param[in] vT Translation vector
 * @param[in] mR Rotation matrix
 * @param[in] flagNormalise If \c true , the result is normalised
 * @return Essential matrix
 * @remarks R. Hartley, A. Zissermann, Multiple View Geometry, 2nd Ed, 2003, pp.257
 * @ingroup Utility
 */
EpipolarMatrixT ComposeEssential(const Coordinate3DT& vT, const RotationMatrix3DT& mR, bool flagNormalise)
{
	EpipolarMatrixT mtR=MakeCrossProductMatrix(vT)*mR;

	if(flagNormalise)
		mtR.normalize();

	return mtR;
}	//EpipolarMatrixT ComposeEssential(const Coordinate3DT& vT, const RotationMatrix3DT& mR)

/**
 * @brief Decomposes a 3D similarity transformation
 * @param[in] mH Homography to be decomposed
 * @return A tuple: Scale, origin, orientation
 * @pre \c mH is a similarity transformation (unenforced)
 * @remarks \c mH is decomposed as \f$ \mathbf{R(X-C}\f$
 * @remarks If \c mH is not a true similiarity transformation, the decomposition is only approximate
 * @ingroup Utility
 */
tuple<ValueTypeM<Homography3DT>::type, Coordinate3DT, RotationMatrix3DT> DecomposeSimilarity3D(const Homography3DT& mH)
{
	typedef ValueTypeM<Homography3DT>::type RealT;

	Homography3DT mHn=mH/mH(3,3);	//Normalise

	//RQ decomposition to extract the rotation matrix and the scale
	//For a similarity transformation, the result should be sI*R. Mind that K is an upper triangular matrix- not necessarily an intrinsic calibration matrix!
	Matrix<RealT, 3, 3> mKR=mHn.block(0,0,3,3);
	IntrinsicCalibrationMatrixT mK;
	RotationMatrix3DT mR;
	tie(mK, mR)=RQDecomposition33(mKR);

	//By convention, scale is positive
	Matrix<RealT, 3, 3> mT; mT.setIdentity();
	for(size_t c=0; c<3; ++c)
		if(mK(c,c)<0)
			mT(c,c)=-1;

	RealT scale=0.5*fabs(mK(0,0)+mK(1,1));
	mR = mT*mR;	//mT is its own inverse

	Coordinate3DT vC=-mKR.inverse()*mHn.block(0,3,3,1);	//t=-RC

	return make_tuple(scale, vC, mR);
}	//tuple<double, Coordinate3DT, RotationMatrix3DT> DecomposeSimilarity3D(const Homography3DT& mH)

/**
 * @brief Composes a 3D similarity transformation
 * @param[in] scale Scale
 * @param[in] origin Origin of the new coordinate frame
 * @param[in] orientation Orientation of the new coordinate frame
 * @return 3D Homography matrix representing the transformation
 * @remarks The return value represents the transformation \f$ \mathbf{R(X-C}\f$
 * @ingroup Utility
 */
Homography3DT ComposeSimilarity3D(ValueTypeM<Homography3DT>::type scale, const Coordinate3DT& origin, const RotationMatrix3DT& orientation)
{
	Homography3DT mH; mH.row(3)<<0,0,0,1;
	mH.block(0,0,3,3)=scale*orientation;
	mH.block(0,3,3,1)=-mH.block(0,0,3,3)*origin;
	return mH;
}	//Homography3DT ComposeSimilarityTransformation(ValueTypeM<Homography3DT>::value scale, const Coordinate3DT& origin, const RotationMatrix3DT& orientation)

/**
 * @brief Jacobian of a similarity transformation wrt/ its components
 * @param[in] mH Homography
 * @return Jacobian. [scale; centre(3); orientation(9)]
 * @ingroup Jacobian
 */
Matrix<ValueTypeM<Homography3DT>::type,16,13>  JacobiandHdsCR(const Homography3DT& mH)
{
	typedef ValueTypeM<Homography3DT>::type RealT;
	RealT s;
	Coordinate3DT vC;
	RotationMatrix3DT mR;
	tie(s, vC, mR)=DecomposeSimilarity3D(mH);

	Homography3DT mHn=mH/s;	//Homography without the scale

	Matrix<RealT,16,13> mJ; mJ.setZero();

	typedef Matrix<RealT,12,1> VectorT;
	mJ.col(0).head(12)=Reshape<VectorT>(mHn.topRows(3), 12, 1);	//dHds = dsHnds

	//dHdC,R = s dHndC,R
	mJ.block(0,1,12,12)=s*JacobiandPdCR(mHn.block(0,0,3,3));

	return mJ;
}	//Matrix<ValueTypeM<Homography3DT>::type,12,13> JacobiandHdsCR(const Homography3DT& mH)

/**
 * @brief Dual absolute quadric from the rectifying homography
 * @param[in] mH Rectifying homography
 * @return Dual absolute quadric
 * @ingroup Utility
 */
Quadric3DT MakeDAQ(const Homography3DT& mH)
{
	Homography3DT mH2(mH); mH2.col(3).setConstant(0);
	Quadric3DT mQ=mH2*mH2.transpose();
	mQ.normalize();

	return mQ;
}	//Quadric3DT MakeDAQ(const Homography3DT& mH)

/**
 * @brief Extracts the rectifying homography from the dual absolute quadric
 * @param[in] mQ Dual abosolute quadric
 * @return Rectifying homography
 * @pre \c mDAQ is symmetric and rank 3 (unenforced)
 * @ingroup Utility
 */
optional<Homography3DT> ExtractRectifyingHomography(const Quadric3DT& mQ)
{
	optional<Homography3DT> output;

	//Eigendecomposition is not guaranteed to yield a symmetric eigenvector matrix
	//LDLT is not rank-revealing- not sure about its implications for this problem (after all, the determinant of D is still 0)
	//SVD: In other libraries, there were cases where U was not identical to V (strange!)

	JacobiSVD<Quadric3DT, Eigen::FullPivHouseholderQRPreconditioner> svd(0.5*(mQ+mQ.transpose()), Eigen::ComputeFullU | Eigen::ComputeFullV);	//Symmetrise, and SVD
	assert(svd.nonzeroSingularValues()==3);

	Homography3DT mH(svd.matrixU());
	for(size_t c=0; c<3; ++c)
		mH.col(c)*=sqrt(svd.singularValues()[c]);
	mH.col(3)<<0, 0, 0, 1;

	output=mH;
	return output;
}	//optional<Homography3DT> ExtractRectifyingHomography(const Quadric3DT& mDAQ)

/**
 * @brief Makes the line ax+by+c from the coefficients
 * @param a Coefficient of x
 * @param b Coefficient of y
 * @param c Intercept
 * @return Line object
 */
auto MakeLine2D(ValueTypeM<HCoordinate2DT>::type a, ValueTypeM<HCoordinate2DT>::type b, ValueTypeM<HCoordinate2DT>::type c) ->Line2DT
{
	return Line2DT(Coordinate2DT(a,b),c);
}	//Line2DT MakeLine2D(ValueTypeM<HCoordinate2DT>::type a, ValueTypeM<HCoordinate2DT>::type b, ValueTypeM<HCoordinate2DT>::type c)

/**
 * @brief Returns the parameters of the line y=ax+b
 * @param line Line object
 * @return The slope and the intercept for the line. Invalid if the line is parallel to the y axis (i.e. has infinite slope)
 */
optional<tuple<ValueTypeM<HCoordinate2DT>::type, ValueTypeM<HCoordinate2DT>::type> > DecomposeLine2D(const Line2DT& line)
{
	typedef ValueTypeM<HCoordinate2DT>::type RealT;
	optional<tuple<RealT, RealT> > output= (line.coeffs()[1]==0) ? optional<tuple<RealT, RealT>>() : make_tuple(-line.coeffs()[0]/line.coeffs()[1], -line.coeffs()[2]/line.coeffs()[1]);
	return output;
}	//optional<tuple<ValueTypeM<HCoordinate2DT>::type, ValueTypeM<HCoordinate2DT>::type> > ExtractLineParameters(const Line2DT& line)

/**
 * @brief Extract the focal lengths and relative rotation from the 2D homography due to a change in rotation and/or focal length
 * @param[in] mH Homography to be decomposed
 * @return A tuple: Focal length for the first camera, focal length for the second camera, orientation relative to the SECOND camera. Fails if
 * 	- In-plane rotation
 * 	- No real focal length values
 * 	- Negative determinant
 * @pre \c mH is normalised with the known skewness, aspect ratio and principal point on both sides, i.e. the intrinsic calibration matrices are of the form [f f 1] (unenforced)
 * @remarks If the calibration parameters of a camera pair differ by orientation and intrinsics, the points in the first image are mapped to the second via the homography \f$ \mathbf{ K_2 R_2 R_1^T K_1^{-1}  } \f$
 * @remarks Algorithm
 * 	- Compute an estimate of the first focal length from each row pair
 * 	- Compute an estimate of the second focal length from each column pair
 * 	- For each combination of focal lengths, estimate the rotation matrix. Pick the one with the smallest error
 * @ingroup Utility
 */
optional<tuple<ValueTypeM<Homography2DT>::type, ValueTypeM<Homography2DT>::type, RotationMatrix3DT> > DecomposeK2RK1i(Homography2DT mH)
{
	typedef ValueTypeM<Homography2DT>::type RealT;

	//Focal length estimation

	optional< tuple<RealT, RealT, RotationMatrix3DT> > output;

	//f2
	RealT zero=3*numeric_limits<RealT>::epsilon();
	RealT num, denum, fSq;

	vector<RealT> f2; f2.reserve(3);
	for(size_t c1=0; c1<3; ++c1)
		for(size_t c2=c1+1; c2<3; ++c2)
		{
			num=mH.col(c1).dot(mH.col(c2));
			denum=mH(2,c1)*mH(2,c2);

			fSq=1-num/denum;
			if(fSq<zero)
				continue;

			f2.push_back(sqrt(fSq));
		}	//for(size_t c2=c1+1; c2<3; ++c2)

	if(f2.empty())
		return output;

	//f1
	vector<RealT> f1; f1.reserve(3);
	for(size_t c1=0; c1<3; ++c1)
		for(size_t c2=c1+1; c2<3; ++c2)
		{
			num=mH.row(c1).dot(mH.row(c2));
			denum=mH(c1,2)*mH(c2,2);

			fSq=1-num/denum;
			if(fSq<zero)
				continue;

			f1.push_back((RealT)1/sqrt(fSq));
		}	//for(size_t c2=c1+1; c2<3; ++c2)

	if(f1.empty())
		return output;

	//Rotation estimation

	//All components of the decomposition should have a positive determinant
	if(mH.determinant()<0)
		return output;

	IntrinsicCalibrationMatrixT mK1=IntrinsicCalibrationMatrixT::Identity();
	IntrinsicCalibrationMatrixT mK2i=IntrinsicCalibrationMatrixT::Identity();

	RealT minError=numeric_limits<RealT>::infinity();
	Homography2DT mK2iH;
	Homography2DT mK2iHK1;
	for(size_t c2=0; c2<3; ++c2)
	{
		mK2i(0,0)=1.0/f2[c2]; mK2i(1,1)=mK2i(0,0);
		mK2iH=mK2i*mH;

		for(size_t c1=0; c1<3; ++c1)
		{
			mK1(0,0)=f1[c1]; mK1(1,1)=mK1(0,0);
			mK2iHK1=mK2iH * mK1;

			RealT scale=sqrt(3)/mK2iHK1.norm();	//Restore the norm to sqrt(3)
			mK2iHK1*=scale;

			RotationMatrix3DT mR=ComputeClosestRotationMatrix(mK2iHK1);

			RealT error= (mR-mK2iHK1).norm();
			if(error<minError)
			{
				minError=error;
				output=make_tuple(f1[c1], f2[c2], mR);
			}	//if(error>minError)
		}	//for(size_t c2=0; c2<3; ++c2)
	}	//for(size_t c2=0; c2<3; ++c2)

	return output;
}	//tuple<double, double, RotationMatrix3DT> DecomposeK1RK2i(const Homography2DT& mH)

/********** EXPLICIT INSTANTIATIONS **********/
template optional<tuple<Coordinate3DT, RotationMatrix3DT> > DecomposeEssential(const EpipolarMatrixT&, const CoordinateCorrespondenceList2DT& validationSet, typename ValueTypeM<EpipolarMatrixT>::type );

}	//ElementsN
}	//SeeSawN

