/**
 * @file Viewpoint3D.ipp Implementation details for the 3D viewpoint object
 * @author Evren Imre
 * @date 29 Jan 2016
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef VIEWPOINT3D_IPP_6109232
#define VIEWPOINT3D_IPP_6109232

#include <Eigen/Dense>
#include <tuple>
#include "Coordinate.h"
#include "GeometricEntity.h"
#include "Camera.h"
#include "../Geometry/Rotation.h"
#include "../Wrappers/EigenMetafunction.h"
#include "../Numeric/LinearAlgebraJacobian.h"

namespace SeeSawN
{
namespace ElementsN
{

using Eigen::Matrix;
using std::ignore;
using std::tie;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Homography3DT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::ElementsN::DecomposeSimilarity3D;
using SeeSawN::GeometryN::JacobianQuaternionConjugation;
using SeeSawN::GeometryN::JacobianQuaternionMultiplication;
using SeeSawN::GeometryN::JacobianQuaternionToRotationMatrix;
using SeeSawN::WrappersN::ValueTypeM;
using SeeSawN::NumericN::JacobiandABdB;
using SeeSawN::NumericN::JacobiandABdA;

/**
 * @brief A viewpoint in a 3D coordinate frame
 * @ingroup Elements
 */
struct Viewpoint3DC
{
	typedef ValueTypeM<Coordinate3DT>::type real_type;	///< Floating point type

	Coordinate3DT position;	///< Position
	Quaternion<ValueTypeM<Coordinate3DT>::type> orientation;	///< Orientation

	Viewpoint3DC(const Coordinate3DT& pposition=Coordinate3DT(0,0,0), const Quaternion<ValueTypeM<Coordinate3DT>::type>& oorientation=Quaternion<ValueTypeM<Coordinate3DT>::type>(1,0,0,0));

	Viewpoint3DC(const CameraMatrixT& mP);	///< Construction from a camera matrix
	Viewpoint3DC(const Homography3DT& mH);	///< Construction from a similarity matrix
};	//struct Viewpoint3DC


}	//ElementsN
}	//SeeSawN

#endif /* VIEWPOINT3D_IPP_6109232 */
