/**
 * @file FeatureUtility.h Public interface for various utilities for manipulating features
 * @author Evren Imre
 * @date 19 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef FEATURE_UTILITY_H_3413156
#define FEATURE_UTILITY_H_3413156

#include "FeatureUtility.ipp"
namespace SeeSawN
{
namespace ElementsN
{

template<class CoordinateRangeT> vector<size_t> DecimateFeatures2D(const CoordinateRangeT& features, typename ValueTypeM<typename range_value<CoordinateRangeT>::type>::type stride, const Coordinate2DT& upperLeft, const Coordinate2DT& lowerRight); ///<Decimates a 2D coordinate set by picking the elements closest to a specified grid
template<class CoordinateRangeT> tuple<Coordinate2DT, Coordinate2DT> ComputeBoundingBox2D(const CoordinateRangeT& coordinates);    ///< Finds the bounding box for a set of 2D coordinates

template<class FeatureRangeT> vector<typename DecomposeFeatureRangeM<FeatureRangeT>::coordinate_type > MakeCoordinateVector(const FeatureRangeT& features); ///< Extracts the coordinate components of a feature list
template<class FeatureRangeT, class MatrixT> void ApplyFeatureCoordinateTransformation(FeatureRangeT& featureList, const MatrixT& mT);	///< Applies a homogeneous transformation to the coordinate components of a range of features
template<class FeatureRangeT, class BoxT> vector<size_t> FindContained(const FeatureRangeT& featureList, const BoxT& box);	///< Returns the indices of the features, whose coordinates are contained in a specified box

namespace DetailN
{
template<class Point2DT> struct LexicalCompare2DC; ///< Lexical comparison of a pair of 2D points
template<typename ValueT> struct LexicalCompare2DC<point_xy<ValueT> >;  ///< Lexical comparison for 2D points, specialised for Boost.Geometry
}   //DetailN

/********** EXTERN TEMPLATES **********/
extern template vector<Coordinate2DT> MakeCoordinateVector(const vector<ImageFeatureC>&);
extern template void ApplyFeatureCoordinateTransformation(vector<ImageFeatureC>&, const Matrix<ValueTypeM<Coordinate2DT>::type,3,3>&);
extern template void ApplyFeatureCoordinateTransformation(vector<SceneFeatureC>&, const Matrix<ValueTypeM<Coordinate3DT>::type,4,4>&);


namespace DetailN
{
extern template class LexicalCompare2DC<Coordinate2DT>;
}   //DetailN
}   //ElementsN
}	//SeeSawN

#endif /* FEATURE_UTILITY_H_3413156 */
