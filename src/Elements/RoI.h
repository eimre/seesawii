/**
 * @file RoI.h Region-of-Interest types
 * @author Evren Imre
 * @date 29 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef ROI_H_2356123
#define ROI_H_2356123

#include <Eigen/Dense>
#include <vector>

namespace SeeSawN
{
namespace ElementsN
{

/** @ingroup Elements */ //@{
//Vector type (precision and fixed/dynamic) can be changed
typedef Eigen::VectorXd ImageRoIT;  ///< An image region-of-interest
//@}
}   //ElementsN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/
extern template class std::vector<SeeSawN::ElementsN::ImageRoIT>;

#endif /* ROI_H_2356123 */
