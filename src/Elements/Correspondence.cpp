/**
 * @file Correspondence.cpp Implementation and instantiatino of correspondence-related structures
 * @author Evren Imre
 * @date 14 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "Correspondence.h"

namespace SeeSawN
{
namespace ElementsN
{

/********** MatchStrengthC **********/

/**
 * @brief Constructor
 * @param[in] ssimilarity Similarity score
 * @param[in] aambiguity Ambiguity value
 */
MatchStrengthC::MatchStrengthC(double ssimilarity, double aambiguity) : similarity(ssimilarity), ambiguity(aambiguity)
{}

/**
 * @brief Returns a reference to similarity
 * @return A reference to similarity
 */
double& MatchStrengthC::Similarity()
{
	return similarity;
}	//double& MatchStrengthC::Similarity()

/**
 * @brief Returns a reference to ambiguity
 * @return A reference to ambiguity
 */
double& MatchStrengthC::Ambiguity()
{
	return ambiguity;
}	//double& MatchStrengthC::Ambiguity()

/**
 * @brief Returns a constant reference to similarity
 * @return A reference to similarity
 */
const double& MatchStrengthC::Similarity() const
{
	return similarity;
}	//const double& MatchStrengthC::Similarity() const

/**
 * @brief Returns a constant reference to ambiguity
 * @return A reference to ambiguity
 */
const double& MatchStrengthC::Ambiguity() const
{
	return ambiguity;
}	//const double& MatchStrengthC::Ambiguity() const

/********** FREE FUNCTIONS **********/

/**
 * @brief Filters an index correspondence set via match strength
 * @param[in,out] correspondences Correspondences to be filtered
 * @param[in] threshold Threshold indicating the minimum similarity and maximum ambiguity
 * @ingroup Elements
 */
void ApplyStrengthFilter(CorrespondenceListT& correspondences, const MatchStrengthC& threshold)
{
	CorrespondenceListT filtered;
	for(const auto& current : correspondences)
		if(current.info.Similarity()>=threshold.Similarity() && current.info.Ambiguity()<=threshold.Ambiguity())
			filtered.push_back(current);

	correspondences.swap(filtered);
}	//void ApplyStrengthFilter(CorrespondenceListT& correspondences, const MatchStrengthC& threshold)

/********** EXPLICIT INSTANTIATIONS **********/
template CoordinateCorrespondenceList2DT MakeCoordinateCorrespondenceList(const CorrespondenceListT&, const vector<Coordinate2DT>&, const vector<Coordinate2DT>&);
template void MakeFeatureCorrespondenceList(vector<ImageFeatureC>&, vector<ImageFeatureC>&, const CorrespondenceListT&);
template tuple<CorrespondenceListT, vector<size_t> > SortByAmbiguity(const CorrespondenceListT&);
template vector<unsigned int> RankByAmbiguity(const CorrespondenceListT&);

template Matrix<typename ValueTypeM<Coordinate2DT>::type, Eigen::Dynamic, 1> CorrespondenceRangeToVector<typename ValueTypeM<Coordinate2DT>::type, CoordinateCorrespondenceList2DT>(const CoordinateCorrespondenceList2DT&);
template bimap<list_of<Coordinate2DT>, list_of<Coordinate2DT>, left_based> VectorToCorrespondenceRange(const iterator_range<ValueTypeM<Coordinate2DT>::type*>&, size_t, size_t);

}   //ElementsN
}	//SeeSawN

/********** EXPLICIT INSTANTIATIONS **********/
template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::size_t>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::size_t>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;


template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate2DT>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate2DT>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;

template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate3DT>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate2DT>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;

template class SeeSawN::ElementsN::bimap<SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate3DT>,
                                                SeeSawN::ElementsN::list_of<SeeSawN::ElementsN::Coordinate3DT>,
                                                SeeSawN::ElementsN::left_based,
                                                SeeSawN::ElementsN::with_info<SeeSawN::ElementsN::MatchStrengthC> >;
