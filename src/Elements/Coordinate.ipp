/**
 * @file Coordinate.ipp Details for coordinates
 * @author Evren Imre
 * @date 25 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COORDINATE_IPP_1316356
#define COORDINATE_IPP_1316356

#include <boost/concept_check.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/metafunctions.hpp>
#include <boost/range/functions.hpp>
#include <Eigen/Dense>
#include <vector>
#include <type_traits>
#include <cstddef>
#include <cmath>
#include <array>
#include <algorithm>
#include <climits>
#include <iterator>

namespace SeeSawN
{
namespace ElementsN
{

using boost::ForwardRangeConcept;
using boost::range_value;
using std::transform;
using std::vector;
using std::is_same;
using std::is_arithmetic;
using std::size_t;
using std::fabs;
using std::array;
using std::numeric_limits;
using std::max_element;
using std::distance;

template<typename RealT, unsigned int DIM> using HCoordinateVectorT=Eigen::Matrix<RealT, DIM+1, 1>;

/**
 * @brief Aligns the signs of a range of homogeneous coordinates
 * @tparam ValueT Value type of the coordinates
 * @tparam DIM Dimensionality of the space
 * @tparam HCoordinateRangeT A range of homogeneous coordinates
 * @param[in] coordinates Coordinate list. Size of each coordinate vector is DIM+1
 * @return Coordinates with consistent signs
 * @pre \c HCoordinateRangeT is a forward range of elements of type \c HCoordinateVectorT<ValueT, DIM>
 * @pre \c ValueT is an arithmetic type
 * @remarks Finds the dimension with the largest minimum deviation from 0, and sets it to positive. More robust than using the last dimension.
 * @remarks For more stable operation, all vectors should be at the same scale
 * @ingroup Elements
 */
template<typename ValueT, unsigned int DIM, class HCoordinateRangeT>
vector<HCoordinateVectorT<ValueT, DIM> > AlignSign(const HCoordinateRangeT& coordinates)
{
	//Preconditions
	typedef HCoordinateVectorT<ValueT, DIM> HCoordinateT;
	BOOST_CONCEPT_ASSERT((ForwardRangeConcept<HCoordinateRangeT>));
	static_assert(is_same<typename range_value<HCoordinateRangeT>::type, HCoordinateT>::value, "AlignSigns : HCoordinateRangeT must hold elements of type HCoordinateT");
	static_assert(is_arithmetic<ValueT>::value, "AlignSign : ValueT must be an arithmetic type" );

	//Find the dimension with the largest minimum deviation
	array<ValueT, DIM+1> deviations; deviations.fill(numeric_limits<ValueT>::max());
	auto action=[&](ValueT v, ValueT& d){return fabs(v)<d ? fabs(v):d;};
	for(const auto& current : coordinates)
		transform(current.data(), current.data()+DIM+1, deviations.begin(), deviations.begin(), action);	//Copy if smaller than the largest deviation so far

	auto it=max_element(deviations.begin(), deviations.end());
	size_t index=distance(deviations.begin(), it);

	//Now multiply with -1 as necessary, so that indexth element is positive
	size_t nElements=boost::distance(coordinates);
	vector<HCoordinateT> output; output.reserve(nElements);
	for(const auto& current : coordinates)
		output.push_back( current[index]<0 ? (-current).eval() : current );

	return output;
	return coordinates;
}	//vector<HCoordinateVectorT<DIM> > AlignSigns(const HCoordinateRangeT& coordinates)


}   //ElementsN
}	//SeeSawN

#endif /* COORDINATE_IPP_1316356 */
