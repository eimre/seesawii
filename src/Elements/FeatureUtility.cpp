/**
 * @file FeatureUtility.cpp Implementation and instantiations of feature utilities
 * @author Evren Imre
 * @date 19 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "FeatureUtility.h"

namespace SeeSawN
{
namespace ElementsN
{
/********** EXPLICIT INSTANTIATIONS **********/
template vector<Coordinate2DT> MakeCoordinateVector(const vector<ImageFeatureC>&);
template void ApplyFeatureCoordinateTransformation(vector<ImageFeatureC>&, const Matrix<ValueTypeM<Coordinate2DT>::type,3,3>&);
template void ApplyFeatureCoordinateTransformation(vector<SceneFeatureC>&, const Matrix<ValueTypeM<Coordinate3DT>::type,4,4>&);

namespace DetailN
{
template class LexicalCompare2DC<Coordinate2DT>;
}   //DetailN

}   //ElementsN
}	//SeeSawN


