/**
 * @file Coordinate.h
 * @author Evren Imre
 * @date 25 Aug 2013
 */

/***************************************************************************
This file is a part of SeeSawII, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

SeeSawII is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SeeSawII is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SeeSawII.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifndef COORDINATE_H_2362656
#define COORDINATE_H_2362656

#include "Coordinate.ipp"

namespace SeeSawN
{
namespace ElementsN
{

/** @ingroup Elements */ //@{
//Do not change anything other than the precision. Either all double, or all float
typedef Eigen::Vector2d Coordinate2DT;  ///< A 2D coordinate
typedef Eigen::Vector3d Coordinate3DT;  ///< A 3D coordinate

typedef Eigen::Vector3d HCoordinate2DT;  ///< A homogeneous 2D coordinate
typedef Eigen::Vector4d HCoordinate3DT;  ///< A homogeneous 3D coordinate

template<typename RealT, unsigned int DIM> using CoordinateVectorT=Eigen::Matrix<RealT, DIM, 1>;	///< A generic coordinate vector
template<typename RealT, unsigned int DIM> using HCoordinateVectorT=Eigen::Matrix<RealT, DIM+1, 1>;	///< A generic homogeneous coordinate vector
//@}

template<typename ValueT, unsigned int DIM, class HCoordinateRangeT> vector<HCoordinateVectorT<ValueT, DIM> > AlignSign(const HCoordinateRangeT& coordinates);	///< Aligns the signs of a range of homogeneous coordinates

}   //ElementsN
}	//SeeSawN

/********** EXTERN TEMPLATES **********/

extern template class std::vector<SeeSawN::ElementsN::Coordinate2DT>;
extern template class std::vector<SeeSawN::ElementsN::Coordinate3DT>;
extern template class std::vector<SeeSawN::ElementsN::HCoordinate2DT>;
extern template class std::vector<SeeSawN::ElementsN::HCoordinate3DT>;


#endif /* COORDINATE_H_2362656 */
